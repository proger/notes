# Science

 * [Science Fair Project Ideas, Answers, & Tools](https://www.sciencebuddies.org/).
 * [Kidi'science | Faire aimer les Sciences aux Enfants !](https://kidiscience.cafe-sciences.org/).

 * [Science](https://www.science.org/).

## Chimie

 * [3 manières de fabriquer des cristaux de sel - wikiHow](https://fr.wikihow.com/fabriquer-des-cristaux-de-sel).

 * [Chiralité](https://fr.wikipedia.org/wiki/Chiralit%C3%A9).
 * [Diastéréoisomérie](https://fr.wikipedia.org/wiki/Diast%C3%A9r%C3%A9oisom%C3%A9rie).
 * [Dimère](https://fr.wikipedia.org/wiki/Dim%C3%A8re).
 * [Énantiomérie](https://fr.wikipedia.org/wiki/%C3%89nantiom%C3%A9rie).
 * [Isomérie](https://fr.wikipedia.org/wiki/Isom%C3%A9rie).
 * [Tautomère](https://fr.wikipedia.org/wiki/Tautom%C3%A8re).
 * [Zwitterion](https://fr.wikipedia.org/wiki/Zwitterion).

## Climat

 * [L'Île aux mesures](https://ile-aux-mesures.institut-polaire.fr/).

## Earthquakes

 * [Site de restitution Criter](http://criter.irsn.fr/exercice/acteur/).
 * [CEA alertes sismiques](http://www-dase.cea.fr/evenement/dernieres_alertes.php).

## Genetics

ChIP-Seq:
 * [Cartographie des sites des interactions ADN- protéines : CHIP seq](https://www.france-genomique.org/expertises-technologiques/regulome/cartographie-des-sites-des-interactions-adn-proteines-chip-seq-2/).
 * [ChIP-Seq](https://fr.wikipedia.org/wiki/ChIP-Seq).

 * [Cartographie des sites des interactions ADN- protéines : CHIP seq - France Génomique](https://www.france-genomique.org/expertises-technologiques/regulome/cartographie-des-sites-des-interactions-adn-proteines-chip-seq-2/).

## Informatique

 * [De quoi ChatGPT est-il VRAIMENT capable ? | Ft. Science4All](https://www.youtube.com/watch?v=R2fjRbc9Sa0).
 * [LLM failure archive (ChatGPT and beyond)](https://github.com/giuven95/chatgpt-failures).

## Nature

 * [xeno-canto :: Sharing bird sounds from around the world](https://www.xeno-canto.org/).
 * [Wild Sanctuary](http://www.wildsanctuary.com), Bernie Krause. Sound of nature.
 * [The voice of the natural world](https://www.ted.com/talks/bernie_krause_the_voice_of_the_natural_world?language=en).

Sea:
 * [Deep Ocean Education project](https://deepoceaneducation.org/).
 * [Nautilus Live](https://nautiluslive.org/).
 * [Schmidt Ocean Institute](https://schmidtocean.org/).

## Optique

 * [Polarisation de la lumière](https://www.gatinel.com/recherche-formation/la-nature-de-la-lumiere-approche-historique/polarisation-de-la-lumiere/).
 * [Exploring Materials—Polarizers](https://www.nisenet.org/sites/default/files/catalog/uploads/MaterialsPolarizers_guide_14nov14.pdf).

## Electronics

## Maths

 * [Statistiques pour statophobes](https://perso.univ-rennes1.fr/denis.poinsot/Statistiques_%20pour_statophobes/).
 * [StatistiqueDescriptive Multidimensionnelle (pourlesnuls)](http://www.math.univ-toulouse.fr/~baccini/zpedago/asdm.pdf).
 * [Variance](https://fr.wikipedia.org/wiki/Variance_(math%C3%A9matiques)).
 * [Écart type (Standard deviation)](https://fr.wikipedia.org/wiki/%C3%89cart_type).
 * [Standard error](https://en.wikipedia.org/wiki/Standard_error).
 * [Médiane](https://fr.wikipedia.org/wiki/M%C3%A9diane_(statistiques)).
 * [Understanding Boxplots](https://towardsdatascience.com/understanding-boxplots-5e2df7bcbd51).
 * [Linear regression](https://en.wikipedia.org/wiki/Linear_regression).
 * [Multiple Linear Regression](http://dept.stat.lsa.umich.edu/~kshedden/Courses/Stat401/Notes/401-multreg.pdf).

 * [Voronoi diagram](https://en.wikipedia.org/wiki/Voronoi_diagram).

 * [The On-Line Encyclopedia of Integer Sequences](http://oeis.org/).

 * [The On-Line Encyclopedia of Integer Sequences® (OEIS®)](http://oeis.org/).
 * [List of integer sequences](https://en.wikipedia.org/wiki/List_of_integer_sequences).

### Geometry

 * [Area of a polygon](http://www.mathopenref.com/coordpolygonarea.html).

## Société

 * [Fouloscopie](https://www.youtube.com/c/Fouloscopie).
  + [10 niveaux de foule expliqués avec des tomates](https://www.youtube.com/watch?v=Eh7l4Gvx054).

## Histoire

 * Kyle Harper : Comment l'Empire romain s'est effondré (Format Kindle 9.49€) (The Fate of Rome: Climate, Disease, and the End of an Empire).
