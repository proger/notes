# Jardinage

 * [Larves du hanneton ou vers blancs : comment s'en débarrasser](https://jardinage.lemonde.fr/dossier-725-larve-hanneton-blanc-debarrasser.html).
 * [Hannetons et vers blancs](http://www.chambres-agriculture-bretagne.fr/ca1/PJ.nsf/TECHPJPARCLEF/20731/$File/Note%20nationale%20-%20Hannetons-et-vers-blancs-BSV2013_07.pdf?OpenElement).
 * [European chafer](https://en.wikipedia.org/wiki/European_chafer).

 * [Utiliser les cendres au potager](https://www.gammvert.fr/conseils/conseils-de-jardinage/utiliser-les-cendres-au-potager).

 * [Art. 892 codice civile - Distanze per gli alberi](https://www.brocardi.it/codice-civile/libro-terzo/titolo-ii/capo-ii/sezione-vi/art892.html).

03 Février 2021 sur Vendita Piante Online (Vivai le Georgiche Soc. Agr., Via
Circonvallazione Acciaierie, 25012 Calvisano (BRESCIA), www.legeorgiche.it) :
| -------------------------------------------------------------- | -------- | -------- |
| Prodotto                                                       | Quantità | Prezzo   |
| -------------------------------------------------------------- | -------- | -------- |
| Prunus "Kiku shidare-zakura" (Ciliegio da fiore) [H. 160+ cm.] | 2        | 100,00 € |
| Acer japonicum "Vitifolium" (Acero) [H. 70 cm.]                | 1        | 67,00 €  |
| Acer palmatum "Autumn Red" (Acero) [H. 30-40 cm.]              | 1        | 67,00 €  |
| Cortaderia selloana "Rosea" (Erba della Pampas) - Ø 17 cm      | 3        | 39,00 €  |
| Callistemon Laevis [Vaso Ø14cm]                                | 1        | 11,00 €  |
| Callistemon citrinus "Splendens" [Vaso Ø14cm]                  | 1        | 9,50 €   |
| Betula alba (Betulla) [H. 150-180 cm.]                         | 1        | 50,00 €  |
| Betula pendula (Betulla Bianca) [H. 150-180 cm.]               | 1        | 40,00 €  |
| -------------------------------------------------------------- | -------- | -------- |
	Subtotale:                383,50 €
	Spedizione:               22,50 € tramite Corriere espresso
	Metodo di pagamento:      Paga con il tuo account Amazon
	Totale:                   406,00 €

## Camelia japonica

 * [Camélia du japon](https://www.aujardin.info/plantes/camellia-japonica.php).
Dimensions 3 x 3 m.
Expositionmi-ombre, ombre.
Terre de bruyère, pas calcaire.

## Photinia

 * [Photinia x fraseri compatta "Red Robin"](https://www.venditapianteonline.it/shop/photinia-x-fraseri-compatta-red-robin/).
 * [PHOTINIA X FRASERI 'RED ROBIN'](https://www.gammvert.fr/2-1212-plantes-dexterieur/2-1230-arbres/3-31-arbres-pour-situation-isolee/p-38541-photinia-x-fraseri-red-robin).

Soleil, mi-ombre.
H 2-3m.

## Tasso "Hillii" (IF HYBRIDE HILLII)

 * [Taxus x media "Hillii" (Tasso)](https://www.venditapianteonline.it/shop/taxus-x-media-hillii/).
 * [TAXUS x media 'HILLII](https://www.pepinieres-minier.fr/produit/taxus-x-media-hillii).

Tollera l’aridità e l’inquinamento atmosferico.
H 3m.

## Pittosporo

 * [Pittosporum tobira "Nanum" (Pittosporo nano)](https://www.venditapianteonline.it/shop/pittosporum-tobira-nanum/).

Raggiunge l’altezza di circa 80 cm.
Mezzombra / sole.
Larghezza (estensione) 5 m.

## Rhododendron

 * [Rhododendron : variétés, plantation et entretien](https://www.truffaut.com/rhododendron-plantation-entretien.html).

Plantés en mars 2021 :
 * Rhododendron (Rododendro) Bianco [Vaso Ø 14 cm]
 * Rhododendron (Rododendro) Rosa   [Vaso Ø 16 cm]

Abrité du vent et du soleil brûlant.
Exposition ombragée ou semi-ombragée.

## Pivoines

 * [Planter les pivoines](https://www.gammvert.fr/conseils/conseils-de-jardinage/planter-les-pivoines#:~:text=O%C3%B9%20et%20quand%20planter%20vos,automne%20d'octobre%20%C3%A0%20mars.).

Sols profonds, riches en humus.
Situation abritée et bien ensoleillée.

## Arbre à papillons
 
En plein soleil pour les fleurs.
Sinon pas d'exigence particulière.

## Mimosa

 * [Mimosa](http://site.plantes-web.fr/cavatore/1362/culture_en_pleine_terre.htm).

## Arbres fruitiers

 * [La pollinisation des arbres fruitiers](http://www.aujardin.info/fiches/pollinisation_fruitier.php).

 * [Bouturer un figuier](http://www.rustica.fr/articles-jardin/bouturer-figuier,1734.html#).
 * [La bouture du figuier](http://www.greffer.net/?p=388).

## Lagerstroemia

 * [Lagerstroemia](https://www.gammvert.fr/conseils/conseils-de-jardinage/lagerstroemia).
  + Lagerstroemia indica "Nana Petit Orchid". Questa varietà raggiunge un’altezza massima di circa 60 – 80 cm. Predilige una posizione soleggiata.
  + [Lagerstroemia indica "Rouge"](https://www.jardindupicvert.com/arbustes/22366-lilas-des-indes-rubra-.html#:~:text=Ancienne%20vari%C3%A9t%C3%A9%20de%20lilas%20des,rouge%20et%20superbe%20feuillage%20automnale.). Ensoleillé. Jusqu'à 5m de hauteur.

## Orchidées

 * [Comment favoriser l’apparition et le développement des racines sur un keiki](http://www.tropi-qualite.fr/?p=3310).

## Dipladenia

  * [Dipladénia : Conseils de plantation et entretien](https://www.meillandrichardier.com/dipladenia-conseils/).

## Lantana camara

  * [Lantanier, Mille fleurs, Corbeille d'or](https://www.aujardin.info/plantes/lantanier.php).

## Piscine

 * [L’analyse de l’eau de la piscine](https://www.guide-piscine.fr/analyse-eau-de-piscine/l-analyse-de-l-eau-de-la-piscine-861_A).

 * pH : 7.2-7.6
