##### Daily

 * [RECHERCHE IMMO](https://docs.google.com/document/d/1Vjvfmp199h4E_487e86S9NI7TZncYxhrgL97F7SJfS0/edit#heading=h.yjx040zg5six).
 * [Go Comics | My GoComis](https://www.gocomics.com/mycomics/2536715).
 * [Weather.com](https://weather.com/).
 * [Whatsapp](https://web.whatsapp.com/).
 * [Vacances](https://docs.google.com/document/d/1WnXe600B-ulzGAOhV8vj-zgbGbFutYU11ApeQsz3dSU/edit#).

##### Travail

 * [TalkSpirit](https://cea.talkspirit.com/).
 * [Mattermost](https://mattermost.ibfj-evry.fr/lbi/channels/town-square).
 * [Point LBI | Jitsi Meet](https://meet.ibfj-evry.fr/PointLBI).
 * [Local GitLab at CNRGH](https://gitlab.ibfj-evry.fr/).
 * [VPN SSL](https://vpnssl.cea.fr/).
 * [Slack](https://app.slack.com/).
 * [Framasoft](https://framasoft.org/).
 * [Moodle](https://moodle.com/).

##### BLR

 * [ENC Hauts-de-Seine (OZE)](https://enc.hauts-de-seine.fr/my.policy).
 * [Médiathèque François Villon](https://mediatheque.bourg-la-reine.fr/).
   + [Médiathèque numérique](https://vod.mediatheque-numerique.com/).
 * [Plex BLR](http://192.168.1.41:32400/web/).
 * [Plex CCN](http://192.168.1.3:32400/web/).
