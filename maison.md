# Maison

 * [Étiquetage pour l'entretien des textiles — Wikipédia](https://fr.wikipedia.org/wiki/%C3%89tiquetage_pour_l%27entretien_des_textiles).

 * [Freebox](http://mafreebox.freebox.fr/).
 * [Vodafone Power Station](http://vodafone.station/login.html).
 * [Configurazione Powerline Extender | TP-Link](http://tplinkplc.net/).

## Électroménager

 * Lave-vaisselle : entretien avec alcool ménager ou produit nettoyage, utiliser un cycle à 60°.

## Couture

 * [Comment repriser un trou](https://fr.wikihow.com/repriser-un-trou).
 * [Comment recoudre des trous](https://fr.wikihow.com/recoudre-des-trous).
 * [Couture à la main : les huit points à maîtriser](https://www.pourlesnuls.fr/articles/article/couture-la-main-les-huit-points-maitriser).

 * [Comment coudre une pièce sur un uniforme](https://fr.wikihow.com/coudre-une-pi%C3%A8ce-sur-un-uniforme).
