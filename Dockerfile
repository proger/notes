FROM alpine:3.17

# Install dependencies
RUN apk add make pandoc bash firefox ttf-dejavu ttf-liberation

# Set local variables
ARG user=john
ARG group=$user
ARG home=/home/$user
ARG notes_dir=$home/notes

# Create user and group
RUN addgroup -g 1000 $group
RUN adduser -h $home -s /bin/bash -G $group -D $user -u 1000

# Copy files
COPY --chown=$user:$group Makefile make_index $notes_dir/
COPY --chown=$user:$group bib/ $notes_dir/bib/
COPY --chown=$user:$group games/ $notes_dir/games/
COPY --chown=$user:$group cuisine/ $notes_dir/cuisine/
COPY --chown=$user:$group dev/ $notes_dir/dev/
COPY --chown=$user:$group *.md/ style.css $notes_dir/
COPY --chown=$user:$group science/ $notes_dir/science/

# Set user
USER $user
ENV HOME=$home
ENV PATH="$PATH:$HOME/.local/bin"

# Build HTML
WORKDIR $notes_dir
RUN make public

# Open main page in Firefox
CMD /usr/bin/firefox $HOME/notes/public/index.html
