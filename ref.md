# Références

 * [Wikipedia](https://www.wikipedia.org/).
 * [Wiktionary](https://en.wiktionary.org/wiki/Wiktionary:Main_Page).
 * [Wiktionnaire](https://fr.wiktionary.org/wiki/Wiktionnaire:Page_d%E2%80%99accueil).
 * [wikiHow : tutoriels fiables](https://fr.wikihow.com/Accueil).
 * [Linguee](https://www.linguee.fr/).
 * [DeepL Translator](https://www.deepl.com/translator).
 * [EU Translate text snippet](https://webgate.ec.europa.eu/cas/login?loginRequestId=ECAS_LR-13929233-jIbZH6jy1NkGm4hUcHq9URGRutfqkHnId9uCdB49IyPUBYCR8pzLB7JG793hGK8W3UN5u7SQ6urfQdox9Rbxf4-jpJZscgsw0KNBCX3BRxcxS-Q3Oy7GjOnl9mzVt0VgxEZ7gI7eRwugzziCXtkt05vDS8).
 * [La conjugaison de l'impératif](https://www.maxicours.com/se/cours/la-conjugaison-de-l-imperatif/).
 * [LEXILOGOS](https://www.lexilogos.com/).
 * [Le guide complet de la nouvelle orthographe](https://www.lalanguefrancaise.com/orthographe/guide-complet-nouvelle-orthographe/).

 * [Cartes du monde](https://en.wikipedia.org/wiki/World_map).
 * [Carte des provinces d'Italie](https://upload.wikimedia.org/wikipedia/commons/a/a0/Italian_regions_provinces.svg).

 * [xeno-canto :: Sharing bird sounds from around the world](https://www.xeno-canto.org/).
