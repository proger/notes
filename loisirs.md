# Loisirs

 * [Carte des châteaux de la loire](https://www.chateaux-de-la-loire.fr/carte.htm).

## Sport

 * [UCPA Odyssee](https://ucpa.kidizz.com/login).

 * [Zomb'In The Dark](https://zombinthedark.fr/). Course d'orientation avec zombis.

### Randonnées

 * [Randonnées des Parcs naturels régionaux d'Île-de-France](https://rando.pnr-idf.fr/).
 * [GR 1](https://www.ffrandonnee-idf.fr/randonner/itin%C3%A9raires/gr-1/).

 * [Marche de la Bièvre](https://marche-bievre.fr/). 52km marche de nuit.

## Loire

 * [Loire Kayak](https://www.loirekayak.com/).
 * [Canoë France (Loire)](https://www.rando-canoe-france.com/riviere/loire/).

## Périgord

Périgords blanc, vert, rouge et noir.

 * [Canoë Vézère](http://canoevezere.com/). Près de Périgueux et Brive-la-Gaillarde (Périgord blanc).

## Tarn

 * [Canoë 2000](https://www.canoe-kayak-gorgesdutarn.com/).
