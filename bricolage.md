# Bricolage

 * [Decathlon Watch: Battery Change of Kiprun Kalenji W200 Geonaute, in 5 easy
   steps.  ](https://www.youtube.com/watch?v=HlT7BOqKwos).

## Electronics

 * [Marshall Major III Headband and Earpad Replacement](https://www.youtube.com/watch?v=2FATgbfm3AQ).
 * [ELENCO Snap Circuits: Repair/Upgrade Components](https://www.youtube.com/watch?v=LIlQiLA5M-M).

## Plomberie

 * [Guide Plomberie : Les différentes dimensions de raccords en plomberie](https://www.plomberie-online.fr/blog/guide-plomberie-les-differentes-dimensions-de-raccords-en-plomberie-n6).

## Chaudières

 * [Caldaie da esterno](http://www.italtherm.it/index.php?option=com_djcatalog2&view=items&cid=5&lang=it&search=city%20open%20HE&limitstart=0&order=i.ordering&dir=asc#tlb).
 * [PREZZI VENDITA CALDAIE ITALTHERM](http://www.vendita-caldaie-climatizzatori.com/contents/it/d188_caldaie_a_gas_italtherm.html).

## Perçage

 * [Comment choisir ses forêts ?](https://conseil.manomano.fr/comment-choisir-ses-forets-415).
 * [Percer dans du carrelage](https://www.conseils-outillage.fr/comment-percer-carrelage/).
 * [Percer du carrelage](https://www.pratique.fr/percer-carrelage.html).
 
 * [Bien choisir une cheville](https://www.leroymerlin.fr/v3/p/tous-les-contenus/bien-choisir-une-cheville-l1308216427).

## Joints de carrelage

 * [Comment réussir les joints de carrelage ?](http://www.leroymerlin.fr/v3/p/lm-videos/comment-reussir-les-joints-de-carrelage-vOM3XO3SROVKTKMKBNM099189).

## Réseau

 * [GUIDA AL COLLEGAMENTO DI UNA PRESA RETE RJ45 KEYSTONE](https://www.youtube.com/watch?v=8JqHp6t6ZJE).
 * [Guida al montaggio di un cavo di rete UTP RJ45 CAT5, CAT6, Diretto o Incrociato (Crossover) secondo le specifiche EIA568A e B](http://www.ashmultimedia.com/utilita-configurazione-cavi-di-rete.asp).
 * [RJ-45](https://it.wikipedia.org/wiki/RJ-45).
 * [Branchements câble RJ45 en direct et en croisé](http://www.bb-elec.com/images/EthernetRJ45A.gif).

## Électricité

 * [bticino professionisti](http://professionisti.bticino.it).

Normes italiennes :

 * [Pubblicata in Giugno la VII edizione della norma CEI 64-8: nuova sezione 37 dedicata ai nuovi requisiti per gli ambienti residenziali](http://www.schneider-electric.it/sites/italy/it/supporto/normative/Norma_CEI_64-8.page).
 * [Come mettere a norma un impianto elettrico](http://faidatemania.pianetadonna.it/come-mettere-a-norma-un-impianto-elettrico-183163.html#steps_1).

Tirer des fils:

 * [Les trois outils indispensables pour vous aider à tirer les fils électriques](http://www.installation-renovation-electrique.com/trois-outils-indispensables-pour-vous-aider-a-tirer-les-fils-electriques/).
 * Utiliser un tire-fil (sondino da elettricista o sonda elettrica) et/ou du lubrifiant (marque CAPRIGEL par exemple).
 * [Come Far Passare i Cavi Elettrici nelle Canaline](http://it.wikihow.com/Far-Passare-i-Cavi-Elettrici-nelle-Canaline).
