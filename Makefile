BIB_FILES=$(shell find bib -type f -iname '*.bib')
TEX_START='\documentclass{article}\usepackage[utf8]{inputenc}\usepackage[T1]{fontenc}\usepackage[style=authoryear,backend=bibtex]{biblatex}\bibliography{'
TEX_END='}\begin{document}\nocite{*}\printbibliography\end{document}'
MD_INDEXES=$(addsuffix /index.md,$(shell find . -type d | grep -v .git | grep -v '^\./public'))
MD_FILES=$(shell find . -name '*.md' | grep -v README | grep -v main.md) $(MD_INDEXES)
HTML_FILES=$(MD_FILES:.md=.html)

all: references.bib citecmds.tex public labels

labels: dev/versioning/labels.svg

dev/versioning/labels.svg: dev/versioning/labels.txt gen_labels_svg
	./gen_labels_svg <"$<" >$@

public: $(addprefix public/,$(HTML_FILES))

citecmds.tex: texcitecmds.tsv groupcitations.tex
	maketexcitecmds <$< >$@
	cat groupcitations.tex >>$@

index.md: . make_index
	./make_index "$<" public >"$@"

%/index.md: % make_index
	./make_index "$<" public >"$@"

public/%.html: %.md
	mkdir -p "$(dir $@)"
	pandoc -f markdown -t html --shift-heading-level-by=-1 "$<" -o "$@" --embed-resources --standalone -c style.css

test: all_refs.pdf

test.single.bibs:
	for f in $(BIB_FILES) ; do \
		$(RM) single_bib* ;\
		echo -en "\n\n********************************\n" ;\
		echo -en "$$f\n********************************\n\n\n" ;\
		echo $(TEX_START)$$f$(TEX_END)>single_bib.tex ; \
		( pdflatex single_bib.tex && bibtex single_bib && \
		pdflatex single_bib.tex ) || exit 1 ;\
	done

test.deploy: docker.build docker.run

docker.build:
	docker build -t notes .

docker.run:
	docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$(DISPLAY) -h $(shell hostname) -v $(HOME)/.Xauthority:/home/john/.Xauthority notes

all_refs.pdf: all_refs.tex references.bib
	pdflatex $<
	bibtex all_refs
	pdflatex $<

references.bib: $(BIB_FILES)
	cat $^ >$@

all_refs.tex:
	echo $(TEX_START)references$(TEX_END) >$@

clean:
	$(RM) -r $(MD_INDEXES) public
	$(RM) citecmds.tex
	$(RM) all_refs.tex references.bib *.aux *.bbl *.blg *-blx.bib *.log *.pdf \
		*.run.xml single_bib.tex

.PHONY: all clean labels
