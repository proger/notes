# Langues

 * [Reverso](http://dictionary.reverso.net/).
 * [Linguee](http://www.linguee.fr/).
 * [Word reference](http://www.wordreference.com/).

## 中文 / Chinois

 * [Apprenons le chinois](http://french.cri.cn/audioonline/lecondechinois/index.html).

## Deutsch / Allemand

 * [Apprendre l'allemand](https://www.dw.com/fr/apprendre-lallemand/s-2616). Deutsch Welle.
  + [Deutsch Welle Learn German](https://learngerman.dw.com/fr/overview). Nicos Weg.
  + [Harry gefangen in der Zeit](https://www.youtube.com/playlist?list=PLs7zUO7VPyJ6Ufol7Z4FfkCviaETvl3c8).
  + [Deutschtrainer audio](https://rss.dw.com/xml/DKpodcast_deutschtrainer_audios_fr)
  + [Deutschtrainer video](https://rss.dw.com/xml/DKpodcast_deutschtrainer_videos_fr)

 * [Deutsch – warum nicht?](http://www.dw.com/fr/apprendre-lallemand/deutsch-warum-nicht/s-2618?maca=fra-DKpodcast_dwn1_fr-2868-xml-mrss). MP3 et PDF, 4 niveaux, gratuits. Deutsche Welle et Goethe-Institut.
 * [CNED Formation en allemand](http://www.cned.fr/inscription/8ALLRDIX?gclid=COXXrfXHys8CFcaVGwodM2wGkw).
 * [MOSAlingua Les meilleurs podcasts pour apprendre l’allemand](http://www.mosalingua.com/blog/2014/09/17/les-meilleurs-podcasts-apprendre-lallemand/).
 * [Comment améliorer son allemand avec les podcasts ?](http://www.superprof.fr/blog/podcasts-pour-progresser-en-allemand/).
 * [Toutes les ressources en ligne pour apprendre l'allemand](http://www.sprachcaffe.com/francais/sprachcaffe-magazine-article/toutes-les-ressources-en-ligne-pour-apprendre-lallemand-2015-08-24.htm).
 * [Leçons d'allemand](http://allemandcours.fr/lecons-dallemand).

 * [Dramatized depiction](http://www.dramatized.de). Courtes bandes dessinées en ligne, en allemand.

## English / Anglais

 * Former & latter:
  - "There are two kinds of worries: those you can do something about and those you can't. Don't spend any time on the latter." (Duke Ellington)
  - Former = being the first of two mentioned.
  - Latter = being the second of two mentioned.

<http://acronyms.thefreedictionary.com/>

 * AD = Anno Domini (Anno Domini Nostri Iesu (Jesu) Christi) or Year of our Lord referring to the year of Christ’s birth.
 * BC = Before Christ.
 * CE = Common Era, same as AD.
 * BCE = Before Common Era, same as BC.

 * FYI  = For Your Information
 * IIRC = If I Recall/Remember/Read Correctly
 * IMHO = In My Humble Opinion
 * KPI = Key Performance Indicator

## Français

 * [Le conjugueur](http://www.leconjugueur.com/).
 * [TLFi](http://atilf.atilf.fr/).

 * [Six et dix](http://bdl.oqlf.gouv.qc.ca/bdl/gabarit_bdl.asp?id=3137).

## Italiano / Italien

 * [Centre Culturel Italien](http://www.centreculturelitalien.com/).
 * [Polimnia](http://www.polimnia.eu/).
 * [IIC](http://www.iicparigi.esteri.it/).
 * [Centro Italiano](http://www.centroitaliano.it/), cours d'italien à Naples.

 * [Io parlo italiano](http://www.educational.rai.it/ioparloitaliano/corso.htm).
 * [Livres italiens](http://www.livres-italiens.fr/).
 * [Coniugazione](http://www.coniugazione.it/).
 * [Verbi italiani](http://www.italian-verbs.com/verbi-italiani.htm).

## 日语 / Japonais

 * [Le Japonais en douceur](https://www3.nhk.or.jp/nhkworld/fr/learnjapanese/).
 * [Leçons de japonais en podcasts](https://www.nhk.or.jp/lesson/common/rss/podcast/french.xml).
