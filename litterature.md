# Littérature

 * [Kindle](https://lire.amazon.fr/kindle-library).

 * [Wikisource](https://fr.wikisource.org/).
  + [Strange stories from a Chinese studio, Herbert A. Giles, 1916](https://archive.org/details/strangestoriesf01gilegoog/page/n6/mode/2up).
 * [Bookshare](https://www.bookshare.org/cms/search-freely-available). Some free to download, others need to sign up.
 * [Standard eBooks](https://standardebooks.org/).
 * [La Bibliothèque électronique du Québec](http://beq.ebooksgratuits.com/).
 * [Nos livres : catalogue de livres électroniques du domaine public francophone](http://www.noslivres.net/). Site de recherche redirigeant vers Gallica, BNR, ...
 * [Bibebook](http://www.bibebook.com/). Français.
 * [Manybooks](https://manybooks.net/).
 * [epubBooks](https://www.epubbooks.com/).
 * https://lirenligne.net/

 * [The Public Domain Review](https://publicdomainreview.org/).
 * [Internet Archive](https://archive.org/).

 * [Où télécharger des livres libres de droits ?](https://mediathequemargueriteduras.wordpress.com/2016/08/26/ou-telecharger-des-livres-libres-de-droits/).
 * [Bibliothèque Numérique de la ville de Paris](https://bibliotheques.paris.fr/numerique/).
 * [Feedbooks](https://www.feedbooks.com/). Livres sans DRM à acheter en anglais, italien et français.
 * [Gallica](https://gallica.bnf.fr/).
 * [In Libro Veritas](http://www.inlibroveritas.net/). Français. Inscription nécessaire. Vie pratique, informatique, romans, ...
 * [Numilog](https://www.numilog.com/). Inscription nécessaire, certains livres avec DRM.
 * [Gutenberg Project](https://www.gutenberg.org/). DEAD.

 * Daniel Kahneman : "Thinking fast and slow", "Noise".
 * Abdulrazak Gurnah : Paradise
 * Mohamed Mbougar Sarr : "La plus secrète mémoire des hommes"

 * [Kathasaritsagara (the Ocean of Story)](http://www.wisdomlib.org/hinduism/book/kathasaritsagara-the-ocean-of-story). Online, PDF versions and much more are available at this Wisdom Library website. Inside the Kathasaritsagara there should be the first written version of the story of the Puss in Boots (Le Chat Botté).

 * Qiu Xiaolong (Chinese: 裘小龙), English-language writer. Author of the Detective Chen series.

 * [Le Roi Singe, outils éducatifs](http://digital.philharmoniedeparis.fr/0981474-le-roi-singe.aspx).

Théâtre chinois:
 * [LE THÉÂTRE CHINOIS](http://www.chineancienne.fr/début-20e-s/jacovleff-tchou-kia-kien-le-théâtre-chinois/), Alexandre Jacovleff (1887-1938) et Tchou Kia.
 * [L'OPÉRA CHINOIS](http://digital.philharmoniedeparis.fr/contexte-l-opera-chinois.aspx), Philarmonie de Paris.
 * [LE THÉÂTRE CLASSIQUE CHINOIS](https://halshs.archives-ouvertes.fr/hal-00681795/file/Theatre_chinois_-_Revue_anciens_etudiants_INALCO.pdf), INALCO.
 * [Dans les coulisses de l'Opéra chinois](http://www.confucius-angers.eu/wp-content/uploads/2014/08/Cahier-pédagogique-Dans-les-coulisses-de-l-opéra-chinois.pdf).

 * [Métrique en ligne](http://www.crisco.unicaen.fr/verlaine/explications.html). Explique comment compter la métrique d'un vers. Ne propose pas de télécharger les programmes développés.

Liste par France Culture :
 * Il nome della rosa, Umberto Eco
 * "La Storia" d'Elsa Morante
 * "L’Œuvre au noir" de Marguerite Yourcenar
 * "Bonjour tristesse" de Françoise Sagan
 * "Un barrage contre le Pacifique" de Marguerite Duras
 * "Voyage au bout de la nuit" de Louis-Ferdinand Céline
 * "Sous le soleil de Satan" de Georges Bernanos
 * "Mrs Dalloway" de Virginia Woolf
 * The Left Hand of Darkness d'Ursula K. Le Guin
 * "The Alexandria Quartet" de Lawrence Durrell
 * "Vie et destin" de Vassili Grossman

## Littérature allemande

 * "À l'Ouest, rien de nouveau", Erich Maria Remarque.
 * Ernst Juenger, "Auf Den Marmor Klippen".

## Contes

 * [Il était une histoire](http://www.iletaitunehistoire.com).

 * Strange Stories from a Chinese Studio (Liaozhai zhiyi, Contes extraordinaires du pavillon du loisir), Pu Songling

## Policiers

 * [Liste des romans et nouvelles adaptatées dans la série Maigret avec Bruno Crémer](https://seriecremer.enquetes-de-maigret.com/adaptations/).
 * [Épisodes de Maigret (Bruno Cremer) on Odysee](https://odysee.com/@Maigret:0?view=content).

## Sci-Fi

 * The Forever War, Joe Haldeman.
 * Philip K. Dick 
 * Brave New World
 * Fahrenheit 451
 * They Live
 * 1984

