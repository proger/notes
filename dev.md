# Development

 * [Mutt cheat sheet](http://sheet.shiar.nl/mutt).
 * [Vim Cheat Sheet](http://sheet.shiar.nl/mutt).
 * [List of Unicode characters](https://en.wikipedia.org/wiki/List_of_Unicode_characters).

 * [GitHub](https://github.com/).
  - [GitHub Issues](https://github.com/issues).
  - [GitHub Pull requests](https://github.com/pulls).
 * [GitLab](https://about.gitlab.com/).
 * [Bitbucket](https://bitbucket.org/).
 * [Travis CI](https://travis-ci.org/).

## C++

 * [cppreference.com](https://en.cppreference.com/w/).

## Free resources

Free images:
 * <https://www.stockio.com/> +++
 * <http://www.pdclipart.org/>
 * <https://openclipart.org/>
 * <https://publicdomainvectors.org/>
 * [Country flags](https://github.com/hampusborgos/country-flags).

## W4M

 * [GitHub W4M](https://github.com/workflow4metabolomics/).
 * [Galaxy (local)](http://localhost:8080/).
