# Metabolomics

 * [MetaboNews](http://www.mailman.srv.ualberta.ca/mailman/listinfo/metabonews).

## Journals

 * [Theoretical Concepts and Computational Methods in Metabolomics](http://journal.frontiersin.org/researchtopic/3807/theoretical-concepts-and-computational-methods-in-metabolomics).

## Identification

### CFM-ID

 * [CFM-ID](http://cfmid.wishartlab.com/), Competitive Fragmentation Modeling for Metabolite Identification.
 * [PhD Felicity Allen](http://papersdb.cs.ualberta.ca/~papersdb/uploaded_files/1136/paper_Allen_Felicity_R_201601_PhD.pdf).
 * [Competitive fragmentation modeling of ESI-MS/MS spectra for putative metabolite identification](http://link.springer.com/article/10.1007/s11306-014-0676-4).
 * Discussion with Felicty Allen about improving her code to compile for macOS: `emails/2016-06-21-MTH_Felicity_Allen.eml`.

### MS2Analyzer

 * [MS2Analyzer](http://fiehnlab.ucdavis.edu/projects/MS2Analyzer).
 * [MS2Analyzer manual](http://fiehnlab.ucdavis.edu/projects/MS2Analyzer/MS2Analyzer_User_Manual1.pdf).
 * [MS2Analyzer-ver2-1.jar](http://fiehnlab.ucdavis.edu/projects/MS2Analyzer/MS2Analyzer-ver2-1.jar/at_download/file).
