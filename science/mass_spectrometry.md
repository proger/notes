# Mass Spectrometry

## Databases

### Massbank

 * [Massbank Japan](http://www.massbank.jp/).
 * [Massbank Europe](http://massbank.eu/).

#### API 

 * [API reference](http://www.massbank.jp/manuals/api-doc_en/reference.html).
 * [API URL, Japan](http://www.massbank.jp/api/services/MassBankAPI/).
 * [API URL, Europe](http://massbank.eu/api/services/MassBankAPI/).

Get records:
```bash
wget -O records 'http://massbank.eu/api/services/MassBankAPI/getRecordInfo?ids=EA256108,KOX00001'
```

Get compounds containg certain peaks:
```bash
wget -O compounds.xml 'http://massbank.eu/api/services/MassBankAPI/searchPeak?mzs=80,85&relativeIntensity=200&tolerance=0.3&instrumentTypes=all&ionMode=Both&maxNumResults=0'
```

CASMI
=====

 * [CASMI](http://www.casmi-contest.org/).

METFRAG
=======

Fragmentation simulation.

 * [MetFrag official site](http://c-ruttkies.github.io/MetFrag/).
 * [MetFragR](https://github.com/c-ruttkies/MetFragR). 

CONVERSION
==========

 * [Proteowizard](http://proteowizard.sourceforge.net/index.shtml).
 * [ThermoFisher](https://thermo.flexnetoperations.com/control/thmo/login) MSFileReader. pierrick.roger@cea.fr / voy lc new comm alphanum. Download `MSFileReader_x86_x64.exe` inside the *Utility* section. Then Run it, and it will install a *Thermo* folder inside Windows programs. In it you will find the following files: `MSFileReader_Reference.pdf`, `XRawfile2.dll`, `fileio.dll` and `fregistry.dll`.
 * [Thermo Raw File Reader](http://omics.pnl.gov/software/thermo-raw-file-reader), Pacific Northwest National Laboratory.

GLOSSARY
========

 * M/Z = mass-to-charge ratio. See [IUPAC](http://goldbook.iupac.org/M03752.html).
 * CID = collision-induced dissociation.
 * ESI = ElectroSpray Ionisation
 * PA = Proton Affinities
 * Mass of an electron = 0.000548 u
 * high resolution = help to separate adjacent peaks
 * accurate mass = allows to get molecular formulas

Precursor (<http://mass-spec.lsu.edu/msterms/index.php/Precursor_ion>):
	AKA: progenitor ion
	Deprecated: parent ion.
	Ion that reacts to form particular product ions or undergoes specified neutral losses. The reaction can be of different types including unimolecular dissociation, ion/molecule reaction, change in charge state, possibly preceded by isomerization.

Solvant deutéré:
	Il est possible d'utiliser un solvant deutéré (eau lourde D2O ou méthanol deutéré CD3OD) en MS, et remplacer ainsi les protons mobiles (H mobiles) des molécules étudiées par des atomes de deutérium.

IONS DE BASSE MASSE (Jean-Claude Tabeet 06/06/2014)
===================================================

Certains ions de basse masse sont très utiles à connaître.

En négatif:
	97  acide phosphorique déprotonné
	80  SO3-
	79  acide phosphorique PO3H
	79  CH3PO2H-
	59  acide acétyle
	59  CH3CO2-
	45  HCO2-

En positif:
	44 |
	58 |--> ions de type CH2=NH2+ (masse 30)
	72 |

									CH2\
	44      Aziridinium             |   \
									|    NH2+
									|   /
				   					CH2/

				CH3
	        |\ /
	58      | N
	        |/ \
		        H

				CHH3
	        |\ /
	72      | N
	        |/ \
		        CH3

	En générale il y a la suite 30,44,58,72. Par exemple à partir de la molécule:

	                       |
					/\/\/\/N+--
					       |

BOOK MASS SPECTROMETRY (GROSS)
==============================

## 2.1
A mass spectrometer handles only charged species created from: atoms, molecules, radicals, zwitterions or clusters.

## 2.1.1
EI produces ions from molecules by colliding with electrons at high energy (70eV).
It produces mainly radical positive ions:
M + e --> M+˙ + 2e
Rarely, it produces negative ions.

Production of negative ions are mainly possible using low energy ionization methods (like thermal ionization), because of the energy contribution that needs to be evacuated (if the molecule keeps the electron it needs also to accept the energy or find a way to evacuate it).

## 2.4.3 Bond Dissociation Energies and Heats of Formation
Can be useful to elucidate fragmenta- tion mechanisms.

## 2.5 Quasi-Equilibrium Theory (QET)
Molecules are isolated and thus may only internally redistribute energy => unimolecular reactions such as isomerization or dissociation.
This is due to the fact that all happens under high vacuum (i.e. highly diluted gas phase).
We consider that the energy is randomly distributed over the ion/molecule, so that dissociation is guided by probabilities (i.e. the best case).

## 2.7 Internal Energy – Practical Implications
This chapter contains a basic theoretical example of a fragmentation tree.

## 2.11.2 Breakdown Graphs
If known, breakdown graphs can be used to determine the fragmentation of a molecule depending on the internal energy.

## 3.1.6 Natural Variations in Relative Atomic Mass
Varation of 13C/12C isotopic ratio in nature.

## 3.2 Calculation of Isotopic Distributions
This chapter explains how to compute probabilities of isotopes.
Use of binomial development for diisotopic elements.
Use of polynomial approach for polyisotopic elements.

BOOK SPECTROMETRY EI (MCLAFFERTY)
=================================

TO BE CHECKED: In EI MS the main molecule loses only an electron, so it conserves its weight.

Accuracy of isotope peaks ????
10% of its intensity ? Not lower than +-0.2 ?

## Analysis steps p14
a)  Molecular weight
	element composition
	rings-plus-double-bonds
b)  substrctures
c)  connectivities of substructures
d)  postualed possible molecules, comparing each structure against all available data.

## Isotopes p20
The "A+2" isotopic elements are the easiest to recognize.
Intensity of (A+4) peak for a molecule containing 4 chlorine atoms should be C(4,2) * .32 * .32 = 0.614
See p29 for a procedure for isotope interpretation.

## Rings & double bonds p27
CxHyNzOn => (x - 1/2y + 1/2z + 1) = nb of double bonds and rings
C can be replaced by Si
H ------------------ F, Cl, Br, I (, Li, Na, K ?)
N ------------------ P
O ------------------ S

ex: C5H5N --> 4 (pyridine --> odd-electron)
	C7H5O --> 5.5 (C6H5CO+ benzoyl --> even-electron)

## Main molecule peak p293
If any peak in a spectrum corresponds to the mass of the molecule, this peak must be the one of highest mass.

EXERCICES MCLAFFERTY BOOK
=========================

## 2.1
	35  12  --> Cl-
	36  100 --> ClH
	37  4.1 --> Cl- isotope
	38  33  --> ClH isotope

## 2.2
	94  100 --> BrCH3
	95  1.1 --> BrCH3 carbon isotope
	96  96  --> BrCH3 brome isotope
	97  1.1 --> BrCH3 bromine and carbon isotopes

## 2.3
	64  100 --> SO2
	65  0.9 --> SO2 with either sulfur A+1 isotope or oxygen A+1 isotope (0.79 + 2 * 0.04 = 0.87)
	66  5.0 --> SO2 with either sulfur A+2 isotope or oxygen A+2 isotope (4.4 + 2 * 0.2 = 4.8)

## 2.4
	43  100 --> 3 carbons (a loss of CH3 from m/z 59) C3H7+
	44  3.3 --> 3 carbon isotopes from m/z 43
	58  12  --> 4 carbons C4H10 (butane)
	59  0.5 --> 4 carbon isotopes from m/z 58 (0.5/12 = 4.2% =~ 4.4%)

## 2.5
	78  100 --> 6 carbons   => C6H6
	79  6.8 --> 5 carbons 12 + 1 carbon 13
	80  0.2 --> 4 carbons 12 + 2 carbon 13

## 2.6
	27      --> C2H3 ; 72 - 27 = 45 ==> most certainly a lost of COOH (carboxyl group) ==> acryclic acid CHCOOH
	55  74  --> C3H3 = 36+3=39, reste 16 (oxygen ?) => C3H3O ; 72 - 55 --> perte de 17, NH3 ou OH ? OH because of peak 74.
	56  2.6 --> 2.6/74*100 = 3.5% ==> 3 carbones max.
	72  100 --> C3H3O + (NH3 ou OH) ==> C3H4O2 due to peak 74. 
	73  3.5 --> 2 carbons 12 + 1 carbon 13
	74  0.5 --> 1 carbon 12 + 2 carbons 13 (0.04) + 1 oxygen 18 among 2 (2 * 0.2)

## 2.7
	14  5.2 --> N
	19  8.4 --> F
	33  36  --> NF
	34  0.1 --> NF with an N 15 isotope.
	52  100 --> 52 - 14(N) = 38 (2*F) ==> NF2 ; 71-52 = 19 loss of an F.
	53  0.4 --> suggest presence of N 15 isotope (0.37).
	71  30  --> NF3

## 2.8
	12  0.4 --> carbon
	19  0.2 --> fluorine
	31  1.8 --> phosphorus
	50  6.3 --> 104-50=54=35+19 loss of a Cl and an F ? 50 - 12 =38 = 2 * 19 (2 F) ?
	69  100 --> 104-69=35 loss of a Cl ?
	70  1.2 --> one carbon 13 ?
	85  18  --> 104-85=19 loss of an F ?
	86  0.2 --> .2 /18=1.1% --> one carbone 13 ? 
	104 0.7 --> CF3Cl
	106 0.2 --> .2/.7=28% --> A Cl 37 isotope ?

## 2.9
	m/z Int.        C8 (96) C9 (108)    O(16)
	130 1.0
	131 2.0
	132 100         100     100         100
	133 9.9 +- 1    8.8     9.9         0.04
	134 0.7 +- .2   0.34    0.44        0.20
ratio 134/132 is too low for Si,S,Cl and Br.
C8 and C10 impossible because A+1 ratio (8.8 and 11.0) are outside error limits 9.9+-1 --> 8.9-10.9.
==> C9
ratio 134/132 is only partially answered by carbons ==> oxygen is needed : 0.44+0.2=0.64 in the limits 0.5-0.9.
==> C9OH8
nb rings and double bonds = 9 - 8 / 2 + 1 = 6

## 2.10
	m/z Int.        Norm.       C3(36)  C4(48)  O(16)
	76  80          100         100     100     100
	77  2.7 +-.27   3.4 +-.34    3.3     4.4     0.04
	78  <0.1        <0.125      0.04    0.07    0.20

ratio 78/76 means no oxygen (too low) ==> no A+2 isotopes other than carbons.

==> C3
	C3 + F2 = 36 + 2*19 = 74 --> C3H2F2 impossible since cannot give fragment m/z73
	C3 + P = 36 + 31 = 67   --> C3H9P

	[[[ WRONG
	==> C4
	only hydrogens is impossible because of higher mass ==> C4 + P and/or F
	C4 + F = 48+19 = 67
	C4 + P = 48+31 = 79 ==> excluded, mass too much high
	==> C4H9F
	nb rings and double bonds = 4 - 10 /2 + 1 = 0 ]]]

## 2.11
	m/z         Int.        Norm.       Si(28)  C5(60)  C4(48)
	84          0.0
	85  (M-19)+ 40          100         100     100     100
	86          2.0 +-0.2   5+-0.5      5.1     5.5     4.4
	87          1.3 +-0.2   3.25+-0.5   3.4     0.12    0.07
	88          0.0 +-0.2   0.0+-0.5

	Si + 3F = 28 + 3*19 = 85
	Si3F

	absence of m/z84 ==> no hydrogens

# 2.12
	m/z     Int.        Norm.       O(16)   O2(32)  O3(48) O4(64)   C13(156)    C14(168)    C15(180)
	179     2.2
	180     1.1
	181     7.4
	182     55          100         100     100     100     100     100         100         100
	183     8.3+-0.8    15.1+-1.5   0.04    0.08    0.12    0.16    14.3        15.4        16.5
	184     0.6+-0.2    1.1+-0.4    0.20    0.4     0.6     0.8     0.94        1.1         1.3

several possibilities: C12H10N2, C13H26, C13H7F, ...

## 2.13
	m/z     Int.        Norm.       C6(72)  Br(79)  O(16)
	171     0.1
	172     98          100         100     100     
	173     6.7+-0.7    6.8+-0.7    6.6
	174     100+-10     102+-10     0.18    97.3    102
	175     6.5+-0.7    6.6+-0.7                    0.04
	176     0.5+-0.2    0.5+-0.2                    0.20

m/z174 => Br81
151+16=167 ==> 5 H
C6H5BrO
nb rings and double bonds = 6 - (5 + 1) / 2 + 1 = 4

## 2.14
	m/z     Int.    
	128     0.0
	129     30
	130     100
	131     31+-0.3
	132     98+-9.8
	133     12+-1.2
	134     32+-3.2
	135     1.7+-0.2
	136     3.4+-0.3
	137     0.0+-0.2

m/z132 / m/z130 --> Br isotope. But impossible because of m/z134 which has intensity of 32, so m/z132 should be higher.

series 129/131/133/135 is similar to 130/132/134/136, and fits with Cl3 series.
A       A   A+2 A+4 A+6
Cl3     100 96  31  3

=> C2HCl3
nb rings and double bonds = 2 - 4 / 2 + 1 = 1
