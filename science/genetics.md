# Genetics

 * [Indel](https://en.wikipedia.org/wiki/Indel).
 * [SNP](https://en.wikipedia.org/wiki/Single-nucleotide_polymorphism).

 * [Reference Genomes and Genomics File Formats](https://rockefelleruniversity.github.io/Genomic_Data/presentations/singlepage/GenomicsData.html).

 * [Genome assembly GRCh38](https://www.ncbi.nlm.nih.gov/datasets/genome/GCF_000001405.26/).

 * Assemblage: parcours de graphe.

 * Variant calling: scoring.

## Sequencing

 * [Advantages of paired-end and single-read sequencing](https://www.illumina.com/science/technology/next-generation-sequencing/plan-experiments/paired-end-vs-single-read.html).

## Alignment

 * [Sequence alignment](https://en.wikipedia.org/wiki/Sequence_alignment).
 * [Global alignment vs. Local alignment vs. Semi-global alignment](https://bio.libretexts.org/Bookshelves/Computational_Biology/Book%3A_Computational_Biology_-_Genomes_Networks_and_Evolution_%28Kellis_et_al.%29/03%3A_Rapid_Sequence_Alignment_and_Database_Search/3.03%3A_Global_alignment_vs._Local_alignment_vs._Semi-global_alignment).

 * [Needleman–Wunsch algorithm](https://en.wikipedia.org/wiki/Needleman%E2%80%93Wunsch_algorithm). Global alignment. Ne simule pas l'insertion de base (usuellement 3 bases dans les régions codantes).
   + [Solving the Sequence Alignment problem in Python](https://johnlekberg.com/blog/2020-10-25-seq-align.html).

 * Alignement swiss & waterman : séquentiel. table de pondération.

 * Alignement K-mer: dictionnaire, découpe en mots de taille finie.
 * [K-mer sequencing vs sequence-alignment](https://www.biostars.org/p/485136/).
 * [k-mer](https://en.wikipedia.org/wiki/K-mer).
 * [The k-mer alignment (KMA) specification](https://gensoft.pasteur.fr/docs/kma/1.3.4/KMAspecification.pdf).

## Random generation

 * [Mutation-Simulator](https://github.com/mkpython3/Mutation-Simulator). Takes a FASTA file and simulate mutations (i.e.: generate VCF files).
