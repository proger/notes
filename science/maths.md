# Mathematics

 * [Circles rolling on circles](https://plus.maths.org/content/circles-rolling-circles)

## Fractal

https://realpython.com/mandelbrot-set-python/

## Stats

p-value:
 * [P-Value: What It Is, How to Calculate It, and Why It Matters](https://www.investopedia.com/terms/p/p-value.asp).
   + [Null Hypothesis: What Is It and How Is It Used in Investing?](https://www.investopedia.com/terms/n/null_hypothesis.asp).
 * [p-value](https://en.wikipedia.org/wiki/P-value).
