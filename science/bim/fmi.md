FMI
===

 * [The Functional Mockup Interface for Tool independent Exchange of Simulation Models](https://svn.modelica.org/fmi/branches/public/docs/Modelica2011/The_Functional_Mockup_Interface_paper.pdf).

	As you said the wikipedia entry is always a good introduction:

	http://en.wikipedia.org/wiki/Functional_Mock-up_Interface

	And I attach some tutorial slides.
	I also checked and energyplus seems to export to FMU without problems:
	http://simulationresearch.lbl.gov/fmu/EnergyPlus/export/userGuide/bestPractice.html


	The FMU specification from modellica :
	https://svn.fmi-standard.org/fmi/branches/public/specifications/FMI_for_ModelExchange_and_CoSimulation_v2.0_Beta4.pdf


	There are many libraries to execute FMU’s (in c,  JModellica has a python library http://www.jmodelica.org/assimulo_home/pyfmi_1.0/tutorial.html#).
	We recently developed an adapter to be able to load FMU’s in Grenad. To do that we built on a java wrapper for FMI developed by Berkeley: http://ptolemy.eecs.berkeley.edu/java/jfmi/.

