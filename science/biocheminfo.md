# Biocheminfo

 * [bioshadock](https://docker-ui.genouest.org/app/#/). Containers repository.
 * [eliXir](https://www.elixir-europe.org). Bioinformatics in Europe.

## Sirius

[Sirius](https://bio.informatik.uni-jena.de/software/sirius/). Isotope Pattern Analysis and Fragmentation Tree Computation.

## Mass spectrometry
### Thermo raw files

 * [unthermo, or simply reading out .raw mass spectrometer data](https://pkelchte.wordpress.com/2013/11/25/unthermo/).
 * [unfinnigan](https://code.google.com/archive/p/unfinnigan/).

### Metfrag

 * [MetFrag portal](http://msbi.ipb-halle.de/MetFrag/).
 * [MetFrag source code, c-ruttkies version](https://github.com/c-ruttkies/MetFrag).
 * [MetFrag source code, s-wolf version](https://github.com/s-wolf/MetFrag).

MetFrag is implemented in Java, and uses the CDK.

MetFrag is available through a web application, web services and as java library.

Output is in [SDF](https://en.wikipedia.org/wiki/Chemical_table_file) format.

 * [MetFrag](http://c-ruttkies.github.io/MetFrag/).
 * [MetWare project](http://metware.org/).
 * [MetFrag sources](https://github.com/sneumann/MetFrag) of Steffen Neumann forked from Christoph Ruttkies' sources. Has not been maintained since 2013.
 * [MetFrag sources](https://github.com/c-ruttkies/MetFrag) of Christoph Ruttkies forked from Sebastian Wolf's sources. Has not been updated since December 2014.
 * [MetFrag sources](https://github.com/s-wolf/MetFrag) of Sebastian Wolf. Has not been updated since 2013.
 * Christoph Ruttkies also made a [command line tool](https://github.com/c-ruttkies) for MetFrag.

Articles:

Reference      | Description
-------------- | -------------------------------
wolf-2010      | A presentation of MetFrag, and a comparison with MassFrontier.
scheubert-2013 |
ruttkies-2013  | An application of MetFrag and MetFusion for CASMI 2012.

### MassFrontier

Commercial software.

Articles: zhou-jia-rui-2014.

### MetFusion

Reference      | Description
-------------- | -------------------------------
ruttkies-2013  | An application of MetFrag and MetFusion for CASMI 2012.
gerlich-2013   | 

## File formats

SDF files can be read in R using the package `rcdk`, see [Chemistry in R](http://baoilleach.blogspot.fr/2008/07/chemistry-in-r.html).

## InChI

 * [IUPAC InChI page](http://www.iupac.org/home/publications/e-resources/inchi.html).
 * [InChITRUST](http://www.inchi-trust.org). Provides InChI software (application and library, free of charge), and InChI software certification (cost $5000).

## RDKit

The [RDKit](https://github.com/rdkit/rdkit) is a collection of cheminformatics and machine-learning software written in C++ and Python.

For installation on OSX, use this [Homebrew formula](https://github.com/rdkit/homebrew-rdkit):
```bash
brew tap rdkit/rdkit
brew install --with-inchi rdkit
export RDBASE=/usr/local/share/RDKit
```

## Data formats

### ISA-TAB

 * [Specification documentation: release candidate 1, ISA-TAB 1.0](http://isatab.sourceforge.net/docs/ISA-TAB_release-candidate-1_v1.0_24nov08.pdf).
 * [isatools](http://isa-tools.org).

## Biobanks & chembanks

 * [GMOD](http://gmod.org/wiki/Main_Page), Generic Model Organism Database.
 * [Spectral libraries and public repositories (MS-based metabolomics) - OMICtools](https://omictools.com/metabolite-libraries-category).
 * [Sixty-Four Free Chemistry Databases](http://depth-first.com/articles/2011/10/12/sixty-four-free-chemistry-databases/).
 * [BioCatalogue](http://www.biocatalogue.org/). No more active?

### DSSTox

The U.S. Environmental Protection Agency (EPA) has developed a “distributed structure searchable toxicity” (DSSTox) database containing over 875,000 compounds, which is available via the
CompTox Chemicals Dashboard (Williams et al. 2017), at https://comptox.epa.gov/dashboard/.

Include VOC compounds?

### NCI CACTUS

 * [CADD Group Chemoinformatics Tools and User Services](https://cactus.nci.nih.gov/).
 * [Convert CAS Registry Number to Other Identifiers](https://chejunkie.com/knowledge-base/convert-cas-registry-number-other/).
 * [Chemical Identifier Resolver documentation](https://cactus.nci.nih.gov/chemical/structure_documentation).
 * [Downloadable Structure Files of NCI Open Database Compounds](https://cactus.nci.nih.gov/download/nci/).

Convert a CAS ID into an InChI key:
```bash
wget 'https://cactus.nci.nih.gov/chemical/structure/75-07-0/stdinchikey'
```

Download CML format of an entry using NSC number:
```bash
wget 'https://cactus.nci.nih.gov/ncidb2.2/nci2.2.tcl/nsc7594.cml'
```

### Mass Spectrometry Data Center, NIST

<http://chemdata.nist.gov/>

### mzCloud – Advanced Mass Spectral Database

<https://www.mzcloud.org/>

### PFAM

PFAM db gene annotation
<http://pfam.xfam.org/>

### IEDB

Immune Epitope Database.

 * [IEDB](http://www.iedb.org/).

### CEU Mass Mediator

A tool for searching metabolites in different databases (Kegg, Metlin, LipidMaps and HMDB)

* [CEU Mass Mediator](http://ceumass.eps.uspceu.es).

### GNPS

 * [GNPS](https://gnps.ucsd.edu/ProteoSAFe/static/gnps-splash.jsp).

### MINE

 * [MINE](http://minedatabase.mcs.anl.gov/#/home).

### miRBase

 * [miRBase](http://www.mirbase.org).
 * [miRBase Sequence Download](http://www.mirbase.org/ftp.shtml).
  - [Fasta format sequences of all mature miRNA sequences](ftp://mirbase.org/pub/mirbase/CURRENT/mature.fa.gz).

Get HTML page of mature sequence:
```bash
wget -O mirbase.html 'http://www.mirbase.org/cgi-bin/mature.pl?mature_acc=MIMAT0000433'
```

### ChEBI

 * [ChEBI web services](http://www.ebi.ac.uk/chebi/webServices.do).
 * [ChEBI WSDL](http://www.ebi.ac.uk/webservices/chebi/2.0/webservice?wsdl).
 * [libChEBI: an API for accessing the ChEBI database](https://jcheminf.springeropen.com/articles/10.1186/s13321-016-0123-9).

Substructure search:
	Orchem use fingerprints
		See Venkata in ChEBI
	VentoFoggia
		findSubstructure
		Ullmann

However some functionalities can be called by REST, like getting XML of one entity:
```bash
wget -O record.xml 'http://www.ebi.ac.uk/webservices/chebi/2.0/test/getCompleteEntity?chebiId=30273'
```

Send REST request `getLiteEntity`:
```bash
wget -O record.xml 'https://www.ebi.ac.uk/webservices/chebi/2.0/test/getLiteEntity?search=(gamma)Glu-Leu+Ile&searchCategory=ALL+NAMES&maximumResults=0&starsCategory=ALL'
```

Send SOAP request `getLiteEntity`:
```bash
cat >request.xml <<EOF
<?xml version="1.0" encoding="UTF-8" standalone="no"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="http://www.ebi.ac.uk/webservices/chebi" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><tns:getLiteEntity xmlns:tns="http://www.ebi.ac.uk/webservices/chebi"><tns:search>1*</tns:search><tns:searchCategory>CHEBI ID</tns:searchCategory><tns:maximumResults>100</tns:maximumResults><tns:stars></tns:stars></tns:getLiteEntity></SOAP-ENV:Body></SOAP-ENV:Envelope>
EOF
curl --header "Content-Type: text/xml;charset=UTF-8" --header "SOAPAction: " --data @request.xml http://www.ebi.ac.uk:80/webservices/chebi/2.0/webservice
```

### NCBI

 * [APIs](https://www.ncbi.nlm.nih.gov/home/develop/api.shtml).
 * [Entrez Help](https://www.ncbi.nlm.nih.gov/books/NBK3837/).
 * [Entrez Programming Utilities Help](https://www.ncbi.nlm.nih.gov/books/NBK25501/).,
 * [Entrez Unique Identifiers (UIDs) for selected databases](https://www.ncbi.nlm.nih.gov/books/NBK25497/table/chapter2.T._entrez_unique_identifiers_ui/?report=objectonly).
 * [Genome project tables in the genomes Bioconductor package](http://www.bioconductor.org/packages//2.10/bioc/vignettes/genomes/inst/doc/genome-tables.pdf).
 * [Bionconductor package 'genomes'](https://www.bioconductor.org/packages/release/bioc/html/genomes.html).

About access frequency, see <https://www.ncbi.nlm.nih.gov/books/NBK25497/>.

The request must start with:
http://eutils.ncbi.nlm.nih.gov/entrez/eutils/

In order not to overload the E-utility servers, NCBI recommends that users post no more than three URL requests per second and limit large jobs to either weekends or between 9:00 PM and 5:00 AM Eastern time during weekdays. 

### Expasy Enzyme

 * [Enzyme](http://enzyme.expasy.org). Swissprot (SIB, EBI), ExPASy.
 * [THE ENZYME NOMENCLATURE DATABASE USER MANUAL](ftp://ftp.expasy.org/databases/enzyme/release_with_updates/release/enzuser.txt).

HTML
http://enzyme.expasy.org/EC/1.1.1.1

TEXT
http://enzyme.expasy.org/EC/1.1.1.1.txt

### LIPID MAPS

 * [LIPID MAPS](http://www.lipidmaps.org).
 * [Programmatic Access](http://www.lipidmaps.org/data/structure/programmaticaccess.html).

About access frequency, see <http://www.lipidmaps.org/data/structure/programmaticaccess.html>.

### Metlin

They block IP when querying from an application.

Query on mass:

Replace POST with GET inside search page in order to get the line of parameters:
```
file:///Users/pierrick/Desktop/metabolites_list.php?mass_mid=340&mass_tol=30&mass_mode=1&adducts=M%2BH&adducts=M%2BNa&formVar=M%2BH%2CM%2BNa
```

Then test post method with wget:
```bash
wget 'http://metlin.scripps.edu/metabolites_list.php' --post-data='mass_mid=340&mass_tol=30&mass_mode=1' -O meta.html
wget 'http://metlin.scripps.edu/metabolites_list.php' --post-data='mass_mid=340&mass_tol=30&mass_mode=1&adducts=M%2BH&adducts=M%2BNa&formVar=M%2BH%2CM%2BNa' -O meta.html
wget 'http://metlin.scripps.edu/metabolites_list.php' --post-data='mass_mid=340&mass_tol=30&mass_mode=1&adducts=M%2DH&adducts=M%2BCl&formVar=M%2DH%2CM%2BCl' -O meta2.html
```

Query on ID:
```bash
wget -O response.html 'http://metlin.scripps.edu/metabo_info.php?molid=66045'
```

### UNIPROT

 * [How can I access resources on this web site programmatically?](http://www.uniprot.org/help/programmatic_access).
 * [UniProtKB query fields](http://www.uniprot.org/help/query-fields).
 * [Bioconductor package UniProt.ws](https://www.bioconductor.org/packages/release/bioc/html/UniProt.ws.html).

Download list of all Uniprot IDs:
```bash
wget -O uniprot.tab.gz 'http://www.uniprot.org/uniprot/?query=&columns=id&format=tab&compress=yes'
```

Download list of Uniprot IDs with limit of results:
```bash
wget -O uniprot.tab.gz 'http://www.uniprot.org/uniprot/?query=&columns=id&format=tab&compress=yes&limit=100'
```

### Massbank

 * [MassBank](http://www.massbank.jp/).
 * [MassBank WEB-API Reference](http://www.massbank.jp/manuals/api-doc_en/reference.html).
 * [WSDL](http://www.massbank.jp/api/services/MassBankAPI?wsdl).
 * [All opendata records on SVN server](http://www.massbank.jp/SVN/OpenData/record/).
 * [MassBank Record Format 2.10 (draft)](http://www.massbank.jp/manuals/MassBankRecord_en.pdf).

Web service API address: `http://www.massbank.jp/api/services/MassBankAPI/`.

`ping` works on `massbank.jp` but not on `massbank.eu`.

REST services do not work anymore. Only SOAP is allowed.

Get a record:
```bash
cat >request.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="http://api.massbank">
	<SOAP-ENV:Body>
		<tns:getRecordInfo>
			<tns:ids>KOX00001</tns:ids>
			<tns:ids>KOX00002</tns:ids>
			<tns:ids>TY000040</tns:ids>
			<tns:ids>FU000001</tns:ids>
		</tns:getRecordInfo>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOF
curl --header "Content-Type: text/xml;charset=UTF-8" --header "SOAPAction: " --data @request.xml 'http://www.massbank.jp/api/services/MassBankAPI.MassBankAPIHttpSoap11Endpoint/'
```

Search mz: TODO update
```bash
cat >request.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="http://api.massbank">
	<SOAP-ENV:Body>
		<tns:searchPeak>
			<tns:mzs>100</tns:mzs>
			<tns:relativeIntensity>0</tns:relativeIntensity>
			<tns:tolerance>1</tns:tolerance>
			<tns:instrumentTypes>all</tns:instrumentTypes>
			<tns:ionMode>Both</tns:ionMode>
			<tns:maxNumResults>10</tns:maxNumResults>
		</tns:searchPeak>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOF
curl --header "Content-Type: text/xml;charset=UTF-8" --header "SOAPAction: " --data @request.xml 'http://www.massbank.jp/api/services/MassBankAPI.MassBankAPIHttpSoap11Endpoint/'
```

### ChemSpider

 * [ChemSpider Webservices](http://www.chemspider.com/AboutServices.aspx).
 * [ChemSpider search API documentation](http://www.chemspider.com/Search.asmx).
 * [ChemSpider Search API WSDL](http://www.chemspider.com/Search.asmx?WSDL).
 * [ChemSpider MassSpec API documentation](http://www.chemspider.com/MassSpecAPI.asmx).
 * [ChemSpider MassSpec API WSDL](http://www.chemspider.com/MassSpecAPI.asmx?WSDL).

Send request `filter-mass-post`:
```bash
curl -X POST --header "Content-Type: " --header "apikey: 3cvKJuyxCa3GDkwtjcILyL6r1whgZAv2" -d "{
	    \"mass\": 100,
		        \"range\": 10
}" "https://api.rsc.org/compounds/v1/filter/mass"
```

Send SOAP request `SearchByMassAsync` (old ChemSpider API):
```bash
cat >request.xml <<EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SearchByMassAsync xmlns="http://www.chemspider.com/">
      <mass>500</mass>
      <range>10</range>
      <token>YOUR TOKEN HERE</token>
    </SearchByMassAsync>
  </soap:Body>
</soap:Envelope>
EOF
curl --header "Content-Type: text/xml;charset=UTF-8" --header 'SOAPAction: "http://www.chemspider.com/SearchByMassAsync"' --data @request.xml http://www.chemspider.com/MassSpecAPI.asmx
```

### Mirbase

Getting accession number from ID:
```bash
wget -O response.html 'http://www.mirbase.org/cgi-bin/query.pl?terms=hsa-miR-142-3p&submit=Search'
```

Getting mirna page:
```bash
wget -O response.html 'http://www.mirbase.org/cgi-bin/mature.pl?mature_acc=MIMAT0000434'
```

#### Using whole file of a species

1) Download zip file :
wget ftp://ftp.sanger.ac.uk/pub/mirbase/targets/v5/arch.v5.gff.homo_sapiens.zip
2) Expand it.
3) look into it for the miRBase ID, and get the URL for Ensembl database, as well as the start and end.
4) Use Ensembl database for getting sequence.
Getting extended compound info:
```bash
wget -O record.xml 'http://www.chemspider.com/MassSpecAPI.asmx/GetExtendedCompoundInfo?CSID=31982&token=achemspidertoken'
```

Getting an array of extended compound info:
```bash
wget -O records.xml 'http://www.chemspider.com/MassSpecAPI.asmx/GetExtendedCompoundInfoArray?CSIDs=31982&CSIDs=4444962&token=achemspidertoken'
```

### PubChem

[PubChem](https://pubchem.ncbi.nlm.nih.gov) is composed of three different databases: Compound (CID), Substance (SID) and BioAssay (AID).

 * [PubChem Web services](https://pubchem.ncbi.nlm.nih.gov/pug_rest/PUG_REST.html).
 * [A PUG REST Tutorial](https://pubchem.ncbi.nlm.nih.gov/pug_rest/PUG_REST_Tutorial.html).
 * [PubChem FTP](ftp://ftp.ncbi.nlm.nih.gov/pubchem/).

About access frequency, see <https://pubchem.ncbi.nlm.nih.gov/pug_rest/PUG_REST.html>.

File of parents: insde PubChem FTP: <ftp://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-Parent.gz>. Contains a list of all CIDs of the compound database, with the parent CID in the second column.

Getting several compounds at the same time:
```bash
wget -O records.xml 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/1,2,3,4,5/XML'
```

Searching by term:
```bash
wget -O records.xml "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pccompound&term=*acid*&field=synonym&retmax=100"
```
A first run with `term=0` permits to get the number of matches, then we can use this number in a second call with `term=<the_number>` in order to get all matches.

Getting entries by mass range:
```bash
wget -O records.xml "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=protein&term=70000:90000[molecular+weight]"
```

Getting entries by term and mass range:
```bash
wget -O records.xml "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=protein&term=acid%20AND%2070000:90000[molecular+weight]"
```

There are three databases in PubChem:
 - PubChem BioAssay.
 - PubChem Substance.
 - PubChem Compound.

 * [What is the difference between a substance and a compound in PubChem?](https://pubchemblog.ncbi.nlm.nih.gov/2014/06/19/what-is-the-difference-between-a-substance-and-a-compound-in-pubchem/).

### Metabolights

 * [Metabolights](http://www.ebi.ac.uk/metabolights/).

Accessing a private study on the FTP server: /prod/15fef9e0-9187-4c8a-857d-93d8e7df53d0

### BioCyc

 * [BioCyc](https://biocyc.org/)

Pathways.

### KEGG

 * [Rest API](http://rest.kegg.jp/).
 * [Rest API documentation](http://www.kegg.jp/kegg/docs/keggapi.html).

Get a compound entry:
```bash
wget -O entry.txt 'http://rest.kegg.jp/get/cpd:C00751'
```

Get a pathway entry:
```bash
http://rest.kegg.jp/get/path:map00680
```

Get a pathway module entry:
```bash
http://rest.kegg.jp/get/md:M00344
```

### HMDB

 * [Downloads](http://www.hmdb.ca/downloads).
 * MS search: <http://www.hmdb.ca/spectra/ms/search>.
 
Retrieve all metabolites:
```bash
wget -O hmdbmetabolites.zip "http://www.hmdb.ca/system/downloads/current/hmdb_metabolites.zip"
```

Search for metabolites by name (returns HTML):
```bash
http://www.hmdb.ca/unearth/q?query=acid&searcher=metabolites
http://www.hmdb.ca/unearth/q?page=2&query=acid&searcher=metabolites
http://www.hmdb.ca/unearth/q?page=3&query=acid&searcher=metabolites
```

Search for metabolites by molecular mass (get an HTML page):
```bash
wget 'http://www.hmdb.ca/structures/search/metabolites/mass?utf8=✓&query_from=99.90&query_to=100.12&search_type=molecular'
```

Search for metabolites by monoisotopic mass (get an HTML page):
```bash
wget 'http://www.hmdb.ca/structures/search/metabolites/mass?&query_from=97.00&query_to=100.12&search_type=monoisotopic'
wget 'http://www.hmdb.ca/structures/search/metabolites/mass?&page=2&query_from=97.00&query_to=100.12&search_type=monoisotopic'
wget 'http://www.hmdb.ca/structures/search/metabolites/mass?&page=3&query_from=97.00&query_to=100.12&search_type=monoisotopic'
```

```bash
wget 'http://specdb.wishartlab.com/ms/search.csv?utf8=TRUE&mode=positive&query_masses=420.159317&tolerance=0.000001&database=HMDB&commit=Download Results As CSV'
```

### Peakforest

 * [PeakForest](https://peakforest.org/).
 * [PeakForest alpha](https://peakforest.clermont.inra.fr/webapp-alpha/home).
 * [Webservices documentation](http://doc-ws.peakforest.org).
 * [REST server](http://rest.peakforest.org/).
 * [Excel template](https://peakforest.org/template).
 * [PeakForest alpha](https://peakforest-alpha.inra.fr/), serveur "bac-à-sable".
 * [PeakForest REST alpha](https://peakforest-alpha.inra.fr/rest/), serveur "bac-à-sable".

Example of request with API key token:
```bash
wget "https://rest.peakforest.org/compounds/1?token=mytoken"
wget "https://rest.peakforest.org/compounds/all/names?molids=33,42,666&token=mytoken"
```

Getting all LCMS spectra IDs:
```bash
wget "https://rest.peakforest.org/spectra/lcms/all/ids?token=mytoken"
```

To search for a compound by name:
```bash
wget "https://rest.peakforest.org/search/compounds/acid?token=mytoken"
```

To get list of columns:
```bash
curl "https://rest.peakforest.org/metadata/lc/list-columns"
```
On server alpha:
```bash
curl "https://peakforest-alpha.inra.fr/rest/metadata/lc/list-code-columns?token=mytoken"
```

To get all compound IDs:
``` {.bash}
curl "https://rest.peakforest.org/compounds/all/ids"
```

## MeV

 * [MeV](http://www.tm4.org/mev/) official site.

Biology analysis software.

MultiExperiment Viewer, a versatile microarray data analysis tool, incorporating sophisticated algorithms for clustering and visualization.
