# Local

 * [Free Mobile](https://mobile.free.fr/index.php?).

 * [meteoalarm (EUMETNET)](http://meteoalarm.eu/).
 * [Windfinder Cicciano](https://www.windfinder.com/#10/40.9357/14.7179).
 * [Centro di allerta meteo Italia](http://allarmi.meteo-allerta.it/index.html).

 * [ANTICOR](https://www.anticor.org/outils-citoyens/).

 * [Étiquetage pour l'entretien des textiles](https://fr.m.wikipedia.org/wiki/%C3%89tiquetage_pour_l%27entretien_des_textiles).
 * [Speed Test](https://www.speedtest.net/).
 * [Free Mobile](http://mobile.free.fr/).
 * [Freebox OS](http://mafreebox.freebox.fr/).
 * [Orari Circumvesuviana](https://www.eavsrl.it/web/orari).
 * [Institut Paris Région](https://www.institutparisregion.fr/).
  - [Cartoviz](https://cartoviz.institutparisregion.fr/).
 * [VITRAIL - Trafic aérien](https://vitrail.entrevoisins.org/vitrail/).
 * [Comune di Cicciano](https://www.comune.cicciano.na.it/).
 * [Bourg-la-Reine](http://www.bourg-la-reine.fr/).
  - [Ludothèque AGF](http://www.ludotheque-agf.org/).
  - [Le guide du tri](http://www.valleesud-tri.fr/le-guide-du-tri/).
  - [Espace Citoyens](https://www.espace-citoyens.net/bourg-la-reine/espace-citoyens/).

## Bourg-la-Reine

 * [Site officiel - Ville de Bourg-La-Reine](https://www.bourg-la-reine.fr/).
 * [Espace citoyens BLR](https://www.espace-citoyens.net/bourg-la-reine/espace-citoyens/).
 * [Infrastructure de Données Géographiques — Open data Île-de-France](https://data.iledefrance.fr/pages/idg/).

### École et enfance

 * [Service Enfance - Service municipal](https://www.bourg-la-reine.fr/annuaire/24885/10313-service-enfance.htm).

 * [Collège Evariste Galois - Site officiel du collège Evariste Galois](http://www.clg-galois-bourg-la-reine.ac-versailles.fr/).
 * [Pass+](https://www.passplus.fr/Beneficiaire/LandingPage.aspx?ReturnUrl=%2fBeneficiaire%2findex.html).
 * [ENC Hauts-de-Seine (OZE)](https://enc.hauts-de-seine.fr/my.policy).
 * [Accueils de loisirs périscolaires - Ville de Bourg-La-Reine](https://www.bourg-la-reine.fr/10707-accueils-de-loisirs-periscolaires.htm).
 * [CHAM Instrumentale | le CRD de Bourg-la-reine/Sceaux](https://www.professeurs-crd-blrscx.fr/cham-college-2020-2021/cham-instrumentale/).
  * [CHAM Vocale | le CRD de Bourg-la-reine/Sceaux](https://www.professeurs-crd-blrscx.fr/cham-college-2020-2021/cham-vocale/).
 * [Programmes des activités périscolaires - Ville de Bourg-La-Reine](https://www.bourg-la-reine.fr/10708-programmes-des-activites-periscolaires.htm).
 * [bon'App / Elior](https://bonapp.elior.com/connexion?returnUrl=%2F).
 * [So English!](https://langues-eleves.editions-hatier.fr/collection_soenglish.php).

### Activités culturelles

 * [Médiathèque François Villon](https://www.mediablr.net/).
   + [Médiathèque numérique](https://vod.mediatheque-numerique.com/).
 * [COURS THEATRE ENFANTS | Bourg-la-Rei | Sur La Route Des Comédiens](https://www.surlaroutedescomediens.com/cours-enfants).
 * [Cour des marguerites : ateliers ludiques de langues à Antony](https://courdesmarguerites.fr/).
 * [CAEL](https://cael.goasso.org/).
 * [le CRD de Bourg-la-reine/Sceaux | blog du CRD](https://www.professeurs-crd-blrscx.fr/).
 * [Les Spectacles - Les Gémeaux](http://www.lesgemeaux.com/spectacles/).
 * [Extranet du Conservatoire de Bourg-la-Reine/Sceaux](https://bourglareine.rdl.fr/).
 * [Domaine départemental de SCEAUX](https://domaine-de-sceaux.hauts-de-seine.fr/).

### Sports

 * [L'association - ASBR92 - Association Sportive de Bourg la Reine](https://www.asbr92.fr/lassociation).
 * [Piscine des Blagis - Sceaux | Vallée Sud - Grand Paris](https://www.valleesud.fr/fr/piscine-des-blagis-sceaux).
 * [Le club | ASBR92](https://asbr-92.assoconnect.com/page/1111844-le-club).
 * [Accueil EBR basket](http://www.ebrbasket.com/EBR/).
 * [Equipe U13- Benjamins](http://www.ebrbasket.com/EBR/index.php?option=com_content&view=article&id=14&Itemid=9).

### Immobilier

 * [Recherche appartement](https://docs.google.com/document/d/1Vjvfmp199h4E_487e86S9NI7TZncYxhrgL97F7SJfS0/edit#).
 * [Recherche garage](https://docs.google.com/document/d/12uWo9hHCuNunjp8KdnqraUm3ueIksxkK6PZhmvId-zY/edit#).
 * [Prix immobilier](https://www.meilleursagents.com/prix-immobilier/).
 * [DVF](https://app.dvf.etalab.gouv.fr/).
 * [Vitrail | Visualisation des trajectoires des avions](https://vitrail.entrevoisins.org/vitrail/).

 * [Galé Immobilier - Agence Immobilière](http://www.galeimmobilier.com/nosbiens).
 * [Immo Condorcet](https://www.immocondorcet.fr/).
 * [Carte SESAME ESCALES SOLO à PARIS @ GRAND PALAIS - Billets & Places](https://billetterie.grandpalais.fr/sesame-escales-solo-carte-grand-palais-paris-non-datee-css5-rmnsesame-pg101-ri7456559.html).

## Musées, châteaux, ...

 * Château de Maisons-Laffitte
 * [Château de Fontainebleau](https://www.chateaudefontainebleau.fr/).
 * Musée d'Archéologie nationale - Château de Saint-Germain-en-Laye
 * Château de Champs-sur-Marne
 * Château de Nemours
 * château de Breteuil dans les Yvelines
 * Château de Dampierre
 * Château de Rambouillet
 * Château de La Roche-Guyon
 * Chateau de Dourdan
 * Château d'Ecouen - Musée national de la Renaissance

## Cueillettes en Île-de-France

 * [La Ferme de Viltain](https://www.viltain.fr/).
 * Chapeau de Paille :
  - [Cueillette de Torfou](https://www.cueillettedetorfou.fr/).
  - [Cueillette de Gally](https://www.cueillettedegally.com/).
 * [La Cueillette du Plessis](https://www.cueilletteduplessis.com/).
