# Binary data

## dhex

Diff hexadecimal editor.

Install:
```sh
yay -S dhex
```

```sh
dhex myfileA myfileB
```

## hexdump

Tool for dumping a file content in hexadecimal format:
```sh
hexdump myfile
```

Display also ASCII characters:
```sh
hexdump -C myfile
```

Using hexdump to compare binary files:
```sh
hexdump -C file1.bin >file1.txt
hexdump -C file2.bin >file2.txt
diff file1.txt file2.txt
```

## hexedit

Hexademical editor.

```sh
hexedit myfile
```

Key    | Description
------ | --------------------
/      | Search hexadecimal.
Tab+/  | Search ASCII.
Tab    | Toggle between ASCII and hexadecimal.
Ctrl+T | Toggle between ASCII and hexadecimal.

## split

Split a big file into several one giga bytes files:
```sh
split -b 1G myfile.bin myfile
```

## dd

Cut binary stream, keeping only the first 30 bytes:
```sh
dd iflag=skip_bytes,count_bytes count=30 </dev/random
```
We can also skip first bytes with `skip=20`.

## truncate

Truncate a file:
```sh
truncate -s 1G test.txt
```

## strings

Extract strings from binary file.
```sh
strings mybinaryfile
```

## xxd

Dump a file in hexadecimal.

```sh
xxd -p myfile
```

Searching for a sequence of bytes into a file using xxd and grep:
```sh
xxd -p myfile | tr -d '\n' | grep -c 'e280a8'
```

## cmp

Compare two binary files:
```sh
cmp -l file1.bin file2.bin
```
