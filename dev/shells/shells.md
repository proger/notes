# Shells

 * [Interactive and non-interactive shells and scripts](http://www.tldp.org/LDP/abs/html/intandnonint.html).

`~/.xessionrc` is read when loging graphically under Debian familly systems.
 * [Difference between .xinitrc, .xsession and .xsessionrc](https://unix.stackexchange.com/questions/281858/difference-between-xinitrc-xsession-and-xsessionrc).

 * [Nushell](https://www.nushell.sh/). Cross-platform shell with structured data in pipelines.

## SH

sh doesn't accept `for ((...` statement structure. It's reserved to bash.

## SHEBANG

Shebang (SHarp BANG or haSH BANG) is the first line in script, indicating the interpreter to run:
```bash
#!/bin/bash
```

One can pass arguments to the interpreter:
```python
#!/usr/bin/python -c
```

Full path of the interpreter is compulsory.

In order to avoid specifying full path for the interpreter, one can invoke it through the env command which most of the time resides in /usr/bin/env (except in Unicos and OpenServer)
```perl
#!/usr/bin/env perl
```
