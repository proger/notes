# zsh
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','zsh':'sh'} -->

 * [ZSH Documentation](http://zsh.sourceforge.net/Doc/).
 * [A User's Guide to the Z-Shell](http://zsh.sourceforge.net/Guide/zshguide.html).

## Configuration files

`~/.zprofile`: read in console and ssh login.
`~/.zshenv`: always read, in all sessions.
`~/.zshrc`: read on interactive sessions.

## Built-in variables

In addition to the `PATH` environment variable, zsh defines a `path` array.

## Command Substitution (backsticks and $ parens)

When affecting a variable, new lines are not replaced with space in zsh:
```zsh
x=$(find . -type f)
```
In order to get space, use `tr`:
```zsh
x=$(find . -type f | tr "\n" " ")
```

## bindkey

List key bindings:
```zsh
bindkey
```

Bind Ctrl-A to beginning of line:
```zsh
bindkey "^A" beginning-of-line
```

Bind Ctrl-E to end of line:
```zsh
bindkey "^E" end-of-line
```

## for

Loop on words in a string does not work in zsh. The following code will lead to
only one iteration with x having the value of s:
```zsh
s="a b c"
for x in $s ; do
    echo "$x"
done
```
For this loop to work like in bash, write it the following way:
```zsh
s="a b c"
for x in $(echo "$s") ; do
    echo "$x"
done
```

## rehash

 * [Persistent rehash](https://wiki.archlinux.org/title/zsh#Persistent_rehash).

Rehash paths to executable after a change of `PATH` env var:
```zsh
rehash
# or
hash -r
```

## which

`which` is a built-in command in zsh, that returns "<command> not found" on stdout when failing.

## Arrays

Index starts at 1.
