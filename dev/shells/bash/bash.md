# bash
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','bash':'sh'} -->

## `.bashrc`, `.bash_profile`, `.bash_logout`

`.bashrc`:
bash run command file ("rc" stands for "run command", which means that the file is executed/read when bash command is run/started) when invoked as an interactive non-login shell.
i.e.: .bashrc is read when bash is executed or when a new terminal/shell window is opened under the window system --> = new bash instance

`.bash_profile`:
personal initialization file
Sourced when started in interactive login mode (from ssh login or OS login in console mode).
/etc/profile is read first, then :  first of ~/.bash_profile, ~/.bash_login, or ~/.profile.
executed/loaded when run as interactive login shell.

`~/.bash_logout`:
executed when exiting from login shell

## source, `.`

Include/run code from another file:
```bash
. myfile
source myfile
```


## Running

Run a bash command on command line:
```bash
bash -c "mycommand -myopt"
```

All short options of the `set` command can be used on `bash` command line.

## History

To call the last command begining with a sequence of chars:
```bash
!mycmd
```

To call the last command containing a sequence of chars:
```bash
!?sometext
```

To search in history: ctrl-r

To list all commands with their history number:
```bash
history
```

To run a command by its history number:
```bash
!<history number>
```

## set

Disable POSIX mode:
```bash
set +o posix
```
For instance to enable process substitution.

Enable inheritance of any trap function on ERR signal:
```bash
set -o errtrace
# or
set -E
```
This allows to trap ERR signal in shell functions, command substitutions and
commands in a subshell.

Exit immediately if a pipeline, a list (commands separated by `;`, `&`, `&&` or
`||`) or a compound command (`()`, `{}`, `(())`, `[[]]`) fails:
```sh
set -o errexit
# or
set -e
```

Treat unset variables as errors:
```sh
set -o nounset
```

Make a pipeline fails if any command fails:
```bash
set -o pipefail
```
The status code of the pipeline is the status code of the rightmost command that has a non-zero status.

Prints each line of code:
```bash
set -v
```

Prints each command and its arguments:
```bash
set -o xtrace
# or
set -x
```

Forbids overwriting of existing files with redirection operator `>`:
```sh
set -o noclobber
# or
set -C
```

Set editing mode to vi (default is Emacs):
```bash
set -o vi
```
 * [How To Use Vim Mode On The Command Line In Bash](https://dev.to/brandonwallace/how-to-use-vim-mode-on-the-command-line-in-bash-fnn).
 * [The way to distinguish command-mode and insert-mode in Bash's Vi command line editing](https://stackoverflow.com/questions/7888387/the-way-to-distinguish-command-mode-and-insert-mode-in-bashs-vi-command-line-ed/42107711#42107711).

Emacs  | Vim  | Result
-----  | ---  | ------
Ctrl+A  | 0  | Move cursor to beginning of line.
Ctrl+E  | $  | Move cursor to end of line.
Alt+B  | b  | Move cursor back one word.
Alt+F  | w  | Move cursor right one word.
Ctrl+B  | h  | Move cursor back one character.
Ctrl+F  | l  | Move cursor right one character.
Ctrl+P  | k  | Move up in Bash command history.
Ctrl+R  | j  | Move down in Bash command history.

## trap

Define a command to execute when receiving a signal:
```bash
trap "rm $my_file; exit" SIGHUP SIGINT SIGTERM
```

| -------- | --------------- |
| Signal   | Description     |
| -------- | --------------- |
| SIGWINCH | Window resized. |
| -------- | --------------- |

Remove a file on exit:
```bash
trap "rm myfile" EXIT
```

Run a function on exit:
```bash
trap myfct EXIT
```

Remove a command associated with a signal:
```bash
trap - EXIT
```

List available signals:
```bash
trap -l
```

List commands associated with signals:
```bash
trap -p
```

## ulimit

To run a program without stack limit:
```bash
ulimit -s unlimited ; ./myprog
```

## Timeout

In order to run a program and kill after some time, run:
```bash
myprog & sleep 5 ; kill $!
```

See <http://stackoverflow.com/questions/687948/timeout-a-command-in-bash-without-unnecessary-delay> for a generic script.

## Debugging a script

Prints each line of code:
```bash
bash -v myscript.sh
```

Prints each command and its arguments:
```bash
bash -x myscript.sh
```

## Program name

Get script name:
```bash
SCRIPT_NAME="${0##*/}"
SCRIPT_NAME=$(basename $0)
```

Get path to scriptname:
```bash
SCRIPT_DIR="${0%/*}" # Returns script name if no path.
SCRIPT_DIR=$(dirname $0) # Returns '.' if no path.
```
This only works if there is a path (i.e.: at least one slash), but this should be the case since the system prefixes the program name with the path used inside PATH when calling a program.

## Prompt

```bash
PS1="\[\033[0;33m\][\$(date +%H%M)][\u@\h:\w]$\[\033[0m\] "
export PS1='\[\e[1;32m\][\u@\h \W]\$\[\e[0m\] '
```
Backslashed brackets are here to enclose command characters that control coloring. This allows bash to isolate those characters and do not count them when computing prompt length. Prompt length is needed when going backward into previous commands, searching into previous commands, wraping and going to the beginning of line.

Escape sequence | Description
--------------- | ----------------------
`\w`            | Full path of current directory.
`\W`            | Current directory.
`\u`            | User name.
`\h`            | Host name.

## Command Substitution (backsticks and $ parens)

 * [Command Substitution](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_06_03).
 * [Bash scripting: Moving from backtick operator to $ parentheses](https://www.redhat.com/sysadmin/backtick-operator-vs-parens).

`$()` should be preferred on backstick, as it is more evident to spot when
reading.
A `$()` call can be nested in another `$()` call.

Attention when checking the result status.
If used to set a variable's value in a declaration (using `declare` or
`local`), the result status will be hidden (i.e.: it will always be zero, the
result status of `declare` or `local`). The solution is to do it in two steps:
```sh
declare myvar
myvar=$(somecall)
```

## Redirections (pipes and process substitutions)

 * [Bash Process Substitution](https://www.linuxjournal.com/content/shell-process-redirection).
 * [Process substitution and pipe](https://unix.stackexchange.com/questions/17107/process-substitution-and-pipe).
 * [Performance differences between pipelines and process substitution](https://unix.stackexchange.com/questions/127645/performance-differences-between-pipelines-and-process-substitution).
 * [3.2.3 Pipelines](https://www.gnu.org/software/bash/manual/html_node/Pipelines.html).
 * [Using Named Pipes (FIFOs) with Bash](https://www.linuxjournal.com/content/using-named-pipes-fifos-bash).

Each command of a pipeline is executed in its own subshell.

Process substitution is not POSIX. You need to enable this feature explicitly:
```bash
set +o posix
```

Redirect stdin:
```bash
myprogram < somefile
```

Redirections:
```bash
myprogram > someotherfile   # redirect STDOUT
myprogram 2>err.log     # redirect STDERR
myprogram 2>/dev/null       # discard STDERR (put into null device)
myprogram >myfile.log 2>&1  # redirect both STDOUT and STDERR into a log file
myprogram 2>&1 >myfile.log  # redirect STDOUT to a file and STDERR to original STDOUT (not to the file)
myprogram >&2           # redirect STDOUT to STDERR
myprogram &>somefile        # redirect all outputs to a file
myprogram &>/dev/null       # silence all outputs
```

Pipe stdout:
```bash
prog1 | prog2
```

Pipe both stderr and stdout:
```bash
prog1 2>&1 | prog2
```

Process substitution for input:
```bash
prog1 <(prog2)
```
The process substitution creates a file (named pipe) in /proc to write the
output of `prog2` and pass the filename to `prog1`.
To pass the content of the named pipe instead of its filename, use also STDIN
redirection:
```bash
prog1 < <(prog2)
```

Process substitution for output:
```bash
prog1 >(prog2)
```

Multiple process substitution for one command:
```bash
comm -3 <(sort file1 | uniq) <(sort file2 | uniq)
```

## Alias

Creating an alias:
```bash
alias blabla='echo coucou'
```

Removing an alias:
```bash
unalias blabla
```

## Variables

Declaring a typed variable:
```bash
declare -r CST=12   # read-only variable (= constant)
declare -i n        # integer variable
declare -a arr      # indexed array
declare -g myvar    # declare variable myvar in the global scope
declare -l myvar    # variable in lowercase always
declare -A arr      # associative array (not available in bash 3.2)
declare -f fct      # function
declare -u myvar    # variable in uppercase always
declare -x VAR      # export variable in environment
```

Declaring a local variable:
```bash
function myfct {
    local i=0
}
```

Default value set if variable is unset or null:
```bash
myvar2=${myvar1:-default value}
```
If the colon is omitted, the default value is set only if the variable is unset:
```bash
myvar2=${myvar1-default value}
```

Value replaced if variable is neither unset nor null:
```bash
myvar2=${myvar1:+value}
```
If the colon is omitted, the value is replaced also if the variable is null:
```bash
myvar2=${myvar1+value}
```

Test if a variable is not defined (never set, even to empty string):
```bash
[[ ! ${myvar} ]] && ...
```

Length of a variable (number of characters):
```bash
len=$#myvar
len=${#myvar}
```

Dereferencing a variable:
```bash
toto=coucou
varname=toto
echo ${!varname}
eval echo '$'$varname
```
The same for an array:
```bash
declare -a myarr=(green blue red)
arrayvarname=myarr[@]
echo ${!arrayvarname}
```

Dynamic variable names:
```bash
toto_email=zozo@gmail.com
account=toto
varname=${account}_email
email=${!varname}
```
See example `examples/bash/dyn_ref_var`.

Setting the value of a dynamic variable with the `declare` keyword:
```bash
declare "${account}_email=zozo@gmail.com"
```

## Built-in variables

`IFS`: Stores the characters used to separate elements of a list. By default it
is set to space, tabulation and new line, but it can be set to any value.

`LINENO`: Current line number.

`BASH_LINENO`: Array containing the line numbers of the call stack.
`${BASH_LINENO[$i]}` is the line number in the source file
`(${BASH_SOURCE[$i+1]})` where `${FUNCNAME[$i]}` was called (or
`${BASH_LINENO[$i-1]`} if referenced within another shell function).

`${FUNCNAME[0]}` is the current function.
`${FUNCNAME[-1]}` is the main code, labelled `main`.
`${BASH_LINENO[0]}` is the line number of the calling code.
`${BASH_SOURCE[1]}` is the script file of the calling code.
`${FUNCNAME[1]}` is the calling function.
`${BASH_LINENO[-1]}` is always `0`.

`FUNCNAME`: Array of function names of the call stack.
`BASH_SOURCE`: Array of the script names of the call stack.

`LINES`: number of lines of the current terminal.
`COLUMNS`: number of columns of the current terminal.

`$BASH_VERSION`.
`${BASH_VERSINFO[0]}`       The major version number (the release).
`${BASH_VERSINFO[1]}`       The minor version number (the version).
`${BASH_VERSINFO[2]}`       The patch level.

`${PIPESTATUS[@]}`: An array containing the status codes of all commands of a pipeline, from left to right.

## Arrays

 * [The Ultimate Bash Array Tutorial with 15 Examples](http://www.thegeekstuff.com/2010/06/bash-array-tutorial).

Index starts at 0.

Declaring an array:
```bash
my_array=(apple pear lemon "pink lady")
```
or
```bash
declare -a my_array=(apple pear lemon)
```

Push a value into an array:
```bash
myarr+=('newval')
```

Insert a value at the start of an array:
```bash
myarr=("some_value" "${myarr[@]}")
```

Getting first value of an array:
```bash
${my_array[0]}
```

Getting last value of an array:
```bash
${my_array[-1]}
```

Setting a value:
```bash
my_array[2]="foo"
```

Getting all values of an array:
```bash
${my_array[@]}
```

Getting number of values (length, size) of an array:
```bash
${#my_array[@]}
```

Get all indices of the array:
```bash
for i in "${!my_array[@]}" ; do
    # ...
done
```

Check if an array contains a value (supposing no value contains spaces):
```bash
[[ " ${myarr[@]} " == *" $myvalue "* ]] && do_something
```

Add a prefix to all elements:
```bash
myarr=("${myarr[@]/#/myprefix}")
```

Add a suffix to all elements:
```bash
myarr=("${myarr[@]/%/mysuffix}")
```

Remove an element (in fact set to empty string ""):
```bash
myarr=("${myarr[@]/myelem}")
```

Reset second element (size of array is unchanged, the element still exists but is null):
```bash
unset -v 'myarr[1]' 
```

Delete second element (size of array is redecuded by one):
```bash
myarr=( "${myarr[@]:0:1}" "${myarr[@]:2}" )
```

Slice an array:
```bash
myarr=("${myuarr[@]:1:2}")
```
First figure is the start index.
Second figure is the length.

Iterate over all elements of an array:
```bash
for e in ${my_array[@]} ; do
    # ...
done
```

If elements contain spaces, use quotes:
```bash
for e in "${my_array[@]}" ; do
    # ...
done
```

Read an array from a file:
```bash
myarr=($(cat myfile))
```

Read an array from a file:
```bash
readarray -t my_array <my_file.txt
```

List files into an array (glob function):
```bash
myarr=(/some/folder/*)
```
ATTENTION: if the path does not exist, the array will have the pattern as value.

Join elements of an array using space as separator:
```bash
echo "${my_array[*]}"
```

Join elements of an array using a custom separator (see [Join elements of an array?](https://stackoverflow.com/questions/1527049/join-elements-of-an-array)):
```bash
function join_by { local IFS="$1"; shift; echo "$*"; }
join_by , a "b c" d #a,b c,d
join_by / var local tmp #var/local/tmp
join_by , "${FOO[@]}" #a,b,c
```

## Associative array

It's also possible to define associative arrays (starting from bash 4):
```bash
declare -A my_array
declare -A my_array=(key1 value1 key2 value2 ...) # Is it correct? Works in bash 5
declare -A my_array=([key1]=value1 [key2]=value2 ...) # Using subscripts (bash 4)
my_array[some_key]=some_value # Works on bash 4.2
my_array+=("some_key" "some_value") # Works in Bash 5.1.6, but not in 5.0.018.
my_array+=(["some_key"]="some_value") # Concatenate with existing value on bash 4.2
echo ${my_array[some_key]}
```

In bash 4.2 a bug prevents to declare a global associative array from inside a
function. Thus associative array **must** be declared at global scope first
before using them globally into functions:
```bash
declare -gA myAssocArr

function foo {
    myAssocArr=()
    myAssocArr[a]=10
    ...
}
```

Get all keys of an associative array:
```bash
keys=${!my_array[@]}
```

Test if a key exists (has a non-empty value):
```bash
[[ -n ${my_array[mykey]} ]] && do_something
```

## Strings

Remove a part of a string:
```bash
echo ${MYVAR#a*o}   # Remove the shortest string a*o at the beginning of MYVAR
echo ${MYVAR##a*o}  # Remove the longest string a*o at the beginning of MYVAR
echo ${MYVAR%.*}    # Remove the shortest string .* at the end of MYVAR
echo ${MYVAR%%.*}   # Remove the longest string .* at the end of MYVAR
```

Replace:
```bash
echo ${MYVAR/a/e}   # Replace first 'a' by 'e'
echo ${MYVAR//a/e}   # Replace all 'a' by 'e'
echo ${MYVAR/%a/e}   # Replace 'a' by 'e' if it is placed at the end
echo ${MYVAR/#a/e}   # Replace 'a' by 'e' if it is placed at the beginning
```

If myvar is empty returns empty otherwise returns "zzz":
```bash
${myvar:+zzz}
```

Test if a string contains another string (Important: pattern must be at right
of `==`):
```bash
if [[ " a b c " == *" $s "* ]] ; then
    # ...
fi
```

Test with a [POSIX extended regex](https://en.wikibooks.org/wiki/Regular_Expressions/POSIX-Extended_Regular_Expressions):
```bash
if [[ $s =~ .*-[0-9]\.txt ]] ; then
    # ...
fi
```
Catch group with regex:
```bash
if [[ $s =~ .*-([A-Z0-9]+)\.txt ]] ; then
    local name="${BASH_REMATCH[1]}"
fi
```

Test if a list, whose elements are separated by spaces, contains an element:
```bash
if [[ $mylist =~ (^| )$myelem($| ) ]] ; then
    # ...
fi
```

Get the length of string variable:
```bash
str=abcd
echo ${#str}
```

Extract a substring:
```bash
str=abcd
echo ${str:1}     # "bcd"
echo ${str:1:1}   # "b"
echo ${str:1:2}   # "bc"
echo ${str: -1:0} # "c". Take last character. Space is required in order to not be confused with `:-` syntax.
echo ${str:0:-1}  # "abc". All but last character.
```

## Integers

Create sequence of integers:
```bash
echo {1..8}
echo {-7..8}
```

## Pattern expansion

To set a variable to a path with expansion:
```bash
mypath=$(echo /my/path/to/some/text/file/*.txt)
```

By default:
```bash
for f in *.txt ; do
        echo $f
done
```
will print `*.txt` literally if no `.txt` file exists.
To get an empty string when no file exists, the following option must be set:
```bash
shopt -s nullglob # make glob pattern yields empty string if no file exists.
```

## Operator !

Negates the status code of a command:
```sh
! my_command || echo "My command was successful"
```

When you set environment variables for the command, put the `!` operator before:
```sh
! MYVAR=myvalue my_command || echo "My command was successful"
```

## [[ Test operator - Double bracket

Bash defines a special double bracket `[[` operator for testing, similar to the `[` (or `test`) command line program.

Quoting of variable is not required with bash `[[` operator:
```bash
[[ -z $MYVAR ]] || exit 1
```

The logical AND and OR binary operator are the same than in C language:
```bash
[[ -z $MYVAR1 && -n $MYVAR2 ]] || exit 1
[[ -z $MYVAR1 || -n $MYVAR2 ]] || exit 1
```

## Forking

Simple forking to another program:
```bash
my_prog &
do_my_other_stuff
```

Forking with a function is the same:
```bash
my_func &
do_my_other_stuff
```

To do same tasks in parallel:
```bash
my_func & # Child
my_func   # Parent
```

Get PID of a child process:
```bash
my_child &
child_pid=$! # (when asked from inside parent)
current_pid=$$ # (the same PID value of the parent script is returned when asked either from child or parent)
parent_pid=$PPID # (parent of parent, same value returned from child or parent)
```

wait for a child to finish:
```bash
wait $pid
```

## Command line arguments

Several variables allow to access command line arguments:

Variable | Description
-------- | ------------------------------------------
`$#`     | The number of arguments.
`$*`     | 
`"$*"`   | All of the positional parameters, seen as a single word.
`$@`     |
`"$@"`   | Same as `$*`, but each parameter is a quoted string, that is, the parameters are passed on intact, without interpretation or expansion. This means, among other things, that each parameter in the argument list is seen as a separate word.

Shift command line arguments:
```bash
shift # shift by 1
shift 3 # shift by 3
```

Using `getopts` (`OPTARG` and `OPTIND` are two special variables linked with `getops`):
```bash
function read_args {
    while getopts "hm:o:" flag ; do
        case $flag in
            h) print_help ; exit 0 ;;
            m) msg=$OPTARG ;;
            o) origin=$OPTARG ;;
        esac
    done
    shift $((OPTIND - 1))
}
```
Then you can use this function as is:
```bash
read_args "$@"
```

Unfortunately `getopts` is limited to short options.
There is a unix tool (`getopt`) that is able to parse long options, however it
fails on arguments containing spaces. Moreover implementations differ between
Linux and BSD, and only the GNU version works.
A better (and more common) approach is to implement the parsing (see
<http://mywiki.wooledge.org/BashFAQ/035>):
```bash
function get_opt_val {
    if [ -z "$2" ] ; then
        printf "ERROR: \"$1\" requires a non-empty option argument.\n" >&2
        exit 1
    fi
    echo $2
}

 while true ; do
    case $1 in
        -f|--file) g_param_file=$(get_opt_val $1 $2) ; shift 2 ;;
        *) break
    esac
done
```

## Get directory of executing script

```bash
scriptdir=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
SCRIPT_NAME=$(basename "${BASH_SOURCE[0]}")
```

## Arithmetic operator `((...))`

See `ARITHMETIC EVALUATION` in `bash` man page.

Compute an equation:
```bash
((y=x*4+1))
```
or
```bash
y=$((x*4+1))
```

Rename files with numbers:
```bash
i=0 ; for f in *.JPG ; do ((i=i+1)) ; g=`printf p%03d.jpg $i` ; mv "$f" "$g" ; done
```

Compute with hexadecimal numbers:
```bash
echo $((0xa0 + 0xc56))
```

## Random numbers

Get a random number between 0 and 32767:
```bash
n=$RANDOM
```

## Random string

```bash
myRndStr=$(cat /dev/urandom | tr -dc 'A-Za-z0-9' | head -c 8)
```

## Completion

Completion configuration files are in `/usr/share/bash-completion/completions`. Example to enable Git bash completion:
```sh
source /usr/share/bash-completion/completions/git
```

## bind

To list all possible Readline functions:
```bash
bind -l
```

To list all possible Readline functions along with their key bindings:
```bash
bind -P
```

To bind C-b to beginning of line (in addition to C-a):
```bash
bind '"\C-b":beginning-of-line'
```

## case

 * [Pattern Matching](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Pattern-Matching).

Case statement:
```bash
case $var in
    4* ) echo do something ;;& # go on to next cases as if this case had not been matched
    45 ) echo case number 1 ;;
    [AaBbCc] ) echo case number 2 ;; # characters
    [1-6] ) echo case number 1 ;; # test figures range
    tata ) ;& # go on to next case and execute its action (no matching test).
    toto | titi ) echo case number 1 ;;
    * ) echo default case ;;
esac
```

## command

POSIX command.

Test if a command exists:
```bash
command -v mycmd >/dev/null 2>&1
```

## declare

List defined function:
```bash
declare -F
```

## echo

 * [Moving cursor](http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html).

Printing carriage returns:
```bash
echo -ne 'A\nB\nC\n' # Prints with carriage returns
echo $(echo -ne 'A\nB\nC\n') # Transforms carriage returns in spaces: "A B C"
echo "$(echo -ne 'A\nB\nC\n')" # Prints with carriage returns
```

Print unicode char:
```bash
echo -e '\u2500'
```
Printing a range of unicode chars:
```bash
for ((i=0x2500;i<=0x25ff;++i)); do echo -en "\u$(printf "%X" $i)" ; done
```

To print in color, use the -e option and the escape character (`\e`, `\033` or `\x1B`):
```bash
echo -e "\e[31mHello World\e[0m"
echo -e "\033[31mHello\e[0m World"
```
The escape sequence `\e[0m` disable coloring.

If the -e option does not work in the terminal (like in OS-X), use the $'' feature of bash:
```bash
echo $'\e[34mCOLORS\e[0m'
```

Dark color  | Value | Light color  | Value
----------- | ----- | ------------ | -----
Black       | 0;30  | Dark Gray    | 1;30
Red         | 0;31  | Light Red    | 1;31
Green       | 0;32  | Light Green  | 1;32
Brown       | 0;33  | Yellow       | 1;33
Blue        | 0;34  | Light Blue   | 1;34
Purple      | 0;35  | Light Purple | 1;35
Cyan        | 0;36  | Light Cyan   | 1;36
Light Gray  | 0;37  | White        | 1;37

Printing in italics:
```bash
echo $'\E[3m some text in italics \E[23m'
```
Not all terminals can handle italics. MacOS-X Terminal cannot. However iTerm2 is replacement to Terminal that handles italics.
However the terminal info bust be changed in order to achieve italics handling with iTerm2 :
  1. Choose a font in iTerm2 that provides italics.
  2. Create a file `xterm-256color-italic.terminfo` and paste the following text in it:
        xterm-256color-italic|xterm with 256 colors and italic,
        sitm=\E[3m, ritm=\E[23m,
        use=xterm-256color,
  3. run `tic xterm-256color-italic`.
  4. Modify iTerm2 settings, and set terminal to `xterm-256color-italic`.
`screen` cannot handle italics, even inside iTerm2.

## eval

`eval` is a POSIX keyword (see [POSIX man page](http://www.unix.com/man-page/posix/1posix/eval/)) which evaluates an expression, replacing variables in it.

```bash
x=12
y='$'x
echo $y # will print '$x'
```

```bash
x=12
y='$'x
eval echo $y # will print '12'
```

## export

Export a function:
```bash
export -f my_function
```

## hash

Remove cached locations of executable, thus forcing shell to look again for executables when required:
```bash
hash -r
```

## if, then, else

```bash
if [ -f $file ] ; then
    # some code...
elif [ -d $dir ] ; then
    # some other code...
else
    # ...
fi
```


## false (built-in)

Returns 0.

## for

For loop:
```bash
for f in $files ; do
    # some code...
done
```

Looping with a number:
```bash
for((i=0;i<10;++i)) ; do echo $i; done
```

Continue & break:
```bash
for f in $files ; do
    # some code...
    if ... ; then
        continue
    fi
    if ... ; then
        break
    fi
done
```

Change of character separator in a loop:
`IFS` is a built-in variable that stores the characters used to separate elements of a list. By default it is set to space, tabulation and new line, but it can be set to any value.

To loop on elements of a list separated by colon (like in `PATH`):
```bash
oldifs=$IFS
IFS=:
for p in $PATH ; do
    echo $p
done
IFS=$oldifs
```

## mapfile / readarray

Set array from input stream.

Split string:
```sh
readaray -d , -t myarr <<<"a,b,c"
```

## popd

Remove the top directory from the stack and change to the next directory:
```bash
popd
```

## pushd

Change to a directory and push it on the stack:
```bash
pushd /my/path
```

## read

 * [Bash read builtin command](https://www.computerhope.com/unix/bash/read.htm).

`read` reads words from standard input into variables:
```bash
read var1 var2 var3
```
Last variable gets all the remaining words.
Words separator is IFS.
To get full lines (including spaces at start and end), change IFS to new line
character:
```bash
IFS="\n"
read line
```

Split a string to set an array:
```bash
local -a myarr
read -ra myarr <<<"a b c"
```
`-r`: backslashes are not escaped.
`-a`: write into an array.

Read stdin line by line:
```bash
while read myvar ; do
    echo $myvar
done
```

Read line by line the output of a command using a pipe (using a subshell ==> can
not modify variables inside loop):
```bash
mycommand | while read myvar ; do
    echo $myvar
done
```
or with process substitution, no subshell:
```bash
while read myvar ; do
    echo $myvar
done < <(mycommand)
```

Read password on prompt:
```sh
echo -n 'Password: '
read -s password
echo $password
```

Reading a file inside current bash instance, line by line:
```bash
while read line ; do
    # do something with $line
done <"myfile"
```
or using a subshell (variables set inside the while loop will not be available
outside):
```bash
cat myfile | while read line ; do
    # do something with $line
done
```

Reading a variable containt:
```bash
while read line ; do
    # do something with $line
done <<< $lines
```

Read an answer:
```bash
while true ; do
    read -p "Do you wish to install this program?" yn
    case $yn in
        [Yy]*) make install ; break ;;
        [Nn]*) exit ;;
        *)     echo "Please answer yes or no." ;;
    esac
done
```

Wait for any key to be pressed:
```bash
read -n 1 -srp "Insert verso pages and press any key to continue."
```

## select

Select (user choice):
```bash
echo "Do you wish to install this program?"
select yn in "Yes" "No" ; do
    case $yn in
        Yes) make install ; break ;;
        No)  exit 0 ;;
    esac
done
```

## true (built-in)

Returns 1.

## type

Determine the type of an object:
```bash
type -t while # Returns 'keyword'.
type -t alias # Returns 'builtin'.
type -t foo # Returns 'function' if the foo() is defined, otherwise ''.
```

## while

Loop forever:
```bash
while true ; do
    if [ -z "$var" ] ; then
        break;
    fi
done
```

## Exit status

Value between 0 and 255.

Test command exist status:
```bash
if my_command ; then
    # do something on success
else
    # do something on failure
fi

if ! my_command ; then
    # do something on failure
fi

if [ $? -gt 0 ] ; then
    # an error occured
fi
```

## Functions

Define function:
```bash
function my_func {
        var1=$1
        var2=$2
}
```

Call function:
```bash
my_func arg1 arg2
my_func "$@" # calls with all arguments from command line
```

Function returning a string:
```bash
function my_func {
    echo "my string"
}
```

Function returning a status:
```bash
function my_func {
    return 1
}
```
status testing is done with `$?`.

Function returning nothing:
```bash
function foo {
    if [ -z "$myvar" ] ; then
        return
    fi

    # do something
}
```

Calling a function and testing the value returned:
```bash
if [ `my_func` = "some string" ] ; then
        echo yes
fi
```

## Evaluation

Backticks and `$()` are used to run a command and get its output:
```bash
a=`ls -1`
b=$(ls -1)
```
Bash introduced `$()` format instead of backsticks because it allows multiple nesting.

