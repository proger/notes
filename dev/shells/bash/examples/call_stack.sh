# Display the function, source filename and line number from the call stack

function foo2 {
    echo
    echo FOO 2 function
    echo "CURRENT LINE=$LINENO"
    echo "LINES=${BASH_LINENO[@]}"
    echo "FCTS=${FUNCNAME[@]}"
    echo "FILE=${BASH_SOURCE[@]}"
    local frame=0
    while caller $frame ; do
        ((frame++));
    done
}

function foo {
    echo
    echo FOO function
    echo "CURRENT LINE=$LINENO"
    echo "LINES=${BASH_LINENO[@]}"
    echo "FCTS=${FUNCNAME[@]}"
    echo "FILE=${BASH_SOURCE[@]}"
    local frame=0
    while caller $frame ; do
        ((frame++));
    done
    foo2
}

function on_exit {
    echo 
    echo EXITING
    echo STATUS=$?
    local frame=0
    while caller $frame ; do
        ((frame++));
    done
}

function failing_foo {
    echo "FAILING FOO  function"
    exit 1
}

echo
echo MAIN
echo "CURRENT LINE=$LINENO"
foo

trap on_exit EXIT
echo
echo START failing_foo test
failing_foo
echo STOP failing_foo test
trap - EXIT
