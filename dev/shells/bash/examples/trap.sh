function trap_exit {
	echo STATUS=$?
}

function foo {
	echo "Call exit"
	exit 2
}

trap trap_exit EXIT
foo
trap - EXIT
