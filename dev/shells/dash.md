# dash

Debian shell. Light shell.

## echo

To print in colors, use printf instead.

## hash

Same as bash to clean cache of executables:
```dash
hash -r
```
