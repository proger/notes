# cmd
<!-- vimvars: b:markdown_embedded_syntax={'dosbatch':''} -->

 * [DOS](https://fr.wikibooks.org/wiki/DOS).
 * [Windows Batch Scripting](https://en.wikibooks.org/wiki/Windows_Batch_Scripting).

See this [A-Z Index](http://ss64.com/nt/) for a list of all commands.

## Run

Get list of options:
```dosbatch
CMD /?
```

Run a command and exit:
```dosbatch
CMD /C my command ...
```

## Command line arguments

To get the path used to execute the script:
```dosbatch
%0
```

To know the directory of the executing script:
```dosbatch
%~dp0
```

Arguments:
```dosbatch
%1
%2
rem ...
%9
```

All parameters:
```dosbatch
%*
```

Shift the position of replaceable parameters in a batch file (internal command):
```dosbatch
shift
shift /5
```

## Current directory

Get current directory:
```dosbatch
echo %cd%
```

## Change directory

Change Directory - move to a specific Folder (internal command):
```dosbatch
cd mynewdir
```

Move to current disk root:
```dosbatch
cd \
```

Using current directory stack:
```dosbatch
pushd mytemporarydir
rem do some stuff
popd
```

## If

Test if a file or directory exists:
```dosbatch
if exist "C:\My\Path\To\A\File.txt" ...
```

Use quotes for variables, in case there are special characters like spaces:
```dosbatch
if exist "%my_file%" ...
```

Negate condition:
```dosbatch
if not exist "%my_file%" ...
```

Test variable:
```dosbatch
if "%myvar%" == "value" ...
if not "%myvar%" == "" ...
```

If/Else on DOS (version?):
```dosbatch
if exist myfile goto a
REM `else` code goes here
echo ELSE
goto endif

:then
REM `then` code goes here
echo THEN

:endif
```

If/Else on Windows batch (version?):
```dosbatch
if exist myfile (
echo THEN
) else (
echo ELSE
)
```

## Strings

When defining a string, you can escape some special characters `[()>< ]` by using caret `^`:
```dosbatch
set myvar=Some^ Value
```

## Set

`set` definition can be enclosed in quotes.
Be careful not to put space before the equal sign:
```dosbatch
set "myvar=some value"
```

## Echo

Setting command echoing off:
```dosbatch
@echo off
```

## Loop

For loop on command line:
```dosbatch
for %i in (1 2 3 4) do echo %i
```

For loop in a batch file:
```dosbatch
for %%i in (1 2 3 4) do echo %%i
```

For loop on multiple lines:
```dosbatch
for %%i in (1 2 3 4) do (
echo %%i
some_program %%i
)
```

Appending value to a variable inside a for loop doesn't work by doing it directly like:
```dosbatch
for %%f in (*.jar) do (set CLASSPATH=%CLASSPATH%;%%f)
```
It must be done by definning a function:
```dosbatch
for %%f in (*.jar) do (call :set_classpath "%%f")

:set_classpath
        (set CLASSPATH=%CLASSPATH%;%1)
:EOF
```

## Environment variables

Display variables:
```dosbatch
set
```

Changes will disappear after script finishes:
```dosbatch
SET myvar
SET myvar=somevalue
```

Permanent changing of environment variables. Changes will stay after end of script:
```dosbatch
SETX myvar myvalue
SETX myvar myvalue -m
```

## Comments

```dosbatch
rem mycomment
:: another comment
```

## Function

```dosbatch
for %%i in (1 2 3 4 5) do CALL:MYFUNC %%i
GOTO:EOF

:MYFUNC
echo %~1

:EOF
```

## Where

Find an executable in the PATH (equivalent of UNIX which command).

## Copy & xcopy

Copy one or more files to another location (internal command):
```dosbatch
COPY myfile1.txt ..\toto\myfile1_renamed.txt
```

Copy into a directory:
```dosbatch
xcopy /Y myfile1 mydir
```
`/Y` avoid prompting if destination file exists.

## Remove, delete

Remove file:
```dosbatch
DEL myfile
```

Remove a directory:
```dosbatch
RD mydir
RMDIR mydir
RMDIR /Q mydir		REM /Q: do not ask confirmation
```

Delete a directory tree:
```dosbatch
RMDIR /S /Q mydir		REM >= Windows 2000
DELTREE mydir			REM < Windows 2000
```

## Start

Start an application:
```dosbatch
START /WAIT /MIN myapp
```

