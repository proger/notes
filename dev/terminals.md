# Terminals
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

## xterm

Nice font:
```bash
xterm -fa monaco
```

Enable 16 colors:
```bash
export TERM=xterm-color
```

Enable 256 colors:
```bash
export TERM=xterm-256color
```

To set background color:
```bash
xterm -bg black
```

Enable cursor blinking:
```bash
xterm -bc
```

Set cursor color:
```bash
xterm -cr white
```

## MobaXterm

 * [Download](https://mobaxterm.mobatek.net/download.html).
 * [plugins](https://download.mobatek.net/sources/).

An X server and client for Windows, with an SSH client.
A lot of UNIX tools available as plugins.

## urxvt
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

 * [rxvt-unicode](https://wiki.archlinux.org/title/rxvt-unicode).
 * [rxvt-unicode/Tips and tricks](https://wiki.archlinux.org/index.php/Rxvt-unicode/Tips_and_tricks#Use_urxvt_as_application_launcher).
 * [Configuring URxvt to Make It Usable and Less Ugly](https://addy-dclxvi.github.io/post/configuring-urxvt/). --> How to install Perl extensions.
 * [urxvt-resize-font](https://github.com/simmel/urxvt-resize-font)

Help:
```sh
man urxvt # main manpage
man 7 urxvt # reference, FAQ
```

Keys:
 * M-s (Alt-s) to search in scrollback.
 * M-u (Alt-u) for selecting URL with keyboard (module url-select).

Packages to install on Archlinux (xsel for copy & paste, urxvt-perls for
mouseless text selection, xorg-fonts-misc for all fonts 7x14, 8x13, ...):
```sh
pacman -S xsel xclip urxvt-perls rxvt-unicode xorg-fonts-misc
yay -S urxvt-resize-font-git # Resize font on the fly
```
Restart X after installing fonts.
You can check the list of fonts with `xlsfonts`.

Installing on Debian/Ubuntu:
```sh
apt install xsel xclip rxvt-unicode xfonts-base
```

## kitty

 * Terminal able to display images.
 * Nice default font.
 * <!-- WARNING --> XCompose is not working --> solved by correction `~/.Xmodmap` with `keycode 108 = Multi_key Alt_R Meta_R Alt_R Meta_R`.

Reload config: ctrl+shift+f5

Change font:
```sh
kitten choose-fonts
```
