# perlcritic

Install:
```sh
cpanm -i Perl::Critic
```

Static code checking:
```sh
perlcritic MyModule.pm
```
