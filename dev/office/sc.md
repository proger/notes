# SC

 * [Console Spreadsheets](https://lock.cmpxchg8b.com/spreadsheet.html).

sc is a spreadsheet program. It has its own file format.

View a CSV file:
```sh
cat myfile.csv | psc -k -d , | sc
```

View a TSV file:
```sh
cat myfile.tsv | psc -k -d $'\t' | sc
```

Each cell is associated with a string (label) and an expression.

To input a label:
 * `<` for a left-justified label
 * `>` for a right-justified label
 * ..
`E` to edit an existing label.

Insert a row: `ir`.
Insert a column: `ic`.

Set format `F`:
 * `"#`: integer (0 is not printed, space is put instead)
 * `"0`: integer with 0 as 0 (not spaces).

Set range format:
 * Hit `rF`
 * Select range with arrow keys
 * Hit Tab key
 * Enter format (i.e.: `"#`)

Increase cell/column width: `f` then `h` or `l`

Move range:
 1. Hit `rm` when on destination cell (upper-left corner)
 2. Move to first cell of source range.
 2. Hit `TAB`.
 4. Select opposite corner of source range.
