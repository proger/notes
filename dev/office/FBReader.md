# FBReader

An ebook reader.

Install on Arch Linux:
```sh
pacman -S fbreader
```

Running:
```sh
FBReader mybook.epub
```
