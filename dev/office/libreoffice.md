# libreoffice

Install on Archlinux:
```sh`
yay -S libreoffice-fresh
```

For spellchecking, libreoffice uses `hunspell`. See `hunspell` for installing
different dictionaries.

Drawing a graph of repartitions of marks:
 * Create a column with bins (ex: 2, 4, 6, 8, 10, 12, 14, 16, 18, 20).
 * Inside the cell at the right of the first bin value (ex: 2), insert the formula: `{=FREQUENCY(<marks_range>, <bins_range>)}`.
 * `<marks_range>` is the range containing marks, example: `A2:A40`, and `<bins_range>` is the range containg bin values (ex.: `A50:A59`).
