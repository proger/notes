# pcal

Generate a calendar in postscript format.

Generate month January 2022 on one page in A4 format:
```sh
pcal -P a4 01 2022 | ps2pdf - mycal.pdf
```
