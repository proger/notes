# psc

Convert a file to sc format.

Example:
```sh
cat myfile.csv | psc -k -d, | sc
```
