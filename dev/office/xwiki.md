# Xwiki

 * [Xwiki](https://www.xwiki.org/xwiki/bin/view/Main/WebHome).

To insert a reference to another page, copy the reference of the page that you
can find in the "Information" tab of the page (bottom of the page).
Then insert the following code (source):
`[[Some Title>>The.Reference.To.The.Page]]`.
