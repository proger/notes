# LaTeX
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':'','sh':''} -->

 * [Code listing](https://www.overleaf.com/learn/latex/Code_listing).
 * [CTAN](http://www.ctan.org/).

Presentation:
 * [How to Make a Presentation in LaTeX](https://www.lucidchart.com/techblog/2016/12/07/how-to-make-a-presentation-in-latex/).
 * [LaTeX Templates for presentations](https://www.latextemplates.com/cat/presentations).
 * [LaTeX/Presentations](https://en.wikibooks.org/wiki/LaTeX/Presentations).

## Installing

On ArchLinnux:
```sh
pacman -S texlive-meta
```

On Ubuntu/Debian:
```sh
apt install -y texlive # Selection of most used packages
apt install -y texlive-full
apt install -y texlive-lang-all
```

## Vim support

In LaTeX syntax highlighting, force to restart the syntax highlighting engine at some line:
```tex
%stopzone
```

## Compiling

To produce a DVI:
```sh
latex myfile.tex
```

To produce a PDF:
```sh
pdftex myfile.tex
```

To explicity set the output format:
```sh
pdftex -output-format pdf myfile.tex
pdftex -output-format dvi myfile.tex
```

`pdflatex` compiles a LaTeX input file into a PDF  file. It takes the same options as pdftex.

To get `pdftex` and `pdflatex`, install `texlive-bin` on Archlinux.

Some files are produced during the process and can be deleted afterwhile:
	.log .aux .out .toc
	
To produce references (images, table of contents (.toc), ...) and for some
tables, run command twice:
```sh
pdflatex myfile.tex
pdflatex myfile.tex
```

## Organisation d'un document

Un certain nombre de commandes permettent d'organiser les documents LaTeX. Vous pouvez en particulier organiser le document en différentes parties et sous-parties grÃ¢ce aux commandes suivantes :
```tex
\part{titre}, \chapter{titre}, \section{titre}, \subsection{titre}, \subsubsection{titre}, \paragraph{titre}, \subparagraph{titre}.
```

Ces commandes ne sont pas toutes disponibles pour toutes les classes d'article
et ont un rendu parfois différent en fonction des classes (espacement
différent entre autres). Par exemple, la commande \chapter n'est pas prise en
compte par des documents de classe article, mais l'est pour ceux de classe
report. En plus de la création de titres pour chaque partie, ces commandes
sont utilisées par LaTeX pour la création de la table des matières. Si
toutefois vous ne souhaitez pas voir apparaître le titre d'une partie dans la
table des matières, il suffit de faire précéder le titre d'un astérisque.
Par exemple :
```tex
\subsection*{titre}
Afin d'afficher la table des matières dans le document LaTeX final, il suffit
d'entrer la commande \tableofcontents après la commande \begin{document}
```

Il n'y a pas de saut de ligne automatique après un élément paragraph. Le
saut de ligne s'effectue après l'élément qui suit. On rajoute une mbox vide
après le paragraphe.
```tex
\paragraph{mytitle}\mbox{}
```

## Items list

Write a list:
```tex
\begin{itemize}
	\item First element.
	\item Second element.
	\item Third element.
\end{itemize}
```

Remove line spacing:
```tex
\begin{itemize}
	\setlength{\itemsep}{0pt}
	\setlength{\parskip}{0pt}
	\setlength{\parsep}{0pt}
	\item First element.
	\item Second element.
	\item Third element.
\end{itemize}
```

Enumeration:
```tex
\begin{enumerate}
	\item First element.
	\item Second element.
	\item Third element.
\end{enumerate}
```

Use other style for numbering:
```tex
\begin{enumerate}[I] % Roman
\begin{enumerate}[i] % Roman
\begin{enumerate}[A]
\begin{enumerate}[a]
\begin{enumerate}[(a)]
```

Description type:
```tex
\begin{description}
	\item[First] First element.
	\item[Second] Second element.
	\item[Third] Third element.
\end{description}
```

## Font

Font size:
```tex
\tiny
\scriptsize
\footnotesize
\small
\normalsize
\large
\Large
\LARGE
\huge
\Huge
```

Usage:
```tex
\tiny some tiny text
{\tiny some tiny text}
\begin{tiny}some tiny text\end{tiny}
```

The following declarations let you choose between three pre-defined font
families:
```tex
\rmfamily % selects a roman (i.e., serifed) font family
\sffamily % selects a sans serif font family
\ttfamily % selects a monospaced (âtypewriterâ) font family

% Within each font family, the following declarations select the âseriesâ (i.e., dark- ness or stroke width),
\mdseries regular
\bfseries bold
% and the âshapeâ (i.e., the form of the letters):
\upshape upright
\slshape slanted
\itshape italic
\scshape Caps and Small Caps
```

For each declaration there exists a text- generating command as a counterpart;
it typesets only its argument in the de- sired style:
```tex
\textsf % corresponds to \sffamily.
```

Changing default font families for the whole document:
The families selected by \rmfamily, \sffamily and \ttfamily are determined by
the values of the related macros \rmdefault, \sfdefault and \ttdefault.
```tex
\renewcommand{\rmdefault}{ptm}	% ptm is the name of the font family "Times".
```

Setting default font for the whole document:
```tex
\renewcommand{\familydefault}{\sfdefault}
```

## \\\\\, \\newline, \\newpage, ...

```tex
\\              % Start a new paragraph.
\\*             % Start a new line but not a new paragraph.
\-              % OK to hyphenate a word here.
\cleardoublepage% Flush all material and start a new page, start new odd
                % numbered page.
\clearpage      % Plush all material and start a new page.
\hyphenation    % Enter a sequence of exceptional hyphenations.
\linebreak      % Allow to break the line here.
\newline        % Request a new line.
\newpage        % Request a new page.
\nolinebreak    % No line break should happen here.
\nopagebreak    % No page break should happen here.
\pagebreak      % Encourage page break.
```

## \\author

Must be set before `document` environment.

Specify institution or firm for author: use \\ to go to next line:
```tex
\author{Pierrick Roger Mele\\CEA / LIST / DCSI / LOAD}
```

Specify multiple authors:
```tex
\author{first author \and second author}
```

Make foot note in \author or \title (use \thanks command):
```tex
\author{first author \thanks{email@address.com}}
```

## \\bf

Toggles bold:
```tex
\bf mytext
```

## \\cite

Cite a reference:
```tex
\cite{myref}
```

## \\def

Declaration:
```tex
\def \myvar {some text}
```

Usage:
```tex
Some text with \myvar inside.
```

## \\documentclass

Class   | Description
------- | --------------------------------
article | Permet de créer des documents courts, comme des comptes-rendus de travaux ou de réunions, par exemple.
report  | Est utilisée pour les documents plus longs, car elle gère les chapitres,ce que ne fait pas la classe article.
book    | Permet d'écrire des livres et est très similaire à la classe report.
letter  | Sert à écrire des courriers.
slides  | Permet de créer des transparents.

### article

Set document class and paper size:
```tex
\documentclass[a4paper]{article}
```

The following solution only changes the page content to landscape, but not the
actual page layout:
```tex
\documentclass[a4paper,landscape]{article}
```

### letter

```tex
\documentclass[11pt]{letter}

\name{}% Nom de l'expéditeur
\address{}% Adresse de l'expéditeur
\signature{}% Signature de l'expéditeur 
\date{}

\begin{document}
\begin{letter}{}% Nom du destinataire
\opening{}% Formule de salutation : cher monsieur, etc.

% Corps de la lettre

\closing{}% Formule de politesse : veuillez agréer, etc.
\ps{}% Post-scriptum
\cc{}% Autres destinataires de la lettre
\encl{}% Pièces jointes
\end{letter}
\end{document}
```

### slides

```tex
\documentclass[a4paper]{slides}

\title{ELMABIO \\ Environnement logiciel pour le développement}
\author{Pierrick ROGER MELE}
\date{13 mars 2012}

\pagestyle{empty}	% turns off page numbering

\begin{document}

\maketitle

\begin{slide}
\end{slide}

\end{document}
```

## \\em

Toggles italic:
```tex
Normal \em italic \em normal
```

## \\emph

Set text in italic:
```tex
\emph{mytext}
```

## \\hspace

To indent at beginning of a line (works inside tabular):
```tex
\hspace*{1em}
```

## \\ifnum

Test a numeric value:
```tex
\ifnum\thevalue=2\dosomething\fi
\ifnum\thevalue>2\dosomething\fi
\ifnum\thevalue<2\dosomething\fi
```

## \\indent
	
To indent at beginning of a line:
```tex
\indent
```

## \\include

`\include{filename}` essentially does a `\clearpage` before and after
`\input{filename}`, together with some magic to switch to another .aux file,
and omit the inclusion at all if you have an `\includeonly` without the filename
in the argument. This is primarily useful when you have a big project on a slow
computer; changing one of the include targets won't force you to regenerate the
outputs of all the rest

`\include` gets you the speed bonus, but it also can't be nested, can't appear
in the preamble, and forces page breaks around the included text.

```tex
\include{filename}
```

## \\input

`\input{filename}` imports the commands from filename into the target file;
it's equivalent to typing all the commands from filename right into the current
file where the `\input` line is.

Input content of a (TeX) file:
```tex
\input{"myfile"} % look for myfile.tex or myfile
\input{"myfile.txt"}
```

Input output of shell command (needs to compile with `latex --shell-escape`):
```tex
\input{|"ls"}
```

### Use \input to create sub-documents

Main file:
```tex
\documentclass[a4paper,11pt]{article}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[normalem]{ulem}

\title{Foo}
\author{Me}
\date{}

\begin{document}

\maketitle \clearpage
\tableofcontents \clearpage
\input{foo1}
\input{foo2}
\end{document}
```

`foo1.tex` file:
```tex
\section{Foo1}
Some text.
\clearpage
```

`foo2.tex` file:
```tex
\section{Foo2}
Some text.
\clearpage
```

## \\label

Define a anchor inside an object, to be referenced later on using `\ref{}` for
the index/number of the object, `\pageref{}` for the page number or
`\nameref{}` (see package `nameref`) for the title:
```tex
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[width=\textwidth]{filename2}
\caption{default}
\label{fig:figure2}
\end{minipage}
```

## \\maketitle

Use it inside `document` environment to generate the defined title:
```tex
\maketitle
```

## \\multido

```tex
\multido{\n=1.0+0.5,\i=1+3,\nX=0.4+1.1}{12}{%
	%...
}
```

First letter indicates variable type:
 * d/D   dimension
 * n/N   number (initial value and increment must have the same number of digits to the right of the decimal, however initial value can always be an integer).
 * i/I   integer
 * r/R   real (initial value and increment must be integers or numbers with 4 digits on each side of the decimal).

## \\newcommand

Defining a new command:
```tex
\newcommand...
```

A command can accept up to 9 parameters refered to with #1...#9 notations.
```tex
\newcommand{\myparagraph}[1]{\paragraph{#1}\mbox{}}
\myparagraph{some text}
```

Multiple args:
```tex
\newcommand{\myparagraph}[3]{\paragraph{#1 #2 #3}\mbox{}}
\myparagraph{some text}{another text}{a text again}
```

Optional args:
```tex
\usepackage{xargs}
\newcommandx*\coord[3][1=1, 3=n]{(#2_{#1},\ldots,#2_{#3})}
```

Key/val args:
```tex
\usepackage{keyval}

% definition:
\define@key{resumeheader}{title}{\centerline{\Huge \bf #1}}
\define@key{resumeheader}{name}{{\huge #1}}
\newcommand*\resumeheader[1]{\setkeys{resumeheader}{#1}}

% usage:
\resumeheader{name=Pierrick,title=Architecte Logiciel}
% which is different from:
\resumeheader{title=Architecte Logiciel,name=Pierrick}
% since the order of definition will be the order of evaluation
```

## \\newcounter

Declare a new counter variable:
```tex
\newcounter{mycounter}
```

Use the counter:
```tex
The value of the counter is \themycounter.
```

## \\newenvironment

```tex
\newenvironment{txlAlgo}[1]{\begin{algorithmic}}{\end{algorithmic}}
```

## \\newif

```tex
% First the definition of the condition:
\newif\iftoto \totofalse        % iftoto will be false
% or
\newif\iftoto \tototrue        % iftoto will be true

% then the use of the condition:
\iftoto
	% do something
\else
	% do something else
\fi
```

## \\nocite

???

## \\pagestyle and \thispagestyle

To set page numbering off:
```tex
\begin{document}
\maketitle
\thispagestyle{empty} % turn off page numbering. It needs to be made after \maketitle, since it sets it on.
\pagestyle{empty}
```

## \\providecommand

Defining a command only if it doesn't already exist:
```tex
\providecommand...
```

## \\ProvidesPackage

Creates a package.

Filename must be the package name: `mypackage.sty`.

First lines are:
```tex
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{mypackage}[2013/03/05 Bla bla bla]
```

## \\raggedright

Avoid justification to the right:
```tex
\raggedright
```

## \\renewcommand

Redining an existing command:
```tex
\renewcommand...
```

## \\section\*

To make a section without numbering:
```tex
\section*{name}
```

## \\setcounter

```tex
To set level of numbering:
\setcounter{secnumdepth}{0}
```
0 = chapter, -1 = part, 1 = section ...

## \\stepcounter

Increment a counter:
```tex
\stepcounter{mycounter}
```

## \\tableofcontents

Set TOC depth (optional):
```tex
\setcounter{tocdepth}{1}
```

Insert TOC:
```tex
\tableofcontents
```

## \\textbf

Set text in bold:
```tex
\textbf{mytext}
```

## \\textit

Set text in italic:
```tex
\textit{mytext}
```

## \\textsc

Put in small capitals:
```tex
\textsc{My Text}
```

## \\textsubscript

Put in subscript:
```tex
T\textsubscript{0}
```

## \\title

Define the title (to be used before `document` environment):
```tex
\title{Rapport sur la faisabilité d'une connexion TRNSYS/CONTAM}
\author{Pierrick Roger Mele}
\date{30 novembre 2011}
\begin{document}
	\maketitle
```

## \\underline

Underline:
```tex
\underline{mytext}
```

## \\usepackage

```tex
\usepackage{mypackage}
```

Package search path:
Set env varTEXINPUTS to .:/my/path/to/sty/files to tell TeX to search into all
sub-tree of a path, terminate it with // :
```sh
export TEXINPUTS=.:/my/path/to/sty/files//
```
Note: it seems that setting TEXINPUTS removes current standard directories from
the search path.

Use `kpsepath` to get default TeX path:
```sh
export TEXINPUTS=$(kpsepath tex):/My/Path
```

## \\verb

Display some words in monospaced font:
```tex
My sentence with \verb|monospace text|.
```

To type text that won't be interpreted by the compiler:
```tex
Bla bla bla \verb@%JAVA_HOME%@
```
`\verb` doesn't let you use the font formating (bold, italic, ...).

## \\write

Writes to a file stream.

The stream 18 is the operating system shell.
Example of running `ls` and getting its output:
```tex
\immediate\write18{ls}
```
The `\immediate` command tells TeX to execute `\write` immediately.
The default behaviour of `\write` is wait for the page being shipped before
executing.

## itemize

 * [Unordered lists](https://www.overleaf.com/learn/latex/Lists#Unordered_lists).

```tex
\begin{itemize}
	\item My first item
	\begin{itemize}
		\item A subitem.
	\end{itemize}
	\item My second item
\end{itemize}
```

## minipage

```tex
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[width=\textwidth]{filename2}
\caption{default}
\label{fig:figure2}
\end{minipage}
```

## tabular

Create a two columns table:
```tex
\begin{tabular}{l c}
\end{tabular}
```
The parameters are the columns types:
* c = centered
* l = left align
* r = right align
* p{width} = paragraph = fixed width column justified (width examples: 3cm, 10em}

For drawing vertical lines, use character `|`:
```tex
\begin{tabular}{|l|c|}
\end{tabular}
```

For drawing an horizontal line:
```tex
\hline
```

To remove left and right margins:
```tex
\begin{tabular}{@{}lc@{}}
```

Merging columns:
```tex
\multicolumn{2}{l}{\bf Blabla}
```

Merging rows:
```tex
\usepackage{multirow}
\multirow{2}{*}{Blabla}
```

The tabular object needs to be included inside a table object for placement on
the page and referencing:
```tex
\begin{table}
	\begin{tabular}{cc}
	% ...
	\end{tabular}
\end{table}
```
Row & column spacing:
```tex
\begin{table}%
	\setlength{\tabcolsep}{6pt}% General space between cols (6pt standard)
	\renewcommand{\arraystretch}{1}% General space between rows (1 standard)
	% Add @{\hskip Xpt} to tabular to add space between certain columns
	% Add [Xpt] after \\ of a certain row to add extra space before next row
	\begin{tabular}{c@{\hskip 12pt}c}%
	% table content
	\end{tabular}%
\end{table}%
```

## tabbing

Use `\=` for marking a tab stop.
Use `\>` to move to next tab stop.
`\+` causes left margin of subsequent lines to be indented one tab stop to the
right, just as if a `\>` command were added to the beginning of subsequent lines.
`\-` undoes the effect of a previous `\+`.
`\'` indents text flush right against the next tab stop.

Example:
```tex
\begin{tabbing}%
	If \= it's raining \\
	\> then \= put on boots,\\
	\> \> take hat;\\
	\> or \> smile. \\
	Leave house.
\end{tabbing}%
```

## verbatim

Write in code in monospaced font:
```tex
\begin{verbatim}
All text is written as is.
\end{verbatim}
```

## verbatim\*

Write in code in monospaced font with emphasized spaces:
```tex
\begin{verbatim*}
All text is written as is.
\end{verbatim}
```

## Paragraph, line breaking, blank line, ...

 * [LaTeX Line and Page Breaking](http://www.personal.ceu.hu/tex/breaking.htm).
 * [Line breaks and blank spaces](https://www.overleaf.com/learn/latex/Line_breaks_and_blank_spaces).

End a line for switching to next paragraph:
```tex
My first paragraph\\
My second paragraph

```

```tex
My first paragraph\\
\bigskip
My second paragraph
```

Other space sizes:
```tex
\vspace{\baselineskip}
\vspace{2ex}
\medskip
\smallskip
[2ex]
```

## Special characters

Special characters:
	# $ % & ~ _ ^ \ { }
Backslash must be written:
```tex
$\backslash$
```
The other characters must be backslashed:
```tex
\# \$ \% \& \~ \_ \^ \{ \}
```

 * [Special Characters](http://www.emerson.emory.edu/services/latex/latex_155.html).
 * [LaTeX/Special Characters](https://en.wikibooks.org/wiki/LaTeX/Special_Characters).

In text mode:
```tex
\textasciicircum	% For ^
\^{}				% For ^
\textbackslash
\textasciitilde
\textunderscore
\%
```

The tilde is an unbreakable space:
```tex
Figure~\ref{fig:myfigure}
```

Quoting:
```tex
``Yes.''
```

Ellipsis:
```tex
\ldots
```

Euro symbol from current font:
```tex
\usepackage[gen]{eurosym}
\euro{}
```

Official Euro symbol:
```tex
\usepackage[official]{eurosym}
\euro{}
```

Degree symbol:
```tex
$^{\circ}$
```
or
```tex
\usepackage{textcomp}
\textdegree
```
or
```tex
\usepackage{gensymb}
\degree
```
```tex
\usepackage{amsmath}
\usepackage{siunitx}

A $\SI{45}{\degree}$ angle.

It is \SI{17}{\degreeCelsius} outside.
```

Tilde:
```tex
\~
```
or
```tex
\textasciitilde
```

Colon:
```tex
A: B       % put a space before the colon
A\colon B  % no space before the colon
```

Trademark symbol:
```tex
\textregistered
```

To type utf8 inside tex file:
```tex
\usepackage[utf8]{inputenc}
```

## References

Il est possible d'utiliser des références à des objets déjà définis. La
commande \label{nom de la référence} permet de définir la référence, la
commande \ref{nom de la référence} permet d'utiliser le numéro de la
référence, enfin \pageref{nom de la référence} permet de faire appel au
numéro de page de la référence. Afin d'utiliser une numérotation relative
aux figures, sections (et autres parties) et tableaux, le nom de référence
doit s'écrire de la faÃ§on suivante :
```tex
{fig:nom de la figure} % pour une figure
{tab:nom du tableau} % pour un tableau
{section:nom de la section} % pour une section
{subsection:nom de la subsection} % pour une sous-section
```

## BibTeX

 * [Bibtex Entry Types, Field Types and Usage Hints](https://www.openoffice.org/bibliographic/bibtex-defs.html).

See `xampl.bib` for examples. Example of location: `/usr/share/texmf-dist/bibtex/bib/base/xampl.bib`.

Additional fields:
```bib
@misc{mytag,
	version = {},
	url = {url1},
	url = {url2}
}
```

Article:
```bib
@article{mytag,
% Required fields:
	author = {LastName1, FirstName1 and LastName2, FirstName2 and others}, % "and others" will be translated into "et al."
	title = {},
	journal = {},
	year = {},
% Optional fields:
	volume = {},
	number = {},
	pages = {},
	month = {},
	note = {} 
}
```

A document having an author and title, but not formally published:
```bib
@unpublished{mytag,
%  Required fields:
	author = {},
	title = {},
	note = {},
%  Optional fields:
	month = {},
	year = {}
}
```

The proceedings of a conference:
```bib
@proceedings{mytag,
% Required fields:
	title = {},
	year = {},
%  Optional fields:
	editor = {},
	volume = {}, % one of volume or number.
	number = {},
	series = {},
	address = {},
	month = {},
	organization = {},
	publisher = {},
	note = {}
```

Article in conference proceedings:
```bib
@inproceedings{mytag, % Synonym: @conference. For Scribe compatibility.
% Required fields:
	author = {},
	title = {},
	booktitle = {},
	year = {},
% Optional fields:
	editor = {},
	volume = {}, % one of volume or number.
	number = {},
	series = {},
	pages = {},
	address = {},
	month = {},
	organization = {},
	publisher = {},
	note = {}
}
```

Technical report (A report published by a school or other institution, usually numbered within a series):
```bib
@techreport{mytag,
% Required fields:
	author = {},
	title = {},
	institution = {},
	year = {},
% Optional fields:
	type = {},
	number = {},
	address = {},
	month = {},
	note = {}
```

Book:
```bib
@book{mytag,
% Required fields:
	author = {}, % One of author or editor, or both.
	editor = {},
	title = {},
	publisher = {},
	year = {},
% Optional fields:
	volume = {}, % One of volume or number.
	number = {},
	series = {},
	address = {},
	edition = {},
	month = {},
	note = {}
}
```

Technical documentation:
```bib
@manual{mytag,
% Required field:
	title = {},
%  Optional fields:
	author = {},
	organization = {},
	address = {},
	edition = {},
	month = {},
	year = {},
	note = {}
}
```

A Master's thesis:
```bib
@mastersthesis{mytag,
% Required fields:
	author = {},
	title = {},
	school = {},
	year = {},
%  Optional fields:
	type = {},
	address = {},
	month = {},
	note = {}
}
```

A PhD thesis:
```bib
@phdthesis{mytag,
% Required fields:
	author = {},
	title = {},
	school = {},
	year = {},
%  Optional fields:
	type = {},
	address = {},
	month = {},
	note = {}
}

A part of a book, which may be a chapter (or section or whatever) and/or a range of pages:
```bib
@inbook{mytag,
% Required fields:
	author = {}, % One of author or editor, or both.
	editor = {},
	title = {},
	chapter = {}, % One of chapter of pages, or both.
	pages = {},
	publisher = {},
	year = {},
%  Optional fields:
	volume = {}, % one of volume or number.
	number = {},
	series = {},
	type = {},
	address = {},
	edition = {},
	month = {},
	note = {}
}
```

A part of a book having its own title:
```bib
@incollection{mytag,
% Required fields:
	author = {},
	title = {},
	booktitle = {},
	publisher = {},
	year = {},
% Optional fields:
	editor = {},
	volume = {}, % one of volume or number.
	number = {},
	series = {},
	type = {},
	chapter = {},
	pages = {},
	address = {},
	edition = {},
	month = {},
	note = {}
}
```

Booklet (A work that is printed and bound, but without a named publisher or sponsoring institution):
```bib
@booklet{mytag,
% Required field:
	title = {},
% Optional fields:
	author = {},
	howpublished = {},
	address = {},
	month = {},
	year = {},
	note = {}
```

When nothing else fits:
```bib
@misc{mytag,
% Optional fields:
	author = {},
	title = {},
	howpublished = {},
	month = {},
	year = {},
	note = {}
}
```

Defining a bibtex element in LaTeX:
```tex
\begin{thebibliography}{9} % The number is the maximum number of references which will be used to determine the width (in number of characters) of the label.

\bibitem{lamport94}
  Leslie Lamport,
  \emph{\LaTeX: A Document Preparation System}.
  Addison Wesley, Massachusetts,
  2nd Edition,
  1994.

\end{thebibliography}
```

Printing whole bibliography in a PDF:
```tex
\documentclass{article}
\usepackage[style=authoryear,backend=bibtex]{biblatex} %backend tells biblatex what you will be using to process the bibliography file
\bibliography{bib}

\begin{document}

\nocite{*}
\printbibliography

\end{document}
```

## Maths

Subscript:
```tex
$n_0$
```

## Errors

	! LaTeX Error: File `forloop.sty' not found.
Run the following command as ROOT after installing any new package inside `<root>/share/texmf/tex/latex`:
```sh
sudo texhash
```

