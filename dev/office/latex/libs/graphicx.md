# graphicx

Not available in slides or letters document classes.

```tex
\usepackage{graphicx}
```

With LaTeX, only .eps files are allowed.

When generating with pdflatex, allowed files are PNG, GIF, JPG and PDF.
EPS files are forbidden.

```tex
\usepackage[pdftex]{graphicx}
```

```tex
\includegraphics[width=\textwidth]{image.png}
```

Insert a figure:
```tex
\begin{figure}[h]
	\begin{center}
		\includegraphics[width=\textwidth]{figures/couplage_fort.eps}
		\caption{Principe du couplage fort \label{fig:couplage_fort}}
	\end{center}
\end{figure}
```

Options:
```tex
\begin{figure}[h]% place figure here. TeX tries to arrange text the best it can, and may move the figure to another page.
\begin{figure}[!h]% figure must be placed here. TeX will never move the figure to another place.
```
List of options:
Code | Description
---- | ----------------
h    | Here.
t    | Top of a text page.
b    | Bottom of a text page.
p    | Alone on a text page.
!    | Insist on respecting instructions.

Make a reference to a figure:
```tex
\ref{fig:couplage_faible}
```

Change width of a graphic in percentage of text width:
```tex
\includegraphics[width=\textwidth]{figures/couplage_fort.eps}
\includegraphics[width=0.4\textwidth]{figures/couplage_fort.eps}
```

Change scale of a graphic:
```tex
\includegraphics[scale=1]{figures/couplage_fort.eps}
```

Putting two figures side by side:
```tex
\begin{figure}[ht]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[scale=1]{filename1}
		\caption{default}
		\label{fig:figure1}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[scale=1]{filename2}
		\caption{default}
		\label{fig:figure2}
	\end{minipage}
\end{figure}
```

