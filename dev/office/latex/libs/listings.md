# listings
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

 * [The Listings Package](http://texdoc.net/texmf-dist/doc/latex/listings/listings.pdf).
 * [LaTeX/Source Code Listings](https://en.wikibooks.org/wiki/LaTeX/Source_Code_Listings).

Set language globally:
```tex
\usepackage{listings}
\begin{document}
	\lstset{language=Pascal}
```

Insert code inline:
```tex
\usepackage{listings}
\lstinline!var i:integer;!
```

Insert block of code:
```tex
\usepackage{listings}
\begin{lstlisting}
for (int i=0; i < 10 ; ++i)
	dosomething(i);
\end{lstlisting}
```

Set up language for
```tex
\usepackage{listings}
\begin{lstlisting}[language=C]
for (int i=0; i < 10 ; ++i)
	dosomething(i);
\end{lstlisting}
```

Use a reference:
```tex
\begin{lstlisting}[label=lst:biodbInst,caption=Library initialization and
	termination,float,frame=tb, language=R]
mybiodb <- biodb::newInst()
# ...
mybiodb$terminate()
\end{lstlisting}
```
