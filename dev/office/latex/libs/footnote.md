# footnote

Footnotes in tables:
```tex
\usepackage{footnote}
\begin{savenotes}
	\begin{table}[h!]
		\begin{tabular}{lll}
			blabla & klgkg\footnote{some note} & fdhbjh \\
		\end{tabular}
	\end{table}
\end{savenotes}
```
