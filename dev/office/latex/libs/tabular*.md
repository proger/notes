# tabular\*
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

Full-width with tabular\*
works by expanding inter-columns space
```tex
\begin{tabular*}{\textwidth}{c @{\extracolsep{\fill}} ccccc}
% ...
\end{tabular*}
```
