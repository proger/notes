# verbatim
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

Reimplements the LaTeX verbatim and verbatim\* environments.
