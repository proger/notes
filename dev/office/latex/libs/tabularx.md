# tabularx
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

Full-width tabular with tabularx.
Works by expanding the X columns.
By default all X columns have the same size.
```tex
\usepackage{tabularx}
\begin{tabularx}{\textwidth}{|l|X|}
	blabla & \centerline{tagazoo}
\end{tabularx}
```

Attention when using tabularx in an new environment, you must use the command form of tabularx:
```tex
\newenvironment{resknowhow}
{\tabularx{\textwidth}{|lX|}} % before
{\endtabularx} % after
```
