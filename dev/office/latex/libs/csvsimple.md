# csvsimple
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

 * [The csvsimple package](https://distrib-coffee.ipsl.jussieu.fr/pub/mirrors/ctan/macros/latex/contrib/csvsimple/csvsimple.pdf).
 * [Importing csv file into latex as a table](https://tex.stackexchange.com/questions/146716/importing-csv-file-into-latex-as-a-table).

```tex
\usepackage{csvsimple}

\begin{document}
    \begin{tabular}{l|c}%
    \bfseries Person & \bfseries Matr.~No.% specify table head
    \csvreader[head to column names]{grade.csv}{}% use head of csv as column names
    {\\\hline\givenname\ \name & \matriculation}% specify your coloumns here
    \end{tabular}
\end{document}
```
