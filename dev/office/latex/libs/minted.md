# minted

Format and highlight programming language source code.

```tex
\usepackage{minted}
\begin{minted}{language}
	code
\end{minted}
```
