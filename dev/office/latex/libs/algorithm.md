# algorithm
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

```tex
\usepackage{algorithm}
```

Setting caption, making reference and putting in TOC:
```tex
\begin{algorithm}[H]% H option is required in slide/beamer/powerdot document.
	\caption{#1}%
	\label{alg1}%
	\begin{algorithmic}[1]%
		%...
	\end{algorithmic}%
\end{algorithm}%
```

## Comments

Use `\COMMENT` command at the end of a statement line:
```tex
\begin{algorithmic}%
	\STATE $s \gets 0$ \COMMENT{Initialize to zero.}%
\end{algorithmic}%
```

Statements as `FOR` and `WHILE` use an option for comment, since their special construct doesn't allow the use of `\COMMENT` command:
```tex
\FOR[my comment]{$i \gets 1$ \TO $n$}
	%...
\ENDFOR
```
