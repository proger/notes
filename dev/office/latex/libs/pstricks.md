# pstricks

```tex
\usepackage{pstricks}
```

Axes:
```tex
\usepackage{pst-plot}
\begin{pspicture}(0,0)(5,5)
	\psaxes[linewidth=1.2pt,labels=none,ticks=none]{<->}(2,1)(0,0)(4,3)
\end{pspicture}
```

Line:
```tex
\begin{pspicture}(0,0)(5,5)
	\psline[linestyle=dashed]{}(2,0)(2,2.9) % n0 dashed line
\end{pspicture}
```

Curve:
```tex
\begin{pspicture}(0,0)(5,5)
	\pscurve(0,0)(1.5,3)(2,2.9)(4,4)(5,4.5) % c2.g(n)
\end{pspicture}
```

Writing text at point:
```tex
\begin{pspicture}(0,0)(5,5)
	\rput[t](2,0){n0}
	% First option is placement relative to point:
	% Horizontal        Vertical
	% l Left            t Top
	% r Right           b Bottom
	%                   B Baseline
\end{pspicture}
```

Writing text at distance from a point:
```tex
\begin{pspicture}(0,0)(5,5)
\end{pspicture}
```
