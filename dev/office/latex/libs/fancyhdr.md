# fancyhdf

```tex
\usepackage{fancyhdr}

% Set on
\pagestyle{fancy}

% Erase default style
\fancyhead{}
\fancyfoot{}

% Set own style
% E: Even page
% O: Odd page
% L: Left field
% C: Center field
% R: Right field
% H: Header
% F: Footer
\fancyhead[RO,RE]{Some text}
\fancyfoot[C]{Confidential}
\fancyfoot[RO, LE] {\thepage}

% Setting thickness of decorative lines
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}
```
