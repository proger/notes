# xcolor
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

 * [Extending LATEX's color facilities: the xcolor package](https://mirrors.chevalier.io/CTAN/macros/latex/contrib/xcolor/xcolor.pdf).
 * [LATEX Color](http://latexcolor.com/), a list of some color useful color definitions to copy & paste.

```tex
\usepackage{xcolor}
```

Defining colors:
```tex
\definecolor{bannerLight}{RGB}{255, 0, 0}
\definecolor{bannerDark}{HTML}{B00000}
\definecolor{mycolor}{rgb}{1.0, 0.8, 0.5}
```

From an existing color:
```tex
\colorlet{tableheadcolor}{gray!25}
```

Use colors in text:
```tex
\textcolor{mycolor}{my text}
```
