# alltt
	
`alltt` is a verbatim environment that allows bold or italic:
```tex
\usepackage{alltt}
\begin{alltt}
Bla bla \emph{emphasize} words.
\end{alltt}
```
For bold text, use `\textbf{}`.
However note that the default font has no bold chars, so you must change it:
```tex
\renewcommand{\ttdefault}{txtt}
```
