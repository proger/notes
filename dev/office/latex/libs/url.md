# url

www reference:
```tex
\usepackage{url}
\urlstyle{sf}
...
\url{http://hostname/~username}
```

Mail reference:
```tex
\documentclass[a4paper,english,12pt]{article}
\usepackage{hyperref}
\newcommand{\email}[1]{\href{mailto:#1}{\nolinkurl{#1}}}
\begin{document}

\email{moi@web.com}

\end{document}
```
