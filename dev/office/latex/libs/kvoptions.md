# kvoptions

Defines options for package.

Inside the package:
```tex
\RequirePackage{kvoptions}
\DeclareStringOption{lang}[french]
\ProcessKeyvalOptions*
\PassOptionsToPackage{\mypackage_name@lang}{babel}
```

From the TeX file:
```tex
\usepackage[lang=french]{mypkg}
```
