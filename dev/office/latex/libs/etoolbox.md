# etoolbox

`\iftoggle` command:
```tex
\usepackage{etoolbox}

\newtoggle{paper}
% which is set with either
\toggletrue{paper}
\togglefalse{paper}

% And to use it:
\iftoggle{paper}{%
	  % using paper
}{%
	  % electronic
}
```
