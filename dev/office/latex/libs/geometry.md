# geometry

To set custom paper size:
```tex
\usepackage[a4paper]{geometry}
```
or
```tex
\usepackage[paperwidth=5.5in, paperheight=8.5in]{geometry}
```

Set margins:
```tex
\usepackage[top=length, bottom=length, left=length, right=length]{geometry}
```

Set the whole document in landscape:
```tex
\usepackage[landscape]{geometry}
```
