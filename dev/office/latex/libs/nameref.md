# nameref
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':'','sh':''} -->

Make a reference using the name of the referenced object, not its index/number:
```tex
\usepackage{nameref}
% ...
\nameref{fig:myFigure}
```
