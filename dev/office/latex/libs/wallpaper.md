# wallpaper
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

Set wallpaper:
```tex
\usepackage{wallpaper}
% After that you can use commands like:

\LRCornerWallPaper{0.5}{somerights.png} % The numerical argument 0.5 scales the image to 0.5 the size of the page.
\URCornerWallPaper{<scaling>}{<filename>}	% Upper Right
\LLCornerWallPaper{<scaling>}{<filename>}	% Lower Left
\ULCornerWallPaper{<scaling>}{<filename>}	% Upper Left
\CenterWallPaper{<scaling>}{<filename>}
\TileWallPaper{<width>}{<height>}{<filename>}
\TileSquareWallPaper{<number>}{<filename>}

% The This at the start of the command place the image only on the current page, without this the image is placed on all following pages.
\ThisLRCornerWallPaper{0.5}{somerights.png}
```
