# algorithmic
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

```tex
\usepackage[noend]{algorithmic}
```
`noend`: don't write additional lines for end of statement (end for, end if, end while).

Algo example:
```tex
\begin{algorithmic}%
	\FOR{$i \gets 1$ \TO $n$}%
		\STATE $s \gets s + a[i]$%
	\ENDFOR%
\end{algorithmic}%
```

Display line numbers:
```tex
\begin{algorithmic}[1]%     <-- the number is the step (1=each line, 2=each 2 lines, etc.)
	%...
\end{algorithmic}%
```

Set line number format:
```tex
\algsetup{linenodelimiter=}
```
`linenodelimiter=` removes the delimiter (':' by default).

Remove `do` statement:
```tex
\renewcommand{\algorithmicdo}{}
```
