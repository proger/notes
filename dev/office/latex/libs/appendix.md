# appendix
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

Add appendices:
```tex
\documentclass{book}
\usepackage[toc,page]{appendix}

\begin{document}
\tableofcontents

\chapter{Regular Chapter}
\begin{appendices}
\chapter{Some Appendix}
The contents...
\end{appendices}

\end{document}
```
