# hyperref
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

```tex
\usepackage{hyperref}
\href{http://www.my.link/}{My Title}.
\url{http:/www.my.url/}
```


```tex
\usepackage{hyperref}

% web link
\url{http://www.wikibooks.org}
\href{http://www.wikibooks.org}{Wikibooks home}

% email address:
\href{mailto:my_address@wikibooks.org}{my\_address@wikibooks.org}
```

## Forms

```tex
% SIMPLE TEXT FIELD
\documentclass[a4paper,11pt]{article}
\usepackage[latin1]{inputenc} 
\usepackage[pdftex]{hyperref}

\overfullrule3pt

\begin{document}
\noindent \TextField[name=one, width=\hsize]{type here:}
\end{document}

% FIELDS INSIDE INSIDE A FORM TO BE SENT THROUGH HTTP
\documentclass{article}
\usepackage{hyperref}
\begin{document}
\begin{Form}[action={http://your-web-server.com/path/receiveform.cgi}]
\begin{tabular}{l}
\TextField{Name} \\\\
\CheckBox[width=1em]{Check} \\\\
\Submit{Submit}\\
\end{tabular}
\end{Form}
\end{document}

% FIELDS INSIDE INSIDE A FORM TO BE SENT THROUGH EMAIL
\documentclass{article}
\usepackage{hyperref}
\begin{document}
\begin{Form}[action=mailto:forms@stackexchange.invalid?subject={The submitted form},method=post]
\noindent\TextField[name=name]{Name:}\\[1mm]
\ChoiceMenu[radio,name=gender]{Gender:}{male=male,female=fem}\\[1mm]
\TextField[name=email,width=5cm]{E-mail:}\\[5mm]
\Reset{Reset} \quad \Submit{Submit} \quad  \Acrobatmenu{Print}{Print}
\end{Form}
\end{document}
```
