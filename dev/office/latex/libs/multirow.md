# multirow

Rotate text:
```tex
\usepackage{multirow}
\usepackage{rotating}

\multirow{4}{15mm}{\begin{sideways}\parbox{15mm}{text}\end{sideways}}

\multirow{14}{*}{\rotatebox{90}{\mbox{text}}}
```
