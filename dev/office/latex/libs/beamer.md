# beamer

 * [The Beamer package](https://en.wikibooks.org/wiki/LaTeX/Presentations#The_Beamer_package).
 * [Thèmes complets de présentation](http://mcclinews.free.fr/latex/beamergalerie/completsgalerie.html).

`beamer` class:
```tex
\documentclass[a4paper]{beamer}

\title{ELMABIO \\ Environnement logiciel pour le développement}
\author{Pierrick ROGER MELE}
\date{13 mars 2012}

\beamertemplatenavigationsymbolsempty % To disable navigation bar
\usetheme{AnnArbor}

\begin{document}

	\frame{\titlepage}

	\begin{frame}
		\frametitle{Introduction}
		\framesubtitle{}
	\end{frame}
\end{document}
```
