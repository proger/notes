# inputenc

To use UTF-8 inside TeX file:
```tex
\usepackage[utf8]{inputenc}
```
