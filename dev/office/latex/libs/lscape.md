# lscape

```tex
\usepackage{lscape}
```

Put some pages in landscape:
```tex
\begin{landscape}
% ...
\end{landscape}
```
