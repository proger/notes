# eso-pic
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

Set background image:
```tex
\usepackage{eso-pic}
\newcommand\BackgroundPic{
\put(0,0){
\parbox[b][\paperheight]{\paperwidth}{
\vfill
\centering
\includegraphics[width=\paperwidth,height=\paperheight,
keepaspectratio]{background.png}
\vfill
}}}

% ...and this immediately after \begin{document} :
\AddToShipoutPicture*{\BackgroundPic}
% The * will make sure that the background picture will only be put on one page.

% If you wish to use the picture on multiple pages, skip the *
\AddToShipoutPicture{\BackgroundPic}
% Then use this command to stop using the background picture:
\ClearShipoutPicture
```
