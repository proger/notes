# tikz

 * <http://www.texample.net/tikz/examples/>.

Fit picture on page.
Crop a tikz graphic and make it a standalone pdf:
```tex
\documentclass{article}
\usepackage{graphics}
\usepackage{tikz}
\pgfrealjobname{survey}
\begin{document}

\beginpgfgraphicnamed{survey-f1}
  \begin{tikzpicture}
    \fill (0,0) circle (10pt);
  \end{tikzpicture}
\endpgfgraphicnamed

\end{document}
```

The following command produces the image file `survey-f1.pdf` from the above
document, cropped to just the TikZ picture:
```sh
pdflatex --jobname=survey-f1 survey.tex
```

Shaded rectangle:
```tex
\usepackage{tikz}
\usepackage{xcolor}
\definecolor{bannerLight}{HTML}{FF0000}
\definecolor{bannerDark}{HTML}{B00000}
\begin{document}
	\begin{tikzpicture}
		\shade[top color=bannerLight, bottom color=bannerDark] (1,1) rectangle (6cm,20cm);
	\end{tikzpicture}
\end{document}
```
