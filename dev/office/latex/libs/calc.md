# calc

Package to allow computing of length (- + ...).

```tex
\usepackage{calc}
```

Frame box:
```tex
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.5pt}
```

Tabular:
```tex
\setlength{\tabcolsep}{5pt} % column separation
```

Paragraph:
```tex
\parindent
```

Length of a string:
```tex
\widthof{my text}
```

Create a length:
```tex
\newlength{\mylength}
\setlength{\mylength}{\textwidth}
```

Insert an horizontal space equals to the specified text:
```tex
\par Here is some text
\par \hphantom{Here is some} more text.
```
