# babel

Use French:
```tex
\documentclass{article}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}	% --> what for ?
\selectlanguage{french}
```
