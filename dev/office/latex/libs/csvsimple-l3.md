# csvsimple-l3
<!-- vimvars: b:markdown_embedded_syntax={'tex':'','bib':''} -->

 * [The csvsimple-l3 package](https://ctan.crest.fr/tex-archive/macros/latex/contrib/csvsimple/csvsimple-l3.pdf).

Load package:
```tex
\usepackage{csvsimple-l3}
```

Use tab separator:
```tex
\csvautobooktabular[separator=tab]{myfile.tsv}
```
