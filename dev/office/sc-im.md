# SC-Improved

 * [sc-im](https://github.com/andmarti1424/sc-im).
 * [Console Spreadsheets](https://lock.cmpxchg8b.com/spreadsheet.html).
 * [sc-im manpage](https://raw.githubusercontent.com/andmarti1424/sc-im/freeze/src/doc).

Vim like spreadsheet program.
Can read `.sc` files from `sc`.

Unstable on Ubuntu 22.04, 05.12.2022: Core Dump.

Can load `.csv` and `.tsv`.

Insert a sum:
```
=@sum(B1:K1)
```

Help:
```
:help
```

## NORMAL mode

Key  | Description
---  | ---
`yy` | Yank/copy cell.
`p`  | Paste.
`E`  | Edit label.
`e`  | Edit formula.
`<`  | Set label with left alignment.
`>`  | Set label with right alignment.
