# calibre

An ebook reader.

Running:
```sh
calibre
```
or
```sh
calibre mybook.epub
```

Converter from epub to PDF.

The epub must be an archive, not a folder.
To get a file from a folder, just zip the folder.
