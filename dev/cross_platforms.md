# Cross platforms

## rappdirs (R)

 * [rappdirs](https://cran.r-project.org/web/packages/rappdirs/index.html).

Get user's data dir, config dir and cache dir.

Get user's cache folder:
```r
rappdirs::user_cache_dir("biodb")
```

## platformdirs (Python)

 * [platformdirs](https://pypi.org/project/platformdirs/).

Get user's folders: cache, home, config, ...
