# logging
<!-- vimvars: b:markdown_embedded_syntax={'cpp':'','python':''} -->

## C++

 * [BOOST Log](https://www.boost.org/doc/libs/1_85_0/libs/log/doc/html/index.html).

```cpp
#include <boost/core/null_deleter.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>

static boost::log::trivial::severity_level g_log_level;

void init_logging(int verbosity)
{
    auto backend = boost::make_shared<boost::log::sinks::text_ostream_backend>();
    backend->add_stream(boost::shared_ptr< std::ostream >(&std::clog, boost::null_deleter()));

    backend->auto_flush(true);

    typedef boost::log::sinks::synchronous_sink<boost::log::sinks::text_ostream_backend> sink_t;
    boost::shared_ptr<sink_t> sink(new sink_t(backend));

    // Format
    sink->set_formatter( boost::log::expressions::format("[%1%] %2%") %
        boost::log::trivial::severity % boost::log::expressions::smessage);

    // Register sink
    boost::log::core::get()->add_sink(sink);

    // Set verbosity level
    switch (verbosity) {
      case 0: g_log_level = boost::log::trivial::warning; break;
      case 1: g_log_level = boost::log::trivial::info; break;
      case 2: g_log_level = boost::log::trivial::debug; break;
      default: g_log_level = boost::log::trivial::trace;
    }
    boost::log::core::get()->set_filter(boost::log::trivial::severity >=
                                        g_log_level);
}
```

For colored logs:
```cpp
static bool g_log_coloring;

void coloring_formatter(boost::log::record_view const& rec,
                        boost::log::formatting_ostream& os)
{
  // Get severity
  auto severity = rec[boost::log::trivial::severity];

  // Set severity color
  if (g_log_coloring && severity)
  {
    // Set the color
    switch (severity.get())
    {
    case boost::log::trivial::severity_level::trace:
        os << "\033[0;33m"; break;
    case boost::log::trivial::severity_level::debug:
        os << "\033[1;33m"; break;
    case boost::log::trivial::severity_level::info:
        os << "\033[1;32m"; break;
    case boost::log::trivial::severity_level::warning:
        os << "\033[1;35m"; break;
    case boost::log::trivial::severity_level::error:
    case boost::log::trivial::severity_level::fatal:
        os << "\033[1;31m"; break;
    }
  }

  // Format the message here...
  if (severity) {
    std::string sev(boost::log::trivial::to_string<char>(*severity));
    std::transform(sev.begin(), sev.end(), sev.begin(), ::toupper);
    os << "[" << sev << "] ";
  }
  os << rec[boost::log::expressions::smessage];

  // Restore the default color
  if (severity)
    os << "\033[0m";
}

void init_logging(int verbosity, bool coloring)
{
    ...

    sink->set_formatter(&coloring_formatter);

    ...

    // Enable/disable coloring
    g_log_coloring = coloring;
}
```

## Python

 * [logging — Logging facility for Python](https://docs.python.org/3/library/logging.html?highlight=logging#module-logging).
 * [coloredlogs](https://pypi.org/project/coloredlogs/).
 * [Log formatting with colors!](https://pypi.org/project/colorlog/).

```python
import logging
```

Set level of root logger (all other loggers will inherit from this setting):
```python
root = logging.getLogger()
root.setLevel(logging.DEBUG)
```

Set a handler for the root logger:
```python
handler = logging.StreamHandler(sys.stderr)
root.addHandler(handler)
```
Possible outputs: file, stderr, email, datagrams, sockets, HTTP Server.

Enable logging of levels and set format with printing of timestamp and level:
```python
fmt = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
handler.setFormatter(fmt)
```

Set a file output for the root logger:
```python
fh = logging.FileHandler('MyLogFile.log')
fh.setLevel(logging.DEBUG)
root = logging.getLogger()
root.setLevel(logging.DEBUG)
root.addHandler(fh)
```

Log a message from some part of code:
```python
logger = logging.getLogger(__name__) # A good practice is to use __name__ (the name of the module) for defining a logger. Loggers are organised hierarchically by names. Dot `.` is used as separator to build hierarchy inside names.
logger.debug('Debugging information')
logger.info('Informational message')
logger.warning('Warning:config file %s not found' % 'server.conf')
logger.error('Error occurred')
logger.critical('Critical error -- shutting down')
```

Create TRACE logging level:
```python
logging.TRACE = 5
logging.addLevelName(logging.TRACE, "TRACE")
def trace(self, message, *args, **kws):
    if self.isEnabledFor(logging.TRACE):
        self._log(logging.TRACE, message, args, **kws) 
logging.Logger.trace = trace
```
