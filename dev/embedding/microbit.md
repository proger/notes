# micro:bit

 * [BBC micro::bit](https://www.microbit.co.uk/).

 * [Introduction to the micro:bit](http://www.penguintutor.com/programming/docs/introduction-to-microbit.pdf).
 * [Microsoft MakeCode for micro:bit](https://makecode.microbit.org/).

 * [BBC micro:bit MicroPython documentation](https://microbit-micropython.readthedocs.io/en/latest/index.html).
   + [Build and Flash MicroPython](https://microbit-micropython.readthedocs.io/en/latest/devguide/flashfirmware.html).

See `uflash` to transfer a Python program to the micro:bit.
See `pxt` Javascript package (install with `npm`) to transfer a Javascript program to the micro:bit.
