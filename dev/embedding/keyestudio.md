# keyestudio

Robot model: SKU:KS0426
Micro:bit Minit Smart Robot Car Kit V2

 * [Ks0426 keyestudio Micro:bit Mini Smart Robot Car Kit V2](https://wiki.keyestudio.com/Ks0426_keyestudio_Micro:bit_Mini_Smart_Robot_Car_Kit_V2). All about the robot, including the cabling.
   + [KS0426(KS4019) Keyestudio Micro：bit Mini Smart Robot Car Kit V2](https://www.dropbox.com/sh/hyhp9usgldbtb0o/AABiOXrtr4F2JHyMH2wYMkPqa?dl=0).

 * [PCA9685](https://www.az-delivery.de/fr/products/pca9685-servotreiber): utilisé pour envoyer des signaux [PWM](https://en.wikipedia.org/wiki/Pulse-width_modulation) aux moteurs et aux LEDs de devant.
 * [PCA9685 - 16-Channel, 12-Bit PWM Fm+ I²C-Bus LED Controller](https://www.nxp.com/products/power-management/lighting-driver-and-controller-ics/led-controllers/16-channel-12-bit-pwm-fm-plus-ic-bus-led-controller:PCA9685). The PCA9685 is an I²C-bus controlled 16-channel LED controller optimized for Red/Green/Blue/Amber (RGBA) color backlighting applications....

 * [micro:bit](https://microbit.org/).
  + In Makecode we need to import `K-bit extension library` from the extensions
    in order to program the robot. Search for extension `https://github.com/mworkfun/pxt-k-bit.git`.
  + To import code, go to main page: <https://makecode.microbit.org/>.
  + Makecode Extension (search for keyestudio or pxt-robotcar): <https://makecode.microbit.org/pkg/veilkrand/pxt-robotcar> Javascript only?
  +  [pxt-robotcar](https://github.com/veilkrand/pxt-robotcar),
