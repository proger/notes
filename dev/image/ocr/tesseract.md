# tesseract

Old OCR developped by HP in the 1980's. Now Open Source, but only supports TIFF uncompressed images.
runs under \*NIX.

## Install

Install on macos with MacPorts:
```sh
sudo port info tesseract +fra
```
or Homebrew:
```sh
brew install leptonica --with-libtiff
brew install tesseract
```

For installations that don't include desired language package, download
language package:
 * <http://tesseract-ocr.googlecode.com/files/eng.traineddata.gz>
 * <http://tesseract-ocr.googlecode.com/files/fra.traineddata.gz>
and install them, unzipped, in /opt/local/share/tessdata or
`/usr/local/share/tessdata`.

# Run

Convert an image to tiff with no compression:
```sh
ppm2tiff -c none myfile.ppm myfile.tif
```

Run tesseract:
```sh
tesseract myfile.tif myfilebase # will output one file myfilebase.txt
tesseract myfile.tif myfilebase batch.nochop   # <--- batch.nochop (or nobatch, or batch) specifies a predefinied config file in  tessdata/tessconfigs/.
```

Use language:
```sh
tesseract myfile.tif myfilebase -l fra
```
First you need to download the appropriate language pack,
fra.traineddata in our case, and put it inside tessdata directory
which should be either in `/usr/local/share/tessdata` or in
tessdata installation directory (`<version>/share/tessdata`).

Output letter boxes in file:
```sh
tesseract myfile.tif myfilebase -l fra batch.nochop makebox # will output one file myfilebase.box
```

Train tesseract from letter box:
```sh
tesseract myfile.tif myfilebase nobatch box.train # will output two files: myfilebase.tr and myfilebase.txt (empty ?)
```

From homebrew information
The easiest way to use it is to convert the source to a Grayscale tiff:
```sh
convert source.png -type Grayscale terre_input.tif
```
then run tesseract:
```
tesseract terre_input.tif output
```

## Image reading error

If the following error occurs:
```
Tesseract Open Source OCR Engine v3.02.02 with Leptonica
Error in findTiffCompression: function not present
Error in pixReadStreamTiff: function not present
Error in pixReadStream: tiff: no pix returned
Error in pixRead: pix not read
Unsupported image type.
```
Then check installation of leptonica, if it has been compiled with tiff support.

