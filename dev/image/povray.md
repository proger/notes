# povray

 * [Rendering transparent icons with POV-Ray](http://www.imagico.de/pov/icons.php).

Running:
```bash
povray +Ooutput_file -D +FN +Wwidth +Hheight +Q11 pov_file
```

Option | Description
------ | -------------------------------------------
`+FN`  | PNG output format.
`-D`   | Don't display image in progress screen.
------ | ---------------------------------------------
`+A0.1`| Enables antialising with threshold of 0.100.
`+AM2` | Antialiasing method, 2=adaptive.
------ | -------------------------------------------
`+UA`  | Set alpha channel on.

Setting a camera:
```pov
camera {
  location <0, 10, -20>
  direction 3*y
  up        y
  right     x*image_width/image_height
  look_at  3*y
}
```

Lights:
```pov
light_source {< -50, 25, 50> color White }
```

Shadowless:
```pov
light_source {< -50, 25, 50> color White shadowless }
```

Define an RGB color:
```pov
rgb <1,0.9,0.9>
rgb <0.1,0.5,0.3,0.4> // with transparency value
```

Macros:
```pov
#declare my_macro = texture { T_Glass4 }
```

Objects:
```pov
sphere { <Center>, Radius ... }
sphere { <0, 3, 0>, 2 }
cylinder { <Base_Point>, <Cap_Point>, Radius ... }
box { <Corner_1>, <Corner_2> ... }
cone { <Base_Point>, Base_Radius, <Cap_Point>, Cap_Radius ... }
plane { }
```

Objects options:
```pov
texture { T_Glass4 }
texture { T_Glass4 pigment {rgb<88/255, 249/255, 146/255, 1.0>} }
```

Transformations:
```pov
sphere { <10,10,10> 1 scale <2, 1, 0.5> }
sphere { <10,10,10> 1 translate <2, 1, 0.5> }
sphere { <10,10,10> 1 rotate <20, 0, 0> } // rotate 20° around X axis
```

Don't render object on image:
```pov
sphere { <0,0,0> 10 no_image }
```

