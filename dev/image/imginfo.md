# imginfo
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

To get info about an image:
```sh
imginfo <imagefile
```
