# zbarimg

Install on Ubuntu:
```sh
sudo apt-get install zbar-tools
```

Scan barcode:
```sh
zbarimg myfile.png
```
