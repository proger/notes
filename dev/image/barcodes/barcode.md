# barcode

Bar code generation.

Install on Debian:
```sh
apt install barcode
```

Generate bar code for fidelity card:
```sh
barcode -b "9932214333098" -e ean -S -o bp.svg
```

Does not encode well code39.
