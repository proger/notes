# zint

 * [4. Using the Command Line](https://zint.org.uk/manual/chapter/4).

Bar code generator.

Example:
```sh
zint -b CODE39 -o myfile.svg -d "123456"
```
