# netpbm
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Convert to bmp:
```sh
ppmtobmp <file.ppm >file.bmp
```

Convert png to jpeg with scaling:
```sh
pngtopnm abyss.png | pnmscale 0.5 | pnmtojpeg >abyss.jpeg 
```
