# vimiv

 * [vimiv](https://karlch.github.io/vimiv/).
 * [Keybindings](https://karlch.github.io/vimiv/docs/keybindings_commands).

Config dir: `$XDG_CONFIG_HOME/vimiv/`.

General keys:
	`gi`    Enters image mode.
	`gl`    Enters library mode.
	`gt`    Enters thumbnail mode.
	`gm`    Enters manipulate mode.

Keys:
	`+`     Zoom in
	`-`     Zoom out
	`n`     Next image
	`p`     Previous image
	`g`     First image
	`G`     Last image
	`h`     Scroll image
	`j`     Scroll image
	`k`     Scroll image
	`l`     Scroll image
	`w`     Fit window size
	`e`     Fit horizontally
	`E`     Fit vertically
	`ss`    Start slideshow
	`sh`    Decrease speed of slideshow
	`sl`    Increase speed of slideshow
	`x`     Delete current image.
	`>`     Rotate right.
	`<`     Rotate left.
	`|`     Flip vertically.
	`_`     Flip horizontally.
