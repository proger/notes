# ppmtobmp
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Install with package netpbm.

Convert to bmp:
```sh
ppmtobmp <file.ppm >file.bmp
```
