# png2icns
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Part of [libicns](https://github.com/kornelski/libicns).

Installation on ArchLinux:
```sh
pacman -S libicns
```

Usage:
```sh
png2icns myicon.icns img1.png img2.png ...
```
