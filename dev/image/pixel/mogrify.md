# mogrify

Write text on an image:
```sh
mogrify -font <fontname> -annotate XxY "text" <file>
```
`mogrify` does not handle BMP files.
