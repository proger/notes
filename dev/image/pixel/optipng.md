# optipng

Optimize PNG file by compressing image.

Install on Archlinux:
```sh
pacman -S optipng
```

Usage:
```sh
optipng myfile.png
```
