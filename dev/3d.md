# 3D graphics

### OpenGL

 * [opengl-tutorial](http://www.opengl-tutorial.org/).
 
See Vulkan, the successor of OpenGL.

### Vulkan

 * [Vulkan Tutorial](https://vulkan-tutorial.com/Overview).

## Grasshopper 3D

 <http://www.grasshopper3d.com>

Generate shapes using generative algorithms, Grasshopper® is a graphical
algorithm editor tightly integrated with Rhino’s 3-D modeling tools. 

## Open CASCADE

<http://www.opencascade.org>

Open CASCADE Technology, 3D modeling & numerical simulation

email cea / pierrick.rogermele / perso alphanum

* [Open CASCADE Community Edition](https://github.com/tpaviot/oce).

Installing:
```sh
git clone git://github.com/tpaviot/oce.git
mkdir build
cd build
cmake ..
make
make install/strip
cd ..
```
