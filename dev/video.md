# Video
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

## xawtv

display default camera:
```sh
xwatv
```

display USB camera:
```sh
xawtv -c /dev/video2
```

Press 'j' to Shoot a photo and save it in JPEG format.

## Screencast

### peek

Animated GIF screencast, screen recorder.

Install:
```sh
apt install peek # Debian
```

### camtasia

<http://www.techsmith.com/camtasia.html>

Screen recording and video editing.

### CamStudio

<http://camstudio.org>.

A free open source camtasia like software.

Generated a DVD without menu, readable by VLC, starting from an MP4 file.

## DVDStyler

## DeVeDe

Create a standard video DVD from an mkv or mp4.

Generated a DVD not readable by VLC, starting from an MKV file.

## lsdvd

Install:
```sh
apt install lsdvd # Ubuntu
yay -S lsdvd # Archlinux
```

List DVD titles:
```sh
lsdvd /dev/sr0
```

```
libdvdread: Encrypted DVD support unavailable.
```
Install: `libdvdnav` and `libdvdcss`.

## exiftool

Package `perl-image-exiftool` on ArchLinux.

Read media file metadata.

Read EXIF information:
```sh
exiftool myfile.jpeg
```

Get all Date tags:
```sh
exiftool -*Date SP_A0522.jpg 
```

Get all Date tags, printing tag name instead of description:
```sh
exiftool -s -*Date SP_A0522.jpg 
```

Set tag value (`exiftool` will rename the original file `my_file.ext_original`
and output a copy of the file with the original name):
```sh
exiftool -FileModifyDate='2015:09:01 12:00:03+02:00' SP_A0522.jpg 
exiftool -Title="My New Title" mymovie.mp4
```

Extract one tag:
```sh
exiftool -ImageHeight myimg.jpg
```

Exclude one tag from the listing:
```sh
exiftool --ImageHeight myimg.jpg
```

Output in CSV format:
```sh
exiftool -csv img1.jpg img2.png img3.jpg ...
```

Remove an orientation information:
```sh
exiftool -Orientation= myfile.jpeg
```

## ffmpeg

Option          | Description
---             | ---
`-ac`           | Set the number of audio channels.
`-acodec`       | Alias for `-codec:a` for setting audio codec.
`-aq`           | Alias for `-q:a` for setting audio quality.
`-map_metadata` | Set metadata info for next output file.
`-vcodec`       | Alias for `-codec:v` for setting video codec.
`-vn`           | Block all video streams.

Shift subtitles by 10 seconds (advance them):
```sh
ffmpeg -itsoffset -10 -i input.srt -c copy output.srt
```

Shoot photo from webcam:
```sh
ffmpeg -f video4linux2 -i /dev/video0 -vframes 1 -video_size 640x480 test.jpeg
```

Make a video from jpg images:
```sh
ffmpeg -framerate 10 -i shot_%06d.jpg -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p output.mp4
```

To convert from AIFF to Apple Lossless:
```sh
ffmpeg -i audio.wav -c:a alac audio.m4a
```

To convert from mp3 to wav:
```sh
ffmpeg -i myfile.mp3 myfile.wav
```

To convert m4a into mp3:
```sh
ffmpeg -i myfile.m4a -c:a libmp3lame -b:a 96k myfile.mp3
```

To convert m4a to ogg:
```sh
ffmpeg -i file.m4a -acodec libvorbis -aq 4 -vn file.ogg
vorbiscomment -d iTunSMPB -d iTunNORM -d iTunMOVI -d account_id file.ogg # Remove unwanted tags
```
Default quality (`-aq`) of libvorbis is 3 (112kbps).

We can also use `ffmpeg` directly to encode the images files into a movie. If
we start with files name 001.jpg, 002.jpg, ..., we can use:
```sh
ffmpeg -r 10 -b 1800 -i %03d.jpg test1800.mp4
```

Concatenate two audio files, one after the other:
```sh
ffmpeg -i file1.mp3 -i file2.mp3 -filter_complex '[0:0][1:0]concat=n=2:v=0:a=1[out]' -map '[out]' output.mp3
```

Convert MP4 to AVI:
```sh
ffmpeg -i input.mp4 -c:v libx264 -c:a libmp3lame -b:a 384K output.avi
```

Convert MP4 to MPG:
```sh
ffmpeg -i video.mp4 -c:v mpeg2video -q:v 5 -c:a mp2 -f vob video.mpg
```

Convert mkv to MP4 with all tracks (video, audio, subtitles):
```sh
ffmpeg -i input.mkv -codec copy -map 0 output.mp4    
```

Editing metadata fields:
```sh
ffmpeg -metadata title="TITLE" -metadata artist="ARTIST" -metadata date=DATE -metadata genre="GENRE" -metadata comment="COMMENT" -codec copy -i old.avi new.avi
```

Resize video:
```sh
ffmpeg -i video_1920.mp4 -vf scale=640:360 video_640.mp4
```

Slow down a video:
```sh
ffmpeg -i input.mp4 -filter:v "setpts=2.0*PTS" -an output.mp4
```

Cut video:
```sh
ffmpeg -ss 00:02:30 -to 00:10:45 ...
```
`-ss`: Start time
`-to`: Stop time

Extract album cover from audio file:
```sh
ffmpeg -i file.mp3 -an -c:v copy file.jpg
```

## ffprobe

Get file metadata:
```sh
ffprobe myfile.mkv
```

## fswebcam

Shoot photo from webcam:
```sh
fswebcam -r 640x480 --jpeg 85 -D 1 shot.jpg
```

Webcam Logitech, resolutions:
 * 640x480
 * 960x720
 * 1280x720
 * 1600x1000
 * 1600x1200 --> Timed out waiting for frame! No frames captured.

## gnome-subtitles

Subtitles editor. Can edit .srt files. Can adjust all subtitle items by
resseting start times of first and last subtitles.

## HandBrake

DVD ripping software.

Install:
```sh
yay -S handbrake # Archlinux
```

Run:
```sh
ghb
```

On Ubuntu 22.04 Handbrake version does not handle correctly the substitles. They
are not synchronized.
Solution is to compile and install version 1.3.x from GitHub at
https://github.com/HandBrake/HandBrake.git.

Install then:
```sh
apt install libdvdnav4 libdvd-pkg
dpkg-reconfigure libdvd-pkg # Not sure this step is necessary
```

Run as command line:
```sh
HandBrakeCLI -i /dev/sr0 -t 1 -o avg.mkv -f av_mkv --all-audio --all-subtitles
```

Rip all titles of a DVD:
```sh
for i in $(seq $(lsdvd /dev/sr1|grep ^Title | wc -l)) ; do HandBrakeCLI -i /dev/sr1 -t $i -f av_mp4 --all-audio --all-subtitles -o "$(blkid -o value -s LABEL /dev/sr1)_$i.mp4" ; done
```
Not sure `--min-duration` works as expected.

## Jellyfin

 * [Jellyfin](https://jellyfin.org/).

Open source **media server**.

Allow user `jellyfin` to read media files.
Open website at <http://localhost:8096/>.

## kazam

Screencast and screenshot.

Install:
```sh
apt install kazam # Debian
```

## Kodi

 * [Kodi](https://kodi.tv/)

Open Source Home Theatre

## mencoder
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

 * [MEncoder](https://wiki.archlinux.org/title/MEncoder).

For a separate audio input:
```sh
mencoder -audiofile <file> ...
```

Audio out of sync:
	-mc 0 : set maximum A-V sync correction per frame to zero.
	-noskip -skiplimit 1

Telecined various options (for NTSC series):
	-vf pullup,softskip -ofps 24000/1001
	-vf ivtc=1,pp=lb,softskip
	-ofps 30000/1001
	-ofps <fps> : output frame rate.   Must be  set  for  variable  fps  (ASF,  some  MOV)  and  progressive (30000/1001 fps telecined MPEG) files.
NTSC series or movies are encoded on DVD at 60FPS with one half of the image on each frame, so a resulting 30FPS. Not sure those options do the job of merging each pair of images.
See <http://www.mplayerhq.hu/DOCS/HTML/en/menc-feat-telecine.html>.
What about PAL?

H264:
```sh
mencoder -dvd-device /dev/disk2 dvd://1 -o video.avi -of avi -vf pp=fd -ovc x264 -oac lavc -aid 0x81 -channels 6 -lavcopts acodec=ac3 -x264encopts bitrate=1500

mencoder amy1.flv -oac mp3lame -srate 44100 -lameopts  vbr=2:q=8:aq=0:mode=1:lowpassfreq=16000 -ovc x264 -x264encopts threads=auto:subq=5:partitions=4x4:8x8dct:frameref=3:me=hex:bframes=4:b_pyramid:psnr:bitrate=348\ -of lavf -lavfopts format=mov -o amy1.mp4
```

Pour extraire les sous-titres :
```sh
mencoder -dvd-device /dev/dvd dvd://1 -vobsubout bigsleep -vobsuboutindex 0 -vobsuboutid en -sid 0 -ovc frameno -o tmp.avi -nosound
```

```sh
mencoder input.avi -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=800 -o movie.avi
```
	lavc codec options :
	autoaspect		auto aspect ratio
	vpass=1 & vpass=2	 For 2 passes
	vqmax=<1-31>		Maximum quantizer (pass 1/2), 10-31 should be a sane range (default: 31).
	

One pass with mpeg4:
```sh
mencoder -dvd-device /dev/dvd1 dvd://2 -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg4:vqmax=4:autoaspect  -vf pp=fd,crop=688:576:12:0 -o movie.avi
```

Two passes with Xvid:
```sh
mencoder -dvd-device /dev/dvd1 dvd://1 -vf pp=fd,crop=704:416:8:80 -nosound -ovc xvid -xvidencopts pass=1:bitrate=1200:turbo -o /dev/null && nice mencoder -dvd-device /dev/dvd1 dvd://1 -vf pp=fd,crop=704:416:8:80 -nosound -ovc xvid -xvidencopts pass=2:bitrate=1200 -o lor2_mencoder_1200.avi
```

Two passes with mpeg4:
```sh
mencoder -dvd-device /dev/dvd1 dvd://2 -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=900:vpass=1:autoaspect -o maigret.avi && nice -19 mencoder -dvd-device /dev/dvd1 dvd://2 -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=900:vpass=2:autoaspect -o maigret.avi
```
	-vf crop=704:464:10:56
	-vf crop=96:320:318:130
	-vf crop=320:352:204:108
	-vf crop=320:368:204:98

Two passes with mpeg4:
```sh
mencoder -ss 5 05-At\ the\ ends\ of\ the\ earth\ -\ Konstantion\ Bronzit\ -annecy\ 1999.mpg -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=900:vpass=1:autoaspect -o ends.avi && nice -19 mencoder 05-At\ the\ ends\ of\ the\ earth\ -\ Konstantion\ Bronzit\ -annecy\ 1999.mpg -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=900:vpass=2:autoaspect -ss 5 -o ends2.avi
```

## mkvextract

Install in Archlinux:
```sh
pacman -S mkv mkvtoolnix-cli
```

Extract an audio track:
```sh
mkvextract myfile.mkv tracks 2:english.ogg
```

Extract an attachment (use default file name):
```sh
mkvextract attachments lab.mkv 1
```

Extract an attachment (force file name):
```sh
mkvextract attachments lab.mkv 1:cover.jpg
```

## mkvinfo

Install:
```sh
pacman -S mkv mkvtoolnix-cli # Archlinux
apt-get install -y mkvtoolnix # Debian
```

Gets info about a mkv (with track IDs to be used in `mkvextract` and
`mkvmerge`):
```sh
mkvinfo myfile.mkv
```
## mkvmerge

Install in Archlinux:
```sh
pacman -S mkvtoolnix-cli
```

Remove audio and subtitles:
```sh
mkvmerge -o out.mkv -A -S in.mkv
```

Remove video:
```sh
mkvmerge -o out.mkv -D in.mkv
```

Keep only some tracks from video, audio and subtitles:
```sh
mkvmerge -o out.mkv -d 0 -a 2,3 -s 5,6 in.mkv
```

Merging an audio file and a movie file:
```sh
mkvmerge screen_recording.mov audio_recording.m4a -o screen.mkv
```
	
Adding an image:
```sh
mkvmerge input.mkv --attachment-mime-type image/jpg [--attachment-name "cover.jpg"] --attach-file <file> -o output.mkv
```

Add a new audio track:
```sh
mkvmerge -o out.mkv in.mkv audio.ogg
```

Pour les .srt s'assurer que le fichier est bien encodé en UTF-8. Exemple :
```bash
cat Le\ Secret\ des\ Poignards\ Volants.fr.srt | perl -e 'binmode (STDOUT, ":utf8"); while(<STDIN>) { print $_; }' > poignards.fr.srt

mkvmerge -o Le\ Secret\ des\ Poignards\ Volants.mkv Le\ Secret\ des\ Poignards\ Volants.avi --language 0:fre Le\ Secret\ des\ Poignards\ Volants.fr.srt

mkvmerge -o Dial\ M\ For\ Murder.mkv dialm.avi --language 0:eng --language 1:fre dialm.idx

mkvmerge -o Shrek\ 2.mkv Shrek\ 2.avi --language 0:eng --language 1:fre Shrek\ 2.idx

mkvmerge -o Robin\ Hood.mkv -A Robin\ Hood.avi Robin\ Hood.ogg --language 0:eng --language 1:fre Robin\ Hood.idx

mkvmerge -o "Breakfast at Tiffany's.mkv" --aspect-ratio 16/9 -A breakfast.avi breakfast.ogg --language 0:eng --language 1:fre breakfast.idx
```

Pour s'assurer qu'un fichier srt est en utf8:
```bash
Perl -e 'binmode (STDOUT, ":utf8"); while(<STDIN>) { print $_;}' <input.srt >output.srt
```

## mkvpropedit

Edit MKV file properties.

Tracks are numbered separately for each type (video, audio, ...), and each time starting from 1.

Set/unset a defaul track
To set a default track for a type (audio, sub), one must also unset the current default track.
```sh
mkvpropedit La\ Freccia\ Azzurra.mkv --edit track:a1 --set flag-default=0
mkvpropedit La\ Freccia\ Azzurra.mkv --edit track:a2 --set flag-default=1
```

## mplayer
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

MPlayer sources: `svn checkout svn://svn.mplayerhq.hu/mplayer/trunk mplayer`.

[Slave mode protocol](http://www.mplayerhq.hu/DOCS/tech/slave.txt):
```sh
mplayer -slave -quiet myfile
```

We can also use a FIFO:
```sh
mkfifo /tmp/fifofile
mplayer -slave -input file=/tmp/fifofile myfile
```

Play DVD menu:
```sh
mplayer dvdnav:// -dvd-device /dev/sr0
```
To enable support for encrypted DVD (region DVD support), install `libdvdcss`.
On Archlinux:
```sh
pacman -S libdvdcss
```
On Ubuntu:
```sh
apt install libdvd-pkg
dpkg-reconfigure libdvd-pkg
```

Play DVD track 1:
```sh
mplayer -dvd-device /dev/sr0 dvd://1
```

Start 16 minutes after the beginning:
```sh
mplayer -ss 0:16:00 ...
```

Select audio channel by ID:
```sh
mplayer -aid 0x80 ...
```

Select audio channel by language code:
```sh
mplayer -alang fr ...
```

Run in full screen:
```sh
mplayer -fs ...
```

To select audio output at ALSA level, look first at
available audio devices with `aplay` (`alsa-utils` package
on ArchLinux):
```sh
aplay -l
```
Then run mplayer with option `-ao`:
```sh
mplayer -ao alsa:device=hw=1,0,0
```
Where the three indices are in order: the card index, the device index, the subdevice index.

Key   | Description
----- | ------------------------------
o     | Display time elapsed and time remaining for a video.
f     | Full screen.
j     | Change subtitle track.
`#`   | Change audio track.
space | Pause.

To get a list of compiled-in video output drivers.
```sh
mplayer -vo help       
```

To convert a wma:
```sh
mplayer music.wma -ao pcm
```
mplayer creates a file named `audiodump.wav`.

To record what is played:
```sh
mplayer -dvd-device MYDISC.dmg dvd://1 -dumpstream -dumpfile myfile.vob 
```

To play only a part of the video/audio:
```sh
mplayer ... -ss 100 -endpos 60
```
	-ss       start position
	-endpos   end position

### Detecting crop area

For detecting crop area :
```sh
mplayer Annecy\ Animation\ Festival\ 2004.mov -vf cropdetect -ao null -vo null
```
	-vf : filters. Only one -vf must be provided. Filters are separated by a comma.
	-vf pp=fd		Deinterlacing filter
	-vf crop=w:h:x:y	Cropping filter :
	-ss hh:mm:ss		start encoding at specified position
	-endpos hh:mm:ss	end encoding at specified position

## Open Movie Editor

<http://www.openmovieeditor.org>

Free and Open Source Video Editor for Linux
version 0.2

## OpenShot Video Editor

<http://openshot.org>

version 1.4

## Plex

 * [Plex installation](https://support.plex.tv/hc/en-us/articles/200288586-Installation).

Proprietary **media server**.

On Ubuntu/Debian it installed as a systemd service: `plexmediaserver`, or
`snap.plexmediaserver.plexmediaserver` if installed with snap.
The snap version is not good, it runs as `root`. The debian package version uses
a `plex` user.

Connect to the server for configuration: <http://localhost:32400/web>

No need of a Plex account to use on local network. Just set the server IP
address on the client.

On Linux log files are in `/var/lib/plexmediaserver/Library/Application\ Support/Plex\ Media\ Server/Logs`.

## Shotcut

 * <http://www.shotcut.org>

Movie editor.

Windows, Linux, MacOS-X

## tcextract

Extract sub-titles:
```bash
tccat -i /dev/dvd -T 2 -L | tcextract -x ps1 -t vob -a 0x21 > subs-fr
```

Pour un format vob :
```bash
Subtitle2vobsub
```

Pour un format srt :
```bash
subtitle2pgm -o french -c 255,255,0,255 < subs-fr
pgm2txt -v -f fr french
srttool -s -w < french.srtx > french.srt
aspell --lang=french --encoding=iso8859-1 french.srt
```

## transcode

Pour éliminer les bords noirs :
```sh
transcode -i 78\ Tours.avi -J detectclipping=limit=24 -y null,null
transcode -i 78\ Tours.avi -j 0,8,0,8 - R 1 -y xvid4,null && transcode -i 78\ Tours.avi -j 0,8,0,8 - R 2 -y xvid4 -o 78tours_2.avi

nice transcode -i grease.1800.avi -w 900 -R 1 -y divx4,null && nice transcode -i grease.1800.avi -w 900 -R 2 -y divx4,null -o grease.900.divx4.avi

nice -19 transcode -i The\ Young\ Master.avi -j 0,8,0,8 -w 810 -R 1 -y xvid4,null && nice -19 transcode -i The\ Young\ Master.avi -j 0,8,0,8 -w 810 -R 2 -y xvid4 -o youngmaster.avi

transcode -i easter.avi -w 240 -c 70-22071 -R 1 -y xvid4,null && transcode -i easter.avi -w 240 -c 70-22071 -R 2 -y xvid4 -o easter.2.avi
```
	
Convert mkv to avi:
```sh
mkvmerge -i movie.mkv # to obtain the list of tracks
mkvextract tracks movie.mkv 1:movie.xvid
mkvextract tracks movie.mkv 2:movie.ogg
transcode -i movie.xvid -p movie.ogg -y xvid4 -o movie.avi
```

Pour extraire une piste audio d'un avi et l'écrire dans un fichier ogg:
```sh
transcode -i movie.avi -y null,ogg -o movie.ogg
```

	-J : filter
	-T : dvd title
	-a : audio track
	-j : select frame region by clipping border.
	-o : output file
	-y : output encoder
	-R n : n=1 first pass, n=2 second pass
	-Z wxh : resize
	-w : bitrate
		$video_bitrate = int($video_size/$runtime/1000*1024*1024*8);
		  with $video_size in MB and $runtime in seconds
	-c f1-f2	: frames range in frame numbers or time (HH:MM:SS)
	-I 5 : deinterlace
	-C 3 : anti-aliasing, full frame
	--import_asr <number> : aspect ratio
			1:	1:1
			2:	4:3	= 1.33
			3:	16:9	= 1.78
			4:	2.21:1
	
```sh
transcode -i Zorba\ The\ Greek.avi -x mplayer -w 800 --import_asr 3 -I 5 -C 3 -Z 720 -J detectclipping=limit=24 -y null,null
nice -19 transcode -i Zorba\ The\ Greek.avi -x mplayer -y null,ogg -o zorba.ogg && nice -19 transcode -i Zorba\ The\ Greek.avi -x mplayer -w 800 --import_asr 3 -I 5 -C 3 -Z 720 -j 16,0,16,0 -R 1 -y xvid4,null && nice -19 transcode -i Zorba\ The\ Greek.avi -x mplayer -w 800 --import_asr 3 -I 5 -C 3 -Z 720 -j 16,0,16,0 -R 2 -y xvid4,null -o zorba.avi

nice -19 transcode -i Why\,\ Charlie\ Brown\,\ Why.mpg --import_asr 2 -Z 768 -w 600 -I 5 -j 8,16,0,16 -R 1 -y xvid4,null && nice -19 transcode -i Why\,\ Charlie\ Brown\,\ Why.mpg --import_asr 2 -Z 768 -w 600 -I 5 -j 8,16,0,16 -R 2 -y xvid4 -o why.avi

nice -19 transcode -i grease.1800.avi -x mplayer -w 900 -R 1 -y xvid4,null && nice -19 transcode -i grease.1800.avi -x mplayer -w 900 -R 2 -y xvid4,null -o grease.900.avi

nice -19 transcode -i Fast\ Film.avi -j 8,0,8,0 -R 1 -y xvid4,null && nice -19 transcode -i Fast\ Film.avi -j 8,0,8,0 -R 2 -y xvid4 -o fast.avi

nice -19 transcode -i Fast\ Film.avi -j 8,0,8,0 -w 10 -R 3 -y xvid4 -o fast.R3.10.avi

transcode -i For\ the\ Birds.mpg --import_asr 3 -Z 640 -j 64,8,72,8 -w 5 -R 3 -y xvid4 -o birds.avi
nice -19 transcode -i For\ the\ Birds.mpg --import_asr 3 -Z 640 -j 64,8,72,8 -w 300 -R 1 -y xvid4,null && nice -19 transcode -i For\ the\ Birds.mpg --import_asr 3 -Z 640 -j 64,8,72,8 -w 300 -R 2 -y xvid4 -o birds.avi

nice -19 transcode -i Martinko.avi -j 0,8,0,8 -w 900 -R 1 -y xvid4,null && nice -19 transcode -i Martinko.avi -j 0,8,0,8 -w 900 -R 2 -y xvid4 -o matrinko.R12.900.avi
nice -19 transcode -i Martinko.avi -x mplayer -j 0,8,0,8 -w 5 -R 3 -y xvid4 -o martinko.R3.5.avi

nice -19 transcode -i glace.avi -x mplayer --import_asr 3 -I 5 -C 3 -Z 720 -j 16,0,16,0 -w 5 -R 3 -y xvid4,ogg -o glace.R3W5.avi

transcode -i 05-At\ the\ ends\ of\ the\ earth\ -\ Konstantion\ Bronzit\ -annecy\ 1999.mpg -c 0:0:12-10000000 -j 56,8,56,0 -y xvid4 -o ends.trans.avi

transcode -i Ce\ Qui\ Me\ Meut.avi -j 0,8,0,8 -w 5 -R 3 -y xvid4 -o meut.avi
```	

Pour calculer la zone de clipping :
```sh
transcode -i /dev/dvd -T 2,-1 -a 1 -J detectclipping=limit=24 -y null,null
	
nice -19 transcode -i /dev/dvd1 -T 1,-1 -j 16,32,16,32 -I 5 -C 3 --import_asr 3 -w 3 -R 3 -y xvid4 -o porco.avi
```

Normal encoding :
```sh
transcode -i /dev/dvd -T 2,-1 -a 1 -j 70,0,70,0 -Z 720x306 -o poignards.avi -y xvid4
```
	
Multiple pass encoding :
First pass :
```sh
transcode -i /dev/dvd -T 2,-1 -a 1 -j 70,0,70,0 -Z 720x306 -R 1 -y xvid4,null
```
Second pass :
```sh
transcode -i /dev/dvd -T 2,-1 -a 1 -j 70,0,70,0 -Z 720x306 -R 2 -o poignards.avi -y xvid4
```

	-R 3 : constant quantizer encoding
	For -R 2 : -w bitrate
	For -R 3 : -w quantizer (1..31) (1=perfect, 31=bad)
	-C 3 : anti-aliasing

## v4l2-ctl

List all video devices:
```sh
v4l2-ctl --list-devices
```

List the configurable settings of a video device:
```sh
v4l2-ctl -d /dev/video0 --list-ctrls
```

## vlc

 * [How to use VLC to watch any video on Apple TV](http://www.cultofmac.com/406734/how-to-use-vlc-to-watch-any-video-on-apple-tv/).

Run from command line on macos:
```sh
/Applications/VLC.app/Contents/MacOS/VLC 'rtsp://mafreebox.freebox.fr/fbxtv_pub/stream?namespace=1&service=481&flavour=sd'
```

Verbosity level:
	-vvv # or -v 2 --> debug messages
	-vv # -v 1 --> warnings
	-v # -v 0 : errors and standard messages

Interactive version on Linux:
```sh
cvlc
```
On macoS:
```sh
/Applications/VLC.app/Contents/MacOS/VLC -I rc
```

## vokoscreen

Screen recorder.

## xawtv

Video device viewer.

Open camera:
```sh
xawtv -c /dev/video0
```

## youtube-dl

DEPRECATED

Download a youtube video:
```sh
youtube-dl "https://www.youtube.com/watch?v=JVzyoMDF9JA&feature=emb_title"
```

Set output file name:
```sh
youtube-dl -o myfile.mp4 "https://www.youtube.com/watch?v=JVzyoMDF9JA&feature=emb_title"
```

Output file name may be a template, see manpage.

## yt-dlp

Downloader of videos from YouTube and other sites

Python package.

Download:
```sh
yt-dlp https://...
```

Install:
```sh
apt install yt-dlp # Ubuntu
```
