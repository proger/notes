# uname

Get OS type:
```bash
uname
```
Returns `"Darwin"`, `"Linux"`, etc.

Get system info (OS type, machine name, etc):
```bash
uname -a
```
