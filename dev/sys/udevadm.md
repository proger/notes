# udevadm

 * [Hibernate on low battery level](https://wiki.archlinux.org/title/laptop#Hibernate_on_low_battery_level).

Reload rules:
```sh
udevadm control --reload-rules
udevadm trigger
```

Get information about a device:
```sh
udevadm info -n /dev/sdb
```
