# dscl

macos command line tool to manage directory services (including NFS).
It replaces nicl command.

List users:
```bash
dscl . -list /Users
```

Read information about a user:
```bash
dscl . -read /Users/pierrick
```

Search for user with uid <uid>:
```bash
dscl . -search /Users uid <uid>
```

Create a group:
```sh
grp=MyNewGroup
sudo dscl . create /Groups/$grp
sudo dscl . create /Groups/$grp RealName "My Group Title"
sudo dscl . create /Groups/$grp passwd "*"
sudo dscl . create /Groups/$grp gid 799
sudo dscl . create /Groups/$grp GroupMembership myuser
```

Create a new user:
```bash
sudo mkdir /Users/toto
sudo dscl . -create /Users/toto
sudo dscl . -append /Users/toto RealName "Mr. Toto"
sudo dscl . -append /Users/toto PrimaryGroupID 103
sudo dscl . -append /Users/toto UniqueID 512
sudo dscl . -append /Users/toto NFSHomeDirectory /Users/toto
sudo dscl . -append /Users/toto UserShell /bin/bash
sudo dscl . -passwd /Users/toto "tata"
sudo chown -R toto:titi
```
taken from a shell script:
```bash
new_user=toto
sudo $dscl . -create "/Users/$new_user"
sudo $dscl . -append "/Users/$new_user" RealName "$firstname $lastname"
sudo $dscl . -append "/Users/$new_user" NFSHomeDirectory "/Users/$new_user"
sudo $dscl . -append "/Users/$new_user" UserShell /bin/bash   
sudo $dscl . -append "/Users/$new_user" PrimaryGroupID $new_gid
sudo $dscl . -append "/Users/$new_user" UniqueID $new_uid
sudo $dscl . -append "/Users/$new_user" hint ""
sudo $dscl . -append "/Users/$new_user" comment "user account \"$firstname $lastname\" created: $(/bin/date)"
sudo $dscl . -append "/Users/$new_user" picture "/Library/User Pictures/Animals/Butterfly.tif"
sudo $dscl . -append "/Users/$new_user" sharedDir Public
sudo $dscl . -passwd "/Users/$new_user" "$passwd1"
```

Change the UID and GID of a user:
```sh
usr=MyUser
sudo dscl . create /Users/$usr PrimaryGroupID 450
sudo dscl . create /Users/$usr UniqueID 450
sudo chown -R `id -un`:`id -gn` ~
```
