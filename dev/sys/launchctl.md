# launchctl

List services under macOS:
```bash
launchctl list
```

Sart a service under macOS:
```bash
launchctl start org.postfix.master
```

Stop a service under macOS:
```bash
launchctl stop org.postfix.master
```
