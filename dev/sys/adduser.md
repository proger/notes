# adduser

On Alpine.

Create new user with specific UID on BSD and Debian:
```sh
sudo adduser -u 1200 myuser
```

Add a user to sudoers group on BSD and Debian:
```sh
sudo adduser myuser sudo
```
