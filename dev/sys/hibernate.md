# Hibernate

 * [How To Enable Hibernation On Ubuntu (When Using A Swap File)](https://www.linuxuprising.com/2021/08/how-to-enable-hibernation-on-ubuntu.html).

Setup hibernate with an encrypted roo file system and swap file on this root
file system.
1. Add the `resume` module to the kernel, after `encrypt` and `filesystems`. See `mkinitcpio`.
2. Get the UUID of the swap file. See `findmnt`.
3. Get the physical offset the swap file. See `filefrag`.
4. Update Grub `GRUB_CMDLINE_LINUX_DEFAULT` with `resume=UUID=... resume_offset=...`. See `grub`.
5. Reboot.
