# systemctl

Enable a service with systemd (ArchLinux, ...) for starting on bootup:
```sh
systemctl enable myunit
```

For user services:
```sh
systemctl --user enable myunit
```

Check if a service is enabled:
```sh
systemctl is-enabled myunit
```
Returned values:
 - `static`: no `[Install]` section defined for the service.

Enable and start now:
```sh
systemctl enable --now myunit
```

List services:
```sh
systemctl list-units
```

Deactivate a service (stop it):
```sh
systemctl stop myunit
```

Putting a machine to sleep with systemctl:
```sh
systemctl suspend
```

Hibernate:
```sh
systemctl hibernate
```

Suspend then hibernate:
```sh
systemctl suspend-then-hibernate 
```
Set delay for hibernate inside `/etc/systemd/sleep.conf`:
```
HibernateDelaySec=15min
```
