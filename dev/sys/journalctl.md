# journalctl

Print system logs:
```sh
journalctl
```

Print system logs in reverse time order:
```sh
journalctl -r
```

Print logs of a service in particular:
```sh
journalctl -ru myservice
```
