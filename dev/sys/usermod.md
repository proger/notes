# usermod

Add a user to a group in Linux:
```sh
usermod -a -G somegroup someuser
```
To refresh group membership, see `newgrp`.
