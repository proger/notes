# poweroff

Shutdown an Alpine machine:
```bash
poweroff
```
Other commands: `reboot` and `halt`.
