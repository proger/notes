# useradd

On Debian.

Create new user:
```sh
sudo useradd myuser
```

Create a new user with home directory:
```sh
sudo useradd -m myuser
```

See `mkhomedir_helper` to create home folder after creating a user.
