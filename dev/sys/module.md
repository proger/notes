# module
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

 * [Environment Modules](https://modules.readthedocs.io/en/latest/index.html).

Install on Arch Linux:
```sh
yay -S env-modules
```

## Running

## Commands

### avail

List available modules:
```sh
module avail
ml av         # Using abbreviations
```

List only default versions:
```sh
module avail -default
ml av -d        # Using abbreviations
```

List only most recent versions:
```sh
module avail -latest
ml av -L        # Using abbreviations
```

Search by prefix:
```sh
module avail python
```

### display

Same as `show` command.

### help

Display help about a sub-command, or info about a module (`show` command).

### list

List loaded modules:
```sh
module list
```

### load

Load a module:
```sh
module load python/3.7
```

### purge

Unload all modules:
```sh
module purge
```

### restore

Restore an collection:
```sh
module restore mycoll
module restore # Load the default collection
```

### save

Save the current environment into a collection:
```sh
module save mycoll
module save # Save into the default collection
```

### savelist

List saved collections:
```sh
module savelist
```

### saverm

Delete a collection:
```sh
module saverm mycoll
```

### saveshow

List content of a collection:
```sh
module saveshow mycoll
module saveshow # default environment
```

### show

Get info on a module:
```sh
module show python/3.7
```

### switch

Replace a version with another:
```sh
module switch python python/3.6
```

### unload

Unload a module:
```sh
module unload python/3.7
```

Unload all versions of a product:
```sh
module unload python
```

## Modulefiles

Modulefiles are searched in folders defined in `MODULEPATH` environment variable.
