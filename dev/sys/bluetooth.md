# Bluetooth

 * [Bluetooth](https://wiki.archlinux.org/title/Bluetooth).

Install on ArchLinux:
```sh
pacman -S bluez bluez-utils
```

Enable service:
```sh
systemctl enable --now bluetooth
```

For pairing with audio device:
```sh
pacman -S pulseaudio-bluetooth
```
And logout and login to restart properly pulseaudio server (the server is attached to the user):
```sh
systemctl --user status pulseaudio
```

See `bluetoothctl` tool for pairing.

To power on at startup, edit `/etc/bluetooth/main.conf` and set:
```
[Policy]
AutoEnable=true
```
Note: Useless. Already at `true` by default.
