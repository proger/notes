# systemd

 * [systemd](https://wiki.archlinux.org/index.php/Systemd).
 * [unit file syntax (.service)](https://www.freedesktop.org/software/systemd/man/systemd.service.html).
 * [Hibernation](https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Hibernation).

`systemd` configuration for power management is stored in `/etc/systemd/logind.conf`.
It handles how to react when system is idle, and when power/sleep/hibernate button is pressed, or when laptop lid is closed.
To disable short press on power button, and enable long press for poweroff:
```
HandlePowerKey=ignore
HandlePowerKeyLongPress=poweroff
```

See man page `logind.conf`.

In `/etc/systemd/sleep.conf` is enabled or disabled suspend, hibernate and other hydrid modes.

See man page `systemd-sleep.conf`.

In unit files (.service files), specifiers can be used to get system or user information: %h for user's home, %u for user name, etc. See [specifiers](https://www.freedesktop.org/software/systemd/man/systemd.unit.html#Specifiers).

Reload unit files (.service files):
```sh
systemctl daemon-reload
``` 

Target user services (in `~/.config/systemd/user`):
```sh
systemctl --user ...
```
