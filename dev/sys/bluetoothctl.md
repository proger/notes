# bluetoothctl

 * [Bluetooth](https://wiki.archlinux.org/title/Bluetooth).
 * [how to check bluetooth version on my laptop](https://askubuntu.com/questions/591803/how-to-check-bluetooth-version-on-my-laptop).
 * [Bluetooth-Company-Identifiers.csv](https://gist.github.com/angorb/f92f76108b98bb0d81c74f60671e9c67).

| hex | dec |Bluetooth version |
|-----|-----|---------|
| 0x0 |   0 | 1.0b    |
| 0x1 |   1 | 1.1     |
| 0x2 |   2 | 1.2     |
| 0x3 |   3 | 2.0+EDR |
| 0x4 |   4 | 2.1+EDR |
| 0x5 |   5 | 3.0+HS  |
| 0x6 |   6 | 4.0     |
| 0x7 |   7 | 4.1     |
| 0x8 |   8 | 4.2     |
| 0x9 |   9 | 5.0     |
| 0xa |  10 | 5.1     |
| 0xb |  11 | 5.2     |
| 0xc |  12 | 5.3     |
| 0xd |  13 | 5.4     |

Bluetooth manufacturers:
 * `0x03e0`: Actions (Zhuhai) Technology Co., Limited
 * `0x0002`: Intel Corp.

For pairing interactively, run `bluetoothctl` tool.

Inside `bluetoothctl`, run:
```
list                   --> list all controlers  (eventually)
select <MAC_ADDRESS>   --> select the controler (eventually)
power on
scan on
devices                --> list known devices
pair    <MAC_ADDRESS>  --> pair with a new device
connect <MAC_ADDRESS>  --> connect to a known device
exit
```

List paired devices:
```sh
bluetoothctl paired-devices
```

Pair device:
```sh
bluetoothctl pair <MAC_ADDRESS>
```
Unpair device:
```sh
bluetoothctl cancel-pairing <MAC_ADDRESS>
```

Connect:
```sh
bluetoothctl connect <MAC_ADDRESS>
```

Error when trying to connect to device:
```
Failed to connect: org.bluez.Error.Failed br-connection-profile-unavailable
```
Solution:
Run `sudo -e /etc/bluetooth/main.conf` and set `ControllerMode = bredr`.
