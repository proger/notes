# errno

Install on Ubuntu:
```sh
apt install moreutils
```

Get description of error number:
```sh
errno 2
```
