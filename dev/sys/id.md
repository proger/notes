# id

Get user UID and all groups to which he belongs:
```bash
id
```
