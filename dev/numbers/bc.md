# bc

Set the number of decimals to print for output:
```bc
scale=2
```
By default only integer parts are printed (zero decimals).

`;` can be used to separate instructions.
```bc
scale=3;print 1.2356
```

Taking the integer part of a number:
```bc
scale=0;print 1.23
```

Print in hexadecimal:
```bc
obase=16;46
```

Compute on command line:
```sh
echo "100 - 64.43" | bc -l
```
