# calc

 * <http://www.isthe.com/chongo/tech/comp/calc/>

For printing a number in hexadecimal base:
```
base(16),134525
```

To input an hexadecimal number:
```
0x12
```

To change base
```
base(10)  # decimal
base(16)  # hexa
```
 * 2 "binary" binary fractions
 * 8 "octal" octal fractions
 * 10 "real" decimal floating point
 * 16 "hex" hexadecimal fractions
 * -10 "int" decimal integer
 * 1/3 "frac" decimal fractions
 * 1e20 "exp" decimal exponential

To get current base:
```
base()
```
