# seq

Generate a sequence of numbers:
```sh
seq 4 # --> 1 2 3 4
seq 100 104 # --> 100 101 102 103 104
```
