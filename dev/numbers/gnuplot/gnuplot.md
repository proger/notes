# gnuplot

 * [Gnuplot](http://www.gnuplot.info/).
 * [GNUplot tips for nice looking charts from a CSV file](https://raymii.org/s/tutorials/GNUplot_tips_for_nice_looking_charts_from_a_CSV_file.html).

```sh
echo "set terminal png;set output 'toto.png'; plot 'toto.txt'" | gnuplot
```

```gnuplot
plot  "force.dat" using 1:2 title 'Column', "force.dat" using 1:3 title 'Beam'
```

Run a file:
```sh
gnuplot -display ':0.0'
```

```gnuplot
load "myfile.txt"
```

```gnuplot
plot  "force.dat" using 1:2 title 'Column', \
    "force.dat" using 1:3 title 'Beam'
```

Display output on screen (X11):
```gnuplot
set term x11 0
```
Display a second window:
```gnuplot
set term x11 1
```

Output into a PDF:
```gnuplot
set term pdf
set output "myfile.pdf"
```

Plot of a time series (CSV file with date in column 1):
```gnuplot
set xdata time
set timefmt '%d/%m/%Y'
set datafile separator ','
set term pdf
set output "notes.pdf
set yrange [0:20]
set key autotitle columnhead
set key right bottom
set key font ",8"
set xtics rotate
N = system(" head -n 1 notes.csv | sed 's/[^,]//g'|wc -c")
plot for [i=2:N] "notes.csv" using 1:i with lines
```
