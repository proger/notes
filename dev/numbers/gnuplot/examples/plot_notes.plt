set xdata time
set timefmt '%d/%m/%Y'
set datafile separator ','
set term pdf
set output "notes.pdf
set yrange [0:20]
set key autotitle columnhead
set key right bottom
set key font ",8"
set xtics rotate
N = system(" head -n 1 notes.csv | sed 's/[^,]//g'|wc -c")
plot for [i=2:N] "notes.csv" using 1:i with lines
