# Diagrams
<!-- vimvars: b:markdown_embedded_syntax={'dot':'','sh':''} -->

## Flowchart

 * [Flowchart](https://en.wikipedia.org/wiki/Flowchart).
 * https://sketchviz.com/flowcharts-in-graphviz
 * https://sketchviz.com/graphviz-examples
 * https://d2lang.com/

## GraphViz

 * [Graphviz](http://www.graphviz.org).
 * [Documentation](https://graphviz.org/documentation/):
   + [DOT Language](https://graphviz.org/doc/info/lang.html).
   + [Node Shapes](https://graphviz.org/doc/info/shapes.html).
   + [Color Names](https://graphviz.org/doc/info/colors.html).
 * [Graph with orthogonal edges](https://forum.graphviz.org/t/graph-with-orthogonal-edges/2362).
 * [Drawing graphs with dot](https://www.graphviz.org/pdf/dotguide.pdf).


 * [UML Diagrams Using Graphviz Dot](http://www.ffnn.nl/pages/articles/media/uml-diagrams-using-graphviz-dot.php).
 * [Family tree layout with Dot/GraphViz](https://stackoverflow.com/questions/2271704/family-tree-layout-with-dot-graphviz).

 * [Clusters](https://graphviz.org/Gallery/directed/cluster.html).

 * [Guide to Flowcharts in Graphviz](https://sketchviz.com/flowcharts-in-graphviz).

File extension is `.dot` (or `.gv`?).

Comments:
```dot
// My comment
```

Force nodes to be on the same row:
```dot
digraph G {
    { rank=same; 71; 1; 278; }
}
```

Force order of nodes on the same row from left to right:
```dot
digraph G {
    { rank=same; edge[style=invisible]; rankdir=LR; 71->1->278; }
}
```
**Attention**: this uses invisible edges, that influence the drawing of the visible ones.

Orient graph from bottom to top instead of top to bottom:
```dot
digraph G {
    rankdir=BT;
}
```

Set graph title:
```dot
digraph G {
    label="My title";
    labelloc="t"; // Write on the top of the page
    labeljust="l"; // Left justify
}
```

Set default values for all nodes:
```dot
digraph G {
    node [shape=box];
}
```

Set values for a node:
```dot
digraph G {
    mynode [shape=box;style=filled;label="My Node";fillcolor=red];
}
```

Set values for an edge:
```dot
digraph G {
    A->B[label="My Label"];
}
```

Define a subgraph (with `dot` the subgraph must start with the `cluster` prefix).
Names must be unique. If two subgraphs have the same name, they are merged.
```dot
digraph G {
    subgraph cluster2 {
        label="My Label"
        fillcolor=green;
        style=filled;
        node1; node2; node3;
        subgraph cluster_included {
            fillcolor=red;
            style=filled;
            A; B; C; D;
        }
    }
}
```

### gc

Count edges, nodes, etc.

### dot

Draw directed graphs.

See graphviz.

Generate a graph in SVG format:
```sh
dot -Tsvg mygraph.gv -o mygraph.svg
```

## PlantUML

Conversion of UML diagrams from text description to SVG, PNG, ...

Install on Ubuntu:
```sh
apt install plantuml
```

## Dia

 * [Git repos](https://gitlab.gnome.org/GNOME/dia).

Key shortcuts:
	Ctrl E : Fit diagram on screen
	Ctrl + : Zoom in
	Crtl - : Zoom out
	Arrows : Move diagram

Export to a file and exit:
```sh
dia -e <output_file> -t <format> <input_file>
```

Remark: option `-s` to set size of output file does not work with PNG or JPG
 format. To get nice output with those formats, set paper format to A0 for each
.dia file inside the Dia editor.
Another solution is to export in SVG and then use ImageMagick to convert to PNG
or JPG.

Formats:
	cgm (Computer Graphics Metafile, ISO 8632)
	dia (Native dia diagram)
	dxf (Drawing Interchange File)
	eps or eps-builtin or eps-pango (Encapsulated PostScript) The format specifications eps and eps-pango both use the font renderer of the Pango library, while eps-builtin uses a dia specific font renderer. If you have problems with Pango rendering, e.g. Unicode, try eps-builtin instead.
	fig (XFig format)
	mp (TeX MetaPost macros)
	plt or hpgl (HP Graphics Language)
	png (Portable Network Graphics)
	shape (Dia Shape File)
	svg (Scalable Vector Graphics)
	tex (TeX PSTricks macros)         --> BAD OUTPUT, the figure is full of bugs.
	wpg (WordPerfect Graphics)
	wmf (Windows MetaFile)

Remove splash screen:
```sh
dia -n ...
```

### dia under macos

Under MacOS-X, dia is no more available in Homebrew.
A MacOS-X integrated version must be downloaded and installed.

Unfortunately it doesn't allow to use options from command line.
The script /Applications/Dia.app/Contents/Resources/bin/dia that launches dia-bin, doesn't accept arguments.
The solution is to modify the last line of the script so it allows passing of arguments and doesn't run in GUI mode by default.
```sh
exec "$CWD/dia-bin" --integrated
```
becomes:
```sh
# From MacOS-X GUI, dia script is called with the command "dia -psn_0_??????"
if [ $# -gt 1 ] ; then
	"$CWD/dia-bin" "$@"
else
	exec "$CWD/dia-bin" --integrated
fi
```
Then add `/Applications/Dia.app/Contents/Resources/bin` to the PATH.

## Umbrello

UML diagraming tool.

Install on Ubuntu:
```sh
apt install umbrello
```

Run:
```sh
umbrello5
```

## yEd

 * [yEd](https://www.yworks.com/products/yed).

Professional diagraming tool. Free to use. Crossplatform (Java).
