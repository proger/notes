# ris2xml

Convert ris citation formation to XML format.

Install on ArchLinux:
```sh
yay -S bibutils
```

Convert to XML:
```sh
ris2xml myfile.ris >myfile.xml
```

Convert to BibTeX:
```sh
ris2xml myfile.ris | xml2bib >myfile.bib
```
