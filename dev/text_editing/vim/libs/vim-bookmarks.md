# vim-bookmarks

 * [vim-bookmarks](https://github.com/MattesGroeger/vim-bookmarks).

`mm` for toggling bookmark on current line.
`ma` to open quickfix window.
