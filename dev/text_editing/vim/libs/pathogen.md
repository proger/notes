# pathogen

DEPRECATED because of native package loading.

Pathogen is a package for easying package installation.

 * [Pathogen](http://www.vim.org/scripts/script.php?script_id=2332).
 * [The Modern Vim Config with Pathogen](http://tammersaleh.com/posts/the-modern-vim-config-with-pathogen/).

With Pathogen, packages are now installed inside ~/.vim/bundle folder in a dedicated folders, and are automatically configured.
