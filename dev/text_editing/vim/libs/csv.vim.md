# csv.vim

 * [csv.vim](https://github.com/chrisbra/csv.vim).

Command        | Description
-------------- | --------------------
WhatColumn     | Get index of current column.
WhatColumn!    | Get name of current column (read from header).
ArrangeColumn  | Align columns.
ArrangeColumn! | Align columns (force recalculating).
SumCol         | Sum of a column.
Header         | Open top window with one line header.
Header 4       | Open top window with 4 lines header.
VHeader 2      | Open left window with 2 first columns.
VHeader 2!     | Open left window with 2nd column.
Header!        | Close header window.
AddColumn      | Insert new column.
