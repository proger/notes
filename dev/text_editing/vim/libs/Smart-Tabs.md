# Smart-Tabs

 * [Smart-Tabs](https://github.com/vim-scripts/Smart-Tabs).

Use normal tabs for the beginning of a line, and expand tabs as spaces anywhere
else. 
