# SyntaxRange

 * [SyntaxRange](https://github.com/vim-scripts/SyntaxRange).

Enable syntax highlighting of a language embedded into another.
