# SyntaxAttr.vim

 * [SyntaxAttr.vim](https://github.com/vim-scripts/SyntaxAttr.vim).

Displays the syntax highlighting attributes of the character under the cursor.
