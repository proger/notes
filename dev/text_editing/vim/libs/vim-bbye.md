# vim-bbye

 * [vim-bbye](https://github.com/moll/vim-bbye).

Bbye allows you to do delete buffers (close files) without closing your windows
or messing up your layout.
