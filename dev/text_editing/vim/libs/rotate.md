# rotate

 * [rotate](https://www.vim.org/scripts/script.php?script_id=1186).
 * [rotate.vim](https://github.com/vim-scripts/rotate.vim).

Rotate text clockwise or counter clockwise.
`Rot l` to rotate left and `Rot r` to rotate right on a visual block.
