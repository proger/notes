# vimteractive

 * [vimteractive](https://github.com/williamjameshandley/vimteractive).

Vimteractive aims to provide a robust and simple link between text files and
interactive interpreters.
