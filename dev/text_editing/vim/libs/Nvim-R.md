# nvim-R

 * [For R: nvim-R](https://hpcc.ucr.edu/manuals_linux-cluster_terminalIDE.html#for-r-nvim-r).
 * [nvim-R](https://github.com/jalvesaq/Nvim-R).

Start an R session (does not need an opened .R file):
```vim
call StartR("R")
```

Nvim-R commands:
 * Intialize a connected R session with `\rf` (Once an .R file has been opened).
 * `\l`: send current line to R session.
 * `\bb`: send current block.
 * `\cc`: send current chunk.
 * `\ff`: send current function.
 * `\ss`: send selection.
 * `\ro`: open Object Browser.
