# vim-commentary

 * [vim-commentary](https://github.com/tpope/vim-commentary).

Comment stuff out.

command | Description
------- | -------------
`gcc`   | comment out or uncomment a line.
`gc`    | to comment out a selected block.
