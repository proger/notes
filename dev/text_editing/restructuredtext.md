# Restructured text

 * [reStructuredText technical specification](http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html).

List:
```rst
A bullet list:

 - first item.
 - second item.
```

Code block:
```rst
Here is some code::
  My code here...
```
