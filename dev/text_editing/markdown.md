# Markdown

 * [6 Markdown Editors That Play Nice With Google Drive](http://www.makeuseof.com/tag/6-markdown-editors-play-nice-google-drive/).
 * [Grip -- GitHub Readme Instant Preview](https://github.com/joeyespo/grip).

Markdown is a Lightweight markup languages (LML).

Markdown files are text files with markups, that markdown processes and transform into html files.
Markdown file extension is `.md`.

See [official site](http://daringfireball.net/projects/markdown/).

## URL reference

 * [A link to something](https://some.site/with/a/file.html).

 * <https://some.site/with/a/file.html>.

## Bullet lists

 * Level 1 item A
   + Level 2 item a (indented with two spaces).
   + Level 2 item b (indented with two spaces).
 * Level 1 item B

## Reference to another section of the same file

A reference to chapter [Insert an image](#insert-an-image).

## Reference to a local file

[Some file](myfile.md)

## Insert an image

```markdown
![alternative text](path/to/my/image.png "Text displayed when mouse is over.").
```

## Footnote

That's some text with a footnote.[^1]

[^1]: And that's the footnote.

Here another footnote[^longnote].

[^longnote]: Here's one with multiple blocks.

	Subsequent paragraphs are indented to show that they
	belong to the previous footnote.

	        { some.code }

	The whole paragraph can be indented, or just the first
	line.  In this way, multi-paragraph footnotes work like
	multi-paragraph list items.


