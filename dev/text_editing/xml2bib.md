# xml2bib

Convert citation in XML format to BibTeX format.

Install on ArchLinux:
```sh
yay -S bibutils
```

Convert to XML:
```sh
xml2bib myfile.xml >myfile.bib
```
