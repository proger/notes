# nbib2xml

Convert citation in nbib format to XML format.

Install on ArchLinux:
```sh
yay -S bibutils
```

Convert to XML:
```sh
nbib2xml myfile.nbib >myfile.xml
```

Convert to BibTeX:
```sh
nbib2xml myfile.nbib | xml2bib >myfile.bib
```
