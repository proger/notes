# wakeonlan

On Debian, see [Set up Wake On LAN (WOL) on a Debian Server](https://www.lisenet.com/2013/set-up-wake-on-lan-wol-on-a-debian-wheezy-server/) or <https://wiki.debian.org/WakeOnLan>.

To see if a card has WOL enabled, run `ethtool`:
```bash
ethtool my_card
```
and look for the lines `Supports Wake-on: g` (card supports WOL) and `Wake-on: (d|g)` (`d` means WOL is disabled, and `g` enabled).
To enable WOL:
```bash
ethtool -s my_card wol g
```

Wake a machine:
```bash
wakeonlan <MAC address>
```
