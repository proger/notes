# ping

Ping server:
```sh
ping myserver
```

Ping only once:
```sh
ping -c 1 myserver
```

Set timeout (in case of no response) to 1 second:
```sh
ping -W 1 myserver
```
