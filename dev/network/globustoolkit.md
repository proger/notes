# Globus Toolkit

 * [Globus Toolkit](https://github.com/globus/globus-toolkit),

## Use of X509 proxy certificate

`X509_USER_PROXY` env var is used in functions:
 * `globus_gsi_sysconfig_get_proxy_filename_unix()` which does not seem to be used anywhere.
 * `environment_check()` which is used only in `grid-cert-diagnostics.c` program.
