# showmount

Macos command to list shared disk by server:
```sh
showmount -e myserver
```
