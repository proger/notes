# ZeroConf

 * [ZeroConf](https://doc.ubuntu-fr.org/zeroconf).

Automatic name resolution, MDNS, service publication, ...
Activated by default on Ubuntu. Same technology as Apple's *Bonjour*, aka *RendezVous*.

Allows to reach all local computers on .local network.
Example:
```sh
ping mymachine.local
```
