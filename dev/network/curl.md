# curl

curl is available on Windows PowerShell as `curl.exe`, not be confused with the
`Invoke-WebRequest` alias `curl`.

Download a file with ftp:
```sh
curl -O "ftp://mysite.fr/my/file.txt"
```

Follow page relocation:
```sh
curl -L -O "http://mysite.fr/my/file.txt"
```

Rename output file:
```sh
curl -o my_filename.txt "http://mysite.fr/my/file.txt"
```

Get information:
```sh
curl -gsLI "http://mysite.fr/my/file.txt"
```
`-g` option turns off special interpretation of '[]' and '{}'.

Make `curl` fails (status > 0) when server fails (i.e.: returns an error code):
```sh
curl --fail-with-body ...
```

Choose request method:
```sh
curl -X POST ...
```
Default is `GET`.

Providing a header:
```sh
curl -H "Content-Type: application/json" ...
```

Send JSON data:
```sh
curl -H "Content-Type: application/json" -d'
{
  "some_key": "my_value"
}'
```
