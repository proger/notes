# lspci

List network cards:
```sh
lspci -kvd::280
```

List all cards:
```sh
lspci -vnn
```
