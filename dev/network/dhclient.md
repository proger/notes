# dhclient

Renew client lease on specific card:
```sh
dhclient -r enp2s0 # Revoke
dhclient enp2s0    # Obtain a new lease
```
