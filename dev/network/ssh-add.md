# ssh-add
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','sshconfig':''} -->

Add manually a specific key to the ssh agent:
```sh
ssh-add .ssh/myprivatekey
```

To enable automatic addition of keys to ssh-agent, set the following option
into `~/.ssh/config`:
```sshconfig
AddKeysToAgent=yes
```
