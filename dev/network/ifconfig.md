# ifconfig

Deprecated (See <http://www.archlinux.org/news/deprecation-of-net-tools/>).

Install on ArchLinux:
```sh
pacman -S net-tools
```

Get IP of a card:
```sh
ifconfig mycard
```
