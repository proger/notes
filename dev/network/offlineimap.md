# offlineimap

 * [OfflineIMAP](https://wiki.archlinux.org/index.php/OfflineIMAP).
 * [OfflineIMAP Python3 code](https://github.com/OfflineIMAP/offlineimap3.git).

For documentation about the configuration file `~/.offlineimaprc`, see
`/usr/share/offlineimap/offlineimap.conf`.

Synchronize locally with an IMAP account.

On Archlinux:
```sh
pacman -S offlineimap
```

Install on macos:
```sh
brew install offlineimap
```
or with MacPorts:
```sh
sudo port install offlineimap
```

To use SSL, set for each account:
```offlineimaprc
sslcacertfile = /etc/ssl/certs/ca-certificates.crt
```
On macos, be careful of reading access for everyone for
`/etc/ssl/certs/ca-certificates.crt`.
If the file does not exist, install `brew install ca-certificates`, and then
copy installed file
(`/usr/local/Cellar/ca-certificates/2023-08-22/share/ca-certificates/cacert.pem`)
to `/etc/ssl/certs/ca-certificates.crt`.

 * [Folder filtering and Name translation](https://offlineimap.readthedocs.org/en/latest/nametrans.html).
 * [Use Mac OS X's Keychain for Password Retrieval in OfflineIMAP](https://blog.aedifice.org/2010/02/01/use-mac-os-xs-keychain-for-password-retrieval-in-offlineimap/).

Run manually:
```sh
offlineimap # For all default accounts.
offlineimap -a myaccount # For one specific account.
```

For running offlineimap as daemon under Ubuntu, see `/usr/share/doc/offlineimap/examples`.

For running offlineimap as daemon under Ubuntu, run:
```bash
cat >/etc/systemd/user/offlineimap.service <<EOF
[Unit]
Description=OfflineIMAP Service
After=network.target

[Service]
Type=simple
ExecStart=/usr/bin/offlineimap
EOF
cat >/etc/systemd/user/offlineimap.timer <<EOF
[Timer]
OnUnitInactiveSec=120s
Unit=offlineimap.service
EOF
chmod a+r /etc/systemd/user/offlineimap.*
```
Then as a user:
```bash
systemctl --user daemon-reload
systemctl --user start offlineimap.timer
systemctl --user start offlineimap.service
```
See [OfflineIMAP: Periodically fetch emails with systemd's timers](https://kdecherf.com/blog/2012/12/23/offlineimap-periodically-fetch-emails-with-systemds-timers/) and [Integrating OfflineIMAP into systemd](http://www.offlineimap.org/doc/contrib/systemd.html).
