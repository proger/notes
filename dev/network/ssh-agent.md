# ssh-agent

 * [SSH agent forwarding and tmux done right](https://blogsystem5.substack.com/p/ssh-agent-forwarding-and-tmux-done).

See also `ssh-add`.

Start the ssh agent (per-user basis):
```sh
eval $(ssh-agent)
```

Get the PID of the current ssh-agent:
```sh
echo $SSH_AGENT_PID
```

Kill the current ssh agent:
```sh
ssh-agent -k
```
