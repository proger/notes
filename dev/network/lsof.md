# lsof

Lists open files:
```sh
lsof
```

Lists open files in a specific folder:
```sh
lsof /my/folder
```

Lists processes that uses a port:
```sh
lsof -i:8080
```
