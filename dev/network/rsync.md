# rsync

Use verbose flag to print processed files:
```bash
rsync -v ...
```

Synchronizing deletion of files in the source:
```bash
rsync -avP --delete teddy:/home/data/documents .
```

Specify remote shell to use:
```bash
rsync -e ssh
```
or
```bash
export RSYNC_RSH=ssh
rsync ...
```

ERROR "rsync: failed to set times on":  add -O option, it tells rsync to omit directories when preserving times.
```bash
rsync -O ...
```

Compress:
```bash
rsync -z ...
```

Recurse in sub-directories:
```bash
rsync -r ...
```

Exclude files:
```bash
rsync --exclude '*~' ...
rsync --exlude '*~' --exlude '.DS_Store' --delete-excluded ... # also delete excluded files from dest dirs
rsync --exclude-from=FILE # read include patterns from FILE
```

To synchronize two directories:
```bash
rsync --delete /.../folder1/ /.../folder2
```
The slash at the end of the source path tells to synchronize all files.

Print the progress of transfer:
```bash
rsync --progress
```

Authorize partial transfer (keep partially transferred files):
```sh
rsync --partial
```
Or for enabling for --partial and --progress at the same time:
```sh
rsync -P
```

Regular sync (revursive, links, times), doesn't synchronize the permissions:
```bash
rsync -rlt ...
```
