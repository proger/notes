# iw

 * See [iwd](iwd.md) for the associated daemon.

Get card name:
```sh
iw dev | grep Interface
```

Get SSID:
```sh
iw dev | grep ssid
```

Get info on a card:
```sh
iw mycard info
```
