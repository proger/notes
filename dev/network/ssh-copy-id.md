# ssh-copy-id

Copy public key to a remote machine to authorize public key authenticaton.
```sh
ssh-copy-id my.host.url
```
