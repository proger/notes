# nc

GNU netcat.

Test if a port is open:
```sh
nc -zv myhost 631
```
