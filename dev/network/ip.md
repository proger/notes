# ip
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

To get a list of network devices:
```sh
ip link show
```
or
```sh
ip addr
```
