# iwctl

 * [iwd](https://wiki.archlinux.org/title/Iwd).

Start interactive:
```sh
sudo iwctl
```

List devices:
```sh
iwctl device list
```

If no device is shown, try to list physical adapters:
```sh
iwctl adapter list
```
If nothing is shown, try with `lspci`:
```sh
lspci -kvd::280
```
If the network controller is a Broadcom, then install Broadcom packages:
```sh
pacman -S broadcom-wl-dkms
```

Now search for networks on your device:
```sh
iwctl station mydevice scan
```
And print the found networks:
```sh
iwctl station mydevice get-networks
```

Connect:
```sh
iwctl --passphrase passphrase station device connect SSID
```
