# Grid Community Toolkit (GCT)
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

A continuation of the Globus project, a fork of OpenSSH that implements X509
certificate handling for authentication.

Tools: `gsissh`, `gsiscp`, `gsisftp`.

 * [gct Repository](https://github.com/gridcf/gct).
 * [GCT 6.2 Component Guide to Public Interfaces: GSI-OpenSSH](https://gridcf.org/gct-docs/latest/gsiopenssh/pi/index.html).

To compile and install in specific directory:
```sh
# Install libtool in conda environment;
# conda install -y libtool
git clone https://github.com/gridcf/gct.git
cd gct
mkdir -p $HOME/tmp/gct/usr/local/globus-6
export CFLAGS="-I$HOME/tmp/gct/usr/local/globus-6/include -I$CONDA_PREFIX/lib"
export LDFLAGS="-L$HOME/tmp/gct/usr/local/globus-6/lib -L$CONDA_PREFIX/include"
autoreconf -i
./configure --prefix=$HOME/tmp/gct/usr/local/globus-6
make
make install
```
