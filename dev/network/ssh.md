# ssh
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

 * [RFC 4254](https://www.rfc-editor.org/rfc/rfc4254).

Generate private and public keys:
```sh
ssh-keygen 
```

Login without password. In `/etc/ssh/sshd_config`:
```
RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
```
Then:
```sh
ssh-copy-id -i ~/.ssh/id_rsa.pub username@remotebox
```

Using local tunneling for accessing mail server:
```sh
ssh -gNL 1993:imap.mail.me.com:993 server.addr
```
Local port used is `1993`. Everything going to this local port will be forwarded
to `imap.mail.me.com:993` through `server.addr:22`.
See [List of TCP and UDP port numbers](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers)
for choosing a custom port number.
Ephemeral ports: C000 (49152) to FFFF.

A tunnel to connect at `my.faraway.server:80` through `user@my.other.computer`
using local address `localhost:8080`:
```sh
ssh -gNL 8080:my.faraway.server:80 user@my.other.computer
```

Protocol | Default/usual port
-------- | --------------------------------
http     | 80
https    | 443
ssh      | 22

Print debug information
```sh
ssh -v ...
```

X11 forwarding:
```sh
ssh -X ...
```
If you see the error `X11 forwarding request failed on channel 0`, edit
`/etc/ssh/sshd_config`, and set the following lines:
```
X11Forwarding yes
X11UseLocalhost no
```
then restart ssh service:
```sh
systemctl restart sshd
```

## Config file

The `.ssh/config` file can contain host nicknames and specific or generic configurations for matching hosts.

Help: `man ssh_config`.

Definition of a host nickname:
```sshconfig
Host myhost
    Hostname myhost.foo.zz
```

Configuration:
```sshconfig
Match host gitlab.com
    IdentityAgent SSH_AUTH_SOCK
    IdentityFile  ~/.ssh/id_ed25519_%h
    AddKeysToAgent yes
    ForwardX11 no
    ForwardX11Trusted no
```

Tokens (see manpage):
 * `%h`: remote hostname.
 * `%u`: local username.
 * `%r`: remote username.

## Subsystem

 * [Subsystems](https://docstore.mik.ua/orelly/networking_2ndEd/ssh/ch05_07.htm).

On the server side, in `/etc/ssh/sshd_config`, alias can be defined for
commands:
```
Subsystem myalias /my/command
```

This is useful to hide the real command to the user, and thus be able to change
it at any time.

The command is used as any ssh command, but we must specify the `-s` flag:
```sh
ssh -s my.server myalias --some-option
```
