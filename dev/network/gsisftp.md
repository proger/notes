# gsisftp

Connect using X509 certificate (`X509_USER_CERT`, `X509_USER_PROXY` and `X509_USER_KEY` must be defined):
```sh
gsisftp mylogin@server.to.connect.to
```

Example of values for env vars:
```sh
X509_USER_CERT=/.../.globus/usercert.pem
X509_USER_PROXY=/.../.globus/x509up...
X509_USER_KEY=/.../.globus/userkey.pem
```
