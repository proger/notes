# netstat

**Deprecated**. See `ss`.

Check if a port is open and how:
```sh
netstat -apn | grep :631
```
