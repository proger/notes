# sftp

SSH File Transfer Protocol.

 * [SFTP version 3](https://www.ietf.org/archive/id/draft-ietf-secsh-filexfer-02.txt).
 * [SFTP version 13](https://www.ietf.org/archive/id/draft-ietf-secsh-filexfer-13.txt).

A secured version of `ftp`, distributed alongside `ssh` and `scp`.
