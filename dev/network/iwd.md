# iwd

 * [iwd](https://wiki.archlinux.org/title/Iwd).

Install on ArchLinux:
```sh
pacman -S iwd
```

Enable and start the service:
```sh
systemctl enable --now iwd
```
Make sure NetworkManager and wpa_supplicant are disabled:
```sh
systemctl disable --now NetworkManager
systemctl disable --now wpa_supplicant
```
and that the DHCP client daemon is running:
```sh
systemctl enable --now dhcpcd
```

Now use [iwctl](iwctl.md) to configure.
