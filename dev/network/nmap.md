# nmap

Check that a port is open:
```sh
nmap -p 8180 mymachine
```

List all open ports:
```sh
nmap mymachine
```
