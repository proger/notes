# wol (wake on lan)

See [wakeonlan](wakeonlan.md) to learn how to enable WOL on a card.

Wake a machine:
```bash
wol <MAC address>
```
