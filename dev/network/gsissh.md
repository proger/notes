# gsissh

A version of OpenSSH `ssh` that accepts X.509 certificates.
Part of the [Globus Toolkit](https://github.com/globus/globus-toolkit),
developed by the University of Chicago. See `globustoolkit.md`.
Support was ended in 2018.

The [Grid Community Toolkit](https://gridcf.org/) has forked the project.
It is open source. See documentation at
[Grid Community Toolkit Official Documentation](https://gridcf.org/gct-docs/#_intro)
and code source at [Grid Community Toolkit](https://github.com/gridcf/gct).

Connect to topaze:
```sh
gsissh rogerpie@topaze.ccc.cea.fr
```
