# ssh-keygen

Create a key in `.ssh/id_rsa` and `.ssh/id_rsa.pub` files:
```sh
ssh-keygen
```

Create a key using a custom filename:
```sh
ssh-keygen -f theprivatekeyfile
```

Create a key with ED25519 in a custom file:
```sh
ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519_gitlab.com
```

Use a custom comment:
```sh
ssh-keygen -C "john.smith@some.place"
```

Change password on a private key file:
```sh
ssh-keygen -p -f theprivatekeyfile
```

View private key:
```sh
ssh-keygen -y -f theprivatekeyfile
```

Remove all keys belonging to a hostname:
```sh
ssh-keygen -f ~/.ssh/known_hosts -R 1.2.3.4
```
