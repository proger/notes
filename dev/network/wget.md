# wget

Download recursively:
```bash
wget -r http://some.site.fr/
```

Resume a download:
```bash
wget -c http://some.site.fr/myfile.zip
```

Set file output:
```sh
wget -O myfile.html http://some.site.fr/some/page.html
```

```bash
wget -i blabla -o zop http://fsgjkbnkfjg.bgjnfdgb/dfbkjgn.xml
```

Quiet:
```bash
wget -q -O myfile.html http://some.site.fr/some/page.html
```
