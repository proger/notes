# samba

 * [Samba](https://wiki.archlinux.org/index.php/samba).

To setup Samba under macOS, declare the workgroup:
	System Preferences -> Network -> [your network device] -> Advanced -> WINS -> Workgroup
The path from a Windows computer is:
	\\<server name\<shared folder>

Access folders on samba share:
```bash
smbclient -L 123.456.789.012 -U INTRA\\PR228844
```

Get the netbiosname from a computer:
```bash
nbtscan 123.456.789.012
```

Install a Samba server on Debian:
```bash
apt-get install samba
```

Configuration in `/etc/samba/smb.conf`:
```
[global]
server string = Vampire
workgroup = Workgroup
netbios name = Vampire
public = yes
encrypt passwords = true

[projects]
path = /home/zero/projects
read only = no
writeable = yes
valid users = zero
comment = all_my_projects
```

Check the conf file:
```bash
testparm
```

Be careful that all files under `/var/lib/samba` and `/var/cache/samba` belong to `root:root`.

Create a user:
```bash
smbpasswd -a a_username
```
Samba uses existing Linux users, but has its own list of passwords.

Restart samba:
```bash
/etc/init.d/samba restart
```

To debug samba demon, run:
```sh
sudo /usr/bin/smbd --foreground --no-process-group -i -S -d 5
```

Start `smdb` and `nmdb` as daemons:
```sh
sudo /usr/bin/smbd -D
sudo /usr/bin/nmbd -D
```

On ArchLinux, to start Samba service:
```bash
sudo systemctl start smb
```

To configure samba with guest access:
```
[global]
security = share

[myshareddir]
guest ok = yes
```

#### Sambashare

Use sambashare to allow non-root users to create their shared drives:
```bash
mkdir /var/lib/samba/usershares
groupadd -r sambashare
chown root:sambashare /var/lib/samba/usershares
chmod 1770 /var/lib/samba/usershares
```

In `/etc/samba/smb.conf`:
```
[global]
  usershare path = /var/lib/samba/usershares
  usershare max shares = 100
  usershare allow guests = yes
  usershare owner only = yes
```

Add users to the group:
```bash
gpasswd sambashare -a <username>
```

Then as a user:
```bash
net usershare add <sharename> <abspath> [comment] [<user>:{R|D|F}] [guest_ok={y|n}]
net usershare delete <sharename>
net usershare list <wildcard-sharename>
net usershare info <wildcard-sharename>
```
Where `R`, `D` and `F` stand for Read-only, Deny and Full respectively.


