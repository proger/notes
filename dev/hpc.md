# HPC
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','c':''} -->

Books:
 * [Synchronization Algorithms and Concurrent Programming](https://www.amazon.com/Synchronization-Algorithms-Concurrent-Programming-Taubenfeld/dp/0131972596/ref=sr_1_1?s=books&ie=UTF8&qid=1472635893&sr=1-1&keywords=synchronization+algorithms+and+concurrent+programming).
 * [The Art of Multiprocessor Programming, Revised Reprint](https://www.amazon.com/Art-Multiprocessor-Programming-Revised-Reprint/dp/0123973376/ref=sr_1_2?s=books&ie=UTF8&qid=1472635893&sr=1-2&keywords=synchronization+algorithms+and+concurrent+programming).
 * [An Introduction to Parallel Programming](https://www.amazon.com/Introduction-Parallel-Programming-Peter-Pacheco/dp/0123742609/ref=sr_1_1?s=books&ie=UTF8&qid=1472635961&sr=1-1&keywords=parallel+programming).
 * [CUDA by Example: An Introduction to General-Purpose GPU Programming](https://www.amazon.com/CUDA-Example-Introduction-General-Purpose-Programming/dp/0131387685/ref=sr_1_1?s=books&ie=UTF8&qid=1472635991&sr=1-1&keywords=cuda+by+example+an+introduction+to+general-purpose+gpu+programming).
 * [Programming Massively Parallel Processors, Second Edition: A Hands-on Approach](https://www.amazon.com/Programming-Massively-Parallel-Processors-Second/dp/0124159923/ref=sr_1_1?s=books&ie=UTF8&qid=1472636023&sr=1-1&keywords=programming+massively+parallel+processors).

 * [Groupe Calcul](https://calcul.math.cnrs.fr/).
 * <http://www.teratec.eu/>.
 * [PRACE](http://www.prace-project.eu/).
 * [PRACE events](https://events.prace-ri.eu/).
 * [CINECA](http://www.cineca.it/).

## Kokkos

 * [Kokkos](https://github.com/kokkos).

The Kokkos C++ Performance Portability Ecosystem is a production level solution
for writing modern C++ applications in a hardware agnostic way.

There exists also a Python framework: [PyKokkos](https://github.com/kokkos/pykokkos).

## Taxonomy

### Flynn's taxonomy

Data \ instructions Single instruction      Multiple instruction
------------------- --------------------    ----------------------
Single data         SISD                    MISD
Multiple data       SIMD                    MIMD

### SISD (Single Instruction, Single Data)
Von Neumann architecture.

### SIMD (Single Instruction, Multiple Data)
It describes computers with multiple processing elements that perform the same operation on multiple data simultaneously. Thus, such machines exploit data level parallelism.

The first use of SIMD instructions was in VECTOR SUPERCOMPUTERS of the early 1970s.

SIMD (Single Instruction, Multiple Data)

It can be thought of as having a single control unit and multiple ALUs.

In "classical" SIMD system, the ALUs must operate synchronously.

#### Vector processors
Operate on arrays or vectors of data.
Characteristics:
_ vector registers (from 4 to 128 64-bit elements)
_ vectorized and pipelined functional units (addition on 2 vectors, ...)
_ vector instructions (language machine instruction made for vectors: add, load, store, ...)
_ interleaved memory. The memory system is divided in "banks" of memory. There's a delay in accessing several times the same bank. But it's fast to access successive banks that follow each other.
_ strided memory access: access of elements of a vector located at fixed intervals (stride).
_ hardware scatter/gather: write (scatter) or read (gather) of elements of a vector located at irregular intervals.

#### Graphics processing unit (GPU)
Not pure SIMD.

_ large number of ALUs on each GPU
_ very high rates of data movement
_ hardware multithreading
_ several GPUs on each card

Drawback: many threads processing a lot of data are needed to keep the ALUs busy, so GPUs may have relatively poor performance on small problems.

### MISD (Multiple Instruction, Single Data)
Many functional units perform different operations on the same data. 

PIPELINE architectures belong to this type, though a purist might say that the data is different after processing by each stage in the pipeline. 

FAULT-TOLERANT COMPUTERS executing the same instructions redundantly in order to detect and mask errors, in a manner known as task replication, may be considered to belong to this type.

### MIMD (Multiple Instruction, Multiple Data)
MIMD machines can be of either shared memory or distributed memory categories. These classifications are based on how MIMD processors access memory. Shared memory machines may be of the bus-based, extended, or hierarchical type. Distributed memory machines may have hypercube or mesh interconnection schemes.

MIMD (Multiple Instruction, Multiple Data)

In MIMD systems each processor operates at its own pace, which means the system is usually asynchronous.

#### Shared Memory Model
A collection of autonomous processors is connected to a memory system via an interconnection network, and each processor can access each memory location.
The processors communicate implicitly by accessing shared data structures.

The most widely used shared memoy systems use one or more multicore processors.
Each core has a level 1 cache, while other caches may or may not be shared between the cores.

##### UMA (Uniform Memory Access)
All processors have access to the same main memory.
Access time to memory is uniform accross all processors.

##### NUMA (Non-Uniform Memory Access)
Each processor have direct access to a block of main memory, and can access others' blocks of main memory through special hardware built into the processors.
Access to the reserved block of main memory for a processor is quicker than access to others' blocks.

##### Interconnection networks

BUSES
A bus connect all the processors to the main memory through parallel wires.
Since the communication bus (parallel wires + hardware chip controler) are shared among the processors, as the number of processors increase there will be contention on the bus.

CROSSBARS
Interesting for high number of processors.
This is also called "switched interconnects".
Main memory is divided in modules, and each processor is connected individually to one of the memory module. So several processors can be connected at a given time to different memory modules.

P1  P2  P3  P4
|   |   |   |
M1-/    |   |   |
M2------/   |   |
M3----------+---/
M4----------/

#### Distributed Memory Model
Each processor is paired with its own private memory, and the processor-memory pairs communicate over an interconnection network.
The processors usually communicate explicitly by sending messages or by using special functions that provide access to the memory of another processor.

The most widely available distributed memory systems are called clusters.

Nowadays, each node of a distributed system is also a shared-memory system in itself. Such a coupled system (distributed + shared) was called before a "hybrid system".

In a grid (nodes + communication network), nodes may be built from different types of hardware, in which case the system is said to be heterogeneous.

##### Interconnection networks

Bisection width: measure of connectivity (number of simultaneous communications). When dividing the nodes in two equal sets, this is the number of simultaneous connections that can take place between the two halves in the worst case (i.e.: we must evaluate all combinations of sets and take the worst result for all). Another way to compute the bisection width, it to find the minimum number of links to remove in order to cut the nodes in two halves.

Bisection bandwitdh = (bisection width) * (bandwidth of links)

latency = time between sending of message and reception of this message

message transmission time = latency + size_of_message / bandwitdh

###### Direct interconnect
Each switch is directly connected to a processor-memory pair, and then switches are connected together.

RING
number of links = 2p (each processor is linked to 2 other processors, each switch has 3 links)
bisection witdh = 2

TOROIDAL MESH
number of links = 3p (each processor is linked to 4 other processors, each switch has 5 links)
`bisection width = 2*sqrt(p)`

FULLY CONNECTED NETWORK
Each switch is connected to every other switch.
number of links = p^2/2 + p/2 (each switch has p links)
Bisection width = p^2/4

Impractical to construct except for small number of nodes.

HYPERCUBE
A one-dimensional hypercube is a fully-connected system with 2 processors.
A two-dimensional hypercube is built from 2 one-dim hypercubes by joining "corresponding" switches.
A three-dimensional hypercube is built from 2 two-dim hypercubes.
...
A hypercube of dimension d has p = 2^d nodes.
Each switch is connected to one processor and d switches.
Bisection width = p/2

###### Indirect interconnect
The switches may not be directly connected to a processor.
Each processor has an outgoing and an incoming link, which are connected to a switching network.

CROSSBAR
All processors can simultaneously communication at the condition that two processors don't attempt to communicate with the same processor.
Number of switches = p^2
Bisection bandwidth = p^2

OMEGA NETWORK
2-by-2 crossbars switches connected together.
Unlike the crossbar, there are communications that cannot occur simultaneously.
Number of switches = p/2 * log2(p)
Bisection bandwidth = p/2
Cheaper than the crossbar since it uses less switches.

### SMP

SMP (Symmetric MultiProcessing) involves a multiprocessor computer hardware architecture where two or more identical processors are connected to a single shared main memory and are controlled by a single OS instance. Most common multiprocessor systems today use an SMP architecture. In the case of multi-core processors, the SMP architecture applies to the cores, treating them as separate processors.

### SPMD

SPMD (Single Process, Multiple Data or Single Program, Multiple Data)

Is a subcategory of MIMD.

One program is written to be run on multiple threads or processors. By condition branches it can then behave differently.
For instance it can implement data-parallelism by deciding that core 0 takes care of first half of data and core 1 takes of the second half.
It can also implement task-parallelism by the same way.

How to parallelize :
1) Divide work among the processes/threads
a. in such a way that each processor/thread gets roughly the same amount of work (i.e.: load balancing)
b. in such a way that the amount of communication required is minimized.
2) Arrange for the processes/threads to synchronize.
3) Arrange for communication among the processes/threads.

2) and 3) are interrelated. In distributed-memory programs, we often implicitly synchronize the processes by communicating among them, and in shared-memory programs, we often communicate among the threads by synchronizing them.

Shared Memory Model. Example of implementation: OpenMP, pThreads, and all threading systems.

Distributed memory (examples of implementation: PVM, MPI) is the programming style used on parallel supercomputers from homegrown Beowulf clusters to the largest clusters on the Teragrid.

## Interprocess communication

It's important to keep a balance between system utilization (number of processes) and communication.
For an application, if, when you increase the number of tasks/processes, communication increases too much, it will slow down processing.

## Implicit parallel computing

 * PGAS: CAF, UPC (Unified Parallel C).
 * GA (Global Array).

Process interaction is not visible to the programmer.

## Useful links

Liste calcul (GDR Calcul CNRS):

 * [Official site](http://calcul.math.cnrs.fr/).
 * [Email address](calcul@listes.math.cnrs.fr).
 * [Gestion](https://listes.mathrice.fr/math.cnrs.fr/info/calcul).

## Method for designing a parallel program

Ian Foster in is online book [Designing and Building Parallel Programs](http://www.mcs.anl.gov/~itf/dbpp/):

	1. Partitioning. The computation that is to be performed and the data operated on by this computation are decomposed into small tasks. Practical issues such as the number of processors in the target computer are ignored, and attention is focused on recognizing opportunities for parallel execution.

	2. Communication. The communication required to coordinate task execution is determined, and appropriate communication structures and algorithms are defined.

	3. Agglomeration. The task and communication structures defined in the first two stages of a design are evaluated with respect to performance requirements and implementation costs. If necessary, tasks are combined into larger tasks to improve performance or to reduce development costs.

	4. Mapping. Each task is assigned to a processor in a manner that attempts to satisfy the competing goals of maximizing processor utilization and minimizing communication costs. Mapping can be specified statically or determined at runtime by load-balancing algorithms.

## Clusters

### COW (Cluster of Workstations)
A computer cluster is a group of linked computers, working together closely thus in many respects forming a single computer. The components of a cluster are commonly, but not always, connected to each other through fast local area networks.

### Beowulf cluster
Difference with COW (Cluster of Workstations): one of the main differences between Beowulf and a Cluster of Workstations (COW) is that Beowulf behaves more like a single machine rather than many workstations. In most cases client nodes do not have keyboards or monitors, and are accessed only via remote login or possibly serial terminal. Beowulf nodes can be thought of as a CPU + memory package which can be plugged in to the cluster, just like a CPU or memory module can be plugged into a motherboard.

## Work assignment

It's important to dispatch work among threads/computers in a fair maner.

For instance, if there's a function `f(i)` which takes more time to complete when i's value is big than when it's small, and we dispatch the work as blocks (block dispatching):

values of i     thread/computer
-------------   ----------------
0-5000          0
5001-10000      1

then, thread/computer 1 will finish a long time after thread/computer.

Other ways of dispatching work exist and are described below.

Cyclic dispatching:

values of i             thread/computer
---------------------   ---------------
0,2,4,...,9998,10000    0
1,3,5,...,9997,9999     1

Dynamic dispatching:

As soon a thread/computer has finished processing, it requests more data to process.
This can be combined with block dispatching, giving each time a block of values of i to process.

time    values of i     thread/computer
-----   --------------- ---------------
0       0,1,2,3,4       0
1       5,6,7,8,9       1
2       10,11,12,13,14  0
3       15,16,17,18,19  1
...
		
## Performance

Linear speedup = when a parallelisation of a serial program (which takes time
Tserial to run), using p cores, takes time Tparallel=Tserial/p. This never
happens in practice.

Speedup = S = Tserial / Tparallel

Efficiency = S / p = Tserial / ( p * Tparallel)

As the problem size increases, the speedups and the efficiencies increase.

In real life, there's an overhead (due to critical sections and message
passing, and serial code that is not parallelizable) introduced when
transforming a serial program into a parallel program, so:
Tparallel = Tserial / p + Toverhead.

### Amdahl's law
Suppose we're able to parallelize 90% of the serial code.
Suppose that the speedup is linear (i.e. is perfect).

Tparallel = 0.9 * Tserial / p + 0.1

S = Tserial / (0.9 * Tserial / p + 0.1 * Tserial)

lim S = Tserial / (0.1 * Tserial) = 10
p->∞

So regardless of the number of processors, we'll never get a better speed-up than 10.

ISSUE: Amdahl's law doesn't take into account the problem size.

As we increase the problem size, the "inherently serial" fraction of the program (i.e. the serial part of the program that can't be parallelized) decreases in size.
See GUSTASFSON'S LAW.

### Scalability
A parallel program is scalable if for a increase of the number of processors/threads by a factor k, we can find a factor of increase x of the problem size so that the efficiency is unchanged.

If x = k, then the program is said to be weakly scalable.
If x = 1 (problem size unchanged), then the program is said to be strongly scalable.

### Taking timings
UNIX shell command time reports whole program time, so also input and output of data.

CPU time (time reported by C function clock) includes our code but also library functions (pow, sin, ...) and system calls (printf, scanf, ...). It doesn't include time during which the program is idle.

So we must use wall clock time to measure timings. Example of code:
```c
shared double global_elapsed;
private double my_start, my_finish, my_elapsed;

/* Synchronizing all processes/threads */
Barrier();
my_start = Get_current_time();

/* Code that we want to time */

my_finish = Get_current_time();
my_elapsed = my_finish - my_start;

/* Find the max across all processes/threads */
global_elapsed = Global_max(my_elapsed);
if (my_rank == 0)
	printf("The elapsed time = %e seconds\n", global_elapsed);
```
We run several time the program, and retain the minimum time found.

### Performance measuring tools

 * Scalasca
 * Vampir
 * Tau

## Solutions

### MPC

Library that encapsulates MPI, OpenMP and pThreads.
See [official site](mpc.sourceforge.net).

### UPC

Berkeley UPC - Unified Parallel C
See [official site](http://upc.lbl.gov/).

Implements SPMD model.

Can execute the program through multiple threads on one machine.
Can be executed on parallel on several machines, using shared data.

### PVM

PVM (Parallel Virtual Machine)
Implements SPMD model.
See [official site](http://www.csm.ornl.gov/pvm/).

### QUATTOR

QUATTOR is an open-source software for maintaining and administrating a grid/cluster of computers.
See [official site](http://quattor.org).
## Python

Global Interpreter Lock (GIL)

 * [Python and multi-threading. Is it a good idea?](https://www.tutorialspoint.com/python-and-multi-threading-is-it-a-good-idea).

HPC libraries:
 * Dask
 * joblib
 * multiprocessing
 * queue -> synchronized queue class
 * thread module
 * threading -> built on top of thread module
 * [MPI for Python](https://mpi4py.readthedocs.io/en/stable/).

## WMS (Workflow Management Systems)

### nextflow

 * [nextflow](https://www.nextflow.io/).

Workflow management system written in Java.

### Snakemake

#### SLURM and Snakemake

 * [Parallélisation slurm avec snakemake](https://community.france-bioinformatique.fr/t/parallelisation-slurm-avec-snakemake/280).

Using Snakemake with SLURM:
```sh
snakemake --drmaa --jobs=400 <my-output-data>
```
[DRMAA](https://en.wikipedia.org/wiki/DRMAA).

### Pegasus

A workflow management system.

 * [Pegasus](https://pegasus.isi.edu/).
 * [Welcome to Pegasus WMS’s documentation!](https://pegasus.isi.edu/documentation/).

 * [Executing Workflows using Pegasus WMS](https://arokem.github.io/2013-09-16-ISI/lessons/pegasus-workflows/tutorial.html).

Install on MacOS:
```sh
brew tap pegasus-isi/tools
brew install pegasus htcondor
brew tap homebrew/services
brew services list
brew services start htcondor
```

Install on Ubuntu:
```sh
curl https://download.pegasus.isi.edu/pegasus/gpg.txt | sudo apt-key add -
echo 'deb https://download.pegasus.isi.edu/pegasus/ubuntu jammy main' | sudo tee /etc/apt/sources.list.d/pegasus.list
sudo apt-get update
sudo apt-get install pegasus
```

#### pegasus-mpi-cluster

`pegasus-mpi-cluster` must be installed from [Pegasus sources](http://download.pegasus.isi.edu/pegasus/5.0.5/pegasus-5.0.5.tar.gz) by compiling `packages/pegasus-mpi-cluster`.

Attention in the `Makefile`, `-lnuma` must be set as the last option when calling `mpicxx`/`ld`. For the linking of the program `pegasus-mpi-cluster` and all test programs.

#### pegasus-plan

 * [pegasus-plan](https://pegasus.isi.edu/documentation/manpages/pegasus-plan.html).

Needs:
 * A Replica Catalog: define the locations of the input files on the execution site (maps Logical File Names (LFNs) to Physical File Names (PFNs).
 * A Transformation Catalog (`transformations.yml` by default): map the Logical Transformations to Physical Executables.
