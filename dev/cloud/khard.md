# khard

 * [khard](https://github.com/scheibler/khard/).

CLI address book program.

Search for a contact (case insensitive):
```sh
khard durand
```

List all contacts:
```sh
khard list
```

Edit contact (open default editor):
```sh
khard edit durand
```
