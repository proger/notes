# wego

 * [wego](https://github.com/schachmat/wego).
 * [wttr.in](https://github.com/chubin/wttr.in).

`wego` is a weather client for the terminal.

The website wttr.in uses wego to display a weather report in text mode:
```sh
curl wttr.in
```
