# lynx

 * [Lynx users guide](http://lynx.invisible-island.net/lynx_help/Lynx_users_guide.html).

Open page
```bash
lynx www.mypage.com
```
Or press "G" key inside lynx

Cmd | Desc.
--- | --------
a   | Add current document or current highlighted link to bookmarks.
r   | Remove a bookmark (when in the bookmarks list).
v   | View bookmarks.

Dump web page in text format:
```lynx
lynx -dump myfile.html
```
`-width=600` sets the number of columns to use (default 80, max 1024).
`-list_inline` writes the links inside the text at their
place instead of writing them at the end of the output.

Display content from stdin:
```sh
mycommand | lynx -stdin
```

## Debugging

Turning on log output
```bash
lynx -trace ...
```
Log is written inside Lynx.trace file.

## Config file

First lynx reads the common config file (On MacOS-X brew install: /usr/local/Cellar/lynx/2.8.8/etc/lynx.cfg)

Force cfg file
```bash
lynx -cfg my_file
```

## Print configuration
```bash
lynx -show_cfg
```

Setting option value in config file:
```
character_set=utf-8
```
For a comment use #.

## Key commands

Key              | Description
---              | ---
`=`              | Display current link and other info.
`G`              | Open link.
`UP`             | Previous link in the page.
`DOWN`           | Next link in the page.
`LEFT`           | Back to previous page.
`RIGHT`          | Follow link.
`PGUP`           | Move up in a page.
`SPACE`/`PGDOWN` | Move down in a page.
`CTRL-R`         | Reload current page.
