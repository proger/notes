# mpack

`munpack` extract attachments from `.eml` file:
```sh
munpack mymessage.eml
```

Installing on Archlinux:
```sh
yay -S aur/mpack
```
