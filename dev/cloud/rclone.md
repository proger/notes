# rclone

 * [Rclone syncs your files to cloud storage](https://rclone.org/).
 * [How to Use Microsoft OneDrive in Linux With Rclone Open-Source Tool](https://itsfoss.com/use-onedrive-linux-rclone/).

## Install

On macos, HomeBrew's `rclone` cannot use the `mount` command.
Same with a `port` install.
`rclone` needs to be installed from original package at <https://rclone.org/downloads/>, and macfuse needs to be installed with `brew install macfuse`. Then a kernel extension from rclone's developer needs to be allowed in System Preferences, and the machine needs to be rebooted.

## Configure

Configuration of a remote:
```sh
rclone config
```

Mount a remote:
```sh
rclone --vfs-cache-mode writes -v mount onedrive: ~/onedrive
# Or in full cache mode:
rclone --vfs-cache-mode full -v mount onedrive: ~/onedrive
```
In daemon mode:
```sh
rclone --vfs-cache-mode full -v mount --daemon onedrive:  ~/onedrive
```

Unmount:
```sh
umount ~/MyFolder
```
If resource is busy but you know no shell or app is using it, force it with:
```sh
fusermount -uz ~/MyFolder
```

Sync with local dir (one way sync, erase locally the files deleted on remote):
```sh
rclone sync onedrive:Documents/famille /mnt/usb/bumeu/backup/onedrive_famille -P
```

Get content of remote file and send it to stdout:
```sh
rclone cat "onedrive:movies/short-movies/Adam (1991).mkv" | mplayer -
```

In Ubuntu/Gnome, set the following line inside the Startup Applications
Preferences:
```sh
sh -c "rclone --vfs-cache-mode full mount onedrive: ~/onedrive"
```
