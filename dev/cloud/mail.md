# mail

Read mail from system mailbox:
```bash
mail
```

Send a mail to the system:
```bash
mail -s "my_subject" user.name
```
and then type your message body and finish by `ctrl+d`, or
```bash
echo "my text" | mail -s "my_subject" user.name
```
