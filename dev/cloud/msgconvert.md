# msgconvert

Convert Outlook `.msg` message file into `.eml`.

To install on Archlinux:
```sh
yay -S aur/perl-email-outlook-message
```

To convert a message:
```sh
msgconvert mymessage.eml
```
