# w3m

Open a link:
```bash
w3m www.lemonde.fr
```

Cookie:
```bash
w3m -cookie ...
```
Don't work perfectly with cookies. Doesn't recognize/accept cookies of www.mediapart.fr for instance.

Commands:

Key      | Description
-------- | ----------------------------------------------------
`l`      | Cursor right.
`h`      | Cursor left. Cursor pad also works.
`j`      | Cursor down.
`k`      | Cursor up.
`SPC`    | Go forward one screen of page
`b`      | Go back one screen of page
`:`      | Toggle auto detection of URLs (http://...):
`B`      | Go backward (previous opened page)
`RET`    | Follow link.
`TAB`    | Next link.
`ESC TAB`| Previous link.
