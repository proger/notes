# msmtp

Mail sender program, able to handle multiple SMTP accounts.

Install:
```sh
pacman -S msmtp gnu-netcat # Archlinux
apt install msmtp # Ubuntu
apt install msmtp msmtp-mta mailutils # Ubuntu ---> ????
```

Using from command line:
```bash
echo "some text" | msmtp [-a account] email@address
```
Complete example:
```sh
msmtp -a apple sympa@listes.math.cnrs.fr <<EOF
From: pierrick.rogermele@icloud.com
To: sympa@listes.math.cnrs.fr
Subject: UNSUBSCRIBE calcul
unsubscribe
EOF
```

In Mutt:
```muttrc
set sendmail="/usr/local/bin/msmtp"	# for normal msmtp
```

Using queuing in Mutt:
```bash
mkdir $HOME/.msmtp.queue
```
And in Mutt:
```muttrc
set sendmail="/usr/local/bin/msmtpq"	# for queueing version
```
Queued messages are sent later on when sending another email, when connected.
It's also possible to force sending with the following command:
```bash
msmtp-queue -r
```
Which means it could be used inside a cron task.

## macOS install

msmtp exists in brew, but it doesn't install queueing scripts. However they are present in '/usr/local/Cellar/msmtp/<version>/share/msmtp/scripts/msmtpq/'.
