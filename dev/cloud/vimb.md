# vimb

 * [Manpage](https://fanglingsu.github.io/vimb/man.html).

Vim like browser.

| ---- | -------------------------------- |
| Keys | Action                           |
| ---- | -------------------------------- |
| bma  | Add bookmark                     |
| bmr  | Remove bookmark                  |
| o    | Open URL.                        |
| y    | Yank current URI to clipboard    |
| "xy  | Yank current URI into register x |
| ---- | -------------------------------- |

Commands:
 * `open`: Open URL.
