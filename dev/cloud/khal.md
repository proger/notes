# khal

 * [khal](http://lostpackets.de/khal/).
 * [Sync private iCloud calendar with MagicMirror](https://forum.magicmirror.builders/topic/5327/sync-private-icloud-calendar-with-magicmirror).

CLI calendar program.
There is a ncurses version:
```sh
ikhal
```

List upcoming events (today and tomorrow):
```sh
khal list
```
