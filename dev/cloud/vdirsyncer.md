# vdirsyncer

Synchronizes your calendars and addressbooks between two storages.

 * [Vdirsyncer](https://github.com/pimutils/vdirsyncer).

Enable service:
```sh
systemctl --user enable --now vdirsyncer.timer
```

Show service logs:
```sh
journalctl --user -u vdirsyncer
```

Edit service configuration (and set time interval):
```sh
systemctl --user edit vdirsyncer
```
Then write inside:
```systemd
OnBootSec=5m  # This is how long after boot the first run takes place.
OnUnitActiveSec=15m  # This is how often subsequent runs take place.
```
