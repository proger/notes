# vercmp

On ArchLinux, the `vercmp` script is available for comparing versions:
```bash
if [ $(vercmp $myver $requiredver) -ge 0 ] ; then
	# Wrong version
	# ...
fi
```
