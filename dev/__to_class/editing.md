# Editing & text encoding

## Unicode

 * [Inline markup and bidirectional text in HTML](http://www.w3.org/International/articles/inline-bidi-markup/).

## Fonts

### BDF

xmbdfed to edit a BDF font file
GENTOO: x11-misc/xmbdfed

 * [BDF](http://www.fileformat.info/format/bdf/egff.htm).


## MSWORD

F5 | Go to page, section, etc.

Put in small caps on macos: SHIFT+COMMAND+K

### Search & replace

Special characters:

Special characters | Meaning               
------------------ | ----------------------
`^w`               | Space.
`^l`               | Manual line break.
`^p`               | Paragraph break.
`^n`               | Column break.
`^m`               | Manual page break.
`^b`               | Section break.
`^t`               | Tab character.
`^?`               | Any charactger.
`^&`               | Text found by the `Find what` expression.

Wildcard characters (select `Use wildcards` or `Utiliser les caractères génériques`):

Special characters | Meaning                | Example
------------------ | ---------------------- | --------
`@`                | One or more occurences | `lo@t` finds "lot" and "loot."

See [Use wildcard characters to find or replace text](https://support.office.com/en-us/article/Use-wildcard-characters-to-find-or-replace-text-610e37dc-bb2f-4a8b-8fa5-aa991160eafb).

To put text in uppercase:
	Find what: ^?
	Replace with: ^&        and choose Format->Font->All caps

To find text in specific style:
	Find what: 

### Commands

For a complete list of all Word commands, run the ListCommands (ListerCommandes in French) command:

 * Tools -> Macro -> Macros...
 * Type "ListCommands" in the "Macro name" field, then click "Run". Then select "All Word commands". It will create a new document containing a table listing all Word commands.

### Trier

Pour trier un texte, sélectionner le texte et aller sous "Tableau->Trier..."

### Masquer

Masquer du texte:

 * Sélectionner le texte -> clique droit -> Police -> Masqué (case à cocher)

Afficher le texte masqué:

 * Outils -> Options -> Affichage -> Marques de format -> Texte masqué (case à cocher)

### Notes

Convertir toutes les notes de bas de page en notes de fin :

 1. Affichage -> Normal.
 2. Affichage -> Notes de bas de page, puis sélectionner toutes les notes et cliquer droit "Convertir en note de fin".

Mettre les notes de fin après la section :

 * Par défaut les notes de fin sont à la fin du document.
 * Pour les mettre après la section courante, cliquer droit sur une note de fin, puis choisir "Notes de Fin" -> "Fin de Section".

Configure footnotes: `Insert->Notes`, then `Apply`.

Rénuméroter les notes de bas de page à chaque page : Insertion -> Notes -> Renuméroter à chaque page.

### Sous-documents

Pour créer des sous-documents à partir d'un document, aller en mode Plan.
Sélectionner le texte avec comme première ligne un titre (Titre 1, Titre 2, ou autre), puis cliquer sur "Créer un sous-document".

Si la command "Créer un sous-document" n'est pas disponible. Aller dans "View -> Toolbars -> Customize Toolbars and Menus...", puis dans l'onglet "Commands". Rechercher la commande CreateSubdocument dans la liste "All Commands" et l'ajouter dans le menu en la faisant glisser.

La liste complète des commandes concernant les sous-documents est :

ENGLISH             | FRENCH                            | INDEX
------------------- | --------------------------------- | -----
CreateSubdocument   | CréerSousDocument                 |
InsertSubdocument   | InsérerSousDocument               |
MergeSubdocument    | FusionnerSousDocuments            |
OpenSubdocument     | OuvrirSousDocument                |
RemoveSubdocument   | SupprimerSousDocument             |
SplitSubdocument    | DiviserSousDocument               |
ToggleMasterSubdocs | AffichageDocumentPrincipalSousDoc | 0374

### Accents

Look into "Insert symbols" to know which shortcut to use to get a special character (like ç, é, è, ï, ô, ...).

Keys                 | Character
-------------------- | ---------
`CRTL + , c`         | ç
`CTRL + `` e`        | è
`CTRL + ' e`         | é
`CTRL + SHIFT + ˆ u` | û
`CTRL + SHIFT + : i` | ï

## EtherCalc

 * [EtherCalc](https://ethercalc.net/).

Javascript spreadsheet software for simultaneous collaborating.

## EXCEL

 * [Excel keyboard shortcuts](https://support.office.com/en-us/article/Excel-keyboard-shortcuts-ef213435-a57e-4ce1-bac6-2b39f16cb121).

If:
```vb
IF(value;if_true;if_false)
```

Test if a cell is empty:
```vb
ISBLANK(A2)
```

Concatenate strings:
```vb
s1 & s2
```

`INDIRECT` returns the value of the cell referenced by the containt of the cell specified in parameter as text:
```vb
INDIRECT("B6")
```

`OFFSET`:
```vb
OFFSET(B3, 0, -1, 1, 1)
```


## Google Sheet

### References

A reference like this:
```vb
A2
```
will be increased automatically when clicking and sliding.
To block this behaviour for the row, the column or both, use the `$` operator:
```vb
$A$2
```

Define a range:
```vb
A10:C30
```

### Search for value in a table

How to match a line in a table and get value of one of the columns:
```vb
VLOOKUP("text_to_match", A1:E100, 2, FALSE)
```
Param 1: the value to search for.
Param 2: the subtabtle on which to run the function. The first column is used as the keys where to find the text to match.
Param 3: the column where to get the value to return.
Param 4: set to `TRUE` if the values in which to search are sorted.
In the French version:
```vb
RECHERCHEV("text_to_match"; A1:E100; 2 ; FAUX)
```

### Regular expression

Extract a substring:
```vb
REGEXEXTRACT("BLABLA_100";"^([^_]+)_.*$")
```


```
===================== Open Office.txt
Rotating text
=============
Select the text to be rotated and then click Format > Character.

Hiding a Sheet
==============
Format > Sheet > Show

Change links to files into embedded objects (images, text, ...)
===============================================================
Choose Edit > Links from the menu bar.
The Edit Links dialog shows all the linked files. In the Source file list, select the files you want to change from linked to embedded.
Click the Break Link button.
===================== gucharmap.txt
install popt package (getopt)
compile without gnome:
./configure --prefix=/Users/dev --disable-gnome
===================== pdf.txt
# -*- mode:shell-script -*-

# merging two pdf files
gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=firstANDsecond.pdf -dBATCH first.pdf second.pdf
===================== pdftops.txt
# -*- Shell-script -*-

# Convert a pdf to an eps
pdftops -eps myfile.pdf
===================== software.txt
EPS editors
===========
Inkscape    http://inkscape.org
IPE         http://ipe7.sourceforge.net
===================== MS Office/updates.txt
Aller sur le site de microsoft et rechercher la section "Office Update" pour installer les mises à jour nécessaires automatiquement.
===================== postscript/errors.txt
EPS ERROR
=========
dvips:  expected to see %%EndBinary at end of data; struggling on

Download http://tomas.rokicki.com/illbug/fixill.pl
and run ./fixill <input.eps >output.eps
===================== printing/CANON LBP2900.txt
64 bits machine
==============
Canon drivers are compiled as 32 bits, and so do not work with 64 bits cups.
==> try to compile cups 	as 32 bits :
CHOST="i686-pc-linux-gnu" in /etc/make.conf

pstocapt write error,32.
=====================
/usr/lib/cups/filter/pstocapt

config
======
/etc/ccpd.conf

HOWTO
=====
About
This howto describes how to use local printer Canon LBP 2900 using proprietary Canon drivers with CUPS. Main problem with this printer is postscript support absence. Canon proprietary driver includes daemon to convert PS to LBP2900 internal format (Canon Printer Daemon).

Install
First of all download last drivers here ftp://download.canon.jp/pub/driver/lasershot/linux/ You should download two files:

cndrvcups-capt
cndrvcups-common
Warning: Tarballs ( .tar.gz files ) is the source code. I got a lot of compilation errors on Gentoo.
Warning: I did succesful test only on drivers version 1.30-1. Did not test others.
Binary drivers are available only as RPM. I installed it using rpm command with --nodeps flag.

Warning: This driver is installed in /usr/lib/cups directory. But Gentoo CUPS system is located in /usr/libexec/cups. You should move driver files from installation default directory into correct location.
Setup
Turn on your printer. Your printer should be available in list of USB devices. You will see something like that if all is ok

lsusb
Bus 001 Device 002: ID 04a9:2676 Canon, Inc.
Restart cupsd:

/etc/init.d/cupsd restart
Change fifo access mode (if it does not exist you can create it using mkfifo command)

chmod 777 /var/ccpd/fifo0
Create directory and link it to .ppd file

cd /usr/share/ppd/
ln -s /usr/share/cups/model/CNCUPSLBP2900CAPTK.ppd
System printer using lpadmin in CUPS

/usr/sbin/lpadmin -p LBP2900 -m CNCUPSLBP2900CAPTK.ppd -v ccp:/var/ccpd/fifo0 -E
Check if your printer have device node:

ls -l /dev/usb/lp0
and register it using ccpdadmin in Canon Printer Deamon

/usr/sbin/ccpdadmin -p LBP2900 -o /dev/usb/lp0
Startup script
Create /etc/init.d/ccpd file:

#!/sbin/runscript
#
# ccpd          startup script for Canon Printer Daemon for CUPS
#
#               Modified for Debian GNU/Linux
#               by Raphael Doursenaud <rdoursenaud@free.fr>.
#               Modified for Gentoo GNU/Linux
#               by NStorm
DAEMON=/usr/sbin/ccpd
LOCKFILE=/var/lock/subsys/ccpd
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
NAME=ccpd
DESC="Canon Printer Daemon for CUPS"
test -f $DAEMON || exit 0

depend() {
  need net
  after cupsd
}

start() {
  ebegin "Starting $DESC: $NAME"
  start-stop-daemon --start --quiet --exec $DAEMON
  eend $?
}

stop() {
  ebegin "Stopping $DESC: $NAME"
  start-stop-daemon --stop --quiet --oknodo --exec $DAEMON
  eend $?
}

status() {
 echo "$DESC: $NAME:" `pidof $NAME`
}
Restart
Run ccpd, add deamons to startup

/etc/init.d/ccpd start
rc-update add ccpd default
rc-update add cupsd default
Your printer should be available in CUPS now.===================== printing/CUPS.txt
ADMIN PAGE
==========
http://localhost:631
 
Allow remote access
===================
in /etc/cups/cupsd.conf:
<Location />
  Order allow,deny
  Allow localhost
  Allow 192.168.1.*
  Deny all
</Location>

Listen *:631
#Listen localhost:631

===================== printing/MacOS-X.txt
ADD A PRINTER WITH CUSTOM PRINT QUEUE NAME
==========================================
Add the printer with its IP address.
Leave Queue name field blank, and inside the printer name field type the print queue name you would like.
The printer is created with the queue name set with the printer name.
Then it's easy to change the printer name.

========================================
"Image Capture Extension is trying to modify the printer settings. Type the name and password of a user in the 'Print Administrators' group to allow this."

Add user to lpadmin group:
dseditgroup -o edit -u admin_name -p -a user_name -t user _lpadmin
===================== printing/cupsfilter.txt
# vi: ft=sh

# Convert a text file to pdf
cupsfilter myfile.txt > myfile.pdf
===================== printing/inkolab.txt
www.inkolab.com
===================== printing/lp.txt
# vi: ft=sh

# lp is available in UNIX/Linux, MacOS-X.

# print a text file
lp README

# print 3 copies
lp -n 3 README
===================== printing/lpstat.txt
# vi: ft=sh

# Getting list of printers
lpstat -p -d
===================== printing/printing.txt


===== DEPRECATED =====

Produire un fichier ppd (description d'une imprimante) :
	foomatic-ppdfile -p 'Epson-Stylus_Color_680'

Ajouter une imprimante à CUPS :
	/usr/sbin/lpadmin -p epsonSC680 -E -v usb:/dev/usb/lp0 -m epsonStylusColor680.ppd

Pour imprimer :
	lp <file>

Pour voir la queue d'impression :
	lpq
	
Pour supprimer un travail d'impression :
	tout supprimer : 			lprm -
	supprimer le travail 7 :	lprm 7
	
===================== text_and_unicode/UCD.txt
UCD (Unicode Character Database)

http://www.unicode.org/Public/UNIDATA/

Command download all interesting files :
wget -r -l 2 -nH --cut-dirs=2 -A "*.txt,*.html" -R "robots.txt,index.html*" http://www.unicode.org/Public/UNIDATA

######
# VI #
######
# In vi, you can remove carriage return ( ^M ) characters with the following command:
# :1,$s/^M//g
# Note: To input the ^M character, press Ctrl-v , and then press Enter or return.

#  In vim, use :set ff=unix to convert to Unix; use :set ff=dos to convert to Windows.
===================== text_and_unicode/encodings.txt
latin-1 (iso-8859-1)
====================
Windows-1252 (CP1252) is based on latin-1 and is often confused with it by editors in UNIX world. In latin-1 the range 80-9f is not used, but in Windows-1252 it is, which can lead to unrecognized characters.

MAC (MACINTOSH MACROMAN CSMACINTOSH) encoding uses 8E for é.

===================== text_and_unicode/scramble.txt
scramble, shuffle, unsort a file
================================
perl -wne 'printf "%016.0f%s", rand 2**53, $_'  | sort | cut -c17-
===================== text_and_unicode/uni2ascii.txt
# -*- mode:shell-script -*-

# MacOS-X Ports package: uni2ascii

# transform to ascii
uni2ascii -B <file>

# many encoding of unicode chars into their code number are availabe with option -a
===================== MS Office/Excel/csv_files.txt
Excel opens automatically CSV files, using coma or semicolon separator depending on OS configuration.
Under Windows, check the list separator in Language & Region configuration.
Under MacOS-X, check the decimal separator in Language & Region settings.
===================== MS Office/Excel/macros.txt
For displaying developer bar
============================
Go to "Office" button at top-left of window.
A window appears.
Click on button "Excel options..." at bottom.
Then select "Enable Developer bar in ribbon".
===================== MS Office/Excel/password.txt
To remove password on VBA macros
================================
Run an hex editor (like hexedit in UNIX/Linux) and find DPB= string.
Change it for DPx=
On opening Excel won't recognize it, will display an error and propose to repair it.
```
