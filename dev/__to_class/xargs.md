# xargs

Flag        | Description
----------- | -------------------------------
-I replstr  | Use a replace string, instead of appending arguments to the end of the command.

Execute the command with one argument at a time:
```bash
my_command | xargs -n 1 chmod a-x
```

To use \0 for strings separator instead of spaces or newlines:
```bash
... | xargs -0 ...
```

To place the arguments at a specified place instead of appending them, use the `-I` flag. The replace_string must be a distinct argument to xargs, it can't be inside a string for instance or it will not be replaced.
```bash
/bin/ls -1d [A-Z]* | xargs -I % cp -rp % destdir
```
