# test / [

File test operators:

Flag | Description
---- | -------------------
`-e` | File exists.
`-a` | File exists (deprecated).
`-f` | File is a regular file (not a directory or device file).
`-s` | File is not zero size.
`-d` | File is a directory.
`-b` | File is a block device.
`-h` | File is a symbolic link.
`-L` | File is a symbolic link.
`-r` | File has read permission (for the user running the test).
`-w` | File has write permission (for the user running the test).
`-x` | File has execute permission (for the user running the test).

```bash
if test -z "$var" ; then
	echo empty var
fi
```

You can use '[' instead of 'test'.
Be careful to put spaces before and after [ and ], because [ is a real command and ] is a real argument
```bash
if [ -z "$var" ] ; then
	echo empty var
fi
```

Compare integers:
```bash
if [ $v -eq 1 ] ; then
	echo yesh
fi
```

Grouping expressions:
```bash
if [ "$a" = 'x' -a '(' "$b" = 'y' -o "$c" = 'z' ')' ] ; then
	echo yesh
fi
```
