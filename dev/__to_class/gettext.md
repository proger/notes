# gettext

 * [GNU gettext utilities](https://www.gnu.org/software/gettext/manual/gettext.html).

Internationalization of applications.

Create a template file by extracting strings to translate from the code:
```bash
xgettext --from-code=UTF-8 -o messages.pot *.php
```
