# Fonts

Install CJK font on Archlinux:
```sh
pacman -S wqy-microhei
```

```sh
pacman -S xorg-fonts-misc
```

Install Microsoft fonts:
```sh
yay -S ttf-ms-win10-auto
# OR
yay -S ttf-ms-win11-auto
```
