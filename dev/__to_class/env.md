# env

Running command from shebang:
```r
#!/usr/bin/env -S Rscript --vanilla
```
If `-S` is not set:
```
/usr/bin/env: ‘Rscript -S "--vanilla"’: No such file or directory
/usr/bin/env: use -[v]S to pass options in shebang lines
```
