# su

Login as root user and stay in current directory:
```bash
su
```

Login as root user inside root's home directory:
```bash
su -
```
