# md5sum

Compute MD5 sum from file:
```sh
md5sum myfile.txt
```

Check MD5 of files:
```sh
md5sum -c mysums.md5
```
