# crontab

To edit user's own crontab:
```bash
crontab -e
```

Environment variables already set by cron:

Variable    | Value
----------- | --------------------
`PATH`      | `/usr/bin:/bin`.
`HOME`      | From `/etc/passwd`.
`LOGNAME`   | From `/etc/passwd`.
`SHELL`     | `/bin/sh`.

To receive mails with output of run commands, you must set the `MAILTO` variable:
```crontab
MAILTO="username" # local mail
```
or
```crontab
MAILTO="someone@mail.server"
```

The format of a crontab line is `m h d M D command`, where 5 first fields define the time when to run the specified command.

Description of the five time fields, in order:

field        | allowed values
------------ | --------------
minute       | 0-59
hour         | 0-23
day of month | 1-31
month        | 1-12 (or names, see below)
day of week  | 0-7 (0 or 7 is Sun, or use names)

Ranges can be specified
```crontab
0 6-11 * * * command
```
Command will be run each day from 6 to 11 o'clock.

Several values or ranges can be separated by commas
```crontab
0 6,10,18 * * * command
```
Command will be run each day at 6:00, 10:00 and 18:00

Timesteps can be specified using a slash: `*/5`.

Run command each day at 9:00 and 21:00:
```crontab
0 9-23/12 * * * command
```

Run a command every 2 hours:
```crontab
0 */2 * * * command
```
