PARSING
=======

## BNF

 * [BNF and EBNF](http://www.garshol.priv.no/download/text/bnf.html).
 * [Backus–Naur form](https://en.wikipedia.org/wiki/Backus–Naur_form).
 * [Extended Backus–Naur form](https://en.wikipedia.org/wiki/Extended_Backus–Naur_form).

## ANTLR

 * [ANTLR](https://supportweb.cs.bham.ac.uk/documentation/tutorials/docsystem/build/tutorials/antlr/antlr.pdf).
 * [ANTLR 3](http://jnb.ociweb.com/jnb/jnbJun2008.html).

## TO CLEAN

	===================== Gold.txt
	http://goldparser.org
	===================== JavaCC.txt
	From Wikipedia: JavaCC (Java Compiler Compiler) is an open source parser generator for the Java programming language. JavaCC is similar to yacc in that it generates a parser from a formal grammar written in EBNF notation, except the output is Java source code. Unlike yacc, however, JavaCC generates top-down parsers, which limits it to the LL(k) class of grammars (in particular, left recursion cannot be used). The tree builder that accompanies it, JJTree, constructs its trees from the bottom up.
	===================== LALR.txt
	From Wikipedia: An LALR parser is a type of parser defined in computer science as a Look-Ahead LR parser.
	LALR parsers are based on a finite-state-automata concept, from which they derive their speed[citation needed]. The data structure used by an LALR parser is a pushdown automaton (PDA). A deterministic PDA is a deterministic-finite automaton (DFA) with the addition of a stack for a memory, indicating which states the parser has passed through to arrive at the current state. Because of the stack, a PDA can recognize grammars that would be impossible with a DFA; for example, a PDA can determine whether an expression has any unmatched parentheses, whereas an automaton with no stack would require an infinite number of states due to unlimited nesting of parentheses.
	===================== LL.txt
	LL recognizers begin the recognition process at the most abstract language level, they are called top-down recognizers. 
	
	From Wikipedia: An LL parser is a top-down parser for a subset of the context-free grammars. It parses the input from Left to right, and constructs a Leftmost derivation of the sentence (hence LL, compared with LR parser).
	
	From ANTLR's author: LL recognizers are goal-oriented. They start with a rule in mind and then try to match the alternatives. For this rea- son, LL is easier for humans to understand because it mirrors our own innate language recognition mechanism.
	
	Left-recursive grammars can't be implemented using an LL recognizer:
	expr : expr '++' ;
	because the generated code would lead to infinite recursive call:
	void expr() { expr(); match("++"); }
	===================== LR.txt
	An LR recognizer/parser performs rightmost derivations (YACC builds LR-based recognizers).
	
	From ANTLR's author: LR recognizers are called bottom-up recognizers because they try to match the leaves of the parse tree and then work their way up toward the start- ing rule at the root. Loosely speaking, LR recognizers consume input symbols until they find a matching complete alternative.
	===================== Xtext.txt
	http://www.eclipse.org/Xtext/
	
	Xtext - Language Development Framework
	--------------------------------------
	From the web site: With Xtext you can easily create your own programming languages and domain-specific languages (DSLs). The framework supports the development of language infrastructures including compilers and interpreters as well as full blown Eclipse-based IDE integration. 
	===================== backtracking.txt
	When a rule is not LL(*), generator can use backtracking.
	The principle is to test each alternative of the rule in turn until one matches.
	
	From ANTLR ref book: The speed penalty arises from having to repeatedly evaluate a rule for the same input position. A decision can have multiple alternatives that begin with the same rule reference, say, expression. Backtracking over several of these alternatives means repeatedly invoking rule expression at the left edge. Because the input position will always be the same, evaluating expression again and again is a waste of time.
	To avoid this, ANTLR use MEMOIZATION, a technique which consists of storing the evaluation of expression, so it can be used again for the next alternative.
	===================== context and precedence.txt
	From ANTLR: What if you can’t interpret a phrase at all without examining a previous or future phrase? For example, in C++, expression T(i) is either a func- tion call or a constructor-style typecast (as in (T)i). If T is defined else- where as a function, then the expression is a function call. If T is defined as a class or other type, then the expression is a typecast.
	
	See ANTLR/predicate.txt
	===================== left-factoring.txt
	Left factoring is the action of removing left recursion.
	
	Left factoring is required when two or more grammer rule choices share a common prefix string, as in the rule:
	A → αβ | αγ
	Obviously, an LL(1) parser cannot distinguish between the production choices in such a situation.
	
	If we have productions of the form :
	A → Aα₁ | ... | Aαᵤ | β₁ | ... | βᵥ
	we write this as:
	A  → β₁A' | ... | αβᵥA'
	A' → A'α₁ | ... | A'αᵤ | ε
	
	Non LL production rules
	=======================
	Here is a typical example where a programming language fails to be LL(1):
					statement -> assign-stmt | call-stmt | other
					assign-stmt -> identfier := exp
	        call-stmt -> identifier ( exp-list )
	This grammar is not in a form that can be left factored. We must first replace assign-stmt and call -stmt by the right-hand
	sides of their defining productions:
					statement -> identifier := exp | identifier ( exp-list ) | other
	Then we left factor to obtain:
					statement  -> identifier statement' | other
					statement' -> := exp | ( exp-list )
	Note how this obscures the semantics of call and assignment by separating the identifier from the actual call or assign action.
	===================== lex, yacc & bison.txt
	YACC/Bison handle LALR grammars.
	===================== lookahead.txt
	LL(k) recognizers use a lookahead of k.
	LL(*) recognizers use an adaptative lookahead.
	
	The lookahead is the number of words/tokens you need to look at for distinguishing between two possibilities.
	
	Example:
	decl : 'int' ID '=' INT ';' // E.g., "int x = 3;" 
	     | 'int' ID ';' // E.g., "int x;"
	     ;
	
	The following alternative definition doesn't need a lookahead:
	decl : 'int' ID ('=' INT)? ';' // optionally match initializer
	       ;
	
	NON-LL(k) GRAMMAR
	=================
	Some grammars are non-LL(k).
	Example:
	decl : // E.g., "int x = 3;", "static int x = 3;"
	       modifier* 'int' ID '=' INT ';'
	       | // E.g., "int x;", "static int x;", "static register int x;",
	        // "static static register int x;" (weird but grammar says legal) 
	       modifier* 'int' ID ';'
	      ;
	
	modifier // match a single 'static' or 'register' keyword
	         : 'static'
	         | 'register' ;
	
	LL(*) can recognize such grammar.
	
	NESTED STRUCTURE
	================
	However LL(*) can't see past nested structures because it uses a DFA, not a pushdown machine, to scan ahead. This means it cannot handle some decisions whose alternatives have recursive rule references.
	Example:
	decl : 'int' declarator '=' INT ';' // E.g., "int **x=3;" 
	     | 'int' declarator ';' // E.g., "int *x;"
	     ;
	declarator // E.g., "x", "*x", "**x", "***x"
	     : ID
	     | '*' declarator ;
	===================== pushdown machine.txt
	Pushdown machine = state machine to which we've added a stack.
	
	The stack allows to memorize previous actions. Pushdown machine can produce correct sentences.
	
	To describre a pushdown machine, we use a Syntax Diagram, which looks like a set of flowcharts. One flowchart for each submachine (one submachine for each phrase).
	Ex:
	statement  --> (assignment) --> [';'] -->
	assignment --> [ID] --> ['='] --> (expr) -->
	expr      ---> [INT] ----------------------------------->
	          |--> [ID] --> ['['] --> (expr) --> [']'] --|
		  |--> ['('] --> (expr) --> [')'] -----------|
	
	The rectangular elements, between [], generate vocabulary symbols, and the rounded elements, between (), invoke the indicated submachine.
	Like a method call, the pushdown machine returns from a submachine invocation upon reaching the end of that submachine.
	===================== recursive rules.txt
	Left recursive rules are of the general form  : A → Aα | β
	Right recursive rules are of the general form : A → αA | β
	
	Both following rules generate a series {aⁿ | n ≥ 1}:
	A → Aa | a    is left recursive
	A → aA | a    is right recursive
	
	The following rules give a* (ε = void ?) :
	A → Aa | ε
	A → aA | ε
	===================== state machine.txt
	DFA is a Finite State Automaton.
	'Finite' means that the machine has a fixed number of states.
	
	DFA (Deterministic Finite Automation)
	Deterministic means that all transition labels emanating from any single state are unique. In other words, every state transitions to exactly one other state for a given label.
	
	A DFA can be cyclic, in which case it can produce an infinite number of results. Cyclic means that there's a loop between some states of the machine.
	
	On the other hand, if a DFA is acyclic, it means it contains no loop, and thus it produces a finite number of results.
	
	MEMORY
	======
	State machines do NOT have a memory. States do not know which states, if any, the machine has visited previously. This weakness is central to why state machines gen- erate some invalid sentences. Analogously, state machines are too weak to recognize many common language constructs.
	
	WHY STATE MACHINES CAN'T PARSE LANGUAGES
	========================================
	(From ANTLR ref book, which cites Pinker’s reasons from pp. 93–97 in The Language Instinct)
	State machines generate invalid sentences for the following reasons:
	• Grammatical does not imply sensible. For example, “Dogs revert vacuum bags” is grammatically OK but doesn’t make any sense. In English, this is self-evident. In a computer program, you also know that a syntactically valid assignment such as employeeName= milesPerGallon; might make no sense. The variable types and mean- ing could be a problem. The meaning of a sentence is referred to as the semantics. The next two characteristics are related to syntax.
	• There are dependencies between the words of a sentence. When confronted with a ], every programmer in the World has an invol- untary response to look for the opening [.
	• There are order requirements between the words of a sentence. You immediately see “(a[i+3)]” as invalid because you expect the ] and ) to be in a particular order (I even found it hard to type).
	
	===================== ANTLR/AST.txt
	// vi: ft=ANTLR
	
	// AST = Abstract Syntax Tree
	
	// ANTLR automatically builds AST.
	// All it requires, is specifying the following options:
	options {
		output = AST;
		ASTLabelType = CommonTree;
	}
	
	// XXX What's the difference between a CommonTree and a Tree ?
	
	// By default, ANTLR builds a flat tree (i.e.: a list of the nodes).
	// Thus we must specify which node is a subtree root, using token ^.
	expr: multExpr (('+'^|'-'^) multExpr)*;
	
	// Also a node can be excluded from the tree, using token !:
	atom: INT | ID | '(' ! expr ')' ! ; // --> exclude parenthesis.
	
	/////////////////////////
	// TREE REWRITE SYNTAX //
	/////////////////////////
	
	// An alternative syntax for specifying subtree roots and excluding nodes is the tree rewrite syntax:
	stat: expr NEWLINE        -> expr
	    | ID '=' expr NEWLINE   -> ^('=' ID expr)
	    | NEWLINE               -> ;
	
	// Using a tree rewrite syntax, one can defines imaginery nodes (i.e.: nodes that don't exist in the parsing rule):.
	variable: type ID ';' -> ^(VAR type ID) ;
	// the imaginery node token (VAR) must be declared inside the tokens secton (see tokens.txt).
	
	/////////////////////
	// PRINTING A TREE //
	/////////////////////
	
	myrule: mysubrule { System.out.println( $mysubrule.tree == null ? "null" : $mysubrule.tree.toStringTree()); };
	===================== ANTLR/add class members.txt
	// vi: ft=ANTLR
	
	// Adds functions or data to lexer or parser class (parser by default)
	@members {
	    private int myvar;
	
	    public static myfunc() {}
	}
	
	// Adds functions or data to Lexer.
	@lexer::members {
	    private int myvar;
	    public static myfunc() {}
	}
	
	// Adds functions or data to Parser.
	@parser::members {
	    private int myvar;
	    public static myfunc() {}
	}
	===================== ANTLR/errors.txt
	// vi: ft=ANTLR
	
	// to disable default error handling, and through exceptions
	@rulecatch { }
	
	// get number of errors
	org.antlr.runtime.Parser.getNumberOfSyntaxErrors();
	===================== ANTLR/exceptions.txt
	// vi: ft=ANTLR3
	
	// catching/throwing an exception
	date returns[java.util.Date value]: DATE { $value = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:dd").parse($DATE.text); };
		catch [java.text.ParseException e] {
			System.err.println(e.getMessage());
			throw new RecognitionException(input);
		}
	===================== ANTLR/grammar files.txt
	// vi: ft=ANTLR
	
	// USING TWO GRAMMAR FILES
	// Say that you're using 2 grammar files A.g and B.g to describe your grammar.
	// For instance A can be the lexer and B the parser.
	// A must be processed first by ANTLR.
	// B must contain a reference to the tokens file of A:
	parser grammar B;
	options {
		    tokenVocab=A;
	}
	// And ANTLR must be called with -lib options in order to specify in which directory is stored the tokens file.
	===================== ANTLR/header.txt
	// vi: ft=ANTLR
	
	// Adds code at top of lexer or parser file. 
	// If a parser is defined, adds the code to the parser file, otherwise adds it to the lexer file.
	@header {
	    package com.myfirm.mypackagename;
	}
	
	// Adds header code to the Lexer
	@lexer::header {
	    package com.myfirm.mypackagename;
	}
	
	// Adds header code to the Parser
	@parser::header {
	    package com.myfirm.mypackagename;
	}
	===================== ANTLR/lexer.txt
	// vi: ft=ANTLR
	
	// The lexer simply creates tokens following some strict rules:
	// 1) try to match tokens from top to bottom in the lexer rules (rules defined first are tried first);
	// 2) match as much text as possible. In case 2 rules match the same amount of text, the rule defined first will be matched.
	
	// Filter option (false by default)
	options { filter = true; }
	
	// A lexer rule begins with an uppercase letter
	Digit : ('+'|'-') ('0'..'9')+ ;
	
	// fragment rules don't create tokens
	fragment DIGIT : '0'..'9'; // won't give a token
	Integer : ('+'|'-') DIGIT+ ; // will give a token
	
	// To throw tokens away
	SINGLE_COMMENT: '//' ~('\r' | '\n')* ('\r'? '\n')+ { skip(); };
	
	// To write tokens to the special "hidden" channel. This channel is sent to the parser but not automatically inspected by it.
	WHITESPACE: (' ' | '\t')+ { $channel = HIDDEN; };
	
	// GREDDY option
	// The greedy option defaults to true except when the patterns ".*" and ".+" are used.
	// You can set greedy option explicitly
	MULTI_COMMENT options { greedy = false; } : '/*' .* '*/' { skip(); }; // here greedy=false is not necessary since ".*" is used.
	===================== ANTLR/overview.txt
	// -*- ANTLR -*-
	// vi: ft=ANTLR
	
	// ANTLR handles LL grammars. It uses an LL(*) parsing strategy, that is a parser with "infinite" look ahead.
	// LL(*) uses cyclic prediction machines.
	// LL(k) uses acyclic machines.
	
	// Lexer rules are named with uppercase characters.
	// Grammar rules are named with lowercase characters.
	
	// Direct output from parsing:
	// characters --> [LEXER} --> tokens [PARSER] --> [emitter] --> output
	
	// Building of an AST:
	// characters --> [LEXER} --> tokens [PARSER] --> AST
	
	// Using the AST to emit an output:
	// AST --> [tree walker] --> output
	
	// To produce characters output, it is recommanded to use StringTemplate.
	
	// ABOUT TREES
	// Derivation trees: when generating sentences.
	// Parse trees: when recognizing sentences. 
	===================== ANTLR/parser.txt
	// vi: ft=ANTLR
	
	// A parser rule begins with a lowercase letter
	decl : VAR_TYPE LABEL ';' ;
	
	// building an AST from parser
	options {output=AST;}
	
	// building a custom tree when parsing
	expr returns [pNode expr_tree]
	  { pNode e1 = NULL;
	    pNode e2 = NULL;
	  }
	  : e1 = primary_expr PLUS e2 = primary_expr
	    { expr_tree = factory.build_binary( PLUS, e1, e2 ); }
	  ;
	
	primary_expr [pNode returns id_node]
	  : IDENT { id_node = factory.make_node( n_id ); }
	  ;
	===================== ANTLR/predicate.txt
	// vi: ft=ANTLR
	
	////////////////////////
	// SEMANTIC PREDICATE //
	////////////////////////
	
	// A predicate is used to handle context-sensitive language. That means the choice of a rule depends on the context or on a preceding match.
	// It turns off the alternative that makes no sense in the current context.
	expr : {«lookahead(1) is function»}? functionCall 
	     | {«lookahead(1) is type»}? ctorTypecast ;
	
	//////////////////////////
	// SYNTACTIC PREDICATES //
	//////////////////////////
	// Encode the precedence of the alternatives.
	
	// In the following rule, the first alternative has a syntactic predicate that means, “If the current statement looks like a declaration, it is.” Even if both alternatives can match the same input phrase, the first alternative will always take precedence.
	stat : (declaration)=> declaration
	     | expression
	     ;
	
	/////////////////////
	// GATED PREDICATE //
	/////////////////////
	
	// Example with a comment that uses '*' as commenting character and always start at first column.
	// We must distinguish it from the use of '*' as the multiply sign.
	// If we use a simple disambiguating predicate ({...}?) then the parser enters an infinite loop.
	SINGLELINE_COMMENT : COMMENT_TOKEN (NEW_LINE | ~(NEW_LINE)*) { $channel = HIDDEN; } ;
	WHITESPACE : (SPACE | HTAB | NEW_LINE)+ { $channel = HIDDEN; } ;
	COMMENT_TOKEN: {getCharPositionInLine()==0}?=> STAR;
	MULTIPLY: STAR;
	===================== ANTLR/rule.txt
	// vi: ft=ANTLR
	
	// SPECIAL CHARACTERS ALLOWED: ? + *
	
	// Parenthesis can be used to group
	
	// '|' is used to signal a choice
	
	// .. is used to specify a range:
	LETTER : 'A'..'Z';
	
	// getting token text
	// $MYTOKEN.text
	myrule: ID '=' expr NEWLINE { $ID.text }
	
	// returning a value from a rule
	myrule returns [int value]
		: INT { $value = Integer.parseInt($INT.text); }
		;
	// using this returned value in another rule
	myotherrule: ID '=' myrule { $myrule.value}
	
	// arguments
	myrule[int arg]: /*...*/;
	mysecondrule: myrule[3];
	
	// iterating action in a rule
	multExpr returns [int value]
		: e=atom {$value = $e.value;} ('*' e=atom {$value *= $e.value;})*
		;
	===================== ANTLR/tokens.txt
	// vi: ft=ANTLR
	
	// tokens section is used to define tokens that aren't present inside the input grammar. Thus they aren't defined inside the LEXER part.
	// Most of they are used to create imaginery nodes inside the 'tree rewrite syntax' of the parser rules (see AST.txt).
	
	// tokens section must defined just after the options section:
	
	grammar Idf;
	
	options {
	    output = AST;
	}
	
	tokens {
		NAME;
	}
	===================== ANTLR/tree grammar.txt
	// vi: ft=ANTLR
	
	// Used to parse an AST. 
	
	// Tree grammar name, must match filename.
	tree grammar MyTreeGrammar;
	
	options {
		tokenVocab   = MyGrammar; // read token types from MyGrammar.tokens file, generated by the original grammar.
	       	ASTLabelType = CommonTree;
	}
	
	// rule syntax (identical to syntax of the "tree rewrite syntax" expressions of the original grammar.
	expr returns [int value]
		: ^('+' a=expr b=expr) {$value = a+b;}
		| ^('-' a=expr b=expr) {$value = a-b;}
		| ^('*' a=expr b=expr) {$value = a*b;}
		| ID
		{
			Integer v = (Integer)memory.get($ID.text);
			if ( v!=null ) $value = v.intValue();
			else System.err.println("undefined variable "+$ID.text);
		}
		|   INT                  {$value = Integer.parseInt($INT.text);}
	        ;
	===================== Coco/Coco.txt
	http://ssw.jku.at/Coco/
	
	Generate parser in : C#, Java, C++, F#, VB.Net, ...
	===================== Python/PYL.txt
	http://www.dabeaz.com/ply/
	
	A lex & yacc parser generator for Python.
	===================== Python/funcparserlib.txt
	A library for helping writing parser.
	This isn't a parser generator.
	===================== Python/pyparsing.txt
	
	===================== Perl/RecDescent/actions.txt
	# Actions are block codes executed when encountered by the parser.
	
	# An action succeeds if it returns a defined value, and fails otherwise.
	
	# $item[0] stores the name of the current rule.
	
	# $item[1..$#item] stores the values of all the matched items situated before the action.
	
	# %item stores also the values of items, but gives access through the name of the item, if it's a subrule. If the item is not subrule, then it can be accessed through a special naming:
	 stuff: /various/ bits 'and' pieces "then" data 'end' { save }
	     { print $item{__PATTERN1__}, # PRINTS 'various'
		 	$item{__STRING2__},  # PRINTS 'then'
			$item{__ACTION1__},  # PRINTS RETURN
			# VALUE OF save
		}
	
	# $item{__RULE__} stores the name of the current rule (=$item[0]).
	
	####################
	# START-UP ACTIONS #
	####################
	
	# Actions that are defined before all rules definitions.
	# These actions are executed inside the parser's special namespace just before rules are compiled.
	
	{ my $lastitem = '???'; }
	
	list: item(s)   { $return = $lastitem }
	
	item: book  { $lastitem = 'book'; }
		  bell  { $lastitem = 'bell'; }
		  candle    { $lastitem = 'candle'; }
	===================== Perl/RecDescent/comments.txt
	# USING SKIP DIRECTIVE
	
	# <skip> defines what the parser considers whitespace.
	
	parse: <skip: qr{(?xs:
	                            (?: \s+                       # Whitespace
	                                       |   /[*] (?:(?![*]/).)* [*]/  # Inline comment
	                                                 |   // [^\n]* \n?             # End of line comment
	)
								       )*}>
	       main_rule
	              /\Z/
	                     { $item[2] }
	
	
	# OR USING RULES
	
	AsIni: Line(s?) /\Z/
	
	Line:    CommentLine
	        | BlankLine
	        | SectionDeclaration
	        | AssignmentLine
	        | <error>
	
	
	CommentLine:    <skip: q{}> /^\s*/m ';' /.*$/m
	{
		print STDERR qq{\tSkipping comment: $item[4]\n} ;
	}
	
	BlankLine:    <skip: q{}> /^\s+$/m
	{
	    print STDERR qq{\tSkipping blank line\n}
	}
	===================== Perl/RecDescent/rules.txt
	#############
	# ARGUMENTS #
	#############
	
	# Passing arguments to a rule is done through @arg and %arg.
	
	# Arguments are passed through a comma separated list inside square brackets:
	classdecl: keyword decl[ $item[1] ]
	
	################
	# RETURN VALUE #
	################
	
	# Use the $return variable.
	
	 list:     <matchrule:$arg{rule}> /$arg{sep}/ list[%arg] { $return = [ $item[1], @{$item[3]} ] }
	         | <matchrule:$arg{rule}>                        { $return = [ $item[1]] }
	
	function: 'func' name '(' list[rule=>'param',sep=>';'] ')'
	
	###########
	# ACTIONS #
	###########
	
	See actions.txt.
	===================== Perl/Yapp/Yapp.txt
	Parse::Yapp
	
	Not maintained since 2001.
