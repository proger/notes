# sudo

Editing a file:
```bash
sudo -e /etc/some/system/filerc
```

Running a shell:
```bash
sudo -s
```

Running a command:
```bash
sudo <command>
```

Write to a root file with sudo:
```sh
echo "My Text" | sudo tee /my/file.cfg
```
