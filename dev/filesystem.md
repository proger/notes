# File system
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','python':'','cpp':''} -->

## Shell / system
### lsblk

List storage devices.
List block devices with mount points.

```sh
lsblk
```

Get model and UUID:
```sh
lsblk -o NAME,UUID,FSTYPE,LABEL,MOUNTPOINT,SIZE,MODEL
```

List UUIDs of a device:
```sh
lsblk -no NAME,UUID /dev/nvme0n1
```
Other method:
```sh
ls -l /dev/disk/by-uuid/
```

### shred

Overwrite a file to hide its content.

Shred a disk:
```sh
shred -vz /dev/sde
```

### blkid

Get UUID of storage devices.

### Benchmarking

#### hdparm

Install:
```sh
apt install hdparm # Debian
dnf install hdparm # Fedora
pacman -S hdparm   # Arch
```

Run:
```sh
hdparm -t --direct /dev/sdx1
```

#### bonnie++

Install:
```sh
apt install bonnie++ # Debian
dnf install bonnie++ # Fedora
pacman -S bonnie++   # Arch
```

Run:
```sh
bonnie++ -d /my/folder/where/to/run/tests
```

### LVM

 * [How to add an extra second hard drive on Linux LVM and increase the size of storage](https://www.cyberciti.biz/faq/howto-add-disk-to-lvm-volume-on-linux-to-increase-size-of-pool/).

PV (Physical Volume): a storage device (e.g.: `/dev/sda`).
VG (Volume Group): a group of PVs (e.g.: myvg made of `/dev/sda` and `/dev/sdc`).
LV (Logical Volume): a division of a VG (e.g.: myvg/root, myvg/swap).

Command   | Description
---       | ---
pvs       | List physical volumes.
vgs       | List volume groups.
lvs       | List logical volumes.
pvdisplay | Print detailed info on PVs.
vgdisplay | Print detailed info on VGs.
lvdisplay | Print detailed info on LVs.

On a LUKS encrypted device:
```sh
pvcreate /dev/mapper/my-encrypted-device
vgcreate my-vol-grp-name /dev/mapper/my-encrypted-device
lvcreate -l "100%FREE" my-vol-grp-name --name my-logical-vol-name
mkfs.ext4 /dev/meumeu/mediatheque
```

### Encryption

#### LUKS

 * [How can I add a new physical volume to extend an existing LUKS-encrypted lvm (volume group) and maintain encryption?](https://unix.stackexchange.com/questions/618877/how-can-i-add-a-new-physical-volume-to-extend-an-existing-luks-encrypted-lvm-vo).
 * [Using a single passphrase to unlock multiple encrypted disks at boot](https://unix.stackexchange.com/questions/392284/using-a-single-passphrase-to-unlock-multiple-encrypted-disks-at-boot).

Encrypted disks are in `/etc/crypttab`.

Encrypt a storage device with LUKS:
```sh
cryptsetup luksFormat /dev/sdx
```

Open/Decrypt an encrypted device:
```sh
cryptsetup luksOpen /dev/sdx my_name
```

Close a device
```sh
cryptsetup luksClose my_name
```

Extending an LVM volume on an encrypted disk with a new encrypted disk:
```sh
cryptsetup luksFormat /dev/sda
cryptsetup luksOpen /dev/sda sda_crucial_bx500ssd_crypt
pvcreate /dev/mapper/sda_crucial_bx500ssd_crypt
vgextend vgubuntu /dev/mapper/sda_crucial_bx500ssd_crypt
lvextend -l +100%FREE /dev/vgubuntu/root
resize2fs /dev/mapper/vgubuntu-root
## Add new disk in /etc/crypttab
sudo update-initramfs -c -k all
```

#### e4crypt

Allow to encrypt only a folder on a device.

Install on Debian:
```sh
apt install e2fsprogs
```

Create a key (at least one encrypted partition must exists, see `tune2fs` to enable encryption on an existing ext4 partition):
```sh
e4crypt add_key
```
If no salt (option -S) is provided it will create a key for each available hard
drive, using a different salt for each drive.
Otherwise it is possible to specificy the salt, for instance as a character
string:
```sh
e4crypt add_key -S "s:my string"
```

See `keyctl` to list content of keyring.
<!-- TODO IMPORTANT After rebooting the new key does not appear in the session keyring. The encrypted folder are still encrypted and thus unreadable. When running `e4crypt get_policy` the right encryption key is listed.
	* Do we need to save the key in some way?
	* Does the key have to be reloaded from somewhere?
	* Does the exact same key need to be created again?
	-->
The key is created inside the session keyring, thus it will be removed when you log off. We need to create it again, the same way, next time we log in.

Encrypting a directory (must be empty):
```sh
e4crypt set_policy 1133557799bbddff myfolder
```

Check if a directory is encrypted:
```sh
e4crypt get_policy myfolder
```

### Apple Time Capsule

 * [How to mount apple Airport Time Capsule with write access on linux Mint20.1?](https://stackoverflow.com/questions/70608542/how-to-mount-apple-airport-time-capsule-with-write-access-on-linux-mint20-1). Does not work.

### sfill

Wipes free disk space.

Install:
```sh
yay -S secure-delete      # Arch
apt install secure-delete # Debian
```

### Partition management

#### fsck

Check file system:
```sh
fsck /dev/sdb
```

#### wipefs

Wipe signature of a device.

List signatures:
```sh
wipefs /dev/sdb*
```

#### resize2fs

To shrink `/home` and expand another partition in LVM:
```bash
umount /home
e2fsck -f /dev/mapper/vg_oracle-lv_home
resize2fs /dev/mapper/vg_oracle-lv_home 20G
lvreduce -L 20G /dev/mapper/vg_oracle-lv_home
lvextend -l +100%FREE /dev/mapper/vg_oracle-lv_root
resize2fs /dev/mapper/vg_oracle-lv_root
mount /home
```
See https://unix.stackexchange.com/questions/213245/increase-root-partition-by-reducing-home.

#### e2label

Manage the partition label of an ext2/ext3/ext4 system.

Display the current partition label:
```sh
e2label /dev/sda1
```

Set the partition label:
```sh
e2label /dev/sda1 MyLabel
```

#### fatlabel

### Disk partitioning

#### gdisk

Get disk info:
```sh
gdisk -l /dev/sda
```

#### cfdisk

Ncurses version of fdisk.

#### fdisk

List all storage devices:
```sh
fdisk -l
```

Get disk info:
```sh
fdisk -l /dev/sdb
fdisk -l /dev/sdb1
```

Create/edit partition table:
```sh
fdisk /dev/sdb
```

Which partition types ?
Code | Type      | Comments
---- | --------- | --------------------------------
06   | FAT16     | Filenames in 8.3 format. Fixed number of sectors for a partition. Max size 2GB (original FAT16), and 16GB for newer versions of FAT16.
??   | VFAT      | Filenames in 8.3 format with handling of long filenames up to 255 chars. Fixed number of sectors for a partition.
0b   | W95 FAT32 | Good for USB keys. Long filenames allowed. Sector size well handled. Max size: 32GB.

To create Windows portable USB key in FAT (for a small key), FAT16 up to 2GB:
 * Create a single primary partition with number 1
 * Set type to b (W95 FAT32) for up to 2GB.
 * Set type to c (W95 FAT32 (LBA)) for up to 2TB.
 * Activate the partition (command `a`) --> bootable ???
 * `mkfs -t vfat -n LABEL /dev/sdz1`

 * `sudo fdisk /dev/sdX`
 * Delete existing partitions: `d ...`
 * Create new MBR/DOS partition table: `o`.
 * Create primary partition: `p`, `1`, ...
 * Choose partition type: `t`, `06` (FAT16).
 * `sudo mkfs.fat -F 16 -n MYLABEL /dev/sdX1`

`exFAT` seems better accepted:
 * See [What is the best way to format a USB stick such that it can be used with both Linux and Windows?](https://askubuntu.com/questions/1281698/what-is-the-best-way-to-format-a-usb-stick-such-that-it-can-be-used-with-both-li).
 * `lsblk -o NAME,UUID,FSTYPE,LABEL,MOUNTPOINT,SIZE,MODEL` to identify the USB device.
 * `umount /dev/sdX1`.
 * `sudo fdisk /dev/sdX`
 * Create new MBR/DOS partition table: `o`.
 * Create new primary partition: `n`, `p`, defaults.
 * Choose partition type: `t`, `7` (HPFS/NTFS/exFAT).
 * Save and quit: `w`.
 * `sudo mkfs.exfat -n MYLABEL /dev/sdX1`

To create Windows USB key (8GB or more):
```sh
parted /dev/sdX mklabel gpt
parted /dev/sdX mkpart primary 0% 100%
mkfs.exfat -L MYLABEL /dev/sdX1
```

### Disk formatting

#### mkfs

Install:
```sh
yay -S dosfstools # For FAT*
yay -S exfatprogs # For exFAT
```

Format a ext4 partition with encryption enabled:
```sh
mkfs -t ext4 -O encrypt /dev/sde2
```

Format a FAT partition and set its label:
```sh
mkfs -t fat -n MyLabel /dev/sde1
```

### FUSE

#### fuseiso

 * [FuseISO](https://wiki.archlinux.org/title/FuseISO).

Mount ISO as user.

Install:
```sh
pacman -S fuseiso
```

Usage:
```sh
fuseiso my.iso myfolder # Mount
fusermount -u myfolder  # Unmount
```

### Apple

applecommander: manipulates Apple ][ disk images. --> fails to install on Archlinux.

### autofs

Mount device automatically.

Install:
```sh
yay -S autofs # ArchLinux
apt install autofs # Debian
```

Inside `/etc/autofs/auto.master`, write a line:
```
/mnt/myfolder /etc/autofs/auto.myext
```
Then inside `/etc/autofs/auto.myext`, to mount automatically a USB drive:
```
mysubfoldername -fstype=auto UUID=f5450ff5-0f84-4194-96ad-638149a5897e
```

For NFS, in `/etc/autofs/auto.master`:
```
/mnt /etc/autofs/auto.nfs --ghost,--timeout=60
```
Then inside `/etc/autofs/auto.nfs`:
```
Photos_serveur  -fstype=nfs,rw   192.168.1.3:/home/blinckers/Photos
```

Start the service:
```sh
systemctl enable --now autofs
```
### ccd2iso

Convert a bin/cue format into ISO:
```sh
ccd2iso myfile.bin myfile.iso
```

May fail. Try `iat` in that case.
### cdrecord

Pour graver une image iso :
```bash
cdrecord -v speed=4 dev=/dev/cdrom1 image.iso
```

Pour un disque multisession (surlequel on peut ajouter d'autres fichiers plus tard) :
```bash
cdrecord	-multi ...
```

Pour vider un CD-RW
```bash
cdrecord -v speed=2 dev=/dev/cdrom1 blank=fast
```

Pour graver un CD audio:
The audio files must be in CD-DA CD audio format (they usually have a `.cdda' or `.cdr' file name extension). 
They should contain 16-bit stereo at 44,100 samples/second.
En fait des fichiers wav suffisent.

Convertir de ogg en wav :
```bash
ogg123 -d wav -o file:mysong.wav mysong.ogg
```
ou
```bash
oggdec *.ogg
```
ou de mp3 en wav :
```bash
mpg123 -s mysong.mp3 | sox -t raw -r 44100 -s -w -c 2 - mysong.wav
```

Puis graver :
```bash
cdrecord -pad -v dev=1,0 -dao *.wav		
```
ou `dev=/dev/cdwriter`.

`-pad`: Because many structured audio files do not have an integral number of blocks (1/75th second) in length, it is often necessary to specify the -pad option as well. 
`-dao`: Set Disk At Once mode. This currently only works with MMC drives that support non raw Session At Once mode.

### dd

Copy a device into a .iso file:
```bash
dd if=/dev/dvd of=dvd.iso
```

To make an ISO from your CD/DVD, place the media in your drive but do not mount it. If it automounts, unmount it.
```bash
dd if=/dev/dvd of=dvd.iso # for dvd
dd if=/dev/cdrom of=cd.iso # for cdrom
dd if=/dev/scd0 of=cd.iso # if cdrom is scsi 
```

Command for writing a huge ISO on a USB key:
```bash
dd bs=4M if=myiso.iso of=/dev/sdx status=progress oflag=sync && sync
```
### eject

Eject disk:
```sh
eject
```
### growisofs
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Install on ArchLinux:
```sh
pacman -S dvd+rw-tools
```

Burn a DVD or BD:
```sh
growisofs -dvd-compat -Z /dev/sr0=myimg.iso
```

`dvdrecord` ne marche pas, il vaut mieux utiliser `growisofs`.

How to burn an ext2 fs on a DVD:
 * Create an empty file of 4400MB (`dd if=/dev/zero of=bigfile bs=1024k count=4400`)
 * Loopback mount the file (losetup /dev/loop0 bigfile)
 * Put an ext2 filesystem on the device (mke2fs -b4096 -N2000 bigfile)
 * Mount the device as root (mount /dev/loop0 /mnt/temp)
 * Copy all of the data to the loop mount (cp -r pr /mnt/temp)
 * Unmount the loop device (umount /dev/loop0)
 * Now burn the disk, ext2 will hold a 4GB file (growisofs -Z /dev/scd0=bigfile)

Pour graver un dvd :
```bash
mkisofs -r -f -iso-level 4 -V "DVD02" dir >dvd.iso
growisofs -dvd-compat -Z /dev/dvdrw=dvd.iso
growisofs -dvd-compat -Z /dev/dvd -iso-level 4 $dir
```

Graver un .iso sur un dvd :
```bash
growisofs -dvd-compat -Z /dev/cdwriter=file.iso
```
`-dvd-compat` permet de fermer le DVD.

```bash
growisofs -Z /dev/dvd=/dev/zero
```

Capacité d'un DVD simple couche : 4706074624 octets (taille max du fichier iso)

`growisofs` n'arrive pas éffacer les disques DVD-RW, il faut utiliser :
```bash
dvd+rw-format -blank /dev/cdwriter
```
### iat

Install:
```sh
apt install -y iat # Ubuntu
```

Convert a bin/cue format into ISO:
```sh
iat -i myfile.bin -o myfile.iso --iso
```
### mkisofs

Create an ISO file from a folder:
```bash
mkisofs -iso-level 4 -o disk.iso disk
```

For a Windows system:
```bash
mkisofs -iso-level 4 -J -R -input-charset iso8859-1 -o winxp.iso WinXP
```
### u3-tool

To get rid of the U3 CD-ROM partition on a USB U3 key:
```sh
sudo u3-tool -p 0 /dev/sdb
```
### Explorers
#### Nautilus

Gnome file explorer.
#### ranger

An ncurses/vim-like file manager.

Key | Description
--- | ----------------------------
om  | Sort by modification time
#### thunar

Xfce file manager.
### basename

Return filename portion of pathname:
```bash
basename /my/path/to/a/file
```
### cd

Change to previous working directory:
```sh
cd -
```
Equivalent to:
```sh
cd "$OLDPWD" && pwd
```
Side effect: write directory to output.
### chmod

When the sgid bit is set on a directory, all files & dirs created in this
directory will automatically have the same group as the directory:
```bash
chmod g+s mydir
```

Set execute/search flag for directories only:
```
chmod -R g+X mypath
```

To recursively remove execution flag on files in a directory tree, with GNU version:
```sh
chmod -R a-x,a+X mydir
```

### setfacl / getfacl

Set access rights for multiple groups and multiple users.

### convmv

Convert the encoding of a filename:
```
convmv -f iso8859-1 -t utf-8 --notest myfile.ext
```

Installation on Debian:
```
apt install convmv
```
### cramfs

Read-only file system (for CDROM, ...).
Replaced by SquashFS, except for very small memory devices.
### dirname

Return directory portion of pathname:
```bash
dirname /my/path/to/a/file
```
### dolphin

KDE file manager.

Install on Archlinux:
```sh
pacman -S dolphin
```

To get icons, install also a style like `breeze`:
```sh
pacman -S breeze
```
### du

To measure disk usage of a folder:
```bash
du -shc <folder>
```
### e2label

Get label:
```sh
e2label /dev/sb1
```

Set label:
```sh
e2label /dev/sb1 newname
```
### filefrag

Get the physical offset of a swap file:
```sh
filefrag -v /mySwapFile | awk '$1=="0:" {print substr($4, 1, length($4)-2)}'
```
### file

Gives MIME type of a file using among other things the magic number stored in a magic list (see man file).

Install:
```sh
apt-get install -y file
```

Prints human readable string:
```sh
file <file>
```

Prints only the type:
```sh
file -b --mime-type <file>
```

The `file` command also prints the character encoding. 

Print the label of a drive:
```sh
file /dev/sdb1 -s
```
### find
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Look for files containing a specified string in their name, starting from the root:
```sh
find / -name "gnomeprint"
```

Use regexp:
```sh
find / -regex <pattern>
find / -iregex <pattern> # case insensitive
find -E / -regex <pattern> # use extended regular expressions
```

Deleting found files:
```sh
find documents/ -iname '*~' -delete
```

Execute a command for each found file:
```sh
find <dir> -iname '*.txt' -exec printf '{}' \;
find . -iname '*.todo' -exec perl -e '$f="{}"; ($g=$f) =~ s/\.todo$/ N.txt/; printf "$f --> $g\n"; system("cat \"$f\" >>\"$g\"");' \;
```

Execute some code for each found file:
```sh
find . | while read f ; do 
	echo Found: $f
done
```
or
```sh
while IFS= read -r -d $'\0' file; do
  echo "$file"
done <<(find . -print0)
```

Move recent files (from today) from Download folder to another place:
```sh
find ~/Downloads/ -type f -daystart -mtime 0 -exec mv \{\} my/other/place/ \;
```

Levels of search
```sh
find . -maxdepth 1
```
level 0 tells to look only among the command line arguments
```sh
find R-* -maxdepth 0 -type d # return the list of all directories beginning wiht R-*
```

Filter on time:
```sh
find <dir> -newermt 2009-10-15
```
Find all files whose modification type is more recent than the specified date.
<!-- WARNING --newermt Non standard ? -->

Filter on type:
```sh
find <dir> -type <type>
```

Type | Description
---- | -----------
b    | Block special.
c    | Character special.
d    | Directory.
f    | Regular file.
l    | Symbolic link.
p    | FIFO.
s    | Socket.

Operators:

Operator       | Description
-------------- | -----------
-false         | False.
-true          | True.
! expr         | Logical not.
-not expr      | Logical not.
expr expr      | Logical and.
expr -and expr | Logical and.
expr -or expr  | Logical or.
( expr )       | Grouping.

Filter on permissions:
```sh
find <dir> -perm -u+x       # Standard notation. 
find <dir> -perm +0422      # Octal notation.
find . -type f -perm 700    # Search for executable files.
```
Permission prefix | Decription
----------------- | ------------------------
-                 | All bits are set.
NOTHING           | Bits match exactly the file mode.
+                 | Any of the bist is set. Be careful with this + sign because it can be interpreted as a mode (not clear why for me, TODO).

Permissions GNU notation:
```sh
find <dir> -perm /u=x
```

Print output with \0 separator for results instead of \n:
```sh
find ... -print0
```

Quote filenames in output, one filename per line:
```sh
find ... -printf '"%p"\n'
```

List small files:
```sh
find . -maxdepth 1 -type f -size -50M
```
### findmnt

Find a file system.

Get the UUID of mounted swap file:
```sh
findmnt -no UUID -T /mySwapFile
```
### flock

Creates a locking file.

Execute a command only if the locking file is free:
```sh
flock -n myfile.lock mycommand
```
### fstab

To mount automatically and system wide a USB drive:
1. List the devices to find the right one by running `blkid`.
2. Add the following line to `/etc/fstab` for an ext4 drive:
   UUID=.... /mnt/some/folder ext4 auto,user,rw 0 0
3. Run `mount -av` as root to reload `/etc/fstab`.

Mounting an NFS drive:
```fstab
192.168.1.41:/mnt/zomeu/mediatheque /mnt/mediatheque nfs ro,hard,intr,rsize=8192,wsize=8192,timeo=14,noauto 0 0
```
### install

It has the interesting property of changing the ownership of any file it copies to the owner and group of the containing directory. So it automatically sets the owner and group of our installed files to root:root if the user tries to use the default /usr/local prefix, or to the user’s id and group if she tries to install into a location within her home directory.

Create directories:
```bash
install -d mydir
```

Install file:
```bash
install src_file dst_file
```

Install several files in a directory:
```bash
install src_file_1 src_file2 ... dst_directory
```

Setting mode (by default set to `rwxr-xr-x`):
```bash
install -m u+rwx ...
```
### ln

Create a symbolic link inside current directory:
```sh
ln -s /my/target
```

Create a symbolic link inside a specific directory:
```sh
ln -s -t /my/folder/ /my/target
```

Create a symbolic link with a specific link name:
```sh
ln -s /my/target /my/link
```

Remove existing destination:
```sh
ln -sf /my/target /my/link
```

When running twice the creation of a symbolic link to a directory, a symbolic
link is created inside the initial folder:
```sh
ln -s /my/target/dir /my/link/folder # /my/link/folder is created correctly as
                                     # a symbolic link.
ln -sf /my/target/dir /my/link/folder # Now a new symbolic link
                                      # /my/link/folder/dir is created.
```
This is because the second time `/my/link/folder` exists and is thus treated as
a target directory.
To avoid this, use `-T` (`--no-target-directory`) option:
```sh
ln -s /my/target/dir /my/link/folder
ln -sfT /my/target/dir /my/link/folder
```
### ls

BSD ls and GNU ls are different.
GNU ls command is part of the coreutils package.

Sorting by size:
```bash
ls -S *
```

Reverse sort order:
```bash
ls -r *
```

Quoting names and escaping characters:
```bash
ls -Q
```

Print access time instead of last modification time:
```bash
ls -u *
```

In MacOS-X, a `@` character after permissions means that the file has additional attributes. You can see these additional attributes by typing the following command:
```bash
xattr -l <filename>
xattr -d <attrname> <filename> # to remove an attribute
```

In MacOS-X, a `+` character after permissions means that the file has an ACL, short for Access Control List, which is used to give fine grained control over file permissions, beyond what is available with the regular unix permission tables.
Entering the following command will show these additional permissions for files in a directory:
```bash
ls -le
```

#### Coloring ls output

 * [dircolors: modify color settings globaly](https://unix.stackexchange.com/questions/94299/dircolors-modify-color-settings-globaly). Tips on how to set 256 colors.
 * [Customize file extension colors for ls output in Cshell](https://stackoverflow.com/questions/27290258/customize-file-extension-colors-for-ls-output-in-cshell).
 * See `man dircolors` and `man dir_colors`.

`LSCOLORS` (see man ls), is an environment variable that defines the colors to be used by `ls` command. The default is "exfxcxdxbxegedabagacad", i.e. blue foreground and default background for regular directories, black foreground and red background for setuid executables, etc.

Color code | Descrption
---------- | -----------------------------------------------
a          |  Black.
b          |  Red.
c          |  Green.
d          |  Brown.
e          |  Blue.
f          |  Magenta.
g          |  Cyan.
h          |  Light grey.
A          |  Bold black, usually shows up as dark grey.
B          |  Bold red.
C          |  Bold green.
D          |  Bold brown, usually shows up as yellow.
E          |  Bold blue.
F          |  Bold magenta.
G          |  Bold cyan.
H          |  Bold light grey; looks like bright white.
x          |  Default foreground or background.

Note that the above are standard ANSI colors.  The actual display may differ depending on the color capabilities of the terminal in use.

The order of the attributes are as follows:

Rank | Description
---- | ---------------------------------
1    | Directory.
2    | Symbolic link.
3    | Socket.
4    | Pipe.
5    | Executable.
6    | Block special.
7    | Character special.
8    | Executable with setuid bit set.
9    | Executable with setgid bit set.
10   | Directory writable to others, with sticky bit.
11   | Directory writable to others, without sticky bit.


### Midnight Commander

A console base file manager.
### mcopy

Copy MSDOS files to/from UNIX.
### mktemp
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','bash':'sh'} -->

 * GNU version replaces Xs in template.
 * BSD version doens't use the Xs and appends its own random string.
 
We can test if we're running BSD or GNU version
```bash
test_template=tmp.XXXXXX
bsd_version=$(mktemp -u -t $test_template | grep '/$test_template')
if [ -n "$bsd_version" ] ; then
	options="-t $prefix"
else
	options="-t $prefix.XXXXXX"
fi
tmp_file=$(mktemp $options)
```

Or always set XXXXX template:
```bash
tmp_file=$(mktemp -t tmp.XXXXXX)
```

To create a directory (BSD and GNU):
```bash
tmp_file=$(mktemp -d -t tmp.XXXXXX)
```
### mlabel

Set label of an MSDOS disk.
### mount

```bash
mount -t cifs //123.456.78.90/MyFolder my/local/dir -o username=...,password=...
```

To mount automatically, on demand, create a file /etc/autofs/auto.myext:
```conf
mylocalfoldername -fstype=cifs,username=******,workgroup=*****,password=*******,uid=${UID},gid=******* ://myserver/my/path/to/my/folder/
```

Mount a samba directory under MacOS-X:
```bash
mount -t smbfs //PR228844@DRTFAUCON-1/DCSI/LM2S/CLIMB smbnetdir
```
Then it asks the password.

Mount a samba directory under Linux:
```bash
mount -t smbfs -o username=PR228844 //DRTFAUCON-1.INTRA.CEA.FR/DCSI/LM2S/CLIMB smbnetdir
mount -t smbfs -o username=PR228844,password=Mypass0123 //DRTFAUCON-1.INTRA.CEA.FR/DCSI/LM2S/CLIMB smbnetdir
```

To allow non-root users to mount a samba drive, use sudo.
Create a `samba` group and add users to it.
Edit `/etc/sudoers` and add the following line:
```
%samba ALL=(ALL) NOPASSWD: /bin/mount,/bin/umount,/sbin/mount.cifs,/sbin/umount.cifs,/usr/bin/smbmount,/usr/bin/smbumount
```
The `NOPASSWD:` directive prevents `sudo` from prompting for the user's password. It is optional.

Mount an ISO on Linux:
```bash
mount -o loop file.iso <folder>
```

Mount the disk of an Apple AirPort Time Capsule:
```sh
sudo mount.cifs //192.168.0.xxx/Data /mnt/airport -o user=smith,password=MyPwd,sec=ntlm,vers=1.0,gid=$(id -g),uid=$(id -u),forceuid,forcegid
```

Mount a FAT16 disk image:
```sh
fdisk -l mydisk.img # Get the sector size (e.g.: 512) and the start sector (e.g.: 32)
mkdir myfolder
sudo mount -t msdos -o loop,offset=16384 mydisk.img myfolder
```
### ncdu

Analyze disk usage:
```sh
ncdu
```
or
```sh
ncdu /my/path
```

Do not explore mounted drives:
```sh
ncdu -x /
```
### parted

Get disks info:
```sh
parted -l
```
### Partitioning

 * [9.15.5. Recommended Partitioning Scheme](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/installation_guide/s2-diskpartrecommend-x86).

/         >=30GB (Ubuntu)
/boot     500MB, ext4
/boot/efi 300MB, FAT32/EFI System (type 1), flagged as boot partition.
/swap     --> swap partition cannot be encrypted, better to have a swap file on the encrypted root partition.
/home
/var ?
/usr ?
/data ?
### readlink

Get the path pointed to by a symbolic link:
```sh
readlink my/link
```

Get the absolute path of file, folder, etc:
```sh
readlink -f  my/path/to/something
```
### realpath

Install on macos:
```sh
brew install coreutils
```

Returns resolved absolute path:
```sh
realpath my/path/to/some/where
```

Get relative path to directory:
```sh
realpath --relative-to=/my/base/path my/path/to/some/where
```

Not portable. Not installed by default on OpenBSD.
Use `readlink -f` instead.
### relpath

Compute relative path from given base directory.

Install on Debian:
```sh
apt-get install fp-utils
```
### SquashFS

Read-only file system (for CDROM, ...).
Successor of cramfs
### stat

Gives information on a file:
```sh
stat myfile
```

#### On macos

```sh
stat -f %z fs/ln.md
```

#### On Linux

Print size of file:
```sh
stat -c "%s" myfile
```

The `--printf` does the same as `-c` and includes backslash escapes:
```sh
stat --printf="%s\n" myfile
```

List found files by size:
```sh
find . -name *.h5 | xargs -n 1 -I @ stat --printf="%s %n\n" "@" | sort -n
```
### swapon

Create SWAP file and use it:
```sh
cd /mnt
dd if=/dev/zero of=swap bs=1M count=1024
mkswap swap
swapon swap
chmod 0600 swap
```

Get info on used swap:
```sh
swapon
```
### trash-empty

Empty the trash:
```sh
trash-empty
```

Remove only from the trash files older than 5 days (i.e.: more than 5 days in
the trash):
```sh
trash-empty 5
```
### trash-list

List files in trash:
```sh
trash-list
```
### trash

The default user Trash folder of the freedesktop specifications is `~/.local/share/Trash` (`$XDG_DATA_HOME/Trash`). See:
 * [The FreeDesktop.org Trash specification](https://specifications.freedesktop.org/trash-spec/trashspec-latest.html)
 * [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-0.6.html).

Install:
```sh
pacman -S trash-cli # Archlinux
apt install trash-cli
```

Command line trashcan (recycle bin) interface, respecting the freedesktop specifications.

Put file in trash:
```sh
trash myfile
```

Get help:
```sh
trash -h
```
### trash-restore

Restore a file:
```sh
trash-restore /full/path/to/my/file
```
### trash-rm

Remove trashed files matching a pattern:
```sh
trash-rm '*.o'
```
### tree

List contents of directories in a tree-like format:
```sh
tree mydir
```
### tune2fs

Manage ext2/ext3/ext4 partitions or volumes.

Enable encryption on an existing ext4 partition:
```sh
tune2fs -O encrypt /dev/xxx
```

Get drive information:
```sh
tune2fs -l /dev/xxx
```

Set partition label:
```sh
tune2fs -L myLabel /dev/sda1 
```
### udisksctl

Mount and unmount removale media.

Mount a removable media:
```sh
udisksctl mount -b /dev/sdc1
```

Unmount a removable media:
```sh
udisksctl unmount -b /dev/sdc1
```

Get info on device:
```sh
udisksctl info -b /dev/sdb
```
### umask

`umask` set the default permissions for new files.
### which

The `which` command returns the path of a command. If the command cannot be found, an error is returned.

On BSD/macOS `which` has a silent option:
```sh
which -s myprog
```

Install:
```sh
dnf -y install which
```

### xattr

macos file attributes.

To see special file attributes:
```sh
ls -@
```
or
```sh
xattr <filename>
```

To remove attributes:
```bash
xattr -d <attribute_name> <filename>
```

To remove attributes for a whole directory:
```bash
xattr -dr <attribute_name> <dirname>
```

## Python

 * [os — Miscellaneous operating system interfaces](https://docs.python.org/3/library/os.html).
   + [os.path — Common pathname manipulations](https://docs.python.org/3/library/os.path.html).
 * [shutil — High-level file operations](https://docs.python.org/3/library/shutil.html).
 * [tempfile — Generate temporary files and directories](https://docs.python.org/3/library/tempfile.html).

### pathlib

 * [pathlib — Object-oriented filesystem paths](https://docs.python.org/3/library/pathlib.html).

Expand `~`:
```python
import pathlib

mypath = pathlib.Path("~/Downloads").expanduser()
```

### os & shutil

Path manipulation:
```python
import os
os.path.dirname(path)	# git directory part
filename = os.path.basename(mypath)
(dirname, filename) = os.path.split(path)
path = os.path.join(dirname, filename)
(root, ext) = os.path.splitext(path)
```

Get name and extension:
```python
name, ext = os.path.splitext(os.path.basename(path))
```

Get path of the current script:
```python
import os
script_path = os.path.realpath(__file__)
```

Get the folder path of a path:
```python
dirs = os.path.dirname(mypath)
```

Test if file exists:
```python
import os
os.path.exists("/my/path/to/file")
```

Test if it is a directory, a file, ...:
```python
os.path.isfile("/my/path")
```

Directories
```python
os.path.isdir("/my/path")
os.rmdir("mydir") # Delete an empty directory
os.mkdir('mydir') # One directory
os.makedirs("/my/dir") # Multiple folders
os.makedirs("/my/dir", exist_ok=True) # No exception if folder exists
```

Current working directory:
```python
os.getcwd()
```

Change directory:
```python
os.chdir(mypath)
```

Remove a file:
```python
os.unlink(myfile)
os.remove(myfile)
```

Get current working directory:
```python
os.getcwd()
```

Rename a file:
```python
os.rename('current_name', "new_name")
```

Test if a path is absolute:
```python
os.path.isabs(mypath)
```

Copy a file:
```python
shutil.copy(src, dst)
```

Remove a folder and its content:
```python
shutil.rmtree(myfolder)
shutil.rmtree(myfolder, ignore_errors=True)
```

Copy a file only if more recent:
```python
dstfile, updated = distutils.file_util.copy_file(src, dst, update=1)
```

Copy a folder recursively:
```python
shutil.copytree(srcdir, dstdir, dirs_exist_ok=True)
```

Copy a folder recursively, copying files only if more recent:
```python
distutils.dir_util.copy_tree(src, dst, update=1)
```

Copy the mode of a file onto another:
```python
shutil.copymode("model_file", "target_file")
```

## C++

```cpp
 #include <boost/filesystem.hpp>

namespace fs = boost::filesystem;
```

Creating path:
```cpp
boost::filesystem::path boost_path("/my/path");
```

Test if path is empty:
```cpp
my_path.empty();
```

String used:
```cpp
boost::filesystem::path::value_type     my_char; // char or wchar_t
boost::filesystem::path::string_type    my_string; // std::string or std::wstring
```

Testing directory existence:
```cpp
boost::filesystem::path data_dir(fs::current_path());
boost::filesystem::is_directory(data_dir);
```

Testing existence:
```cpp
if (boost::filesystem::exists(my_dir))
	throw error();
```

Creating a directory:
```cpp
boost::filesystem::create_directory("my_relative_dir");
```

Testing file type:
```cpp
if (boost::filesystem::is_regular_file(my_path))
	/*...*/;
```

Path decomposition:
```cpp
boost::filesystem::path p;
p.parent_path(); // get parent folder
p.filename(); // get filename
p.stem(); // get filename without directory path and without extension
p.extension(); // return the extension, including the dot, if it exists.
p.replace_extension(new_ext); // replace the current extension with a new one, dot included.
```

Loop on all sub-paths of a path:
```cpp
for (auto p = mypath.begin() ; p != mypath.end() ; ++p) {
    ...
}
```

Concatenate paths:
```cpp
mynewpath = mypath1 / mypath2;
```

Convert path to string:
```cpp
boost::filesystem::path p;
std::string s = p.string();
```

Generic pathname format: uses / (so on Windows, \ are converted to /).
Native pathname format: uses / on UNIX and \ on Windows.

List content of folder (glob):
```cpp
for (auto p = boost::filesystem::directory_iterator(mypath) ;
   p != boost::filesystem::directory_iterator() ; ++p) {

    auto a_path = p->path();
}
```

