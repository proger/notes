# gcc
<!-- vimvars: b:markdown_embedded_syntax={'c':''} -->

 * [GCC SSE code optimization](https://stackoverflow.com/questions/7919304/gcc-sse-code-optimization).

Print default include dirs:
```sh
echo | gcc -xc -E -Wp,-v -
echo | gcc -xc++ -E -Wp,-v -
```

Using SSE instructions, we can write a matrix sum example that uses vectorization:
```c
#include <xmmintrin.h>

mm_c = (__m128*)c;
mm_b = (__m128*)b;
mm_a = (__m128*)a;
for (int i = 0 ; i < N / 4 ; ++i) {
	*mm_c = _mm_add_ps(*mm_a, *mm_b);
	mm_c++;
	mm_a++;
	mm_b++;
}
```
