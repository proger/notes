# objdump

Print symbol table of a dynamic library:
```sh
objdump -T libfoo.so
```
