# pkg-config

List all supported packages:
```bash
pkg-config --list-all
```

Get include directory:
```bash
pkg-config --variable=includedir axis2c
```

Get library directory:
```bash
pkg-config --variable=libdir axis2c
```

Display all linker flags:
```sh
pkg-config --libs mylib
```
