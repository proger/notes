# readelf

Get DWARF version of a binary:
```sh
readelf --debug-dump=info my_binary | grep -A 2 'Compilation Unit @'
```
