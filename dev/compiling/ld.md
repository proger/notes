# ld

GNU linker.

Libraries must be listed at the end:
```sh
ld a.o b.o c.o -o myapp -lmylib
```
