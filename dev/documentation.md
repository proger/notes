# Documentation
<!-- vimvars: b:markdown_embedded_syntax={} -->

## NEWS and CHANGELOG files

### CHANGELOG file

 * [Keep a Changelog](https://keepachangelog.com/en/1.1.0/).
 * [Changelog](https://en.wikipedia.org/wiki/Changelog).
 
Name `CHANGELOG.md`.

### NEWS file

What to put inside a NEWS file and how to present it.

#### Plain NEWS file

Example of a plain `NEWS` file (file is indented in order to avoid recognition
of syntax by markdown):
```
  CHANGES IN VERSION 1.3.4
  ------------------------
  
  BUG FIXES
  
    * blablabla...
    * blablabla...
    * blablabla...
  
  NEW FEATURES
  
    * blablabla...
    * blablabla...
  
  DEPRECATION ANNOUNCEMENT
  
    * blablabla...
    * blablabla...
  
  USER SIGNIFICANT CHANGES
  
    * blablabla...
    * blablabla...
```

#### Markdown NEWS file

Example of a markdown `NEWS.md` file:
```markdown
## NEWS

### 1.1.1 - 2023-06-06

 * Limit size of floating text message when hovering on consequence values in gene page and transcript page.

### 0.9.4 - 2022-12-08

#### Minor changes

 * On variant page, set same float format (`%0.4f`) for all lines of frequency column, including total.
 * On variant page, removed rows 'Total', 'Raw' and 'POPMAX' in GnomAD Population Frequencies table.
 * On variant page, set understandable population names in GnomAD Population Frequencies table.

#### Bug fixes

 * Re-enable text completion inside search fields.
 * Solves display of Site Quality Metrics on variant page.

### 0.9.3 - 2022-12-01

#### Enhancement

 * Better examples on homepage.

```
## MkDocs

 * [MkDocs](https://www.mkdocs.org/).
 * [Markdown Exec](https://pawamoy.github.io/markdown-exec/).
 * [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

Project documentation with Markdown.

## Jupyter

### Jupyter (main command)

 * [Jupyter](https://wiki.archlinux.org/title/Jupyter).

Install on Archlinux:
```sh
pacman -S jupyter
```

Run on console:
```sh
jupyter console
```

Choose kernel:
```sh
jupyter console --kernel bash
```

Run Jupyter Lab:
```sh
jupyter lab
```

Install a package from inside Jupyter:
```python
import sys
!{sys.executable} -m pip install numpy
```

In Jupyter console:
	Enter       Add a new line of command
	Shift+Enter Execute lines of command

Install an additional version of Python on Archlinux:
```sh
yay -S python39
### Install manually setuptools and pip,
### See https://pypi.org/ to download sources packages of setuptools and pip.
### Then install first setuptools:
sudo python3.9 setup.py install
sudo chmod -R a+rX /usr/lib/python3.9/site-packages
### And pip:
sudo python3.9 setup.py install
sudo chmod -R a+rX /usr/lib/python3.9/site-packages

python -m pip install ipykernel
sudo python -m ipykernel install --name python3.9 --display-name "Python 3.9"
sudo chmod -R a+rX /usr/share/jupyter/kernels/python3.9
### Modify /usr/share/jupyter/kernels/python3.9/kernel.json to have /usr/bin/python3.9 instead of /usr/bin/python.
```
See [Using Multiple Python Versions and Environments with RStudio Workbench and Jupyter Notebooks](https://docs.rstudio.com/rsw/integration/jupyter-multiple-python-versions/).

#### Kernels

 * [Native R kernel for Jupyter](https://github.com/IRkernel/IRkernel).
 * [Jupyter kernels](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels).

List installed kernels:
```sh
jupyter kernelspec list
```

Install bash kernel:
```sh
python -m pip install bash_kernel
python -m bash_kernel.install
```
Use `nbconvert` version 7.13.1 exactly, not above, otherwise bash_kernel stdout
is split in individual lines.
There is also the kernel `calysto_bash`, but it is old, not maintained, does
not check errors, and has non-coloured output.

Install zsh kernel:
```sh
python -m pip install zsh_jupyter_kernel
python -m zsh_jupyter_kernel.install
```

### Jupyter notebook

 * [The cell magics in IPython](https://nbviewer.org/github/ipython/ipython/blob/1.x/examples/notebooks/Cell%20Magics.ipynb).

Install:
```sh
python -m pip install -U jupyter
```

Execute a notebook and output a new notebook with all outputs included (stdout, stderr):
```sh
jupyter nbconvert --execute --to notebook --output _build/jupyter_execute/toto.ipynb toto.ipynb
```
The same but in-place:
```sh
jupyter nbconvert --execute --to notebook --inplace toto.ipynb
```

### Jupyter book

 * [Jupyter book](https://jupyterbook.org/en/stable/intro.html).
 * [Configuration reference](https://jupyterbook.org/en/stable/customize/config.html). What to put in `_config.yml` file.

Generate HTML and PDF book from Markdown, Notebooks and MyST Markdown (Markdown/Notebooks) files.

Install jupyter book:
```sh
python -m pip install -U jupyter-book
```

Create a new sample book:
```sh
jupyter-book create mybook/
```

Build a book:
```sh
jupyter-book build mybook/
```
This create the `mybook/_build` folder in which you can open the main page at `mybook/_build/html/index.html`.

#### MyST Markdown

 * [jupyter{book}](https://jupyterbook.org/en/stable/intro.html).
   + [MyST syntax cheat sheet](https://jupyterbook.org/en/stable/reference/cheatsheet.html#myst-cheatsheet).
   + [Images and figures](https://jupyterbook.org/en/stable/content/figures.html).
   + [Notebooks written entirely in Markdown](https://jupyterbook.org/en/stable/file-types/myst-notebooks.html).

##### Admonitions

 * [Admonitions](https://jupyterbook.org/en/stable/reference/cheatsheet.html#admonitions).

Write a note:
```{note}
Here is a note
```

Put a note inside the right margin:
```{note}
:class: margin
This note is displayed inside the right margin.
```

##### See also section

Write a *see also* section:
```{seealso}
Another interesting reference.
```

##### Bibliography

Insert the full bibliography from the bibTex file:
```{bibliography}
```

##### Code

Remove code (impossible for the user to see code) and hide output (let user
click on icon to display output):
```{cell-code}
:tags: ["remove-input", "hide-output"]
cat myfile.txt
```

Tag                  | Description
---                  | --
`"remove-cell"`      | Remove input and output.
`"remove-input"`     |
`"remove-output"`    |
`"hide-cell"`        |
`"hide-input"`       |
`"hide-output"`      |
`"full-width"`       |
`"output_scroll"`    |
`"margin"`           |
`"raises-exception"` |

##### Define resource file to download

Two ways to define resource files to download:
 * Here is a [first file](myfolder/myfile.txt) to download.
 * Here is a {download}`second file<myfolder2/myfile2.txt>` to download.

Note: to generate the link, the files must exist.

### jupytext

 * [jupytext - Using at the Command Line](https://jupytext.readthedocs.io/en/latest/using-cli.html).

Install:
```sh
python -m pip install jupytext
```

## R Shiny

## R Markdown

 * [R Markdown](https://rmarkdown.rstudio.com/).
 * [R Markdown Quick Tour](https://rmarkdown.rstudio.com/authoring_quick_tour.html).
 * [R Markdown cheatsheet](https://raw.githubusercontent.com/rstudio/cheatsheets/main/rmarkdown.pdf).
 * [R Markdown: The Definitive Guide](https://bookdown.org/yihui/rmarkdown/).
 * [R Markdown Cookbook](https://bookdown.org/yihui/rmarkdown-cookbook/).
 * [blogdown: Creating Websites with R Markdown](https://bookdown.org/yihui/blogdown/).

`bookdown`, an R package for writing books and articles using R Markdown:
 * [Bookdown](https://bookdown.org/).
 * [bookdown: Authoring Books and Technical Documents with R Markdown](https://bookdown.org/yihui/bookdown/).

Render:
```r
rmarkdown::render("input.Rmd")
```

To cancel totally display of R code:
```{r, echo=FALSE}
my_code...
```

To hide R code but let a button/option to show it:
```{r class.source = 'fold-hide'}
my_code...
```

## Livebook

 * [Livebook](https://livebook.dev/).

Reactive notebook.

## Marimo

 * [Marimo](https://marimo.io/).

Reactive notebook for Python.

## Pluto

Reactive notebook for Julia.

From julia interactive mode, input:
```julia
import Pluto
Pluto.run()
```

## Reactor

 * [Reactor](http://herbsusmann.com/reactor/).

Reactive notebook for R.

## Observable 

 * [Observable](https://observablehq.com/).

An online, non-free, Javascript reactive notebook.
## SPHINX

 * [Getting Started](https://www.sphinx-doc.org/en/master/usage/quickstart.html)
 * [A “How to” Guide for Sphinx + ReadTheDocs](https://sphinx-rtd-tutorial.readthedocs.io/en/latest/index.html):
   + [Writing docstrings](https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html).
   + [sphinx.ext.napoleon – Support for NumPy and Google style docstrings](https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html).
 * [sphinx-pyproject](https://sphinx-pyproject.readthedocs.io/en/latest/). To include Sphinx config inside `pyproject.toml`.

For Python, set `PYTHONPATH` to contain the folder of sources for which to build documentation.
Create a `source` folder where to put two files.

First file is the home page `index.rst`:
```rst
.. parsftp documentation master file, created by
   sphinx-quickstart on Thu Mar 28 15:26:29 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to parsftp's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: parsftp
   :members:
   :imported-members:
..   :show-inheritance:
..   :undoc-members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```

Second file is `conf.py`, Sphinx configuration:
```python
project = 'parsftp'
copyright = '2024, CEA'
author = 'Pierrick ROGER'
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.napoleon', 'sphinx_rtd_theme']
templates_path = ['_templates']
exclude_patterns = []
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
```

Then run sphinx:
```sh
sphinx-build -M html source build
```
Web pages are in `build/html`.

## Python

 * [Common Docstring Formats in Python](https://stackabuse.com/common-docstring-formats-in-python/).

Several formats/styles exist for documenting Python code:
 * Google docstrings.
 * reStructured Text.
 * NumPy/SciPy docstrings.
 * Epytext.

Example of documenting a module using NumPy/SciPy style:
```python
"""My module description."""
import someothermodule

def MyClass:
    """This is my class.

    This is the long description of my class.

    Attributes
    ----------
    a : int
        Some attribute.
    b : int
        Some other attribute.

    Methods
    -------
    a_member_function(x, y)
        Do something.
    """

    def __init__(self, a, b):
        """Initialize object.

        Parameters
        ----------
        a : int
            Some parameter.
        b : int
            Some other parameter.
        """

        self.a = a
        self.b = b

    def a_member_function(self, x, y):
        """Do something.

        Describe better what this method does.

        Parameters
        ----------
        x : float
            First number.
        y : float
            Second number.

        Returns
        -------
        float
            The sum of the two numbers passed in parameters.

        Raises
        ------
        TypeError
            If you pass wrong types for x or y.
        """

        return x + y

def my_global_function():
    """Brief title.

    Long description.
    """
    pass
```
