# tabix

Generate index (.tbi file) for a BED file:
```sh
tabix my_file.bed.gz
