# Fastaq
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

FASTA and FASTQ file manipulation tools.

Install:
```sh
apt install fastaq # Debian/Ubuntu
```
