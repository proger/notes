# VCFtools

 * [VCFtools](https://github.com/vcftools/vcftools).
 * [Variant identification and analysis](https://www.ebi.ac.uk/training/online/courses/human-genetic-variation-introduction/variant-identification-and-analysis/). What is variant calling?

Tools for Variant Call Format files.

 * Stalled in 2020.
 * No tests. 
