# seqtk
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Fast and lightweight tool for processing sequences in the FASTA or FASTQ format.

Install:
```sh
apt install seqtk # Debian/Ubuntu
```

Get help:
```sh
seqtk
```

Base/quality summary:
```sh
seqtk fqchk myfile.fastq
```
Attention: it does not check the validity of the FASTQ file. Errors in the file format are silently ignored.
