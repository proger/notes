# bcftools

Generate index (.tbi file) for a VCF file:
```sh
bcftools index -t my_file.vcf.gz
```
