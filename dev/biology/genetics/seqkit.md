# seqkit
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Cross-platform and ultrafast toolkit for FASTA/Q file manipulation.

Install:
```sh
apt install seqkit # Debian/Ubuntu
```

Get help:
```sh
seqkit -h
```
