# FastQC
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Quality control for high throughput sequence data.

 * [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/).

Install:
```sh
apt install fastqc # Debian/Ubuntu
```
