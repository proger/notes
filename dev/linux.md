# Linux
<!-- vimvars: b:markdown_embedded_syntax={'sh':'bash'} -->

## Version

See `/etc/os-release` and `/usr/lib/os-release`.

## Login

### agetty

Display current issue file:
```sh
agetty --show-issue
```

Display an issue file:
```sh
agetty --show-issue -f myfile
```

### /etc/issue

Prelogin message. See `man issue`.

It is parsed by agetty.

Display current issue file:
```sh
agetty --show-issue
```

To replace the current `/etc/issue` file with the output of `linux_logo`:
 * Replace all `\` by `\\`.
 * Replace `Linux` by `\s`.
 * Replace OS release (`5.14.10-arch1-1`) by `\r`.
 * Replace OS version (`#1 SMP PREEMPT Thu, 07 Oct 2021 20:00:23 +0000`) by `\v`.
 * Replace machine name (`spirou`) by `\n`.

### linux_logo

Get a Linux logo in ASCII art.

Install:
```sh
yay -S linux_logo # Archlinux
```

Print a Linux logo with system information:
```sh
linux_logo
```

Print without coloring:
```sh
linux_logo -a
```

List available logos:
```sh
linux_logo -L list
```

Print ArchLinux logo:
```sh
linux_logo -L archlinux_logo
```

### /etc/motd

Message of the day. See `man motd`.

If not present, create it in `/etc/motd` with reading flag for all.

### screenfetch

Install on ArchLinux:
```sh
pacman -S screenfetch
```

Display Linux logo and system information:
```sh
screenfetch
```
