# zenity

Opens a dialog box and possibly get user input.
```bash
zenity -question -title="Query" -text="Would you like to run the script?"
```
