# dialog

Install:
```sh
pacman -S dialog
```

Create a configuration file:
```sh
dialog --create-rc ~/.dialogrc
```
