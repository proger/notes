# Network
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','bash':'sh'} -->

 * [O'Reilly - The Networking CD Bookshelf version 2.0](https://docstore.mik.ua/orelly/networking_2ndEd/).

## hostname

Install on ArchLinux:
```sh
pacman -S inetutils
```

## IP addresses

 * [Reserved IP addresses](https://en.wikipedia.org/wiki/Reserved_IP_addresses).

192.168.0.0/16 	192.168.0.0–192.168.255.255 	65536 	Private network 	Used for local communications within a private network.

IANA-reserved private IPv4 network ranges:

Blocks                             | Start       | End             | No. of addresses
---                                |         --- |             --- | ---
24-bit Block (/8 prefix, 1 x A)    |   10.0.0.0  |  10.255.255.255 | 16,777,216
20-bit Block (/12 prefix, 16 x B)  | 172.16.0.0  |  172.31.255.255 | 1,048,576
16-bit Block (/16 prefix, 256 x C) | 192.168.0.0 | 192.168.255.255 | 65,536

## Ports

 * [List of TCP and UDP port numbers](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers).

## FTP

Using `curl` to send a file:
```sh
curl -T myfile.txt "ftp://my.site/" --user "anonymous:my.email@address"
```

Connect to an FTP site:
```sh
ftp ftpperso.free.fr
```

Anonymous login (which `ftp` command?):
```sh
ftp -a ...
```

## NTP (Network Time Protocol)

To add new servers, edit /etc/ntp.conf, and lines like this one :
server my.ntp.server.org

To install ntp
Install ntp and ntp-date package.
Under Debian:
```sh
apt-get install ntp
apt-get install ntpdate
```

Forcing setting of time with ntpdate
We must stop the daemon and run 3 times ntpdate (or until offset is low enough)
```sh
/etc/init.d/ntp stop
ntpdate -u <myntpserver>
ntpdate -u <myntpserver>
ntpdate -u <myntpserver>
/etc/init.d/ntp start
```

### timesyncd

 * [systemd-timesyncd](https://wiki.archlinux.fr/Systemd-timesyncd).
 * [NTP configuration on Debian](https://wiki.debian.org/NTP).

On Ubuntu, timesyncd is used by default. To configure it, see `/etc/systemd/timesyncd.conf`.
To check if timesyncd is on, run:
```sh
timedatectl
```
To set it on:
```sh
sudo timedatectl set-ntp on
```

The configuration is in `/etc/systemd/timesyncd.conf`.

To restart timesyncd:
```sh
systemctl restart systemd-timesyncd
```

Check status:
```sh
systemctl status systemd-timesyncd
```

Enable and start:
```sh
systemctl enable --now systemd-timesyncd
```

Check configuration:
```sh
timedatectl show-timesync --all
```

## NFS

 * [NGS](https://wiki.archlinux.org/title/NFS).
 * [macOS X Mount NFS Share / Set an NFS Client](https://www.cyberciti.biz/faq/apple-mac-osx-nfs-mount-command-tutorial/).

Install the package on both server (NFS daemon) and client (`/sbin/mount.nfs`):
```sh
pacman -S nfs-utils # Archlinux
```

To export a disk with NFS, edit `/etc/exports` file:
```
/mnt/zomeu/mediatheque 192.168.1.1/24(ro,sync,no_subtree_check,no_root_squash)
```

List exported NFS:
```sh
showmount -e my.server
```

NFS service: `nfs-server` (on Archlinux).
NFS error `rpc.nfsd[1028]: rpc.nfsd: unable to bind AF_INET TCP socket: errno 99`: => restart `nfs-server`.

From the client, list exported NFS drives:
```sh
showmount -e my.server
```

## SSHFS

Install:
```sh
pacman -S sshfs
```

Mount a folder:
```sh
sshfs "my.server:/my/remote/folder" "/my/local/folder"
```

## CNAME (Canonical Name)

 * [CNAME record](https://en.wikipedia.org/wiki/CNAME_record).


## FQDN (Fully Qualified Domain Name)

 * [Fully qualified domain name](https://en.wikipedia.org/wiki/Fully_qualified_domain_name).

## CORS

 * [CORS errors](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS/Errors).

Requirement:
 * The domain name, must not be `localhost`. Use a private IP address or a Fully
   Qualified Domain Name (FQDN).

Error example:
```
Cross-Origin Request Blocked: The Same Origin Policy disallows reading the
remote resource at http://fromag/data/gencode.v19.annotation.bed.gz.tbi.
(Reason: CORS request did not succeed). Status code: (null).
```

## Avahi

 * [Avahi](https://wiki.archlinux.org/index.php/Avahi#Hostname_resolution).

Automatic network service discovery and configuration (printers, files, ...).

Before enabling daemon you disable `systemd-resolved`:
```sh
systemctl disable systemd-resolved
systemctl enable --now avahi-daemon
```

## Cloud

 * [Three Alternatives to Ubuntu One Cloud Service](https://www.linux.com/learn/three-alternatives-ubuntu-one-cloud-service).
 * [ownClound providers](https://owncloud.org/providers/).

## Web services

 * [Understanding SOAP and REST Basics And Differences](http://blog.smartbear.com/apis/understanding-soap-and-rest-basics/).
 * [SOAP Request with curl](http://dasunhegoda.com/make-soap-request-command-line-curl/596/).
 * [SOAPClient](http://www.soapclient.com/soapmsg.html). Interface for building a SOAP XML request message skeleton.

## Phishing & spamming

 * For SPAM: <is-spam@labs.sophos.com>.
 * For phishing: <spearphish@labs.sophos.com>.

## Flash

 * [Chapter 2: Reading Data from Flash Sites](https://www.propublica.org/nerds/item/reading-flash-data).

## Monitoring traffic

### lsof

List application listening to ports:
```sh
lsof -i
lsof -i TCP -P # Filter on TCP, and display port
```

List application listening to a specific port:
```sh
lsof -i :23456
```

Filter on TCP protocol:
```sh
lsof -i tcp:23456
```

### nettop

 * [Watch Network Traffic in Mac OS X via Command Line with nettop](http://osxdaily.com/2013/06/07/watch-network-traffic-mac-os-x-nettop/).

On macOS, you can use the Activity Monitor or the command line tool `nettop`.

Use arrow keys to navigate.

Command | Description
------- | -------------------------
`p`     | Display a list of processes for which you can enable or disable monitoring.
`q`     | Quit.

## VPNs

 * TSL (Transport Layer Security).
 * SSL (Secure Sockets Layer). --> not available on macOS (when looking in `System Preferences -> Network Configuration`).
 * L2TP (Layer 2 Tunneling Protocol).
 * PPTP (Point-to-point Tunneling Protocol).
 * IPsec (Internet Protocol Security).

VPN clients:

 * [vpnc](https://www.unix-ag.uni-kl.de/~massar/vpnc/).
 * [OpenConnect](http://www.infradead.org/openconnect/building.html).

## RDPs

 * remmina
 * gnome-remote-desktop

## BitTorrent

### transmission-cli

```sh
apt install transmission-cli
```

```sh
transmission-cli http://releases.ubuntu.com/16.10/ubuntu-16.10-desktop-amd64.iso.torrent -w ~/Downloads
```

## TO CLEAN
	===================== MAC address.txt
	to get the MAC address of a device:
	arp <IP>
	===================== NFS.txt
	
	################################################################################
	# DEBIAN
	
	# Install NFS server On Debian
	apt-get install nfs-kernel-server nfs-common portmap
	
	# Add directory to export
	cat >>/etc/exports <<EOF
	/var/software		132.166.220.254(rw,sync,no_subtree_check,insecure)
	EOF
	# "insecure" option is needed when exporting for a MacOS-X client
	
	# Update server configuration
	exportfs -a
	
	################################################################################
	# NFS CLIENT
	
	# Permission denied, no rights to write or read.
	# Often this is due to mismatch between uid/gid of server and client.
	# ==> change uid and gid on server or on client to match the other.
	
	################################################################################
	# MACOS-X
	
	# To add a permanent NFS mount with the possibility to specify options to the mount command:
	# Disk Utility -> File -> NFS mounts
	===================== SOA.txt
	Service Oriented Architecture
	===================== arp.txt
	# -*- Shell-script -*-
	
	# address resolution display and control
	
	# get MAC address from IP
	
	arp my.address.com
	arp 132.166.22.18
	===================== domain name.txt
	Creation de nom de domaine en .eu
	=================================
	www.eurid.eu
	
	Création et gestion de noms des domaines associés à des IP fixes ou dynamiques
	==============================================================================
	http://www.dyndns.com/
	===================== resolv.conf.txt
	# List of DNS:
	nameserver 132.166.182.9
	
	# List of domains to search
	search intra.cea.fr
	
	CYGWIN
	======
	1) choose & install OpenSSH package
	2) create required users
	3) for each user, run ssh-user-config and choose the keys to create
	4) run ssh-host-config :
		answer "no" to question about privelege separation
		answer "yes" to question about sshd
		CYGWIN=ntsec (this is default offered by the script)
	5) chown system.system /etc/ssh*
	6) run net start sshd. If it fails, run sshd manually : /usr/sbin/sshd
	
	If you add other users later, you will have to update manually the passwd and group files :
	mkpasswd -l >/etc/passwd
	mkgroup -l >/etc/group
	
	X forwarding
	============
	For authorizing X forwarding, set the following value inside /etc/ssh/sshd_config of the both computers :
	X11Forwarding yes
	
	Warning: No xauth data; using fake authentication data for X11 forwarding.
	==========================================================================
	In fact this is xauth which is complaining. If you run 'xauth list' you will get :
	xauth:  error in locking authority file /home/pierrick/.Xauthority
	
	Here I was logged in as eatoni using 'su - eatoni' from pierrick account. XAUTHORITY variable was set to /home/pierrick/.Xauthority.
	In /home/eatoni/.bash_profile I added :
	XAUTHORITY=$HOME/.Xauthority
	
	then exit and relogged.
	I erased existing /home/eatoni/.Xauthority file, and then ran :
	xauth generate :0.0 .
	
	user environment
	================
	create file
	~/.ssh/environment
	
	################################################################################
	# ERRORS
	
	# After changing RSA/DSA key, you get the following error message when trying to connect with ssh onto a machine where you've put your public key:
	# "Agent admitted failure to sign using the key."
	# SOLUTION: on the client, run the following command:
	ssh-add
	===================== telnet-windows.txt
	Install telnet:
	pkgmgr /iu:”TelnetServer”
	OR
	Programs and Features, click Turn Windows features on or off.
	Windows Features list, select Telnet Server, and then click OK.
	
	Open services panel. If Telnet isn't there, restart computer.
	Start Telnet service.
	
	Add users to TelnetClients group.
	Open Control Panel -> Administrative Tools -> Computer Management -> System Tools -> Local Users and Groups -> Telnet Clients
	===================== wakeonlan.txt
	# -*- mode:shell-scripts -*-
	
	# 1) configurer le routeur pour laisser passer les paquest WakeOnLan et les broadcaster sur le réseau local.
	# Pour le freebox: la mettre en mode routeur, et cocher la case WOL.
	
	# 2) utiliser un site web qui envoie un paquet WOL. Par exemple:
	# http://www.wakeonlan.me/
	# renseigner l'IP et l'adresse MAC de l'ordinateur à réveiller et envoyer le paquet.
	
	# En local on peut tester la fonctionnalité avec l'outil UNIX/Linux wakeonlan.
	
	# wake machine on the local network
	wakeonlan <MAC ADDRESS>
	
	# en PHP, possiblité de réveiller une machine aussi à travers le web avec ===================== webdav apps.txt
	iDisk: can't read UTF-8 files on iPhone
	Dropbox: can read UTF-8 files on iPhone. App exists for Android too.
	SugarSync: can't read UTF-8 files on iPhone. App exists for Android too.
	===================== wget.txt
	# -*- mode:shell-script -*-
	
	# For getting all files from a directory :
	wget -r -l 1 -nd http://www.blablabla.com/toto
	# -r : recursive
	# -l 1 : depth set to one
	# -nd : no directories hierarchy, all files are put in the current directory
	# -A "*.txt,*.gif" : download only the txt and gif files
	# -R "*.zip" : do not download zip files
	# -nH : Disable generation of host-prefixed directories.
	# --cut-dirs=number : Ignore number directory components.
	
	# -O <output_file>
	
	# To use POST method in order to send data:
	wger myurl --post-data='myvar1=12&myvar2=3&myvar3=%2B1'
	===================== wifi.txt
	to share internet from a Mac through wifi with Windows computer
	===============================================================
	enable Airport on the Mac.
	set up standard 128-bit WEP-protected WiFi internet sharing on Mac
	on Windows go to Wireless Network Connection Properties/Wireless Networks
	add new WiFi network
	enter network name matching your Mac access point
	set Network Authentication: Shared
	set Data Encryption: WEP
	uncheck "The key is provided automatically" option and enter (and repeat) your 13 character password under "Network key"
	uncheck "Enable IEEE 802.1x authentication ..." under Authentication tab
	===================== mail/pop3.txt
	testing a pop3 mail server
	==========================
	telnet eatoni.com 110
	USER pierrick
	PASS ....
	STAT
	LIST
	RETR 1
	===================== server hosting/Kimsufi.txt
	http://www.kimsufi.com/
	hébergeur de serveurs
	===================== web/GoogleMap.txt
	KVM files
	=========
	Files used to describe a map for Google Map and Google Earth.
	Objects like PlaceMark or other forms can be created in order to be drawn on the map.
	===================== web/Mime types.txt
	http://www.iana.org/assignments/media-types/
	
	format: <mime>/<type>
	===================== web/RGraph.txt
	http://www.rgraph.net/
	RGraph: HTML5 canvas graph library based on the HTML5 canvas tag
	
	++ plutôt joli
	
	wide variety of graph types
	===========================
	bar chart, bi-polar chart (also known as an age frequency chart), donut chart, funnel chart, gantt chart, horizontal bar chart, LED display, line graph, meter, odometer, pie chart, progress bar, rose chart, scatter graph and traditional radar chart.
	
	===================== web/XWiki.txt
	http://www.xwiki.org/
	
	Development plateform for developing a wiki.
	===================== web/flash.txt
	.fla files are saved files from Flash Adobe software. They have to transformed into .swf by the software in order to be embedded into an HTML page.
	
	to embed a flash .swf into an HTML page
	=======================================
	<object width="550" height="400">
	<param name="movie" value="somefilename.swf">
	<embed src="somefilename.swf" width="550" height="400">
	</embed>
	</object>
	
	--> There should be a few more attributes added:
	_ classid is an attribute to the <object> tag. It tells Internet Explorer to load the ActiveX plug-in if it is not installed
	_ pluginspage is an attribute to the <embed> tag. It displays a link to the Shockwave download page if Netscape does not have it 
	FLASH DISPLAY IN HTML PAGE
	==========================
	<script type="text/javascript"> 
	AC_FL_RunContent('codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0', 'wmode', 'transparent', 'width', '475', 'height', '300', 'src', 'projects-slideshow', 'quality', 'high', 'pluginspage', 'http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash', 'movie', 'projects-slideshow')</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="$width" height="$height"> 
	<param name="movie" value="$file"/> 
	<param name="quality" value="high"/> 
	<param name="wmode" value="transparent"> 
	<embed src="$file" quality="high" wmode=transparent pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="$width" height="$height"></embed> 
	</object></noscript> 
	
	how to prevent javascript menu from getting hidden under flash movies
	=====================================================================
	You must do three things to solve this problem :
	1) in the object tag add the following parameter 
	<PARAM NAME=wmode VALUE=transparent>
	
	2) in the embed tag add the following attribute
	<EMBED src="jet.swf" wmode=transparent ....
	
	3) add the same name-value pair to the following script
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0' ,'wmode','transparent',...rest of your name-value pairs go here ); //end AC code
	</script>
	===================== web/well designed sites.txt
	http://www.squaretrousseau.com/
	===================== mail/postfix/mailq.txt
	# vi: ft=sh
	
	# listing queued messages in postfix
	mailq
	===================== mail/postfix/postfix.txt
	postfix : MESSAGE SIZE ERROR
	============================
	postdrop: warning: uid=502: File too large
	sendmail: fatal: pierrick@eatoni.com(502): message file too big
	
	--> change message_size_limit in /etc/postfix/main.cf
	===================== mail/postfix/postsuper.txt
	
	# To delete a message
	postsuper -d queue_id
	postsuper -d -          # read queue ids from stdin

## NetworkManager

nmcli:
```sh
nmcli radio # See status of WiFI
nmcli radio wifi on # Power on WiFi
iwconfig # See the network interfaces
nmcli d disconnect <WifiInterface>
nmcli d connect <WifiInterface>
nmcli c down <SavedWiFiConn>
nmcli c up <SavedWiFiConn>

nmcli c # List saved WiFi connections

nmcli d wifi list # List available WiFi hotspots
```

Connect on SSID with WEP password:
```sh
nmcli d wifi connect MY_SSID password 'MY_PWD' wep-key-type phrase
```

Curses interface: `nmtui`

## WiFi

See:
 * iw
 * iwctl
 * iwd
 * iwgetid

Get current WiFi (card and SSID):
```sh
iwgetid
```

### Configure EAP-PEAP SSID connection

Place certificate file inside `/etc/ssl/certs`.

Create file `/var/lib/iwd/myssid.8021x`:
```
[Security]
EAP-Method=PEAP
EAP-Identity=anonymous@realm.edu
EAP-PEAP-CACert=/etc/ssl/certs/mycert.crt
EAP-PEAP-ServerDomainMask=radius.realm.edu
EAP-PEAP-Phase2-Method=MSCHAPV2
EAP-PEAP-Phase2-Identity=johndoe@realm.edu
EAP-PEAP-Phase2-Password=hunter2

[Settings]
AutoConnect=true
```

## Sockets

### C++

Example from [The Linux Programming Interface](https://www.man7.org/tlpi/):
 * [Server code](https://man7.org/tlpi/code/online/dist/sockets/ud_ucase_sv.c.html).
 * [Client code](https://man7.org/tlpi/code/online/dist/sockets/ud_ucase_cl.c.html).
 * [Header file](https://man7.org/tlpi/code/online/dist/sockets/ud_ucase.h.html).
 * [TLPI Header](https://man7.org/tlpi/code/online/dist/lib/tlpi_hdr.h.html).

`recv()`
`recvfrom()`

### Bash

Use OpenBSD netcat (nc), to open UNIX domain sockets:
```sh
nc -U -u -s "$socket_file" "$socket_file"
```
See full `examples/sockets/unix_domain_sockets.sh`.
