# Audio
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

## CD rip

 * cdrip
 * ripperX
 * asunder --> Hangs sometimes. May even not respond to KILL signal.
 * Sound Juicer

### abcde

To get MusicBrainz info, needs `abcde-musicbrainz-tool`, and
install perl modules for MusicBrainz:
```sh
yay -Ss abcde perl-musicbrainz-discid
cpanm --force WebService::MusicBrainz
```
Then set `CDDBMETHOD=musicbrainz` in `~/.abcde.conf`.

Was not able to configure CDDB (no error output, nothing found).

## Microphone

 * [No microphone input](https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture/Troubleshooting#Microphone).

To test microphone:
```sh
arecord --duration=5 --format=dat test-mic.wav
aplay test-mic.wav
```

With pulseaudio, check that the microphone accessible:
 * Run `pacmd list-sources` and search for `alsa_input`.
 * If it is not accessible, run `pacmixer`, and check the card:
  + Press F3 and select the card. For "Built-in Audio", make sure that "Analog Stereo Duplex" is selected and not only Input or Output mode.

Testing microphone in browser:
 * <https://mozilla.github.io/webrtc-landing/gum_test.html>
 * <https://rendez-vous.renater.fr/home/test_browser>

## beep

 * [Beep](https://wiki.archlinux.org/title/PC_speaker#Beep).

## ALSA

ALSA: linux kernel level subsystem.

Install:
```sh
pacman -S alsa-utils
```

### arecord

Record from microphone with CD quality:
```sh
arecord -f cd test.wav
```

### aplay

ALSA player.

Play a wav file with ALSA layer:
```sh
aplay myfile.wav
```

List available audio devices:
```sh
aplay -l
```

### alsactl

Configure ALSA sound kernel layer:
```bash
alsactl init
```

Restore default:
```bash
alsactl -F restore
```

### amixer

Command line ALSA tool to manage sound card.

Get a list of available controls:
```sh
amixer scontrols
```

Get a list of available controls with their set values:
```sh
amixer scontents
```

Set microphone boost level (values: 0, 1, 2 and 3):
```sh
amixer sset "Internal Mic Boost" 0
```

Set microphone LED indicator (values: On, Off, Follow Capture, Follow Mute):
```sh
amixer sset "Mic Mute-LED Mode" Off
```

### alsamixer

Ncurses console ALSA tool to manage sound card configuration.
Move with left/right to select.
Press up/down to modify.

Disabling "Auto-Mute Mode" is possible.

To enable microphone, run `alsamixer`, and then:
 * In 'Playback' view (F3), set microphone to "Follow Capture".
 * In 'Capture' view (F4), increase volume of CAPTURE and "Internal Mic".

## PipeWire

 * [PipeWire](https://wiki.archlinux.org/title/PipeWire).

A compatible replacement for PulseAudio.

```sh
pactl info
```

## PulseAudio

PulseAudio: sound server program, runs on top of kernel level subsystems like ALSA or OSS (legacy).

Install on Archlinux:
```sh
pacman -S pulseaudio
```

Check pulseaudio server with systemctl:
```sh
systemctl --user status pulseaudio
```

Test if server is running:
```sh
pulseaudio --check && echo RUNNING
```

Kill server:
```sh
pulseaudio -k
```

Start as daemon:
```sh
pulseaudio -D
```

### apulse

PulseAudio emulator for ALSA.

**Skype** for Linux needs `pulseaudio` in order to recognize microphone, so
with alsa configuration, you need first to install `apulse`, a PulseAudio
emulator for ALSA and then run:
```sh
apulse skypeforlinux
```

### pacat

Play a wav file:
```sh
pacat <myfile.wav
```

### parecord

Record audio on microphone:
```sh
parecord myfile.wav
```
Stop recording with CTRL-C.

Record audio on microphone:
```sh
parecord --channels=1 -d <input_name> myfile.wav
```
See pacmd on how to get input name.

### pactl

List audio devices:
```sh
pactl list sinks
```

Get default (current?) sink (symbolic name):
```sh
pactl get-default-sink
```

### pacmd

List input sources:
```sh
pacmd list-sources | grep alsa_input
```

List output sources:
```sh
pacmd list-sources | grep alsa_output
```

List sources and their parameters:
```sh
pacmd list-sources
```

### pacmixer

Console mode mixer for pulse audio.
Controls inputs, outputs, and playback (selection of each audio device for each application).

### pulsemixer

Console mixer for pulse
Controls inputs, outputs, and playback (selection of each audio device for each application).

Key   | Description
---   | ---
ENTER | Context menu. Change affectation (device output/input) of application.


### pavucontrol

Tool to select and control audio input and output devices.
Works for selecting HDMI as audio output.
Select also devices for each app.

## Conversion

### fre:ac

 * [fre:ac](https://freac.org/).

Ripper and convert.

### sox

Convert between many file formats:
```sh
sox myfile.aif myfile.mp3
```

Cut some part of an audio file:
```sh
sox myfile.aif myfile.mp3 trim 0 11:00
```
Here the file will cut from start to eleventh minute. Format is `trim start duration`.

To cut a sequence:
```sh
sox <intput file> <output file> trim start [length]
```
format of start and length is hh:mm:ss.frac
or the number of samples ending with 's': 8000s

verbose:
	-S verbose output, with progress

Increase/decrease volume:
```sh
sox -v <factor> <intput file> <output file>
```
`factor` is a float.

### afconvert

`afconvert` is a MacOS-X program that converts between CAF and AIF file formats
To convert a `.caf` file into an `.aif` one:
```bash
afconvert  -f AIFF -d I8 myfile.caf myfile.aif
```

## espeak

Text to speech.

In French:
```sh
espeak -v fr "Bonjour"
```

Record in a file:
```sh
espeak -v fr "Bonjour" -w myfile.wav
```

## Tag edition

See exiftool.

### puddletag

Install:
```sh
yay -S puddletag
```

### kid3-cli

Audio file tag editor.

Add covert art to ogg files:
```sh
kid3-cli -c "select all" -c 'set picture:"/path/to/Cover.jpg" "back cover"' "/path/to/*.ogg"
```

## Players

 * amarok
 * clementine
 * audacious
 * rhythmbox

### kew

Display album covers inside `konsole` or `kitty` terminal.

Run first by indicating the library folder to scan:
```sh
kew /my/library
```

To play only one folder:
```sh
kew dir "My Folder"
```

### termusic

Display album covers inside `kitty` terminal.

### Musikcube

 * [Musikcube](https://github.com/clangen/musikcube/wiki/installing).

Console player.

### moc

Console music player.
Simple navigation inside directory tree.

Install:
```sh
apt install moc
```

Usage:
```sh
mocp Music
```

### cmus.md

Console music player.

Install:
```sh
apt install cmus
```

## MP3

### mpg123

Command line mp3 player.

```sh
mpg123 myfile.mp3
```

## normalize

Adjust WAV file volume.

Install:
```sh
yay -S normalize 
```

## normalize-audio

Adjust WAV file volume.

Install:
```sh
apt-get install -y normalize-audio
```

## Ogg / Vorbis

Install:
```sh
pacman -S vorbis-tools
```

### oggenc

```sh
oggenc -o myfile.ogg myfile.wav
```

### vorbiscomment

List tags of ogg file:
```sh
vorbiscomment myfile.ogg
```

## Music

 * [Instruments, Samples, and Sound Effects](https://wiki.linuxaudio.org/wiki/instruments).
 * [Synth sounds](https://www.noiiz.com/sounds/instruments/67).
 * [open-samples](https://github.com/pumodi/open-samples).
