# File archiving
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

## arepack

Install:
```sh
apt install atool # Debian
yay -S atool # Archlinux
```

Convert all zip in current folder into 7z format (zip files are not deleted):
```sh
arepack -e -F 7z *.zip
```

## 7z

Install:
```sh
apt install p7zip-full # Ubuntu
```

Uncompress archive:
```sh
7z x myarchive.7z
```

Extract files without full path:
```sh
7z e myarchive.7z
```

List files from an archive:
```sh
7z l myarchive.7z
```

Create an archive or add files to an existing archive:
```sh
7z a myarchive.7z myfile1 myfile2 ...
```

Delete files from an archive:
```sh
7z d myarchive.7z '*/myfile-*.txt'
```

Rename a file or folder inside an archive:
```sh
7z rn myarchive.7z mycurrentfoldername mynewfoldername
```

## TNEF (winmail.dat files)

MSOutlook attachment format.

Install `tnef`:
```sh
sudo apt install tnef
```

Open archive:
```sh
tnef winmail.dat
```

## Zip

To compress files:
```bash
zip my_zip_file file1 file2 ...
```

To compress a folder:
```bash
zip -r my_dir my_dir # recursive
zip -j foo foo/* # to leave off the path
```

Use highest compression rate:
```sh
zip -9 ...
```

# unzip

To uncompress:
```bash
unzip myfile.zip
```

Unzip to stdout:
```sh
unzip -p myfile.zip
```

List files contained inside a zip:
```bash
unzip -l myfile.zip
```

Unzip quietly:
```bash
unzip -q myfile.zip
```

### Cracking zip password

Install
```sh
apt install fcrackzip
```

Run:
```sh
fcrackzip -v myarchive.zip
```

## gzip / gunzip

Unarchive a file:
```sh
gunzip myfile.txt.gz
```

Write content of a gzipped file onto stdout:
```sh
gzunip -c myfile.txt.gz
```

## pigz

 * [pigz](https://zlib.net/pigz/).

## tar
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

To tar files without the parent directory, first change to that dir with `-C` option:
```sh
tar -cjf my_pkg.tbz -C my_dir .
```

Delete a file from inside a tar:
```sh
tar -f myfile --delete my/file.txt
```

Incremental backup (GNU only):
```sh
tar -cf /backup/my_dir.0.tar -g /backup/my_dir.snar /my/dir
tar -cf /backup/my_dir.1.tar -g /backup/my_dir.snar /my/dir
tar -cf /backup/my_dir.2.tar -g /backup/my_dir.snar /my/dir
```
Best practice is to do one full dump each week, and then a level 1 backup (incremental backup from the full dump) each day.
Note the option `--no-check-device`, which tells `tar` to do not rely on device numbers when preparing a list of changed files for an incremental dump. Device numbers are not reliable with NFS.
To restore an incremental backup:
```sh
tar -xf /backup/my_dir.0.tar -g /dev/null
tar -xf /backup/my_dir.1.tar -g /dev/null
tar -xf /backup/my_dir.2.tar -g /dev/null
```

Use xz:
```sh
tar -cJf mydir.tar.xz mydir
```

Extract in another directory:
```sh
tar -xzf my.file.tar.gz -C my/dest/dir
```

List content of an archive:
```sh
tar -tzf myfile.tar.gz
```

Take list of files to tar from a file:
```sh
tar -T my_list_of_files.txt -czf output.tgz
```

Take a list of files from stdin:
```sh
tar -T - -czf output.tgz
```

## xz / unxz

Compress a file:
```sh
xz myfile.txt
```

Use as much threads as there are CPUs:
```sh
xz -T 0 myfile.txt
```
This will not work if memory limit is too low (default ~4GiB).
Use `-M` to increase memory limit instead:
```sh
xz -M 8GiB myfile.txt
```

Use maximum compression level:
```sh
xz -9 myfile.txt
```

Decompress a file:
```sh
unxz myfile.txt.xz
```

Use as much threads as there are CPUs:
```sh
unxz -T 0 myfile.txt.xz
```
