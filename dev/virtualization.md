# Virtualization
<!-- vimvars: b:markdown_embedded_syntax={'dosbatch':'','sh':'','dockerfile':'','ruby':''} -->

## SEGA

### BlastEm

Sega Mega Drive emulator on Linux.

### dgen

Sega Genesis/Mega Drive emulator.

 * [DGen](https://segaretro.org/DGen).

Install:
```sh
yay -S dgen-sdl # Archlinux
```

Run:
```sh
dgen myrom.md
```

Run fullscreen:
```sh
dgen -f ...
```

Keys | Command
---- | --------------------------------
0-9  | Select save slot.
F2   | Save in selected slot.
F3   | Load selected slot.
F7   | Previous slot.
F8   | Next slot.
ESC  | Exit emulator.

### kega-fusion

Sega SMS/GG, Genesis/Megadrive emalator.

Install on Archlinux:
```sh
yay -S 
```

Fail with ALSA when running:
```
ALSA lib dlmisc.c:337:(snd_dlobj_cache_get0) Cannot open shared library libasound_module_pcm_pulse.so (/usr/lib32/alsa-lib/libasound_module_pcm_pulse.so: cannot open shared object file: No such file or directory)
```

### Redream

Dreamcast emulator. Works fine, place cue/gdi and bin files into a folder, and
select the folder inside the emulator.

 * [redream](https://redream.io/).

Linux binaries to download from website directly.

## Nintendo

### desmume

Nintendo DS emulator.

### dolphin-emu

First, run it alone to configure the controller:
```sh
dolphin-emu
```

The run it in batch mode (no GUI) with a game:
```sh
dolphin-emu -b mygame.iso
```

### fceux

Fast and ultra-compatible Nintento NES/Famicom emulator

```sh
fceux mygame.nes
```

### mgba

Game Boy Advance emulator.

Use `mgba-qt` to get access to configuration menu for configuring gamepads.

### mupen64plus

Nintendo 64 emulator.

Set the following driver to avoid flickering:
```sh
mupen64plus --gfx mupen64plus-video-glide64mk2 mygame.z64
```

Run in fullscreen:
```sh
mupen64plus --fullscreen ...
```

### zsnes

Nintendo SNES emulator.

```sh
zsnes mygame.sfc
```

If there is no sound and got the following messages:
```
ALSA lib dlmisc.c:339:(snd_dlobj_cache_get0) Cannot open shared library libasound_module_pcm_pulse.so (libpulse.so.0: cannot open shared object file: No such file or directory)
```
Then install the 32 bits versions of pulse and alsa plugins:
```sh
pacman -S multilib/lib32-alsa-plugins multilib/lib32-libpulse
```

## Amiga

### fs-uae

 * [FS-UAE](https://fs-uae.net/).
   + [Command line](https://fs-uae.net/command-line).
   + [Configuration files](https://fs-uae.net/configuration-files). See <https://github.com/FrodeSolheim/fs-uae/tree/master/docs/options>.

Log file: `$HOME/Documents/FS-UAE/Cache/Logs/fs-uae.log.txt`.

Install:
```sh
yay -S gamemode fs-uae-git
```
`gamemoded` daemon is started automatically by fs-uae.
Message `WARNING: CPU governor is 'schedutil' - Switch to 'performance' to avoid stuttering`

Commodore Amiga emulator built using the [AROS Project](https://github.com/aros-development-team).
It has a default ROM (Amiga 500), however it is no sufficient to run most games.
Amiga Forever sells Amiga ROMs.

Some ROMs are available at:
 * <http://amigauae.free.fr/kickstart.html> --> OK, trois ROMs.
 * <https://fsck.technology/software/Commodore/Amiga/Kickstart%20ROMs/>, not sure they are good.

Key       | Description
---       | ---
ALT/F12   | Emulator Menu.
Arrows    | Move inside menu.
ENTER     | Select item.
BACKSPACE | Go back to previous menu (i.e.: exit sub-menu).

Run with
```sh
fs-uae Drakkhen_FR_Disk_1.adf
```

Default configuration file is `Default.fs-uae` (which PATH?).
To load a custom configuration file:
```sh
fs-uae config.fs-uae
```

We should be able to configure another ROM within the configuration file, but I
was not able to make it work:
```ini
kickstart_file = kickstart_v2.0_Commodore_A3000.rom
floppy_drive_0 = drakkhen_1.adf
floppy_drive_1 = drakkhen_2.adf
```

To configure up to 4 floppy drives (e.g.: for multi-disks game):
```ini
floppy_drive_0 = Disk1.adf
floppy_drive_1 = Disk2.adf
floppy_drive_2 = Disk3.adf
floppy_drive_3 = Disk4.adf
```

For more than 4 disks, use floppy swapping and F12 menu to swap floppies:
```ini
floppy_drive_0 = Disk1.adf
floppy_image_0 = Disk2.adf
floppy_image_1 = Disk3.adf
floppy_image_2 = Disk4.adf
floppy_image_3 = Disk5.adf
floppy_image_4 = Disk6.adf
```

### vice

A commodore 8-bit emulator.

Installation on Archlinux:
```sh
pacman -S vice
```

 * `xvic`
 * `x64` fast emulation
 * `x64sc` accurate emulation

## Atari

### atari800

Install:
```sh
yay -S atari800 # Archlinux
```

Run cartridge game:
```sh
atari800 -5200 -cart-type 6 -cart Galaxian.BIN
```

Run in full screen mode:
```sh
# ???
```

Key   | Description
----- | -------------------------
0     | Fire
Alt-F | Toggle fullscreen

### hatari

Install on Archlinux:
```sh
pacman -S hatari
```

Press F12 to get menu.

### stella

Atari 2600 VCS emulator.

Install on Archlinux:
```sh
pacman -S stella
```

## Amstrad

### arnold

 * [Arnold emulator for MacOS-X](http://bannister.org/software/arnold.htm).

Dependency error on Archlinux.

Run a disk with arnold:
```sh
arnold -d mydisk.dsk
```

### Caprice32

 * [Amstrad - CPC (Caprice32)](https://docs.libretro.com/library/caprice32/).
 * [Caprice32](https://github.com/ColinPitrat/caprice32).
 * [Manual page](http://htmlpreview.github.io/?https://github.com/ColinPitrat/caprice32/blob/master/doc/man.html)
 * See `/etc/cap32.cfg`.

Emulator for Amstrad CPCs.

Install on Archlinux:
```sh
yay -Ss caprice32-git
```

Launch with a disk:
```sh
cap32 mydisk.dsk
```

Launch from a snapshot:
```sh
cap32 my_snapshot.sna
```

Default Keys:
 * F1 - Show GUI (and pause the emulator)
 * F2 - Toggle fullscreen / windowed mode
 * F3 - Take screenshot
 * F4 - Press tape reader play key
 * F5 - Reset the emulator
 * F6 - Multiface II stop (advanced users only)
 * F7 - Toggle joystick emulation
 * F8 - Toggle FPS display
 * F9 - Toggle speed limitation
 * F10 - Exit the emulator
 * F12 - Toggle debug mode
 * Shift+F1 - Show virtual keyboard
 * Shift+F3 - Take a machine snapshot

### Javacpc

Install:
```sh
yay -S javacpc # Archlinux
```

Exceptions everywhere...

### RetroVirtualMachine

 * [Retro Virtual Machine](https://www.retrovirtualmachine.org/).

Amstrad CPC, ZX Spectrum and SEGA emulator (for Windows).
Can save and load game states (snapshots) as `.sna` files.

On Ubuntu, install from `.deb` file from website, not with snapstore (no sound
issue).

Install on Archlinux:
```sh
yay -S retrovirtualmachine
```

Run:
```sh
RetroVirtualMachine
```

Run game from command line:
```sh
RetroVirtualMachine -b=cpc6128 -i sorcery.dsk -c='RUN"SORCERY\n' -p
```

### xcpc

 * [xcpc](https://www.xcpc-emulator.net/).

Keyboard issue when using Italian keyboard on Ubuntu.

## Duckstation

PlayStation 1 emulator.

 * [DuckStation](https://duckstation.org/).
   + [Command Line Arguments](https://github.com/stenzek/duckstation/wiki/Command-Line-Arguments).

Save states are saved into `~/.local/share/duckstation/savestates`.

## MSX2

`fmsx` is an MSX/MSX2/MSX2+ emulator.

Install:
```sh
yay -S fmsx
```

## MAME

Multiple Arcade Machine Emulator 

 * [MAME dev](https://www.mamedev.org/).
   + [GitHub repos](https://github.com/mamedev/mame).
 * [ROMs / MAME .239 ROMs](https://edgeemu.net/browse-mame-G.htm).
 * [Command-line Index](https://docs.mamedev.org/commandline/commandline-index.html).
 * [Universal Command-line Options](https://docs.mamedev.org/commandline/commandline-all.html).

Install on Ubuntu:
```sh
apt install mame
```

Create ROMs folder:
```sh
mkdir -p $HOME/mame/roms
```

Run with ROM folder:
```sh
mame -rp my/path/to/roms
```

Copy ROMs inside folder and start mame.

Run Galaga (needs first to download game files (but where?)):
```sh
mame galaga
```

## Thomson

### dcom5

Thomson computers

 * [DCMO5](http://dcmo5.free.fr/v11/dcmo5v11fr.html). THOMSON MO5/TO7. Multi-plateform, to compile.

## ScummVM

 * [Official page](https://www.scummvm.org/).
 * [Wikipedia](https://fr.wikipedia.org/wiki/ScummVM).

## DOSBox

 * [DOSBox](http://www.dosbox.com/) is a DOS emulator that runs on different plateforms (Windows, Linux, MacOS, ...).
 * [Special Keys](https://www.dosbox.com/wiki/Special_Keys).
 * [Dosbox.conf](https://www.dosbox.com/wiki/Dosbox.conf).

Run executable:
```sh
dosbox myprg.exe
```
Run inside local folder:
```sh
dosbox .
```

Key       | Description
---       | ---
ALT-ENTER | Full scren.
ALT-PAUSE | Pause emulation
CTRL-F1   | show keymapper config
Ctrl-F10  | Capture/release mouse.
CTRL-F11  | Decrease CPU speed.
CTRL-F12  | Increase CPU speed.

For a tutorial, type `INTRO` on the command line.

Mounting a UNIX dir to C:
```dosbatch
mount c ~/mydir
```

Mounting a CDROM image:
```dosbatch
IMGMOUNT D myfile.iso -t iso
```
See [IMGMOUNT](https://www.dosbox.com/wiki/IMGMOUNT).
Mounting multiple ISO images on the same drive:
```dosbatch
IMGMOUNT D a.iso b.iso c.iso -t iso
```
Then switch to next ISO with CTRL+F4.

Mounting a folder as a CDROM:
```dosbatch
mount d myfolder -t cdrom
```

Generate a configuration file:
```dosbatch
CONFIG -writeconf dosbox.conf
```

Start IPX server to configure network communication:
 * Set `ipx=true` inside `dosbox.conf` file on both machines.
 * Start `dosbox` on first and run `ipxnet startserver 65000`
 * Start `doxbox` on second machine and run `ipxnet connect 192.168.1.100 65000` (use IP of first machine).
 * Start game/program on each machine.

Example for `dosbox.conf`:
```ini
[render]
scaler = normal3x

[cpu]
cycles = fixed 20000

[autoexec]
mount c .
c:
MYPROG.EXE
EXIT
```

## qemu

For installing Windows 2000:
```bash
qemu -win2k-hack ...
```

For installing an OS:
```bash
qemu -cdrom os.iso -boot d
```

Option                               | Description
------------------------------------ | -----------
`-m 512`                             | Memory.
`-hda my_file`                       | Hard drive to use.
`-usb`                               | Enable USB.
`-localtime`                         | Use localtime for clock.
`-soundhw sb16`                      | Sound card.
`-no-acpi`                           | Disable ACPI (Advanced Configuration and Power Interface).
`-kernel-kqemu`                      | On Linux, kqemu is a kernel module that enhances emulation performance..
`-net nic -net user,hostname=bernie` | Use host machine's network card.

Example for installing Windows 2000 on macosx:
```bash
qemu -m 512 -hda bernie.qcow2 -usb -localtime -soundhw sb16 -no-acpi -net nic -net user,hostname=bernie -win2k-hack -cdrom win2000.iso -boot d
```

Example for installing WinXP:
```bash
qemu-img create -f qcow2 vmwinxp.qcow2 8G
qemu -m 512 -hda vmwinxp.qcow2 -usb -localtime -soundhw sb16 -no-acpi -net nic -net user,hostname=vmwinxp -cdrom winxp.iso -boot d
```

### Qemu-KVM

Go into BIOS and enable virtualization modes for CPU.
Shutdown and restart computer.

On Ubuntu and Debian:
```bash
apt-get install qemu-kvm
apt-get install aqemu
```

Check that modules are loaded:
```bash
lsmod | grep kvm
```
If they aren't loaded, run:
```bash
sudo modprob kvm-intel
sudo modprob kvm
```
or restart the computer.

### qemu-img

To convert from one virtual machine format to another:
```bash
qemu-img convert -f vpc -O raw "XP SP2 with IE7.vhd" XPIE7.bin
```

Format | Description
------ | -----------
qcow2  | New qemu format.
vdi	   | VirtualBox format.
vmdk   | VMWare 3 and 4.
vpc    | VirtualPC.

Create a new machine:
```bash
qemu-img create -f qcow2 bernie.qcow2 8G
```

## windows95

 * [windows95](https://github.com/felixrieseberg/windows95).

Windows 95 emulation using Electron.

## wine

 * [Wine](https://wiki.archlinux.org/title/wine).
 * [iTunes12-Wine-Ubuntu.txt](https://gist.github.com/schorschii/a22c17e21ec48f4931e9a2b2ea5a01bb).
 * [Wine Applications Database](https://appdb.winehq.org/).

Install first the multilib repository by uncommenting the `[multilib]` section
in `/etc/pacman.conf`, then:
```sh
pacman -Syu
pacman -S wine # or wine-staging
```

If you see the following error:
```
ntlm_check_version ntlm_auth was not found or is outdated. Make sure that
ntlm_auth >= 3.0.25 is in your path. Usually, you can find it in the winbind
package of your distribution.
```
then, on ArchLinux, install samba:
```sh
pacman -S samba
```

Start Task Manager:
```sh
wine taskmgr
```

Run configurator:
```sh
wine winecfg
```

Run a program:
```sh
wine myprog.exe
```

Set prefix (default is `~/.wine`):
```sh
WINEPREFIX=~/.mywineprefix wine
```

Set architecture (default is `win64`):
```sh
WINEARCH=win32 wine
```

Run in 32 bits with dedicated prefix:
```sh
WINEPREFIX=~/.wine32 WINEARCH=win32 winecfg
WINEPREFIX=~/.wine32 WINEARCH=win32 wine ...
```

To mount a folder as a drive, make a symbolic link inside
`<PREFIX>/dosdevices`:
```sh
ln -s myfolder ~/.wine/dosdevices/f:
```

### regedit

Register editor:
```sh
wine regedit
```

### uninstaller

To uninstall an application, run the *uninstaller*:
```sh
wine uninstaller
```

### Errors

#### Out of adapter memory 

```
0130:err:d3d:resource_init Out of adapter memory.
```

Direct3D needs more video memory.
Run `wine regedit`.
Go to `HKEY_CURRENT_USER` -> `Software` -> `Wine`.
Create a key `Direct3D`.
Inside the key create two string values:
 * `VideoMemorySize`="2048"
 * `DirectDrawRenderer`=""
### winetricks

Install .NET 4.8 (older versions also available):
```sh
winetricks dotnet48
```

Remove wine installation:
```sh
winetricks annihilate
```

Run in 32 bits:
```sh
WINEARCH=win32 WINEPREFIX=~/.wine32 winetricks steam
```

Install MFC40.DLL:
```sh
WINEPREFIX=~/.wine32 WINEARCH=win32 winetricks mfc40
```

Error when installing `MFC42.DLL`:
```sh
WINEPREFIX=~/.wine32 WINEARCH=win32 winetricks dlls mfc42
```
Error:
```
Executing wine /home/pierrick/.cache/winetricks/vcrun6/vc6redistsetup_deu.exe /T:C:\windows\Temp /c
wine: failed to open "/home/pierrick/.cache/winetricks/vcrun6/vc6redistsetup_deu.exe": c0000135
------------------------------------------------------
warning: Note: command wine /home/pierrick/.cache/winetricks/vcrun6/vc6redistsetup_deu.exe /T:C:\windows\Temp /c returned status 53. Aborting.
------------------------------------------------------
```
The file name is written with capitals on disk: `VC6RedistSetup_deu.exe`.
Solution: rename the file:
```sh
mv /home/pierrick/.cache/winetricks/vcrun6/VC6RedistSetup_deu.exe /home/pierrick/.cache/winetricks/vcrun6/vc6redistsetup_deu.exe
```
Then run again:
```sh
WINEPREFIX=~/.wine32 WINEARCH=win32 winetricks dlls mfc42
```

### WineHQ (wine and winetricks)

 * [WineHQ](https://www.winehq.org/).
 * [Ubuntu WineHQ Repository](https://wiki.winehq.org/Ubuntu).

Default prefix is `~/.wine`.

### winefile

Wine File Manager.

### winecfg

Wine Configuration Editor.

## Parallels

### Installing Parallels Tools on Linux

Go to "Virtual Machine -> Install Parallels Tools".
Open the CDROM drive (on SUSE go to /media/Parallels Tools).
Then run:
```bash
sudo ./install
```

If the following error occurs :
Missing kernel sources.
Then install kernel sources for the current kernel.
You can check then current kernel version by running:
```bash
uname -r
```
And you can check installed kernel sources by looking into:
```bash
/usr/src
```

## VMWare

### Migration d'un PC en machine virtuelle

Vérifier que le disque est bien en NTFS.
Peut-être important aussi: les *Shadow* services doivent être en démarrage automatique.

### NAT configuration under MacOS-X (VMWare Fusion)

In order to get a fixed IP address for the virtual machine:

 * Note the MAC address of the network adapter card of the virtual machine.
 * Find the `dhcpd.conf` file of the `vmnet8` interface of the VMWare DHCP server: run `sudo find /Library -iname dhcpd.conf`. There could be several files, pay attention to take the one for the `vmnet8` interface.
 * Edit the file as root user.
 * Look at the section `subnet ... netmask ...`, it gives the `range` of IPs used by the DHCP server. Your fixed IP must be outside this range.
 * Write the following lines at the end of the file, but before the comment line saying 'DO NOT MODIFY SECTION':
			host your_vm_hostname {
				hardware ethernet FF:FF:FF:FF:FF:FF;
				fixed-address 111.222.333.444;
			}
    Where you must replace `FF:FF:FF:FF:FF:FF` by the MAC address of your virtual machine network adapter card, and `111.222.333.444` by the IP address you want.
 * Restart WMWare Fusion.
Explanations taken from <http://socalledgeek.com/blog/2012/8/23/fixed-dhcp-ip-allocation-in-vmware-fusion>.

## VirtualBox

 * [Chapter 6. Virtual Networking](https://www.virtualbox.org/manual/ch06.html).

Do not forget to add yourself to the `vboxusers` group and install the
virtualbox-ext-pack in order to get access to USB devices from the VM.

See `vboxmanage`.

On Ubuntu install `apt install virtualbox`. There will be a window about security opening during the install. It will ask to input a password (at least 8 chars, be careful to choose characters that can be input when inside the BIOS).
When booting, choose "Enroll MOK".
If you missed the BIOS message or were not able to input the password, run the command `sudo update-secureboot-policy --enroll-key` to enable a new password when back in Linux.

To install on Windows 10, the Microsft Visual C++ 2019 redistribution package is needed.
See <https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170> and choose X64 version.

### Install

```sh
yay -S virtualbox
yay -S virtualbox-guest-iso  # Installed into /usr/lib/virtualbox/additions
yay -S virtualbox-ext-oracle # ...
```

To access USB devices, add running user to `vboxusers` group.

### vboxmanage (Virtual Box)

`vboxmanage` or `VBoxManage` manages the VirtualBox application and its virtual
machines.

 * [VirtualBox](https://www.virtualbox.org) official site.
 * [VirtualBox](https://wiki.archlinux.org/title/VirtualBox).
 * [Oracle VM VirtualBox: Networking options and how-to manage them](https://blogs.oracle.com/scoter/networking-in-virtualbox-v2).
 * [How to Mount USB Drives on VirtualBox](https://dzone.com/articles/how-to-mount-usb-drives-on-virtualbox).

Install on ArchLinux:
```sh
pacman -S virtualbox virtualbox-host-modules-arch
modprobe vboxdrv vboxnetadp vboxnetflt
```
If you see the following error:
```
modprobe: FATAL: Module vboxdrv not found in directory /lib/modules/
```
Then update the kernel `pacman -Syu linux` so it matches the one configured in the
version of VirtualBox installed.

Under UNIX, add user to VBox group:
```sh
usermod -a -G vboxusers myuser
```

Get VirtualBox version:
```bash
VBoxManage --version
```

List VMs:
```bash
VBoxManage list vms
```

List running VMs:
```bash
VBoxManage list runningvms
```

Get info on a VM:
```bash
VBoxManage showvminfo myvm
```

Start a VM without display:
```bash
VBoxManage startvm my_vm --type headless
```

Forward a port in NAT mode, while the machine is running:
```bash
VBoxManage controlvm "minikube" natpf1 "glx,tcp,,30700,,30700"
```

Show info on hard disk:
```sh
vboxmanage showmediuminfo ~/VirtualBox\ VMs/crocodiles/crocodiles.vdi
```

List OS types:
```sh
vboxmanage list ostypes
```

#### USB

For handling USB 2.0 and 3.0, install extension pack.
On Archlinux:
```sh
yay -S aur/virtualbox-ext-oracle
```

#### Creating a macOS VM

 * [How to Install macOS High Sierra in VirtualBox on Windows 10](https://www.howtogeek.com/289594/how-to-install-macos-sierra-in-virtualbox-on-windows-10/).
 * [How to Install macOS in VirtualBox](https://www.maketecheasier.com/install-macos-virtualbox/).
 
1. Create the ISO from the cdr file
```bash
hdiutil create -o $HOME/tmp/HighSierra.cdr -size 7316m -layout SPUD -fs HFS+J
hdiutil attach $HOME/tmp/HighSierra.cdr.dmg -noverify -nobrowse -mountpoint /Volumes/install_build
asr restore -source /Applications/Install\ macOS\ High\ Sierra.app/Contents/SharedSupport/BaseSystem.dmg -target /Volumes/install_build -noprompt -noverify -erase
hdiutil detach /Volumes/OS\ X\ Base\ System
hdiutil convert $HOME/tmp/HighSierra.cdr.dmg -format UDTO -o $HOME/tmp/HighSierra.iso
mv $HOME/tmp/HighSierra.iso.cdr $HOME/tmp/HighSierra.iso
```

2. Create the VM, named here crocodiles, in VirtualBoxr. In the settings set the ISO into the optical drive. Quit VirtualBox.

3. VM post-configuration
```bash
vboxmanage modifyvm crocodiles --cpuidset 00000001 000306a9 04100800 7fbae3ff bfebfbff
vboxmanage setextradata crocodiles "VBoxInternal/Devices/efi/0/Config/DmiSystemProduct" "MacBookPro11,3"
vboxmanage setextradata crocodiles "VBoxInternal/Devices/efi/0/Config/DmiSystemVersion" "1.0"
vboxmanage setextradata crocodiles "VBoxInternal/Devices/efi/0/Config/DmiBoardProduct" "Mac-2BD1B31983FE1663"
vboxmanage setextradata crocodiles "VBoxInternal/Devices/smc/0/Config/DeviceKey" "ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc"
vboxmanage setextradata crocodiles "VBoxInternal/Devices/smc/0/Config/GetKeyFromRealSMC" 1
```

4. Open VirtualBox and start the VM

### Error on Ubuntu with version from Oracle repository

```
[...] UBSAN: array-index-out-of-bounds in /tmp/vbox.0/SUPDrvGip.c:2206:20
[...] index 1 is out of range for type 'SUPGIPCPU [1]'
...
[...] UBSAN: array-index-out-of-bounds in /tmp/vbox.0/common/log/log.c:1558:29
[...] index 412 is out of range for type 'uint32_t [1]'
...
[...] systemd-shutdown[1]: Waiting for process: asunder
[...] sd-umoun[...]: Failed to unmount /oldroot: Device or resource busy
[...] sd-umoun[...]: Failed to unmount /oldroot/run: Device or resource busy
[...] sd-umoun[...]: Failed to unmount /oldroot/dev: Device or resource busy
[...] shutdown[1]: Could not detach DM /dev/dm-2: Device or resource busy
[...] shutdown[1]: Could not detach DM /dev/dm-1: Device or resource busy
[...] shutdown[1]: Could not detach DM /dev/dm-0: Device or resource busy
```

 * [Longer boot time, login time and slow opertions after a boot error](https://www.reddit.com/r/Ubuntu/comments/197cacn/longer_boot_time_login_time_and_slow_opertions/).
 * [UBSAN messages after install Virtualbox 7.0.12](https://forums.virtualbox.org/viewtopic.php?t=110674).
 * [kubuntu UBSAN error after latest distro push](https://forums.virtualbox.org/viewtopic.php?t=110315).

After configuring Oracle Debian repository and installing VirtualBox 7.0.14:
```
[...] UBSAN: array-index-out-of-bounds in /var/lib/dkms/virtualbox/6.1.48/...
[...] index 1 is out of range for type 'SUPGIPCPU [1]'
```
Uninstall all 6.1.48 packages:
```sh
apt-get remove --purge virtualbox virtualbox-dkms virtualbox-ext-pack
apt-get autoremove --purge virtualbox-ext-pack
```

## Vagrant

 * [Vagrant](https://www.vagrantup.com) official site.
 * [HashiCorp boxes](https://atlas.hashicorp.com/boxes/search).
 * [Multi-Machine](https://www.vagrantup.com/docs/multi-machine/).
 * [Creating a Base Box (general guide)](https://developer.hashicorp.com/vagrant/docs/boxes/base).
   + [Creating a Base Box (for VirtualBox)](https://developer.hashicorp.com/vagrant/docs/providers/virtualbox/boxes).

Windows VM provisioning:
 * [Using Vagrant for Windows VMs provisioning](https://codeblog.dotsandbrackets.com/vagrant-windows/).
 * [Windows Virtual Machine with Vagrant](https://fishilico.github.io/generic-config/windows/vagrant.html).
 * [Microsoft/EdgeOnWindows10 Vagrant box](https://app.vagrantup.com/Microsoft/boxes/EdgeOnWindows10).
  + This box is outdated (guest additions outdated), use instead [breeze/win10-edge](https://app.vagrantup.com/breeze/boxes/win10-edge).
 * [Vagrantfile for Microsoft/EdgeOnWindows10](https://gist.github.com/bugyt/445a7ef487ee9a5ad52f).

Vagrant & Ansible on Windows host:
 * [Vagrant with Ansible Provisioner on Windows](https://gist.github.com/tknerr/291b765df23845e56a29). **DEPRECATED** Old plugin (2018), does not work.

Create a default `Vagrantfile` file:
```sh
vagrant init # Blank file
vagrant init amitpatole/vagrant-windows-98 --box-version 0.0.1 # File initialized with a box and its version.
```

Add a box:
```sh
vagrant box add hashicorp/precise32
```

Create and start a virtual machine:
```sh
vagrant up
```
Rerun provisionning if machine already exists:
```sh
vagrant up --provision
```
Create a specific machine if multiple machines has been defined inside the Vagrantfile:
```sh
vagrant up --provision mymachine
```

Log on the virtual machine:
```sh
vagrant ssh
```

Stop and destroy the virtual machine:
```sh
vagrant destroy
vagrant destroy mymachine # Destroy a specific machine
vagrant destroy -f mymachine # Force destroying
```

Vagrant creates a shared directory `/vagrant` on the virtual machine, that points to the directory containing the `Vagrantfile` file on the host.

Reload (restart) the virtual machine:
```sh
vagrant reload
vagrant reload --provision  # in addition, run the provision scripts.
```

Run provision scripts when machine is running:
```sh
vagrant provision
```

Vagrant configuration file:
```ruby
Vagrant.configure(2) do |config|

  # Define the box to use
  config.vm.box = "puphpet/centos65-x64"

  # To forward port
  config.vm.network "forwarded_port", guest: 80, host: 8080

  # To run installation/configuration scripts
  config.vm.provision :shell, path: "vagrant-bootstrap.sh"
  config.vm.provision :shell, privileged: false, path: "vagrant-install-galaxy.sh"
  config.vm.provision :shell, privileged: false, path: "vagrant-run-galaxy.sh", run: "always" # Run script each time we run `vagrant up` or `vagrant reload`.
end
```

Enable GUI:
```ruby
config.vm.provider "virtualbox" do |v|
  v.gui = true
end
```

Set memory:
```ruby
config.vm.provider "virtualbox" do |vb|
  vb.memory = 2048
end
```

Error with log file when running an Ubuntu VM on another machine:
```
RawFile#0 failed to create the raw output file
/home/pr228844/dev/exhalobase/ubuntu-bionic-18.04-cloudimg-console.log
(VERR_PATH_NOT_FOUND).
```
This is because the Ubuntu box has been built with an exported log file.
To disable the log:
```ruby
config.vm.provider "virtualbox" do |vb|
  vb.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
  end
```

Update plugins:
```sh
vagrant plugin update myplugin
```

### Provisioning

Single line shell command:
```ruby
config.vm.provision "shell", inline: "echo Hello, World"
```

Naming provision directives:
```ruby
  config.vm.provision "Run my bootstrap command.", type: "shell", path: "vagrant-bootstrap.sh"
```
Provision directives can be named. The name will be displayed during the construction of the VM as a message. Be careful, that since this is a name, if you name two provision directives with the exact same string, only one will be seen and run (XXX BUG ?).


```ruby
config.vm.provision "shell" do |s|
  s.privileged = true
  s.inline = "python3 -m pip install jupyterlab jupyter-book jupytext bash_kernel"
end
```

Shell script:
```ruby
config.vm.provision :shell, path: "vagrant-bootstrap.sh"
```


### Install

 * [Install Vagrant](https://developer.hashicorp.com/vagrant/install).

### upload

Copy files from host to vm:
```sh
vagrant upload mysrc mydst
```
Works for files and folders.

### Vagrant settings

Vagrant settings can be modified inside `config.vagrant`.

#### config.vagrant.plugins

Define the list of required plugins.

```ruby
config.vagrant.plugins = "vagrant-vbguest" # Single plugin
config.vagrant.plugins = ["winrm", "winrm-fs", "winrm-elevated"] # Multiple plugins
config.vagrant.plugins = {"vagrant-scp" => {"version" => "1.0.0"}} # With a specific version
```

### vagrant-vbguest plugin

**Attention ! DEPRECATED**

Allow for automatic update of VirtualBox guest additions.

Install plugin:
```sh
vagrant plugin install vagrant-vbguest
```

### vagrant-alpine plugin

Install the vagrant-alpine plugin (allow renaming of alpine box):
```sh
vagrant plugin install vagrant-alpine
```
### vagrant winrm\* plugins

**DEPRECATED** Not needed anymore starting with version 2.4.

Install plugins:
```sh
vagrant plugin install winrm
vagrant plugin install winrm-fs
vagrant plugin install winrm-elevated
```

In `Vagrantfile`:
```ruby
config.vagrant.plugins = ["winrm", "winrm-fs", "winrm-elevated"]
```

When creating a VM that uses `WinRM` plugin, the following may occur:
```
An error occurred executing a remote WinRM command.

Shell: Cmd
Command: hostname
Message: Digest initialization failed: initialization error
```

### Example with guest additions

`Vagrantfile`:
```vagrant
# -*- mode: ruby -*-
# vi: ft=ruby ts=2 et

Vagrant.configure(2) do |config|

  config.vm.box = "ubuntu/trusty64"
  config.vm.hostname = "ubuntu-xfce4"

  config.vm.provider "virtualbox" do |v|
    v.gui = true
  end
  
  config.vm.provision :shell, path: "install-xfce4.sh"

end
```

`install-xfce4.sh`:
```sh
#!/bin/bash

apt-get update
apt-get upgrade -y
apt-get install -y xfce4 virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
```

### VM address

To fix the IP address of a VM with VirtualBox, do the following:
```ruby
address = "192.168.23.11"

Vagrant.configure(2) do |config|
    
  config.vm.define "mymachine" do |cfg|
    # Network
    cfg.vm.network "private_network", ip: address
  end
end
```

Vagrant defines a new interface device `eth1` for this IP address, but keeps
its NAT network interface on `eth0` with IP `10.0.2.15`.

Attention, by default VirtualBox is configured with a short range of addresses:
```
Ranges: 192.168.56.0/21

Valid ranges can be modified in the /etc/vbox/networks.conf file. For
more information including valid format see:

https://www.virtualbox.org/manual/ch06.html#network_hostonly
```

To allow all `192.168.*` addresses, do the following:
```sh
sudo mkdir -p /etc/vbox
echo "* 192.168.0.0/16" | sudo tee /etc/vbox/networks.conf
sudo chmod -R a+rX /etc/vbox
```

### Ansible

 * [Ansible and Vagrant](https://www.vagrantup.com/docs/provisioning/ansible_intro).

By default Vagrant generates an inventory file at path
`.vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory`.
It overrides any setting of the `ansible_host` variable by `127.0.0.1`.
Thus, for building a VM with a chosen IP, we need to use a custom inventory file:
```conf
mymachine ansible_host=192.168.56.10
```

### OpenStack

 * [Using Vagrant with OpenStack](https://blog.scottlowe.org/2015/09/28/using-vagrant-with-openstack/).
 * [Vagrant OpenStack Cloud Provider](https://github.com/ggiamarchi/vagrant-openstack-provider).

## Containerization

### podman

podman fonctionne en mode root-less (pas de démon central comme Docker).
Les utilisateurs sont donc tous indépendants.

### Singularity

 * [Singularity (software)](https://en.wikipedia.org/wiki/Singularity_(software\)).
 * [Docker vs. Singularity for data processing: UIDs and filesystem access](https://pythonspeed.com/articles/containers-filesystem-data-processing/).
 * [User Guide](https://sylabs.io/guides/3.7/user-guide/index.html)
  + [Definition Files](https://sylabs.io/guides/3.7/user-guide/definition_files.html).

Install on Archlinux:
```sh
pacman -S singularity-container
```

Build a container from a definition file:
```sh
sudo singularity build foo.sif foo.def
```

Run a container:
```sh
singularity run foo.sif
```

Example of definition file:
```def
Bootstrap: library
From: debian:7

%labels
	Author pierrick.roger@cea.fr
	Version v0.0.1
```

### Docker

Automates the deployment of applications inside software containers.
Uses a daemon to handle container images that are stored in a central place.

 * [Docker](http://docs.docker.com/).
 * [Dockerfile reference](https://docs.docker.com/engine/reference/builder/).
 * [Docker wikipedia page](https://en.wikipedia.org/wiki/Docker_%28software%29).
 * [Ansible & containers - a natural fit](http://www.ansible.com/docker).
 * [Kubernetes](http://kubernetes.io/) is a docker scheduler that can run applications contained inside docker images on a cluster/cloud.

 * [Installing docker on Debian](https://docs.docker.com/engine/installation/linux/debian/#install-using-the-repository).
 * [Get Docker for Ubuntu](https://docs.docker.com/engine/installation/linux/ubuntu/).
  + [Install using the repository](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository).

 * [When Not to Use Docker: Understanding the Limitations of Containers](https://www.channelfutures.com/open-source/when-not-to-use-docker-understanding-the-limitations-of-containers).
 * [Database in a Docker container — how to start and what’s it about](https://wkrzywiec.medium.com/database-in-a-docker-container-how-to-start-and-whats-it-about-5e3ceea77e50).
 * [Use volumes](https://docs.docker.com/storage/volumes/).

Install on Debian/Ubuntu:
 * see [Install using the apt repository](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository).
 * Be careful to set `ubuntu` and not `debian` inside `/etc/apt/sources.list.d/docker.list`.
 * Then run `sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin`.

Delete all Docker cache folders: `docker system prune -a`.

#### Installing

 * [Docker installation on Ubuntu](https://docs.docker.com/engine/installation/linux/ubuntulinux/).

Normally you have to be part of the group `docker` to run Docker. However it may not work, in which case you will be forced to run Docker as root (`sudo docker ...`).

Install on Archlinux:
```sh
pacman -S docker docker-buildx
systemctl enable --now docker
usermod -a -G docker myuser
```
Then logout and login again.

Install on MacOS:
```sh
brew install docker # Client
brew install homebrew/cask/docker # Server
```

#### Dockerfile

Only the context folder (i.e.: the folder where the Dockerfile
file resides) and its content is accessible when building an image.
Be careful, the whole context folder is transmitted to Docker for the building step.
Use the `.dockerignore` to omit certain files and folders.

Example:
```dockerfile
FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN apt-get install -y libterm-readline-gnu-perl 
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y --no-install-recommends blastem
RUN apt-get install -y --no-install-recommends pulseaudio
RUN apt-get install -y --no-install-recommends xfonts-base

RUN mkdir /sega

RUN groupadd -g 1000 pierrick
RUN useradd -d /home/pierrick -s /bin/bash -m pierrick -u 1000 -g 1000
USER pierrick
ENV HOME /home/pierrick

COPY --chown=pierrick:pierrick *.md /sega/.

ENTRYPOINT ["/usr/games/blastem", "/sega/Sonic the Hedgehog (16-bit) (Prototype).md" ]
```

Build an clean image:
```dockerfile
FROM ubuntu AS myimg
RUN ...
ENTRYPOINT ...
FROM ubuntu
COPY --from=myimg ...
...
```

##### ARG

 * [ARG](https://docs.docker.com/engine/reference/builder/#arg).

Create a variable that is only accessible during build time:
```dockerfile
ARG myvar=mydefaultvalue
```
The value set is a default value, it can be overriden by the build command with
the `--build-arg` option:
```sh
docker build --build-arg myarg=mynewvalue .
```

Use a variable for setting the tag of the base image:
```dockerfile
ARG DOCKER_BASH_VERSION=5.1
FROM bash:${DOCKER_BASH_VERSION}
```

##### CMD

 * [CMD](https://docs.docker.com/engine/reference/builder/#cmd).

Define a command to run when a container is run:
```dockerfile
CMD cowsay Hello
### or
CMD ["cowsay", "Hello"]
```
The defined command can be overriden, and we can run another program.

##### ENTRYPOINT

 * [ENTRYPOINT](https://docs.docker.com/engine/reference/builder/#entrypoint).

ENTRYPOINT defines a default application to run with/without default arguments.
Example:
```dockerfile
ENTRYPOINT cowsay Hello
### or
ENTRYPOINT ["cowsay", "Hello"]
```

Define a default application (`ENTRYPOINT`) with its default parameters (`CMD`):
```dockerfile
ENTRYPOINT ["cowsay", "Hello"]
CMD ["Jim"]
```

Arguments can be passed on the command line of `docker run`.

##### ENV

Create an environment variable that will be available at run time:
```dockerfile
ENV MY_ENV_VAR=my_value
```

#### .dockerignore

File to list files and folders to ignore in the context folder.

#### attach

Re-attach to a container executing in background:
```sh
docker attach my_container
```

#### build

 * [Multi-stage builds](https://docs.docker.com/build/building/multi-stage/).

To build an image from a Dockerfile, run:
```sh
docker build -t myimage .
```
`-t` for setting the tag (i.e.: the image name).

Choose another docker file:
```sh
docker build -f my_other.dockerfile
```

Full progress output:
```sh
docker build --progress=plain ...
```

Disable the cache (i.e.: rebuild all):
```sh
docker build --no-cache ...
```

#### buildx

```sh
docker buildx build ...
```

#### builder

Delete All build cache:
```sh
docker builder prune
```

#### commit

Create a new image...

#### diff

```sh
docker diff ...
```

#### exec

To run a command inside a running container:
```sh
docker exec 0440ee7a58e5 pwd
docker exec -i 0440ee7a58e5 /bin/bash # to run interactively
```

#### image

Remove all images without at least one container associated to them:
```sh
docker image prune -a
```

#### images

To list images:
```sh
docker images
```

#### inspect

Display metadata of an image:
```sh
docker inspect my_image
```

#### kill

Send SIGKILL:
```sh
docker kill my_container
```

#### ps

Info dernier conteneur executé:
```sh
docker ps -l
```

Identifiant du dernier conteneur executé:
```sh
docker ps -ql
```

To list all containers:
```sh
docker ps -a
```

To list only running containers:
```sh
docker ps
```

#### push

Push an image to DockerHub (or other repository ?)

#### rm

For removing all containers:
```sh
docker rm $(docker ps -aq)
```

#### rmi

Removing all images of a repository:
```sh
docker rmi $(docker images -q mydocker)
```

Removing all images:
```sh
docker rmi $(docker images -qa)
```

#### run

Run an image:
```sh
docker run myimage
```
It will run either the default `CMD` and/or `ENTRYPOINT`.

To run a container with a custom name:
```sh
docker run --name mycontainer myimage
```

To run a container interactively (redefine the entrypoint):
```sh
docker run -it --entrypoint /bin/bash myimage
```
`-i`: interactive
`-t`: allocate a pseudo-TTY.

To mount a local folder onto the container file system, in read/write mode:
```sh
docker run -v /my/localhost/folder:/my/docker/folder myimage
```
In read-only mode:
```sh
docker run -v /my/localhost/folder:/my/docker/folder:ro myimage
```

#### save

Export an image:
```sh
docker save my_img -o my_img.tar
```

#### search

Searching for public docker base images:
```sh
docker search centos
```

Export an image:
```sh
docker save -o myimage.img myimage
```

Import an image:
```sh
docker load -i myimage.img
```

To go inside a docker image:
```sh
docker run -t -i --entrypoint=bash myimage
```
`-t`: allocate a pseudo-tty.

#### start

Restart a stopped container:
```sh
docker start ...
```

#### system

`docker system prune -a` removes:
 - all stopped containers
 - all networks not used by at least one container
 - all images without at least one container associated to them
 - all build cache


#### stop

Send SKIGTERM then SIGKILL:
```sh
docker stop my_container
```
