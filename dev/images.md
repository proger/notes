# Image
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

## Free or open licensed photo sites

 * https://search.creativecommons.org/

 * [pixabay](https://pixabay.com/). Germany.
 * [Pexels](https://www.pexels.com/). Germany.
 * [Unsplash](https://unsplash.com/). Canada, Québec.
 * https://iconduck.com/
 * https://thenounproject.com/
 * https://www.flaticon.com/

## Image viewers

### feh

Light image viewer.

Display images of the current directory:
```sh
feh -g 640x480 --auto-rotate -. -d -S filename .
```
	-g ...x... set/force geometry
	-. scale image
	-d Print filename onto the image
	-S ... sort by type: filename, name, dirname, mtime, width, size, ...

Set X background image:
```sh
feh --bg-scale /path/to/image.file
```
Other scaling for background:
	--bg-tile FILE
	--bg-center FILE
	--bg-max FILE
	--bg-fill FILE

Set randomly the background image:
```sh
feh --bg-fill --randomize ~/data/wallpapers/
```

Key           | Desc
--            | ---
`!`           | Fit one direction only (width or height) to cover all window.
`Alt-<arrow>` | Move picture.
`/`           | Fit totally in window.
`*`           | Real size.
UP            | Zoom in.
DOWN          | Zoom out.
`f`           | Toggle fullscreen.
`<` `>`       | Rotate 90° left or right.
`s`           | Save current image into `feh_<PID>_<ID>_<image_filename>` file.

### digikam

Photo viewer, with a map view.
Map is a glob, not very detailed, not very easy to use.

## Image Magick

Resize to 50%:
```bash
magick convert -resize 50% input.jpg output.jpg
```

Make a PDF from images:
```bash
magick convert *.jpg myfile.pdf
```

Image quality for jpeg and mpeg (1 lowest, 100 best):
```bash
magick convert -quality 60% input.jpg output.jpg
```
See <http://www.imagemagick.org/script/command-line-options.php#quality>.

Convert to gray scale:
```bash
magick convert -colorspace Gray input.jpg output.jpg
```

```bash
for f in *ppm ; do convert -quality 100 $f `basename $f ppm`jpg; done 
```

Rotate 180 degrees:
```bash
magick convert -rotate 180 input.jpeg output.jpeg
```

Create a .ico file:
```sh
convert myimage.png -define icon:auto-resize=16,24,32,48,64,72,96,128,256 myicon.ico
```

Converting:
```sh
convert image.eps image.png
```

fill: set the fill color
	-fill blue
	-fill "#ddddff"
	-fill "rgb(255,255,255)"

Background:
	-background white
	-background transparent

Resize:
	-resize 100x200 # --> preserve aspect ratio, stop as soon as output image is inside input image
	-resize x200 # --> only care about height
	-resize 100 # --> only care about width
	-resize 100x200^ # --> stop as soon as one of width or height is met
	-resize 100x200! # --> don't preserve aspect ratio
	-resize 30% # --> resize to 30% of width
	
Resize & crop center:
```sh
convert -resize 50x50^ -gravity Center -crop 50x50+0+0 in.jpg out.jpg
```

Transparent:
	-transparent '#ffffff'
can be used with fuzz

Fuzz: specify a tolerance when replacing color
	-fuzz 10% -transparent '#aaaaaa'
	
Brightness:
	-modulate brightness[,saturation,hue]
Expressed in percentage of the original values.
Example:
```sh
convert -modulate 150 in.jpg out.jpg
```

List available fonts:
```sh
convert -list font
```

Convert text to image:
```sh
convert -background white -fill '#3ba661' -font Bitstream-Charter-Italic -pointsize 72 label:Some_text name.gif
```

Create a color gradient:
```sh
convert -size 100x100 gradient:green-yellow gradient_range3.jpg
```

Set the resolution:
```sh
convert -density 200 myfile.jpg myfile.pdf
```

Convert a PDF into a PDF made of only images:
```sh
convert -density 150 -quality 10 -compress jpeg in.pdf out.pdf
```

Trim the borders (useless area with a single color) of an images:
```sh
convert -trim original.png trimmed.png
```

### ERRORS

Error on Debian:
```
convert-im6.q16: attempt to perform an operation not allowed by the security
policy `PDF' @ error/constitute.c/IsCoderAuthorized/421
```

Solution:
 * [imagemagick - convert not allowed [duplicate]](https://askubuntu.com/questions/1127260/imagemagick-convert-not-allowed).

In `/etc/ImageMagick-6/policy.xml` change:
```xml
<policy domain="coder" rights="none" pattern="PDF" />
```
in
```xml
<policy domain="coder" rights="read|write" pattern="PDF" />
```

## Color pickers/choosers

### gcolor3

Color picker based on GTK.

Install:
```sh
apt install gcolor3 # Ubuntu
```
