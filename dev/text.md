# Text processing
<!-- vimvars: b:markdown_embedded_syntax={'sh':'bash','bash':'','awk':''} -->

Measure length of lines:
```sh
awk '{print length}' myfile | sort -n | uniq -c | column -t -O 2,1
```

## ASCII art

 * [pyfiglet](https://pypi.org/project/pyfiglet/).
 * [Python ASCII Magic](https://pypi.org/project/ascii-magic/).
 * [Python ASCII Matics](https://pypi.org/project/asciimatics/) helps create full-screen text UIs, including also ASCII animations.

### banner

Install:
```sh
pacman -S banner # Archlinux
```

Generates simple ASCII art:
```sh
banner 0123
```

### figlet

Generates ASCII art:
```sh
figlet -f ogre "Potion"
```
Use `slant` font for italic.

Fonts are stored in `/usr/share/figlet/fonts` as `.flc` and `.tlf`.

### toilet

A `figlet` replacement.

Store fonts in `/usr/share/figlet/` as `.tlf`.

To use a toilet font:
```sh
toilet -f future spirou
```

To use a figlet font:
```sh
toilet -f fonts/slant spirou
```

## agrep

Aproximate grep.

On ArchLinux, install package `tre` package.

## aspell

To decompress a words list from installation tar.bz2 file:
```sh
precat *.cwl
preunzip *.cwl
```

To have correct UTF-8 handling by *aspell*, the `LC_CTYPE` locale environment variable must be set accordingly:
```sh
export LC_CTYPE=UTF-8
```
or
```sh
export LC_CTYPE=en_US.UTF-8
```

To dump full list of words for a language:
```sh
aspell -l fr dump master
```
## cat

For printing special characters (carriage returns, ...):
```sh
cat -vte
```
Display ^M for the carriage return, and $ for the newline.

## colordiff

Install:
```sh
apt install colordiff
```

Print diff in colors:
```sh
colordiff file1.txt file2.txt
```

## colrm

Remove columns from a file, where a column means a character column.

Remove all characters from 5 to end:
```bash
colrm 5
```

Remove all characters from 5 to 10:
```bash
colrm 5 10
```
## column

Align the column of a file for viewing:
```bash
column -t myfile
```
By default the columns are considered separated by white spaces (space and tab).

If you have a CSV file:
```bash
column -t -s , myfile.csv
```

With a TSV file:
```bash
column -t -s $'\t' myfile.tsv
```
## comm

Compares two sorted files line by line.
Outputs three columns: 
 1. Lines unique to first file.
 2. Lines unique to second file.
 3. Lines common to both files.

Prints only columns 1 and 2:
```sh
comm -3 file1 file2
```
## csplit

Split file horizontally, on line numbers or line matching a pattern.

## cut

To cut a portion of text of each line:
```bash
cut -c 3-5,10-20 <my_file
```
Take only characters 3 to 5 and 10 to 20.

Select fields separated by a character:
```bash
cut -d ',' -f 1,3,4 <my_file
```
## diff

Install:
```sh
apt install diffutils # Debian
yum install diffutils # Fedora
```

To make a patch:
```bash
diff -ru <old sources> <new sources> >myfile.patch
diff -ruwNB old_v3 new_v3 > v3.patch
```

Flag | Description
---- | --------------------------------
`-w` | Ignore all white spaces.
`-B` | Ignore blank lines.
`-N` | Make patch also for new files.

Exclude files:
```bash
diff -x .c ...
```
It works for directory inside the path, for instance:
```bash
diff -x .git ...
```
Will exclude all files containing `.git` in the path, including `/.../.git/.../somefile`.
## docx2txt

Extract text from docx file:
```sh
docx2txt myfile.docx myfile.txt
```
## dos2unix & unix2dos

Convert a text file with Windows style (line ending CRLF) file into a unix style file:
```bash
dos2unix myfile.txt
```
## expand

Replace tabulations by spaces.

Replace tabs in a file:
```sh
expand -t 4 myfile
```

Replace tabs in the output of a command:
```sh
some_command | expand -t 4
```
## expr

Measure length of a string:
```bash
expr length "mystring"
expr "mystring" : '.*'
```

String matching with regexp:
```bash
expr match "My text to search." '^.* [txe]\+ '
```
Returns the numbers of matched characters.
The start of line character is implicit. Thus the following line will return 0:
```bash
expr match "My text to search." ' [txe]\+ '
```

String extraction with regexp:
```bash
expr match "My text to search." '^.* \([txe]\+\) '
expr "My text to search." : '^.* \([txe]\+\) '
```
## fold

Wraps long lines to fit width.

To wrap lines to 80 columns:
```bash
fold -w 80 <my_file
```

To wrap at last space character:
```bash
fold -s -w 80 <my_file
```
## grep

Specifying multiple expressions:
```sh
grep -e toto -e titi # will match all lines of files that match either toto or titi
```

Displaying list of matching or non-matching files:
```sh
grep -l -e resto * # display matching files
grep -L -e resto * # display non-matching files
```

Count number of match:
```sh
grep -c my_string myfile.txt
```

Find non-UTF-8 chars in a file:
```sh
grep -axnv '.*' my.file
```

Labelling standard input:
```sh
gzip -cd foo.gz | grep --label=foo something
```

Print filename for each match:
```sh
grep -H toto *
```

Print line number for each match:
```sh
grep -n toto *
```

Print context (some lines before and/or after):
```sh
grep -A 2 toto * # 2 lines after
grep -B 2 toto * # 2 lines before
grep -C 2 toto * # 2 lines before and 2 lines after
```

Exclude files:
```sh
grep --exclude=*.class
```

Select files (include only specified files):
```sh
grep --include=*.java
```

Print only n lines:
```sh
grep -m <n> ...
```

Ignore binary files:
```sh
grep -I ...
```

Match whole lines:
```sh
grep -x ...
```

Exclude from one file lines listed in another:
```sh
grep -v -x -f toexclude.txt myfile.txt
```

No regex:
```sh
fgrep ...
```

Extended regex:
```sh
egrep '^([a-z]+)(.txt)?$' myfile
```
or
```sh
grep -E '^([a-z]+)(.txt)?$' myfile
```
Extended reg ex can also be used with backslash:
```sh
grep '^\([a-z]\+\)\(.txt\)\?$' myfile
```
Use of backslash for extended regular expressions is (fully?) not POSIX, so it
will not work on BSD.

Recursive:
```sh
rgrep ...
```

Search for files containing non-ASCII characters and control characters:
```sh
LC_ALL=C grep -l '[^[:print:]]'
```
## head

Output the 3 first lines:
```sh
head -n 3 myfile
```

Output all lines but the last 5:
```sh
head -n -5 myfile
```
## hunspell

Spell checker.

Install on Archlinux:
```sh
pacman -S hunspell hunspell-en_us hunspell-fr hunspell-it
```
## iconv

Converts between encodings.

Convert from Mac-OS Roman encoding to Unicode on standard output:
```sh
iconv -f mac -t utf-8 myfile
```
## join

Join two files in a way similar to relational databases:
```bash
join a.txt b.txt >c.txt
```
By default `join` uses the first column as the join field.

Set the join fields, using column 3 in the first file and column 4 in the second file:
```bash
join -1 3 -2 4 a.txt b.txt >c.txt
```
## less

Text file viewer.
## m4

 * [Macro Magic: M4 Complete Guide](https://www.linuxtoday.com/blog/macro-m4-guide/).

Macro processor.

```sh
m4 -Dmytag=My_Value my_file.txt
```
## more

Text file viewer.
## nl

Numbers the lines of a file.
## pandoc

 * [Official website](http://johnmacfarlane.net/pandoc/).
 * [User guide](http://johnmacfarlane.net/pandoc/README.html).
 * [Demos](http://johnmacfarlane.net/pandoc/demos.html).
 * [Slide show](http://johnmacfarlane.net/pandoc/demo/example9/producing-slide-shows-with-pandoc.html).
 * [Templates](http://johnmacfarlane.net/pandoc/demo/example9/templates.html).
 * [Git clone](https://github.com/jgm/pandoc.git).

Pandoc converts markdown files into many different formats: HTML, LaTeX, PDF, MS Word, ...

Generate a full HTML document:
```sh
pandoc -s --to html -o mydoc.html myfile.md
```

Generate a PDF:
```sh
pandoc -s --to pdf -o mydoc.pdf myfile.md
```

### Syntax highlighting

To know which languages are supported, run:
```bash
pandoc --version
```

### Main options

Flag                      | Description
------------------------- | ---------------------------
--toc                     | Generate table of content.
-N                        | Generate chapter numbering.
--latex-engine=myengine   | Set latex engine to use: xelatex, ...
--reference-docx=ref.docx | Specify docx to use as template.

### Pictures

#### Insert picture in the middle of text

```markdown
See the ![GrenadFactory UML diagram](diagrams/factory.png) for an illustration.
```

Or if you want the image alone on a line, add a non-breaking space:

```markdown
![GrenadFactory UML diagram](diagrams/factory.png)\ 
```

#### Use a caption and a reference for an image.

```markdown
See figure \ref{factory}.
![GrenadFactory UML diagram\label{factory}](diagrams/factory.png)
```
The image line must have nothing at the end: no space, dot, nothing.
## paste

Paste two or more files side by side:
```bash
paste a.txt b.txt c.txt >d.txt
```
By default, replace new line chars in a.txt and b.txt by tabulation.

## patch

Patching a source tree:
```bash
patch -p1 < patch-file-name-here
```

Patching a file:
```bash
patch original_file patch_file
```
## pcre2grep

Same as pcregrep, but uses the PCRE2 library.
## pcregrep

PERL-Compatible Regular Expression grep.

Search for an expression on two lines:
```sh
pcregrep -M '^msgstr ""$\n^$' site/locale/fr_FR.utf8/LC_MESSAGES/exhalobase.po
```
## printf

Formats and prints data.

Repeat the equal (`=`) character 5 times:
```sh
printf "=%.0s" {1..5}
```

Repeat the equal (`=`) character n times:
```sh
printf "=%.0s" $(seq 1 $n)
```

Convert ASCII code to character:
```sh
printf \\$(printf '%03o' 65)
```

Attention, `printf` uses the current numeric locale:
```sh
printf "%.2f" 64.37 # With LC_NUMERIC=en_US.UTF-8
printf "%.2f" 64,37 # With LC_NUMERIC=fr_FR.UTF-8
```

Print in colors
```sh
printf '%b' "\033[1;34mlight blue\033[0m\n"
```
## sed

To remove spaces at the end of a line:
```sh
sed -re 's/\s+$$//'
```

In shells, the `$` sign must be doubled in roder to do not be confused with a variable:
```sh
sed -re 's/\r$$//'
```

To remove carriage returns at the end of a line (Windows text file):
```sh
sed -re 's/\r$//'
```

To delete lines:
```sh
sed -re '/COUCOU/d'
```

To run multiple expressions in the same sed:
```sh
sed -re '...' -e '...' -e '...' ...
```

To edit the first line only:
```sh
sed -e '1s/foo/bar/g' myfile
```

Edit and print only selected lines:
```sh
sed -n -e 's/^some text\(.*\)/new text\1/p' myfile
```

Print even lines:
```sh
sed -n 'n;p' filename
```

Print odd lines:
```sh
sed -n 'p;n' filename
```

Print line 12:
```sh
sed -n '12p' filename
```

Print only the lines inside a range of regex addresses:
```sh
sed -n '/GOOGLE/,/ssl/p' ~/.offlineimaprc
sed "0,/<p class='short'/d;/<\/p>/Q" myfile
```

To delete lines inside a range of regex addresses:
```sh
sed '/^---$/,/^---$/d' myfile
```

Delete first line:
```sh
sed '1d' myfile
```

To insert a carriage return, you need to actually type it:
```sh
echo "MY LINE OF TEXT" | sed 's/ /\
/g'
```

As with `grep` extended reg ex are available through the use of the backslash
character:
```sh
sed 's/^\([a-z]\+\)\(.txt\)\?$/\1/'
```
However it is better to use the `-E` option for compatibility with BSD:
```sh
sed -E 's/^([a-z]+)(.txt)?$/\1/'
```

### In-place editing

In-place editing option is not compatible between BSD and GNU tools.
To have something portable, use `perl` with `-pe` and `-i` flags instead of `sed`.
See `perl.md`.

To edit in-place (BSD sed):
```sh
sed -e mycommand -i .bkp myfile # save backup with .bkp extension
sed -e mycommand -i '' myfile # no backup
```
For GNU sed:
```sh
sed -e mycommand -i.bkp myfile # save backup with .bkp extension
sed -e mycommand --in-place=.bkp myfile # save backup with .bkp extension
sed -e mycommand -i myfile # no backup
```

To add a carriage return to the end of a file:
```sh
sed -i '' -e '$a\' toto.txt
```
## shuf

Permute lines:
```sh
shuf myfile
```

Generate and permute a series of numbers:
```sh
shuf -i 1-10
```
## sort

Sorting inplace:
```sh
sort myfile -o myfile
```

In MacOS-X, sort will not recognize unicode in UTF-8 files.
One must use iconv first to convert file into UTF8-MAC format:
```sh
iconv -t UTF8-MAC myfile.txt | sort
```

Sort according to ASCII order (`.` before letters, ...):
```sh
LC_COLLATE=C sort myfile.txt
```

Sort numbers:
```sh
sort -n ...
```
## tail

Output the 3 last lines:
```sh
tail -n 3 myfile
```

Output from line 4 to the end:
```sh
tail -n +4 myfile
```
## tr

Set in uppercase:
```bash
tr '[:lower:]' '[:upper:]'
```

Removing all carriage returns:
```bash
tr -d '\r'
```

Convert a string to lowercase:
```bash
echo $MY_VAR | tr '[:upper:]' '[:lower:]'
```

Capitalize first letter:
```bash
$(tr '[:lower:]' '[:upper:]' <<< ${myvar:0:1})${myvar:1}
```
## uniq

Filter out duplicated lines:
```bash
uniq myfile
```

Output only the duplicated lines:
```bash
uniq -d myfile
```
## unix2dos

To convert from UNIX style to Windows style:
```bash
unix2dos myfile.txt
```
## wc

Count number of lines, words, bytes or characters of files.

Get number of lines (number of new line characters):
```sh
wc -l myfile
```
or
```sh
my_command | wc -l
```

Get length of longest line:
```sh
wc -L myfile
```

Count number of bytes:
```sh
wc -c myfile
```

Print the length of a string:
```sh
echo "Ma chaîne" | wc -m
```

Unfortunately if the last line is not empty but does not contain a new line, it is not counted.
In this case it is better to use the following command:
```sh
awk 'END { print NR }' myfile
```
## zgrep

Run grep on a compressed file.
