# OOP
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':'','cpp':''} -->

## class relationships

Public inheritance means isa
    By writing that D derives publicly from B, you say that D "is a" B.
    Which means that wherever an object of type B is used, an object of type D can be used in place.
```cpp
class B {};
class D: public B {};
```

HAS-A:
    has-a or is-implemented-in-terms-of relationships must be implemented through layering ( = composition, containment and embedding) i.e. as private member.

    example illustrating is-implemented-in-terms-of:
      Suppose we want to create a Set class from std::list. For example to have a Set template class that doesn't require operator '<' like std::set does.
      It's wrong to make Set inherit publicly from std::list, since a set isn't a list.
      However we can say that a set can be implemented in terms of a list. So we make std::list a private member of Set.
```cpp
template<class T> class Set {
private:
  std::list<T> data;
};
```

Private inheritance 
    private inheritance means is-implemented-in-terms-of.
    Layering (composition) has to be prefered over private inheritance.
    For a good use of private inheritance, see templates/code bloat.


## Python

 * [Data model](https://docs.python.org/3/reference/datamodel.html). Lists all built-in attributes and methods of function objects and classes (__name__, __del__, __init__, ...).
 * [Private Variables](https://docs.python.org/3/tutorial/classes.html#private-variables).
 * [Metaclasses](https://www.python-course.eu/python3_metaclasses.php).

### Abstract classes

Allows to define abstract classes and methods.

Using metaclass style declaration:
```python
import abc
class MyAbstractClass(metaclass=abc.ABCMeta):
...
```
About metaclasses see <https://www.python-course.eu/python3_metaclasses.php>.

Using dummy ABC class for cleaner declaration:
```python
import abc
class MyAbstractClass(abc.ABC):
```

Abstract method:
```python
import abc
class MyClass(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def my_abstract_method(self):
        raise NotImplementedError
```
Abstract class and method with `abc` module:
```python
from abc import ABCMeta, abstractmethod

class MyClass(metaclass=ABCMeta):

    @abstractmethod
    def my_abstract_method(self):
        return 1 # Implement some default behaviour that is callable by subclasses with `super()`.

class MySubClass(MyClass):

    def my_abstract_method(self):
        return 2 + super(self.__class__, self).my_abstract_method()
```

### Public/private

All methods and attributes are public in Python.
A common convention is to prefix by an underscore all methods and attributes
that we want to be private.

### Definition

Class definition:
```python
class MyClass:
    """A simple example class"""
    i = 12345
    def f(self):
        return ’hello world’
```

### Instantation

Class instantiation:
```python
obj = MyClass()
```

### Properties

Overwrite a property in a sub-class:
```python
class B(A):

    @property
    def foo(self):
        return self._foo
```

### `__init__`

Initialization method:
```python
class MyClass:
    def __init__(self):
        self.data = []
class MyComplex:
    def __init__(self, realpart, imagpart):
        self.r = realpart
        self.i = imagpart
```

### `__del__`

TODO

### `__enter__`

### `__exit__`

### `__getitem__`

```python
def __getitem__(self, val: int | slice) -> str:
    if isinstance(val, int):
        return self._seq[val]
    return self._seq[val.start:val.stop]
```

### `__iter__`

### `__len__`

### `__str__`

`__str__()` returns a string to be printed when calling the `print()` method
on a object.

If no `__str__()` is defined, Python uses the `__repr__()` method.

```python
class A:
    def __str__(self):
        return "Some description"
```

### `__repr__`

`__repr__()` returns a text to be printed when calling `repr(myObj)` or when in
interactive prompt.

```python
class A:
    def __repr__(self):
        return "Some description"
```

### Inheritance

Calling mother class' constructor:
```python
class B(A):
    def __init__(self, arg1):
        super().__init__(arg1)
```
To pass all parameters to mother class without knowing them use `**kwd`:
```python
class B(A):
    def __init__(self, **kwd):
        super().__init__(**kwd)
```

You can't define multiple constructors, `__init__()` method is unique.
Solutions are:
 1. Accept different arguments, and test their type inside `__init__()` in order to decide what to do.
 2. Define a factory method using the `@classmethod` attribute.

Reference to class object:
```python
myobject.__class__
```
    
Declaring a method that is defined outside the class:
```python
def myfunc(self, x, y) # absolutely awful !
    return x+y
class MyClass:
    myfunc_in_class = myfunc
```

Inheritance:
```python
class MySubClass(MyBaseClass):
    # ...
```

Testing inheritance:
```python
issubclass(myobj, myclass)
issubclass(bool, int)    # <-- True
issubclass(unicode, str) # <-- False
```

Multiple inheritance:
```python
class Base1:
    def __init__(self):
        super().__init__()

class Base2:
    def __init__(self):
        super().__init__()

class Base3:
    def __init__(self):
        super().__init__()

class MyDerivedClass(Base1, Base2, Base3):
    def __init__(self):
        super().__init__()
```
Old-style classes: rule for searching an attribute or method is depth-first, then left-to-right.
New-style classes: the method resolution order changes dynamically to support cooperative calls to super(). --> TODO: search for more explanations.

### Attributes (aka properties, members or variables)

 * [The @property Decorator in Python: Its Use Cases, Advantages, and Syntax](https://www.freecodecamp.org/news/python-property-decorator/).

If declared outside of a method, a variable is a class variable (i.e.: static):
```python
class MyClass:
    my_var = 5 # Class variable (static).

    def __init__(self):
        my_var = 'foo' # Instance variable, overrides the class variable of
                    # the same name.
```

An instance member can also be defined outside a class:
```python
class MyClass:
    pass

o = MyClass()
o.my_var = 10 # Instance variable.
```

Adding an attribute dynamically to an instance:
```python
setattr(self, 'property_name', property_value)
```

Testing if an attribute exists:
```python
hasattr(obj, 'name')
```

Looping on all attributes of an object:
```python
for attr, value in myobj.__dict__.iteritems():
    print(str(attr) + ': ' + str(value))
```
`__dict__` is of type `dict`.

#### Properties, aka managed attributes

A property is a wrapping of an attribute with getter and setter methods:
```python
class MyClass:

    @property
    def size(self):
        """The size."""
        return self._size

    @size.setter
    def size(self, value):
        self._size = value

    @size.deleter
    def size(self):
        del self._size
```
Inside `MyClass`, `size` is an object with at least one of `__get__()`,
`__set__()` or `__delete__()`.
Very useful to return a copy of an attribute with the getter method, and check
new value in the setter method:
```python
import copy

class MyClass:

    @property
    def items(self):
        return copy.copy(self._items)

    @items.setter
    def items(self, new_items):
        if not new_items is list:
            raise TypeError("New items must be passed inside a list object.")
        self._items = new_items
```

The same declaration can be done directly using the `property` class:
```python
class MyClass:

    def _get_size(self):
        return self._size

    def _set_size(self, value):
        self._size = value

    def _del_size(self):
        del self._size

    size = property(fget=_get_size, fset=_set_size, fdel=_del_size,
        doc="The size.")
```

Overriding a property in a sub-class:
```python
class A:

    @property
    def foo(self):
        return self._foo

    @foo.setter
    def foo(self, val):
        self._foo = val

class B(A):

    @A.foo.getter
    def foo(self):
        return self._foo + '_T'

    @A.foo.setter
    def foo(self, val):
        self._foo = 'S_' + val
```

### Methods

Class method:
```python
@classmethod
def make_from_file(cls, filename):
    # ...
```

Calling a method of a base class:
```python
MyBaseClass.method_name(self, arg1, arg2)
```

Virtual methods: all methods are virtual in Python.

Instance method objects have two attributes:
```python
m.im_self   # The instance object
m.im_func   # the function object
```

Static method (Does not take the class as first argument):
```python
class MyClass:

    @staticmethod
    def my_static_method(a, b):
        return a * b
```

Class method (Needs to take the class object as first argument):
```python
class MyClass:
    x = 1.0

    @classmethod
    def my_class_method(cls):
        return cls.x

    def my_instance_method(self):
        return MyClass.x

    def my_other_instance_method(self):
        return self.__class__.x
```

### Introspection

Getting class information:
```python
type(myinstance)
```

Getting the class name:
```python
type(myinstance).__name__
```

Testing the class/type of an object:
```python
isinstance(obj, int) # is True if obj.__class__ is int.
isinstance(myObj, MyClass)
```

Test if a class inherits from another class:
```python
issubclass(MySubClass, MyClass)
```

### Iterators

 * [Iterable class in python3](https://stackoverflow.com/questions/56237021/iterable-class-in-python3).

To create explicitly an iterator on a object:
```python
myIter = iter(myObj)
```

On an iterable object, one can call `next()` to get the next object in interation:
```python
try:
    x = obj.next()
except StopIteration: # Raised in case no more objects are avaible in the iteration.
    do_something()
```

When looping with for statement, as in:
```python
for element in [1, 2, 3]:
    # do something
```
the interpreter calls `__iter__()` on the container object, which returns an
iterator object that defines method `next()`. `next()` returns the next object
of the collection, and when no more objects are available it raises a
`StopIteration` exception.

Thus for any user-defined class, one can define an iterator function `__iter__`
that will allow to iterate on any instance of that class, using the `for`
statement. The `__iter__` returns either an iterator on a object or uses the `yield` keyword to returns objects itself.
The returned iterator instance can be the class instance itself, if it defines
the method `next()`, or a specific iterator class.
For instance:
```python
class MyClass: # The class is an iterator
    def __init__(self, data)
        self.data = data
        self.index = len(data)
    def __iter__(self):
        return self
    def next(self):
        if self.index == 0
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]
```
Of course, using the container class as the iterator class isn't satisfactory,
since it raises the issue of the index. Only one iteration can be done at a
time on a container instance.
Thus using a specific iterator class, and returning a new instance on each
`__iter__()` call, is always a best approach.

