# Graphics

 * [From Data to Viz](https://www.data-to-viz.com/).

## Galaxy

 * [Visualization of RNA-Seq results with Volcano Plot](https://training.galaxyproject.org/training-material/topics/transcriptomics/tutorials/rna-seq-viz-with-volcanoplot/tutorial.html).

## R

 * [The R Graph Gallery](https://r-graph-gallery.com/).
 * [Posit Cheatsheets](https://rstudio.github.io/cheatsheets/#listing-listing-page=1).
   + [RStudio IDE :: Cheatsheet](https://rstudio.github.io/cheatsheets/html/rstudio-ide.html).
   + [Apply functions with purrr :: Cheatsheet](https://rstudio.github.io/cheatsheets/html/purrr.html).
   + [Data import with the tidyverse :: Cheatsheet](https://rstudio.github.io/cheatsheets/html/data-import.html).
   + [Data tidying with tidyr :: Cheatsheet](https://rstudio.github.io/cheatsheets/html/tidyr.html).
   + [Data visualization with ggplot2 :: Cheat Sheet](https://rstudio.github.io/cheatsheets/html/data-visualization.html).
 * [Datacamp](https://www.datacamp.com/): a website to learn R, non-free. Some first courses in dplyr are free.
   + [The data.table R Package Cheat Sheet](https://www.datacamp.com/cheat-sheet/the-datatable-r-package-cheat-sheet).
   + [Text Data In R Cheat Sheet](https://www.datacamp.com/cheat-sheet/text-data-in-r-cheat-sheet).
   + [Tidyverse Cheat Sheet For Beginners](https://www.datacamp.com/cheat-sheet/tidyverse-cheat-sheet-for-beginners).
   + [Introduction to the Tidyverse](https://www.datacamp.com/courses/introduction-to-the-tidyverse).

 * [R colours](https://www.datanovia.com/en/blog/awesome-list-of-657-r-color-names/).

### Tidyverse

 + [Data transformation with dplyr :: Cheatsheet](https://rstudio.github.io/cheatsheets/html/data-transformation.html).
 * [The new R pipe](https://www.r-bloggers.com/2021/05/the-new-r-pipe/).
 * [Datacamp](https://www.datacamp.com/): a website to learn R, non-free. Some first courses in dplyr are free.
   + [Data Manipulation with dplyr in R Cheat Sheet](https://www.datacamp.com/cheat-sheet/data-manipulation-with-dplyr-in-r-cheat-sheet).
   + [Reshaping Data with tidyr in R](https://www.datacamp.com/cheat-sheet/reshaping-data-with-tidyr-in-r).

## Python

 * [The Python Graph Gallery](https://python-graph-gallery.com/).
