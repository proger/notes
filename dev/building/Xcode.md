# Xcode

 * [Installing the Xcode Command Line Tools (Xcode CLT)](https://developer.xamarin.com/guides/testcloud/calabash/configuring/osx/install-xcode-command-line-tools/).

Get path to the Xcode command line tools:
```bash
xcode-select -p
```

Install the Xcode command line tools:
```bash
xcode-select --install
```

## xcodebuild

`xcodebuild` is a command line tool to build an Xcode project.

Code signing error:
```
/Users/eatoni/v3/applications/iphone/twitter/build/Release-iphoneos/twitter.app: User interaction is not allowed.
** BUILD FAILED **
```
run:
```bash
Security unlock-keychain /Users/eatoni/Library/Keychains/login.keychain
```

## How to change "__MyCompanyName__" ?

Where does ProjectBuilder (Xcode) stores the string to use as `__MyCompanyName__` ? 

Open the ProjectBuilder preferences file (~/Library/Preferences/com.apple.ProjectBuilder.plist) or the Xcode one (~/Library/Preferences/com.apple.Xcode.plist) and edit the dictionary associated with the key PBXCustomTemplateMacroDefinitions (create one if it does not exist, as child of the root node), edit or add the key ORGANIZATIONNAME, the associated string value will be used when PB creates new source files.

You could also do it via Terminal.app:

```bash
Defaults write com.apple.ProjectBuilder PBXCustomTemplateMacroDefinitions '{ "ORGANIZATIONNAME" = "My Company";}'
```
Or
```bash
Defaults write com.apple.Xcode PBXCustomTemplateMacroDefinitions '{ "ORGANIZATIONNAME" = "My Company";}'
```

## Error messages

```
ERROR: a signed resource has been added, modified or deleted
```
	Build -> Clean All Targets
	Build and Go

