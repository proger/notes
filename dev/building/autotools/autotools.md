# Autotools
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','bash':'sh','make':'','config':''} -->

 * [Introduction to GNU Autotools](https://opensource.com/article/19/7/introduction-gnu-autotools).
 * [Autoconf](https://www.gnu.org/software/autoconf/manual/autoconf-2.66/autoconf.html).

 * [Autotools - a guide to autoconf automake libtool](http://www.freesoftwaremagazine.com/books/autotools_a_guide_to_autoconf_automake_libtool).
 * [A tutorial for porting to autoconf & automake](http://mij.oltrelinux.com/devel/autoconf-automake/).
 * [Les outils Automake et Autoconf....](http://www-igm.univ-mlv.fr/~dr/XPOSE/Breugnot/).
 * [The Basics of Autotools](https://devmanual.gentoo.org/general-concepts/autotools/index.html).

 * [Autobook](http://www.fifi.org/doc/autobook/html/autobook_toc.html#SEC_Contents).
 * [Simple Guide to Using GNU AutoTool](https://apps.dtic.mil/sti/pdfs/ADA553215.pdf).

Languages supported by the autotools:

 * C.
 * C++.
 * Objective C.
 * Fortran.
 * Fortran 77.
 * Erlang.

Installing:
```sh
apt-get install autoconf automake autotools-dev # On Debian/Ubuntu.
```

## Autoconf

### `autoconf`

Used to generate a `configure` script.
It is better to use `autoreconf` than `autoconf`, see below.

### `autoreconf`

Regenerate everything that needs to be regenerated from the `configure.ac` file.
It runs all programs from Autotool, Automake and Libtool packages that need to
be run.

If files `config.guess`, `config.sub`, `install-sh` and `missing` are missing,
run:
```sh
autoreconf -i
```

### bootstrap

Usually a `bootstrap` (or `autogen.sh`) script is included at the project's root, that run all
necessary commands to initialize autotools.
For instance it runs at least `autoreconf`.

### `autoheader`

Generates a `config.h.in` header file from `configure.ac`.

### `autom4te`

Cache manager that speeds up access to `configure.ac` for other scripts.

### `autoscan`

Helps generating a "reasonable" `configure.ac` file for a new project.
From an existing `configure.a`c file, it scan all source files and creates the
following files: `configure.scan` and `autoscan.log`.
It doesn't alter any existing file. In particular in doesn't touch
`configure.ac`, but instead create `configure.scan` that you can use in
replacement of your `configure.ac`.

### `autoupdate`

Update `configure.ac` and template (`*.in`) files to the syntax of the current
version of Autotools.

### `ifnames`

Scan a list of source files and displays found `C` preprocessor definitions.

### configure.ac

#### Automake initialization

```config
AM_INIT_AUTOMAKE
```

Allow to reuse object file already generated when including source file from
another folder:
```config
AM_INIT_AUTOMAKE([subdir-objects])
```

#### Message display

```config
AC_MSG_NOTICE("BOOST_LDFLAGS=$BOOST_LDFLAGS")
```

#### AX\_APPEND\_FLAG

Add a flag only if it is not already there:
```config
AX_APPEND_FLAG([ -pedantic], [CFLAGS]) 
```

#### AC\_CHECK\_LIB

Check if a library is installed:
```config
AC_CHECK_LIB(ssh2, libssh2_session_init_ex)
```

#### AC\_SEARCH\_LIBS

Search for a library:
```config
AC_SEARCH_LIBS([libssh2_session_init_ex], [ssh2])
```

If the library resides in a non-standard path, add it to `LDFLAGS`:
```config
LDFLAGS="-L/my/path/lib $LDFLAGS"
AC_SEARCH_LIBS([libssh2_session_init_ex], [ssh2])
```

## Automake

### `automake`

 * [Flag Variables Ordering](https://www.gnu.org/software/automake/manual/html_node/Flag-Variables-Ordering.html). Ordering of `AM_CLAGS`, `CFLAGS` and custom flag variables.

Generates makefile templates (`Makefile.in`) from `Makefile.am`.

If files `config.guess`, `config.sub`, `install-sh` and `missing` are missing,
run:
```sh
automake --add-missing
```

### `aclocal`

Generates `aclocal.m4` for `autoconf`. `aclocal.m4` file was originaly designed for defining user-provided extension macros. Now that it is used by `aclocal`, there's a new file name `acinclude.m4` for the purpose of the user.

Today the `aclocal`/`acinclude` pardigm is obsolete. The current recommandation is to make a `m4` directory in the project, and place in it all `.m4` files. All these `m4` files will be gathered into one `aclocal.m4` file before Autoconf processes the `configure.ac` file.

### Makefile.am

 * See chapter [13 What Gets Cleaned](http://www.gnu.org/software/automake/manual/html_node/Clean.html).

Writing a custom clean task:
```make
clean-local:
	$(RM) -r myfolder
```

Allowing to use a `README.md` file instead of the GNU standard `README` file:
```make
AUTOMAKE_OPTIONS = foreign
```

Define subfolders in which other `Makefile.am` are defined:
```make
SUBDIRS = src doc
```

## Libtool

Provides :
 * a set of Autoconf macros that hide library naming differences in makefiles.
 * an optional library of dynamic loader functionality that can be added to your programs, allowing you to write more portable runtime dynamic shared object management code.

### `libtool` (program)

### `libtoolize` (program)

Generates a custom version of libtool script.
This script is then used by Automake-generated makefiles.

### `ltdl` (static and shared libraries) & `ltdl.h` (header)

Provides a consistent run-time shared object manager across platforms.
May be linked statically or dynamically.

## `configure` generated script

When run, `configure` checks system and generates `config.status` which is a
script that generates the `config.h` and the makefiles from `config.h.in` and
`Makefile.in` files.
`configure` also writes a `config.log` file.
`configure` calls `config.status` just after having created it.

### Remote building

`configure` can be called from a different place that the sources tree.
Example:
```bash
tar -zxvf doofabble-3.0.tar.gz
mkdir doofabble-3.0.debug
cd doofabble-3.0.debug
../doofabble-3.0/configure --enable-debug
make
```

## Example: a simple scripts project
	
How to use autotools with a simple projects containing only script files ?
	
Run autoscan:
```bash
autoscan
mv configure.scan configure.ac
```

Edit `configure.ac`, and find `AC_INIT` line. Replace text with project name, version number and email address for bugs.
	
Create makefile `Makefile.am`:
```make
AUTOMAKE_OPTIONS = foreign
bin_SCRIPTS = <list of scripts (separated by spaces)>
```
The "foreign" values is for telling `aclocal` that this is not a GNU project (a GNU project requires certain folders and files to be present, such as `doc`, `src`, `INSTALL`, `COPYING`, etc.)
	
Edit `configure.ac` again. Initialize `automake`, right after `AC_INIT`, by adding the following line:
```
AM_INIT_AUTOMAKE(<project_name>, <version_number>)
```
Create `Makefile` by adding the following line:
```
AC_OUTPUT(Makefile)
```

Run aclocal:
```bash
aclocal
```
It generates `aclocal.m4` (macros for `automake`).
	
Run `automake`
```bash
automake --add-missing
```
It creates `Makefile.in` files.

Run `autoconf`:
```bash
autoconf
```
It creates the `configure` script.
	
Remark: the steps (`aclocal`, `automake` and `autoconf`) can be replaced by the single command `autoreconf`.

## Issues

### make[2]: *** No rule to make target 'hsseq.c', needed by 'hsseq.o'.  Stop.

Remove `.deps` folder in `src` folder.
