# Processes
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','c':'','python':''} -->

## C

Replace current process by a system command:
```c
const char* const args[] = {"-h"}
if (execvp("foo", args))
```

## C++

 * [C++ Signal Handling](https://www.tutorialspoint.com/cplusplus/cpp_signal_handling.htm).
 * [std::signal](https://en.cppreference.com/w/cpp/utility/program/signal).

Run system command and get output:
```cpp
std::string output;
std::array<char, 128> buffer;

std::unique_ptr<FILE, decltype(&pclose)> pipe(popen("my_cmd",
                                                    "r"), pclose);
if (! pipe)
  throw std::runtime_error("popen() failed!");
while (fgets(buffer.data(), static_cast<int>(buffer.size()), pipe.get()) != nullptr)
  output += buffer.data();
```

## Python

### subprocess

 * [subprocess — Subprocess management](dev/languages/python_libs/subprocess.md).

Get the stdout and stderr of a command:
```python
subprocess.getoutput('ls /bin/ls')
```

Run a command and get the output:                                               
```python                                                                       
import subprocess                                                               
batcmd="dir"                                                                    
result = subprocess.check_output(batcmd, shell=True)                            
```                                                                             
OR
```python
proc = subprocess.Popen(["myprg", "arg1", "arg2"], stdout=subprocess.PIPE)
exit_code = proc.wait()
assert exit_code == 0
(stdout, stderr) = proc.communicate()
# OR
lines = proc.stdout.readlines()
```
OR with the `with` statement:
```python
with subprocess.Popen(["myprg", "arg1", "arg2"], stdout=subprocess.PIPE) as proc:
    exit_code = proc.wait()
    assert exit_code == 0
    (stdout, stderr) = proc.communicate()
```

## Shell / system

About signals, see:
  * `man 7 signal`.
  * [LINUX Signals](https://faculty.cs.niu.edu/~hutchins/csci480/signals.htm).
  * [Signal (IPC)](https://en.wikipedia.org/wiki/Signal_(IPC)).
  * [An Overview of Linux Exit Codes](https://www.agileconnection.com/article/overview-linux-exit-codes). Exit codes of signal are 128 (0x80) + signal number

### choom

Display and adjust score of Out-of-Memory killer.

Display score:
```sh
choom -p 12345
```

Prevent a process of being killed:
```sh
sudo choom -p 12345 -n -1000
```
### htop

 * [Understanding and using htop to monitor system resources](https://www.deonsworld.co.za/2012/12/20/understanding-and-using-htop-monitor-system-resources/).

A top app with %CPU for each core and colors.

Install on CentOS:
```sh
yum -y install epel-release
yum update
yum -y install htop
```
### killall

Send signal to processes by name.
Not portable.

Fix string:
```bash
killall -HUP myprocess
```

Regex:
```bash
killall -r -HUP 'myproc[0-9]*'
```
### kill

 * See `man 7 signal` for an overview of signals.

Send a signal to a process by name or number:
```sh
kill -HUP 12648
kill -HUP myproc
```

Default signal is TERM:
```sh
kill myproc
```

List available signals (names and numbers):
```sh
kill -l
```

Sends QUIT, TERM and KILL with 1000ms waiting time between each:
```sh
kill --verbose --timeout 1000 TERM --timeout 1000 KILL --signal QUIT 12345
```
### lsof

List applications that have open a certain file:
```bash
lsof | grep MYFILE
```

List applications using network:
```bash
lsof | grep TCP
```
### mpstat
<!-- vimvars: b:markdown_embedded_syntax={'sh':'bash'} -->

Get CPU usage.

Install on Arch Linux:
```sh
pacman -S sysstat
```

Install on Debian:
```sh
apt install sysstat
```

Display global stat:
```sh
mpstat
```

By default `mpstat` gives stastics starting from the moment the computer was
started up.
To get statistics from the present moment one must provide an interval in
seconds:
```sh
mpstat 2
```
In this format, mpstat never stops running. To make it stop after a number of
time, give it a cound, after which it will print an average of all measures:
```sh
mpstat 2 5
```

Display stats on all CPUs:
```sh
mpstat -P ALL
```
### pgrep

Search for processes.

Example:
```sh
pgrep myprog
```
### pkill

Send signal to processes by name.
Portable?

Use regexpattern by default:
```bash
pkill -HUP [sn]mbd
```
### progress

Monitors progress of Coreutils commands (i.e.: mv, cp, tar, gzip, rsync, grep, md5sum, sha256sum, xz, 7z, etc.).

Install on Archlinux:
```sh
pacman -S progress
```

Print progress:
```sh
progress
```

Running with watch to monitor constantly:
```sh
watch progress
```
### ps

List every process (standard syntax):
```sh
ps -e
ps -ef # + user, full path
ps -eF # + user, fullpath, memory size
ps -ely
```

List every process (BSD syntax):
```sh
ps ax
```
Option          | Description
--------------- | ----------------------------------------------------
 -a             | Display also processes of all users.
 -x             | display also processes which do not have a controlling terminal.
 -U username    | Display the processes owned by the specified user.

For monitoring memory usage of processes:
```sh
while true ; do ps -o %mem,rss,sz,vsize,fname ; sleep 1 ; done
```

Get time of execution of a process:
```sh
ps -p 40769 -o etime
```
### pstree

Install on CentOS:
```sh
yum install -y psmisc
```

### reptyr

 * [reptyr](https://github.com/nelhage/reptyr).

Reparent a process, for instance for moving a process from the jobs list of a
shell session into a tmux session:
 * CTRL-Z inside the shell
 * bg
 * `jobs -l` to identify the process
 * `disown 12345` to detach the process from the shell
 * Inside `tmux`: `reptyr 12345`

Does not work on Archlinux:
```
Unable to attach to pid 625711: Operation not permitted
The kernel denied permission while attaching. If your uid matches
the target's, check the value of /proc/sys/kernel/yama/ptrace_scope.
For more information, see /etc/sysctl.d/10-ptrace.conf
```

### strace
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Trace system calls and signals.

```sh
strace mycommand
```
### time

Print time used by a process:
```sh
time myprg
```

Print custom values:
```sh
time -f "somevalue	%e	%U	%S	%P	%M	%t	%I	%O" myprg
```
 * `%e` : real time
 * `%U` : user time
 * `%S` : system time
 * `%P` : % CPU
 * `%M` : Max memory
 * `%t` : Avg memory
 * `%I` : number of blocks of 512 bytes read.
 * `%O` : number of blocks of 512 bytes written.

Output in a file:
```sh
time -o myfile,txt myprg
```

Append to a file:
```sh
time -a -o myfile.txt myprg
```
### top


### watch

Runs a command periodically.

Example watching updates of a file (e.g.: when copying a file or monitoring an output progress) every two seconds (default):
```sh
watch ls -l /my/file
```

Watch every 500ms:
```sh
watch -n 0.5 ls -l /my/file
```
