(define (factorielle n) ; main function
	(define (iterer n acc) ; recursive function, note the use of this extra function with an accumulator
		(if (<= n 1)
			acc ; n == 1 --> return accumulator
			(iterer (- n 1) (* acc n)))) ; n > 1 --> tail recursion call
	(iterer n 1)) ; first call to recursive function
