(define (factorielle n)
	(cond ((= n 0) 1)
		(else (* n (factorielle (- n 1)))))) ; the use of operator mult (*) cancels the tail recursion, since the stack must be used.
