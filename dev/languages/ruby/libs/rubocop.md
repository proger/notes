# rubocop

 * [RuboCop](https://rubocop.org/).

Linter for Ruby.

Disable temporarily a warning:
```ruby
# rubocop:disable Metrics/MethodLength
...
# rubocop:enable Metrics/MethodLength
```

To disable permanently a warning, create a `.rubocop.yml` file and put the
following lines:
```yaml
---
Layout/ArgumentAlignment:
  Enabled: false
```
To use such a file, all fields must be set, otherwise a warning about all unset
values will be printed.

You can also create any file, like `foo.yml` and pass it on command line.
It will have the advantage on applying on top of default values:
```sh
rubocop -c foo.yml ...
```
