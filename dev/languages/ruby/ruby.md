# Ruby
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','ruby':''} -->

 * [Documentation](https://ruby-doc.org/).

## Installation

macOS: Ruby is already installed, but you can install a more recent version using brew:
```sh
brew install ruby
```

## Packages (Ruby gems)

See `gem`.

## Running

Interactive session:

```sh
irb
```

## Style conventions

 * [Ruby style guide](https://github.com/bbatsov/ruby-style-guide).

For indentation, use 2 spaces, no tabs.

## Types & variables

### Get or test type

Test type
```ruby
if myvar.kind_of?(String)
    do_something
end
```

### Global variables

Declare a global variable:
```ruby
$myglobalvar = 1
```

### Nil

Set a variable to nil:
```ruby
myvar = nil
```

Test if a variable is set to nil:
```ruby
if myobj.nil?
  do_something
end
```

### Booleans

```ruby
v = true
v = false
```

### Strings

Put in lowercase:
```ruby
s = 'My String'.downcase()
```

Concatenate two strings:
```ruby
s = s1 + s2
```

Test string equality:
```ruby
s == 'bof'
```
or
```ruby
s.eql? 'bof'
```

String interpolation:
```ruby
s = "Using some var inside a literal string: #{some.other.string.var}."
t = "My array value: #{x[:a]} #{x['b']}"
```

Split string into array:
```ruby
s.split(',')
s.split(/[a-z]/)
```

Regex match:
```ruby
s = 'My string'[/^.* (.*)$/, 1]
```

Regex replace:
```ruby
"my String".gsub(/ABC_([a-z]+)/, '\1')
```

### Symbols

A symbol:
```ruby
:mysymb
```

### Arrays

Create an array:
```ruby
myarray = ['1', '2', 'a']
```

Check if a value is inside an array:
```ruby
is_in_array = ['zap', 'hop', 'plouf'].include? 'zap'
```

Push in an array:
```ruby
myarray.push('x', 'y')
```

Get length:
```ruby
myarray.length()
```

Build an arrau with the same value repeated n times:
```ruby
[myvalue] * n
```

## Built-in constants

Constant        | Description
---             | ---
`__dir__`       | Folder of current code file.
`__FILE__`      | Name of current code file. Test is against `$PROGRAM_NAME` to know if we are running as a program or loaded as a module.
`$PROGRAM_NAME` | Name of the current executed script.

```ruby
if __FILE__ == $PROGRAM_NAME
    main()
end
```

## Hash / Dictionaries

Define a dictionary with strings as keys:
```ruby
mydict = {
    'a' => 1,
    "b" => 2
}
```

Build a dictionary with two lists:
```ruby
mydict = Hash[keys.zip values]
```

Access a key:
```ruby
mydict['mykey']
```

Test if a key exists:
```ruby
mydict.key?('mykey')
```

Define a dictionary with symbols as keys:
```ruby
mydict = {
    :a => 1,
    b: 2,
    'c': 3,
    "d": 4
}

Setting a new value:
```ruby
mydict["mykey"] = "myval"
```

## Environment variables

Environment variables are stored inside `ENV` dictionary.

```ruby
myenvvar = ENV['MY_ENV_VAR']
```

Export an env var: 
```ruby
ENV['MY_VAR'] = 'my value'
```

## Operators

 * [Ruby Operators](https://www.tutorialspoint.com/ruby/ruby_operators.htm).

Ternary operator:
```ruby
x = a == 1 ? a : a * 2  
```

And:
```ruby
myvar1 and myvar2
myvar1 && myvar2
```

Or:
```ruby
myvar1 or myvar2
myvar1 || myvar2
```

Not:
```ruby
not myvar
! myvar
```

## Statements

### If

```ruby
if somecond
  do_something
elsif someothercond
  do_something_else
else
  do_something_else_else
end
```

### Loop

For loop:
```ruby
s = 0
for i in myarray
    s += i
end
```

Ruby style loop:
```ruby
myarray.each do |i|
    s += i
end
```
or
```ruby
myarray.each { |i| s += i }
```

Loop on a hash:
```ruby
myhash.each do |key, value|
end
```

### Logical operators

 * [Precedence](https://ruby-doc.org/3.2.2/syntax/precedence_rdoc.html).

```ruby
enabled = ! a.nil? and a == 5
```

### Function

Simple function:
```ruby
def envvar_enabled(x)
  x + 5
end
```
The returned value is the result of the last evaluated statement.

Explicit returned value:
```ruby
def envvar_enabled(x)
  x + 5
  return x
end
```

Default value:
```ruby
def foo(x=123)
    ...
end
```

Using the keyword syntax, we pass the arguments in the order we want:
```ruby
def foo(a:, b:, c:123)
    ...
end

foo(b:"abc", a:456)
```

### Blocks: do/end and curly braces

`do`/`end` and curly braces are two types of block statements.
`{}` is preferred for one line block, and `do`/`end` for multiple lines block.

They have different different behaviours.

Example where both block types return the same result `[2,3,4]`:
```ruby
[1,2,3].map { |k| k + 1 }
[1,2,3].map do |k| k + 1; end
```
However `{}` block will always be resolved first, starting from the most inner
ones, which is not the case of a `do`/`end` block:
```ruby
puts [1,2,3].map { |k| k + 1 } # Puts is executed after resolution of the right part and prints the elements of the resulted array.
puts [1,2,3].map do |k| k + 1; end # Puts is executed first on `[1,2,3].map` and returns an enumerator object.
puts([1,2,3].map do |k| k + 1; end) # Does the same as the curly braces version.
```

## system

Call program:
```ruby
system "myprog", "arg1", "arg2"
```


## puts

```ruby
puts('My text')
```

## Dir

List files matching pattern:
```ruby
Dir.glob('*.txt') { |file| do.something(file) }
```

## File

 * [File](https://ruby-doc.org/core-2.5.1/File.html).

Join paths:
```ruby
File.join("my", "path", "to", "a", "file")
```

Create a file:
```ruby
File.open('myfile.txt', 'w') { |file| file.write("mytext") }
```

Get dirname:
```ruby
dirs = File.dirname(mypath)
```

Open a file for writing:
```ruby
```

## require

Use `require` (as for library), to load a file only once even if `require` is called more than once:
```ruby
require 'my_file' # File `my_file.rb` will be loaded.
```

## load

`load` includes a file each time it is called:
```ruby
load 'my_file.rb'
```

## OOP

 * [Exploring Object Oriented Programming with Ruby](https://reintech.io/blog/exploring-object-oriented-programming-with-ruby).
 * [class Class](https://ruby-doc.org/3.2.2/Class.html).

## Exception

Raise a runtime error (`RuntimeError` class):
```ruby
raise "my error"
```

Raise a standard error (may be rescued):
```ruby
raise StandardError.new "This is an exception"
```

Raise an exception:
```ruby
raise Exception.new "This is an exception"
```
