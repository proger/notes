(*
This script toggles between two audio outputs in the "Sound" pane in "System Preferences" and adjusts the volume. 
Modified from a script at http://forums.macosxhints.com/showthread.php?t=45384 
to add volume control and GUI scripting detection. --David Battino, www.batmosphere.com.
USES GUI SCRIPTING; "ENABLE ASSISTIVE DEVICES" OPTION MUST BE CHECKED IN THE "UNIVERSAL ACCESS" PREFERENCE PANE
*)

tell application "System Preferences"
     activate
     set current pane to pane "com.apple.preference.sound"
end tell

tell application "System Events"
  if UI elements enabled then
    try
    tell application process "System Preferences"
    tell tab group 1 of window "Sound"
    click radio button "Output"
    if (selected of row 3 of table 1 of scroll area 1) then --headset is selected
      set selected of row 1 of table 1 of scroll area 1 to true
      set deviceselected to "Line Out"
      set verbal_description to "Line out."
      tell application "Finder"
        set volume 7
      end tell
    else
      set selected of row 3 of table 1 of scroll area 1 to true
      set deviceselected to "Logitech USB Headset"
      set verbal_description to "Headset."
      tell application "Finder"
        set volume 2
      end tell
    end if
  end tell
end tell

tell application "System Preferences" to quit
  tell me to activate
    say verbal_description using "Trinoids"
    display dialog "Audio output is now..." & return & return & "* " & deviceselected buttons {"Rock on"} default button 1 giving up after 3
    on error
  tell me to activate
    display dialog "Please plug in the headset." buttons {"Whoops!"} default button 1
 end try
else --GUI scripting is disabled
tell application "System Preferences"
activate
set current pane to pane "com.apple.preference.universalaccess"
end tell
display dialog "Please check the box called \"Enable access for assistive devices.\"" buttons {"Okay"} with icon 1 default button 1
end if
end tell
