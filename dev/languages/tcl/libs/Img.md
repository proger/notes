# Img
<!-- vimvars: b:markdown_embedded_syntax={'tcl':''} -->

If you want to manipulate image formats like TIFF, JPEG, etc., don't forget to
load the following package:
```tcl
package require Img
```

Create an image object:
```tcl
image create photo imgobj -file $imgfile
```

Get image width & height:
```tcl
image width imgobj
image height imgobj
```
