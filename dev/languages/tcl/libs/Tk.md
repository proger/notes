# Tk
<!-- vimvars: b:markdown_embedded_syntax={'tcl':''} -->

Tk is a Tcl package for GUI.

Syntax for creating a widget/window:
```tcl
type variableName arguments options
```

Widget/Window naming convention:
	.                 Root window.
	.mybutton         A button inside the root window.
	.frame1.button1   Button button1 is inside frame frame1.

## Window manager

Set top-level window title:
```tcl
wm title . "My Application"
```

Set top-level window size:
```tcl
wm geometry . 300x200
```
