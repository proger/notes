# Tcl
<!-- vimvars: b:markdown_embedded_syntax={'tcl':''} -->

TCL stands for Tool Command Language, and thus everything is oriented toward
command definiton and evaluation.

Everything that is between double quotes (`""`) or braces (`{}`) is evaluated
as a single argument for a command.
```tcl
puts "Hello World"
```

Inside quotes, substitutions of variables are allowed.
```tcl
puts "The result is: $x"
```

But inside braces, substitutions of variables are disabled.
```tcl
puts {The result is: $x} ; # writes litteral '$x'
```

A command placed inside square brackets (`[]`) is evaluated and replaced by its
value, which is then used as a single argument for another command.

## Run

The interpreter is called tclsh (and not tcl).

The shebang for Tcl is:
```tcl
#!/usr/bin/env tclsh
```

The shebang for Tk/wish is:
```tcl
#!/usr/bin/env wish
```
Or if you prefer using tclsh:
```tcl
#!/usr/bin/env tclsh
package require Tk
```

## Comments and separators

Comments are set using `#`.

Commands are separated by new lines or semicolons.

Warning: comments are evaluated (removed) after grouping of commands
(evaluation of braces), so pay attention to balanced braces inside comments !!!
```tcl
# if {$condition} {
	    puts "condition met!"
# }
```

Comments can be placed where a command is expected, but not a data:
```tcl
if $condition {# good place
	switch -- $x {
		#bad_place {because switch tests against it}
		some_value {do something; # good place again}
	}
}
```

To comment multiple line of codes, use `if 0`:
```tcl
if 0 {
	puts "This code will not be executed"
	This block is never parsed, so can contain almost any code
	- except unbalanced braces :)
}
```

Don't forget to put a semicolon before a comment at the end of a code line:
```tcl
puts "this is the command" ;# that is the comment
```

## expr

Evaluate mathematical expression:
```tcl
expr acos(-1)
```

Performance tip: enclosing the arguments to expr in curly braces will result in
faster code. So do:
```tcl
expr {$i * 10}
```
instead of simply:
```tcl
expr $i * 10
```

SECURITY BREACHES: letting expr (but also: if, while, ...) evaluates variables
directly can lead to security breaches:
```tcl
set userinput {[puts DANGER!]}
expr $userinput == 1 ; # evaluates [puts DANGER!]
```
So always surround expr argument with braces:
```tcl
expr {$userinput == 1}
```

## File system

List files in a directory:
```tcl
set img_files [glob -nocomplain -directory $dir *.{tif,tiff}]
```

## for

Loop on an integer range:
```tcl
for {set i 0} {$i < 5} {incr i} {...}
```

## if

if:
```tcl
if {$x < 0} {set x 0}
```

if / else:
```tcl
if {$x == 5} {
	set y 2
} else { ; # be careful to write the "else" just after the curly brace
	set y 10
}
```

elseif:
```tcl
if {$x == 5} {
	set y 2
} elseif {
	set y 4
} else {
	set y 10
}
```

## Packages

The list of paths searched for libraries:
```tcl
puts $auto_path
```

## print

Print text:
```tcl
puts Hello
puts "My var =  $myvar \n"
```

Specifying output stream:
```tcl
puts stdout {Hello, World!}
```

## Strings

Compute the length of a string:
```tcl
string length blabla
```

To split a string in lines:
```tcl
split $str "\n"
```

Append to a string:
```tcl
append myvar blabla zozo a b c
```

Translate characters:
```tcl
string map "food" { f p d ll oo u } ; # --> pull
```

## Variables

Set:
```tcl
set X "This is a string"
set Y 1.24
```

Use variable:
```tcl
puts "blablabla $Y"
```

## Version

Print version:
```tcl
puts $tcl_version
```

Get more precised version information:
```tcl
info patchlevel
```
