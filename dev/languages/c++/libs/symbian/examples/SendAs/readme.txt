SendAs v1.0 Example Application for the Series 60 Platform
==========================================================
Copyright (C) 2003 Nokia Corporation


- This example code shows how to use the CSendAs class to create draft messages and store them into the Drafts folder on a Series 60 device.

- The use of CSendAppUi class is demonstarated with SMS messages. CSendAppUi is an easy-to-use API, offering means to create messages in similar fashion to CSendAs. The main difference is that CSendAppUi opens the created draft message in an appropriate message editor, and the message can be sent immediately.

These two functionalities are offered to the user of this example application in the application menu. Otherwise the example application UI is irrelevant to the functionality of this example.

At the time of creation, this example was tested to be functional on Nokia 7650. The ZIP package includes source code and a ready, compiled SIS file.


- end of file -

