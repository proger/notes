/*

        SAEX.CPP - source file for SAEX application
        

*/

 
#include <txtrich.h>                // for CRichText
#include <eikenv.h>                 // for CEikonEnv 
#include <smut.h>                   // for message type UIDs
#include <sendas.h>                 // for CSendAs
#include <msvuids.h>                // for Message type UIDs
#include <miutset.h>                // for KUidMsgTypeSMTP
#include <AknQueryDialog.h>         // for CAknTextQueryDialog
#include <mtmuids.h>                // for KUidMtmQueryCanSendMsg etc. capability query UIDs
#include <txtfmlyr.h>               // for CParaFormatLayer, CCharFormatLayer 
#include <sendui.h>                 // for CSendAppUi
#include <mmsconst.h>               // for KUidMsgTypeMultimedia

#include "saex.h"                   // own definitions
#include "saex.hrh"                 // own resource header
#include <saex.rsg>


// this is the content of the message
_LIT(KSAEXTag, "SAEX");



// Own constants
const TUid KUidSAEX = { 0x101F402D };       // SAEX application UID 
const TInt KTagLength = 4;                  // length of our message tag
const TInt KMaxTelephoneNumberLength = 30;  // maximum length for a gsm number
const TUid KSAEXViewId = { 1 };             // ID of SAEX view


//
// CSAEXContainer
//

/*
-------------------------------------------------------------------------------

    CSAEXContainer::ConstructL()

    Description: 2nd phase Constructor.

    Return value: N/A

-------------------------------------------------------------------------------
*/
void CSAEXContainer::ConstructL()
    {
    CreateWindowL();
    }


/*
-----------------------------------------------------------------------------

    CSAEXContainer::Draw()

    Simple Draw method.

-----------------------------------------------------------------------------
*/
void CSAEXContainer::Draw(const TRect& /*aRect*/) const
    {
        CWindowGc& gc = SystemGc();
        gc.Clear();
    
        // Draw text "SAEX for Series 60"
        gc.SetPenColor(KRgbBlack); 
        const CFont* fontUsed = iEikonEnv->TitleFont();
        gc.UseFont(fontUsed);

        TInt baseline = (Rect().Height() / 2) - fontUsed->AscentInPixels()*2; // set text 2 * text ascent abowe the centerline
        TInt margin=0; // margin is zero so that the text will be cenetred

        _LIT(K1stLine,"SAEX");
        gc.DrawText(K1stLine,Rect(),baseline,CGraphicsContext::ECenter, margin);
        
        baseline = (Rect().Height() / 2) + fontUsed->AscentInPixels()*2; // 2nd line goes below the centerline

        _LIT(K2ndLine,"for Series 60");
        gc.DrawText(K2ndLine,Rect(),baseline,CGraphicsContext::ECenter, margin);
    }

//
// CSAEXAppView
//

/*
-----------------------------------------------------------------------------

    CSAEXAppView::NewL()

    2nd phase construction.

    Return values:      CSAEXAppView*

-----------------------------------------------------------------------------
*/
CSAEXAppView* CSAEXAppView::NewL()
    {
    CSAEXAppView* self = NewLC();
    CleanupStack::Pop(); // self
    return self;
    }

/*
-----------------------------------------------------------------------------

    CSAEXAppView::NewLC()

    2nd phase construction.

    Return values:      CSAEXAppView*

-----------------------------------------------------------------------------
*/
CSAEXAppView* CSAEXAppView::NewLC()
    {
    CSAEXAppView* self = new(ELeave) CSAEXAppView();
    CleanupStack::PushL(self);
    self->ConstructL();
    return self;
    }

/*
-----------------------------------------------------------------------------

    CSAEXAppView::CSAEXAppView()

    C++ constructor

-----------------------------------------------------------------------------
*/
CSAEXAppView::CSAEXAppView()
    {
    }

/*
-----------------------------------------------------------------------------

    CSAEXAppView::ConstructL()

    2nd phase constructor.

    Return values:      CSAEXAppView*

-----------------------------------------------------------------------------
*/
void CSAEXAppView::ConstructL()
    {
    }

/*
-------------------------------------------------------------------------------

    ~CSAEXAppView()

    Description: Destructor.

    Return value: N/A

-------------------------------------------------------------------------------
*/
CSAEXAppView::~CSAEXAppView()
    {
    if(iContainer)
        AppUi()->RemoveFromStack(iContainer);
    }

/*
-------------------------------------------------------------------------------

    CSAEXAppView::Id()

    Description: Returns the id of the view object.

    Return value: TUid

-------------------------------------------------------------------------------
*/
TUid CSAEXAppView::Id() const
    {
    return KSAEXViewId;
    }

/*
-------------------------------------------------------------------------------

    CSAEXAppView::DoActivateL()

    Description: Activate this view.

    Return value: N/A

-------------------------------------------------------------------------------
*/
void CSAEXAppView::DoActivateL(const TVwsViewId& /*aPrevViewId*/, TUid /*aCustomMessageId*/, const TDesC8& /*aCustomMessage*/ )
    {
    if (!iContainer) // container hasn't been created yet
        {
        // Then construct the UI components
        iContainer = new(ELeave) CSAEXContainer;             
        iContainer->ConstructL();             // Construct a view control
        iContainer->SetRect(ClientRect());    // Sets view control's extent to the space available
        }

    iContainer->ActivateL();                  // Activate the view control
    }


/*
-------------------------------------------------------------------------------

    CSAEXAppView::DoDeactivate()

    Description: Deactivate this view.

    Return value: N/A

-------------------------------------------------------------------------------
*/
void CSAEXAppView::DoDeactivate()
    {
    if (iContainer)
        {
        delete iContainer;
        iContainer = NULL;
        }
    }


//
// CSAEXAppUi
//

/*
-----------------------------------------------------------------------------

  CSAEXAppUi::ConstructL()                          
  
  2nd phase constructor

-----------------------------------------------------------------------------
*/
void CSAEXAppUi::ConstructL()
    {

    BaseConstructL();                                   // init this AppUi with standard values

    iRecipient=HBufC::NewL(KMaxTelephoneNumberLength);  // for recipient sms number
    iRecipient->Des() = _L("5551234567");                             

    // Create SendUi
    iSUI = CSendAppUi::NewL(ESAEXCmdSendUi); 

    // create and setup sendAs
    iSendAs = CSendAs::NewL(*this);


    // Series60 view launching
    CSAEXAppView* view = CSAEXAppView::NewLC(); 
    AddViewL(view);                     // add created view to this AppUi
    ActivateLocalViewL( view->Id() );   // activate view
    CleanupStack::Pop(); // view
    }

/*
-----------------------------------------------------------------------------

    CSAEXAppUi::~CSAEXAppUi()

    Destructor.

-----------------------------------------------------------------------------
*/
CSAEXAppUi::~CSAEXAppUi()
    {  
    delete iRecipient;

    if(iSUI)
        delete iSUI;

    if(iSendAs)
        delete iSendAs;
    }



/*
-----------------------------------------------------------------------------

    CSAEXAppUi::HandleCommandL(TInt aCommand)

    Handle the commands from CBA and menu items

-----------------------------------------------------------------------------
*/
void CSAEXAppUi::HandleCommandL(TInt aCommand)
    {
    switch (aCommand)
        {
    case ESAEXCmdSendUi:
        SendUI();    
        break;
    case ESAEXCmdCreate:
        CmdCreateL();
        break;

    case EAknSoftkeyExit:
    case EClose:
        CmdExitL();
        break;
    default:
        break;
        }
    }

/*
-----------------------------------------------------------------------------

    CSAEXAppUi::SendUI()

    Demonstrate a simple usage of the CSendAppUi interface. This will
    open the default SMS editor with the given values (body text).

-----------------------------------------------------------------------------
*/
void CSAEXAppUi::SendUI()
    {
    if(iSUI)
        {
        CParaFormatLayer* paraFormatLayer = CParaFormatLayer::NewL();
        CleanupStack::PushL(paraFormatLayer);

        CCharFormatLayer* charFormatLayer = CCharFormatLayer::NewL();
        CleanupStack::PushL(charFormatLayer);

        CRichText* messageBodyContent = CRichText::NewL(paraFormatLayer, charFormatLayer); 
        CleanupStack::PushL(messageBodyContent);

        messageBodyContent->InsertL(0, KSAEXTag);

        iSUI->CreateAndSendMessageL(KUidMsgTypeSMS, messageBodyContent);
        CleanupStack::PopAndDestroy(3); // messageBodyContent, charFormatLayer, paraFormatLayer
        }
    }

/*
-----------------------------------------------------------------------------

    CSAEXAppUi::CapabilityOK()

    From MSendAsObserver. Checks back if the queried capabilities are what
    was expected. If checking more than one cap, aCapability could be handled
    in a switch sentence.

    Return values: TBool 

-----------------------------------------------------------------------------
*/
TBool CSAEXAppUi::CapabilityOK(TUid /*aCapability*/, TInt /*aResponse*/)
    {
    return ETrue;
    }

/*
-----------------------------------------------------------------------------

    CSAEXAppUi::CreateNewSMSMessageL()

    Creates a new SMS message using the SendAs API.

    Return values: N/A

-----------------------------------------------------------------------------
*/
void CSAEXAppUi::CreateNewSMSMessageL()
    {
    
    delete iSendAs;
    iSendAs = CSendAs::NewL(*this);
    if(iSendAs)
        {
        // Using SMS MTM
        iSendAs->SetMtmL(KUidMsgTypeSMS);
        
        // Set SC to use
        if(iSendAs->AvailableServices().Count()<1)
            {
            CEikonEnv::Static()->InfoMsg(_L("No SMS services available."));
            User::Leave(KErrNotFound);
            }

        // using default SC (same as not setting any service)
        iSendAs->SetService(0); // you can set the service with the service index


        // Query message capabilities
        User::LeaveIfError(iSendAs->QueryMessageCapability(KUidMtmQueryCanSendMsg, EFalse));
        User::LeaveIfError(iSendAs->QueryMessageCapability(KUidMtmQueryMaxBodySize, ETrue));


        // Create a new message into the Drafts folder (you can specify a folder as a param, Drafts is the default)
        iSendAs->CreateMessageL();

        iSendAs->AddRecipientL( iRecipient->Des() );

        // insert message body text (ClientMtm::Body() gives a CRichText object)
        iSendAs->ClientMtm().Body().InsertL(0, KSAEXTag);

        User::LeaveIfError(iSendAs->ValidateMessage());
        iSendAs->SaveMessageL(ETrue);               // message is saved into Drafts
        }
    }

/*
-----------------------------------------------------------------------------

    CSAEXAppUi::CreateNewMMSMessageL()

    Creates a new MMS message using the SendAs API.

    Return values: N/A

-----------------------------------------------------------------------------
*/
void CSAEXAppUi::CreateNewMMSMessageL()
    {
    delete iSendAs;
    iSendAs = CSendAs::NewL(*this);
    if(iSendAs)
        {
        // Using MMS MTM
        iSendAs->SetMtmL(KUidMsgTypeMultimedia);

        // Set SC to use
        if(iSendAs->AvailableServices().Count()<1)
            {
            CEikonEnv::Static()->InfoMsg(_L("No MMS services available."));
            User::Leave(KErrNotFound);
            }

        iSendAs->CreateMessageL();

        iSendAs->AddRecipientL( iRecipient->Des() );
        iSendAs->SetSubjectL( KSAEXTag ); 
        
        // add attachment(s) here

        User::LeaveIfError(iSendAs->ValidateMessage());
        iSendAs->SaveMessageL(ETrue); // message is saved into Outbox(drafts)
        }
    }

/*
-----------------------------------------------------------------------------

    CSAEXAppUi::CreateNewSMTPMessageL()

    Creates a new SMTP message using the SendAs API.

    Return values: N/A

-----------------------------------------------------------------------------
*/
void CSAEXAppUi::CreateNewSMTPMessageL()
    {
    delete iSendAs;
    iSendAs = CSendAs::NewL(*this);
    if(iSendAs)
        {
        // Using SMTP MTM
        iSendAs->SetMtmL(KUidMsgTypeSMTP);

        // Set SC to use
        if(iSendAs->AvailableServices().Count()<1)
            {
            CEikonEnv::Static()->InfoMsg(_L("No SMTP services available."));
            User::Leave(KErrNotFound);
            }

        iSendAs->CreateMessageL();

        iSendAs->AddRecipientL( iRecipient->Des() );
        iSendAs->SetSubjectL( KSAEXTag ); 

        // insert message body text (ClientMtm::Body() gives a CRichText object)
        iSendAs->ClientMtm().Body().InsertL(0, KSAEXTag);     

        User::LeaveIfError(iSendAs->ValidateMessage());
        iSendAs->SaveMessageL(ETrue); // message is saved into Outbox(drafts)
        }
    }


/*
-----------------------------------------------------------------------------

    CSAEXAppUi::CmdSendL()

    Handle send command  
    
-----------------------------------------------------------------------------
*/
void CSAEXAppUi::CmdCreateL()
    {
    // set up a new messages and save them into Drafts folder
    CreateNewSMSMessageL();
    CreateNewMMSMessageL();
    CreateNewSMTPMessageL();

    }


/*
-----------------------------------------------------------------------------

    CSAEXAppUi::CmdExitL()
    
    Exit application
  
-----------------------------------------------------------------------------
*/

void CSAEXAppUi::CmdExitL()
    {
    CBaActiveScheduler::Exit(); // Call the CBaActiveScheduler's Exit 
                                // function that stops the application's thread
                                // and destroys it.
    }




//
// CSAEXDocument
//

/*
-----------------------------------------------------------------------------

    CSAEXDocument::NewL(

    2nd phase construction.

-----------------------------------------------------------------------------
*/
CSAEXDocument* CSAEXDocument::NewL(CEikApplication& aApp)
    {
    CSAEXDocument* self = new(ELeave) CSAEXDocument(aApp);
    CleanupStack::PushL(self);
    self->ConstructL();
    CleanupStack::Pop(); //self.
    return self;
    }

/*
-----------------------------------------------------------------------------

    CSAEXDocument::CSAEXDocument()

    C++ constructor

-----------------------------------------------------------------------------
*/
CSAEXDocument::CSAEXDocument(CEikApplication& aApp)
    : CEikDocument(aApp)
    {
    }

/*
-----------------------------------------------------------------------------

    CSAEXDocument::ConstructL()

    2nd phase constructor.

-----------------------------------------------------------------------------
*/
void CSAEXDocument::ConstructL()
    {    
    }

/*
-----------------------------------------------------------------------------

    CSAEXDocument::CreateAppUiL()

    Create new CSAEXAppUi object

    Return values:      CEikAppUi*

-----------------------------------------------------------------------------
*/
CEikAppUi* CSAEXDocument::CreateAppUiL()
    {
    return (new(ELeave) CSAEXAppUi);
    }


//
// CSAEXApplication
//

/*
-----------------------------------------------------------------------------

    CSAEXApplication::AppDllUid()

    Returns application UID of SAEX application

-----------------------------------------------------------------------------
*/
TUid CSAEXApplication::AppDllUid() const
    {
    return KUidSAEX;
    }

/*
-----------------------------------------------------------------------------

    CSAEXApplication::CreateDocumentL()

    Create new application document

    Return values:      CApaDocument*

-----------------------------------------------------------------------------
*/
CApaDocument* CSAEXApplication::CreateDocumentL()
    {
    return (CSAEXDocument::NewL(*this));
    }



//
// Functions for Application Architecture
//

EXPORT_C CApaApplication* NewApplication()
    {
    return (new CSAEXApplication);
    }


//
// DLL entry point
//

GLDEF_C TInt E32Dll(TDllReason)
    {
    return KErrNone;
    }
