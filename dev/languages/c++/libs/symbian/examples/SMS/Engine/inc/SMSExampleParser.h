/*
* ============================================================================
*  Name     : CSMSExampleParser from SMSExampleParser.h
*  Part of  : SMSExample
*  Created  : 08.02.2005 by Forum Nokia
*  Description:
*     Parser that searches for a number from SMS message body.
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#ifndef SMSEXAMPLEPARSER_H
#define SMSEXAMPLEPARSER_H

// System includes 
#include <e32base.h> // CBase

/**
* Parser
*/
class CSMSExampleParser: public CBase
	{
	public:

		/**
		* Create new CSMSExampleParser object
		* 
		* @return a pointer to the created instance of CSMSExampleParser.
		*/
		IMPORT_C static CSMSExampleParser* NewL();

		/**
		* Destructor.
		*/
	    IMPORT_C ~CSMSExampleParser(void);

		/**
		* Parse body and try to find a number.
		* @param aMessage SMS message body.
		* @param aNumber Found number. Searches for exactly 10 digits that are one after 
		*                another(no whitespaces).
		* @return ETrue if found, EFalse otherwise.
		*/
	    IMPORT_C TBool ParseMessage(const TDesC& aMessage, TDes& aNumber);
      
	private:

		/**
		* ConstructL()
		*/
        void ConstructL();

	private:

		/**
		* A constructor.
		*/
		CSMSExampleParser(void);
		
	};

#endif 