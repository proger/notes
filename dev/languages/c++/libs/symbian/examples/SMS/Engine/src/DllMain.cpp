//  EXTERNAL INCLUDES
#include <e32std.h>

// DLL entry point
#ifdef __SERIES60_30__
GLDEF_C TInt E32Dll()
#else
GLDEF_C TInt E32Dll( TDllReason /*aReason*/ )
#endif
	{
	return KErrNone;
	}

//  End of file