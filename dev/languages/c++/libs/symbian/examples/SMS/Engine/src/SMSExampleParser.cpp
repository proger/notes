/*
* ============================================================================
*  Name     : CSMSExampleParser from SMSExampleParser.h
*  Part of  : SMSExample
*  Created  : 12.03.2005 by Forum Nokia
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#include "SMSExampleParser.h"

const TInt KNumberLength = 10;

CSMSExampleParser::CSMSExampleParser(void)
	{
	}
 
EXPORT_C CSMSExampleParser::~CSMSExampleParser(void)
	{
	}

EXPORT_C CSMSExampleParser* CSMSExampleParser::NewL()
	{
	CSMSExampleParser* self = new (ELeave) CSMSExampleParser();
    CleanupStack::PushL(self);
    self->ConstructL();
    CleanupStack::Pop(self);
    return self;
	}
	  
void CSMSExampleParser::ConstructL()
	{
	}

EXPORT_C TBool CSMSExampleParser::ParseMessage(const TDesC& aMessage, TDes& aNumber)
	{
	TLex numberLex = TLex( aMessage );

	// while not end-of-file
	while ( !numberLex.Eos() )
		{
		TChar character = numberLex.Get();

		if ( character.IsDigit() ) 
			{
			aNumber.Append( character );
			}
		else 
			{
			aNumber.Delete( 0, aNumber.Length() );
			}

		// Search for a number which is in format XXXXXXXXXX ( X = [0-9], 
		// whitespaces are not allowed). 
		// For example "0551234567" would return true. 
		// but "055 1234567" would not
		if ( aNumber.Length() == KNumberLength ) 
			{
			return ETrue;
			}
		}

	return EFalse;
	}
