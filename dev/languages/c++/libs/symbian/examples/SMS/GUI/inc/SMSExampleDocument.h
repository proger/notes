/*
* ============================================================================
*  Name     : CSMSExampleDocument from SMSExampleDocument.h
*  Part of  : SMSExample
*  Created  : 08.02.2005 by Forum Nokia
*  Description:
*     Declares document for application.
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#ifndef SMSEXAMPLEDOCUMENT_H
#define SMSEXAMPLEDOCUMENT_H

// INCLUDES
#include <akndoc.h>
   
// CONSTANTS

// FORWARD DECLARATIONS
class  CEikAppUi;

// CLASS DECLARATION

/**
*  CSMSExampleDocument application class.
*/
class CSMSExampleDocument : public CAknDocument
    {
    public: // Constructors and destructor
        /**
        * Two-phased constructor.
        */
        static CSMSExampleDocument* NewL(CEikApplication& aApp);

        /**
        * Destructor.
        */
        virtual ~CSMSExampleDocument();

    public: // New functions

    public: // Functions from base classes
    protected:  // New functions

    protected:  // Functions from base classes

    private:

        /**
        * EPOC default constructor.
        */
        CSMSExampleDocument(CEikApplication& aApp);
        void ConstructL();

    private:

        /**
        * From CEikDocument, create CSMSExampleAppUi "App UI" object.
        */
        CEikAppUi* CreateAppUiL();
    };

#endif

// End of File

