/*
* ============================================================================
*  Name     : CRichTextEditorRTE from SMSExamplerRichTextEditorRTE.h
*  Part of  : SMSExample
*  Created  : 12.03.2005 by Forum Nokia
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

// INCLUDES
#include "SMSExamplerichtexteditorrte.h"
#include <barsread.h> // TResourceReader
#include <smsexample.rsg> // resources

//only for third edition
#include <eikrted.h> // CEikRichTextEditor
#include <txtrich.h> // CRichText


_LIT(KTextLines, "---------------------------");
// ----------------------------------------------------------------------------
// CRichTextEditorRTE::CRichTextEditorRTE(void)
//
// Standard symbian OS 2nd phase constructor
// ----------------------------------------------------------------------------
void CRichTextEditorRTE::ConstructL()
	{
	TResourceReader reader;

	// Construct RichTextEditor from resource
	iCoeEnv->CreateResourceReaderLC(reader, R_RICHTEXTEDITOR_RICH_TEXT_EDITOR);

	ConstructFromResourceL(reader);

	CleanupStack::PopAndDestroy(); // reader

	// Sets that the control has keyboard focus
	SetFocus(ETrue);
	}


// ----------------------------------------------------------------------------
// CRichTextEditorRTE::CRichTextEditorRTE(void)
//
// Constructor
// ----------------------------------------------------------------------------
CRichTextEditorRTE::CRichTextEditorRTE(void)
	{
	}

// ----------------------------------------------------------------------------
// CRichTextEditorRTE::AddCarriageReturnL()
//
// Insert one line break at the end of the text.
// ----------------------------------------------------------------------------
void CRichTextEditorRTE::AddCarriageReturnL()
	{
	CRichText* richText = RichText();
	richText->InsertL(richText->DocumentLength(), CEditableText::ELineBreak);
	}


// ----------------------------------------------------------------------------
// CRichTextEditorRTE::AddTextL(const TDesC& aText)
//
// Draws text using black color.
// ----------------------------------------------------------------------------
void CRichTextEditorRTE::AddTextL(const TDesC& aText)
	{
	CRichText* text = RichText();
	TInt textSize = text->DocumentLength();

	// Interested in color
	iCharacterFormatMask.SetAttrib(EAttColor);
	// Set it to Black
    iCharacterFormat.iFontPresentation.iTextColor = KRgbBlack;
	text->InsertL (textSize, aText);
	// Apply formatting
	text->ApplyCharFormatL(iCharacterFormat, iCharacterFormatMask,
	                       textSize,aText.Length());
	AddCarriageReturnL();
	HandleTextChangedL();

	//Keep displaying the bottom of the screen
	MoveCursorL (TCursorPosition::EFPageDown, EFalse);
	}

// ----------------------------------------------------------------------------
// CRichTextEditorRTE::DrawTextWithoutCarriageL( const TDesC& aText )
//
// Draw text without adding one line break at the end of the text.
// ----------------------------------------------------------------------------
void CRichTextEditorRTE::DrawTextWithoutCarriageL( const TDesC& aText )
	{
	CRichText* text = RichText();
	TInt textSize = text->DocumentLength();

	// Interested in color
	iCharacterFormatMask.SetAttrib(EAttColor);
	// Set it to Black
    iCharacterFormat.iFontPresentation.iTextColor = KRgbBlack;
	text->InsertL (textSize, aText);
	// Apply formatting
	text->ApplyCharFormatL(iCharacterFormat, iCharacterFormatMask,
	                       textSize,aText.Length());
	HandleTextChangedL();
	}


// ----------------------------------------------------------------------------
// CRichTextEditorRTE::DrawTextWithoutCarriageL( const TDesC& aText )
//
//  Sets underline on or off.  This will be applied to text added in AddTextL()
// ----------------------------------------------------------------------------
void CRichTextEditorRTE::SetTextUnderlineOn(TBool aUnderlineOn)
	{
	iCharacterFormatMask.SetAttrib(EAttFontUnderline);
	if (aUnderlineOn)
		{
		iCharacterFormat.iFontPresentation.iUnderline = EUnderlineOn;
		}
	else
		{
		iCharacterFormat.iFontPresentation.iUnderline = EUnderlineOff;
		}
	}

// ----------------------------------------------------------------------------
// CRichTextEditorRTE::DrawLineL()
//
// Draw one line.
// ----------------------------------------------------------------------------
void CRichTextEditorRTE::DrawLineL()
	{
	AddTextL( KTextLines );
	HandleTextChangedL();
	}

// ----------------------------------------------------------------------------
// CRichTextEditorRTE::NewL()
//
// Symbian OS 2 phase constructor.
// ----------------------------------------------------------------------------
CRichTextEditorRTE* CRichTextEditorRTE::NewL()
	{
	CRichTextEditorRTE* self = CRichTextEditorRTE::NewLC();
	CleanupStack::Pop(self);
	return self;
	}

// ----------------------------------------------------------------------------
// CRichTextEditorRTE::NewLC()
//
// Symbian OS 2 phase constructor.
// ----------------------------------------------------------------------------
CRichTextEditorRTE* CRichTextEditorRTE::NewLC()
	{
	CRichTextEditorRTE* self = new (ELeave) CRichTextEditorRTE;
	CleanupStack::PushL(self);
	self->ConstructL();
	return self;
	}

// ----------------------------------------------------------------------------
// TKeyResponse CRichTextEditorRTE::OfferKeyEventL(const TKeyEvent& aKeyEvent,
//		TEventCode aType)
//
// Called by the framework whenever a key event occurs.	Handles scrolling
// events.
// ----------------------------------------------------------------------------
TKeyResponse CRichTextEditorRTE::OfferKeyEventL(const TKeyEvent& aKeyEvent,
                                                TEventCode aType)
	{

    if (aType == EEventKey)
		{
        if (aKeyEvent.iCode == EKeyDownArrow)
            {
            MoveCursorL (TCursorPosition::EFPageDown, EFalse);

            return EKeyWasConsumed;
            }
        else if (aKeyEvent.iCode == EKeyUpArrow)
            {
            MoveCursorL (TCursorPosition::EFPageUp, EFalse);

            return EKeyWasConsumed;
            }
		else
			{
			return CEikRichTextEditor::OfferKeyEventL(aKeyEvent, aType);
			}
        }

    return EKeyWasNotConsumed;
	}

// End of file
