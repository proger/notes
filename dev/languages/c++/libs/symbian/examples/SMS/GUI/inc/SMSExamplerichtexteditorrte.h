/*
* ============================================================================
*  Name     : CRichTextEditorRTE from SMSExamplerRichTextEditorRTE.h
*  Part of  : SMSExample
*  Created  : 12.03.2005 by Forum Nokia
*  Description:
*      RichtTextEditor that prints text on the screen and handles scrolling.
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#ifndef __SMSEXAMPLE_RICHTEXTEDITOR_H
#define __SMSEXAMPLE_RICHTEXTEDITOR_H

// INCLUDES
#include <eikrted.h> // CCEikRichTextEditor
#include <txtfrmat.h> // TCharFormatMask
#include <gdi.h>

// CLASS DECLARATION

/**
*  CRichTextEditorRTE Editor that prints text.
*/
class CRichTextEditorRTE : public CEikRichTextEditor
	{


	public:

		/**
		* Create a CRichTextEditorRTE object.
		* @return Instance of CRichTextEditorRTE.
		*/
		static CRichTextEditorRTE* NewL();

		/**
		* Create a CRichTextEditorRTE object.
		* @return Instance of CRichTextEditorRTE.
		*/
		static CRichTextEditorRTE* NewLC();


	public: // from CoeControl

		TKeyResponse OfferKeyEventL(const TKeyEvent& aKeyEvent, TEventCode aType);

	public: // members

		/**
		* Add one carriage return.
		*/
		void AddCarriageReturnL ();

		/**
		* Print text on the screen.
		* @param aText text to be displayed
		*/
		void AddTextL (const TDesC& aText);

		/**
		* Set text undeline on.
		* @param aUnderlineOn ETrue if underline is used, EFalse otherwise.
		*/
		void SetTextUnderlineOn(TBool aUnderlineOn);

		/**
		* Print text on the screen. No Carriage return at the end.
		* @param aText text to be displayed
		*/
		void DrawTextWithoutCarriageL( const TDesC& aText );

		/**
		* Draw a line that separates log events
		*/
		void DrawLineL();

	private: // Basic two-phase symbian OS constructors

		void ConstructL();

		/**
		* Default contructor.
		*/
		CRichTextEditorRTE();

	private: //data

		// Formatting options for the RichText
		TCharFormatMask iCharacterFormatMask;
		TCharFormat iCharacterFormat;
	};

#endif	// __SMSEXAMPLE_RICHTEXTEDITOR_H
