/*
* ============================================================================
*  Name     : CSMSExampleRTEContainer from SMSExampleRTEContainer.h
*  Part of  : SMSExample
*  Created  : 08.02.2005 by Forum Nokia
*  Description:
*     Declares container control for application. Container for RichTextEditor
*     that is used to log events.
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#ifndef SMS_EXAMPLE_RTE_CONTAINER_H
#define SMS_EXAMPLE_RTE_CONTAINER_H

// INCLUDES
#include <coecntrl.h>
   
// FORWARD DECLARATIONS
class CRichTextEditorRTE;

// CLASS DECLARATION

/**
*  CSMSExampleRTEContainer Container control class.
*  
*/
class CSMSExampleRTEContainer : public CCoeControl, MCoeControlObserver
    {
    public: // Constructors and destructor
        
		/**
		* Create new CSMSExampleRTEContainer object
		*
		* @param aRect Frame rectangle for container.
		* @return a pointer to the created instance of CSMSExampleRTEContainer.
		*/
		static CSMSExampleRTEContainer* NewL(const TRect& aRect);

		/**
		* NewLC
		*/ 
		static CSMSExampleRTEContainer* NewLC(const TRect& aRect);

        /**
        * Symbian OS default constructor.
        * @param aRect Frame rectangle for container.
        */
        void ConstructL(const TRect& aRect);

        /**
        * Destructor.
        */
        ~CSMSExampleRTEContainer();

    public: // New functions

		/**
		* Print text on the screen.
		* @param aText text to be displayed.
		*/
		void DrawTextL( const TDesC& aText );

		/**
		* Print text on the screen. No Carriage return at the end.
		* @param aText text to be displayed.
		*/
		void DrawTextWithoutCarriageL( const TDesC& aText );

		/**
		* Print underlined text on the screen.
		* @param aText text to be displayed.
		*/
		void DrawUnderlinedTextL( const TDesC& aText );

		/**
		* Draw one line.
		*/
		void DrawLineL();

		/**
		* Add one line break.
		*/ 
		void AddCarriageReturnL();

    private: // Functions from base classes

        /**
        * From CoeControl,SizeChanged.
        */
        void SizeChanged();

        /**
        * From CoeControl,CountComponentControls.
        */
        TInt CountComponentControls() const;

        /**
        * From CCoeControl,ComponentControl.
        */
        CCoeControl* ComponentControl(TInt aIndex) const;

        /**
        * From CCoeControl,Draw.
        */
        void Draw(const TRect& aRect) const;

		/**
		* From CoeControl, OfferKeyEventL
		*/
		TKeyResponse OfferKeyEventL(const TKeyEvent& aKeyEvent,TEventCode aType);

		/**
		* From MCoeControlObserver
		* Acts upon changes in the hosted control's state. 
		*
		* @param aControl The control changing its state
		* @param aEventType	The type of control event 
		*/
        void HandleControlEventL(CCoeControl* aControl,TCoeEvent aEventType);
        
        //void HandleScreenDeviceChangedL();
        //void HandleResourceChange();

    private: //data
        
		// Editor
		CRichTextEditorRTE* iRte;
    };

#endif // SMS_EXAMPLE_RTE_CONTAINER
