/*
* ============================================================================
*  Name     : CListboxView from SMSExampleListboxView.h
*  Part of  : SMSExample
*  Created  : 12.03.2005 by Forum Nokia
*  Description: ListBox view.
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#ifndef __SMSEXAMPLE_LISTBOXVIEW_H__
#define __SMSEXAMPLE_LISTBOXVIEW_H__

// INCLUDES
#include <aknview.h>
#include <badesca.h> // CDesC16ArrayFlat
#include <msvstd.h> // TMsvId

// FORWARD DECLARATIONS
class CMarkableListContainer;
class CSmsEngine;
class CLogView;

/**
* ListBox view.
*
* Improvement would be to get a rid of reference to engine
* and implement MVC.
*/
class CListboxView: public CAknView
    {

	public:

		/**
		* Create a CListboxView object.
		* @param aEngine Pointer to CSmsEngine.
		* @param aView1 Pointer to CLogView.
		* @result a pointer to the created instance of CListboxView.
		*/
		static CListboxView* NewL(CSmsEngine* aEngine, CLogView* aView1);

		/**
		* Create a CListboxView object.
		* @param aEngine Pointer to CSmsEngine.
		* @param aView1 Pointer to CLogView.
		* @result a pointer to the created instance of CListboxView.
		*/
		static CListboxView* NewLC(CSmsEngine* aEngine, CLogView* aView1);


		/**
		*  Destroy the object
		*/
		~CListboxView();

	public:

		/**
		*  Set ListBoxMode (delete, copy, move).
		*  @see SMSExample.hrh.
		*  @param aMode ListBox mode.
		*/
		void SetListBoxMode(TInt aMode);

		/**
		* Load source folder messages into
		* ListBox.
		*/
		void InitializeListBoxL();

		/**
		*  Set source folder (from which copy or move is performed).
		*/
		void SetFolderID(TMsvId aFolderID);

		/**
		*  Set target folder (to which folder copy or move is targeted).
		*/
		void SetTargetFolderID(TMsvId aTargetFolderID);

	public: // from CAknView

		/**
		* Identity of this view to the system.
		* @result the ID of view.
		*/
		TUid Id() const;

		/**
		* Handle a user command.
		* @param aCommand Command to be handled.
		*/
		void HandleCommandL(TInt aCommand);

		/**
		* Create the Container class object.
		* @param aPrevViewId The id of the previous view.
		* @param aCustomMessageId message identifier.
		* @param aCustomMessage custom message provided when the view is changed.
		*/
		void DoActivateL(const TVwsViewId& aPrevViewId,
						 TUid aCustomMessageId,
						 const TDesC8& aCustomMessage);

		/**
		* Remove the container class instance from the App UI's stack and
		* deletes the instance.
		*/
		void DoDeactivate();

        /**
        * From CAknView reaction if size change.
        */
        void HandleClientRectChange();

        void HandleResourceChangeL(TInt aType);

        void SizeChanged();
	private:

		/**
		* Act based on ListBox mode that is currently activated.
		* Means moving, copying or deleting messages.
		*/
		void HandleBasedOnModeL();

		/**
		* Perform the first phase of two phase construction .
		* @param aEngine Pointer to CSmsEngine.
		* @param aView1 Pointer to CLogView.
		*/
		CListboxView(CSmsEngine* aEngine, CLogView* aView1);

		/**
		* Perform the second phase construction of a CListboxView object.
		*/
		void ConstructL();

	private:

		// Container for this view
		CMarkableListContainer* iListBoxContainer;

		// SMS engine.
		CSmsEngine* iEngine;

		// Log
		CLogView* iLogView;

		// ListBox action modes. See SMSExample.hrh for modes.
		TInt iMode;

		// Source folder Id
		TMsvId iFolderID;

		// Target folder Id
		TMsvId iTargetFolderID;

		// iIdentifier identifier for this view.
		TUid iIdentifier;
    };


#endif // __SMSEXAMPLE_LISTBOXVIEW_H__
