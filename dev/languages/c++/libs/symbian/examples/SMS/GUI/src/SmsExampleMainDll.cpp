// SMSExampleAppMainDll.cpp
// ----------------------
//

////////////////////////////////////////////////////////////////////////
//
// Application entry point for pre-EKA2.
//
////////////////////////////////////////////////////////////////////////

#include "SMSExampleApp.h"

// ================= OTHER EXPORTED FUNCTIONS ==============
//
// ---------------------------------------------------------
// NewApplication()
// Constructs CSMSExampleApp
// Returns: created application object
// ---------------------------------------------------------
//
EXPORT_C CApaApplication* NewApplication()
    {
    return new CSMSExampleApp;
    }

// ---------------------------------------------------------
// E32Dll(TDllReason)
// Entry point function for EPOC Apps
// Returns: KErrNone: No error
// ---------------------------------------------------------
//
GLDEF_C TInt E32Dll( TDllReason )
    {
    return KErrNone;
    }

// End of file
