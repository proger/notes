/*
* ============================================================================
*  Name     : CListboxView from SMSExampleListboxView.h
*  Part of  : SMSExample
*  Created  : 12.03.2005 by Forum Nokia
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#include <aknviewappui.h>
#include <aknconsts.h>
#include <aknnotewrappers.h>
#include <msvids.h> // Folder Ids
#include <smsexample.rsg>

#include "SMSExampleListboxView.h"
#include "SMSExampleLogView.h"
#include "SMSExampleMarkableListContainer.h"
#include "SmsExample.hrh"
#include "SMSExampleAppUi.h"

_LIT(KMessages, " messages");
_LIT(KCopiedMessages, "Copied ");
_LIT(KDeletedMessages, "Deleted ");
_LIT(KMovedMessages, "Moved ");
_LIT(KNoMessagesInFolder, "there are no messages in that source folder!");

const TInt KMaxMessageCount(100);

// ----------------------------------------------------------------------------
// CListboxView::NewL()
//
// Symbian OS 2 phase constructor.
// ----------------------------------------------------------------------------
CListboxView* CListboxView::NewL(CSmsEngine* aEngine, CLogView* aView1)
    {
    CListboxView* self = CListboxView::NewLC(aEngine, aView1);
    CleanupStack::Pop(self);

    return self;
    }

// ----------------------------------------------------------------------------
// CListboxView::NewLC()
//
// Symbian OS 2 phase constructor.
// ----------------------------------------------------------------------------
CListboxView* CListboxView::NewLC(CSmsEngine* aEngine, CLogView* aView1)
    {
    CListboxView* self = new (ELeave) CListboxView(aEngine, aView1);
    CleanupStack::PushL(self);
    self->ConstructL();
    return self;
    }

// ----------------------------------------------------------------------------
// CListboxView::CListboxView(CSmsEngine* aEngine,
//									  CLogView* aView1)
//
// Constructor.
// ----------------------------------------------------------------------------
CListboxView::CListboxView(CSmsEngine* aEngine, CLogView* aView1):
	iEngine(aEngine), iLogView(aView1)
    {
	iListBoxContainer = 0;
    }

// ----------------------------------------------------------------------------
// CListboxView::~CListboxView()
//
// Destructor frees resources.
// ----------------------------------------------------------------------------
CListboxView::~CListboxView()
    {
    delete iListBoxContainer;
    iListBoxContainer = NULL;

	iEngine = 0;
	iLogView = 0;
    }

// ----------------------------------------------------------------------------
// CListboxView::ConstructL()
//
// Constructor.
// ----------------------------------------------------------------------------
void CListboxView::ConstructL()
    {
    BaseConstructL(R_LISTBOX_MULTIVIEWS_VIEW);
    }

// ----------------------------------------------------------------------------
// CListboxView::Id() const
//
// Return view Id.
// ----------------------------------------------------------------------------
TUid CListboxView::Id() const
    {
    return TUid::Uid(EListboxViewId);
    }

// ----------------------------------------------------------------------------
// CListboxView::DoActivateL(const TVwsViewId& /*aPrevViewId*/,
//                                    TUid /*aCustomMessageId*/,
 //                                   const TDesC8& /*aCustomMessage*/)
//
// Activate ListBox view. Create a new Listbox and add it to stack.
// Initialize ListBox with sms messages got from a specific folder.
// See SetFolderID and InitializeListBoxL.
// ----------------------------------------------------------------------------
void CListboxView::DoActivateL(const TVwsViewId& /*aPrevViewId*/,
                                    TUid /*aCustomMessageId*/,
                                    const TDesC8& /*aCustomMessage*/)
    {
	iListBoxContainer = CMarkableListContainer::NewL(ClientRect());

	iListBoxContainer->SetMopParent(this);

    AppUi()->AddToStackL(*this, iListBoxContainer);
	InitializeListBoxL();
    }


// ----------------------------------------------------------------------------
// CListboxView::SetFolderID(TMsvId aFolderID)
//
// Set source folder id. Source means all messages in folder which id is
// aFolderID
// ----------------------------------------------------------------------------
void CListboxView::SetFolderID(TMsvId aFolderID)
	{
	iFolderID = aFolderID;
	}

// ----------------------------------------------------------------------------
// CListboxView::InitializeListBoxL()
//
// Initialize ListBox with SMS messages. Load source folder messages into
// ListBox. Source folder must be set before calling this.
// ----------------------------------------------------------------------------
void CListboxView::InitializeListBoxL()
	{

	// Source folder must be set
	if ( iFolderID != KMsvGlobalInBoxIndexEntryId &&
	     iFolderID != KMsvGlobalOutBoxIndexEntryId &&
		 iFolderID != KMsvDraftEntryId )
		{
		User::Leave(KErrArgument);
		}

	// Listbox takes ownership of the lists. Take messages bodys & addresses
	// and set them into ListBox.
	CDesCArrayFlat* arrayAddr = 0;
	CDesCArrayFlat* arrayMsgBody = 0;
	iEngine->GetFolderSMSMessageInformationL(iFolderID,arrayAddr,arrayMsgBody);
	iListBoxContainer->SetArraysL( arrayAddr, arrayMsgBody );

	RArray<TMsvId>* array = iEngine->GetMessageIds();
	iListBoxContainer->SetIdArray ( array );

	// Check if there are no messages in source folder.
	if ( array->Count() == 0 )
		{
		iLogView->LogEventBeginningL();
		iLogView->DrawTextL( KNoMessagesInFolder );

		RArray<TMsvId>* selected = iListBoxContainer->GetSelectedItems();
		selected->Reset();
		delete selected;
		selected = 0;

		AppUi()->ActivateLocalViewL(TUid::Uid(ELogViewId));
		}
	}

// ----------------------------------------------------------------------------
// CListboxView::DoDeactivate()
//
// Deactivate this view. Delete ListBox.
// ----------------------------------------------------------------------------
void CListboxView::DoDeactivate()
    {
    if (iListBoxContainer)
        {
        AppUi()->RemoveFromStack(iListBoxContainer);
		delete iListBoxContainer;
        iListBoxContainer = NULL;
        }
    }

// ----------------------------------------------------------------------------
// CListboxView::SetListBoxMode(TInt aMode)
//
// ListBox mode for ListBox. See SMSExample.hrh for list of modes.
// ----------------------------------------------------------------------------
void CListboxView::SetListBoxMode(TInt aMode)
	{
	iMode = aMode;
	}

// ----------------------------------------------------------------------------
// CListboxView::SetTargetFolderID(TMsvId aTargetFolderID)
//
// Set Target folder of ListBox action.
// -----------------------------------------------------------------------------
void CListboxView::SetTargetFolderID(TMsvId aTargetFolderID)
	{
	iTargetFolderID = aTargetFolderID;
	}

// ----------------------------------------------------------------------------
// CListboxView::HandleClientRectChange()
//
// Set ListBox rectangle.
// ----------------------------------------------------------------------------
void CListboxView::HandleClientRectChange()
    {
    if ( iListBoxContainer )
        {
        iListBoxContainer->SetRect( ClientRect() );
        }
    }

// ----------------------------------------------------------------------------
// CListboxView::HandleBasedOnModeL()
//
// Do action based on ListBox mode.
// ----------------------------------------------------------------------------
void CListboxView::HandleBasedOnModeL()
	{
	RArray<TMsvId>* array = iListBoxContainer->GetSelectedItems();

	if ( !array )
		{
		return;
		}

	// Copy from one folder to another
	if ( iMode == ESMSExampleModeCopy )
		{

		for (TInt i = 0; i < array->Count(); i++)
			{
			iEngine->CopyMessageL( (*array)[i], iTargetFolderID );
			}

		iLogView->LogEventBeginningL();
		TBuf<KMaxMessageCount> textBuffer;
		textBuffer.Append( KCopiedMessages );
		textBuffer.AppendNum( array->Count() );
		textBuffer.Append(KMessages);

		iLogView->DrawTextL( textBuffer );
		}

	// Delete a group of messages
	else if ( iMode == ESMSExampleDelete )
		{

		for (TInt i = 0; i < array->Count(); i++)
			{
			iEngine->DeleteMessageL( (*array)[i] );
			}

		iLogView->LogEventBeginningL();
		TBuf<KMaxMessageCount> textBuffer;
		textBuffer.Append( KDeletedMessages );
		textBuffer.AppendNum( array->Count() );
		textBuffer.Append(KMessages);

		iLogView->DrawTextL( textBuffer );
		}

	// Move a group of messages to another folder
	else if ( iMode == ESMSExampleMove )
		{

		for (TInt i = 0; i < array->Count(); i++)
			{
			iEngine->MoveToFolderL( (*array)[i], iTargetFolderID );
			}

		iLogView->LogEventBeginningL();
		TBuf<KMaxMessageCount> textBuffer;
		textBuffer.Append( KMovedMessages );
		textBuffer.AppendNum( array->Count() );
		textBuffer.Append(KMessages);

		iLogView->DrawTextL( textBuffer );
		}

	array->Reset();
	delete array;
	array = 0;

	AppUi()->ActivateLocalViewL(TUid::Uid(ELogViewId));
	iMode = 0;
	}

// ----------------------------------------------------------------------------
// CListboxView::HandleCommandL(TInt aCommand)
//
// Handle commands.
// ----------------------------------------------------------------------------
void CListboxView::HandleCommandL(TInt aCommand)
    {
	switch ( aCommand )
		{
     case EAknSoftkeyBack:
            {
			// This is responsible for destroying selected messages.
			RArray<TMsvId>* array = iListBoxContainer->GetSelectedItems();
			array->Reset();
			delete array;
			array = 0;

			// Move back to RichTextEditor view.
            AppUi()->ActivateLocalViewL(TUid::Uid(ELogViewId));

            break;
            }
	 case ESMSExampleListBoxSelectAll:
			{
			// Select all messages.
			iListBoxContainer->MarkAllL(ETrue);
			break;
			}
	 case ESMSExampleListBoxDeSelectAll:
			{
			// Deselect all messages.
			iListBoxContainer->MarkAllL(EFalse);
			break;
			}
	 case ESMSExampleListBoxDone:
			{
			// Do the action according to ListBox mode.

			//The main actions for SMS manipulation are executed here.
			TRAPD(err, HandleBasedOnModeL() );
			if( err )
			    {
			    _LIT(KErrText, "Error in SMS handling");
			    CSMSExampleAppUi* appui =
			                      static_cast <CSMSExampleAppUi*> (AppUi());
			    appui->ShowMessageL( KErrText );
			    appui->ShowErrorL(err);
			    }
			break;
			}
        default:
            {
            AppUi()->HandleCommandL( aCommand );
            break;
            }
        }
    }

void CListboxView::SizeChanged()
  {
  if( iListBoxContainer )
      {
      iListBoxContainer->SetRect( ClientRect() );
      }
  }

// End of File
