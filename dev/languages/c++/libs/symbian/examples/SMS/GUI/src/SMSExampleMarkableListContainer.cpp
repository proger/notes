/*
* ============================================================================
*  Name     : CMarkableListContainer from SMSExampleMarkableListContainer.h
*  Part of  : SMSExample
*  Created  : 12.03.2005 by Forum Nokia
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

// INCLUDE FILES
#include "SMSExampleMarkableListContainer.h"
#include "SmsExamplePanics.pan"

#include <akniconarray.h> // CAknIcon
#include <aknlists.h> // CAknDoubleGraphicStyleListBox
#include <barsread.h> // TResource Reader
#include <e32def.h> // STATIC_CAST
#include <SMSExample.mbg> // icons
#include <smsexample.rsg> // R_SMSEXAMPLE_MARKABLE_LISTBOX
#include <stringloader.h> // StringLoader
#include <uikon.hrh> // TKeyCode #defines

#ifdef __UI_FRAMEWORKS_V2__
    #include <GULICON.H>
    #include <AknIconUtils.h>
    #include <FBS.H> //fbscli.lib
#endif

#define KListPosition TPoint(0,0)

const TInt KAknExListAddItemBufLength(512);
const TInt KNumberOfIcons(2);
const TInt KBeginIdx=0;
const TInt KEndIdx=2;

_LIT(KIconSelectedText,"1\t");
_LIT(KIconNotSelectedText,"0\t");

// ----------------------------------------------------------------------------
// CMarkableListContainer::ConstructL(const TRect& aRect)
//
// Symbian OS 2 phase constructor. Creates a Window for the controls, which it
// contains.
// ----------------------------------------------------------------------------
void CMarkableListContainer::ConstructL(const TRect& aRect)
	{
	CreateWindowL();

	// Create the list
	CreateListL();

	iAddress = 0;
	iMessage = 0;

	// Set the icons in the list's drawer
	SetupListIconsL();

	// Set up scroll bars
	SetupScrollBarsL();

	iSelectedIds = new (ELeave) RArray<TMsvId>;

	SetRect(aRect);
	ActivateL();
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::CreateListL()
//
// 2nd phase constructor constructs the iSmsListBox and sets its window.
// ----------------------------------------------------------------------------
void CMarkableListContainer::CreateListL()
	{
	// First phase construction
	iSmsListBox = new (ELeave) CAknDoubleGraphicStyleListBox();
	iSmsListBox->SetContainerWindowL(*this);
	iSmsListBox->SetListBoxObserver(this);

	// Second Phase Construction
	TResourceReader reader;
	CEikonEnv::Static()->CreateResourceReaderLC(reader,
	                                            R_SMSEXAMPLE_MARKABLE_LISTBOX);
	iSmsListBox->ConstructFromResourceL(reader);
	CleanupStack::PopAndDestroy(); // reader
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::SetupScrollBarsL()
//
// Create scrollbars. Scrollbars appear automatically.
// ----------------------------------------------------------------------------
void CMarkableListContainer::SetupScrollBarsL()
	{
	iSmsListBox->CreateScrollBarFrameL(ETrue);
	iSmsListBox->ScrollBarFrame()->SetScrollBarVisibilityL(
		CEikScrollBarFrame::EOff, CEikScrollBarFrame::EAuto);
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::SetArraysL(CDesCArrayFlat* aAddress,
//								  	  CDesCArrayFlat* aMessage )
//
// Insert both lists into listbox.
// ----------------------------------------------------------------------------
void CMarkableListContainer::SetArraysL(CDesCArrayFlat* aAddress,
                                        CDesCArrayFlat* aMessage )
	{
	iAddress = aAddress;
	iMessage = aMessage;

	for (TInt i = 0; i < aAddress->Count(); i++)
		{
		AddItemL( (*aAddress)[i], (*aMessage)[i] );
		}
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::MarkAllL(TBool aSelectionMode)
//
// Select or deselect all messages that ListBox displayes.
// ----------------------------------------------------------------------------
void CMarkableListContainer::MarkAllL(TBool aSelectionMode)
	{
	if  (iSmsListBox)
		{
		// Not taking ownership
		CTextListBoxModel* model = iSmsListBox->Model();
		model->SetOwnershipType (ELbmOwnsItemArray);
		CDesCArray* smsArray = STATIC_CAST(CDesCArray*,model->ItemTextArray());

		// Change all icon values.
		for (TInt i = 0; i < smsArray->Count(); i++)
			{
			TBuf <KAknExListAddItemBufLength> changeItem( (*smsArray)[i] );


			if ( aSelectionMode ) // Select one item
				{
				// Change the icon to selected
				changeItem.Replace(KBeginIdx, KEndIdx, KIconSelectedText);
				}
			else  // Deselect one item
				{
				// Change the icon to not selected
				changeItem.Replace(KBeginIdx, KEndIdx, KIconNotSelectedText);
				}

				// Delete one item
				smsArray->Delete(i);
				// Insert changed item back.
				smsArray->InsertL(i, changeItem);
			}

			// Change selected ids array to match the case.
			if ( aSelectionMode )
				{
				// Select all.
				SelectItem( 0, ETrue );
				}
			else
				{
				// Deselect all.
				DeSelectItem( 0, ETrue );
				}

			iSmsListBox->HandleItemAdditionL();
		}
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::MarkOrUnMarkL()
//
// Change icon and modify iIdArray to correspond situation.
// The icon is used to display if the item has been selected or not.
// ----------------------------------------------------------------------------
void CMarkableListContainer::MarkOrUnMarkL()
	{
	if  (iSmsListBox)
		{
		// Cursor's current position
		TInt itemIndex = iSmsListBox->CurrentItemIndex();

		if (itemIndex >= 0)
			{
			// not taking ownership
			CTextListBoxModel* model = iSmsListBox->Model();
			model->SetOwnershipType (ELbmOwnsItemArray);
			CDesCArray* smsArray = STATIC_CAST(CDesCArray*,
			                                   model->ItemTextArray());

			if( smsArray->Count() <= itemIndex )
	    		{
    		    User::Leave(KErrTooBig);
	    		}

			TBuf <KAknExListAddItemBufLength> changeItem(
			                                            (*smsArray)[itemIndex]
			                                            );

			// Compare index to see which is the case. Deselected -> Selected
			if ( changeItem.Left(KEndIdx) == KIconNotSelectedText )
				{
				changeItem.Replace(KBeginIdx, KEndIdx, KIconSelectedText);
				// Modify iIdArray
				SelectItem( itemIndex, EFalse );
				} // Selected -> Deselected
			else if ( changeItem.Left(KEndIdx) == KIconSelectedText )
				{
				changeItem.Replace(KBeginIdx, KEndIdx, KIconNotSelectedText);
				// Modify iIdArray
				DeSelectItem( itemIndex, EFalse );
				}

			smsArray->Delete(itemIndex);
			smsArray->InsertL(itemIndex, changeItem);

			iSmsListBox->HandleItemAdditionL();
			}
		}
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::SelectItem(TInt aIndex, TBool aSelectAll)
//
// Select item.
// ----------------------------------------------------------------------------
void CMarkableListContainer::SelectItem(TInt aIndex, TBool aSelectAll)
	{
	// Copy
	if ( aSelectAll )
		{
		iSelectedIds->Reset();
		for (TInt i = 0; i < iIdArray->Count(); i++)
			{
			iSelectedIds->Append( (*iIdArray)[i] );
			}
		}
	else if( iIdArray->Count() > aIndex )
		{
		TMsvId id = (*iIdArray)[aIndex];
		// Try to find the id, if not present then append id onto list.
		if ( KErrNotFound == iSelectedIds->Find( id ) )
			{
			iSelectedIds->Append( id );
			}
		}
	else
	    {
	    Panic(EGuiGeneral);
	    }

	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::DeSelectItem(TInt aIndex, TBool aDeselectAll)
//
// Deselect item.
// ----------------------------------------------------------------------------
void CMarkableListContainer::DeSelectItem(TInt aIndex, TBool aDeselectAll)
	{
	if ( aDeselectAll )
		{
		iSelectedIds->Reset();
		}
	else if( iIdArray->Count() > aIndex )
		{
		TInt index = iSelectedIds->Find( (*iIdArray)[aIndex] );

		if ( index != KErrNotFound  )
			{
			iSelectedIds->Remove( index );
			}
		}
	else
	    {
	    Panic(EGuiGeneral);
	    }

	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::SetupListIconsL()
//
// Create icon array.
// ----------------------------------------------------------------------------
void CMarkableListContainer::SetupListIconsL()
	{
	// Get the name of the file containing the icons
	HBufC* iconFilename = NULL;
  	iconFilename = StringLoader::LoadLC(R_ICON_FILE_NAME); //#ifdef in loc file
	// Create an array of icons, reading them from the file
	CArrayPtr<CGulIcon>* icons = new(ELeave) CAknIconArray(KNumberOfIcons);
	CleanupStack::PushL(icons);

    #ifdef __UI_FRAMEWORKS_V2__ // NOT IN FP3, should executed also there
                                // because scalable ui is supported
        CFbsBitmap* icon = NULL;
        CFbsBitmap* iconMask = NULL;
        CFbsBitmap* icon2 = NULL;
        CFbsBitmap* iconMask2 = NULL;

        AknIconUtils::CreateIconLC(icon, iconMask, *iconFilename,
                                   EMbmSmsexampleTock,EMbmSmsexampleTock_mask);


        CGulIcon* guiIcon = CGulIcon::NewL( icon, iconMask );
        CleanupStack::PushL(guiIcon);
        icons->AppendL( guiIcon );
        CleanupStack::Pop(guiIcon);
        CleanupStack::Pop(2); //icon, iconMask

        AknIconUtils::CreateIconLC(icon2, iconMask2, *iconFilename,
                                   EMbmSmsexampleTick,EMbmSmsexampleTick_mask);

        CGulIcon* guiIcon2 = CGulIcon::NewL( icon2, iconMask2 );
        CleanupStack::PushL(guiIcon2);
        icons->AppendL( guiIcon2 );
        CleanupStack::Pop(guiIcon2);
        CleanupStack::Pop(2); //icon2, iconMask2
    #else
    	CGulIcon* icon = iEikonEnv->CreateIconL(*iconFilename,
    	                                        EMbmSmsexampleTock,
    	                                        EMbmSmsexampleTock_mask);
    	CleanupStack::PushL(icon);
    	icons->AppendL(icon);
    	CleanupStack::Pop(icon);
        icon =iEikonEnv->CreateIconL(*iconFilename, EMbmSmsexampleTick,
                                                    EMbmSmsexampleTick_mask);
    	CleanupStack::PushL(icon);
    	icons->AppendL(icon);
    	CleanupStack::Pop(icon);
    #endif

	CleanupStack::Pop(icons);

	CleanupStack::PopAndDestroy(iconFilename);

	// passing ownership of icons
	iSmsListBox->ItemDrawer()->ColumnData()->SetIconArray(icons);
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::NewL(const TRect& aRect)
//
// Symbian OS 2 phase constructor.
// ----------------------------------------------------------------------------
CMarkableListContainer* CMarkableListContainer::NewL(const TRect& aRect)
	{
	CMarkableListContainer* self = CMarkableListContainer::NewLC(aRect);
	CleanupStack::Pop(self);
	return self;
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::NewLC(const TRect& aRect)
//
// Symbian OS 2 phase constructor.
// ----------------------------------------------------------------------------
CMarkableListContainer* CMarkableListContainer::NewLC(const TRect& aRect)
	{
	CMarkableListContainer* self = new (ELeave) CMarkableListContainer;
	CleanupStack::PushL(self);
	self->ConstructL(aRect);
	return self;
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::~CMarkableListContainer()
//
// Destructor.
// ----------------------------------------------------------------------------
CMarkableListContainer::~CMarkableListContainer()
	{
	Cleanup();
	}

void CMarkableListContainer::Cleanup()
    {
	iIdArray->Reset();
	delete iIdArray;
	iIdArray = 0;

	iAddress->Reset();
	delete iAddress;
	iAddress = 0;

	iMessage->Reset();
	delete iMessage;
	iMessage = 0;

	delete iSmsListBox;
	iSmsListBox = 0;
    }
// ----------------------------------------------------------------------------
// CMarkableListContainer::SizeChanged()
//
// Called by framework when the view size is changed
// ----------------------------------------------------------------------------
void CMarkableListContainer::SizeChanged()
	{
	iSmsListBox->SetExtent (KListPosition, iSmsListBox->MinimumSize());
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::CountComponentControls() const
//
// Called by the framework in compound controls
// ----------------------------------------------------------------------------
TInt CMarkableListContainer::CountComponentControls() const
	{
	return 1; // return number of controls inside this container
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::ComponentControl(TInt aIndex) const
//
// Called by the framework in compound controls
// ----------------------------------------------------------------------------
CCoeControl* CMarkableListContainer::ComponentControl(TInt aIndex) const
	{
	switch (aIndex)
		{
		case 0:
			return iSmsListBox;
		default:
			return NULL;
		}
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::Draw(const TRect& aRect) const
//
// Called by the framework to draw this control.
// ----------------------------------------------------------------------------
void CMarkableListContainer::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	gc.Clear(aRect);
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::OfferKeyEventL(const TKeyEvent& aKeyEvent,
//										        TEventCode aType)
//
// Called by the framework whenever a key event occurs.
// ----------------------------------------------------------------------------
TKeyResponse CMarkableListContainer::OfferKeyEventL(const TKeyEvent& aKeyEvent,
                                                    TEventCode aType)
	{
	if (iSmsListBox)
		return iSmsListBox->OfferKeyEventL (aKeyEvent, aType);
	else
		return EKeyWasNotConsumed;
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::SetIdArray(RArray<TMsvId>* aArray)
// ----------------------------------------------------------------------------
void CMarkableListContainer::SetIdArray(RArray<TMsvId>* aArray)
	{
	iIdArray = aArray;
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::GetSelectedItems()
//
// Return an array of items that have been selected.
// ----------------------------------------------------------------------------
RArray<TMsvId>* CMarkableListContainer::GetSelectedItems()
	{
	return iSelectedIds;
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::HandleListBoxEventL(CEikListBox* /*aListBox*/,
//										       TListBoxEvent aListBoxEvent)
//
// Called by the framework whenever a list event occurs for which this container
// is an observer.
// ----------------------------------------------------------------------------
void CMarkableListContainer::HandleListBoxEventL(CEikListBox* /*aListBox*/,
                                                 TListBoxEvent aListBoxEvent)
	{
	// if the Select Key has been pressed
	if ((aListBoxEvent == MEikListBoxObserver::EEventEnterKeyPressed) ||
		(aListBoxEvent == MEikListBoxObserver::EEventItemClicked))
		{
		MarkOrUnMarkL();
		}
	}

// ----------------------------------------------------------------------------
// CMarkableListContainer::AddItemL(const TDesC& aNewItem)
//
// Add an item into the listbox.
// ----------------------------------------------------------------------------
void CMarkableListContainer::AddItemL(const TDesC& aFirstLabel,
                                      const TDesC& aSecondLabel)
	{
	CTextListBoxModel* model = iSmsListBox->Model();
	model->SetOwnershipType (ELbmOwnsItemArray);
	CDesCArray* smsArray = static_cast <CDesCArray*> (model->ItemTextArray());

	TBuf <KAknExListAddItemBufLength> addedItem( KBeginIdx );

	// List item string format: "0\tFirstLabel\tSecondLabel\t0"
	// where 0 is index to icon array
	// Listbox icon is required at the beginning of a descriptor,
	//" \t" if there is no icon.
	_LIT( KBeginning, "0\t");
	addedItem.Append( KBeginning );
	addedItem.Append( aFirstLabel );

	// Second icon is always empty.
	_LIT( KEnd, "\t");
	addedItem.Append( KEnd );
	addedItem.Append( aSecondLabel );
	addedItem.Append( KEnd );

	// Insert a new item into the array
	smsArray->AppendL( addedItem );

	iSmsListBox->HandleItemAdditionL();
	}

// End of file
