/*
* ============================================================================
*  Name     : CSMSExampleApp from SMSExampleApp.h
*  Part of  : SMSExample
*  Created  : 08.02.2005 by Forum Nokia
*  Description:
*     Declares main application class.
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#ifndef SMSEXAMPLEAPP_H
#define SMSEXAMPLEAPP_H

// INCLUDES
#include <aknapp.h>

// CONSTANTS
// UID of the application
#ifdef __SERIES60_30__
const TUid KUidSMSExample = { 0xE01FF1Cd };
#else
const TUid KUidSMSExample = { 0x101FF1Cd };
#endif

// CLASS DECLARATION

/**
* CSMSExampleApp application class.
* Provides factory to create concrete document object.
*
*/
class CSMSExampleApp : public CAknApplication
    {

    public: // Functions from base classes
    private:

        /**
        * From CApaApplication, creates CSMSExampleDocument document object.
        * @return A pointer to the created document object.
        */
        CApaDocument* CreateDocumentL();

        /**
        * From CApaApplication, returns application's UID (KUidSMSExample).
        * @return The value of KUidSMSExample.
        */
        TUid AppDllUid() const;
    };

#endif

// End of File

