/*
* ============================================================================
*  Name     : CMarkableListContainer from SMSExampleMarkableListContainer.h
*  Part of  : SMSExample
*  Created  : 12.03.2005 by Forum Nokia
*  Description:
*     ListBox is for viewing messages that are in different folders.
*     User can select messages and perform operations on selected messages.
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#ifndef __SMSEXAMPLE_MARKABLELISTCONTAINER_H
#define __SMSEXAMPLE_MARKABLELISTCONTAINER_H

// INCLUDES
#include <badesca.h> // CDesCArray
#include <coecntrl.h> // CCoeControl
#include <eiklbo.h> // MEikListBoxObserver
#include <badesca.h> // CDesCArrayFlat
#include <eikfrlb.h> //CEikFormattedCellListBox
#include <msvstd.h> //TMsvId

// CLASS DECLARATION

/**
* Message ListBox.
*/
class CMarkableListContainer : public CCoeControl, MEikListBoxObserver
	{
	public: // Constructors and destructor

		/**
		* Create new CMarkableListContainer object
		*
		* @param aRect Frame rectangle for container.
		* @return a pointer to the created instance of CMarkableListContainer
		*/
		static CMarkableListContainer* NewL(const TRect& aRect);

		/**
		* NewLC
		*/
		static CMarkableListContainer* NewLC(const TRect& aRect);
		~CMarkableListContainer();

	public: // members

		/**
		* Set SMS recipient and body arrays.
		* @param aAddress Array of SMS addressess
		* @param aMessage Array of SMS bodys.
		*/
		void SetArraysL(CDesCArrayFlat* aAddress, CDesCArrayFlat* aMessage);

		/**
		* Set array of message Ids.
		* @param aArray Array of Ids.
		*/
		void SetIdArray(RArray<TMsvId>* aArray);

		/**
		* Mark or unmark all ListBox items.
		* @param aSelectionMode ETrue if all items should be marked,
		*        EFalse if unmarked.
		*/
		void MarkAllL(TBool aSelectionMode);

		/**
		* Returns all message ids which have been marked.
		* @return Array of ids.
		*/
		RArray<TMsvId>* GetSelectedItems();

	private: // members

		/**
		* Create icon array.
		*/
		void SetupListIconsL();

		/**
		* Add one item into ListBox.
		* @param aFirstLabel First line.
		* @param aSecondLabel Second line.
		*/
		void AddItemL(const TDesC& aFirstLabel, const TDesC& aSecondLabel);

		/**
		* Marks or unmarks one ListBox item at cursor position.
		*/
		void MarkOrUnMarkL();

		/**
		* Select one ListBox Item.
		* @param aIndex ListBox array index.
		* @param aDeselectAll ETrue if all items should be selected.
		*        EFalse otherwise.
		*/
		void SelectItem(TInt aIndex, TBool aSelectAll);

		/**
		* Deselect one ListBox Item.
		* @param aIndex ListBox array index.
		* @param aDeselectAll ETrue if all items should be deselected.
		*        EFalse otherwise.
		*/
		void DeSelectItem(TInt aIndex, TBool aDeselectAll);

		/**
		* Create a ListBox.
		*/
		void CreateListL();

		/**
		* Setup scrollbars.
		*/
		void SetupScrollBarsL();

	private: // constructor

		/**
		* ConstructL()
		*/
		void ConstructL(const TRect& aRect);

	private: // from MEikListBoxObserver
		void HandleListBoxEventL(CEikListBox* aListBox,
		                         TListBoxEvent aListBoxEvent);

	private: // from CoeControl

		/**
        * From CoeControl,SizeChanged.
        */
		void SizeChanged();

		/**
        * From CoeControl,CountComponentControls.
        */
		TInt CountComponentControls() const;

		/**
        * From CCoeControl,ComponentControl.
        */
		CCoeControl* ComponentControl(TInt aIndex) const;

		/**
        * From CCoeControl,Draw.
        */
		void Draw(const TRect& aRect) const;

		/**
		* From CoeControl, OfferKeyEventL
		*/
		TKeyResponse OfferKeyEventL(const TKeyEvent& aKeyEvent,
		                            TEventCode aType);


    private:

        /**
        * Do the cleanup. Added only because for static code analyzers
        * give an error if the code is directly in destructor
        */
        void Cleanup();

	private: //data

		// CAknDoubleGraphicStyleListBox
		CEikFormattedCellListBox* iSmsListBox;

		// One folder SMS recipients
		CDesCArrayFlat* iAddress;

		// One folder SMS bodys
		CDesCArrayFlat* iMessage;

		// All SMS ids of one folder.
		RArray<TMsvId>* iIdArray;

		// Array of Ids of those SMS that have been marked.
		RArray<TMsvId>* iSelectedIds;

	};

#endif	// #ifndef __SMSEXAMPLE_MARKABLELISTCONTAINER_H


