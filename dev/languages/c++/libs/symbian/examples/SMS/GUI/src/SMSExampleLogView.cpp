/*
* ============================================================================
*  Name     : CLogView from SMSExampleLogView.h
*  Part of  : SMSExample
*  Created  : 12.03.2005 by Forum Nokia
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

// INCLUDES
#include <aknviewappui.h>
#include <aknconsts.h>
#include <smsexample.rsg>

#include "SMSExampleRTEContainer.h"
#include "SMSExampleLogView.h"
#include "SmsExample.hrh"

// ----------------------------------------------------------------------------
// CLogView::NewL()
//
// Symbian OS 2 phase constructor.
// ----------------------------------------------------------------------------
CLogView* CLogView::NewL()
    {
    CLogView* self = CLogView::NewLC();
    CleanupStack::Pop(self);
    return self;
    }

// ----------------------------------------------------------------------------
// CLogView::NewLC()
//
// Symbian OS 2 phase constructor.
// ----------------------------------------------------------------------------
CLogView* CLogView::NewLC()
    {
    CLogView* self = new (ELeave) CLogView();
    CleanupStack::PushL(self);
    self->ConstructL();
    return self;
    }

// ----------------------------------------------------------------------------
// CLogView::CLogView()
//
// Default constructor
// ----------------------------------------------------------------------------
CLogView::CLogView()
    {
    }

// ----------------------------------------------------------------------------
// CLogView::~CLogView()
//
// Destructor
// ----------------------------------------------------------------------------
CLogView::~CLogView()
    {
    delete iContainer;
    iContainer = NULL;
    }

// ----------------------------------------------------------------------------
// CLogView::ConstructL()
//
// Symbian OS 2 phase constructor.
// ----------------------------------------------------------------------------
void CLogView::ConstructL()
    {
	BaseConstructL(R_RICHTEXTEDITOR_MULTIVIEWS_VIEW);

	iContainer = CSMSExampleRTEContainer::NewL(ClientRect());
	iContainer->SetMopParent( this );
    }

// ----------------------------------------------------------------------------
// CLogView::Id() const
//
// Id of the view.
// ----------------------------------------------------------------------------
TUid CLogView::Id() const
    {
    return TUid::Uid(ELogViewId);
    }


// ----------------------------------------------------------------------------
// CLogView::DoActivateL(const TVwsViewId& /*aPrevViewId*/,
//                                    TUid /*aCustomMessageId*/,
//                                    const TDesC8& /*aCustomMessage*/)
//
// Activate this view (make it visible again).
// ----------------------------------------------------------------------------
void CLogView::DoActivateL(const TVwsViewId& /*aPrevViewId*/,
                                    TUid /*aCustomMessageId*/,
                                    const TDesC8& /*aCustomMessage*/)
    {
	AppUi()->AddToStackL(*this, iContainer);
	iContainer->MakeVisible(ETrue);
    }


// ----------------------------------------------------------------------------
// CLogView::DoDeactivate()
//
// DeActivate this view (Hide the view). Do not destroy it.
// ----------------------------------------------------------------------------
void CLogView::DoDeactivate()
    {

    if (iContainer)
        {
		iContainer->MakeVisible(EFalse);
        AppUi()->RemoveFromStack(iContainer);

        }
    }

// ----------------------------------------------------------------------------
// CLogView::LogEventBeginningL()
//
// Begin a log event.
// ----------------------------------------------------------------------------
void CLogView::LogEventBeginningL()
	{
	iContainer->DrawLineL();
	TBuf<KMaxTimeFormatSpec+KMaxTimeFormatSpec> time;
	TTime now;
	now.HomeTime(); // Current time
	now.FormatL(time,TTimeFormatSpec());
	iContainer->DrawTextL( time );
	}

// ----------------------------------------------------------------------------
// CLogView::DrawUnderlinedTextL( const TDesC& aText)
//
// Draw underlined text.
// ----------------------------------------------------------------------------
void CLogView::DrawUnderlinedTextL( const TDesC& aText)
	{
	iContainer->DrawUnderlinedTextL(aText);
	}

// ----------------------------------------------------------------------------
// CLogView::DrawTextWithoutCarriageL( const TDesC& aText )
//
// Draw text without carriage.
// ----------------------------------------------------------------------------
void CLogView::DrawTextWithoutCarriageL( const TDesC& aText )
	{
	iContainer->DrawTextWithoutCarriageL(aText);
	}

// ----------------------------------------------------------------------------
// CLogView::HandleCommandL(TInt aCommand)
//
// Handle view commands. Forward command handling to AppUi
// ----------------------------------------------------------------------------
void CLogView::HandleCommandL(TInt aCommand)
    {
    AppUi()->HandleCommandL(aCommand);
    }

// ----------------------------------------------------------------------------
// CLogView::DrawTextL( const TDesC& aText )
//
// Draw into log.
// ----------------------------------------------------------------------------
void CLogView::DrawTextL( const TDesC& aText )
	{
	iContainer->DrawTextL( aText );
	}

// ----------------------------------------------------------------------------
// CLogView::AddCarriageReturnL()
//
// Add line break into log.
// ----------------------------------------------------------------------------
void CLogView::AddCarriageReturnL()
	{
	iContainer->AddCarriageReturnL();
	}

void CLogView::SizeChanged()
  {
  if( iContainer )
    iContainer->SetRect( ClientRect() );
  }

// End of File
