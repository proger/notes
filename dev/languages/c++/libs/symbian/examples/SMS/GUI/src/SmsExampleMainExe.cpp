// SmsExampleMainExe.cpp
// ----------------------
//

////////////////////////////////////////////////////////////////////////
//
// Application entry point for EKA2.
//
////////////////////////////////////////////////////////////////////////

#include "SmsExampleApp.h"
#include <eikstart.h>

EXPORT_C CApaApplication* NewApplication()
    {
    return ( static_cast<CApaApplication*>( new CSMSExampleApp ) );
    }

// The application entry-point
GLDEF_C TInt E32Main()
    {
    return EikStart::RunApplication( NewApplication );
    }


// End of file
