/*
* ============================================================================
*  Name     : CLogView from SMSExampleLogView.h
*  Part of  : SMSExample
*  Created  : 12.03.2005 by Forum Nokia
*  Description: Log view. Keeps track of events. 
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#ifndef __SMSEXAMPLE_LOG_VIEW_H__
#define __SMSEXAMPLE_LOG_VIEW_H__

// INCLUDES
#include <aknview.h>

// FORWARD DECLARATIONS
class CSMSExampleRTEContainer;

/**
* Log view that uses RichTextEditor. 
*/
class CLogView: public CAknView
    {
	public:
	
		/**
		* Create a CLogView object
		* @result a pointer to the created instance of CLogView
		*/
		static CLogView* NewL();

		/**
		* Create a CLogView object
		* @result a pointer to the created instance of CLogView
		*/
		static CLogView* NewLC();

		/**
		* Destroy the object 
		*/
		~CLogView();


	public: // from CAknView

		/**
		* Identity of this view to the system
		* @result the ID of view
		*/
		TUid Id() const;

		/**
		* Handle commands
		* @param aCommand Command to be handled
		*/
		void HandleCommandL(TInt aCommand);

		/**
		* Create the Container class object
		* @param aPrevViewId The id of the previous view
		* @param aCustomMessageId message identifier
		* @param aCustomMessage custom message provided when view is changed
		*/
		void DoActivateL(const TVwsViewId& aPrevViewId,
						TUid aCustomMessageId,
						const TDesC8& aCustomMessage);

		/**
		* Remove the container class instance from the App UI's stack 
		*/
		void DoDeactivate();

        void SizeChanged();
	public: // member methods
	
		/**
		* Draw text into the log.
		* @param aText text to be drawn.
		*/
		void DrawTextL( const TDesC& aText );
	
		/**
		* Draw underlined text into the log.
		* @param aText text to be drawn.
		*/
		void DrawUnderlinedTextL( const TDesC& aText);

		/**
		* Draw text into the log. Do not add line break at the end of the text.
		* @param aText text to be drawn.
		*/
		void DrawTextWithoutCarriageL( const TDesC& aText );

		/**
		* Line break.
		*/
		void AddCarriageReturnL();

		
		/**
		* Begin a log event (prints a line and current time).
		*/
		void LogEventBeginningL();

	private:

		/**
		* Perform the first phase of two phase construction 
		*/
		CLogView();

		/**
		* Perform the second phase construction of a CLogView object
		*/
		void ConstructL();


	private: // data members

		// Container for this view 
		CSMSExampleRTEContainer* iContainer; 

		// Identifier for this view 
		TUid iIdentifier;
    };


#endif // __SMSEXAMPLE_LOG_VIEW__
