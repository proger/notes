/*
* ============================================================================
*  Name     : CSMSExampleAppUi from SMSExampleAppui.h
*  Part of  : SMSExample
*  Created  : 13.03.2005 by Forum Nokia
*  Description:
*     Declares UI class for application.
*  Version  : 1.0
*  Copyright: Nokia Corporation
* ============================================================================
*/

#ifndef SMSEXAMPLEAPPUI_H
#define SMSEXAMPLEAPPUI_H

// INCLUDES
#include <aknappui.h>
#include <eikapp.h>
#include <eikdoc.h>
#include <e32std.h>
#include <coeccntx.h>
#include <aknViewAppUi.h>

#include "SMSExample.hrh"
#include "SMSExampleParser.h"
#include "SMSEngine.h"

// FORWARD DECLARATIONS
class CSMSExampleRTEContainer;
class CLogView;
class CListboxView;

// CLASS DECLARATION

/**
* Application UI class.
*
*/
class CSMSExampleAppUi : public CAknViewAppUi,
                         public MSmsEngineObserver
    {
    public: // // Constructors and destructor

        /**
        * default constructor.
        */
        void ConstructL();

        /**
        * Destructor.
        */
        ~CSMSExampleAppUi();

    public: // New functions

        /**
        * Query message address (recipient).
        * @return dialog return value
        */
		TInt GetAddressL();

        /**
        * Query message body.
        * @return dialog return value
        */
		TInt QueryMessageL();

        /**
        * Execute one operation according to ListBox mode.
		* @param aMode Mode of the ListBox.
		* @param aOriginFolderID Source folder of the operation.
		* @param aDestinationFolderID Target folder of the operation.
        */
		void QueryMessagesAndExecuteL(TInt aMode, TMsvId aOriginFolderID,
									 TMsvId aDestinationFolderID);

        /**
        * Show a dialog containing the message
        * @param aMsg the message
        */
        void ShowMessageL(const TDesC& aMsg) const;

        /**
        * Show a dialog containing the error code
        * @param aErrorCode the error code
        */
        void ShowErrorL(TInt aErrorCode) const;

    public:
        /**
        * From MSmsEngineObserver
        */
        void SendingMessage();
        void MessageSent();
        void MessageReceived(const TDesC& addr, const TDesC& msg);
        void SmsEngineInitialized();
        void SmsEngineError(TInt aErrorCode);
        void MessageDeleted(); //if automatic deletion is on


    public:
        /**
        *  from CAknAppUiBase
        */
        void HandleResourceChangeL(TInt aType);

    private:
        /**
		* From MEikMenuObserver
		*/
        void DynInitMenuPaneL(TInt aResourceId,CEikMenuPane* aMenuPane);

    private:
        /**
        * From CEikAppUi, takes care of command handling.
        * @param aCommand command to be handled
        */
        void HandleCommandL(TInt aCommand);

        /**
        * From CEikAppUi, handles key events.
        * @param aKeyEvent Event to handled.
        * @param aType Type of the key event.
        * @return Response code (EKeyWasConsumed, EKeyWasNotConsumed).
        */
        virtual TKeyResponse HandleKeyEventL(
            const TKeyEvent& aKeyEvent,TEventCode aType);

    private:
        void HandleReceivedMessageL(const TDesC& aAddress, const TDesC& aMessage);

    private: // Data members

		// The application view 1
		CLogView* iLogView;

		/// The application view 2
		CListboxView* iListboxView;

        //The SMS engine
        CSmsEngine* iSmsEngine;

		// Parser that searches "phone numbers"
		CSMSExampleParser* iParser;

		// Recipient address
        TBuf<EMtmsExampleMaxAddressLength> iAddress;

		// Message body
		TBuf<KSmsMessageLength> iMessage;

		// ETrue is automatic delete is on, EFalse otherwise
		TBool iAutomaticDelete;

    };

#endif
