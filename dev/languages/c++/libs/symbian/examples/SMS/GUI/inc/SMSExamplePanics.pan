/* ====================================================================
 * File: SmsExamplePanics.pan
 * Created: 08/03/06
 * Author: 
 * Copyright (c): , All rights reserved
 * ==================================================================== */

#ifndef __SMSEXAMPLE_PAN__
#define __SMSEXAMPLE_PAN__

_LIT(applicationName,"SMSExample");

/** SMSExample application panic codes */
enum TSmsExamplePanics 
    {
    EGuiErrorConstructing=1, //bitmap in wrong place etc.    
    EGuiGeneral
    // add further panics here
    };

inline void Panic(TSmsExamplePanics aReason)
    {
    User::Panic(applicationName, aReason);
    }

#endif // __SMSEXAMPLE_PAN__
