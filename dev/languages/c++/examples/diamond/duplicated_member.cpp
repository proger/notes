/***************************************************************
 * Example of diamond shape with duplicated variable member.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ****************************************************************/

#include <iostream>

class A
{
public:
  int i;
  A(int _i) : i(_i) { std::cout << "A initialized with " << _i << "." << std::endl; }
};

class B1 : public A
{
public:
  B1(int i) : A(i) { std::cout << "B1 initialized with " << i << "." << std::endl; }
};

class B2 : public A
{
public:
  B2(int i) : A(i) { std::cout << "B2 initialized with " << i << "." << std::endl; }
};

class C : public B1, public B2
{
public:
  C() : B1(1), B2(2) {}
};

int main()
{
  C* pc = new C;
  
  // The following expression leads to a compiler error : ambiguity of access of member i (through B1 or through B2 ?). In fact there is two members i, and the compiler can't decide for us which one to take.
  // pc->i;

  std::cout << "Value of i through B1 = " << static_cast<B1*>(pc)->i << std::endl;
  std::cout << "Value of i through B2 = " << static_cast<B2*>(pc)->i << std::endl;
}
