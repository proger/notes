/***************************************************************
 * Example of diamond shape using virtual inheritance to resolve
 * member duplication.
 * B1 and B2 inherit virtualy from A, and C must initialize A.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>

class A
{
public:
  int i;
  A(int _i) : i(_i) { std::cout << "A initialized with " << _i << "." << std::endl; }
};

// B1 inherits virtualy from A.
class B1 : virtual public A
{
public:
  // initializing A in B1 is still legal, of course. But when creating a C object, A is no more initialized in B1 or B2, but only in C. So the calls to A constructor in B1 and B2 are no more compiled.
  B1(int i) : A(i) { std::cout << "B1 initialized with " << i << "." << std::endl; }
};

// B2 inherits virtualy from A.
class B2 : virtual public A
{
public:
  // same comment than for B1 constructor
  B2(int i) : A(i) { std::cout << "B2 initialized with " << i << "." << std::endl; }
};

class C : public B1, public B2
{
public:
  // C has to call A constructor to initialize it, this is the drawback.
  C() : B1(1), B2(2), A(3) {}
};

int main()
{
  C* pc = new C;
  
  // The following expression works perfectly, since there is now only one member i.
  pc->i;

  std::cout << "Value of i through B1 = " << static_cast<B1*>(pc)->i << std::endl;
  std::cout << "Value of i through B2 = " << static_cast<B2*>(pc)->i << std::endl;
}
