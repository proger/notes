/***************************************************************
 * Memento pattern example.
 *
 * The Memento pattern is used to implement a backup/restore
 * (externalization) system.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Memento
{
private:
  friend class Originator;
  int getState() const { return this->state; }
  void setState(int s) { this->state = s; }
  int state;
};

class Originator
{
public:
  Originator(int s) : state(s) {}
  Memento* getMemento() const
  {
    std::cout << "Originator::state = " << this->state << "." << std::endl;
    Memento* m = new Memento(); 
    m->setState(this->state);
    return m;
  }
  void setMemento(const Memento* m)
  {
    this->state = m->getState();
    std::cout << "Originator::state = " << this->state << "." << std::endl;
  }
  void doSomething()
  {
    this->state += 10;
    std::cout << "Originator::state = " << this->state << "." << std::endl;
  }
private:
  int state;
};

class Caretaker
{
public:
  Caretaker(Originator* o) : originator(o), memento(0) {}
  ~Caretaker() { delete this->memento; }
  void backup() 
  {
    std::cout << "Caretaker::backup()." << std::endl;
    this->memento = this->originator->getMemento(); 
  }
  void restore() 
  {
    std::cout << "Caretaker::restore()." << std::endl;
    this->originator->setMemento(this->memento);
    delete this->memento;
    this->memento = 0;
  }
private:
  Originator* originator;
  Memento* memento;
};

int main()
{
  Originator originator(5);
  Caretaker caretaker(&originator);

  caretaker.backup();
  originator.doSomething();
  caretaker.restore();
}
