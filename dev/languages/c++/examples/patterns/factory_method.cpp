/***************************************************************
 * Factory method pattern example.
 *
 * A creator abstract mother class defines a virtual function
 * for creating an object.
 *
 * Subclasses implement this function, deciding which object
 * to create.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Product
{
public:
  virtual ~Product() = 0;
};
Product::~Product() {}

class ConcreteProduct : public Product
{
};

class Creator
{
private:
  Product* product;
public:
  Creator() : product(0) {}
  virtual ~Creator() {}
  // factoryMethod could also take some parameters, allowing to instanciate different concrete subclasses of Product depending on these parameters.
  virtual Product* factoryMethod() = 0;
  Product* getProduct() 
  { 
    if ( ! this->product)
      this->product = this->factoryMethod(); 
    return this->product;
  }
};

class ConcreteCreator : public Creator
{
public:
  virtual Product* factoryMethod() { return new ConcreteProduct; }
};

// Another concrete creator could be a template, taking as template parameter a concrete subclass of Product
template<class T> class TemplateCreator : public Creator
{
public:
  virtual Product* factoryMethod() { return new T; }
};

int main()
{
  ConcreteCreator creator;
  TemplateCreator<ConcreteProduct> creatorT;
  Product* product;

  product = creator.getProduct();
  product = creatorT.getProduct();
}
