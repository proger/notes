/***************************************************************
 * State pattern example.
 *
 * A abstract State class defines a function for changing its
 * state according to a context.
 *
 * "Changing its state" means that a concrete State object is
 * exchanged with another concrete State object of a different
 * class.
 *
 * It's the Context class, which has a reference to the concrete
 * State object that, at the end, receive notification for
 * exchanging the State object for another.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Context;

class State
{
public:
  virtual ~State() {}
  virtual void handle1(Context* c) = 0;
  virtual void handle2(Context* c) = 0;
protected:
  void changeState(Context*, State*);
};

class ConcreteStateA : public State
{
public:
  ConcreteStateA() : State() {}
  virtual void handle1(Context* c) 
  {
    std::cout << "ConcreteStateA::handle1()." << std::endl;
  }
  virtual void handle2(Context* c);
};

class ConcreteStateB : public State
{
public:
  ConcreteStateB() : State() {}
  virtual void handle1(Context* c) 
  {
    std::cout << "ConcreteStateB::handle1()." << std::endl;
    this->changeState(c, new ConcreteStateA());
  }
  virtual void handle2(Context* c)
  {
    std::cout << "ConcreteStateB::handle2()." << std::endl;
  }
};

class Context
{
public:
  Context() : state(new ConcreteStateA()) {}
  ~Context() { delete this->state; }
  void request1() { this->state->handle1(this); }
  void request2() { this->state->handle2(this); }
private:
  State* state;
  friend class State;
  void changeState(State* s) 
  { 
    delete this->state;
    this->state = s; 
  }
};

void State::changeState(Context* c, State* s) { c->changeState(s); }

void ConcreteStateA::handle2(Context* c)
{
  std::cout << "ConcreteStateA::handle2()." << std::endl;
  this->changeState(c, new ConcreteStateB());
}

int main()
{
  Context context;

  context.request1();
  context.request2();
  context.request2();
  context.request1();
  context.request1();
}
