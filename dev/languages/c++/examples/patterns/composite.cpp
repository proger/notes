/***************************************************************
 * Composite pattern example.
 *
 * The composite pattern is used in creation of tree or graph
 * structures.
 *
 * An interface providing functions to add children to a
 * component is inherited by all components:
 *   _ leaf component (doesn't accept children) ;
 *   _ composite components (made for accepting children).
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>
#include <vector>

class Component
{
public:
  virtual void operation() = 0;
  virtual void add(Component*) = 0;
  virtual void remove(Component*) = 0;
  virtual Component* getChild(int) = 0;
  virtual ~Component() {}
};

class Leaf : public Component
{
public:
  virtual void operation() { std::cout << "Leaf operation of " << this << "." << std::endl; }
  virtual void add(Component*) {}
  virtual void remove(Component*) {}
  virtual Component* getChild(int) { return 0; }
};

class Composite : public Component
{
public:
  virtual void operation() 
  { 
    std::cout << "Composite operation of " << this << "." << std::endl; 
    for (int i = 0 ; i < this->children.size() ; ++i)
      this->children[i]->operation();
  }
  virtual void add(Component* c) { this->children.push_back(c); }
  virtual void remove(Component* c) {}
  virtual Component* getChild(int i) { return this->children.at(i); }
private:
  std::vector<Component*> children;
};

int main()
{
  Composite root;
  Composite *tmp1, *tmp2;
  tmp1 = new Composite();
  tmp2 = new Composite();
  tmp2->add(new Leaf());
  tmp2->add(new Leaf());
  tmp2->add(new Leaf());
  tmp1->add(new Leaf());
  tmp1->add(tmp2);
  tmp1->add(new Leaf());
  tmp1->add(new Leaf());
  root.add(tmp1);
  root.add(new Leaf());
  
  root.operation();
}
