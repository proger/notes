/***************************************************************
 * Adapter pattern example.
 * Object adapter flavour.
 *
 * An object adapter has the same function as a class adapter.
 * However instead of inheriting from the Adaptee class, it
 * declares an instance of the Adaptee as a private member.
 * 
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Target
{
public:
  virtual void request() = 0;
};

class Adaptee
{
public:
  void specificRequest() { std::cout << "Calling Adaptee::specificRequest()." << std::endl; }
};

class Adapter : public Target
{
public:
  virtual void request() { adaptee.specificRequest(); }
private:
  Adaptee adaptee;
};

int main()
{
  Adapter adapter;

  adapter.request();
}
