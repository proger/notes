/***************************************************************
 * Singleton pattern example.
 *
 * Singleton hierarchy.
 *
 * In this example, the Singleton class is derived to create a 
 * subclass SubSingleton. This subclass registers itself using
 * the function register.
 * Then the singleton used at runtime is found using a name chosen
 * among the registered singletons.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 * Taken from "Design Patterns" book.
 ***************************************************************/

#include <iostream>
#include <string>
#include <map>

class Singleton
{
public:
  virtual ~Singleton() {}
  static void addToRegistry(const std::string& name, Singleton* instance)
  {
    registry[name] = instance;
  }
  static Singleton* instance()
  {
    if ( ! _instance)
      {
	std::string singleton_name = "SubSingleton"; /*usually : call a function to get dynamically the singleton name*/
	_instance = registry[singleton_name];
      }
    return _instance;
  }
private:
  static Singleton* _instance;
  static std::map<std::string, Singleton*> registry;
};

Singleton* Singleton::_instance = 0;
std::map<std::string, Singleton*> Singleton::registry;

class SubSingleton : public Singleton
{
public:
  SubSingleton() { this->addToRegistry("SubSingleton", this); }
};

static SubSingleton singleton;

int main()
{
  Singleton::instance();
}
