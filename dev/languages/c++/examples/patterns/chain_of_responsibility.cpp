/***************************************************************
 * Chain of responsibility pattern example.
 *
 * In this pattern, the classes inherit a common interface which
 * contains functions for handling requests.
 *
 * When asking to handle a request an instance will eventually
 * delegate to another instance, making in that the chain of
 * reponsibility.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Handler
{
public:
  Handler(Handler* s) : successor(s) {}
  virtual ~Handler() {}
  virtual void handleRequest() = 0;
protected:
  Handler* successor;
};

class ConcreteHandler1 : public Handler
{
public:
  ConcreteHandler1(Handler* s = 0) : Handler(s) {}
  virtual void handleRequest() 
  { 
    std::cout << "ConcreteHandler1::handleRequest()." << std::endl; 
    if (this->successor)
      this->successor->handleRequest();
  }
};

class ConcreteHandler2 : public Handler
{
public:
  ConcreteHandler2(Handler* s = 0) : Handler(s) {}
  virtual void handleRequest() 
  {
    std::cout << "ConcreteHandler2::handleRequest()." << std::endl; 
    if (this->successor)
      this->successor->handleRequest();
  }
};

int main()
{
  ConcreteHandler1 h1;
  ConcreteHandler2 h2(&h1);
  Handler* h = new ConcreteHandler1(&h2);

  h->handleRequest();

  delete h;
}
