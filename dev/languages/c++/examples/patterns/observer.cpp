/***************************************************************
 * Observer pattern example.
 *
 * The Observer pattern is used to get callback from objects on
 * a set of events.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>
#include <set>

class Observer
{
public:
  virtual ~Observer() {}
  virtual void update() = 0;
};

class Subject
{
public:
  virtual ~Subject() {}
  void attach(Observer* o) { this->observers.insert(o); }
  void detach(Observer* o) { this->observers.erase(o); }
  void notify() 
  {
    for (std::set<Observer*>::iterator i = this->observers.begin() ; i != this->observers.end() ; ++i)
      (*i)->update();
  }
private:
  std::set<Observer*> observers;
};

class ConcreteSubject : public Subject
{
public:
  ConcreteSubject(int s) : state(s) {}
  int getState() const { return this->state; }
  void setState(int s)
  {
    this->state = s;
    this->notify();
  }
private:
  int state;
};

class ConcreteObserver : public Observer
{
public:
  ConcreteObserver(ConcreteSubject* s) : subject(s) {}
  virtual void update()
  {
    std::cout << this << " ConcreteObserver::update() ; ConcreteSubject::state = " << this->subject->getState() << "." << std::endl;
  }
private:
  ConcreteSubject* subject;
};

int main()
{
  ConcreteSubject s(2);
  ConcreteObserver o1(&s);
  ConcreteObserver o2(&s);

  s.attach(&o1);
  s.attach(&o2);

  s.setState(5);
  s.setState(10);

  s.detach(&o1);
  s.detach(&o2);
}
