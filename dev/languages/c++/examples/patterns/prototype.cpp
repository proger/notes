/***************************************************************
 * Prototype pattern example.
 *
 * A prototype class, defines a function for cloning itself.
 * Any subclass will override this function in order to clone
 * itself.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Prototype
{
public:
  virtual Prototype* clone() = 0;
  virtual std::string getName() const = 0;
  virtual ~Prototype() {}
};

class ConcretePrototype1 : public Prototype
{
public:
  virtual Prototype* clone() { return new ConcretePrototype1(); }
  virtual std::string getName() const { return "I'm a prototype 1."; }
};

class ConcretePrototype2 : public Prototype
{
public:
  virtual Prototype* clone() { return new ConcretePrototype2(); }
  virtual std::string getName() const { return "I'm a prototype 2."; }
};

int main()
{
  Prototype* prototype1 = new ConcretePrototype1();
  Prototype* prototype2 = new ConcretePrototype2();

  Prototype* object = prototype1->clone();
  std::cout << object->getName() << std::endl;
  delete object;
  object =  prototype2->clone();
  std::cout << object->getName() << std::endl;
  delete object;

  delete prototype1;
  delete prototype2;
}
