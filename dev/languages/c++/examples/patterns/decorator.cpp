/***************************************************************
 * Decorator pattern example.
 *
 * A decorator class adds a new behaviour to an existing class,
 * by inheriting from a common interface, while including as a
 * member an instance of the existing class.
 *
 * The main interest is that the decorator is itself part of 
 * the hierarchy of the existing class, and is used at run-time
 * to add a functionality to an object.
 *
 * When called on a function of the interface, the decorator
 * will run its own code as well as the code of the member
 * instance of the old class.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Component
{
public:
  virtual void operation() = 0;
  virtual ~Component() {}
};

class ConcreteComponent : public Component
{
public:
  virtual void operation() { std::cout << "ConcreteComponent::operation()." << std::endl; }
};

class Decorator : public Component
{
public:
  Decorator(Component* c) : component(c) {}
  virtual ~Decorator() = 0;
  virtual void operation() 
  {
    std::cout << "Decorator::operation()." << std::endl; 
    this->component->operation();
  }
private:
  Component* component;
};

Decorator::~Decorator() {}

class ConcreteDecoratorA : public Decorator
{
public:
  ConcreteDecoratorA(Component* c) : Decorator(c) {}
  virtual void operation() 
  {
    std::cout << "ConcreteDecoratorA::operation()." << std::endl; 
    this->Decorator::operation();
    this->addedBehavior();
  }
private:
  void addedBehavior() { std::cout << "ConcreteDecoratorA::addedBehavior()." << std::endl; }
};

class ConcreteDecoratorB : public Decorator
{
public:
  ConcreteDecoratorB(Component* c) : Decorator(c) {}
  virtual void operation() 
  {
    std::cout << "ConcreteDecoratorB::operation()." << std::endl; 
    this->Decorator::operation();
    this->addedBehavior();
  }
private:
  void addedBehavior() { std::cout << "ConcreteDecoratorB::addedBehavior()." << std::endl; }
};

int main()
{
  ConcreteComponent  component;
  ConcreteDecoratorB decorated_component(&component);

  decorated_component.operation();
}
