/***************************************************************
 * Abstract factory pattern example.
 *
 * An abstract factory is a class which centralizes the instanciation of related or dependent objects (hierarchy of objects).
 *
 * Each derived factory will instanciate a different kind of objects (in a different hierarchy tree).
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class AbstractProductA
{
public:
  virtual ~AbstractProductA() {}
  virtual std::string getName() = 0;
};

struct ProductA1 : public AbstractProductA
{
  virtual std::string getName() { return "I'm Product A1."; }
};

struct ProductA2 : public AbstractProductA
{
  virtual std::string getName() { return "I'm Product A2."; }
};

class AbstractProductB
{
public:
  virtual ~AbstractProductB() {}
  virtual std::string getName() = 0;
};

struct ProductB1 : public AbstractProductB
{
  virtual std::string getName() { return "I'm Product B1."; }
};

struct ProductB2 : public AbstractProductB
{
  virtual std::string getName() { return "I'm Product B2."; }
};

class AbstractFactory
{
public:
  virtual ~AbstractFactory() {}
  virtual AbstractProductA* CreateProductA() = 0;
  virtual AbstractProductB* CreateProductB() = 0;
};

class ConcreteFactory1 : public AbstractFactory
{
  virtual AbstractProductA* CreateProductA() { return new ProductA1(); }
  virtual AbstractProductB* CreateProductB() { return new ProductB1(); }
};

class ConcreteFactory2 : public AbstractFactory
{
  virtual AbstractProductA* CreateProductA() { return new ProductA2(); }
  virtual AbstractProductB* CreateProductB() { return new ProductB2(); }
};

void do_use_factory(AbstractFactory& factory)
{
  AbstractProductA* a = factory.CreateProductA();
  AbstractProductB* b = factory.CreateProductB();

  std::cout << "do use factory : " << std::endl;
  std::cout << a->getName() << std::endl;
  std::cout << b->getName() << std::endl;
  std::cout << std::endl;

  delete a;
  delete b;
}

int main()
{
  ConcreteFactory1 factory1;
  ConcreteFactory2 factory2;

  do_use_factory(factory1);
  do_use_factory(factory2);
}
