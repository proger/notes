/***************************************************************
 * Flyweight pattern example.
 *
 * A Flyweight class is used when one needs to create a huge
 * amount of instances of the same class, but can't do it because
 * of memory/system limitations.
 *
 * The aim is to create only a small number of these instances
 * and share them between all client classes that use them.
 *
 * The Flyweight, being shared by different clients which have
 * different needs, can only store general information. Specific
 * information linked to the use of the Flyweight class, is
 * stored by the client itself and passed to the Flyweight class
 * when neeed (i.e.: when calling one of its functions).
 *
 * A Factory class is responsible for creating and accessing the
 * Flyweight instances, when needed by the clients.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>
#include <map>

class Flyweight
{
public:
  virtual void operation(int extrinsicState) = 0;
  virtual ~Flyweight() {}
};

class ConcreteFlyweight : public Flyweight
{
public:
   ConcreteFlyweight(int s) : intrinsicState(s) {}
  virtual void operation(int extrinsicState) { std::cout << "ConcreteFlyweight::operation(" << extrinsicState << "), intrinsic state = " << this->intrinsicState << std::endl; }
private:
  int intrinsicState;
};

class UnsharedConcreteFlyweight : public Flyweight
{
public:
  virtual void operation(int extrinsicState) { std::cout << "UnsharedConcreteFlyweight::operation(" << extrinsicState << ")" << std::endl; }
};

class FlyweightFactory
{
public:
  // create if necessary and return shared Flyweight objects (ConcreteFlyweight)
  Flyweight* getFlyweight(int key)
  {
    if ( ! this->flyweights[key])
      this->flyweights[key] = new ConcreteFlyweight(key);
    return this->flyweights[key];
  }
private:
  std::map<int, Flyweight*> flyweights;
};

int main()
{
  FlyweightFactory factory;

  UnsharedConcreteFlyweight unshared_flyweight;
  unshared_flyweight.operation(3);

  Flyweight* shared_flyweight;
  shared_flyweight = factory.getFlyweight(2);
  shared_flyweight->operation(1);
  shared_flyweight = factory.getFlyweight(5);
  shared_flyweight->operation(2);
  shared_flyweight->operation(0);
}
