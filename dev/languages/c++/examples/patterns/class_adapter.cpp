/***************************************************************
 * Adapter pattern example.
 * Class adapter flavour.
 *
 * A class adapter is a class inheriting privately from another
 * class, and implementing a specific interface.
 *
 * The adapter has the same role as the mother class, except
 * that it present another interface than its mother class. So
 * it is used to integrate a existing class into an existing
 * framework which excepts a different interface from this class.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Target
{
public:
  virtual void request() = 0;
  virtual ~Target() {}
};

class Adaptee
{
public:
  void specificRequest() { std::cout << "Calling Adaptee::specificRequest()." << std::endl; }
  virtual ~Adaptee() {}
};

class Adapter : public Target, private Adaptee
{
public:
  virtual void request() { this->specificRequest(); }
};

int main()
{
  Adapter adapter;

  adapter.request();
}
