/***************************************************************
 * Bridge pattern example.
 *
 * In this pattern, a new class is created in order to hide
 * another class, as a private member.
 *
 * User of the new class will not know which instance of the
 * hidden class is created (possibly any derived class can be
 * used).
 * 
 * Functions are created in this new class, to call the hidden
 * class functions.
 *
 * The new class can then be derived, and thus gives birth to a
 * new tree of inheritance.
 *
 * Benefits:
 *   _ allows to swith of object implementation at run-time.
 *   _ changes in the implementation doesn't impact clients.
 *   _ in C++, allows to hide your interface code (implementation).
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Implementor
{
public:
  virtual void operationImpl() = 0;
  virtual ~Implementor() {}
};

class ConcreteImplementorA : public Implementor
{
public:
  virtual void operationImpl() { std::cout << "Calling ConcreteImplementorA::operationImpl()." << std::endl; }
};

class ConcreteImplementorB : public Implementor
{
public:
  virtual void operationImpl() { std::cout << "Calling ConcreteImplementorB::operationImpl()." << std::endl; }
};

class Abstraction
{
public:
  Abstraction() : impl(new ConcreteImplementorB()) {}
  void operation() { this->impl->operationImpl(); };
private:
  Implementor* impl;
};

// Eventually, one can derive Abstraction to implement specific behavior.
class RefinedAbstraction : public Abstraction
{
};

int main()
{
  Abstraction abstraction;

  abstraction.operation();
}
