/***************************************************************
 * Template method pattern example.
 *
 * The Template Method is a method which defines an algorithm
 * but lets sub-classes with the opportunity to add personalised
 * behaviour at some points of the algorithm.
 *
 * The overall structure of the algorithm is thus preserved, but
 * sub-classes are able to refine it.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class AbstractClass
{
public:
  void templateMethod() 
  {
    std::cout << "templateMethod() : do something, step 1" << std::endl;
    this->primitiveOperation1();
    std::cout << "templateMethod() : do something, step 2" << std::endl;
    this->primitiveOperation2();
    std::cout << "templateMethod() : do something, step 3" << std::endl;
  }
  virtual void primitiveOperation1() = 0;
  virtual void primitiveOperation2() = 0;
};

class ConcreteClass : public AbstractClass
{
public:
  virtual void primitiveOperation1() { std::cout << "ConcreteClass::primitiveOperation1()" << std::endl; }
  virtual void primitiveOperation2() { std::cout << "ConcreteClass::primitiveOperation2()" << std::endl; }
};

int main()
{
  ConcreteClass c;

  c.templateMethod();
}
