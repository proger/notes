/***************************************************************
 * Iterator pattern example.
 *
 * The Iterator pattern is used to access sequentially elements
 * of an object.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Iterator;

class Agregator
{
public:
  virtual ~Agregator() {}
  virtual Iterator* createIterator() = 0;
  virtual int size() = 0;
  virtual int get(int) = 0;
};

class Iterator
{
public:
  virtual ~Iterator() {}
  virtual void first() = 0;
  virtual void next() = 0;
  virtual bool isDone() = 0;
  virtual int currentItem() = 0;
};

class ConcreteIterator : public Iterator
{
public:
  ConcreteIterator(Agregator* a) : agregator(a) {}
  virtual void first() { this->current = 0; }
  virtual void next() { ++this->current; }
  virtual bool isDone() { return this->current >= this->agregator->size(); }
  virtual int currentItem() { return this->agregator->get(this->current); }
private:
  Agregator* agregator;
  int current;
};

class ConcreteAgregator : public Agregator
{
public:
  ConcreteAgregator(int s) : _size(s), array(0) 
  { 
    this->array = new int[s]; 
    for (int i = 0 ; i < s ; ++i)
      this->array[i] = i;
  }
  ~ConcreteAgregator() { delete this->array; }
  virtual Iterator* createIterator() { return new ConcreteIterator(this); }
  virtual int size() { return this->_size; }
  virtual int get(int i) { return this->array[i]; }
private:
  int* array;
  const int _size;
};

int main()
{
  ConcreteAgregator a(10);

  Iterator* i = a.createIterator();
  for (i->first() ; ! i->isDone() ; i->next())
    std::cout << i->currentItem() << std::endl;

  delete i;
}
