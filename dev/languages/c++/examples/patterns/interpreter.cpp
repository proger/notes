/***************************************************************
 * Interpreter pattern example.
 *
 * The Interpreter pattern is used to evaluate an expression of
 * a language, given its grammar.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

// this class is typically used to store values of variables for a specific evaluation
class Context
{
public:
  Context(int i) : n(i) {}
  int get() const { return this->n; }
private:
  int n;
};

class AbstractExpression
{
public:
  virtual ~AbstractExpression() {}
  virtual void interpret(const Context&) = 0;
};

class TerminalExpression : public AbstractExpression
{
public:
  virtual void interpret(const Context& c) { std::cout << "TerminalExpression::interpret() in context " << c.get() << "." << std::endl; }
};

class NonterminalExpression : public AbstractExpression
{
public:
  NonterminalExpression(AbstractExpression* e) : expression(e) {}
  virtual void interpret(const Context& c) 
  { 
    std::cout << "NonterminalExpression::interpret() in context " << c.get() << "." << std::endl; 
    this->expression->interpret(c);
  }
private:
  AbstractExpression* expression;
};

int main()
{
  Context c1(1);
  Context c2(2);
  TerminalExpression e2;
  NonterminalExpression e1(&e2);
  NonterminalExpression e(&e1);

  e.interpret(c1);
  e.interpret(c2);
}
