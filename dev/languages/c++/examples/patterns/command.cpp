/***************************************************************
 * Command pattern example.
 *
 * A Command class hides the logic of a command that is
 * applied on one or more receiver objects.
 *
 * It permits to factorizes the logic of operations into objects,
 * allowing by that operations like undo, redo, or other complex
 * operations commonly applied on a group of objects.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Receiver
{
public:
  void action() { std::cout << "Receiver::action()." << std::endl; }
};

class Command
{
public:
  virtual ~Command() {}
  virtual void execute() = 0;
};

class ConcreteCommand : public Command
{
public:
  ConcreteCommand(Receiver* r) : receiver(r) {}
  virtual void execute() 
  {
    std::cout << "ConcreteCommand::execute()." << std::endl; 
    this->receiver->action();
  }
private:
  Receiver* receiver;
};

class Invoker
{
public:
  void storeCommand(Command* c) { this->command = c; }
  void invoke() 
  { 
    std::cout << "Invoker::invoke()." << std::endl;
    this->command->execute(); 
  }
private:
  Command* command;
};

int main()
{
  Receiver r;
  ConcreteCommand c(&r);
  Invoker i;

  i.storeCommand(&c);

  i.invoke();
}
