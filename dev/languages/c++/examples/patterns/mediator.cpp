/***************************************************************
 * Mediator pattern example.
 *
 * The Mediator class encapsulates objects and their relations.
 * Letting each object ignore others.
 *
 * The relation between the set of objects is then centralized
 * in one place.
 *
 * A Mediator hierarchy class tree can then be created, letting
 * us choose between different mediators implementing different
 * interactions between the set of objects.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Colleague;

class Mediator
{
public:
  virtual void createColleagues() = 0;
  virtual void notify(Colleague*) = 0;
};

class Colleague
{
public:
  Colleague(Mediator* m) : mediator(m) {}
  virtual ~Colleague() = 0;
  void notify() 
  { 
    std::cout << "Colleague::notify()." << std::endl;
    this->mediator->notify(this); 
  }
private:
  Mediator* mediator;
};
Colleague::~Colleague() {}

class ConcreteColleague1 : public Colleague
{
public:
  ConcreteColleague1(Mediator* m) : Colleague(m) {}
  int getState() const 
  {
    std::cout << "ConcreteColleague1::getState()." << std::endl;
    return 1; 
  }
};

class ConcreteColleague2 : public Colleague
{
public:
  ConcreteColleague2(Mediator* m) : Colleague(m) {}
  void setState(int i) const { std::cout << "ConcreteColleague2::setState(" << i << ")." << std::endl; }
};

class ConcreteMediator : public Mediator
{
public:
  ConcreteMediator() : c1(0), c2(0) {}
  virtual void createColleagues() 
  { 
    c1 = new ConcreteColleague1(this);
    c2 = new ConcreteColleague2(this);
  }
  virtual void notify(Colleague* c)
  {
    std::cout << "ConcreteMediator::notify()." << std::endl;
    if (c == c1)
      if (c1->getState())
	c2->setState(3);
  }
public: // make variables public in order to allow main() to access c1
  ConcreteColleague1* c1;
  ConcreteColleague2* c2;
};

int main()
{
  ConcreteMediator m;

  m.createColleagues();
  m.c1->notify();
}
