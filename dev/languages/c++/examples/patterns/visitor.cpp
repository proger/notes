/***************************************************************
 * Visitor pattern example.
 *
 * The Visitor pattern allows an external class (Visitor) to
 * visit each element (executing an operation for each of them)
 * of a complex structure (tree, graph, ...) without knowing
 * anything about its internal representation.
 *
 * This is a non-intrusive inspection.
 * The structure knows how to scroll through all of its element,
 * and the visitor is only called for each element.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>
#include <vector>

class ConcreteElementA;
class ConcreteElementB;

class Visitor
{
public:
  virtual ~Visitor() {}
  virtual void visitConcreteElementA(ConcreteElementA*) = 0;
  virtual void visitConcreteElementB(ConcreteElementB*) = 0;
};

class Element
{
public:
  virtual ~Element() = 0;
  virtual void accept(Visitor* v)
  {
    for (unsigned int i = 0 ; i < this->children.size() ; ++i)
      this->children[i]->accept(v);
  }
  void add(Element* e) { this->children.push_back(e); }
private:
  std::vector<Element*> children;
};

Element::~Element() 
{
  for (unsigned int i = 0 ; i < this->children.size() ; ++i)
    delete this->children[i];
}

class ConcreteElementA : public Element
{
public:
  virtual void accept(Visitor* v)
  {
    v->visitConcreteElementA(this);
    this->Element::accept(v);
  }
};

class ConcreteElementB : public Element
{
public:
  virtual void accept(Visitor* v)
  {
    v->visitConcreteElementB(this);
    this->Element::accept(v);
  }
};

class ConcreteVisitor1 : public Visitor
{
public:
  virtual void visitConcreteElementA(ConcreteElementA* e) { std::cout << "ConcreteVisitor1::visitConcreteElementA(" << e << ")" << std::endl; }
  virtual void visitConcreteElementB(ConcreteElementB* e) { std::cout << "ConcreteVisitor1::visitConcreteElementB(" << e << ")" << std::endl; }
};

class ConcreteVisitor2 : public Visitor
{
public:
  virtual void visitConcreteElementA(ConcreteElementA* e) { std::cout << "ConcreteVisitor2::visitConcreteElementA(" << e << ")" << std::endl; }
  virtual void visitConcreteElementB(ConcreteElementB* e) { std::cout << "ConcreteVisitor2::visitConcreteElementB(" << e << ")" << std::endl; }
};

int main()
{
  Element* root = new ConcreteElementA();
  Element* e1 = new ConcreteElementB();
  Element* e2 = new ConcreteElementA();
  e1->add(e2);
  e2->add(new ConcreteElementA());
  e2->add(new ConcreteElementB());
  e2->add(new ConcreteElementB());
  e2->add(new ConcreteElementA());
  e1->add(new ConcreteElementA());
  root->add(e1);

  ConcreteVisitor1 v1;
  ConcreteVisitor2 v2;

  root->accept(&v1);
  root->accept(&v2);

  delete root;
}
