/***************************************************************
 * Facade pattern example.
 *
 * A Facade class centralizes other objects creation and access,
 * and factorizes operations run on other objects, by presenting
 * a much simpler interface, hiding all details of the
 * sub-system.
 *
 * It can be viewed as the API of a system.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class SubsystemComponent1
{
public:
  void operation() { std::cout << "SubsystemComponent1::operation()." << std::endl; }
};

class SubsystemComponent2
{
public:
  void operation() { std::cout << "SubsystemComponent2::operation()." << std::endl; }
};

class SubsystemComponent3
{
public:
  void operation() { std::cout << "SubsystemComponent3::operation()." << std::endl; }
};

class Facade
{
public:
  void operation()
  {
    std::cout << "Facade::operation()." << std::endl; 
    SubsystemComponent1 component1;
    SubsystemComponent2 component2;
    SubsystemComponent3 component3;

    component1.operation();
    component2.operation();
    component3.operation();
  }
};

int main()
{
  Facade facade;

  facade.operation();
};
