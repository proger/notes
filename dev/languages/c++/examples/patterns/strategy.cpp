/***************************************************************
 * Strategy pattern example.
 *
 * Presents algorithms as a hierarchy tree of classes. Making
 * them interchangeable.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Strategy
{
public:
  virtual ~Strategy() {}
  virtual void algorithmInterface() = 0;
};

class ConcreteStrategyA : public Strategy
{
public:
  virtual void algorithmInterface() { std::cout << "ConcreteStrategyA::algorithmInterface()." << std::endl; }
};

class ConcreteStrategyB : public Strategy
{
public:
  virtual void algorithmInterface() { std::cout << "ConcreteStrategyB::algorithmInterface()." << std::endl; }
};

class ConcreteStrategyC : public Strategy
{
public:
  virtual void algorithmInterface() { std::cout << "ConcreteStrategyC::algorithmInterface()." << std::endl; }
};

class Context
{
public:
  Context(Strategy* s) : strategy(s) {}
  void contextInterface() { this->strategy->algorithmInterface(); }
private:
  Strategy* strategy;
};

int main()
{
  ConcreteStrategyC s;
  Context c(&s);
  
  c.contextInterface();
}
