/***************************************************************
 * Builder pattern example.
 *
 * A builder pattern consists in 2 classes. One director which
 * directs the construction of an object, using an instance of
 * a builder interface.
 *
 * And a builder class which is reponsible for the details of
 * the construction.
 * 
 * So the director instance doesn't know which builder instance
 * is going to create the object.
 *
 * The builder interface can present the construction in several
 * steps (using several functions), and the director chooses
 * which functions to call. So the high level logic of the
 * creation process is captured inside the director class and
 * the builder interface.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Product
{
};

class Builder
{
public:
  virtual ~Builder() {}
  virtual void buildPart1() = 0;
  virtual void buildPart2() = 0;
  virtual void buildPart3() = 0;
  virtual Product* getProduct() const = 0;
};

class ConcreteBuilderA : public Builder
{
private:
  Product* product;
public:
  ConcreteBuilderA() : product(0) {}
  void buildPart1() { this->product = new Product; std::cout << "ConcreteBuilder1 builds part 1." << std::endl; }
  void buildPart2() { std::cout << "ConcreteBuilder1 builds part 2." << std::endl; }
  void buildPart3() { std::cout << "ConcreteBuilder1 builds part 3." << std::endl; }
  Product* getProduct() const { return this->product; }
};

class Director
{
private:
  Builder* builder;
public:
  Director(Builder* b) : builder(b) {}
  Product* construct()
  {
    this->builder->buildPart1();
    this->builder->buildPart2();
    this->builder->buildPart3();
    return builder->getProduct();
  }
};

int main()
{
  Product*		product;
  ConcreteBuilderA	builder;
  Director		director(&builder);

  product = director.construct();
}
