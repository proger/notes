/***************************************************************
 * Proxy pattern example.
 *
 * A Proxy class instance controls the access to another class.
 *
 * In the typical situation, the Proxy class derives from the
 * same mother class as the objects it wants to control access
 * to.
 *
 * The controled object is stored as a private member.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Subject
{
public:
  virtual void request() = 0;
  virtual ~Subject() {}
};

class RealSubject : public Subject
{
public:
  virtual void request() { std::cout << "RealSubject::request()" << std::endl; }
};

class Proxy : public Subject
{
public:
  Proxy(Subject* s) : real_subject(s) {}
  virtual void request()
  { 
    std::cout << "Proxy::request()" << std::endl; 
    this->real_subject->request();
  }
private:
  Subject* real_subject;
};

int main()
{
  RealSubject real_subject;
  Subject* subject = new Proxy(&real_subject);

  subject->request();

  delete subject;
}
