/***************************************************************
 * Singleton pattern example.
 *
 * Centralizes and protects access to a single instance of a
 * class.
 *
 * It ensures that the class will have one and only one instance.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 ***************************************************************/

#include <iostream>
#include <string>

class Singleton
{
public:
  static Singleton* instance()
  {
    if ( ! _instance)
      _instance = new Singleton();
    return _instance;
  }
protected:
  Singleton() {}
private:
  static Singleton* _instance;
};

Singleton* Singleton::_instance = 0;

int main()
{
  Singleton::instance();
}
