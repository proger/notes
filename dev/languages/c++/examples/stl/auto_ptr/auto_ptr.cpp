#include <memory>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////
// use of auto_ptr in a class
class Element {
public:
	Element() {std::cout << "Element constructor." << std::endl;	}
	~Element() {	std::cout << "Element destructor." << std::endl; }
};

class Object {
public:
	Object() : ptr(new Element) {
		std::cout << "Object constructor." << std::endl;
	}
	~Object() {	std::cout << "Object destructor." << std::endl;	}
private:
	const std::auto_ptr<Element> ptr;
};

////////////////////////////////////////////////////////////////////////////////
// use of auto_ptr in a function
std::auto_ptr<Element> sub_func() {
	std::auto_ptr<Element> ptr(new Element);
	std::cout << "Run sub_func." << std::endl;
	return ptr;
}

void func() {
	std::cout << "Enter func." << std::endl;
	std::auto_ptr<Element> p;

	for (int i = 0 ; i < 3 ; ++i)
		p = sub_func();

	std::cout << "Leave func." << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
// MAIN
int main() {

	// auto_ptr in a function
	std::cout << std::endl << "Run a function which uses auto_ptr." << std::endl;
	func();

	// auto_ptr in a class
	std::cout << std::endl << "create an object which uses an auto_ptr on an member" << std::endl;
	{	Object o;	}

	return 0;
}
