#include <vector>
#include <iostream>
#include <numeric>

int main(int argc, char* argv[]) {

	// sum of an array
	int a[] = {1, 2, 3, 4, 5};
	int n = sizeof(a)/sizeof(int);
	std::cout << "sum of elements of an array = " << std::accumulate(a, a + n, 0) << std::endl;

	// sum of an STL vector
	std::vector<int> v(5, 1);
	std::cout << "sum of elements of an STL vector = " << std::accumulate(v.begin(), v.end(), 0) << std::endl;

	return 0;
}
