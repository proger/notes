/***************************************************************
 * Example for reading UTF-8 chars with wcin string class.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 * Inspired by "Exceptional C++, Items 2 & 3"
 ****************************************************************/

#include <iostream>
#include <string>
#include <fstream>

int main()
{
  std::locale::global(std::locale(""));

  std::wstring ws;
  std::wcin >> ws;
  std::wcout << ws << std::endl;
}
