/***************************************************************
 * Example of case insensitive string class.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 * Inspired by "Exceptional C++, Items 2 & 3"
 ****************************************************************/

#include <iostream>
#include <string>

// case insensitive char traits
struct ci_char_traits : public std::char_traits<char>
{
  static bool eq(char c1, char c2) { return toupper(c1) == toupper(c2); }
  static bool lt(char c1, char c2) { return toupper(c1) < toupper(c2); }
  static int compare(const char* s1, const char* s2, size_t n)
  {
    for (int i = 0 ; i < n ; ++i, ++s1, ++s2)
      {
	if (toupper(*s1) < toupper(*s2))
	  return -1;
	else if (toupper(*s1) > toupper(*s2))
	  return 1;
      }
    return 0;
  }
  static const char* find(const char* s, int n, char a)
  {
    while (n-- > 0 && toupper(*s) != toupper(a))
      ++s;
    return n >= 0 ? s : 0;
  }
};

// case insensitive string
typedef std::basic_string<char, ci_char_traits> ci_string;

int main()
{
  ci_string s1 = "abc";
  ci_string s2 ="aBC";

  // in the following use of std::cout and operator <<, we can't write directly "std::cout << s1" because there isn't any operator << taking a ci_string object defined. So we use the const char* pointer returned by c_str() instead.
  std::cout << "\"" << s1.c_str() << "\" == \"" << s2.c_str() << "\" is "
	    << (s1 == s2 ? "true" : "false") << "." << std::endl;

  // remark : that would be the same when adding a ci_string to a string. There isn't any operator + taking a ci_string defined.
  std::string s = "sss";
  std::string t = "ttt";
  std::string u = s + t.c_str();
  std::cout << "\"" << s << "\" + \"" << t.c_str() << "\" = \"" << u << "\"." << std::endl; 
}
