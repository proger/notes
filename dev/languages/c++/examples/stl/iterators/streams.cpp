/***************************************************************
 * Example of iterators from streams.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 * Inspired by "Exceptional C++, Item 1"
 ****************************************************************/

#include <iostream>
#include <vector>
#include <iterator>

int main() {
	std::vector<int> v;

	std::cout << "Waiting integers from stdin using istream_iterator (end with CTRL+D) : " << std::endl;
	std::copy(std::istream_iterator<int>(std::cin),
	          std::istream_iterator<int>(),
	          back_inserter(v));

	std::cout << std::endl << "Rewrite integers to stdout using ostream_iterator : " << std::endl;
	std::copy(v.begin(),
	          v.end(),
	          std::ostream_iterator<int>(std::cout, "\n"));
}
