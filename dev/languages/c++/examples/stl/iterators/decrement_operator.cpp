/***************************************************************
 * Example of bad use of decrement operator on an iterator.
 *
 * Written by Pierrick Roger (c) 2005 teddysoft.
 * Inspired by "Exceptional C++, Item 1"
 ****************************************************************/

#include <iostream>
#include <vector>

int main() {
	// getting the last element of a vector
  std::vector<int> v;
  v.push_back(2);
  std::cout << *(--v.end()) << std::endl; // ERROR !! v.end() can't be modified.
  std::cout << *(v.end() - 1) << std::endl;

  // of course back() is preferable
  std::cout << v.back() << std::endl;
}
