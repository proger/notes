/*
  PHILOSOPHERS PROBLEM

  The dining philosophers problem is summarized as five philosophers sitting at
  a table doing one of two things: eating or thinking. While eating, they are
  not thinking, and while thinking, they are not eating.
  The five philosophers sit at a circular table with a large bowl of rice in the
  center. A chopstick is placed in between each pair of adjacent philosophers,
  and as such, each philosopher has one chopstick to his left and one chopstick
  to his right. As rice is impossible to serve and eat with a single chopstick,
  it is assumed that a philosopher must eat with two chopsticks. Each
  philosopher can only use the chopsticks on his immediate left and immediate
  right.
 */

#include <iostream>
#include <vector>
#include <queue>
#include <numeric>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <unistd.h>
#include <stdlib.h>

// philosophers
#define NB_PHILOSOPHERS 5
#define NB_MAX_MEALS        2 // number of meals each philosopher must eat before leaving table
std::vector<int>                nb_meals(NB_PHILOSOPHERS);
typedef enum { thinking, eating } philosopher_state_t;
std::vector<philosopher_state_t>    philosopher_state(NB_PHILOSOPHERS);

// chopsticks
#define NB_CHOPSTICKS   NB_PHILOSOPHERS

// common mutex
boost::mutex mut;

////////////////////////////////////////////////////////////////////////////////
// RESOURCE HIERARCHY SOLUTION
/* Another simple solution is achieved by assigning a partial order to the
 * resources (the forks, in this case), and establishing the convention that all
 * resources will be requested in order, and released in reverse order, and that
 * no two resources unrelated by order will ever be used by a single unit of
 * work at the same time. Here, the resources (forks) will be numbered 1 through
 * 5, in some order, and each unit of work (philosopher) will always pick up the
 * lower-numbered fork first, and then the higher-numbered fork, from among the
 * two forks he plans to use. Then, he will always put down the higher numbered
 * fork first, followed by the lower numbered fork. In this case, if four of the
 * five philosophers simultaneously pick up their lower-numbered fork, only the
 * highest numbered fork will remain on the table, so the fifth philosopher will
 * not be able to pick up any fork. Moreover, only one philosopher will have
 * access to that highest-numbered fork, so he will be able to eat using two
 * forks. When he finishes using the forks, he will put down the
 * highest-numbered fork first, followed by the lower-numbered fork, freeing
 * another philosopher to grab the latter and begin eating.
While the resource hierarchy solution avoids deadlocks, it is not always
practical, especially when the list of required resources is not completely
known in advance. For example, if a unit of work holds resources 3 and 5 and
then determines it needs resource 2, it must release 5, then 3 before acquiring
2, and then it must re-acquire 3 and 5 in that order. Computer programs that
access large numbers of database records would not run efficiently if they were
required to release all higher-numbered records before accessing a new record,
making the method impractical for that purpose.
*/

std::vector<boost::shared_ptr<boost::mutex> >               chopstick_mutex(NB_CHOPSTICKS);

// PHILOSOPHER
void philosopher_using_resource_hierarchy(int id) {

    useconds_t d;

    // determine left and right chopsticks
    int left_chopstick_index = id == 0 ? NB_CHOPSTICKS - 1 : id - 1;
    int right_chopstick_index = id;

    // determine order of chopsticks request
    int first_chopstick, second_chopstick;
    if (left_chopstick_index < right_chopstick_index) {
        first_chopstick = left_chopstick_index;
        second_chopstick = right_chopstick_index;
    }
    else {
        first_chopstick = right_chopstick_index;
        second_chopstick = left_chopstick_index;
    }

    // philosopher enters
    {
        boost::lock_guard<boost::mutex> lock(mut);
        std::cout << "Philosopher number " << id << " sits down at the table" << std::endl;
    }
    
    // think & eat
    bool request_sent = false;
    do {
        // think a while
        d = (rand() % 30 + 10) * 1000 * 100; // in micro-seconds
        {
            boost::lock_guard<boost::mutex> lock(mut);
            std::cout << "Philosopher number " << id << " is thinking during " << d / 1000000.0 << "s" << std::endl;
        }
        usleep(d);

        // try to grab chopsticks
        if (chopstick_mutex[first_chopstick]->try_lock()) {
            if (chopstick_mutex[second_chopstick]->try_lock()) {
                d = (rand() % 20 + 10) * 1000 * 100; // in micro-seconds
                ++nb_meals[id];
                {
                    boost::lock_guard<boost::mutex> lock(mut);
                    std::cout << "Philosopher number " << id << " takes his meal number " << nb_meals[id] << " during " << d / 1000000.0 << "s" << std::endl;
                }
                usleep(d);
                chopstick_mutex[second_chopstick]->unlock();
            }
            chopstick_mutex[first_chopstick]->unlock();
        }
        boost::lock_guard<boost::mutex> lock(mut);

    } while (nb_meals[id] < NB_MAX_MEALS);
    
    // philosopher leaves
    {
        boost::lock_guard<boost::mutex> lock(mut);
        std::cout << "Philosopher number " << id << " leaves the table" << std::endl;
    }
}

// run solution
void run_resource_hierarchy_solution() {

    // reset
    nb_meals.assign(nb_meals.size(), 0);
    philosopher_state.assign(philosopher_state.size(), thinking);

    // create chopsticks mutexes: vector can only contains pointers on mutexes, since boost mutexes aren't copyable !
    for (int i = 0 ; i < NB_CHOPSTICKS ; ++i)
        chopstick_mutex[i] = boost::shared_ptr<boost::mutex>(new boost::mutex);

    std::cout << std::endl << "RESOURCE HIERARCHY SOLUTION" << std::endl;

    // create philosophers
    std::vector<boost::shared_ptr<boost::thread> > philosopher_threads(NB_PHILOSOPHERS);
    for(unsigned int i = 0 ; i < NB_PHILOSOPHERS ; ++i)
        philosopher_threads[i] = boost::shared_ptr<boost::thread>(new boost::thread(&philosopher_using_resource_hierarchy, i));

    // wait that all philosopher threads finish
    for(unsigned int i = 0 ; i < NB_PHILOSOPHERS ; ++i)
        philosopher_threads[i]->join();
}

////////////////////////////////////////////////////////////////////////////////
// WAITER SOLUTION
// The simplest solution is achieved by using a waiter. The philosophers must
// ask the waiter his permission to take the chopsticks.

std::vector<bool>               chopstick_used(NB_CHOPSTICKS);
boost::condition_variable cond;
bool waiter_ready;
std::queue<int>             eating_request_msg_queue; // contains indexes of philosophers who request authorization to eat
std::queue<int>             eating_done_msg_queue; // contains indexes of philosophers who signal that they are done eating

// PHILOSOPHER
void philosopher_with_waiter(int id) {

    useconds_t d;

    // philosopher enters
    {
        boost::lock_guard<boost::mutex> lock(mut);
        std::cout << "Philosopher number " << id << " sits down at the table" << std::endl;
    }

    // think & eat
    bool request_sent = false;
    do {
        // think a while
        d = (rand() % 30 + 10) * 1000 * 100; // in micro-seconds
        {
            boost::lock_guard<boost::mutex> lock(mut);
            std::cout << "Philosopher number " << id << " is thinking during " << d / 1000000.0 << "s" << std::endl;
        }
        usleep(d);

        // ask permission to eat
        if ( ! request_sent) {
            boost::lock_guard<boost::mutex> lock(mut);
            eating_request_msg_queue.push(id);
            request_sent = true;
        }

        // check if allowed to eat
        {
            mut.lock();
            if (philosopher_state[id] == eating) {
                d = (rand() % 20 + 10) * 1000 * 100; // in micro-seconds
                ++nb_meals[id];
                std::cout << "Philosopher number " << id << " takes his meal number " << nb_meals[id] << " during " << d / 1000000.0 << "s" << std::endl;
                mut.unlock();
                usleep(d);
                mut.lock();
                eating_done_msg_queue.push(id);
                request_sent = false;
            }
            mut.unlock();
        }
    } while (nb_meals[id] < NB_MAX_MEALS);

    // philosopher leaves
    {
        boost::lock_guard<boost::mutex> lock(mut);
        std::cout << "Philosopher number " << id << " leaves the table" << std::endl;
    }
}

// WAITER
void waiter() {
    {
        boost::lock_guard<boost::mutex> lock(mut);
        std::cout << "Waiter enters the dining room" << std::endl;
    }

    // end condition: checks that all philosophers have eaten
    while (std::accumulate(nb_meals.begin(), nb_meals.end(), 0) != NB_MAX_MEALS * NB_PHILOSOPHERS) {
 
        // check if a philosopher is done eating
        {
            boost::lock_guard<boost::mutex> lock(mut);
            while ( ! eating_done_msg_queue.empty()) {
                int philosopher_index = eating_done_msg_queue.front();
                eating_done_msg_queue.pop();

                // determine left and right chopsticks
                int left_chopstick_index = philosopher_index == 0 ? NB_CHOPSTICKS - 1 : philosopher_index - 1;
                int right_chopstick_index = philosopher_index;

                // free chopsticks
                chopstick_used[left_chopstick_index] = chopstick_used[right_chopstick_index] = false;

                // philosopher returns to thinking
                philosopher_state[philosopher_index] = thinking;
            }
        }

        // check eating request
        {
            boost::lock_guard<boost::mutex> lock(mut);
            if ( ! eating_request_msg_queue.empty()) {
                int philosopher_index = eating_request_msg_queue.front();
                eating_request_msg_queue.pop();

                // determine left and right chopsticks
                int left_chopstick_index = philosopher_index == 0 ? NB_CHOPSTICKS - 1 : philosopher_index - 1;
                int right_chopstick_index = philosopher_index;

                if ( ! chopstick_used[left_chopstick_index] && ! chopstick_used[right_chopstick_index]) {

                    // reserve chopsticks
                    chopstick_used[left_chopstick_index] = chopstick_used[right_chopstick_index] = true;

                    // philosopher is eating
                    philosopher_state[philosopher_index] = eating;
                }

                // chopsticks aren't available, make philosopher wait for eating
                else
                    eating_request_msg_queue.push(philosopher_index);
            }
        }
    }

    {
        boost::lock_guard<boost::mutex> lock(mut);
        std::cout << "All philosophers have finished taking their meals, waiter leaves the dining room" << std::endl;
    }
}

// run solution
void run_waiter_solution() {

    // reset
    nb_meals.assign(nb_meals.size(), 0);
    chopstick_used.assign(chopstick_used.size(), false);
    philosopher_state.assign(philosopher_state.size(), thinking);

    std::cout << std::endl << "WAITER SOLUTION" << std::endl;

    // create waiter
    boost::thread waiter_thread(&waiter);

    // create philosophers
    std::vector<boost::shared_ptr<boost::thread> > philosopher_threads(NB_PHILOSOPHERS);
    for(unsigned int i = 0 ; i < NB_PHILOSOPHERS ; ++i)
        philosopher_threads[i] = boost::shared_ptr<boost::thread>(new boost::thread(&philosopher_with_waiter, i));

    // wait that all philosopher threads finish
    for(unsigned int i = 0 ; i < NB_PHILOSOPHERS ; ++i)
        philosopher_threads[i]->join();

    // wait that waiter thread finishes
    waiter_thread.join();
}

////////////////////////////////////////////////////////////////////////////////
// MAIN
int main(int argc, char* argv[]) {
    srand(time(0));
    run_resource_hierarchy_solution();
    run_waiter_solution();
    return 0;
}
