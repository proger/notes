#include <iostream>
#include <string>
#include <locale>
#include <ios>
#include <fstream>
#include <iconv.h>
#include <vector>
#include <stdlib.h>
#include <xlocale.h>

int main()
{
  std::wstring s;

  //  std::locale loc("");
  // std::locale::global(loc); // set default user locale global (will be used by wcin and wcout), so the locale has to be UTF-8 on this machine.
  //std::locale::global(std::locale());

  //  std::wcin.imbue(loc);
  // std::wcout.imbue(loc);
  while(std::wcin) {
    s = L"";
    std::wcin >> s;
    std::wcout << L"length=" << s.length() << std::endl;
    for (std::wstring::iterator i = s.begin() ; i != s.end() ; ++i)
      std::wcout << std::hex << static_cast<unsigned int>(*i) << L" ";
    std::wcout << std::endl;
    std::wcout << s <<std::endl;

    char* from = new char[s.length()];
    for(std::wstring::size_type i = 0 ; i < s.length() ; ++i)
      from[i] = (char)(s[i]);
    wchar_t* to = new wchar_t[s.length()];

    /*    iconv_t cd = iconv_open("wchar_t", "UTF-8");
    size_t from_bytesleft = s.length();
    size_t to_bytesleft = s.length();
    std::wcout << L"from_bytesleft=" << from_bytesleft << L" to_bytesleft=" << to_bytesleft << std::endl;
    size_t ret = iconv(cd, &from, &from_bytesleft, (char**)&to, &to_bytesleft);
    std::wcout << L"ret=" << ret << L" errno=" << errno << std::endl;
    std::wcout << L"ERRORS E2BIG=" << E2BIG << L" EILSEQ=" << EILSEQ << L" EINVAL=" << EINVAL << std::endl;
    std::wcout << L"from_bytesleft=" << from_bytesleft << L" to_bytesleft=" << to_bytesleft << std::endl;
    iconv_close(cd);
    std::wstring ws(to, s.length() - to_bytesleft);*/

    size_t ret = mbstowcs(to, from, s.length());
    std::wcout << L"ret=" << ret << L" errno=" << errno << std::endl;
    std::wcout << L"ERRORS E2BIG=" << E2BIG << L" EILSEQ=" << EILSEQ << L" EINVAL=" << EINVAL << std::endl;  
    std::wstring ws(to, ret);

    std::wcout << L"WS_LENGTH=" << ws.length() << std::endl;
    for (std::wstring::iterator i = ws.begin() ; i != ws.end() ; ++i)
      std::wcout << std::hex << static_cast<unsigned int>(*i) << L" ";
    std::wcout << std::endl;
    std::wcout << L"WS=" << ws << std::endl;
    }
}
