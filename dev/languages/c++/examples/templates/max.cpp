// use of promotion traits, for writing a max function
// DOES NOT WORK ---> this leads to an warning about temporary variables.
// for a solution, look at "Min and Max - Andrei Alexandrescu.pdf"

#include <iostream>

// IfThenElse
template<bool C, typename Ta, typename Tb> class IfThenElse;
template<typename Ta, typename Tb> class IfThenElse<true, Ta, Tb> { public: typedef Ta ResultT; };
template<typename Ta, typename Tb> class IfThenElse<false, Ta, Tb> { public: typedef Tb ResultT; };

#define GT(a, b) ((a) > (b))
#define LT(a, b) ((a) < (b))

// traits
template<typename T1, typename T2> class Promotion {
public:
	typedef typename IfThenElse<GT(sizeof(T1), sizeof(T2)),
	                            T1,
	                            typename IfThenElse<LT(sizeof(T1), sizeof(T2)), 
	                                                T2, 
	                                                void>::ResultT
	                            >::ResultT ResultT;
};

// specialization
template<typename T> class Promotion<T, T> {
public:
	typedef T ResultT;
};

double const& toto(double const& a, int const& b) { return a < b ? b : a; }

// max function
template<typename T1, typename T2> inline typename Promotion<T1, T2>::ResultT const& max(T1 const&a, T2 const&b) {
	return a < b ? b : a;
}

int main(int argc, char *argv[]) {

	std::cout << max(1, 2) << std::endl;
	int i = 2;
	double d = 3.5;
	std::cout << toto(d, i) << std::endl;
	std::cout << max(3.5, 2) << std::endl;
	std::cout << max(7, 7.5) << std::endl;
	std::cout << max(8.9, 7.5) << std::endl;
	std::cout << max(9, 7.5) << std::endl;
	/* ERROR:
	   max.cpp: In function ‘const typename Promotion<T1, T2>::ResultT& max(const T1&, const T2&) [with T1 = double, T2 = int]’:
	   max.cpp:38:   instantiated from here
	   max.cpp:32: warning: returning reference to temporary	 */

	return 0;
}
