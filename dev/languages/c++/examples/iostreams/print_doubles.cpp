#include <iostream>

void print_double(double x, int precision = -1, bool fixed = false) {

	int old_prec = 0;
	
	if (precision > 0)
		old_prec = std::wcout.precision(precision);
	if (fixed)
		std::wcout.setf( std::ios::fixed, std:: ios::floatfield ); 
	else
		std::wcout.unsetf( std:: ios::floatfield ); 
	std::wcout << "X (pre=" << precision << ", fixed=" << fixed << ")= " <<  x << L"\n";
	if (precision > 0)
		std::wcout.precision(old_prec);
}

int main(int argc, char* argv[]) {

	double a[] = {12.3, 0.071, 0.07099999};
	int pre_arr[] = {-1, 2, 3, 5};

	for(double x: a)
		for(int precision: pre_arr) {
		print_double(x, precision);
		print_double(x, precision, true);
	}

	return 0;
}
