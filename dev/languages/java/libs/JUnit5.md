# JUnit5

 * [JUnit 5](https://junit.org/junit5/).
 * [JUnit 5 - How to Run Test Methods in Order](https://www.codejava.net/testing/junit-tests-order).
 * [The Order of Tests in JUnit](https://www.baeldung.com/junit-5-test-order).

Maven:
```xml
<!-- JUnit5 https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine -->
<dependency>
	<groupId>org.junit.jupiter</groupId>
	<artifactId>junit-jupiter-engine</artifactId>
	<version>5.7.0</version>
	<scope>test</scope>
</dependency>

<!-- JUnit Jupiter API https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api -->
<dependency>
	<groupId>org.junit.jupiter</groupId>
	<artifactId>junit-jupiter-api</artifactId>
	<version>5.7.0</version>
	<scope>test</scope>
</dependency>
```

Run methods in alphabetical order:
```java
@org.junit.jupiter.api.TestMethodOrder(org.junit.jupiter.api.MethodOrderer.MethodName.class)
public class MyTest {
	// ...
}
```

## Customized runner

When a class is annotated with `@RunWith` or extends a class annotated with `@RunWith`, JUnit will invoke the class it references to run the tests in that class instead of the runner built into JUnit.
Under Ant task `junit`, by default, all classes are annotated with `@RunWith`(Enclosed.class), which has the effect of running JUnit on all inner classes of this class.
The class specified by `@RunWith` must extend `org.junit.runner.Runner`.

Usage:
```java
import org.junit.runner.RunWith;
@RunWith(org.junit.internal.runners.JUnit4ClassRunner.class)
class MyTestClass {
}
```

Running from command line with JUnit 4:
```sh
java -cp /usr/share/java/junit.jar org.junit.runner.JUnitCore TestClassName
```
Running from command line with JUnit 3:
```sh
java -cp /usr/share/java/junit.jar junit.textui.TestRunner TestClassName
```

## Running code before and after test methods

To run code before and after test class creation:
```java
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class MyTestClass {

	@BeforeClass
	public static void runSomeCodeBefore() {
	}

	@AfterClass
	public static void runSomeCodeAfter() {
	}
}
```
Several methods can be defined.

To run code before and after each test method of the class:
```java
import org.junit.After;
import org.junit.Before;

public class MyTestClass {

	@Before
	public void runSomeCodeBefore() {
	}

	@After
	public void runSomeCodeAfter() {
	}
}
```
Several methods can be defined.

