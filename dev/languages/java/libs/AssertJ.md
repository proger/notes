# AssertJ

 * [AssertJ](http://joel-costigliola.github.io/assertj/index.html).
 * [AssertJ fluent assertions API](https://www.javadoc.io/doc/org.assertj/assertj-core/latest/index.html).
 * [Assertions class](https://www.javadoc.io/doc/org.assertj/assertj-core/latest/org/assertj/core/api/Assertions.html).

Include AssertJ in Maven `pom.xml` dependencies:
```xml
<dependency>
	<groupId>org.assertj</groupId>
	<artifactId>assertj-core</artifactId>
	<version>3.18.0</version>
	<scope>test</scope>
</dependency>
```

Example with JUnit:
```java
import static org.assertj.core.api.Assertions.assertThat;

public class MyTest {

	@org.junit.Test
	public void testSomething() {
		// ...
		assertThat(mylist).hasSizeGreaterThanOrEqualTo(1);
	}
}
```

Object:
```java
assertThat(myObj).isNull();
assertThat(myObj).isNotNull();
```

List:
```java
assertThat(mylist).hasSizeGreaterThanOrEqualTo(1);
```

File:
```java
assertThat(new java.io.File("myfile.txt")).exists();
assertThat(new java.io.File("myfile.txt")).doesNotExist();
assertThat(new java.io.File("myfile.txt")).isEmpty();
assertThat(new java.io.File("myfile.txt")).isNotEmpty();
```

Test equality of two objects by comparing content:
```java
assertThat(obj1).usingRecursiveComparison().isEqualTo(obj2);
```

Test if two variables points to the same instance:
```java
assertThat(obj1).isEqualTo(obj2);
```

Test if two strings are equal:
```java
assertThat("abc").isEqualTo("abc");
```

Object:
```java
assertThat(myobj).isInstanceOf(MyClass.class);
```

Containers:
```java
assertThat(12).isIn(myList);
assertThat(myList).isEmpty();
assertThat(myList).hasSize(2);
```
