# JUnit4

 * [JUnit 4](https://junit.org/junit4/).

Assertions:
```java
import static org.junit.Assert.*;
assertTrue(a == 2);
```

Testing that an exception is thrown:
```java
@org.junit.Test(expected=IndexOutOfBoundsException.class)
public void testIndexOutOfBoundsException() {
	ArrayList emptyList = new ArrayList();
	Object o = emptyList.get(0);
}
```

Testing equality of particular objects:
```sh
assertTrue("Wrong.", myobj.equals(my_other_obj));
```
