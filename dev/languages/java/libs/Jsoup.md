# Jsoup

HTML parsing.

```java
org.jsoup.nodes.Document doc = org.jsoup.Jsoup.parse(htmlString);
content = doc.text();
```
