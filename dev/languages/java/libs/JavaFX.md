# Oracle JavaFX

 * [Getting Started with JavaFX](https://openjfx.io/openjfx-docs/).
 * [JavaFX API overview](https://docs.oracle.com/javase/8/javafx/api/overview-summary.html).
 * [Integrating JavaFX into Swing Applications](https://docs.oracle.com/javafx/2/swing/swing-fx-interoperability.htm).
 * [Integrating JavaFX into Swing Applications](https://docs.oracle.com/javase/8/javafx/interoperability-tutorial/swing-fx-interoperability.htm).
 * [JavaFX Video Player with MediaView](https://coderslegacy.com/java/javafx-mediaview-video-player/). Video player with play/pause/stop buttons.
 * [Embedding Swing Content in JavaFX Applications](https://docs.oracle.com/javafx/8/embed_swing/jfxpub-embed_swing.htm).

JavaFX can play mp4.

JavaFX must be installed with mod files that are specific for each platform (macos, Windows, Linux).
