# Opencsv

[Opencsv](http://opencsv.sourceforge.net).

Basic CSV parser.

```java
import au.com.bytecode.opencsv.CSVReader;
CSVReader reader = new CSVReader(new FileReader("yourfile.csv"));
String [] line;
while ((line = reader.readNext()) != null) {
	// line[] is an array of values from the line
	System.out.println(line[0] + line[1] + "etc...");
}
```
