import org.openscience.cdk.inchi.InChIToStructure;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomType;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.fingerprint.MACCSFingerprinter;
import org.openscience.cdk.atomtype.CDKAtomTypeMatcher;
import org.openscience.cdk.tools.manipulator.AtomTypeManipulator;
import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.AtomType;
import org.openscience.cdk.Bond;
import net.sf.jniinchi.INCHI_RET;

public class MoleculeFingerPrint {

	///////////////////////
	// INSTANTIATE INCHI //
	///////////////////////

	static IAtomContainer instantiateInchi(String inchi) throws CDKException {

		InChIToStructure parser = InChIGeneratorFactory.getInstance().getInChIToStructure(inchi, DefaultChemObjectBuilder.getInstance());

		INCHI_RET ret = parser.getReturnStatus();
		if (ret == INCHI_RET.WARNING) {
			// Structure generated, but with warning message
			System.err.println("InChI warning: " + parser.getMessage());
		} else if (ret != INCHI_RET.OKAY) {
			// Structure generation failed
			throw new CDKException("Structure generation failed: " + ret.toString() + " [" + parser.getMessage() + "]");
		}

		IAtomContainer container = parser.getAtomContainer();

		return container;
	}

	//////////
	// MAIN //
	//////////

	public static void main(String[] args) throws CDKException {

		// Build ethanol molecule
		IAtomContainer ethanol = instantiateInchi("InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3");

		// Set atom types
		CDKAtomTypeMatcher matcher = CDKAtomTypeMatcher.getInstance(ethanol.getBuilder());
		for (IAtom atom: ethanol.atoms()) {
			IAtomType type = matcher.findMatchingAtomType(ethanol, atom);
			AtomTypeManipulator.configure(atom, type);
		}

		// Make ethanol fingerprint
		CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(ethanol.getBuilder());
		adder.addImplicitHydrogens(ethanol);
		MACCSFingerprinter fingerprinter = new MACCSFingerprinter();
		System.out.println("ethanol fingerprint: " + fingerprinter.getBitFingerprint(ethanol).asBitSet());
	}
}
