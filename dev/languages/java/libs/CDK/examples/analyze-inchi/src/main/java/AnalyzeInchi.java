import org.openscience.cdk.inchi.InChIToStructure;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import net.sf.jniinchi.INCHI_RET;

public class AnalyzeInchi {

	///////////////////////
	// ANALYZE CONTAINER //
	///////////////////////

	static void analyzeContainer(IAtomContainer container) {

		// Atoms
		System.out.println("NB ATOMS = " + container.getAtomCount());
		for (IAtom atom: container.atoms()) {
			System.out.println("  Symbol = " + atom.getSymbol());
			System.out.println("  Formal charge = " + atom.getFormalCharge());
			System.out.println("  Hydrogens = " + atom.getImplicitHydrogenCount());
		}

		// Bonds
		System.out.println("NB BONDS = " + container.getBondCount());
	}

	///////////////////////
	// INSTANTIATE INCHI //
	///////////////////////

	static IAtomContainer instantiateInchi(String inchi) throws CDKException {

		InChIToStructure parser = InChIGeneratorFactory.getInstance().getInChIToStructure(inchi, DefaultChemObjectBuilder.getInstance());

		INCHI_RET ret = parser.getReturnStatus();
		if (ret == INCHI_RET.WARNING) {
			// Structure generated, but with warning message
			System.err.println("InChI warning: " + parser.getMessage());
		} else if (ret != INCHI_RET.OKAY) {
			// Structure generation failed
			throw new CDKException("Structure generation failed: " + ret.toString() + " [" + parser.getMessage() + "]");
		}

		IAtomContainer container = parser.getAtomContainer();

		return container;
	}

	/////////////
	// ANALYZE //
	/////////////

	static void analyze(String name, String inchi) throws CDKException {

		System.out.println("");
		System.out.println("================================");
		System.out.println(name);
		System.out.println(inchi);
		IAtomContainer container = instantiateInchi(inchi);
		analyzeContainer(container);
	}

	public static void main(String[] args) throws CDKException {
		analyze("methane", "InChI=1S/CH4/h1H4");
		analyze("methane+", "InChI=1S/CH4/h1H4/q+1");
		analyze("methane-", "InChI=1S/CH4/h1H4/q-1");
		analyze("acetone+", "InChI=1S/C3H6O/c1-3(2)4/h1-2H3/q+1");
	}
}
