import org.openscience.cdk.inchi.InChIToStructure;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import net.sf.jniinchi.INCHI_RET;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import org.openscience.cdk.silent.*;
import org.openscience.cdk.interfaces.*;
import org.openscience.cdk.layout.*;
import org.openscience.cdk.renderer.*;
import org.openscience.cdk.renderer.font.*;
import org.openscience.cdk.renderer.generators.*;
import org.openscience.cdk.renderer.visitor.*;
import org.openscience.cdk.templates.*;
import org.openscience.cdk.renderer.generators.BasicSceneGenerator.Margin;
import org.openscience.cdk.renderer.generators.BasicSceneGenerator.ZoomFactor;

public class Render2D {

	////////////
	// RENDER //
	////////////

	static void render(String inchi, String file_basename) throws CDKException, java.io.IOException {
		
		InChIToStructure parser = InChIGeneratorFactory.getInstance().getInChIToStructure(inchi, DefaultChemObjectBuilder.getInstance());

		INCHI_RET ret = parser.getReturnStatus();
		if (ret == INCHI_RET.WARNING) {
			// Structure generated, but with warning message
			System.err.println("InChI warning: " + parser.getMessage());
		} else if (ret != INCHI_RET.OKAY) {
			// Structure generation failed
			throw new CDKException("Structure generation failed: " + ret.toString() + " [" + parser.getMessage() + "]");
		}

		IAtomContainer container = parser.getAtomContainer();

		// Get all molecules defined inside the container
		IAtomContainerSet molecules = org.openscience.cdk.graph.ConnectivityChecker.partitionIntoMolecules(container);

		for (int i = 0 ; i < molecules.getAtomContainerCount() ; ++i) {

			IAtomContainer molecule = molecules.getAtomContainer(i);

			// Structure diagram generator XXX What is it ???
			StructureDiagramGenerator sdg = new StructureDiagramGenerator();
			sdg.setMolecule(molecule);
			sdg.generateCoordinates();
			molecule = sdg.getMolecule();

			// Generators XXX What are they ?
			java.util.List generators = new java.util.ArrayList();
			generators.add(new BasicSceneGenerator());
			generators.add(new BasicBondGenerator());
			generators.add(new BasicAtomGenerator());

			// Set drawing area
			int WIDTH = 300; // TODO issue when rectangle is too small to contain molecule. How to fit ?
			int HEIGHT = 250;
			Rectangle drawArea = new Rectangle(WIDTH, HEIGHT);

			// Renderer XXX What is it ?
			AtomContainerRenderer renderer = new AtomContainerRenderer(generators, new AWTFontManager());
//			renderer.setScale(molecule);
//			Rectangle diagramBounds = renderer.calculateDiagramBounds(molecule);
			renderer.setup(molecule, drawArea);
//			renderer.setZoomToFit(drawArea.getWidth(), drawArea.getHeight(), diagramBounds.getWidth(), diagramBounds.getHeight());

			// Model XXX What is it ?
//			RendererModel model = renderer.getRenderer2DModel();
//			model.set(ZoomFactor.class, (double)0.9);

			// Image
			Image image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2 = (Graphics2D)image.getGraphics();
			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, WIDTH, HEIGHT);
			renderer.paint(molecule, new AWTDrawVisitor(g2));
			ImageIO.write((RenderedImage)image, "PNG", new java.io.File(file_basename + (i == 0 ? "" : "-"+i) + ".png"));
		}
	}

	//////////
	// MAIN //
	//////////

	public static void main(String[] args) throws CDKException, java.io.IOException {
		render("InChI=1S/C3H6O/c1-3(2)4/h1-2H3/q+1", "acetone+");
		render("InChI=1S/C3H6O/c1-3(2)4/h1-2H3/q-1", "acetone-");
		render("InChI=1S/C3H6O/c1-3(2)4/h1-2H3", "acetone");
		render("InChI=1S/CH4/h1H4/q+1", "methane+");
		render("InChI=1S/CH4/h1H4", "methane");
		render("InChI=1S/C12H17N4OS/c1-8-11(3-4-17)18-7-16(8)6-10-5-14-9(2)15-12(10)13/h5,7,17H,3-4,6H2,1-2H3,(H2,13,14,15)/q+1", "thiamine1+");
	}
}

