import org.openscience.cdk.inchi.InChIToStructure;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomType;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.fingerprint.MACCSFingerprinter;
import org.openscience.cdk.atomtype.CDKAtomTypeMatcher;
import org.openscience.cdk.tools.manipulator.AtomTypeManipulator;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.isomorphism.UniversalIsomorphismTester;
import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.AtomType;
import org.openscience.cdk.Bond;
import net.sf.jniinchi.INCHI_RET;

public class SubStructureSearch {

	///////////////////////
	// INSTANTIATE INCHI //
	///////////////////////

	static IAtomContainer instantiateInchi(String inchi) throws CDKException {

		InChIToStructure parser = InChIGeneratorFactory.getInstance().getInChIToStructure(inchi, DefaultChemObjectBuilder.getInstance());

		INCHI_RET ret = parser.getReturnStatus();
		if (ret == INCHI_RET.WARNING) {
			// Structure generated, but with warning message
			System.err.println("InChI warning: " + parser.getMessage());
		} else if (ret != INCHI_RET.OKAY) {
			// Structure generation failed
			throw new CDKException("Structure generation failed: " + ret.toString() + " [" + parser.getMessage() + "]");
		}

		IAtomContainer container = parser.getAtomContainer();

		return container;
	}

	//////////
	// MAIN //
	//////////

	public static void main(String[] args) throws CDKException {

		// Build ethanol molecule
		IAtomContainer ethanol = instantiateInchi("InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3");

		// Set atom types
		CDKAtomTypeMatcher matcher = CDKAtomTypeMatcher.getInstance(ethanol.getBuilder());
		for (IAtom atom: ethanol.atoms()) {
			IAtomType type = matcher.findMatchingAtomType(ethanol, atom);
			AtomTypeManipulator.configure(atom, type);
		}

		// Add explicit hydrogens
		CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(ethanol.getBuilder());
		adder.addImplicitHydrogens(ethanol);
		AtomContainerManipulator.convertImplicitToExplicitHydrogens(ethanol);

		// Build alcohol group
		IAtomContainer alcohol = new AtomContainer();
		alcohol.addAtom(new Atom("O"));
		alcohol.addAtom(new Atom("H"));
		alcohol.addBond(new Bond(alcohol.getAtom(0), alcohol.getAtom(1)));
		
		// Set atom types
		matcher = CDKAtomTypeMatcher.getInstance(alcohol.getBuilder());
		for (IAtom atom: alcohol.atoms()) {
			IAtomType type = matcher.findMatchingAtomType(alcohol, atom);
			AtomTypeManipulator.configure(atom, type);
		}

		System.out.println("********************************************************************************");
		System.out.println("Use isSubgraph() method.");
		System.out.println("ALCOHOL IS SUBGRAPH OF ETHANOL: " + new UniversalIsomorphismTester().isSubgraph(ethanol, alcohol));

		// Now search for the alcohol group inside the ethanol molecule
		System.out.println("");
		System.out.println("********************************************************************************");
		System.out.println("Search for alcohol group \"by hand\".");
		for (IAtom atom: ethanol.atoms()) {
			System.out.println("A: " + atom.getSymbol());
			if (atom.getSymbol().equals(alcohol.getAtom(0).getSymbol())) {
				System.out.println("Found oxygen atom.");
				for (IBond bond: ethanol.getConnectedBondsList(atom)) {
					if (bond.getOrder() == IBond.Order.SINGLE) {
						System.out.println("Found single bond connected to oxygen atom.");
						System.out.println("B: " + bond.getConnectedAtom(atom).getSymbol());
						System.out.println("Bond atom count: " + bond.getAtomCount());
						for (int i = 0 ; i < bond.getAtomCount() ; ++i)
							System.out.println("Atom " + i + ": " + bond.getAtom(i).getSymbol());
						if (bond.getConnectedAtom(atom).getSymbol().equals(alcohol.getAtom(1).getSymbol()))
							System.out.println("FOUND ALCOHOL GROUP IN ETHANOL.");
					}
				}
			}
		}
	}
}
