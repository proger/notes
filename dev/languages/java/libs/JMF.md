# JMF

 * [How to play audio and video files in Java applications](https://buddhimawijeweera.wordpress.com/2011/05/01/how-to-play-audio-and-video-files-in-java-applications/). Uses JMF (Java Media Framework), which is deprecated and does not play mp4.

Java Media Framework. Deprecated.
Can play mpg and avi, but not mp4.
