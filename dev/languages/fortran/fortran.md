# Fortran

* COMMON defines global variables. 
      COMMON /LABEL/ VAR1, VAR2, VAR3
* The label is used to identify the global memory block.
* In another part of the program, we can then make reference to the global variables:
      COMMON /LABEL/ VAR1, VAR2, VAR3
* We can even change the name of variables, since only the order is important:
      COMMON /LABEL/ A, B, C

* Even such confusing declarations can be written:
      COMMON /OBTUSE/ X(3)
* And in another part of the program:
      COMMON /OBTUSE/ A, B, C       ! so A=X(1), B=X(2), C=X(3)

* In the same program unit, a COMMON block can be splitted in several part which will be concatenated by the compiler.
* Here block TWO contains 4 items:
      COMMON /ONE/ A, B, C, /TWO/ ALPHA, BETA, GAMMA
      COMMON /TWO/ DELTA

* STORAGE
* Each type of date occupy a certain storage unit: integer, real, and logical occupy 1 numerical storage unit, complex and double precision occupy 2 numerical storage units. Characater storage units are different and can't be mixed with numerical storage units inside a COMMON block:
      COMMON /PLOT/ OPENED, ORIGIN(2), PSCALE, NUMPEN
      LOGICAL OPENED
      INTEGER NUMPEN
      REAL PSCALE, ORIGIN
      SAVE /PLOT/             ! SAVE is used here, so the values aren't discarded between two calls of the same function or if another function is called later and use the same COMMON block. Not that this isn't necessary if the COMMON block is declared in a higher function in the callstack.
      COMMON /PLOTC/ TITLE
      CHARACTER TITLE*40
      SAVE /PLOTC/

*****************
* GLOBAL ARRAYS *
*****************
* The 3 following forms of declaration are equivalent:
      COMMON /DEMO/ ARRAY(5000)
      DOUBLE PRECISION ARRAY
*
      COMMON /DEMO/ ARRAY
      DOUBLE PRECISION ARRAY(5000)
*
      COMMON /DEMO/ ARRAY
      DOUBLE PRECISION ARRAY
      DIMENSION ARRAY(5000)

**********************
* BLANK COMMON BLOCK *
**********************
* The blank common block has no name, and can have different length in different program units:
      COMMON // DUMMY(10000)
* and in another
      COMPLEX SERIES(512,512), SLICE(512), EXPECT(1024)
      COMMON // SERIES, SLICE, EXPECT
* The blanck common block can't be initialized by a DATA statement.
* It never becomes undefined after a procedure exit.

**************
* BLOCK DATA *
**************
* The BLOCK DATA program unit is used to initialize variables and arrays inside a COMMON blocks.
      BLOCK DATA SETPLT
*     SETPLT initialises the values used in the plotting package.
      COMMON /PLOT/ OPENED, ORIGIN(2), PSCALE, NUMPEN
      LOGICAL OPENED
      INTEGER NUMPEN
      REAL PSCALE, ORIGIN
      SAVE /PLOT/
      DATA OPENED/.FALSE./, ORIGIN/2*0.0/, PSCALE/1.0/
      DATA NUMPEN/-1/
      END
===================== DATA.txt
* vi: ft=Fortran ts=6 expandtab sw=6

* Initialize variables at the start of execution of the program
* It can be used either in PROGRAM statement or in FUNCTION or SUBROUTINE
      DATA MYVAR /0/ ! Set MYVAR to 0.
      DOUBLE PRECISION EPOCH
      LOGICAL OPENED
      CHARACTER INFILE*20
      DATA EPOCH/1950.0D0/, OPENED/.TRUE./, INFILE/’B:OBS.DAT’/

* However in FUNCTION or SUBROUTINE it makes only sens with static variables
* Thus for local variables, it is necessary to initialize with assignment operator:
      MYVAR = 0

***********************
* ARRAY INITILIZATION *
***********************
      REAL FLUX(1000)
      DATA FLUX / 512*0.0, 488*-1.0 /
* Named constant can be used
      PARAMETER (NX = 800, NY = 360, NTOTAL = NX * NY)
      DOUBLE PRECISION SCREEN(NX,NY), ZERO
      PARAMETER (ZERO = 0.0D0)
      DATA SCREEN / NTOTAL * ZERO /
* Initialize only some elements
      REAL SPARSE(50,50)
      DATA SPARSE(1,1), SPARSE(50,50) / 1.0, 99.99999 /
* Use an implied-DO loop
      INTEGER ODD(10)
      DATA (ODD(I),I=1,10,2)/ 5 * 43/     ! initialize odd indexes
      DATA (ODD(I),I=2,10,2)/ 5 * 0 /     ! initialize even indexes
* Nested DO loops
      DOUBLE PRECISION FIELD(5,5)
      DATA ((FIELD(I,J),I=1,J), J=1,5) / 15 * -1.0D0 /
===================== DO.txt
C vi: ft=Fortran ts=6 sw=6 expandtab

C Simple loop with increment different of 1
* The number after the DO is the terminal statement (statement number), refering to the last line of the DO loop.
      DO 10 I = 1, 10, 2
            ! ...
10    CONTINUE ! Does nothing => continue loop

**********
* END DO *
**********
* This features isn't in the standard but is provided by all compilers.
      DO N = 1, 100
            ! ...
      END DO
===================== GO TO.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

* Unconditional GO TO
      GO TO 100

* Computed GO TO
      GO TO (100, 110, 112, 150, 200), I ! transfer control to specified line according to value of I
===================== IF.txt
C vi: ft=Fortran ts=6 sw=6 expandtab

* Simple IF
      IF ( A .EQ. 3 ) THEN
            ! ...
      END IF

* Logical-IF Statement
      IF (E .NE. 0.0) RECIPE = 1.0 / E

* IF / THEN / ELSE
      IF (FACTOR .LE. 0.0) THEN
            STOP ’Impossible triangle’
      ELSE IF (A .EQ. 2) THEN
            ! do something else
      ELSE
            AREA3 = SQRT(FACTOR)
      END IF
===================== INCLUDE.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

* INCLUDE or INSERT allows to include source code from another file.
* This keyword isn't in the standard but is provided by all compilers.

* It is most often used to contain a set of specification statements which are common to a number of different program units.
* Example:
      INCLUDE ’trig.inc’
* where the file trig.inc (or maybe TRIG.INC) contains:
      REAL PI, TWOPI, RTOD
      PARAMETER (PI = 3.14159265, TWOPI=2.0*PI, RTOD=PI/180.0)

* It can be used to declare a COMMON block in one place and ensures consistency throughout the program.
===================== IO and files.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

* WRITE
* First parameter is UNIT, the destination. Default is the standard output.
* Second parameter is the formatter string.
      WRITE(UNIT=*,FMT=*) 'Some string to print'
* The default formatter, is the list-directed format. All elements in the following
* list are printed.

* FORMATTED OUTPUT
      WRITE(UNIT=*, FMT=’(1X,I9,F11.2)’) IYEAR, AMOUNT
* 1X is here to put a blank at the beginning of each line. It's required for 
* compatibility reasons (with very early Fortran system). It is later removed
* by Fortran system.
* I9 defines an integer on 9 columns.
* F11.2 defines a floating point number on 11 columns with 2 digits after decimal point.
* Numbers are always right-justified.

* READ
      READ(UNIT=*, FMT=*) AMOUNT, PCRATE, NYEARS

* INQUIRE
      INQUIRE(FILE="/path/to/some/file", EXIST=MYVAR)
===================== MODULE.txt
* vi: ft=Fortran ts=6 expandtab sw=6

      MODULE MYMOD
            INTEGER, PARAMETER :: MYCONST = 3
      END
===================== PROGRAM UNIT.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

===================== PROGRAM.txt
C	vi: ft=Fortran

C	Minimalist program
	PROGRAM TINY
		WRITE(UNIT=*, FMT=*) 'Hello, world'
	END
===================== SAVE.txt
C vi: ft=Fortran

C Set specified variables as static.
C ==> no effect when used in main function.
C ==> no effect on COMMON block names since they already reside in a static area.
===================== STOP.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

* STOP terminates the program
      STOP
* Optionally print a message
      STOP "Something weird occured !"
===================== TYPE.txt
* vi: ft=Fortran ts=6 expandtab sw=6

* Fortran 90 ?

* define a type
      TYPE :: A
            PRIVATE
            INTEGER     :: i
      END TYPE A

* derive a type
      TYPE EXTENDS(A) :: B
      END TYPE B

* access a field
      a%i
===================== WinAPI.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

* To include functions defined in Kernel32.dll, declare the corresponding
* Fortran module:
      USE KERNEL32

*********
* TYPES *
*********
* Some specific structures are needed when calling Windows API functions.
* For Intel Fortran compiler, they are defined in ifwinty.f90 (Intel Fortran Windows Types).

********
* BOOL *
********
* Note that the Windows BOOL type is not equivalent to Fortran LOGICAL and should not be used with Fortran LOGICAL operators and literal constants. Use the constants TRUE and FALSE, defined in IFWINTY, rather than the Fortran literals .TRUE. and .FALSE., and do not test BOOL values using LOGICAL expressions.

****************
* NULL POINTER *
****************
* In ifwinty.f90 (Intel compiler), null pointer constants are defined for each
* data type.
      character, pointer :: NULL_CHARACTER
      type(T_ACL), pointer :: null_acl
      type(T_TOKEN_PRIVILEGES), pointer :: null_token_privileges
      type(T_RECT), pointer :: null_rect
      type(T_POINT), pointer :: null_point
      type(T_OVERLAPPED), pointer :: null_overlapped
      type(T_SECURITY_ATTRIBUTES), pointer :: null_security_attributes
      type(T_PROCESS_INFORMATION), pointer :: null_process_information
      type(T_FILETIME), pointer :: null_filetime
      type(T_STARTUPINFO), pointer :: null_startupinfo
      type(T_PALETTEENTRY), pointer :: null_paletteentry
===================== arithmetic.txt
* vi: ft=Fortran

* 2**(-3) ===>  1/(2**3)  ===>  1/8  ===>  0
* (-2)**3  ===> (-2) * (-2) * (-2)  ===>  -8
* (-2)**3.0 is invalid since the computer would try to evaluate the logarithm of -2.0, which does not exist.
* 3 / 4 * 5.0  ===>  REAL(3/4) * 5.0  ===> 0.0
* 5.0 * 3 / 4  ===> 15.0 / REAL(4)  ===>  3.75

* Prohibited operations :
* dividing by zero, 
* raising a negative value to a real power,
* raising zero to a negative power.
===================== arrays.txt
* vi: ft=Fortran

* Declaring an array
      PARAMETER (N = 100)
      REAL X(N)
* X has indexes from 1 to N
      REAL Y(12:102)
      REAL Z(2:5,10:100)

* Access to an element of an array
	X(3)

* For multiple arrays, the first index varies more rapidly than the second
* That means elements are stored in memory as ARRAY(1,1) ARRAY(2,1) ARRAY(3,1) ... ARRAY(100,1) ARRAY(1,2) ...
* So when looping on both indices, it's faster to affect the first index to the most inner loop:
      DOUBLE PRECISION ARRAY(100,100), SUM
      SUM = 0.0D0
      DO 250,L = 1,100
        DO 150,K = 1,100
          SUM = SUM + ARRAY(K,L)
150     CONTINUE
250   CONTINUE
===================== comments.txt
* vi: ft=Fortran

<<<<<<< HEAD
* Any line with a *, c or C in column 1 is a comment line
* ! can also be used as a comment character but is not standard. However it allows to put comments at the end of a line:
	DATA MYVAR /1/ ! initialization

* !$ signals a special comment (like a #pragma in C).
!$mypragmaID some text
=======
* Comments start with a C (Fortran 66) or a * in first column.
>>>>>>> dfe334852d06d9cdbfc1a04d5f8cd60a307b213a
===================== compiler_errors.txt

===================== constants.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

      REAL PI
      PARAMETER (PI = 3.14159)
      INTEGER N, M
      PARAMETER (N = 100, M = 12)
      CHARACTER*(*) LETTER, DIGIT, ALPNUM
*     *(*) means that the length of the character string will be set to that of the literal constant.
      PARAMETER (LETTER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
     $           DIGIT = '0123456789', ALPNUM = LETTER // DIGIT)

* Writing hexadecimal
      INTEGER I
      I = X'1F02'
===================== debug.txt
* vi: ft=Fortran

* Any line with a d or D in column 1 is a debug line.
* It will only be compiled in debug mode.
===================== forcheck.txt
forcheck is a Fortran syntax checker.
Very useful with a language such as Fortran, since it's a bit permissive on certain points.
===================== fork.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

*     Only available under UNIX/Linux/MacOS-X
      INTEGER*4 PID
      PID = fork()
      IF(PID.LT.0) STOP 'Fork error'
      IF(PID.GT.0) THEN
*           PARENT
      ELSE
*           CHILD
      ENDIF

* Under Windows, call the Windows API CreateProcess.
===================== functions.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

* Default values are signaled by * character, like in WRITE:
      WRITE(UNIT=*, FMT=*) 'Hello, world'
      WRITE(*,*) 'Hello, world'

* Defining a function
      FUNCTION MYFUNC(A, B, C)
      MYFUNC = (A + B)*C
      END

* Statement function
      FAHR(CELS) = 32.0 + 1.8 * CELS
* OR
      REAL M1, M2, G, R
      NEWTON(M1, M2, R) = G * M1 * M2 / R**2  ! The variables G, M1, M2 and R can be used elsewhere in the program unit with no effect on the function evaluation.

* SUBROUTINE
* Defining a subroutine (or procedure)
      SUBROUTINE MEANSD(X, NPTS, AVG, SD)
      END
* Calling a subroutine
      CALL MEANSD(X, NPTS, AVG, SD)

* Calling a function
      INT MYFUNC        ! First we must define the type of the function
      Z = MYFUNC(A, B, C)

* ARGUMENTS
* All arguments are passed by reference.
* So in:
      INTEGER FUNCTION FOO(A, B)
      END
*     ...
      I = FOO(U, U)
* Both A and B are associated with U. This can cause issues, such as when writing A = B

*********************
* RETURN statement. *
*********************
* Returns directly from the function or subroutine.
      REAL FUNCTION HYPOT(X, Y)
* Computes the hypotenuse of a right-angled triangle.
            REAL X, Y
            IF (X .LE. 0.0 .OR. Y .LE. 0.0) THEN
                  WRITE(UNIT=*,FMT=*)’Warning: impossible values’
                  HYPOT = 0.0
                  RETURN
            END IF
            HYPOT = SQRT(X**2 + Y**2)
      END

* A RETURN statement can be followed by an integer n.
* Control is returned to nth alternet. An alternet is indicated by a * or a & inside the SUBROUTINE statement (at the end of the arguments list).
      SUBROUTINE(A, B, C, *, *)
            ! ...
            RETURN 1
            ! ...
            RETURN 2
            ! ...
            RETURN
      END

* In Fortran 90, INTENT allows to specify if an argument is IN/OUT:
      SUBROUTINE SUB(S, T, U)
            REAL, INTENT(IN)    :: S
            REAL, INTENT(INOUT) :: T
            REAL, INTENT(OUT)   :: U
      END SUBROUTINE SUB

* Passing arrays as arguments
      SUBROUTINE DOTPRO(NPTS, X, Y, Z)
             REAL X(NPTS), Y(NPTS), Z(NPTS)
      END SUBROUTINE DOTPRO

* Passing array of unknown size
      REAL FUNCTION ADDTWO(TABLE, ANGLE)
             REAL TABLE(*)
             N = MAX(1, NINT(SIN(ANGLE) * 500.0))
             ADDTWO = TABLE(N) + TABLE(N+1)
      END

* Passing an array of strings
      SUBROUTINE SORT(NELS, NAMES)
            INTEGER NELS
            CHARACTER NAMES(NELS)*(*)
      END SUBROUTINE SORT

*************************************
* PASSING A FUNCTION AS AN ARGUMENT *
*************************************
      SUBROUTINE GRAPH(MYFUNC, XMIN, XMAX)
* Plots functional form of MYFUNC(X) with X in range XMIN:XMAX.
            REAL MYFUNC, XMIN, XMAX
            XDELTA = (XMAX - XMIN) / 100.0
            DO 25, I = 0,100
                  X = XMIN + I * XDELTA
                  Y = MYFUNC(X)
                  CALL PLOT(X, Y)
25          CONTINUE
      END

      PROGRAM CURVES
            INTRINSIC SIN, TAN      ! functions passed as arguments must be declared as INTRINSIC or EXTERNAL
            EXTERNAL MESSY
            CALL GRAPH(SIN, 0.0, 3.14159)
            CALL GRAPH(TAN, 0.0, 0.5)
            CALL GRAPH(MESSY, 0.1, 0.9)
      END

      REAL FUNCTION MESSY(X)
            MESSY = COS(0.1*X) + 0.02 * SIN(SQRT(X))
      END

*******************
* STATIC VARIABLE *
*******************
* SAVE is used to declare static variables inside a function/subroutine
      SUBROUTINE EXTRA(MILES)
            INTEGER MILES, LAST
            SAVE LAST
            DATA LAST /0/
            WRITE(UNIT=*, FMT=*) MILES - LAST, ’ more miles.’
            LAST = MILES
      END
===================== gfortran.txt
# vi: ft=sh

# Under MacOS-X, be sure to set the following environment variables, so gcc can find fortran compiler:
export FC=$(which gfortran)
# There are also the env vars F77 and F90.
===================== labels.txt
* vi: ft=Fortran

* Unsigned integer between 1 and 99999, placed in columns 1 to 5 of an initial line.
* It's forbidden to place a label on a continuation line.
* Blanks and leading zeros are ignored.
 100  SUM = 0.0
===================== loops.txt
C vi: ft=Fortran

C DO loop.
      DO 15,I = 1,N
		S = S + I
 15   CONTINUE
===================== naming.txt
C	vi: ft=Fortran

C	Names must start with a letter and cannot be longer than 6 characters (f77).

C	Names must, like statements, be written in upper-case, since Standard Fortran
C	character set doesn't contain lower-case letters.
===================== operators.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

* Operators of comparisons of numbers
      .LT. ! Less than
      .LE. ! Less than or equal
      .EQ. ! Equal
      .NE. ! Not equal
      .GT. ! Greater than
      .GE. ! Greater than or equal

* Operators for comparing/combining or modifying logicals
      .AND.	      ! logical and
      .OR.	      ! logical inclusive or
      .EQV.	      ! logical equivalence
      .NEQV.      ! logical non-equivalence (i.e. exclusive or).
      .NOT.

* Others
**	! power
===================== pitfalls.txt
* vi: ft=Fortran ts=6 expandtab sw=6

C     Error made in a program of a planetary probes launched by NASA
      DO 15 I = 1.100
C     Which is interpreted by Fortran, which ignores all spaces, like
      DO15I=1.100
C     Value 1.100 affected to variable DO15I
C     What should have been written is :
      DO 15 I = 1,100
C     However Fortran 77 allows an additional comma before the loop variable, which avoids such mistakes :
      DO 15, I = 1,100
===================== pointer.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

* Cray style pointer
      INTEGER A,B
      POINTER (P,I)
      IF (A<>0) THEN
            P=LOC(A)
      ELSE
            P=LOC(B)
      ENDIF
      I=0         ! Assigns 0 to either A or B, depending on A's value
      END

* Fortran 90 pointer
      INTEGER, POINTER :: MYPTR

===================== source line.txt
C vi: ft=Fortran

C Standard Fixed Format
* Only the first 72 columns are scanned
* The first 5 columns must be blank or contain a numeric label
* Continuation lines are identified by a nonblank, nonzero in column 6.
      DATA MYVAR, MYVAR2,
     &     MYVAR3

C Tab-Format
* A tab in any of columns 1 through 6, or an ampersand in column 1, establishes the line as a tab-format source line.
* If the tab is the first nonblank character, the text following the tab is scanned as if it started in column 7.
* A comment indicator or a statement number can precede the tab.
* Continuation lines are identified by an ampersand (&) in column 1, or a nonzero digit after the first tab.
	DATA MYVAR, MYVAR2,
&	     MYVAR3
===================== strings.txt
* vi: ft=Fortran ts=6 expandtab sw=6

* when copying characters to a string, if there's space left in the string then it's padded ; 
* if there are too much characters to copy, they are discarded.

* substring
      CHARACTER METAL*10
      METAL = 'CADMIUM'
      METAL(1:3)	! 'CAD'
      METAL(1:3) = METAL(4:)	! 'MIUMIUM'
      METAL(1:N) = METAL(4:)	! N must be < 4

* concatenate
      METAL = 'CAD' // 'MIUM'

* create a character from ASCII code
      CHAR(I)

* get ASCII code of first character of a string
      ICHAR(S)
      
* search string S2 in S1
      I = INDEX(S1,S2)
* returns 0 if it fails

* get length
      L = LEN(S)

* equality test
      IF (S.EQ.'somestring') THEN
      ENDIF

* comparison
      LGE(S1, S2) ! greater than or equal to
      LGT(S1, S2) ! greater than
      LLE(S1, S2) ! less than or equal to
      LLT(S1, S2) ! less than

* C style null-terminated string
      DATA c_style_string /'some text'C/
===================== subroutines.txt
* vi: ft=Fortran

* IN/OUT parameters
* Specifying which parameter is IN, which is OUT is important, because
* it gives information to the compiler which will then be able to optimize
* code.
	SUBROUTINE myFunc(a, b, n, D)
		INTEGER, INDENT(IN) :: a, n, D(n)
		INTEGER, INDENT(OUT) :: c
		INTEGER :: i	! local variable
		! ...
	END SUBROUTINE
===================== variables.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

C     By default, variables doesn't need to be declared.
C     All undeclared variables are typed according to their first letter :
C     initial letters I-N           interger type
C     initial letters A-H and O-Z   real type
C     To declare a variable:
      INTEGER YEARS
      REAL  X, Y
* Change the default data types
      IMPLICIT DOUBLE PRECISION (D, X-Z), INTEGER (N-P)
      IMPLICIT CHARACTER*100 (C, Z)
      IMPLICIT NONE                 ! states that there are no default data types in this program unit. This form isn't in the standard but is provided by all compilers.
      
C     Arrays
      REAL X(1000)

* DATA TYPES
      INTEGER I
      REAL    X
      DOUBLE PRECISION Y
      COMPLEX          Z
      LOGICAL          B
      CHARACTER        S

* BOOLEAN
      .TRUE.
      .FALSE.
      LOGICAL A = .TRUE.

* STRING
      'my string constant'
      CHARACTER S1*15, S2*30
      CHARACTER*10 S, T, U*4   ! U has size 4, S and T have size 10

* DOUBLE PRECISION
      DOUBLE PRECISION X
      X = 1.0D0
===================== while.txt
* vi: ft=Fortran ts=6 sw=6 expandtab

      n = 1
10    if (n .le. 100) then
            n = 2*n
            write (*,*) n
            goto 10
	endif

===================== writing rules.txt
C vi: ft=Fortran

C	The Fortran character set:
C	the 26 upper-case letters 	A..Z
C	the 10 digits			0..9
C	and 13 special characters:	+ - * / <space or blank> = ( ) . , ' : $

C	Blanks
C	blanks (spaces) are ignored.

C	Length of lines
C	Maximum length is 72 characters. Compilers ignore characters after column 72.

C	Start columns
C	1-5	Label field
C	6	Continuation marker field
C	7-72	Statement fiel

C	Continuation line
C	Maker can be any character except blank or 0. $ is a good choice, since if placed in
C	column 7 it will most probably produces an error at compile time.
C	Up to 19 continuation lines are allowed, which makes 20 lines in total.
