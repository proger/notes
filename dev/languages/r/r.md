# R
<!-- vimvars: b:markdown_embedded_syntax={'r':'','sh':'bash','bash':'','java':'','cpp':'','mail':''} -->

 * [The R Project for Statistical Computing](https://www.r-project.org/).

 * [An Introduction to R](https://cran.r-project.org/doc/manuals/r-release/R-intro.html).
 * [R internals](https://cran.r-project.org/doc/manuals/r-release/R-ints.html).
 * [Advanced R](https://adv-r.hadley.nz/index.html).

 * [BOOKDOWN](https://bookdown.org/). Write HTML, PDF, ePub, and Kindle books with R Markdown.

 * [Travis CI for R — Advanced guide](https://towardsdatascience.com/travis-ci-for-r-advanced-guide-719cb2d9e0e5)

## Installing

 * [UBUNTU PACKAGES FOR R](https://cran.r-project.org/bin/linux/ubuntu/).
 * [How to install R on Ubuntu and Debian](https://www.linode.com/docs/development/r/how-to-install-r-on-ubuntu-and-debian/).

Install:
```sh
brew tap homebrew/science && brew install r # macos
apt-get install r-base # Debian
dnf install R # Fedora
```

Install R Studio:
```sh
yay -S rstudio-desktop-bin # For R-Studio on Archlinux
```
Binary is at `usr/lib/rstudio/rstudio`.

There exists a GUI application downloadable [here](http://www.cran.r-project.org).
On macOS, to uninstall it run:
```sh
rm -rf /Library/Frameworks/R.framework /Applications/R.app /usr/bin/R /usr/bin/Rscript
```

To get the latest R version under Ubuntu, see commands at [Install R](http://cran.stat.unipd.it/bin/linux/ubuntu/):
```sh
apt update -qq
apt install --no-install-recommends software-properties-common dirmngr
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -sc)-cran40/"
apt install --no-install-recommends r-base
```

To get the latest version under Debian, see [Debian Packages of R Software](http://cran.univ-paris1.fr/bin/linux/debian/).

In a conda environment, install r and r-essentials:
```sh
conda install -c conda-forge r==4.2 r-essentials==4.2
```

## Running

Execute specified file:
```sh
R -f myfile
```

Execute a command:
```sh
R -e 'install.packages("devtools")'
```

Don't print startup message:
```sh
R -q ...
```

Print neither startup message, nor lines of program:
```bash
R --slave ...
```

For running from a standalone script, use the following shebang:
```r
#!/usr/bin/env -S R --slave -f
```

To pass arguments:
```bash
R --args ...
```

To run a script, use preferably the `Rscript` executable:
```bash
Rscript myscript.R
```

Another way to run a script:
```bash
R CMD BATCH myscript.R
```

From with an R session:
```r
source("my_script.R")
```

Quit:
```r
q()
q(status = 1)
quit()
quit(status = 1)
```

Under RedHat/CentOS, install from EPEL repos:
```sh
yum install epel-release
yum install R
```

## Comments

```r
# A comment in R
```

## Line continuation

Inside an if/else structure, avoid the following structure:
```r
if (myTest) myAction()
else myOtherAction() # WRONG
```
For R to understand that the statement is not ended after the `if`, it is necessary to start an openning block with `{` or write the `else` at the end of the line:
```r
if (myTest) myAction() else
    myOtherAction()
# or
if (myTest) {
    myAction()
} else {
    myOtherAction()
}
```

For operators, the operator must not be written at the beginning of a line:
```r
x <- myFirstFunctionToCall()
     + mySecondFunctionToCall() # WRONG
```
The operator must be written at then of the line:
```r
x <- myFirstFunctionToCall() +
     mySecondFunctionToCall()
```

## Variables & Types

Name        Description
---------   --------------------
logical     Boolean.
numeric     Float.
character   String.
vector      A 1-D array of same basic type (logical, numeric, character) elements.
list
matrix
data.frame  A list of vectors of same length.

Testing type:
```r
is.matrix(x)
is.data.frame(x)
is.list(x)
```

Converting:
```r
y <- as.vector(x)
y <- as.matrix(x)
y <- as.data.frame(x)
```

Getting the basic type (integer, numeric, logical or character) of an object:
```r
typeof(x)
```

Getting mode (type of object: list, numeric, ...):
```r
mode(x)
```

Getting class (object's class, list, numeric, ...):
```r
class(x)
```
The `class` function returns a vector in case of a class inheriting from other classes.
The `inherits` function eases inheritance test:
```r
inherits(x, "SomeClass")
```

### NA

This i a logical value of length 1:
```r
NA # means Not Available
```

Constants for other atomic vector types:
```r
NA_integer_
NA_real_
NA_complex_
NA_character_ 
```

Testing:
```r
is.na(myvar)
```

Affecting inside a vector:
```r
(xx <- c(0:4))
is.na(xx) <- c(2, 4)
xx                     #> 0 NA  2 NA  4
```

### NULL

 * [R : NA vs. NULL](http://www.r-bloggers.com/r-na-vs-null/).

Test:
```r
is.null(myvar)
```

Convert:
```r
as.null(myvar) # --> returns NULL whatever is passed to it
```

### Strings

  * [Encoding character strings in R](https://rstudio-pubs-static.s3.amazonaws.com/279354_f552c4c41852439f910ad620763960b6.html).

Getting the length of a string:
```r
l = nchar(s)
```

Concatenate strings:
```r
s <- paste(s1, s2, sep="")
s <- paste(s1, s2, s3, s4, sep="")
```
Be careful, by default the separator ('sep' argument) is the space character.

Getting a substring (the first 100 characters):
```r
s <- substr(s, 1, 100)
```

Putting to lower or upper case
```r
s <- tolower(s)
s <- toupper(s)
```

Getting character code:
```r
charToRaw('A') # Returns a string containing the hex code of A.
```

Repeat a character or string n times:
```r
paste(rep(s, 3), sep = '')
```

Pading character strings:
```r
stringr::str_pad(c('abc', 'defghi', '', 'jk'), width = 10, side = 'right', pad = ' ')
```

Remove whitespaces:
```r
stringr::str_trim(s)
```

Create a string in Latin-1 encoding:
```r
iconv("Qui sème le vent récolte la tempête.", from="utf8", to="latin1")
```

#### Reading/writing from/to a string

Write a data frame into a string:
```r
my.dataframe <- data.frame(a = c(1,2), b = (4.10, 10.6))
writetc <- textConnection("my.string", "w", local = TRUE)
write.csv(my.dataframe, writetc)
```

Read a data frame from a string:
```r
readtc <- textConnection("my.string", "r", local = TRUE)
df <- read.csv(readtc)
```
or
```r
df <- read.table(text = my.string) 
```

#### Substrings

Substring extraction:
```r
substr(text, start, stop)
substring(...)
```

Substring replacement:
```r
substr(text, start, stop) <- s
```

#### Split & join

Spliting:
```r
v = strsplit(s, " *;") # Split all strings in vector s, and returns a list of vectors of strings.
v = strsplit(s, " +", perl = TRUE) # Use PERL style
v = strsplit(s, " ", fixed = TRUE) # Do not use regex
```

Joining:
```r
s <- paste(v, collapse = " ")
```

#### Trimming

Trimming leading white spaces:
```r
trim.leading <- function (x)  sub("^\\s+", "", x)
```

Trimming trailing white spaces:
```r
trim.trailing <- function (x) sub("\\s+$", "", x)
```

Trimming leading and trailing white spaces:
```r
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
```

#### Search, replace and regex

 * [PERL regular expressions](https://perldoc.perl.org/perlre).
 * [ICU Regular Expressions](https://unicode-org.github.io/icu/userguide/strings/regexp.html).

Replacing with regexp:
```r
result <- sub('\\.in$', '.out', variable)    # replace first occurence only
new_str <- sub('^.*/([^/]+)$', '\\1', str, perl=TRUE)
result <- gsub('\\.in$', '.out', variable)   # replace all occurences
```

Get index of first match:
```r
pos <- regexpr('\\.[a-z]', 'zap.plouf.hop', perl = TRUE)[[1]]
index <- as.integer(pos)
```

Get indices of all matches:
```r
pos <- gregexpr('\\.[a-z]', 'zap.plouf.hop', perl = TRUE)[[1]]
indices <- as.integer(pos)
```

Extracting substrings:
```r
stringr::str_extract("blabla lala", "la") # search once for the regexp
stringr::str_extract_all("blabla lala", "la") # search several times for the same regexp: returns a list
```

Searching for groups:
```r
stringr::str_match("id=1244 id=3939", "id=([0-9]+)") # Return the first match in the form of a list: first is the whole, then the groups.
stringr::str_match_all("id=1244 id=3939", "id=([0-9]+)") # Return all matches in the form of a matrix: first column is the whole match, then the groups.
```

For `stringr`, see [ICU Regular Expressions](https://unicode-org.github.io/icu/userguide/strings/regexp.html) for the regex format.

Ignoring case:
```r
str_match(str_vect, ignore.case(pattern))
```

Matching also new lines and carriage return with `.`:
```r
match <- stringr::str_match(xml, stringr::regex('^(.*)(<metabolite>.*</metabolite>)(.*)$', dotall = TRUE))
```

#### stringi

Read text file in raw format (bytes):
```r
content <- stringi::stri_read_raw(path)
```

Detect text encoding from raw content:
```r
encs <- stringi::stri_enc_detect(content)
```

Convert from one encoding to another:
```r
content <- stringi::stri_encode(content, from=enc, to='UTF-8')
```

### Array

Arrays are objects that have 1 or more dimensions. Matrix is a special case of arrays.

Create an array 4x5, initialized with integer values from 1 to 20:
```r
A<-array(1:20,dim=c(4,5))
```

A 3D array:
```r
arr <- array(data = c(m1, m2), dim = c(3,2,2)) # construct a 3D array from two 3x2 matrices.
```

Test if two arrays are equal:
```r
isTRUE(all.equal(A, B, tol=0))  # tol is tolerance
```

Example of operations on a array:
```r
x <- c(1:10)
x[(x>8) | (x<5)]
```
yeilds 1 2 3 4 9 10.

Another example which counts the number of elements of an array which are <= 1:
```r
p <- sum(v <= 1) # where v is an array
```


### Integer

Define an integer value
```r
i <- 25L
```

Maximum of an integer value:
```r
.Machine$integer.max # 2147483647 (2^31-1).
```
For both negative and positive values.

`numeric` type can store bigger integer values:
```r
2 ** .Machine$double.digits
```

Integer division:
```r
5 %/% 2
```

Modulo:
```r
5 %% 2
```

### Vector

Elements of a vector can be of the following types: logical, integer, double, complex, character and raw.

Vectors are also called "atomic vectors", to distinguish them from list (called "generic vectors" or "recursive vectors")

Create a vector with integers from 1 to 10:
```r
x <- c(1:10)
```

Create a vector x:
```r
x <- c(10.5, 9.3, 10.2, 4.1)
assign("x", c(10.5, 9.3, 10.2, 4.1))
c(10.5, 9.3, 10.2, 4.1) -> x
y <- vector(length = 2)
y <- vector(mode = 'integer', length = 0)
a <- numeric()
y <- 5:8
y <- 5:1
seq(5) # 1 2 3 4 5
seq(0) # 1 0 !!!!
seq_along(5) # 1
seq_along(c(3,1,3,8)) # 1 2 3 4
y <- seq(from = 12, to = 30, by = 3) # 12 15 18 21 24 27 30
y <- seq(from = 1.1, to = 2, length = 10)
y <- rep(8,4) # 8 8 8 8
y <- rep(c(1,2),4) # 1 2 1 2 1 2 1 2
y <- rep(c(5,12,13), each = 2) # 5 5 12 12 13 13
```

Remove first element of a vector:
```r
b <- seq(10)
b[-1]
```

Empty vector:
```r
numeric()
character()
```

Concatenating vectors:
```r
y <- c(x, 0, x)
y <- c(1, 2, c(6, 7)) # gives 1 2 6 7. Vectors are flattened.
```

Appending to a vector:
```r
a <- c(a, 2)
```

Getting value:
```r
v[4]
```

Setting value:
```r
v[5] <- 19
```
Attention, this operation may involve a reassignment of v (i.e.: a creation of a new vector and destruction of this old). As a matter of fact, what happens is that the operator/function [<- is called as is :
```r
v <- "[<-"(v, 5, value = 19)
```
This should trigger a reassignment, but in fact R tries to avoid this and modify the vector in-place.

Change type of a list or vector, and keep all attributes including names:
```r
storage.mode(v) <- 'integer'
```

### Matrix

Remove dimnames of a matrix:
```r
attr(myobj, 'dimnames') <- NULL
```

```r
p <- nrow(m) # get number of rows
q <- ncol(m) # get number of rows
```

Getting rows:
```r
m[1,] # row 1
```

Getting columns:
```r
m[,2] # column 2
```

Removing columns and rows:
```r
m[c(1,3),] # keeps only rows 1 and 3
m[c(-2),] # removes row 2
m[,c(-2)] # removes column 2
```

For inversion of a matrix, see R/LAPACK/ATLAS.

Submatrices:
```r
y[c(1,3),] <- matrix(c(1,1,8,12),nrow=2) # assign values to a submatrix
y[2,, drop = FALSE] # The option drop=FALSE avoid obtaining a vector instead of a matrix in case the resulting submatrix is a 1-by-n or n-by-1 matrix.
```

Multiplication:
```r
m %*% c(1,1)
```

Addition:
```r
m + 10:13 # Since matrices are vectors, we can add a vector of total size nxm to a matrix nxm.
```

Apply:
```r
apply(m, MARGIN = 1, func) # MARGIN is the code for the dimension (1 = rows, 2 = columns). It tells if the function func is applied on rows or columns.
```

Names:
```r
colnames(m) <- c("a", "b")
m[,"a"]
```

Getting duplicated elements:
```r
x <- duplicated(y) # Returns a vector of boolean
```

Getting unique elements (remove duplicates):
```r
x <- y[ ! duplicated(y)]
```

Select unique values:
```r
x <- unique(x)
```

Split a vector in smaller vectors (chunks) of equal size:
```r
chunks <- split(v, ceiling(seq_along(v) / 50))
```

### Lists

Remove values from a list:
```r
mylist <- list(4, 6, 3, 1, 3, 0)
mylist[1,3] <- NULL
```

Set values to NULL in a list:
```r
mylist <- list(4, 6, 3, 1, 3, 0)
mylist[1,3] <- list(NULL)
```

### Data frames

A data frame is a list of vectors, factors or matrices of equal lengths.
It can aso be view as a matrix with columns of different modes.
A data frame can also contain other data frames.

Creation:
```r
n <- c(2, 3, 5) 
s <- c("aa", "bb", "cc") 
b <- c(TRUE, FALSE, TRUE) 
df <- data.frame(n, s, b)       # df is a data frame
```
or
```r
d <- data.frame(kids=c("Jack","Jill"),ages=c(12,10))
d <- data.frame(kids=c("Jack","Jill"),ages=c(12,10), stringsAsFactors=FALSE) # don't create factors
```

Converting a list into a data frame:
```r
x <- as.data.frame(mylist, stringsAsFactors=FALSE, optional=TRUE)
```
`optional=TRUE` disables `check.names` and thus avoids the rewriting of column names (replacement of space with a dot, ...).
`stringsAsFactors=FALSE` avoids the use of factor type for columns.

Columns and rows can be created at will:
```r
df <- data.frame()
df['zap', 'hop'] <- 4
```

Accessing:
```r
d[[1]] # get first column
d[,1] # get first column
d[,1, drop=FALSE] # get first column, keep as data.frame
d$kids # get column "kids"
```

Remove rows:
```r
d[-c(1,5,10),]
```

A data frame can have a names attribute (for naming columns):
```r
names(my_data_frame)
```

Check that a named column exists:
```r
'mycol' %in% colnames(my_data_frame)
```

Filtering:
```r
examsquiz[examsquiz$Exam.1 >= 3.8,]
subset(examsquiz,Exam.1 >= 3.8)
```

NA values:
```r
mean(x,na.rm=TRUE) # remove NA values when computing the mean.
d4[complete.cases(d4),] # keep only the lines containing no NA values.
```

Apply() can be used on a data frame if all columns are of the same type:
```r
apply(examsquiz,1,max)
lapply(d, sort) # sorts all columns of d individually, and returns a list of the sorted vectors.
```

Appending/inserting a column or a row:
```r
rbind(d,list("Laura",19)) # append a row
examsquiz$ExamDiff <- examsquiz$Exam.2 - examsquiz$Exam.1 # append a column made of a computation on other columns.
d$new_col <- c(1,2) # append a new column and use recycling if c(1,2) is too short compared to the length of columns of d.
```

Using `plyr` library, for appending two data frames with different columns:
```r
library(plyr)
a <- rbind.fill(b, c)
```
If a column is missing in b or in c, it is filled with NA values.

#### Reading

Reading a data.frame from file:
```r
x <- read.table("exams", header=TRUE) # In the header, blanks are replaced with period in names.
all2006 <- read.csv("2006.csv", header=TRUE, as.is=TRUE) # read CSV file. as.is=TRUE disable use of factors (identical to stringsAsFactors=FALSE)
```

Reading a UTF-16 file, with tab separated columns:
```r
x <- read.table("SPI-N1.txt", header=TRUE, quote="", stringsAsFactors=FALSE, sep="\t", fileEncoding="UTF-16")
```

Do not fail when elements are missing on a row:
```r
x <- read.table("mytable.tsv", header=TRUE, sep="\t", fill=TRUE)
```

#### Writing

```r
write.csv(df, file="myfile.csv") # period for decimal separator and comma for field separator
write.csv2(df, file="myfile.csv") # coma for decimal separator and semicolon for field separtor
write.table(df, file="myfile.tsv", sep="\t")
write.table(df, file="myfile.tsv", sep="\t", row.names=FALSE, quote=FALSE) # Do not write row names, do not quote strings.
```

#### Subdata frames

```r
examsquiz[2:5,]
examsquiz[2:5, 2] # returns a vector
examsquiz[2:5, 2, drop=FALSE] # returns a one column data frame
```

### Factors (enum)

Create a factor variable:
```r
gender <- as.factor(c(rep("male",20), rep("female", 30)))
```

There exists an ordered factor where values can be ordered (ex: small, medium, high):
```r
mons = factor(mons,
              levels=c("January","February","March",
                       "April","May","June","July","August","September",
                       "October","November","December"),
              ordered=TRUE)
```

Get levels of a factor, as a character vector:
```r
levels(myfactor)
```

### Table

Compute an histogram: count occurences of each value.

```r
hist <- table(c(1,45,1,6,3,14,45,6,6))
```

### Date & Time

Parse a date:
```r
as.Date('2020-03-18', format='%Y-%m-%d')
```

Get time:
```r
Sys.time()
```

## Statements

### Source

Use source() command to include/load another R file:
```r
source('myfile.R')
```

Switch to sourced file directory while sourcing:
```r
source('../../blabla/myfile.R', chdir=TRUE)
```
Before sourcing the file, R will change to directory `../../blabla`.

By default the names parsed and loaded in the sourced file are put inside the global environment. If you want them to reside inside the current environment use:
```r
source('myfile.R', local=TRUE)
```
or if you want to load them into another environment:
```r
source('myfile.R', local=myenv)
```

Get the path of the current sourced file:
```r
current.file.path <- parent.frame(2)$ofile
```

#### Loading module relativily to script path

Solution 1:
```r
args <- commandArgs(trailingOnly = F)
scriptPath <- dirname(sub("--file=","",args[grep("--file",args)]))
source(file.path(scriptPath, '../input-parser/InputExcelFile.R'), chdir = TRUE)
```

Solution 2:
```r
#- Get path of current R script to source using a relative directory:
source(file.path(dirname(rscript_current()), '../myfile.R'))
#- Where rscript_current is:
rscript_current <- function() {
    stack <- rscript_stack()
    r <- as.character(stack[length(stack)])
    first_char <- substring(r, 1, 1)
    if (first_char != '~' && first_char != .Platform$file.sep) {
        r <- file.path(getwd(), r)
    }
    r
}

```
### for

Loop on a list or vector:
```r
for (x in list.or.vector) {
}
```

Best way to iterate on all indices of a vector:
```r
for (i in seq_along(v))
    v[i] <- compute(v[i], i)
```
`seq_along` is the same as `seq(along.with = v)`, which means it will use the length of the argument (i.e.: length of vector `v`).

*Wrong* way to iterate on all indices of a vector:
```r
for (i in seq(v))
    v[i] <- compute(v[i], i)
```
There will be no iteration if `length(v) == 0`. Also if v is a single integer, it will be interpreted as the number of integers to generate.

How to break / continue:
```r
break
next
```

### While

```r
i <- 0
while (i < 100) {
    i <- i + 1
    # Do something
}
```

### If / else

`if()` is a function in R.
```r
if (TRUE) x <- 2 else x <- 10
```

Logical negation:
```r
if ( ! condition) ...
```

Ternary operator using `if` and `else`:
```r
x <- if (a == 1) 1 else 2 # <=> x = a == 1 ? 1 : 2
```
The same using `ifelse`:
```r
x <- ifelse(a == 1, 1, 2)
```

The `else` clause must appear at the end and on the same line than the `then` clause:
```r
if (...) do_1() else do_2()
```
or
```r
if (...) {
    do_1()
} else {
    do_2()
}
```

### Switch

Value must not be null, and must be a vector of length 1.
The possible values (str1, str2) must be strings and not integer. If these are integers, they must be quoted in order to be strings.
```r
switch(value, # value to test
       str1 = EXPR,
       str2 = EXPR,
       EXPR # default
       )
```

## Operators

 * [Operator Syntax and Precedence](https://stat.ethz.ch/R-manual/R-devel/library/base/html/Syntax.html).

Arithmetic operators:

Operator  | Description
--------- | ----------------------
`+`       | Addition.
`-`       | Subtraction.
`*`       | Multiplication.
`/`       | Division.
`^`, `**` | Exponentiation.
`%%`      | Modulus (x mod y) 5%%2 is 1.
`%/%`     | Integer division 5%/%2 is 2.
`%*%`     | Matrix multiplication.

Assignment operatos:
Operator    | Description
----------- | --------------------------
`<-`, `=`
`<<-`
`->`
`->>`

Logical operators:

Operator    | Description
----------- | --------------------------
`<`         | Less than.
`<=`        | Less than or equal to.
`>`         | Greater than.
`>=`        | Greater than or equal to.
`==`        | Exactly equal to.
`!=`        | Not equal to.
`!`         | Not.
`|`         | Or on all elements of vectors.
`&`         | And on all elements of vectors.
`||`        | Or on first elements of vectors.
`&&`        | And on first elements of vectors.
`isTRUE(x)` | Test if x is TRUE.
`%in%`      | Test if some values are found in another list of values.

## Maths

Logarithm:
```r
log10(x)
```

Power:
```r
10^3
```

Integer division:
```r
5 %/% 2
```

Truncating integer:
```r
trunc(1.23)
floor(1.23)
```

Find roots of an equation:
```r
f <- function(x) x - 300 - x^0.8
uniroot(f, c(300, 1200))
```

Differentiate:
```r
diff(v)
```

Smooth vector:
```r
smooth(v)
```

Cumulative sum/prod/max/min:
```r
cumsum(1:10)
cumprod(1:10)
cummax(1:10)
cummin(1:10)
```

## Package creation

 * [Code examples in the R package manuals](https://blog.r-hub.io/2020/01/27/examples/).

To execute code a package import, define function
`.onLoad()` inside a file called `zzz.R` (usual name).

## R CMD SHLIB (compiling code)

 * [Compile Files for Use with R](https://stat.ethz.ch/R-manual/R-devel/library/utils/html/COMPILE.html).

To compile C source files into a shared library:
```bash
R CMD SHLIB *.c
```

## R environments and namespaces

Global environment (i.e.: working environment):
```r
globalenv()
```

Base environment (i.e.: the environment of the base package):
```r
baseenv()
```

Empty environment (the parent of the base environment):
```r
emptyenv()
```

Current environment:
```r
environment()
```

List all parents of the global environment:
```r
search()
```

Get package environment or global environment:
```r
topenv()
```

Getting an environment from its name/string:
```r
as.environment('package:stats')
```

Get parent environment:
```r
parent.env(e)
```

Accessing a variable in an environment:
```r
e$myvar
```

Modifying a variable inside an environment:
```r
e$myvar <- 'myval'
```

Find the environment where a name is defined:
```r
pryr::where('myname')
```

List objects in an environment:
```r
ls()
objects()
```

Remove objects in an environment:
```r
rm(myvar)
remove(myvar)
```

## Command line arguments

```r
args <- commandArgs(trailingOnly=TRUE)
my_first_arg = args[1]
```
The option `trailingOnly` keeps only arguments after `--args` flag.

Getting script path, script name and script directory:
```r
args <- commandArgs(trailingOnly=FALSE)
script.path <- sub("--file=", "", args[grep("--file=", args)])
script.name <- basename(script.path)
script.dir <- dirname(script.path)
```

## Environment variables

Get all env vars:
```r
ENV = Sys.getenv()
```

Set env var:
```r
Sys.setenv(MY_VAR = "value", MY_VAR2 = "value2")
```

## Data

The data() function loads specified data sets, or list the available data sets.

Loading unknown data set into a separate environment in order to check what is inside:
```r
data(crude, verbose = TRUE, envir = e <- new.env())
ls(e) # check what is in `e`
class(e$crude)
summary(e$crude)
rm(e)
```

Load into workspace:
```r
data(crude)
```

## Load data

Load data from a plain text file with lines like:
1224.233 1228.12e12
and put result in a list
```r
mylist <- scan(file="myfile.txt", what=list(t=0.0, f=0.0))
```

See also `read.table()` in DATA FRAMES.

## Equality

Testing elements of a vector:
```r
x < 8 # returns a vector of logical values
all(x < 8) # returns true if all elements of x are < 8
all(v == w) # returns true if v and w are equals.
identical(v, w) # returns true if v and w are identical (same type, same values). A vector of integers won't be identical to a vector of floating points even if values are equal.
any(x < 8) # returns true if at least one element of x is < 8
```

## I/O

### dput and dget

`dput` and `dget` are used to serialize and deserialize an R object.

### File information

Get file size:
```r
sz <- file.info(myfile)$size
sz <- file.size(myfile)  # Only available from R 3.3.
```

### Loading vector from a file

```r
scan(file = "myfile.txt", what = integer())
```

### Saving vector to a file

```r
write(v, 'myfile.txt', ncolumns=1)
```

### Reading from a file

Open a file for reading:
```r
fd <- file("My file.txt", 'r')
# ...
close(fd)
```

Read all characters from a file and put them into a string:
```r
s <- readChar(filename, file.info(filename)$size)
```

Read all lines:
```r
lines <- readLines(filename)
```

Read 5 lines:
```r
lines <- readLines(filename, n=5)
```

Print a whole file:
```r
cat(readLines(filename), sep="\n")
```

Read from a URL:
```r
u <- url('https://www.uniprot.org/uniprot/?query=&columns=id&format=tab&limit=2')
lines <- readLines(u)
close(u)
```

### Reading from stdin

Reading strings from stdin:
```r
con <- file("stdin")
strings <- readLines(con)
close(con) # close stdin to avoid warning message "closing unused connection 3 (stdin)"
```

Reading floats from stdin:
```r
values <- as.numeric(readLines(file("stdin")))
```

Reading line by line stdin:
```r
con <- file("stdin")
open(con)
while (TRUE) {
    line <- readLines(con, n = 1)
    if (length(line) == 0)
        break
}
close(con)
```

### Writing to a file

```r
cat(..., file="myfile.txt")
write(x, file="myfile.txt")
writeLines(content, file.path)
```

### Default output

Redirect output to a file:
```r
sink("myfile")
```

Redirect to console again:
```r
sink()
```

### Printing

Print a variable:
```r
print(myvar)
```

`str` (structure) prints an object in a compact mode:
```r
str(myobject)
str(mylist)
```

`cat` print as is (without all the transformations print() makes):
```r
cat('Computed PI=',pi,'\n');
```

Print on standard error stream:
```r
write("prints to stderr", stderr())
write("prints to stderr", file=stderr())
```

Print information about an object (field length, field mode, ...):
```r
summary(myobject)
```

Print double values (control number of printed decimals):
```r
print(83.247837878523745) # will be truncated on output

options(digits=22) # now will display all decimals of our number
print(83.247837878523745)
```

## Base functions

### vapply

Apply a function on a list or vector, and returns a vector of a defined type:
```r
myvector = vapply(mylist_or_vector, function(x) paste(x, 'some text'), FUN.VALUE = '')
```

### Filter

Filter values of a list or vector:
```r
mynewlist <- Filter(function(x) ! is.null(x), mylist)
```

### cbind / rbind & Typeg

Combine vector, matrix or data frame by row or columns.

```r
m <- rbind(c(1,4),c(2,2)) # rbind = row bind
m <- cbind(c(1,4),c(2,2)) # cbind = column bind
```

If you encounter the warning message "row names were found from a short variable and have been discarded" with cbind, this means that one of the input has row names and that rows need to be duplicated. To avoid this warning, discard the row names by using the `row.names` option:
```r
z <- cbind(x, y, row.names = NULL)
```

Adding columns and rows to a matrix
```r
rbind(rep(1, dim(m)[1]), m) # insert a row of 1s at top.
cbind(rep(1, dim(m)[2]), m) # insert a column of 1s at left.
```

### Merge

Merging data frames (equivalent of SQL join):
```r
merge(d1, d2) # try to find a column C in common, and only keep rows whose values of C are in d1 and d2.
merge(d1, d2, by.x="col1", by.y="col2") # specify explicitly the columns
```

By default merge will make an exclusive join, and thus will eliminate rows that are only in one of the data frames. To keep those rows:
```r
merge(d1, d2, all = TRUE) # keep single rows of both d1 and d2.
merge(d1, d2, all.x = TRUE) # keep single rows from d1.
merge(d1, d2, all.y = TRUE) # keep single rows from d2.
```

## Throwing an error and catching

```r
stopifnot(cond1, cond2, ...)
```

Try / catch:
```r
tryCatch(expr=stop('Error'), error=function(e){invisible(NULL)}, finally=print('Hello!'))
```

Catch a warning
```r
tryCatch(warning("Warning"), warning=function(w){w$message})
```

## Eval

Evaluate an expression contained in a string:
```r
a <- 1
x <- eval(parse(text="1+4+a"))
```

## Function

Declaring a function:
```r
myfunc <- function(n) {
    return(n+5)
}
```

Arguments can have default values:
```r
myfunc <- function(a, b=2, c=FALSE) {
}
```

Testing if a parameter is not set:
```r
myfunc <- function(a) {
    if (missing(a))
        # ...
}
```

Using enumerate for a function parameter:
```r
myfunc <- function(a, b=c('val1', 'val2', 'val3')) {
    b <- match.arg(b) # 'val1' will be the default value.
    b <- match.arg(b, several.ok=TRUE) # b can have multiple values.
}
```

Returning a value:
```r
return(n) # Can be used at any place in the function
return() # idem
```
The last value of the function is returned.
If the caller doesn't assign the returned value of a function, then it's printed. To avoid this behaviour, use the invisible primitive:
```r
invisible(x)
return(invisible(x))
```

Returning multiple values:
```r
list[a, b] <- myfunc(c) # myfunc returns a list with two values.
list[, b] <- myfunc(c) # we only want the second value
```

Get list of function parameters:
```r
formals(my_func)
formals('my_func')
```

Get list of function parmater names:
```r
formalArgs(my_func)
```

Ellipsis:
```r
foo <- function(x, ...) {
    foo2(z = x, ...)
}
```

Get all function parameters (supplied ones and ones with default values):
```r
foo <- function(a=1, b=2, ...) {
    params <- c(as.list(environment()), list(...))
}
```

Modifying a parameter (i.e.: equivalent to passing by reference?):
```r
foo <- function(x) {
    nameX <- deparse(substitute(x))
    x <- 4
    assign(nameX, x, envir=parent.frame())
    invisible()
}
```

## System environment variables

Getting all system environment variables, into a hash map:
```r
env <- Sys.getenv()
home <- env[["HOME"]]
```

Getting a selected list of system environment variables, into a regular vector:
```r
env <- Sys.getenv(c("HOME", "USER", "PATH"))
home <- env[[1]]
user <- env[[2]]
path <- env[[3]]
```

## Help

Get help on a function:
```r
help(solve)
?solve
help("[[")
```

Getting help about a package:
```r
help(package=MASS)
```

Search:
```r
help.search(solve)
??solve
```

Get examples:
```r
example(topic)
```

## File system

Current working directory:
```r
wd <- getwd()
setwd(dir)
```
*Note*: On MacOS and Windows, `getwd()` will return a canonized form of the
path, replacing backslashes (`\`) by forward slashes (`/`) on Windows, and
inserting possibly a `/private` prefix on MacOS.

List all files in a folder and its subfolders:
```r
list.files('my/path/to/a/folder', recursive=TRUE)
```

List files matching a regex pattern:
```r
list.files('my/path/to/a/folder', pattern='^[A-Za-z0-9_]+\\.log$')
```

Getting current script path:
```r
args <- commandArgs(trailingOnly = F)
script_path <- dirname(sub("--file=","",args[grep("--file",args)]))
```

Test that a file or directory exists:
```r
file.exists(path)
```

Delete a file:
```r
file.remove(path)
```

Test that a directory exists:
```r
dir.exists(path)
```

Concatenate two paths:
```r
newpath <- file.path(path1, path2)
```

Rename a file:
```r
file.rename(current.file.name, new.file.name)
```

Copy a file:
```r
file.copy(srcFile, dstFile)
```

Get absolute path:
```r
normalizePath('tmp/..')
normalizePath('tmp/..', mustWork=FALSE) # No warning
normalizePath('tmp/..', mustWork=TRUE) # Error instead of warning
```

Get two temporary file names:
```r
tempfile("myprefix", fileext=c('.txt', '.csv'))
```
Choose folder:
```r
tempfile("myprefix", tmpdir='/my/folder', fileext='.txt')
```

Create a directory:
```r
dir.create(mydir)
dir.create(mydir, recursive=TRUE) # Create parent directories as needed.
```

## .Deprecated

Declare a method as deprecated:
```r
foo <- function(x) {
    .Deprecated("foo2()")
    foo2(x)
}
```

See also package `lifecycle` for a more flexible approach.

## .libPaths

For installing a package at a particular place (like some system path, for
system wide installation), first look at the list of available destination
directories:
```r
.libPaths()
```
then choose the destination you want by using the `lib` option:
```r
install.packages("pkgname", lib=.libPaths()[2])
```

For installing in user home directory, see `.libPaths()` documentation. On
Ubuntu with R 3.4, the following directory needs to be created:
```bash
mkdir -p $HOME/R/x86_64-pc-linux-gnu-library/3.4
```
This is the default value of the `R_LIBS_USER` environement variable for R
under Linux.
For macOS:
```bash
mkdir -p $HOME/Library/R/3.4/library
```

## .onLoad

Define this function for executing code when package is imported:
```r
.onLoad <- function(libname, pkgname){
    # ...
}
```

This function should be defined inside file `zzz.R` of the package.

## .Platform

Test current platform type:
```r
.Platform$OS.type == "unix"
.Platform$OS.type == "windows"
```

## aggregate

Sum by grouping on one column:
```r
x <- data.frame(a=c('a', 'a', 'b', 'b'), b=1:4)
aggregate(x$b, by=list(x$a), FUN=sum)   
```

Sum by grouping on two columns:
```r
x <- data.frame(a=c('a', 'a', 'a', 'b', 'b', 'b'), b=c(1, 1, 2, 2, 3, 3), c=1:6)
aggregate(x$c, by=list(x$a, x$b), FUN=sum)
```


## call

Construct a call as a string that can be evaluated with `eval`:
```r
my_str_call <- call("my_func", arg1, arg2, arg3)
eval(call("my_func", arg1, arg2, arg3))
```



## do.call

Call a a function from a function object:
```r
myfunc <- function(x,y) x+y
do.call(myfunc, list(x=1, y=2))
```

Call a function from its name as a string:
```r
do.call("my_func", list(arg1, arg2, arg3))
```

## example

Run an example from a package documentation

Run all examples from the `Rule` class of the `sched` package:
```r
example("Rule", package="sched")
```

## exists

Test if an object exists:
```r
exists('my_func', mode='function')
```

## get

Get an object by name (an error is thrown if the object does no exist):
```r
get('myobj')
```

## get0

Get an object by name and return NULL if it does not exist:
```r
get0('myobj')
```

## getFromNamespace

## grep

`grep` returns a vector of indices by default:
```r
grep("blabla", df)
```

Tests if there is at least one match:
```r
length(grep("blabla", df)) > 0
```

Returns the matched elements of the vector:
```r
grep("blabla", df, value=TRUE)
```

Reverse the match (returns elements that do not match):
```r
grep("blabla", df, invert=TRUE)
```

Do not use regexp (matches substrings too):
```r
grep("blabla", df, fixed=TRUE)
```

Case-insensitive search (not compatible with `fixed=TRUE`):
```r
grep("foo", x, ignore.case=TRUE)
```

## grepl

`grepl` returns a logical vector.

Search for regexp in vector, and returns a logical vector:
```r
if (grepl("^blabla", my_string, perl=TRUE)) do_something()
grepl("^blabla", c(s1, s2, s3), perl=TRUE) # --> returns a vector
```

## gzfile

Open a gz compressed file.

Uncompress an archive:
```r
fd <- gzfile('myfile.txt.gz', 'r') # Open gz file for reading
writeLines(readLines(fd), 'myoutputfile.txt') # Write content into other file.
close(fd)
```

## ifelse

Condition on vector elements:
```r
ifelse(v, 'A', 'B')
```

## install.packages

Installing a CRAN package:
```r
install.packages("pkgname")
```

Install with all dependencies:
```r
install.packages("pkgname", dependencies=TRUE)
```

Installing a package from source:
```r
install.packages('RMySQL', type='source')
```

Installing a package from local source bundle:
```r
install.packages('C:/RMySQL.tar.gz', repos=NULL, type='source')
```

Installing using the command line:
```bash
R -e "install.packages('getopt', dependencies=TRUE, repos='https://cloud.r-project.org/')"
```

Getting a list of installed packages:
```r
rownames(installed.packages())
```

See also devtools package.

## length

Length of a vector:
```r
length(x)
```

## library

List available packages:
```r
library()
```

Load package:
```r
library(mypackage)
```
`library()` gives an error if the package can not be loaded.

Non-verbose loading:
```r
library(RMySQL, quietly = TRUE)
```

## ls

List workspace:
```r
ls()
```

List exported methods and classes:
```r
ls("package:mypkg")
```

## match.arg

Using enumerated values for a parameter:
```r
myfct <- function(myparam=c('a', 'b', 'c')) {
    myparam <- match.arg(myparam) # Default will 'a' (first value).
}
```

## match.call

Build a call object:
```r
fooCall <- match.call(foo, call("foo", a=1, b=2))
```

Build a call for itself inside a function:
```r
foo <- function(a=1, b=2) {
    fooCall <- match.call()
}
```

Get the supplied paramaters to a function (not the ones with default values):
```r
foo <- function(a=1, b=2, ...) {
    params <- as.list(match.call(expand.dots=TRUE))
    # ATTENTION First value is the function name
}
```

## mean

Compute the mean:
```r
mean(c(1, 2, 3))
```

## median

Compute the median:
```r
median(c(1, 2, 3))
```

## mget

Get multiple objects by names:
```r
mget(c('a', 'b', 'c'))
```

## options

 * [options: Simple, Consistent Package Options](https://cran.r-project.org/web/packages/options/index.html). Ease the definition of options for a package.
   + [options (vignette)](https://cran.r-project.org/web/packages/options/vignettes/options.html).

Set global options.

Change options for just a section of code:
```r
op <- options(digits=22)
on.exit(options(op)) # Will call options(op) if current function is left with stop().
...
options(op)
```

Set the number of decimal to display:
```r
options(digits=22)
```

Set the user agent for `download.file()`:
```r
options(HTTPUserAgent="myapp")
```

Set timeout for `download.file()`:
```r
options(timeout=120)
```

### Using the options package

Define an option inside the `package.R` file:
```r
#' @import options
options::define_options(
    "Encodings to test when loading a text file's content.",
    encodings = c('default', 'iso8859-1')
)
```

Create a `pkg_opts.R` file, in order to generate documentation:
```r
#' @eval options::as_roxygen_docs()
NULL
```
<!-- TODO --> Is it possible to put content of `pkg_opts.R` inside `package.R`? How is it presented inside the help page?

Use it in function headers:
```r
#' ...
#' @eval options::as_params("my.param" = "my.opt")
#' ...
my.fct = function(my.param=opt('my.opt')) {
}
```

## packageVersion

Getting version of package:
```r
packageVersion('biodb')
```

## R.home

Get R HOME directory:
```r
R.home()
```

## remove.packages

Uninstall a package:
```r
remove.packages("rJava")
```

## rep

Created a list of repeated values:
```r
mylist = rep(list(NULL), 4)
```

## require

```r
if (require(mypackage)) {
    doSomething()
}
```
If the package can not be loadedi `requires()` returns `FALSE` and gives a
warning. It is designed to be used inside a function.

## rm

Remove an object by name:
```r
rm('a')
remove('a')
```

Remove an object by variable:
```r
rm(myobj)
remove(myobj)
```

Clear workspace:
```r
rm(list=ls())
```

## RNGVersion

Fix the RNG (Random Number Generator) version:
```r
RNGversion('3.4.0')
```

## runif

Generate a floating random number, with uniform distribution:
```r
x <- runif(1, 5.0, 7.5)     # generates a random number between 5.0 and 7.5 included
x <- runif(1)   # generates a number in [0.0;1.0]
v <- runif(10)  # generates an array if 10 numbers
```

## sessionInfo

Getting information about loaded packages (package version, ...) in a session:
```r
sessionInfo()
```

## sum

Sum all values:
```r
sum(1, 2, 3)
# or
sum(c(1, 2, 3))
```

## Sys.glob

Glob (list files matching a wildcard pattern):
```r
Sys.glob('*.txt')
Sys.glob(c('*.txt', '*.csv'))
```

Match in another folder:
```r
Sys.glob(file.path('another', 'folder', '*.txt'))
```

## system

Use `system2()` instead, it is more flexible.

Call a system command:
```r
system('mycommand myarg1')
```

## system2

Call a system command:
```r
system2('mycommand', args=c('myarg1', 'myarg2'))
```

## system.file

Getting the path of package data file:
```r
mypath <- system.file("extdata", "myfile.ext", package="mypkg")
```

Test for existence (and raise an error):
```r
mypath <- system.file("extdata", "myfile.ext", package="mypkg", mustWork=TRUE)
```

## topenv


## traceback

Prints current call stack:
```r
traceback(0)
```

## unlink

Remove a directory and its content:
```r
unlink(mydir, recursive=TRUE)
```

## vignette

List all vignettes of all installed packages:
```r
vignette()
```

List all vignettes of a package:
```r
vignette(package='mypkg')
```

Open a vignette:
```r
vignette('someVignette', package='mypkg')
```

Building the vignettes of a package from sources can be done with the
`devtools` package.
## Basic statistical functions

Getting the minimum:
```r
a <- min(v)
```

Getting this index of the minimum in v:
```r
i <- which.min(v)
```

Mean:
```r
mean(c(2,5,8))
```

Standard deviation (écart type):
```r
sd(c(1,4,7.5,6.9))
```

## Sorting

Sort a vector:
```r
v <- (5,1,3,9,10)
sort(v)
```

Get the reordering of indicies of the vector:
```r
order(v)
```

Sort vector:
```r
v <- rbind(v)[,order(v)]
```

Sort data frame:
```r
df <- df[order(df[[1]]), ] # sort on first column.
df <- df[order(df[[1]], df[[3]]), ] # sort on columns 1 and 3 in that order.
df <- df[do.call(order, as.list(df)),] # sort on all columns.
```

Sorting a list/vector of objects:
```r
molecules <- db$getMolecules()
molecules <- rbind(molecules)[,order(vapply(molecules, function(x) x$getId(), FUN.VALUE = 1))]
```

Sort x in the same order as y, when x and y have elements starting at 1, and x is as long as y or longer than y (i.e.: with duplicated elements):
```r
x <- c(1,3,3,4,1,1,2)
y <- c(4,2,3,1)
y[sort(order(y)[x])]
```

Sort x in the same order as y, when x and y have the same number of elements:
```r
x <- c(5,10,3,9)
y <- c(9,5,3,10)
x[order(x)[order(y)]]
```

## Map, Reduce, Filter and Lambda

 * [R Programming Tutorial – Map, Reduce, Filter and Lambda Examples](https://helloacm.com/r-programming-tutorial-map-reduce-filter-and-lambda-examples/).

## Objects (OOP)

 * [OO in R](http://www.r-bloggers.com/oo-in-r/).
 * [OO field guide](http://adv-r.had.co.nz/OO-essentials.html).
 * [Package ‘R.oo’](https://cran.r-project.org/web/packages/R.oo/R.oo.pdf).
 * [S4 Classes in 15 pages, more or less](https://www.stat.auckland.ac.nz/S-Workshop/Gentleman/S4Objects.pdf).
 * [The S4 object system](http://adv-r.had.co.nz/S4.html).
 * [R Inheritance](https://www.programiz.com/r-programming/inheritance).

Workspace = collection of objects.

List objects in memory:
```r
objects()
ls()
```

Delete objects:
```r
rm(x,y,w)
```

Get a list of attributes and their values (object fields):
```r
attributes(x)
```

Class union (define a new class that is an union of several classes):
```r
setClassUnion("newClass", c("C1", "C2"))
```

### S3 (Generics)

 * [R S3 Classes](https://www.datamentor.io).

Objects are lists with an attribute `class` set to the class name.

Methods are handled through generics. A generic function defines a method callable on objects with `mygeneric(myobj)`. A default implementation, `mygeneric.default()` may be defined for all objects, and specific methods for defined classes like `mygeneric.myclass()`.

4 special groups (genericGroup object) exist for defining methods on group levels rather than on individual level. These 4 groups are: Math, Ops, Complex and Summary.

"internal generics" are built-in generic functions that can be specialized by the developer.

Check inheritance:
```r
if (inherits(o, "MyClass"))
    doSomething()
```
`inherits` has been extended to work for S4, but is not reliable. I've experienced malfunctioning from inside a package, when testing if an object inherits from a base class.

### S4 (Formal Classes)

 * [A (Not So) Short Introduction to S4 - cran.r-project.org](https://cran.r-project.org/doc/contrib/Genolini-S4tutorialV0-5en.pdf).

The S4 object system adds the following features:
 * Slots for typed members.
 * Generator function (constructor) returned by `setClass()`.

As in S3, special groups (S4groupGeneric objects) exist: Arith, Compare, Ops, Logic, Math, Math2, Summary and Complex.

```r
library(methods)
```

Check inheritance for an object:
```r
if (methods::is(o, "MyClass"))
    doSomething()
```

Check inheritance between classes:
```r
if (methods::extends("SubClass", "SuperClass"))
    doSomething()
```

Declare a class:
```r
MyClass <- setClass('MyClass', representation=representation(a='integer', b='numeric'))
```

Inheriting:
```r
MySubClass <- setClass('MySubClass', contains='MyClass', representation=representation(c='character'))
```

`ANY` is the root class of all classes. If `contains` is not set, the class inherits from `ANY`.

Instantiate a class:
```r
x <- new('MyClass', a = 1L, b = 45.32)
```
or
```r
x <- MyClass(a = 1L, b = 45.32)
```

Initialize method:
```r
setMethod("initialize", "MyClass", function(.Object, a, b) {

    if ( ! missing(a) && ! missing(b)) {
        .Object@a <- a
        .Object@b <- b
        validObject(.Object)
    }

    return(.Object)
})
```

Since there is only one initializer, a more common way of constructing an S4 object is to use contructor functions instead of the class constructor:
```r
createMyClass <- function(a, b) {
    # ...
}

createMyClassFromList <- function(x) {
    # ...
}
```

Calling super class' method:
```r
MyClass <- setClass('MyClass', contains='MySuperClass', representation=representation(a='integer'))
setMethod("initialize", "MyClass", function(.Object, a) {
    callNextMethod() # Call to super class initializer, can take arguments. If no arguments are provided, it will use the arguments available in the current function.
    # ...
})
```
Another way is to use `as` to cast the instance:
```r
myMethod(as(myobj, "MySuperClass"))
```

Modify slots inherited from super class:
```r
as(myobj, "MySuperClass") <- mySuperClassObj
```

Redefine the `is()` method:
```r
setIs('MyClass1', 'MyClass2', coerce=function(from, to) {
})
```

Virtual class:
```r
setClass('A', representation=representation(x='numeric', 'VIRTUAL'))
```

Accessing an object's slot:
```r
x@a
```
or
```r
slot(x, 'a')
```

Modifying an object's slot:
```r
x@a <- 10L
```
or
```r
slot(x, 'a') <- 10L
```

Get a list of all S4 generic functions:
```r
showMethods()
```

Test if a function is a generic function:
```r
isS4(print) # FALSE, `print` is S3.
isS4(show)  # TRUE.
```

Define a method:
```r
setGeneric("myMethod", function(o) { standardGeneric("myMethod") })
setMethod('myMethod', 'MyClass', function(o) { cat("Do something with o\n") })
```

Define a method with two arguments:
```r
setGeneric("myMethodWith2Args", function(a, b) { standardGeneric("myMethodWith2Args") })
setMethod('myMethodWith2Args', 'integer', function(a, b) { a * b }) # ANY is used for second argument.
setMethod('myMethodWith2Args', 'numeric', function(a, b) { a + b }) # ANY is used for second argument.
setMethod('myMethodWith2Args', c(a='integer', b='numeric'), function(a, b) { a / b })
```
We can also explictly use "ANY" or "missing" for an argument:
```r
setMethod('myMethodWith2Args', c(a='integer', b='missing'), function(a, b) { cat("b is missing.\n") })
setMethod('myMethodWith2Args', c(a='integer', b='ANY'), function(a, b) { cat("b is whatever.\n") })
```

Define a getter:
```r
setGeneric("getMyField", function(o) { standardGeneric("getMyField") })
setMethod('getMyField', 'MyClass', function(o) { return(o@myField) })
```

Define a setter:
```r
setGeneric("setMyField<-", function(object, value) { standardGeneric("setMyField<-") })
setReplaceMethod('setMyField', 'MyClass', function(object, value) {
    object@myField <- value
    validObject(object)
    return(object)
})
```

Redefining the `[` operator:
```r
setMethod('[', 'MyClass', function(x, i, j, drop) {
    return(x@m[i, j, drop=drop])
})
```

Redefining the `[<-` operator:
```r
setReplaceMethod('[', 'MyClass', function(x, i, j, value) {
    x@m[i, j] <- value
    validObject(x)
    return(x)
})
```

Avoid redefinition of a generic:
```r
lockBinding("myMethod", .GlobalEnv)
```

Get a list of all methods of a class:
```r
showMethods(class="MyClass")
```

Get the implementation of a method in a class:
```r
getMethod("myMethod", "MyClass")
```

Get the implementation of a method in a class or in the first superclass where it exists:
```r
selectMethod("myMethod", "MyClass")
```

Test if a method exists for a class:
```r
existsMethod("myMethod", "MyClass")
```

Test if a class or one of its superclasses has a method:
```r
hasMethod('myMethod', 'MyClass')
```

Getting the names of the slots:
```r
slotNames("MyClass")
```

Getting the names and the types of the slots:
```r
getSlots("MyClass")
```

Getting also inheritance information:
```r
getClass("MyClass")
```

Call a method:
```r
myMethod(my_object)
```

Validity checking
```r
setValidity("MyClass", function(object) {
    if (object@n == 0)
        return("WRONG")
    return(TRUE)
})
```
or when defining the class:
```r
MyClass <- setClass("MyClass", representation=representation(n='integer'),
                    validity=function(object) { if (object@n > 0) TRUE else "WRONG" })
```

Default values in initializer:
```r
setClass("MyClass", representation=representation(a='integer'), prototype=prototype(a=4L))
```

Empty object: a class should ensure that when calling `new()` without arguments, the length of the returned object is zero:
```r
cl <- setClass("MyClass", representation=representation(a='integer'))
setMethod("length", "MyClass", function(x) { return(length(x@a)) })
```


### RC (Reference Classes, aka R5)

 * [Reference classes](http://adv-r.had.co.nz/R5.html).
 * [Objects With Fields Treated by Reference (OOP-style)](https://stat.ethz.ch/R-manual/R-devel/library/methods/html/refClass.html).
 * [ReferenceClasses](https://www.rdocumentation.org/packages/methods/versions/3.6.0/topics/ReferenceClasses).

An RC object is an S4 object.

```r
library(methods)
```

Fields declaration:
```r
MyClass <- setRefClass('MyClass', fields=list(size='numeric', name='character'))
```

A field type can be a class:
```r
MyClass <- setRefClass('MyClass', fields=list(size='numeric', name='character', somefield='SomeOtherClass'))
```
But then the field must be initialized to a class instance of that (or a derived class). It can't be initialized to NULL.
In order to be able to set a field to NULL, the field must be of type ANY.
A more correct solution is to set the field to NA in the constructor declaration.

If a valid can be null, it must declared as `"ANY"`:
```r
MyClass <- setRefClass('MyClass', fields=list(someFieldThatCanBeNull='ANY'))
```

#### Inheritance

To inherit from another class
```r
MyClassA <- setRefClass("MyClassA", contains = "MyClassB")
```

!!! For inheritance to work, all arguments of a parent's constructor must have default values (i.e.: an empty constructor must be provided). This is a S4 requirement.
```r
A <- setRefClass("A", fields = list(n = "numeric"),
                 methods = list( initialize = function(a = 0, ...) {
                                 n <<- a
                                 callSuper(...)
                                 }
                               ))

B <- setRefClass("B", methods = list(initialize = function(...) {
                                     callSuper(...)
                                     }
                                    ))
```

To avoid setting default values of fields to empty values like 0 or "", one can set them to NA:
```r
A <- setRefClass("A", fields = list(n = "numeric"),
                 methods = list( initialize = function(a = NA_integer_, ...) {
                                 n <<- a
                                 callSuper(...)
                                 }
                               ))
```

To test inheritance, see S4.

To inherit from more than one class, use a vector to declare all super classes:
```r
C <- setRefClass('C', contains = c('A', 'B'))
```
Apparently, only the first class will provide field members, subsequent classes will only be seen as interfaces.

#### Field validity

The S4 `validObject()` method can be implemented for R5 classes since they are S4 classes.

```r
A <- setRefClass("A", fields=list(x="numeric"))

setValidity("A", function(object) {
    if (length(object$x) != 1L || !all(object$x < 11))
        "'x' must be length 1 and < 11"
    else
        NULL
})
a = A(x=11)
validObject(a)
```
Error in validObject(a) : 
    invalid class "A" object: 'x' must be length 1 and < 11

### R6

 * [Introduction](https://r6.r-lib.org/articles/Introduction.html).
 * [R6 class documentation (with roxygen2)](https://www.tidyverse.org/blog/2019/11/roxygen2-7-0-0/#r6-documentation).
 * [R6 class documenting](https://roxygen2.r-lib.org/articles/rd.html#r6).
 * [R6](https://adv-r.hadley.nz/r6.html).
 * [R6: Encapsulated object-oriented programming for R](https://r6.r-lib.org/).
 * [Handling R6 objects in C++](https://www.r-bloggers.com/2020/07/handling-r6-objects-in-c/).
 * [Authors and Citation](https://r6.r-lib.org/authors.html#citation).

Multiple inheritance is not possible with R6.

Export a class with `roxygen2`:
```r
#' @export
MyClass <- R6::R6Class('MyClass',
public=list(
# ...
))
```

Get values of private fields of current object inside a list:
```r
fieldNames <- names(MyClass$private_fields)
fields <- lapply(fieldNames, function(f) private[[f]])
names(fields) <- fieldNames
```

Call a private method:
```r
myObj$.__enclos_env__$private$myMethod()
```

## Memory

Get pointer/reference of an object:
```r
tracemem(x)
```

Getting reference count:
```r
library(pryr)
address(x) # reference
refs(x) # reference ocunt
```

## Error & warning handling

Throwing an error:
```r
stop("Bad bad error !") # Print message on stderr and quit function immediatly
stopifnot(cond1, cond2, cond3, ...) # quit if conditions are not satisfied. No possibility of leaving a message.
```
To have traceback printed when an error occur, we must provide a user defined function:
```r
options(error = function() { traceback(2) ; q(status = 1) } )
```

To have warnings treated as errors, set the `warn` option to 2:
```r
options(error = function() { traceback(2) ; q(status = 1) }, warn = 2 )
```

Printing a warning:
```r
warning("This was wrong...") # Print message
```
Warning are not displayed by default.
To display them as they occur:
```r
options(warn = 1)
```
To treat them as errors:
```r
options(warn = 2)
```
By default `warn` is set to 0, which means that warnings are stored until the top-level function returns. In order to display all stored warnings, you need to call the `warnings()` method at the end of your code:
```r
warnings()
```

Suppressing warnings like for instance "NAs introduced by coercion":
```r
suppressWarnings(as.integer('WRONG'))
```

Call a function and display all errors on stderr without failing:
```r
try(my_function(a,b,c))
```

Error during wrapup:
`Error during wrapup: one of "yes", "no", "ask" or "default" expected.`
An `Error during wrapup` happens when inside the error callback function defined with `options(error = ...)` (see upon).
The message `one of "yes", "no", "ask" or "default" expected.` is due to the fact that a bad value is passed to the save parameter if the `quit()` method.


### trace and untrace functions

Trace a function:
```r
trace(foo)
```
Each time foo() will be called, a message will be printed.

Stop tracing a function:
```r
untrace(foo)
```

### trace/untrace with the methods package

Set a breakpoint for a class:
```r
MyClass$trace(myFct, browse)
```

Set a breakpoint for an object:
```r
obj$trace(myFct, browse)
```

Cancel a break point:
```r
MyClass$untrace(myFct)
```

## Profiling

```r
Rprof()
#- some code
Rprof(NULL)
#- write a file Rprof.out
```

To read Rprof.out:
```r
summaryRprof()
```

```r
profvis::profvis
```

```r
bench::mark()
```

## Graphical output

```r
pdf("xh.pdf")
hist(rnorm(100))
dev.off()
```

Plot a set of points:
```r
plot(x, y)
```

To a open a new window for plotting:
```r
windows() # on Windows platform
quartz() # on MacOS-X platform
X11() # on UNIX platform (opens a Quartz window on MacOS-X)
```

Plotting columns of a data frame on the same graphic, with first column as X values:
```r
require(ggplot2)
require(reshape2)
mydf <- melt(mydf,  id.vars='myfirstcolumn', variable.name='series')
ggplot(mydf, aes(myfirstcolumn, value)) + geom_line(aes(colour=series))
```

## HPC

TODO see CRAN packages doMC and DoSnow.

 * [High-Performance and Parallel Computing with R](https://cran.r-project.org/web/views/HighPerformanceComputing.html).
 * [(A Very) Experimental Threading in R – Random Remarks](https://random-remarks.net/2016/12/11/a-very-experimental-threading-in-r/).
 * [GPU computing](http://www.r-tutor.com/gpu-computing).

 * [Parallel processing with foreach package](https://riptutorial.com/r/example/5164/parallel-processing-with-foreach-package).

See packages Rmpi, foreach, doParallel.

## Some R error messages

### row names were found from a short variable and have been discarded

Error message from cbind.
Set `row.names = NULL` in cbind:
```r
cbind(df.1, df.2m row.names = NULL)
```

### could not find function "getGeneric"

The exact error is:
```
Error in getGeneric("$") : could not find function "getGeneric"
```

When running script from `Rscript`, the `methods` library is not loaded by default.
Add the following line at the start of your R script file:
```r
library(method)
```

### Registered S3 method overwritten

Messages:
     Registered S3 method overwritten by 'R.oo':
       method        from
       throw.default R.methodsS3
     Registered S3 method overwritten by 'openssl':
       method      from
       print.bytes Rcpp

To mute R 3.6 "Registered S3 method overwritten" warning messages:
```sh
export _R_S3_METHOD_REGISTRATION_NOTE_OVERWRITES_=no
```

