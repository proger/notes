# zoo

Compute mean with sliding window, with output vector same length as input vector:
```r
B <- c(0, 0, 0, 1, 0, 1, 1, 1, 0)
k <- 3
zoo::rollapply(B, 2*k-1, function(x) max(zoo::rollmean(x, k, na.rm = TRUE)), partial = TRUE)
```

Replace NA values by latest non-NA value:
```r
zoo::na.locf(c(1,2,NA,NA,5,NA,9,10,NA))
```
