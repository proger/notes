# tools

Get user cache folder for a package:
```r
tools::R_user_dir('mypkg', which='cache')
```
Returns `~/.cache/R/mypkg`.
Possible values for `which`: `data`, `config` and `cache`.
For `data`, returns `~/.local/share/R/mypkg`.
