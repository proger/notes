# foreach

HPC package.

Define the `foreach` statement for looping on a list and apply a function on
each element like lapply.  The main interest of the package is its capacity to
execute in parallel with the doParallel package.
