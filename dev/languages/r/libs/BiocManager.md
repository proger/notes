# BiocManager

Install a Bioconductor package:
```r
if (!requireNamespace("BiocManager", quietly=TRUE))
	install.packages("BiocManager")
BiocManager::install("mypkg")
```

Search for packages by name in CRAN and Bioconductor:
```r
BiocManager::available('kegg')
```
