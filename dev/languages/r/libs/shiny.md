# shiny

 * [Shiny](https://shiny.rstudio.com).
  + [LESSON 1 Welcome to Shiny](https://shiny.rstudio.com/tutorial/written-tutorial/lesson1/).
  + [Introduction to Shiny Server](https://shiny.rstudio.com/articles/shiny-server.html).
  + [Shiny Server Professional v1.5.16 Administrator's Guide](https://docs.rstudio.com/shiny-server/).
 * [Appli SHINY Covid19 par Eric Venot](https://ericvenot.shinyapps.io/suivicovid/).
  + [suivicovid](https://github.com/ericvenot/suivicovid).
 * [Application layout guide](https://shiny.rstudio.com/articles/layout-guide.html).
 * [Reference](https://shiny.rstudio.com/reference/shiny/).

 * For interactive data visualization see [plotly](https://plotly-r.com/).

Shiny is a package for building interactive web apps.

Create a folder `myapp` and write an `app.R` script inside.
You can then run your script with:
```r
shiny::runApp("myApp")
```

Inside the `app.R` script
```r
ui <- ...
server <- function(input, output) {
	# ...
}
shinyApp(ui=ui, server=server)
```

Inside the `ui` definition, we use `imputId` and `outputId` parameters to set input and output names. We can then use those names inside the server function with `input$myInput` or `output$myOutput`.

Order of code execution:
 1. The `app.R` script is first executed when creating the app (`runApp()` call). What is inside `ui` and `server` is not executed at this stage.
 2. When a new user's session is created (i.e.: a new user connects to the server), the code inside `ui` and `server` (but the not the code inside `reactive()` or `render*()` functions) is executed.
 3. Whenever the user interact with a GUI widget, the corresponding codes (i.e.: if dependent on widget input) in `reactive()` and `render*()` defintions are called.

Plot output (inside `ui` definition):
```r
plotOutput(outputId="myPlotId")
```

Build plot inside server function:
```r
output$myPlotId <- renderPlot({ ... })
```

UI layout pages:
 * `fluidPage`: rows, one element in each row. Can be combined with `fluidRow` to put more than one element in a row.
 * `navbarPage`

UI layouts:
 * `sidebarLayout`: includes `sidebarPanel` and `mainPanel`.

UI panels:
 * `sidebarPanel`
 * `mainPanel`
 * `titlePanel`

## Running

 * [How to launch a Shiny app](https://shiny.rstudio.com/articles/running.html).
 * [App formats and launching apps](https://shiny.rstudio.com/articles/app-formats.html).

Server types: Shiny Server (open source), [shinyapps.io](http://shinyapps.io/), [RStudio Connect](https://www.rstudio.com/products/connect/), Shiny Server Pro.
Server:
 * [Download Shiny Server](https://www.rstudio.com/products/shiny/download-server/).
 * [Shiny Server](https://shiny.rstudio.com/articles/shiny-server.html).
 * [Shiny Server Open Source Administrator's Guide](https://rstudio.github.io/shiny-server/os/latest/).
 * [Installation et administration de shiny-server](Shiny Server Open Source v1.4.0 Administrator's Guide).

## Input widgets

 * [LESSON 3 Add control widgets](https://shiny.rstudio.com/tutorial/written-tutorial/lesson3/).
 * [Build a dynamic UI that reacts to user input](https://shiny.rstudio.com/articles/dynamic-ui.html).

Slider input:
```r
sliderInput(inputId="mySliderId", label="My Title", min=1, max=50, value=20)
```

## Dynamic input widget

Insde the UI:
```r
uiOutput("numberOfCountries"),
```

Inside the server:
```r
output$numberOfCountries <- renderUI({
    n <- length(getAllCountries())
    sliderInput("nCountries", h3("Number of countries to select"),
               min=1, max=n, value=10)
})
# ...
getTopCountries <- reactive({computeSelectedCountries(getTotalNbCasesByCountry(), input$nCountries)$country})
```

## Loading data

 * [LESSON 6 Use reactive expressions](https://shiny.rstudio.com/tutorial/written-tutorial/lesson6/).
 
User session data must be loaded using `reactive()` function inside the server:
```r
server <- function(input, output) {
	myData <- reactive({myFctToLoadData(input$someInputId)})
}
```

Application data can be loaded at two different places: when `app.R` script is loaded, or when a user's session is created:
```r
myAppData1 <- loadMyData() # Executed when script is sourced

ui <- ...

server <- function(input, output) {
	myAppData2 <- loadDataForNewUser() # Executed when for a new user's session
}
```

## Output widgets

 * [LESSON 4 Display reactive output](https://shiny.rstudio.com/tutorial/written-tutorial/lesson4/).

## HTML

HTML functions can be used to format text:
```r
mainPanel(
	h1("First level title"),
	h2("Second level title"),
	...
)
```

Use an attribute:
```r
h6("My title", align="center")
```
List of functions:

Function | HTML Tag   | Description
-------- | ---------- | -----------
p        | `<p>`      | A paragraph of text
h1       | `<h1>`     | A first level header
h2       | `<h2>`     | A second level header
h3       | `<h3>`     | A third level header
h4       | `<h4>`     | A fourth level header
h5       | `<h5>`     | A fifth level header
h6       | `<h6>`     | A sixth level header
a        | `<a>`      | A hyper link
br       | `<br>`     | A line break (e.g. a blank line)
div      | `<div>`    | A division of text with a uniform style
span     | `<span>`   | An in-line division of text with a uniform style
pre      | `<pre>`    | Text ‘as is’ in a fixed width font
code     | `<code>`   | A formatted block of code
img      | `<img>`    | An image
strong   | `<strong>` | Bold text
em       | `<em>`     | Italicized text
HTML     |            | Directly passes a character string as HTML code
