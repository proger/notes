# utils

Open documentation for a subject:
```r
utils::help('myFunctionName', package='mypkg')
```

Unzip an archive:
```r
utils::unzip(zip.path, exdir=extract.dir)
```

Zip a file:
```r
utils::zip(zip.path, c('my/first/file.txt', 'mySecondFile.xml'))
```

## download.file


```r
utils::download.file(url=url, destfile=dest.file, mode='wb',
    method='auto', cacheOK=FALSE, quiet=FALSE)
```
