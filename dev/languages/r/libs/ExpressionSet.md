# ExpressionSet

An encapsulation of the 3 tables as used in W4M.
See SummarizedExperiment package for an improvement of this package.
ExpressionSet may become deprecated and be replaced by SummarizedExperiment at
some point in Bioconductor.
