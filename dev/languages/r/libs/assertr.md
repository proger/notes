# assertr

 * [assertr](https://cran.r-project.org/web/packages/assertr/assertr.pdf).

User assertions into analysis pipelines (mainly for data frames).
