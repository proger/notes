# BiocParallel

 * [BiocParallel](https://bioconductor.org/packages/release/bioc/html/BiocParallel.html).

HPC package.

Parallelize `lapply()`.

```r
BiocParallel::bplapply(mylist, function(x) { ... })
```
