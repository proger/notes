# colorout

 * [colorout](https://github.com/jalvesaq/colorout).

It colorizes R output.

Not on CRAN because " it used non-API entry points not allowed by the CRAN policies.".

Install from github:
```r
devtools::install_github("jalvesaq/colorout", dependencies = TRUE)
```
