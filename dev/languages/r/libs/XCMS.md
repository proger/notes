# XCMS

Framework for processing and visualization of chromatographically separated and single-spectra mass spectral data.

 * [XCMS official page](http://www.bioconductor.org/packages/release/bioc/html/xcms.html).
 * [XCMS^plus^](http://sciex.com/about-us/news-room/xcmssupplus/sup), commercial version which is a personal cloud version of *XCMS Online*.

To install XCMS library, run the following lines in R:
```r
source("http://bioconductor.org/biocLite.R")
biocLite("xcms")
```
