# attachment
<!-- vimvars: b:markdown_embedded_syntax={'r':''} -->

Get package dependencies.

Extract package dependency names from R files:
```r
attachment::att_from_rscripts('~/my/folder/', recursive=FALSE)
```
