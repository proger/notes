# metfRag

[MetFragR](https://github.com/c-ruttkies/MetFragR).

Installing:
```r
source("http://bioconductor.org/biocLite.R")
biocLite("KEGGREST")
library(devtools)
install_github("c-ruttkies/MetFragR/metfRag")
```

```r
library(metfRag)
```
