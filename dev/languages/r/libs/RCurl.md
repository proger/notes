# RCurl

```r
library(RCurl)
```

Getting URL content:
```r
txt <- getURL("http:/.../")
```

Setting the user agent:
```r
txt <- getURL("http:/.../", useragent="MyApp ; www.myapp.fr ; pierrick.rogermele@icloud.com")
```
