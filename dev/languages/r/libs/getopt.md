# getopt

```r
library(getopt)

spec = matrix(c(
 #- longname        shortname   arg_presence    arg_type        description
	'db',           'd',        1,              'character',    'Database name.',
	'help',         'h',        0,              'logical',      'Print this help.',
	'input',        'i',        1,              'character',    'Input excel file.'
 #- arg_presence can be 0 (no arg), 1 (required), 2 (optional)
	), byrow = TRUE, ncol = 5)

opt <- getopt(spec)
```
