# tripack

Triangulation of irregularly spaced data (Delaunay triangulation). Used for visualizing Voronoï cells.
