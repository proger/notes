# MASS

LDA (Linear discriminant analysis) function:
```r
library(MASS)
lda(x, ...)
```

LDA function can complain that "variables appear to be constant within groups".
This means that it has detected the matrix as singular.
It can be true, which really means some variables are constant.
Or it can be that the data is poorly scaled. In that case the threshold must be changed.
The condition to detect that the matrix is singular is that a variable has within-group variance less than tol^2.
"tol" can be set in the parameters
```r
lda(....., tol = 0.0000001, ....)
```
