# XML

Parsing XML file:
```r
xml <- XML::xmlInternalTreeParse("myfile.xml")
```

Parsing HTML file:
```r
xml <-  XML::htmlTreeParse(myfile, useInternalNodes=TRUE)
```

Parsing XML from string:
```r
xml <- XML::xmlInternalTreeParse(s, asText=TRUE)
```

Parsing HTML from string:
```r
xml <-  XML::htmlTreeParse(content, asText=TRUE, useInternalNodes=TRUE)
```

Saving an XML tree into a file:
```r
XML::saveXML(myxml, myfile)
```

Writing an XML tree into a string:
```r
xmlstr <- XML::saveXML(myxml)
```

Getting a list of nodes:
```r
nodes <- XML::getNodeSet(xml, "//ExtendedCompoundInfo")
```

Get a node's text content:
```r
txt <- XML::xpathSApply(xmldoc, "//mynode", XML::xmlValue)
```

Get attributes:
```r
ids <- XML::xpathSApply(xml, '//book', XML::xmlGetAttr, 'id')
```

XML using an anonymous namespace
If the XML top node contains an xmlns attribute (ex: <mytopnode xmlns="http://..../"...>), then it must be defined with a prefix while searching using XPath, otherwise XPath will return nothing.
```r
txt <- XML::xpathSApply(xmldoc, "//mynamespace:mynode", XML::xmlValue, namespaces = c(mynamespace = "http://..../"))
```

Getting XML namespaces:
```r
xml <-  XML::xmlInternalTreeParse(xmlstr, asText = TRUE)
print(XML::xmlNamespace(xmlRoot(xml)))
```
