# git2r

 * [git2r](https://github.com/ropensci/git2r).

Interface to the 'libgit2' library.

Test if inside git repos:
```r
git2r::in_repository('myFolder')
```

Initialize git repos for current directory:
```r
git2r::init('.')
```

Add a remote:
```r
git2r::remote_add('.', 'origin', 'https://github.com/myaccount/myrepos')
```
