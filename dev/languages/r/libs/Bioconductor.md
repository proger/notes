# Bioconductor

 * [Bioconductor](http://www.bioconductor.org).
 * [Installing and Managing Bioconductor Packages](https://cran.r-project.org/web/packages/BiocManager/vignettes/BiocManager.html).
 * [Advanced R / Bioconductor Programming](https://www.bioconductor.org/help/course-materials/2012/Seattle-Oct-2012/AdvancedR.pdf).
 * [Bioc-devel -- Bioconductor Developers' List](https://stat.ethz.ch/mailman/listinfo/bioc-devel).
 * [Release schedule](https://bioconductor.org/developers/release-schedule/).

 * [Docker containers for Bioconductor](https://www.bioconductor.org/help/docker/).

 * [Resources for Bioconductor package reviewers](http://contributions.bioconductor.org/reviewer-resources-overview.html).

 * [Source Control](https://bioconductor.org/developers/how-to/git/).
 * [New package workflow](https://bioconductor.org/developers/how-to/git/new-package-workflow/).
 * [Sync an existing GitHub repository with Bioconductor](https://bioconductor.org/developers/how-to/git/sync-existing-repositories/).
 * [Bioconductor build/check results](https://bioconductor.org/checkResults/).

 * Access the code of a package, example with biodb code: <https://code.bioconductor.org/browse/biodb/>.

 * [Developers](https://bioconductor.org/developers/).
 * [Developers RSS feeds](https://bioconductor.org/developers/rss-feeds/)

Build failing:
 * [Troubleshooting Build Report](https://contributions.bioconductor.org/troubleshooting-build-report.html)
 * [Docker containers for Bioconductor](http://bioconductor.org/help/docker/).
  + Run RStudio in browser: `docker run -e PASSWORD=bioc -p 8787:8787 bioconductor/bioconductor_docker:RELEASE_3_16`, then open <http://localhost:8787> with credentials rstudio / bioc.
  + Run R or bash in terminal: `docker run -it --user rstudio bioconductor/bioconductor_docker:RELEASE_3_16 <mycmd>`
  + Run R or bash in terminal with mounted folder: `docker run -it --user rstudio -v $HOME/dev:/home/dev bioconductor/bioconductor_docker:RELEASE_3_16 <mycmd>`

 * [Long Tests](https://bioconductor.org/developers/how-to/long-tests/).
  + Put a `longtests` folder at the package's root, with the same structure as the `tests` folder.
  + Add a `.BBSoptions` file at the package's root containing `RunLongTests: TRUE`.

 * [Check results](https://bioconductor.org/checkResults/) of packages:
  + [Bioc 3.13 check results](https://bioconductor.org/checkResults/3.13/bioc-LATEST/).
  + [Bioc 3.14 check results](https://bioconductor.org/checkResults/3.14/bioc-LATEST/).
  + [Bioc 3.13 Long Tests check results](http://bioconductor.org/checkResults/3.13/bioc-longtests-LATEST/index.html).

## Installing

 * [Using Bioconductor](https://bioconductor.org/install).

See CRAN package `BiocManager`.

Install Bioconductor core packages:
```r
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install()
```

Install a Bioconductor package:
```r
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install('MyPkg')
```

Install all Bioconductor packages (R version <= 3.4):
```r
source("http://bioconductor.org/biocLite.R")
biocLite()
```

Install one package of Bioconductor (R version <= 3.4):
```r
source("http://bioconductor.org/biocLite.R")
biocLite(c("GenomicFeatures", "AnnotationDbi"))
```

## Creating a bioconductor package

 * [Support site](https://support.bioconductor.org/). --> Add package name to watched tags.
 * [Contributions repos](https://github.com/Bioconductor/Contributions).

 * [Package Submission](https://bioconductor.org/developers/package-submission/).
 * [Coding Style](https://bioconductor.org/developers/how-to/coding-style/).

Le fichier 'NEWS' dans les packages (i.e. change log) suit un format spécifique (il est parsé par bioc). La core team recommande le fichier NEWS <http://www.bioconductor.org/packages/devel/bioc/news/Rsamtools/NEWS> comme exemple.

The `inst/` folder:
 * [Installed files](http://r-pkgs.had.co.nz/inst.html).

## Vignettes

 * [Authoring R Markdown vignettes with Bioconductor style](https://bioconductor.org/packages/3.7/bioc/vignettes/BiocStyle/inst/doc/AuthoringRmdVignettes.html).

See package `BiocStyle`.

## Testing under Travis-CI

When using `r: bioc-devel` for the version of R:
```
Error: Bioconductor version '3.13' requires R version '4.1'; use
  `BiocManager::install(version = '3.12')` with R version 4.0; see
```
