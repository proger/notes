# tidyverse

 * [tidyverse](https://www.tidyverse.org/).
 * [R for Data Science](https://r4ds.had.co.nz/). About the Tidyverse.

A package gathering the following packages: `dplyr`, `ggplot2`, `tibble`,
`tidyr`, `readr`, `purrr`, `stringr`, `forcats`.
