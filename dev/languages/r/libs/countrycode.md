# countrycode

To get list of official country codes, country names, continent of a country, etc.

Get full & normalized country name:
```r
countrycode::countrycode('Antigua', origin='country.name', destination='country.name')
```

Get table with continents and country names:
```r
countrycode::codelist[, c('continent', 'country.name.en')]
```
