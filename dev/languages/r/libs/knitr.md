# knitr

 * [Knitr](https://r-pkgs.org/vignettes.html#knitr), for including code in a vignette.
  + [Knitr options](https://yihui.org/knitr/options/).

Here is the settings to put inside the `DESCRIPTION` file:
```
VignetteBuilder: knitr
Suggests:
	knitr,
	rmarkdown
```

Do not evaluate code in vignette:
```{r, eval=FALSE}
if (!requireNamespace("BiocManager", quietly=TRUE))
    install.packages("BiocManager")
BiocManager::install('biodbHmdb')
```

Do not display the code:
```{r, echo=FALSE}
sum(1:10)
```

Do not display the text output:
```{r, results=FALSE}
sum(1:10)
```
or
```{r, results='hide'}
sum(1:10)
```

Hide messages (from `message()`):
```{r, message=FALSE}
require('magick')
```
