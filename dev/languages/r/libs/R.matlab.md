# R.matlab

 * [Package R.matlab](http://search.r-project.org/library/R.matlab/html/R.matlab-package.html).

```r
library(R.matlab)
```

## Read/write matlab data files

```r
A <- matrix(1:27, ncol=3)
B <- as.matrix(1:10)

filename <- paste(tempfile(), ".mat", sep="")
writeMat(filename, A=A, B=B)
data <- readMat(filename)
print(data)
unlink(filename)
```

## Evaluate matlab code

Run Matlab server:
```r
Matlab$startServer()
```
The 'matlab' executable must be in the PATH.

Create a matlab client:
```r
matlab <- Matlab()
```

Open connection:
```r
open(matlab)
```

Set detailed output:
```r
setVerbose(matlab, -2)
```

Evaluate matlab code:
```r
evaluate(matlab, "A=1+2;", "B=ones(2,20);")
```

Get variables:
```r
matlab_vars <- getVariable(matlab, c("a", "b"))
```

Create a function:
```r
setFunction(matlab, "          \
	function [win,aver]=dice(B)  \
	%Play the dice game B times  \
	gains=[-1,2,-3,4,-5,6];      \
	plays=unidrnd(6,B,1);        \
	win=sum(gains(plays));       \
	aver=win/B;                  \
");
```

Call function:
```r
evaluate(matlab, "[w,a]=dice(1000);")
```

Close client and server (to verify: only if this is the last client ?):
```r
close(matlab)
```
