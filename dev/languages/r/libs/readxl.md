# readxl

Read Excel files.

```r
x <- readxl::read_xlsx('myfile.xlsx')
```
