# rJava

Allows to call Java from R.

## Install

If a jar is compiled with more recent version of Java than the one configure inside R and used to compile rJava, then rJava will complain telling something like "Unsupported major.minor version 52.0".

So be Careful of the version with which R is configured. To reconfigure Java inside R, run:
```bash
R CMD javareconf
#sudo chmod a+r /Library/Frameworks/R.framework/Resources/etc/ldpaths /Library/Frameworks/R.framework/Resources/etc/Makeconf
```
To get help about `javareconf`:
```bash
R CMD javareconf --help
```
On MacOS-X, make sure to set the proper env vars to point to the right version of Java. If `JAVA_HOME` is properly set, it should be:
```bash
export JAVA_CPPFLAGS="-I$JAVA_HOME/include -I$JAVA_HOME/include/darwin"
export JAVA_LIBS="-L$JAVA_HOME/jre/lib/server -L$JAVA_HOME/jre/lib -ljvm"
export JAVA_LD_LIBRARY_PATH="$JAVA_HOME/jre/lib/server:$JAVA_HOME/jre/lib"
R CMD javareconf
```

Then reinstall rJava from R:
```r
install.packages('rJava', type='source', dependencies = TRUE)
```
or for most recent package:
```r
install.packages("rJava",,"http://rforge.net/",type="source", dependencies = TRUE)
```

To check the version of Java used by rJava:
```r
library(rJava)
.jinit()
print(.jcall('java/lang/System', 'S', 'getProperty', "java.version"))
```
If it prints version 1.6 and you want 1.8, and you are under MacOS-X, then there may be an issue with Java OS-X version. Re-install the OS-X Java with (<https://support.apple.com/kb/DL1572?locale=en_US>).
It still doesn't work, then first try to look at what libs are loaded:
```bash
DYLD_PRINT_LIBRARIES=1 R
```

The loading of rJava
```r
library(rJava)
```
should print the line:
	dyld: loaded: /Library/Java/JavaVirtualMachines/jdk1.8.0_25.jdk/Contents/Home/jre/lib/jli/libjli.dylib
And the initializatio
```r
.jinit()
```
should print the lines:
	dyld: loaded: /Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home/bundle/Libraries/libclient64.dylib
	dyld: loaded: /Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Libraries/../Libraries/libjvmlinkage.dylib
	dyld: loaded: /Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Libraries/libverify.dylib
	dyld: loaded: /System/Library/Frameworks/JavaVM.framework/Versions/A/Frameworks/JavaNativeFoundation.framework/Versions/A/JavaNativeFoundation
	dyld: loaded: /Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Libraries/libjava.jnilib
	dyld: loaded: /System/Library/Frameworks/JavaVM.framework/Versions/A/Frameworks/JavaRuntimeSupport.framework/Versions/A/JavaRuntimeSupport
	dyld: loaded: /Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Libraries/libzip.jnilib
Check which is the current JDK:
```bash
ls -l /System/Library/Frameworks/JavaVM.framework/Versions
```
Reinstall Oracle JDK 1.8.
Reconfigure R (`R CMD javareconf`) and reinstall rJava.

## Use

Initialize rJava:
```r
.jinit()
```

Printing JRE version:
```r
print(.jcall('java/lang/System', 'S', 'getProperty', "java.version"))
```

Set class path and call a class method:
```r
library(rJava)
.jaddClassPath("my/paths/to/classes")
.jcall('my/name/space/MyClass', 'V', 'myMethod')
```
`.jcall` uses the JNI notation for returned parameter:
Type                    | Description
----------------------- | -------------
`V`                     | Void.
`B`                     | Byte.
`C`                     | Char.
`I`                     | Int.
`J`                     | Long.
`S`                     | Short.
`Z`                     | Boolean.
`F`                     | Float.
`D`                     | Double.
`[I`                    | 1D array of integers.
`[[I`                   | 2D array of integers.
`Lfr/path/to/my/Class`  | An object of that class.
`[Lfr/path/to/my/Class` | 1D array of instances of that class.

To know the signature of a java method, use `javap -s ...`.

Create an object:
```r
obj <- .jnew('my.name.space.MyClass')
```

Get the class path:
```r
cp <- .jclassPath()
```
