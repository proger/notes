# R.utils

 * [R.utils](https://cran.r-project.org/web/packages/R.utils/index.html).

Various programming utilities.

Fix the seed for random numbers:
```r
v <- R.utils::withSeed(runif(10), 1L)
```

Test if a package is loaded:
```r
R.utils::isPackageLoaded('mypkg')
```

Test if path is absolute:
```r
R.utils::isAbsolutePath(mypath)
```
It does not depend on the current platform.
Returns `TRUE` for:
 * `/...` --> On Windows, absolute path on current drive.
 * `\\...` --> On Windows, absolute path on current drive.
 * `C:...`

Get absolute path (does not fail if path does not exist):
```r
R.utils::getAbsolutePath(mypath)
```
