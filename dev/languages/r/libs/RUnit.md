# RUnit

```r
library(RUnit)
```

Assert true:
```r
checkTrue(1 < 2, "check1.")
```

Assert equality:
```r
checkEquals(10, my_var, "some text.")
```

Assert identity:
```r
checkIdentical(character(0), some_var, "Something isn't right.")
```

Assert exception:
```r
checkException(some_expression_or_function, "Some message") # checks that an exception is thrown
checkException(some_expression_or_function, "Some message", silent = TRUE) # Tells 'try' to do not print error message.
```
