# dplyr
<!-- vimvars: b:markdown_embedded_syntax={'r':''} -->

 * [dplyr](https://dplyr.tidyverse.org/index.html).

Rename columns:
```r
x <- x %>% rename(newNameA='colA', newNameB='colB')
```

Sum by grouping on one column:
```r
library(dplyr)
x <- data.frame(a=c('a', 'a', 'b', 'b'), b=1:4)
x %>% group_by(a) %>% summarize_at(vars(b), list(mySum=sum))
```

Sum by grouping on two columns:
```r
x <- data.frame(a=c('a', 'a', 'a', 'b', 'b', 'b'), b=c(1, 1, 2, 2, 3, 3), c=1:6)
x %>% group_by(a, b) %>% summarize_at(vars(c), list(mySum=sum))
```
