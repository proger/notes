# lgr

 * [lgr](https://cran.r-project.org/web/packages/lgr/index.html).

Logger package.

Can create several loggers. Thus one per package is possible, with different configurations.
