# magick

R frontend for imagemagick library.

 * [The magick package: Advanced Image-Processing in R](https://cran.r-project.org/web/packages/magick/vignettes/intro.html).

Display an image:
```r
myimg
```

Display an image and textual information:
```r
print(myimg)
```
