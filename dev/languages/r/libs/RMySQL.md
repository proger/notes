# RMySQL

```r
library(RMySQL)
```

Open the default test database provided by MySQL installation:
```r
if (mysqlHasDefault()) {
	conn <- dbConnect(RMySQL::MySQL(), dbname = "test")
	# Do some stuff...
	dbDisconnect(conn)
}
```

Send a query:
```r
result <- try(dbSendQuery(conn, query))
```

Test if everything went right:
```r
if (dbHasCompleted(result)) ...
```

## .my.cnf file

One can define connection settings inside the .my.cnf file:
``` {.linux-config}
[my_conn_settings]
host = localhost
user = root
password = root
database = 2biodb
```

The file should be of course only readable by the user.
And then use group parameter to dbConnect
```r
dbConnect(MySQL(), group = 'my_conn_settings')
```
The configuration file path is defined by the default.file parameter of dbConnect, that, by default, is set to `$HOME/.my.cnf` under UNIX type platforms and `C:\my.cnf` under Windows platforms.

## Installing RMySQL on Windows

On windows platform the RMySQL package isn't provided as binary. One needs to
compile it.

For this, one needs to install Rtools and MySQL, and then run:
```r
install.packages('RMySQL', type='source')
```
Be careful of choosing compatible binary versions (32 or 64 bits) for the 3 software: R, Rtools (and the extension to install: "Extras to build 32 bit R: TCL/TK, bitmap code, internationalization") and MySQL.
Eventually look at <http://www.r-bloggers.com/installing-the-rmysql-package-on-windows-7/>.
You'll have to define `MYSQL_HOME` env var to be `C:\Program Files\MySQL\MySQL Server 5.6`, and also to copy `libmysql.dll` from the lib folder of `C:\Program Files\MySQL\MySQL Server 5.6` to its bin folder.


