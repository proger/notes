# R.cache

 * [R.cache](https://cran.r-project.org/web/packages/R.cache/index.html).

Memoisation package: speed up function calls by caching calls.
