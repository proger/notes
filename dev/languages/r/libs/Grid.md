# Grid

 * [Grid](https://stat.ethz.ch/R-manual/R-devel/library/grid/html/00Index.html).

Use to make graphical presentation. See also [gridSVG](https://stat.ethz.ch/R-manual/R-devel/library/grid/html/00Index.html) to export grid object to SVG file.
