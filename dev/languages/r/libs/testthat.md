# testthat

 * [testthat v3.0.0](https://www.rdocumentation.org/packages/testthat/versions/3.0.0).
 * [testthat: Get Started with Testing](https://journal.r-project.org/archive/2011-1/RJournal_2011-1_Wickham.pdf).
 * [Package ‘testthat’](https://cran.r-project.org/web/packages/testthat/testthat.pdf).
 * [How to write tests for cpp functions in R package?](https://stackoverflow.com/questions/40989349/how-to-write-tests-for-cpp-functions-in-r-package).
 * [Running tests in parallel](https://testthat.r-lib.org/articles/parallel.html).

Testing code outside a package
 * Create a folder `test`.
 * Write a file `test_somename.R`.
 * Run `R --slave --no-restore -e "testthat::test_dir('test')"`.

*testthat* propose different reporters.
For instance, when called with devtools for a package, we can trigger a progress reporter (`ProgressReporter`) with a failure reporter (`FailReporter`, that will ensure that a fail message is sent):
```r
R -q -e "devtools::test('.', reporter = c('progress', 'fail'))"
```
Or use the Stop reporter (`StopReporter`) that will stop at the first error:
```r
R -q -e "devtools::test('.', reporter = c('stop', 'summary', 'fail'))"
```

Open a context (needs to be done to split tests in different parts):
```r
testthat::context("My context of tests")
```

Run a test:
```r
testthat::test_that("Something works", {
	x <- 5
	some_code_to_run(x)
	# ...
})
```

Assertions:
```r
testthat::expect_is(myObj, 'MyClass') # Uses `inherits()`
testthat::expect_is(x, 'data.frame')
testthat::expect_type(x, 'integer')
testthat::expect_type(x, 'double')
testthat::expect_type(x, 'list')
testthat::expect_s3_class(x, 'data.frame')
testthat::expect_s4_class(myObj, 'MyS4Class')
```
