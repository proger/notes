# devtools
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','r':''} -->

Install:
```sh
# Install dependencies
conda install -y libcurl pkg-config # Conda
apt install -y libcurl4-openssl-dev libxml2-dev # Debian

#  Install package
R -e "install.packages('devtools')"
```

Installing from a git repos:
```r
devtools::install_git("https://gitlab.com/proger/biodb.git", branch='develop', credentials=git2r::cred_user_pass ("proger", getPass::getPass()))
```

Installing local sources:
```r
devtools::install_local('~/dev/biodb/')
```

DEPRECATED: Building on Windows for testing:
```r
devtools::build_win('~/dev/biodb')
```

Build vignettes:
```sh
R --slave --no-restore -e "devtools::build_vignettes('.')"
```
`pandoc` is needed to build vignettes.

Install dependencies:
```sh
R --slave --no-restore -e "devtools::install_dev_deps('.')"
```

Install package:
```sh
R --slave --no-restore -e "devtools::install_local('.', dependencies=TRUE, build_manual=TRUE, build_vignettes=TRUE)"
```

Uninstall package:
```sh
R --slave --no-restore -e "devtools::uninstall('.')"
```

Run checks:
```sh
R --slave --no-restore -e "devtools::check('.')"
```
Run checks without vignettes:
```sh
R --slave --no-restore -e "devtools::check('.', vignettes=FALSE)"
```
