# Risa

Load ISA-Tab:
```r
isa <- readISAtab('some_isatab_dir')
```

Get assay file names:
```r
assay.fiel.names <- getMSAssayFilenames(isa)
```
