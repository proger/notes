# BiocCheck

Run checks on a Bioconductor package:
```sh
R --slave --no-restore -e 'BiocCheck::BiocCheck(".")'
```

2021.10.12, from Vince Carey (vjcitn):
Note that BiocCheck has an unusual version number. If you have a problem with the standard BiocCheck at this time, use
```r
BiocManager::install('vjcitn/BiocCheck', ref="vjcitn-patch-1")
```
to get a patched version.
