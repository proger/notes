# rmarkdown

 * [Introduction to R Markdown](https://rmarkdown.rstudio.com/articles_intro.html).
 * [Parameters](https://rmarkdown.rstudio.com/lesson-6.html).

Pass parameters:
```sh
R -e 'rmarkdown::render("myscript.Rmd", params=list(input="perf.tsv"))'
```
