# assertthat

 * [assertthat](https://cran.r-project.org/web/packages/assertthat/index.html).
 * [assertthat](https://github.com/hadley/assertthat).

Set of functions to write assertions for pre- and post- checking while calling
functions.
