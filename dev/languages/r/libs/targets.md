# targets

 * [targets](https://docs.ropensci.org/targets/).

Workflow manager.
 * Parallelisation of tasks.
 * Write tasks as targets and compute dependencies for rerunning targets only when needed.

Targets can push that on HPC cluster with `future.batchtools` or `clustermq`
packages.
