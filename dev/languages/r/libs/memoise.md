# memoise

 * [memoise](https://cran.r-project.org/web/packages/memoise/index.html).

Memoisation package: speed up function calls by caching calls.
