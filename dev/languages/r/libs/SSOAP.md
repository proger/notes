# SSOAP

No more in CRAN.

 * [Using R SOAP (SSOAP) to retrieve data from NOAA](http://stackoverflow.com/questions/24528351/using-r-soap-ssoap-to-retrieve-data-from-noaa).
 * [R Package SSOAP](http://arademaker.github.io/blog/2012/01/02/package-SSOAP.html).
 * [SOAP Client with WSDL for R](http://stackoverflow.com/questions/32594448/soap-client-with-wsdl-for-r). --> Example of using RCurl in replacement of SSOAP.
