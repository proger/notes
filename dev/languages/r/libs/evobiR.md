# evobiR

Too much dependencies.

Apply a function within a sliding window on a vector, output vector is smaller:
```r
data <- c(1,2,1,2,10,2,1,2,1,2,3,4,5,6,2,5)
evobiR::SlidingWindow("mean", data, 3, 1)
```
