# zip

 * [zip](https://cran.r-project.org/web/packages/zip/index.html).

```r
zip::unzip('myfile.zip', exdir='my_dst_folder')
```
