# BiocStyle

Here is the settings to put inside the `DESCRIPTION` file:
```
VignetteBuilder: knitr
Suggests:
	BiocStyle,
	knitr,
	rmarkdown
```
