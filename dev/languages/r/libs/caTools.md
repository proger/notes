# caTools

Contains trapz() function for integration (needs bitops).

AUC (Area Under Curve):
```r
library(caTools)
area <- trapz(vx, vy)
```
