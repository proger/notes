# Rcpp

 * [Rcpp: Seamless R and C++ Integration](http://dirk.eddelbuettel.com/code/rcpp.html).
 * [Rcpp API reference](https://dirk.eddelbuettel.com/code/rcpp/html/index.html).
 * [Rcpp for everyone](https://teuder.github.io/rcpp4everyone_en/).
 * [Extending R with C++: A Brief Introduction to Rcpp](http://dirk.eddelbuettel.com/code/rcpp/Rcpp-introduction.pdf).
 * [Writing a package that uses Rcpp](http://dirk.eddelbuettel.com/code/rcpp/Rcpp-package.pdf).
 * [Rewriting R code in C++](https://adv-r.hadley.nz/rcpp.html).

 * [Using R — .Call(“hello”)](https://www.r-bloggers.com/using-r-callhello/).
 * [Rcpp for Seamless R and C++ Integration](http://rcpp.org/).
 * [Calling C++ from R using Rcpp](https://www.r-bloggers.com/calling-c-from-r-using-rcpp/).
 * [Working with Rcpp::StringVector](http://gallery.rcpp.org/articles/working-with-Rcpp-StringVector/).
 * [Rcpp Attributes](http://dirk.eddelbuettel.com/code/rcpp/Rcpp-attributes.pdf).
 * [Frequently Asked Questions aboutRcpp](http://dirk.eddelbuettel.com/code/rcpp/Rcpp-FAQ.pdf).
 * [RcppQuick Reference Guide](http://dirk.eddelbuettel.com/code/rcpp/Rcpp-quickref.pdf).

 * [Exposing a C++ Student Class With Rcpp](https://www.gormanalysis.com/blog/exposing-a-cpp-student-class-with-rcpp/).
 * [Exposing C++ Classes into R Through Rcpp Modules](https://github.com/r-pkg-examples/rcpp-modules-student).
 * [ExposingC++functions and classeswith Rcpp modules](https://cran.r-project.org/web/packages/Rcpp/vignettes/Rcpp-modules.pdf).

Calling R from C++:
 * [Handling R6 objects in C++](https://gallery.rcpp.org/articles/handling-R6-objects-in-rcpp/).

## Testing C++ code with testthat

 * [How to write tests for cpp functions in R package?](https://stackoverflow.com/questions/40989349/how-to-write-tests-for-cpp-functions-in-r-package).

Step 1. Add testthat to LinkingTo field in DESCRIPTION file.

Step 2. Add `catch-routine-registration.R` file that declares `run_testthat_tests()`, in R folder. Content should be:
```r
(function() {
  .Call("run_testthat_tests", FALSE, PACKAGE="mypackage")
})
```

Step 3. Create file `test-cpp.R` inside `tests/testthat` folder. Content is:
```r
run_cpp_tests("mypackage")
```

Step 4. Create file `test-runner.cpp` inside `src` folder. Content:
```cpp
	#define TESTTHAT_TEST_RUNNER
	#include <testthat.h>
```

Step 5. Create your C++ test file (ex: `test-100-foo.cpp`). Content shoud be like:
```cpp
	#include <testthat.h>
	#include "myheader.hpp" // Where foo() is declared
	context("Testing my C++ functions") {
		test_that("foo() works correctly") {
			expect_true(foo() == 4)
		}
	}
```
