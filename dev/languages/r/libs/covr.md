# covr

 * [covr](https://covr.r-lib.org/).

Test coverage.

To add test coverage for a R package repository using Travis-CI:
```sh
R --slave --no-restore -e 'usethis::use_coverage()'
```
Then follow the instructions and include the badge inside the README file and update the Travis YAML file with a command for codecov update.

Run coverage on current folder (inside package root folder) with cobertura output:
```r
covr::to_cobertura(covr::package_coverage(), filename='cobertura.xml')
```

To run locally:
```sh
R --slave --no-restore -e "covr::codecov(token='my_token', quiet=FALSE)"
```
When failing inside tests, messages may not appear. In this case, run devtools to see errors:
```sh
R --slave --no-restore -e "devtools::check('.', vignettes=FALSE)"
```
