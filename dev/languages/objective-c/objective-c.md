MACOS-X
=======

## Play a sound

 * [NSSound](https://developer.apple.com/documentation/appkit/nssound).

```objc
NSSound *sound = [[NSSound alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mySound" ofType:@"mp3"] byReference:NO];
[sound play];
[sound release];
```

## Quick look

 * [Quick Look](https://developer.apple.com/library/mac/documentation/UserExperience/Conceptual/Quicklook_Programming_Guide/Introduction/Introduction.html). For displaying thumbnails and previews in Finder and Spotlight.

The daemon loads the appropriate generator using the document’s content-type UTI and generator’s information property list.
It looks in order:

 * `MyApp.app/Contents/Library/QuickLook/`.
 * `~/Library/QuickLook`, third party generators, accessible only to logged-in user.
 * `/Library/QuickLook`, third party generators, accessible to all users of the system.
 * `/System/Library/QuickLook`, Apple-provided generators, accessible to all users of the system.

The binary of a Quick Look generator must be universal and must be 32-bit only.
For OS X v10.6 and later, you must build Quick Look generators for both 32- and 64-bit

To get the UTI of a file, run:
```
mdls myfile
```
And look for the `kMDItemContentType` field.

# TO CLEAN
	===================== class.txt
	
	// compiler directives
	// begin with "@"
	
	// class declaration
	@interface ClassName : ItsSuperclass 
	{ 
	    instance variable declarations 
	} 
	method declarations 
	@end 
	
	// for use in another class declaration:
	@class AnotherClass, YetAnotherClass;
	
	// NSObject
	// It is necessary to derives from NSObject in order to use alloc function.
	// The following header file has to be included:
	#import <Foundation/Foundation.h>
	
	// methods
	// class methods begin with a '+':
	+ alloc;
	
	// instance methods begin with a '-':
	- (void)foo;
	
	// by default a method returns a value of type 'id'.
	
	// method with arguments:
	- (void)setWidth:(float)width height:(float)height;
	
	// method with variable number of arguments:
	- makeGroup:group, ...;
	
	// abstract class
	// There's no abstract class support in Objective-C.
	// One needs only to define a normal class, and put dumb code insode methods.
	
	// class implementation
	#import "ClassName.h" 
	@implementation ClassName 
	method definitions 
	@end 
	
	#import "myfile.h"
	// identical to #include, except that it checks a header is not loaded twice.
	
	// introspection
	if ( [anObject isMemberOfClass:someClass] ) 
		...;
	if ( [anObject isKindOfClass:someClass] ) 
		...; 
	
	// getting class object from an object
	Class aClass = [anObject class];
	
	// getting class object from a class
	Class aClass = [AClass class];
	
	// getting class object from a string
	Class aClass = NSClassFromString(className);
	
	// testing class
	if (anObject isKindOfClass:[AClass class])
		...;
	
	if ([objectA class] == [objectB class])
		...;
	
	// id
	// it is the default type for objects
	id anObject = [[AClass alloc] init];
	// or
	AClass* anObject = [[AClass alloc] init];
	
	// to set an object to null
	id anObject;
	anObject = nil;
	
	// instanciation
	myObject = [myClass alloc];
	// alloc creates an instance, and initializes members to zero.
	// to initialize with specific values, one creates an init method:
	myObject = [[myClass alloc] init];
	
	// implementing a singleton
	static MyClass *MCLSSharedInstance; 
	@implementation MyClass 
	+ (MyClass *)sharedInstance 
	{ 
	    // check for existence of shared instance 
	    // create if necessary 
	    return MCLSSharedInstance; 
	} 
	
	+ (Soloist *)soloist 
	{ 
	    static Soloist *instance = nil; 
	    if ( instance == nil ) 
	    { 
	        instance = [[self alloc] init]; 
	    } 
	return instance; 
	}
	
	initialize a class
	==================
	+ (void)initialize 
	{ 
	  if (self == [ThisClass class]) { 
	        // Perform initialization here. 
	        ... 
	    } 
	} 
	
	// initiliaze method is called by the runtime system for each class that defines it ==> don't call initialize method of superclass.
	
	// scope of instance variables
	@private
	@protected
	@public
	// @package: On 64-bit, an @package instance variable acts like @public inside the image that implements the class, but @private outside.
	
	// super & self
	// used as any object:
	[self setOrigin:someX :someY];
	
	// init
	- (id)init { 
		if (self = [super init]) { 
			...;
		}
	}
	
	// init can sometimes return nil (so freeing the allocated object), or a different object:
	id anObject = [[SomeClass alloc] init]; 
	if ( anObject ) 
		[anObject someOtherMessage]; 
	else 
		...;
	
	// initializing from a sub-class:
	- (id)init { 
		if (self = [super init])
			creationDate = [[NSDate alloc] init]; 
		return self; 
	}
	
	// failing in init:
	- (id)initWithImage:(NSImage *)anImage { 
		if (anImage == nil) { 
			[self release]; 
			return nil; 
		} 
	
		NSSize size = anImage.size; 
		NSRect frame = NSRectMake(0.0, 0.0, size.width, size.height); 
		if (self = [super initWithFrame:frame])
			image = [anImage retain]; 
		return self; 
	}
	// ==> even better to use convenience constructor in this case (no object creation):
	+ (id)myClassWithImage:(NSImage *)anImage {
		if (anImage == nil)
			return nil;
	}
	
	// factory function
	+ (id)rectangleOfColor:(NSColor *)color { 
		id newInstance = [[self alloc] init]; // EXCELLENT 
		[newInstance setColor:color]; 
		return [newInstance autorelease]; 
	}
	
	categories
	==========
	A category  allows you to add methods to an existing class.
	
	ClassName+CategoryName.h:
	#import "ClassName.h" 
	@interface ClassName ( CategoryName ) 
	// method declarations 
	@end 
	
	ClassName+CategoryName.m:
	#import "ClassName+CategoryName.h" 
	@implementation ClassName ( CategoryName ) 
		// method definitions 
	@end
	
		// extensions
		// Similar to categories, but allow additional required API to be declared for a class in locations other than within the primary class @interface block.
	
		// Class extensions are like “anonymous” categories, except that the methods they declare must be implemented in the main @implementation block for the corresponding class.
	
	@interface MyObject : NSObject 
	{ 
	    NSNumber *number; 
	} 
	- (NSNumber *)number; 
	@end 
	
	@interface MyObject () 
	- (void)setNumber:(NSNumber *)newNumber; 
	@end 
	
	@implementation MyObject 
	- (NSNumber *)number 
	{ 
	    return number; 
	} 
	- (void)setNumber:(NSNumber *)newNumber 
	{ 
	    number = newNumber; 
	} 
	@end 
	===================== main.txt
	// -*- mode:objc -*-
	
	// as in C
	int main(void) {
	    return 0;
	}
	===================== memory.txt
	AUTORELEASE
	===========
	
	returning an auto-releasing object
	----------------------------------
	– (NSArray *)sprockets {
	NSArray *array;
	array = [[NSArray alloc] initWithObjects:mainSprocket,
	auxiliarySprocket, nil];
	return [array autorelease];
	}
	
	Taking Ownership of Objects
	---------------------------
	– (void)setMainSprocket:(Sprocket *)newSprocket {
	if (mainSprocket != newSprocket) {
	[mainSprocket release];
	mainSprocket = [newSprocket retain];
	}
	}===================== properties.txt
	Declaring properties
	====================
	// MyClass.h 
	@interface MyClass : NSObject 
	{ 
	    float value; 
	    NSTextField *textField; 
	@private 
	    NSDate *lastModifiedDate; 
	} 
	@property float value; 
	@property (retain) IBOutlet NSTextField *textField; 
	@end 
	
	// MyClass.m 
	// Class extension to declare private property 
	@interface MyClass () 
	o@property (retain) NSDate *lastModifiedDate; 
	@end 
	@implementation MyClass 
	@synthesize value; 
	@synthesize textField; 
	@synthesize lastModifiedDate; 
	// implementation continues 
	@end 
	
	@synthesize
	===========
	The legacy runtime does not provide a variable automatically, so it must be declared in the class.
	The modern runtime does provide a variable.
	
	property attributes
	===================
	getter=
	setter=
	
	readwrite
	readonly
	
	assign		Specifies that the setter uses simple assignment. This is the default
	retain		increase object counter.
	copy		make a copy of the object.
	
	nonatomic
	
	@dynamic
	========
	instead of @synthesize, tell the compiler we will provide the getter and setter functions.
	
	@implementation MyClass 
	@dynamic value; 
	- (NSString *)value { 
	    return value; 
	} 
	- (void)setValue:(NSString *)newValue { 
	    if (newValue != value) { 
	        value = [newValue copy]; 
	    } 
	} 
	@end 
	
	specific instance variable to use
	=================================
	@synthesize age=yearsOld;
	
	readonly/readwrite
	==================
	a property can be declared readonly in the header file,
	and the re-declared readwrite in the code file.
	===================== protocol.txt
	// -*- mode:objc -*-
	
	@protocol MyXMLSupport
	-(void) initFromXMLRepresentation:(NSXMLElement *)XMLElement; 
	@property (nonatomic, readonly) (NSXMLElement *)XMLRepresentation; 
	@end
	
	// @optional and @required
	// =======================
	@protocol MyProtocol 
	- (void)requiredMethod; // required by default 
	@optional 
	- (void)anOptionalMethod; 
	- (void)anotherOptionalMethod; 
	@required 
	- (void)anotherRequiredMethod; 
	@end 
	
	// adopting a protocol
	// ===================
	@interface ClassName : ItsSuperclass < protocol list > 
	@end
	
	// extending a protocol
	// ====================
	@protocol NewProtocol: OldProtocol
	-(void) newMethod;
	@end
	===================== iOS/Info.plist.txt
	Info.plist resides in application folder.
	It is an XML file which describes the app to the OS.
	
	Either stored as plain XML or as binary/compressed format.
	To read a binary format, run the following command on a Macintosh computer:
	plutil –convert xml1 <plistfile>
	
	CFBundleExecutable: app executable file
	CFBundleIdentifier: product id (ex: com.eatoni.smstool), used to communicate with other apps and to register with SpringBoard. This id is also used to store app preferences.
	CFBundleIconFile: icon filename
	UIPrerenderedIcon: disable icon processing by SpringBoard.
	
	UIStatusBarStyle for setting the look and color of the status bar
	UIStatusBarHidden for hiding the status bar
	UIInterfaceOrientation lets you override the accelerometer to create a landscape- only (UIInterfaceOrientationLandscapeRight) presentation. 
	===================== iOS/MVC.txt
	View: View components are provided by children of the UIView class and by its associated (and somewhat misnamed) UIViewController class. 
	
	Controller: The controller behavior is implemented through three key technologies:delegation,target-action,and notification.
	
	Model: Model methods supply data through protocols such as data sourcing and meaning by implementing callback methods triggered by the controller.
	
	VIEWS
	=====
	UIView		root class
	addSubView	to add a child to a parent
	
	UIViewControler	root class for view controlers. 
			View controllers are there to make your life easier. They :
			_ take responsibility for rotating the display when a user reorients his or her iPhone.
			_ resize views to fit within the boundaries when using a navigation bar or a toolbar.
			_ handle all the interface’s fussy bits and hide the complexity involved in directly managing interaction elements.
			Moving between views:
			_ UINavigationController enables you to drill down between views, smoothly sliding your display between one view and the next.
			_ UITabBarController lets you easily switch between view controller instances using a tabbed display.
	
	UIViewControler is not a controler in the sense of MVC, it's a View, but every subclass of it has itss own loadView method which sets up all the triggers, callbacks, and delegates. It is convenient and easy to program.
	
	CONTROLLERS
	===========
	
	Delegation
	==========
	User interaction is passed to a delegate to handle it.
	The delegate is usually a view controller class or your main application delegate.
	
	example:
	--------
	@protocol FTPHostDelegate <NSObject> 
	- (void) percentDone: (NSString *) percent; 
	- (void) downloadDone: (id) sender; 
	- (void) uploadDone: (id) sender; 
	@end 
	@interface MergedTableController : UIViewController <UITableViewDelegate, 
	UITableViewDataSource> 
	{ 
	UIView                 *contentView; 
	UITableView            *subView; 
	UIButton               *button; 
	id <FTPHostDelegate>   *ftpHost; 
	SEL                    finishedAction; 
	} 
	@end 
	
	
	Target-action
	=============
	You’ll encounter these almost exclusively for children of the UIControlclass.With target-action,you tell the control to contact a given object when a specific user event takes place.
	
	example:
	--------
	UIBarButtonItem *helvItem = [[[UIBarButtonItem alloc] initWithTitle:@"Helvetica" style:UIBarButtonItemStyleBordered target:self action:@selector(setHelvetica:)] autorelease];
	
	Notifications
	=============
	Notifications enable objects in your application to talk among themselves.
	
	Notifications centers:
	_ NSNotificationCenter			in-application notification. Notification done in the form (name + data).
	_ NSDistributedNotificationCenter	interapplication notification. Not fully implemented on iPhone.
	_ DarwinNotificationCenter		interapplication notification. Used on iPhone. Only announcements, no data.
	_ TelephonyNotificationCenter		Telephony notifications are private and unpublished.Unfortunately,Apple did not open up this center,but if you sneak your way into listening to this special-purpose center,you’ll know when phone calls and SMS messages arrive.
	
	The kinds of notification vary by the task you are performing.For example, 
	notifications when rotating an application include 
	UIApplicationWillChangeStatusBarOrientationNotificationand 
	UIDeviceOrientationDidChangeNotification.In some cases,these correspond 
	strongly with existing delegate methods.In other cases,they don’t,so you’d be wise to 
	monitor notifications while writing programs to find any gaps in Apple’s delegate proto- 
	col implementations.Here’s how you listen: 
	[[NSNotificationCenter defaultCenter] addObserver:self 
	selector:@selector(trackNotifications:) name:nil object:nil]; 
	===================== iOS/NSBundle.txt
	This class makes it easy to locate your application’s root folder and to navigate down to your custom sub- folders to point to and load resources.
	===================== iOS/SpringBoard.txt
	SpringBoard is the equivalent of Finder
	It is responsible for authenticating the third-party apps.
	It also has a watchdog to stop apps when necessary (using too much memory or system resources in general).
	===================== iOS/XIB-NIB files.txt
	Interface Builder files have .XIB extension for iPhone and .NIB on Macintosh.
	
	It stores precooked addressable user interface classes.
	
	Removing XIB files
	==================
	When you develop programs that do not use XIB or NIB Interface-Builder bundles, remove the NSMainNibFilekey from Info.plist and discard the automatically generated MainWindow.xib file from to your project.
	===================== iOS/alter view.txt
	// -*- mode:objc -*-
	
	// a Yes/No alterview
	-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	}
	
	-(void) foo {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter connection error" message:@"Failed to connect to twitter.com. Try again ?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		[alert show];
		[alert release]; // --> is it really safe to release it here ?
	}
	
	// a no button alterview ==> it must disappear by itself after a short time ==> we use a timer
	// ================================================================================
	-(void) wordRemovedAlertCallback:(NSTimer*)timer {
		[wordRemovedAlert dismissWithClickedButtonIndex:0 animated:NO];
	}
	
	-(void) eatoniUserWordRemoved:(NSString*)word {
		if ( ! wordRemovedAlert) // wordRemovedAlert is a member variable of type UIAlertView*
			wordRemovedAlert = [[UIAlertView alloc] initWithTitle:@"User dictionary" message:[NSString stringWithFormat:@"Word \"%@\" removed from dictionary.", word] delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
		[NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(wordRemovedAlertCallback:) userInfo:nil repeats:NO];
		[wordRemovedAlert show];
	}
	===================== iOS/animations.txt
	_ beginAnimations:context. Marks the start of the animation block. 
	
	_ setAnimationCurve. Defines the way the animation accelerates and decelerates. 
	Use ease-in/ease-out (UIViewAnimationCurveEaseInOut) unless you have 
	some compelling reason to select another curve.The other curve types are ease in 
	(accelerate into the animation),linear (no animation acceleration),and ease out 
	(accelerate out of the animation).Ease-in/ease-out provides the most natural- 
	feeling animation style. 
	
	_ setAnimationDuration. Specifies the length of the animation,in seconds.This 
	is really the cool bit.You can stretch out the animation for as long as you need it 
	to run.Be aware of straining your user’s patience and keep your animations below 
	a second or two in length. 
	
	_ commitAnimations. Marks the end of the animation block. 
	
	example
	=======
	[UIView beginAnimations:nil context:context]; 
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut]; 
	[UIView setAnimationDuration:1.0]; 
	[contentView setAlpha:0.0f]; 
	[UIView commitAnimations]; 
	
	Note
	====
	Apple often uses two animation blocks one after another to add bounce to their anima- 
	tions. For example,they might zoom into a view a bit more than needed and then use a 
	second animation to bring that enlarged view down to its final size. Use “bounces” to add 
	a little more life to your animation blocks. Be sure that the animations do not overlap. 
	Either add a delay so that the second animation does not start until the first ends 
	(performSelector: withObject: afterDelay:) or assign an animation delegate 
	callback (animationDidStop: finished:) to catch the end of the first animation and 
	start the second.
	
	fading a view in and out
	========================
	See iPhone Cookbook p77 (toggleview example).
	
	mixing several animations
	=========================
	See iPhone Cookbook p79 (Swapping Views).
	
	Flipping views
	==============
	See iPhone Cookbook p81.
	UIViewAnimationTransitionFlipFromLeft 
	UIViewAnimationTransitionFlipFromRight
	
	CATransitions
	=============
	apply on layers.
	[myView layer]		default layer
	
	type: the kind of transition
	subtype: the direction
	
	#import <QuartzCore/QuartzCore.h>
	
	===================== iOS/application.txt
	app with different roles
	========================
	Some SpringBoard variables enable you to define multiple roles for a single application.For example, the Photos and Camera utilities are actually the same application, MobileSlideShow, playing separate roles.
	
	Files Not Found in the Application Bundle
	=========================================
	_ preferences files (generally stored in the application sandbox in Library/Preferences)
	_ application plug-ins (stored in /System/Library at this time and not available for general development)
	_ documents (stored in the sandbox in Documents).
	
	hidden app
	==========
	Using SpringBoard variables, you can specify whether the application is hidden from view.
	
	Fundamental methods used in an application
	===========================================
	_ main() (always use). Every C-based program centers around a main()function.For the iPhone,this function primes memory management and starts the application event loop. 
	
	_ applicationDidFinishLaunching: (always use). This method is the first thing triggered in your program.This is where you create a basic window,set its contents,and tell it to become the key responder for your application. 
	
	_ applicationWillTerminate: (usually use). This method enables you to handle any status finalization before handing control back to SpringBoard. Use this to save defaults,update data,and close files. 
	
	_ loadView (always use). Assuming you’ve built your application around a UIViewController—and there’s usually no reason not to do so—the mandatory loadViewmethod sets up the screen and lays out any subviews.Make sure to call [super loadView]whenever you inherit from a specialized subclass such as UITableViewController or UITabBarController.
	
	_ shouldAutorotateToInterfaceOrientation:  (usually use). Unless you have pressing reasons to force your user to remain in portrait orientation, add the should-autorotate method to allow the UIViewController method to automatically match your screen to the iPhone’s orientation. 
	
	getting the UIApplication instance:
	===================================
	[UIApplication sharedInstance]
	
	Use[[UIApplication sharedInstance] keyWindow]to locate your application’s main window object.
	
	Editing Identification Information 
	==================================
	The default Info.plist file enables you to create a working program but won’t,in its 
	default state,properly handle iPhone provisioning,a necessary precondition for on- 
	phone application testing and distribution,which is discussed later in this chapter.The 
	iPhone requires a signed identity,which you generate on Apple’s iPhone developer program 
	site (http://developer.apple.com/iphone/program).For Simulator use only,you can dou- 
	ble-click the Info.plist file to open it in a new window.Here,you’ll see the application 
	identifier for your program,and you can edit just the company name entry. 
	
	Customization does not end with the Info.plist file.Close the Info and double- 
	clickInfoPlist.strings.This file is listed among your other project files.Edit "© 
	__MyCompanyName__, 2008" to match the actual copyright you want to use for your 
	program.Again choose File,Save (Command-S) to save your changes.26 Chapter 1 Introducing the iPhone SDK 
	
	SCREEN SIZE
	===========
	CGRect screen_bounds = [[UIScreen mainScreen] bounds];
	
	STATUS BAR
	==========
	getting the status bar frame info:
	CGRect status_bar_frame = [[UIApplication sharedApplication] statusBarFrame];
	===================== iOS/applications.txt
	Stanza	(App Store, http://www.lexcycle.com/, gratuit) lecture de livre & pdf sur l'iPhone. Stanza Desktop application permits transfer of files from the Mac to the iPhone.
	===================== iOS/button.txt
	UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect]; 
	[button setFrame:CGRectMake(0.0f, 0.0f, 80.0f, 30.0f)]; 
	[button setCenter:CGPointMake(160.0f, 208.0f)]; 
	[button setTitle:@"Beep" forState:UIControlStateNormal]; 
	[button addTarget:self action:@selector(playSound:) forControlEvents:UIControlEventTouchUpInside]; 
	[contentView addSubview:button];
	 ===================== iOS/clipping.txt
	- (void) drawRect: (CGRect) aRect 
	{ 
	  CGRect bounds = CGRectMake(0.0f, 0.0f, SIDELENGTH, SIDELENGTH); 
	
	  // Create a new path 
	  CGContextRef context = UIGraphicsGetCurrentContext(); 
	  CGMutablePathRef path = CGPathCreateMutable(); 
	
	  // Add circle to path 
	  CGPathAddEllipseInRect(path, NULL, bounds); 
	  CGContextAddPath(context, path);
	
	  // Clip to the circle and draw the logo 
	  CGContextClip(context); 
	  [logo drawInRect:bounds]; 
	  CFRelease(path); 
	} 
	
	Balancing Touches with Clipping
	===============================
	- (BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event 
	{ 
	  CGPoint pt; 
	  float HALFSIDE = SIDELENGTH / 2.0f; 
	
	  // normalize with centered origin 
	  pt.x = (point.x - HALFSIDE) / HALFSIDE; 
	  pt.y = (point.y - HALFSIDE) / HALFSIDE; 
	
	  // x^2 + y^2 = hypoteneus length 
	  float xsquared = pt.x * pt.x; 
	  float ysquared = pt.y * pt.y; 
	
	  // If the length < 1, the point is within the clipped circle 
	  if ((xsquared + ysquared) < 1.0) return YES; 
	  return NO; 
	} 
	
	Testing Touch Hits Against a Bitmap 
	===================================
	// Return a bitmap context using alpha/red/green/blue byte values 
	CGContextRef CreateARGBBitmapContext (CGImageRef inImage) 
	{ 
	  CGContextRef    context = NULL; 
	  CGColorSpaceRef colorSpace; 
	  void *          bitmapData; 
	  int             bitmapByteCount; 
	  int             bitmapBytesPerRow; 
	  size_t pixelsWide = CGImageGetWidth(inImage); 
	  size_t pixelsHigh = CGImageGetHeight(inImage); 
	  bitmapBytesPerRow   = (pixelsWide * 4); 
	  bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh); 
	  colorSpace = CGColorSpaceCreateDeviceRGB(); 
	  if (colorSpace == NULL) 
	  { 
	    fprintf(stderr, "Error allocating color space\n"); 
	    return NULL; 
	  } 
	
	  // allocate the bitmap & create context 
	  bitmapData = malloc( bitmapByteCount ); 
	  if (bitmapData == NULL) 
	  { 
	    fprintf (stderr, "Memory not allocated!"); 
	    CGColorSpaceRelease( colorSpace ); 
	    return NULL; 
	  } 
	  context = CGBitmapContextCreate (bitmapData, pixelsWide, pixelsHigh, 8, bitmapBytesPerRow, colorSpace, kCGImageAlphaPremultipliedFirst); 
	
	  if (context == NULL) 
	  { 
	    free (bitmapData); 
	    fprintf (stderr, "Context not created!"); 
	  } 
	  CGColorSpaceRelease( colorSpace ); 
	
	  return context; 
	} 
	
	// Return Image Pixel data as an ARGB bitmap 
	unsigned char *RequestImagePixelData(UIImage *inImage) 
	{ 
	  CGImageRef img = [inImage CGImage]; 
	  CGSize size = [inImage size]; 
	  CGContextRef cgctx = CreateARGBBitmapContext(img, size); 
	
	  if (cgctx == NULL) return NULL; 
	  CGRect rect = {{0,0},{size.width, size.height}}; 
	  CGContextDrawImage(cgctx, rect, img); 
	  unsigned char *data = CGBitmapContextGetData (cgctx); 
	  CGContextRelease(cgctx); 
	
	  return data; 
	} 
	
	// Create an Image View that stores a copy of its image as an addressable bitmap 
	@interface BitMapView : UIImageView 
	{ 
	  unsigned char *bitmap; 
	  CGSize size; 
	  UIView*colorView; 
	} 
	@end 
	
	@implementation BitMapView 
	// Hit test relies on the alpha level of the touched pixel 
	- (BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event 
	{ 
	  long startByte = (int)((point.y * size.width) + point.x) * 4; 
	  int alpha = (unsigned char) bitmap[startByte]; 
	  return (alpha > 0.5); 
	} 
	
	-(void) setImage:(UIImage *) anImage 
	{ 
	  [super setImage:anImage]; 
	  bitmap = RequestImagePixelData(anImage); 
	  size = [anImage size]; 
	} 
	@end 
	===================== iOS/debugging.txt
	Uncovering Data Source and Delegate Methods 
	===========================================
	In addition to monitoring notifications,message tracking can prove to be an invaluable 
	tool.Add the following snippet to your class definitions to expose all the methods—both 
	documented and undocumented,data source and delegate—that your class responds to: 
	-(BOOL) respondsToSelector:(SEL)aSelector { 
	printf("SELECTOR: %s\n", [NSStringFromSelector(aSelector) UTF8String]); 
	return [super respondsToSelector:aSelector]; 
	} 
	===================== iOS/dragging.txt
	[dragger setUserInteractionEnabled:YES]
	
	see example in iPhone Cook Book p. 45.
	===================== iOS/file system.txt
	NSBundle class: access application's root folder
	
	/Applications: official Apple apps
	/var/mobile/Applications: third-party apps
	/var/mobile/Library/Preferences/<product_id>.plist: app user's preferences
	===================== iOS/fonts.txt
	From eatoni  Thu Apr  9 09:51:19 2009
	Return-Path: <nobody@eatoni.com>
	X-Original-To: pierrick
	Delivered-To: pierrick@eatoni.com
	Received: from eatoni.com [70.107.239.22]
		by iago.lan with POP3 (fetchmail-6.3.9)
		for <eatoni@localhost> (single-drop); Thu, 09 Apr 2009 09:51:19 +0200 (CEST)
	Received: by eatoni.com (Postfix, from userid 65534)
		id B376A7B9FE; Wed,  8 Apr 2009 15:32:54 -0400 (EDT)
	X-Original-To: ports@eatoni.com
	Delivered-To: ports@eatoni.com
	Received: from [192.168.1.16] (saturn.eatoni.com [192.168.1.16])
		by eatoni.com (Postfix) with ESMTP id A1F487B960
		for <ports@eatoni.com>; Wed,  8 Apr 2009 15:32:54 -0400 (EDT)
	Message-ID: <49DCFBE6.7070808@eatoni.com>
	Date: Wed, 08 Apr 2009 15:32:54 -0400
	From: "Howard A. Gutowitz" <hag@eatoni.com>
	Reply-To: ports@eatoni.com
	Organization: Eatoni Ergonomics, Inc.
	User-Agent: Thunderbird 2.0.0.19 (X11/20081227)
	MIME-Version: 1.0
	To: ports@eatoni.com
	Subject: Re: [Ports] a suggestion that it might be possible to embed a font
	 in an app
	References: <49DCFB93.8070000@eatoni.com>
	In-Reply-To: <49DCFB93.8070000@eatoni.com>
	Content-Type: text/plain; charset=UTF-8
	Content-Transfer-Encoding: 7bit
	Status: RO
	Content-Length: 3015
	Lines: 102
	
	Here's some code
	
	
	
	
	Howard A. Gutowitz wrote:
	> All this may change with v3.0, but if it doesn't:
	> 
	> 
	
	Just embed it as a resource, and then use the code
	below to load it for use. You can just reference it by
	name from then on (well, as long as your app is
	running) as you would any other font. (Where
	"kDefaultFontFile" is the name of your TTF file. Ex:
	@"MyFont.TTF")
	
	HTH!
	
	B
	
	
	       NSString *fontPath = [[[[NSBundle mainBundle]
	resourcePath] stringByAppendingPathComponent:@"Fonts"]
	stringByAppendingPathComponent: kDefaultFontFile];
	       NSData *fontData = [NSData dataWithContentsOfFile:
	fontPath];
	
	       ATSFontContainerRef container;
	       OSStatus err = ATSFontActivateFromMemory([fontData
	bytes], [fontData length],
	                         kATSFontContextLocal,
	                         kATSFontFormatUnspecified,
	                         NULL,
	                         kATSOptionFlagsDefault,
	                         &container );
	
	     if( err != noErr )
	         NSLog(@"failed to load font into memory");
	
	     ATSFontRef fontRefs[100];
	     ItemCount  fontCount;
	     err = ATSFontFindFromContainer(
	                         container,
	                         kATSOptionFlagsDefault,
	                         100,
	                         fontRefs,
	                         &fontCount );
	
	       if( err != noErr || fontCount < 1 ){
	         NSLog(@"font could not be loaded.");
	       }
	       else{
	
	           NSString *fontName;
	           err = ATSFontGetPostScriptName(
	                             fontRefs[0],
	                             kATSOptionFlagsDefault,
	                             (CFStringRef*)( &fontName ) );
	
	           NSLog(@"font %@ loaded", fontName);
	       }
	
	
	
	> 
	> mlRe: How do I embed a font in an app?
	> FROM : Jens Alfke
	> DATE : Sun Apr 06 18:43:26 2008
	> 
	> On 4 Apr '08, at 3:37 PM, Jens Alfke wrote:
	> 
	>> In my application's user interface I want to use some TrueType fonts
	>> which are not built into the OS. (Yes, they're free and their
	>> licenses allow this sort of use.) I thought I remembered a way to
	>> embed fonts in an application bundle, or to programmatically load a
	>> font from a file; but I'm drawing a blank now when I try to look it
	>> up.
	> 
	> 
	> FYI, Ben Stiglitz emailed me an even simpler solution that requires no
	> code at all:
	> 
	> Begin forwarded message:
	>> From: Benjamin Stiglitz <<email_removed>>
	>> Date: 4 April, 2008 11:02:06 PM PDT
	>> To: Jens Alfke <<email_removed>>
	>> Subject: Re: How do I embed a font in an app?
	>>
	> 
	>>> ATSFontActivateFromFileSpecification is deprecated in Leopard, but
	>>> the new replacement function, ATSFontActivateFromFileReference, is
	>>> identical except for taking an FSRef instead of an FSSpec. So it
	>>> makes the code even simpler.
	> 
	>> Even better is an Info.plist key for all the fonts you could want:
	>> ATSApplicationFontsPath.
	>>
	> http://developer.apple.com/documentation/MacOSX/Conceptual/BPRuntimeConfig/Articles/PListKeys.html#/
	>> /apple_ref/doc/uid/20001431-SW8
	>>
	> 
	
	===================== iOS/gestures.txt
	touchesBegan: withEvent:
	UITouchclass tells you where the event took place (locationInView:) and the 
	tap count (tapCount),
	===================== iOS/icon.txt
	filename
	========
	If you’d rather not use "icon.png"‘ set the CFBundleIconFile key in your Info.plist file to whatever filename you want to use.
	
	UIPrerenderedIcon
	=================
	SpringBoard smoothes and rounds those corners and adds an automatic gloss and shine effect. If for some com- pelling reason you need to use prerendered art, set UIPrerenderedIcon to <true/> in your Info.plist file.
	
	size
	====
	57x57
	
	If you plan to submit your application to App Store, you need to create a high-resolution (512-by-512 pixel) version of your icon.
	===================== iOS/images.txt
	Default.png: splash screen
	Icon.png: icon used by SpringBoard, can be changed with CFBundleIconFile. "Official" size is 57x57. SpringBoard process the image to fit it into the system look & feel, to disable this process UIPrerenderedIcon to <true/> in your Info.plist file. For App Store, you need a 512x512 icon.
	
	PNG images: Use PNG format, as it is better handled by iPhone. But it uses a non-standard format, so run "pngcrush -iphone" to convert png files into iPhone PNG format.
	===================== iOS/multitouch.txt
	The iPhone screen supports up to five touches at at time.
	
	set multipleTouchEnabled to YES or override isMultipleTouchEnabled for your view.
	
	Now when you call touchesForView:,the returned set may contain several touches.
	Use NSSet’s allObjects method to convert that set into an addressable NSArray.
	
	Visualizing Multitouch 
	======================
	@interface MultiTouchView : UIView 
	{ 
	  CGPoint loc1, loc2; 
	} 
	@property (nonatomic) CGPoint loc1; 
	@property (nonatomic) CGPoint loc2; 
	@end 
	
	@implementation MultiTouchView 
	@synthesize loc1; 
	@synthesize loc2; 
	
	- (BOOL) isMultipleTouchEnabled {return YES;} 
	
	- (void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event 
	{ 
	  NSArray *allTouches = [touches allObjects]; 
	  int count = [allTouches count]; 
	  if (count > 0) loc1 = [[allTouches objectAtIndex:0] locationInView:self]; 
	  if (count > 1) loc2 = [[allTouches objectAtIndex:1] locationInView:self]; 
	  [self setNeedsDisplay]; 
	} 
	
	// React to moved touches the same as to "began" 
	- (void) touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event 
	{ 
	  [self touchesBegan:touches withEvent:event]; 
	} 
	
	- (void) drawRect: (CGRect) aRect 
	{ 
	  // Get the current context 
	  CGContextRef context = UIGraphicsGetCurrentContext(); 
	  CGContextClearRect(context, aRect); 
	
	  // Set up the stroke and fill characteristics 
	  CGContextSetLineWidth(context, 3.0f); 
	  CGFloat gray[4] = {0.5f, 0.5f, 0.5f, 1.0f}; 
	  CGContextSetStrokeColor(context, gray); 
	  CGFloat red[4] = {0.75f, 0.25f, 0.25f, 1.0f}; 
	  CGContextSetFillColor(context, red); 
	
	  // Draw a line between the two location points 
	  CGContextMoveToPoint(context, loc1.x, loc1.y); 
	  CGContextAddLineToPoint(context, loc2.x, loc2.y); 
	  CGContextStrokePath(context); 
	  CGRect p1box = CGRectMake(loc1.x, loc1.y, 0.0f, 0.0f); 
	  CGRect p2box = CGRectMake(loc2.x, loc2.y, 0.0f, 0.0f); 
	  float offset = -8.0f; 
	
	  // circle point 1 
	  CGMutablePathRef path = CGPathCreateMutable(); 
	  CGPathAddEllipseInRect(path, NULL, CGRectInset(p1box, offset, offset)); 
	  CGContextAddPath(context, path); 
	  CGContextFillPath(context); 
	  CFRelease(path); 
	
	  // circle point 2 
	  path = CGPathCreateMutable(); 
	  CGPathAddEllipseInRect(path, NULL, CGRectInset(p2box, offset, offset)); 
	  CGContextAddPath(context, path); 
	  CGContextFillPath(context); 
	  CFRelease(path); 
	} 
	@end 
	===================== iOS/new blank project.txt
	how to start a project from scratch (without NIB or XIB files)
	==============================================================
	
	Taken from Hello World example:
	1. Remove classes. Select the Classes folder in the Groups & Files column on the 
	left and press Delete.Xcode asks you to confirm.Click Also Move to Trash. 
	2. Remove the .xib file. You won’t want to use it for this project. 
	3. Remove two lines from the Info.plist file. These are the lines with the 
	NSMainNibFilekey and the line that immediately follows after,the MainWindow 
	string. 
	4. Add an Images folder. Right-click Hello World in the Groups & Files column 
	and choose Add,New Group.Name the new folder Pictures. 
	5. Add images to the project. Drag the three images from the Chapter One proj- 
	ect folder provided with this book onto the Pictures folder:Icon.png,Default.png, 
	and helloworld.png.Check Copy Items into Destination Group’s Folder (if needed) 
	and click Add.
	6. Replace main.m. Open main.m and replace all the text in that file with the text 
	from the main.m file provided in the Chapter One project folder.Save your 
	changes with File,Save (Command-S) and close the main.m window. 
	7. Select the iPhone Simulator. Choose Project,Set Active SDK,iPhone 
	Simulator. 
	8. Run it. Choose Build,Build & Run (Command-R).The program will compile 
	and launch in the iPhone simulation window.
	===================== iOS/orientation.txt
	Reorienting
	===========
	- (void)willRotateToInterfaceOrientation: 
	  (UIInterfaceOrientation)orientation 
	  duration:(NSTimeInterval)duration  
	{ 
	  CGRect apprect; 
	  apprect.origin = CGPointMake(0.0f, 0.0f); 
	
	  // adjust the frame size based on actual orientation 
	  if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)) 
	    apprect.size = CGSizeMake(480.0f, 300.0f); 
	  else 
	    apprect.size = CGSizeMake(320.0f, 460.0f); 
	
	  // resize each subview accordingly 
	  float offset = 32.0f; 
	  for (UIView*subview in [contentView subviews])    { 
	    CGRect frame = CGRectInset(apprect, offset, offset); 
	    [subview setFrame:frame]; 
	    offset += 32.0f; 
	  } 
	}
	
	// Allow the view to respond to iPhone Orientation changes 
	-(BOOL)shouldAutorotateToInterfaceOrientation: 
	(UIInterfaceOrientation)interfaceOrientation 
	{ 
	  return YES; 
	} 
	
	Centering landscape views
	=========================
	Use affine transform, see iPhone Cookbook p91.
	===================== iOS/persistence.txt
	see iPhone Cook Book p65
	
	Persistence represents a key iPhone design touch point.After users leave a program, 
	Apple strongly recommends that they return to a state that matches as closely to where 
	they left off as possible.
	
	persistence involves:
	1. Storing the data 
	2. Resuming from a saved session 
	3. Providing a startup image that matches the last session 
	
	startup image
	=============
	declare the _writeApplicationSnapshot undocumented method:
	
	@interface UIApplication (Extended) 
	-(void) _writeApplicationSnapshot; 
	@end 
	
	[[UIApplication sharedApplication] _writeApplicationSnapshot];
	
	then use it to save a screen shot before leaving app.
	===================== iOS/screen.txt
	main screen
	===========
	320x480
	in both portrait and landscape, there's a status top bar of 20 pixels height.
	get status bar frame: [[UIApplication sharedApplication] statusBarFrame]
	hide status bar: [[UIApplication sharedApplication] setStatusBarHidden:YES animated:NO]
	
	setting app in landscape-only mode, by setting status bar:
	[[UIApplication sharedApplication] setStatusBarOrientation: UIInterfaceOrientationLandscapeRight]
	
	screen object:
	[UIScreen mainScreen]
	
	By default,UINavigationBar,UIToolbar,and UITabBarobjects are 44 pixels in height each.
	===================== iOS/sending email.txt
	example
	=======
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!";
	    NSString *body = @"&body=It is raining in sunny California!";
	    
	    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	    
	    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
	===================== iOS/simulator.txt
	macro
	=====
	TARGET_IPHONE_SIMULATOR
	
	Using the simulator
	===================
	_ Reorienting the screen. With the Hello World application running,you can 
	reorient the Simulator using Command-Left arrow and Command-Right arrow. 
	
	_ Going home. Tap the Home button (Command-Shift-H) to leave the application 
	and return to the SpringBoard home screen. 
	
	_ Locking the “phone.”Hardware,Lock (Command-L) simulates locking the 
	phone. 
	
	_ Viewing the console. In Xcode,choose Run,Console (Command-Shift-R) to 
	view the Xcode console window.This window is where your printf, NSLog,and 
	CFShowmessages are sent by default.In the unlikely case where NSLogmessages 
	do not appear,use the Xcode organizer (Window,Organizer,Console) or open 
	/Applications/Utilities/Consoleto view NSLogmessages instead. 
	
	_ Running Instruments. Use Run,Start with Performance Tool to select an 
	Instruments template (such as Object Allocations or CPU Sampler) to use with the 
	Simulator.Instruments monitors memory and CPU use as your application runs. 
	
	Note
	====
	Although the Organizer window offers direct access to crash logs,you’ll probably 
	want to enable Developer Mode for Crash Reporter,too. This enables you to see trace 
	backs immediately,without having to click Report all the time. To do this,open 
	/Developer/Applications/Utilities/CrashReporterPrefs,select Developer,and quit. (Tip is 
	courtesy of Emanuele Vulcano of infinite-labs.net.) 
	===================== iOS/splash screen.txt
	Default.png (officially known as your "launch image") provides the splash screen dis- played during application launch.
	===================== iOS/stanford.txt
	iPhone Application Programming:
	http://www.stanford.edu/class/cs193p/cgi-bin/index.php
	===================== iOS/swiping.txt
	When a user quickly drags his or her finger across the screen,the UITouchobjects returned for that gesture include an infoproperty.This property defines the direction in which the user swiped the screen,(for example,up,down,left,or right)
	
	iPhone Cookbook p87
	===================== iOS/threading.txt
	autorelease pool
	================
	in a thread, we always must instantiate an autorelease pool:
	NSAutoreleasePool	 *autoreleasepool = [[NSAutoreleasePool alloc] init];
	...
	[autoreleasepool release];
	
	running a thread
	================
	[NSThread detachNewThreadSelector:@selector(threadMethod:) toTarget:self withObject:arg];
	
	lock
	====
	NSLock *my_lock = [[NSLock alloc] init];
	[my_lock lock];
	[my_lock unlock];
	[my_lock release];
	===================== iOS/touch.txt
	UITouch
	
	A touch tells you several things:where the touch took place (both the current and 
	most recent previous location),what stage of the touch was used (essentially mouse 
	down,mouse moved,mouse up),a tap count (for example,single-tap/double-tap),when 
	the touch took place (through a time stamp),and so forth. 
	===================== iOS/transforming.txt
	Affine transformation of views.
	
	See iPhone Cookbook p 89.
	===================== iOS/url_schemes.txt
	It's possible to launch an application from Safari through a specific URL like "myappp:".
	
	To do that, inside app project, add the "URL Types" key in info.plist file. And Specify the scheme of your choice ("myapp" in the above example) inside "URL Schemes" array.
	
	you can pass a message to the app with :
	myapp:my_message
	
	then in the application delegate, define the following method which will be called when opening application:
	- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
	===================== iOS/view controllers.txt
	Core Classes
	============
	_ UIViewController: parent for all view controllers, used to link to a single view. It handles all view reorientation tasks.
	
	_ UINavigationController: navigation bar
	
	_ UITabBarController: tab bar
	
	Specialized Classes
	===================
	_ UIImagePickerController: take image from album or camera
	
	_ ABNewPersonViewController, ABPersonViewController,ABUnknownPersonViewontroller,and 
	ABPeoplePickerNavigationController: access address book.
	
	_ UITableViewController
	
	_ MPMoviePlayerController: play movie or audio
	
	loadView
	========
	UIViewController does not create a view of its own, you must supply the view by Creating it into loadView method.
	1) Initialize and size your content view by calling initWithFrame:using [[UIScreen mainScreen] applicationFrame]as its argument.
	2) Connect the view to the controller by assigning self.view to a UIViewinstance of some kind.Do this in your loadView method (for example,self.view = contentView).
	
	for specialized controllers call [super loadView].
	
	init
	====
	use it it set view controller's title:
	assign self.title to [[[NSBundle mainBundle] infoDictionary] objectForKey:@“CFBundleName”]
	
	shouldAutorotateToInterfaceOrientation
	======================================
	This method returns either YES or NO.
	
	Setting Up Autorotation Flags:
	contentView.autoresizesSubviews = YES; 
	contentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	
	viewDidAppear: and viewDidDisappear:
	====================================
	Thesemethods are called each time a UIViewController’s view is brought onscreen or 
	pushed offscreen.
	
	didRotateFromInterfaceOrientation:
	==================================
	catch orientation changes.
	
	To force your application into landscape mode
	=============================================
	return NO for shouldAutorotateToInterfaceOrientation:,call 
	[[UIApplication sharedApplication] setStatusBarOrientation: 
	UIInterfaceOrientationLandscapeRight]from your 
	applicationDidFinishLaunching: method,and set UIInterfaceOrientation 
	toUIInterfaceOrientationLandscapeRight in your Info.plist file. 
	===================== iOS/views.txt
	root class
	==========
	UIView
	
	to add a subview
	================
	[parentView addSubview:child]
	
	to get subviews
	===============
	[parentView subviews]
	they are ordered from back to front.
	
	remove a subview
	================
	[childView removeFromSuperview]
	
	reorder subviews
	================
	[parentView exchangeSubviewAtIndex:i withSubviewAtIndex:j]
	bringSubviewToFront:
	orsendSubviewToBack:
	
	tagging a subview
	=================
	Tag your subviews using setTag:.This identifies views by tagging them with a number.Retrieve that view from the child hierarchy by calling viewWithTag:on the parent.
	
	CGRect
	======
	CGRectMake()
	NSStringFromCGRect() & CGRectFromString()
	CGRectInset()	     create a smaller or larger rectangle centered on the same point
	CGRectIntersectsRect()
	CGRectZero()
	
	CGPoint
	=======
	CGPointMake()
	NSStringFromCGPoint()
	CGPointFromString()
	
	CGSize
	======
	CGSizeMake()
	NSStringFromCGSize()
	CGSizeFromString()
	
	set frame
	=========
	[myView setFrame:CGRectMake(0.0f, 50.0f, mywidth, myheight)];
	
	transforms
	==========
	drawRect:	fct for drawing view
	
	Note: when calling Core Graphics functions,keep in mind that Quartz lays out its coordinate 
	system from the bottom left,whereas UIViews have their origin at the top left. 
	
	transparency
	============
	[myView setAlpha:value]
	
	background
	==========
	[myView setBackgroundColor:[UIColor redColor]]
	
	UIViewController’s loadView
	===========================
	- (void)loadView 
	{ 
	  // Create the main view 
	  CGRect appRect = [[UIScreen mainScreen] applicationFrame];
	  contentView = [[UIViewalloc] initWithFrame:appRect]; 
	  contentView.backgroundColor = [UIColor whiteColor]; 
	
	  // Provide support for autorotation and resizing 
	  contentView.autoresizesSubviews = YES; 
	  contentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight); 
	  self.view = contentView; 
	  [contentView release]; 
	
	  // reset the origin point for subviews. The new origin is 0,0 
	  appRect.origin = CGPointMake(0.0f, 0.0f); 
	
	  // Add the subviews, each stepped by 32 pixels on each side 
	  UIView*subview = [[UIViewalloc] initWithFrame:CGRectInset(appRect, 32.0f, 32.0f)]; 
	  subview.backgroundColor = [UIColor lightGrayColor]; 
	  [contentView addSubview:subview]; 
	  [subview release]; 
	
	  subview = [[UIViewalloc] initWithFrame:CGRectInset(appRect, 64.0f, 64.0f)]; 
	  subview.backgroundColor = [UIColor darkGrayColor]; 
	  [contentView addSubview:subview]; 
	  [subview release]; 
	
	  subview = [[UIViewalloc] initWithFrame:CGRectInset(appRect, 96.0f, 96.0f)]; 
	  subview.backgroundColor = [UIColor blackColor]; 
	  [contentView addSubview:subview]; 
	  [subview release]; 
	}
