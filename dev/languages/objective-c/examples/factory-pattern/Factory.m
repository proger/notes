#import <stdio.h>
#import <Foundation/Foundation.h>

////////////////////////////////////////////////////////////////////////////////
// OBJECT

@interface AbstractObject : NSObject {
}
- (void) do_something;
@end

@implementation AbstractObject {
}
- (void) do_something {
	printf("I'm Object mother class !\n");
}
@end

@interface Object1 : AbstractObject {
}
- (void) do_something;
@end

@implementation Object1 {
}
- (void) do_something {
	printf("I'm Object1 class !\n");
}
@end

@interface Object2 : AbstractObject {
}
- (void) do_something;
@end

@implementation Object2 {
}
- (void) do_something {
	printf("I'm Object2 class !\n");
}
@end

////////////////////////////////////////////////////////////////////////////////
// FACTORY

@interface Factory : NSObject {
}
- (AbstractObject*) createObject:(int)param;
@end

@implementation Factory {
}
- (AbstractObject*) createObject:(int)param {
	AbstractObject* object = param % 2 ? [Object1 alloc] : [Object2 alloc] ;
	return object;
}
@end

////////////////////////////////////////////////////////////////////////////////
// MAIN
int main(void) {
	Factory* f = [Factory alloc];
	AbstractObject* o1 = [f createObject:1];
	AbstractObject* o2 = [f createObject:2];
	[o1 do_something];
	[o2 do_something];
	return 0;
}
