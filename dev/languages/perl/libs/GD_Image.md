# GD::Image

To create an image:
```perl
my $im = new GD::Image($width, $height);
```

To load an image:
```perl
my $im = new GD::Image($filepath);
my $im = new GD::Image($file_descriptor);
```

To write text on the image:
```perl
$im->stringFT($color, $ttf_file, $point_size, $angle, $x, $y, $text);
```
angle is expressed in radians.

To write in a file:
```perl
$im->png($compression_level);
```

Allocating a color:
```perl
$im->colorAllocate($red, $green, $blue);
```

Draw a filled polygon:
```perl
$poly = new GD::Polygon;
$poly->addPt($x1 + 200, $y1 + 200);
$poly->addPt($x1 + 250, $y1 + 230);
$poly->addPt($x1 + 300, $y1 + 310);
$poly->addPt($x1 + 400, $y1 + 300);
$im->filledPolygon($poly, $color);
```

Draw a dashed line:
```perl
$im->dashedLine($x1, $y1, $x2, $y2, $color);
```
