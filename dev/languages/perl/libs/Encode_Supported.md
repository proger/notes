# Encode::Supported

 * [CPAN Encode::Supported](http://search.cpan.org/~jhi/perl-5.8.1/ext/Encode/lib/Encode/Supported.pod).

Encodings supported by Encode.
