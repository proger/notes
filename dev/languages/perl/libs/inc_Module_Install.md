# inc::Module::Install

 * [CPAN inc::Module::Install](http://search.cpan.org/~ether/Module-Install-1.17/lib/Module/Install.pod).

Makefile.PL:
```perl
use inc::Module::Install;
name           'DocMaking';
all_from       'lib/Your/Module.pm';
version         '1.0';
requires       'File::Spec'  => '0.80';
test_requires  'Test::More'  => '0.42';
recommends     'Text::CSV_XS'=> '0.50';
no_index       'directory'   => 'demos';
install_script 'slides-maker';
install_script 'get-code-type';
install_script 'split-code';
install_share;      # install everything that is in share directory of the distribution.
WriteAll;
```

On command line:
```bash
perl Makefile.PL PREFIX=$HOME/perl # Use PREFIX and not INSTALL_BASE, so share directory is installed under lib/perl5/site_perl and will be found by File::ShareDir.
make
make test
make install
```

Getting share directory from a program:
```perl
my $share = File::ShareDir::dist_dir('My::Module');
```
