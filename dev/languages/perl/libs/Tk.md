# Tk

```perl
use Tk;
```

Create a main window:
```perl
my $mw = MainWindow->new;
```

Scrollbars:
```perl
my $wmain = $g_main_window->Scrolled('Pane', -height => 1, -width => 1, -scrollbars => 'e');
```

hide/show a window:
```perl
$mw->withdraw(); # hide
$mw->deiconify(); # show
```

Run the GUI:
```perl
MainLoop;
```

Create a button:
```perl
my $button = $mw->Button(-text => "label", -command => sub{foo()});
my $button = $mw->Button(-text => "label", -command => &foo});
```

Load a color image:
```perl
my $img = $mw->Photo(-file => "/my/file/path");
```

Set image on button:
```perl
$button->configure(-image => $img);
```

Grid layout:
```perl
$button->grid(-row => 2, -column => 1);
```

Pack layout:
```perl
$button->pack;
$button->pack(-side => 'bottom', -expand => 1, -fill => 'x');
$button->pack(-side => 'left', -expand => 1, -fill => 'both');
```

`-side` => 'left' | 'right' | 'top' | 'bottom' Puts the widget against the specified side of the window or frame
`-fill` => 'none' | 'x' | 'y'| 'both' Causes the widget to fill the allocation rectangle in the specified direction
`-expand` => 1 | 0 Causes the allocation rectangle to fill the remaining space available in the window or frame
`-anchor` => 'n' | 'ne' | 'e' | 'se' | 's' | 'sw' | 'w' | 'nw' | 'center' Anchors the widget inside the allocation rectangle
`-after` => $otherwidget Puts $widget after $otherwidget in packing order
`-before` => $otherwidget Puts $widget before $otherwidget in packing order
`-in` => $otherwindow Packs $widget inside of $otherwindow rather than the parent of $widget, which is the default
`-ipadx` => amount Increases the size of the widget horizontally by amount ✕ 2
`-ipady` => amount Increases the size of the widget vertically by amount ✕ 2
`-padx` => amount Places padding on the left and right of the widget
`-pady` => amount Places padding on the top and bottom of the widget

Label:
```perl
my $label = $mw->Label(-text => 'label text');
```

Frame:
```perl
my $frame = $mw->Frame();
```

Key presses:
```perl
$mw->bind('Q', sub{exit});
```
