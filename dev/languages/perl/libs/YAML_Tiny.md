# YAML::Tiny

 * [YAML::Tiny](https://metacpan.org/pod/YAML::Tiny).

Does not support boolean type (from 'boolean' package). Need to use plains `1` and `0`.

Load a YAML file:
```perl
use YAML::Tiny;

my $x = YAML::Tiny->read('myfile.yml');
```

Save data into a YAML file:
```perl
my $ymltowrite = YAML::Tiny->new(\@myarr);
$ymltowrite->write("myfile.yml");
```
