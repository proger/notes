# Array::Utils
<!-- vimvars: b:markdown_embedded_syntax={'perl':''} -->

Intersection:
```perl
use Array::Utils;
my @isect = Array::Utils::intersect(@a, @b);
```
