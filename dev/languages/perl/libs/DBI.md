# DBI

Database connection.

```perl
use DBI;
```

Connection:
```perl
my $dbh = DBI->connect('DBI:mysql:mydatabase');
my $dbh = DBI->connect('DBI:mysql:mydatabase', $user, $password, { RaiseError => 1, AutoCommit => 0 });
```

Setting parameters after connection is created:
```perl
$dbh->{AutoCommit} = 1;
```

Espace and quote query string before using it:
```perl
my $quoted_string = $dbh->quote($unquoted_string);
```

Run query:
```perl
my $sth = $dbh->prepare("INSERT INTO mytable VALUES (1, 2)");
$sth->execute();
```

When `AutoCommit` is on, every query is commited. To open a transaction run:
```perl
$dbh->begin_work();
# or
$dbh->do('BEGIN TRANSACTION');
```
Then `AutoCommit` is turned off automatically.
When you are done, commit the transaction with:
```perl
$dbh->commit();
# or
$dbh->do('COMMIT');
```
Or cancel it with:
```perl
$dbh->rollback();
```
Then `AutoCommit` is turned on again.

When `AutoCommit` is off, we are always in a transaction mode. Thus opening explictly a transaction is not needed but does not harm:
However closing a transaction is needed.
