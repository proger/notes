# LWP::Simple

A simplified facade to the `libwww-perl` library.

```perl
use LWP::Simple;
```

Getting a web page content:
```perl
$content = get("http://www.sn.no/");
```
