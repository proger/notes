# SQLite

Install:
```sh
cpan -i DBD::SQLite
```

Open an SQLite connection:
```perl
use DBI;
my $dbh = DBI->connect("dbi:SQLite:dbname=/my/db/file.sqlite", "", "");
```
