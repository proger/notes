# Class::Date

```perl
use Class::Date;
```

Create a Date object from a string "YYYY-MM-DD":
```perl
my $date = new Class::Date("2013-01-03");
```

With time:
```perl
my $date = new Class::Date("2013-01-03 15:30:28");
```

Get UNIX epoch time:
```perl
my $time = $date->epoch;
```

Get current date:
```perl
my $today = Class::Date->now;
```

Addition:
```perl
$date= date('2001-12-11')+'3Y';
$date= Class::Date->new('2001-12-11')+Class::Date::Rel->new('3Y');
```

To change print date format:
```perl
$Class::Date::DATE_FORMAT="%Y-%m-%d";
```
