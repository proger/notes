# XML::LibXML

 * [XML::LibXML](https://metacpan.org/pod/XML::LibXML).

Example:
```perl
use XML::LibXML;
open my $fh, '<', $myfile;
binmode $fh; # drop all PerlIO layers possibly created by a use open pragma
$doc = XML::LibXML->load_xml(IO => $fh);
$root = $doc->documentElement();
my @nodes = $root->getChildrenByTagName("mytag");
```
