# Carp

Module for displaying more useful messages than die.
	
```perl
use Carp;
```

Send a warning:
```perl
carp "my message";
```

Quit:
```perl
coark "my message";
```

Quit and print the call stack:
```perl
confess "my message";
```

Warn user (from perspective of caller):
```perl
carp "string trimmed to 80 chars";
```

Die of errors (from perspective of caller):
```perl
croak "We're outta here!";
```

Die of errors with stack backtrace:
```perl
confess "not implemented";
```

`cluck` not exported by default:
```perl
use Carp qw(cluck);
cluck "This is how we got here!";
```
