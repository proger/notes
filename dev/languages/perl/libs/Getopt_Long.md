# Getopt::Long

 * [Getopt::Long](https://metacpan.org/pod/Getopt::Long).

```perl
use Getopt::Long;
```

Disable `ignore_case` option, enable bundling of short options (e.g.: `-abc`), and disable generation of abbrevations:
```perl
use Getopt::Long qw(:config no_ignore_case bundling no_auto_abbrev);
```

Read a string value:
```perl
my $s = "default value";
Getopt::Long::GetOptions("myopt1=s" => \$s);
```

Read values and put them inside a hash:
```perl
my %args = ( myopt1 => "default value 1", myopt2 => "default value 2" );
Getopt::Long::GetOptions("myopt1=s" => \$args{myopt1}, "myopt2=s" => \$args{myopt2});
```

## Generate help page

## Help page with POD documentation

Using POD:
```perl
use Getopt::Long;
use Pod::Usage;
our $HELP;
our $MAN;

Getopt::Long::GetOptions(
    'help|h' => \$HELP,
    'man' => \$MAN
) or Pod::Usage::pod2usage(2);

__END__

=head1 NAME

my_program - Short description.

=head1 SYNOPSIS

my_program [options]

=head1 OPTIONS

=over 8

=item B<-h, --help>

Prints a brief help message and exits.

=item B<--man>

Prints the manual page and exits.

=back

=head1 DESCRIPTION

B<This program> blablabla.

=cut
```
