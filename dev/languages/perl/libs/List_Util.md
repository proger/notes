# List::Util

Reduce:
```perl
use List::Util qw(reduce);
my $sum = List::Util::reduce { $a + $b } 0, @mynumbers;
```

Remove duplicates:
```perl
use List::MoreUtils qw(uniq);
my @unique = uniq( 1, 2, 3, 4, 4, 5, 6, 5, 7 ); # 1,2,3,4,5,6,7
my $unique = uniq( 1, 2, 3, 4, 4, 5, 6, 5, 7 ); # 7
```

Find maximum and minimum:
```perl
use List::Util qw(max);
my $max = max(@myarray);
my $max = min(@myarray);
```
