# HTML::TreeBuilder::XPath

Add XPath support to `HTML::TreeBuilder`.

```perl
use HTML::TreeBuilder::XPath;
```

```perl
my $tree= HTML::TreeBuilder::XPath->new;
$tree->parse_file( "mypage.html");
```
