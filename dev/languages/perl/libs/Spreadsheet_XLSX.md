# Spreadsheet::XLSX

 * [Spreadsheet::XLSX](https://html.duckduckgo.com/html/?q=perl%20xlsx).

Had some issue (2021.06.15) with UTF-8 inside XSLX file that was not recognized.
Better to use Spreadsheet::ParseXLSX.
