# Image::Magick

 * [PerlMagick Image API for Perl](https://imagemagick.org/script/perl-magick.php).

```perl
use Image::Magick;
```

Create an image object:
```perl
my $image = new Image::Magick;
```

Load an image:
```perl
$image->Read("myimg.png");
```

Save an image:
```perl
$image->Write("myimg.png");
```

Get width and height:
```perl
my $height = $image->Get('height');
my $width = $image->Get('width');
```
