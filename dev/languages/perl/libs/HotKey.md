# HotKey

Input keys

```perl
use HotKey;
use feature "switch";
$key = readkey();
given($key) {
	when('q')    { do_something(); }
}
```
