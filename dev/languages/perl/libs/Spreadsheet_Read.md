# Spreadsheet::Read

 * [Spreadsheet::Read](https://metacpan.org/pod/Spreadsheet::Read).

Depends on `Spreadsheet::ParseExcel` for xls file and `Spreadsheet::ParseXLSX` for xlsx files.

--> Installation OK.
