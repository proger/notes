# File::Spec::Functions

Concatenate a path with a filename:
```perl
my $filepath = File::Spec::Functions::catfile($mypath, $myfilename);
```
