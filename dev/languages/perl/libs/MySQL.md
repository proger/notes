# MySQL

```perl
use Mysql;
```

Connecting:
```perl
my $db = Mysql->connect($host, $database, $user, $password);
```

Selecting database:
```perl
$db->selectdb($database);
```

List databases:
```perl
my @array = $db->listdbs;
```

List tables:
```perl
my @array = $db->listtables;
```

Fields info:
```perl
my $fields = $db->listfields($table);
$fields->name;
$fields->type;
$fields->length;
$fields->isnotnull;
$fields->isprikey;
$fields->isnum;
$fields->isblob;
```

Query:
```perl
my $query = $db->query($sql_query);
mysql_fetch_row($query_id); # get one row, call again to get the next row
my %hash = $query->fetchhash; # get one row in a hash (column name as key)
$query->dataseek($row_number); # first row has index 0
```

Query Statement Statistics:
```perl
my $number = $query->numrows;
my $number = $query->numfields;
my $number = $query->affectedrows;
my $id = $query->insertid; # gives back the automatically inserted id in the new row, if there's one.
```

Espace and quote query string before using it:
```perl
my $quoted_string = $db->quote($unquoted_string);
```

Error:
```perl
$error_message = $db->errmsg();
```

Create/remove a database:
```perl
my $newdb = $dbh->createdb("database name");
my $nodb = $db->dropdb("database name");
```
