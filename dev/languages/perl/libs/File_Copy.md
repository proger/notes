# File::Copy

Copy a file:
```perl
File::Copy::copy($mysrcfile, $mydstfile) || die "Unable to copy file.";
```
