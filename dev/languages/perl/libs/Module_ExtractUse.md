# Module::ExtractUse

```perl
use Module::ExtractUse;
```

Extract the dependencies of a script:
```perl
my $p=Module::ExtractUse->new;
$p->extract_use('/my/perl/script.pl');
```

Get dependencies from command line:
```sh
perl -e 'use Module::ExtractUse; my $p=Module::ExtractUse->new; print($p->extract_use("play-radio")->string);'
```
