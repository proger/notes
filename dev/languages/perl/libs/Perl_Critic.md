# Perl::Critic

A static code analysis tool.

Install:
```sh
cpanm -i Perl::Critic
```

Static code checking:
```sh
perlcritic MyModule.pm
```
