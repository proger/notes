# YAML::XS
 
 * [YAML](https://metacpan.org/pod/YAML::XS).

Attention! Wraps long strings!

Load a YAML file:
```perl
use YAML::XS;
my $data = YAML::XS::LoadFile('myfile.yml');
```

Create a YAML file:
```perl
YAML::XS::DumpFile('output.yml', $data);
```
