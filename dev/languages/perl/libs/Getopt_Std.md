# Getopt::Std

```perl
use Getopt::Std;

getopt('oDI');    # -o, -D & -I take arg.  Sets $opt_* as a side effect.
getopt('oDI', \%opts);    # -o, -D & -I take arg.  Values in %opts
getopts('oif:');  # -o & -i are boolean flags, -f takes an argument
                  # Sets $opt_* as a side effect.
getopts('oif:', \%opts);  # options as above. Values in %opts
```

Arguments found by getopt are removed from @ARGV.
