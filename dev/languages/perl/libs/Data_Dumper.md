# Data::Dumper

To dump a structure:
```perl
use Data::Dumper;
print(Data::Dumper->Dump([\@my_array, \%my_hash], ["array_name", "hash_name"]));
```
the second parameter (variable names) is optional.

To avoid cross-references:
```perl
$Data::Dumper::Deepcopy = 1;
```
	
Encoding issue: be careful to output data structures in ASCII, or do() function could make mistake while reading, and assume another encoding than UTF-8 for strings.
```perl
use Data::Dumper;
binmode(STDOUT, ":utf8");
print Data::Dumper->Dump([\%my_hash]);
```
