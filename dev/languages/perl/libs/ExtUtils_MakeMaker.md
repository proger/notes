# ExtUtils::MakeMaker

 * [CPAN ExtUtils::MakeMaker](http://search.cpan.org/~bingos/ExtUtils-MakeMaker-7.24/lib/ExtUtils/MakeMaker.pm).

```perl
use ExtUtils::MakeMaker;
WriteMakefile( ATTRIBUTE => VALUE [, ...] );
```
	
Put module files under lib directory. Everything will be taken.
	
Scripts:
```perl
WriteMakefile( EXE_FILES => ['scriptA', 'scriptB']);
```
	
On command line:
```bash
perl Makefile.PL
make
make test
make install
```
	
Installing in a specific directory:
```bash
perl Makefile.PL PREFIX=/path/to/your/home/dir      # will install under lib/perl5/site_perl
perl Makefile.PL INSTALL_BASE=/path/to/your/home/dir # will install under lib/perl5
```
	
For testing, add a directory named `t` and put `*.t` files inside. They will be run inside the Test::Harness framework.
For printing output of tests:
```bash
make test TEST_VERBOSE=1
```
