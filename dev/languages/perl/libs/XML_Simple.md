# XML::Simple

DRAWBACKS
 * DEPRECATED
 * Doesn't handle correctly `<?...?>` head tag. Change it in `<opt ...>...</opt>`.
 * On XMLout use option `XMLDecl => 1`.

By default it wants to put a 'name' attribute in tags, so you can't have an empty tag (with no attribute). If you read and write an XML without changing something, then the ouput will be different than the input. Typically something like `<tag1><tag2><tag3>...` becomes `<tag1 name="tag2"><tag3...`

On XMLin and XMLout use option `KeepRoot => 1` to keep root element.

See [XML::Simple](https://metacpan.org/pod/XML::Simple).

```perl
use XML::Simple qw(:strict);
```
