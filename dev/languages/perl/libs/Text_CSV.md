# Text::CSV

 * [Text::CSV](https://metacpan.org/pod/Text::CSV).

Load a TSV file as an array of hashes:
```perl
use Text::CSV;
my $photos = Text::CSV::csv(in => "myfile.tsv", sep_char => "\t", headers => "auto");
```
