#!/usr/bin/env perl

use Data::Dumper;

my %h = ();
print("Array: ".Data::Dumper->Dump([\%h], ['h']));
print("h[0] is defined: ".(defined $h{0} ? "yes" : "no")."\n");
print('h["0"] is defined: '.(defined $h{"0"} ? "yes" : "no")."\n");

print("\n");
my %h = (0 => 'foo');
print("Array: ".Data::Dumper->Dump([\%h], ['h']));
print("h[0] is defined: ".(defined $h{0} ? "yes" : "no")."\n");
print('h["0"] is defined: '.(defined $h{"0"} ? "yes" : "no")."\n");

print("\n");
my %h = ("0" => 'foo');
print("Array: ".Data::Dumper->Dump([\%h], ['h']));
print("h[0] is defined: ".(defined $h{0} ? "yes" : "no")."\n");
print('h["0"] is defined: '.(defined $h{"0"} ? "yes" : "no")."\n");
