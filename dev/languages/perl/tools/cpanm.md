# cpanm

See also `cpan`.

 * [Comprehensive Perl Archive Network](https://cpan.metacpan.org/).

Install Perl module easily.

`cpanm` seems better (more up-to-date about dependencies) than `cpan`. To get
it, install package `cpanminus`, under Debian, macos or ArchLinux.
Or downloading from site:
```bash
curl -L http://cpanmin.us | perl - App::cpanminus
```

To update installed modules easily, use `cpan-outdated` with `cpanm`:
First install `cpan-outdated`:
```bash
cpanm App::cpanoutdated
```
then run:
```bash
cpan-outdated -p | cpanm
```
See [How do I update all my CPAN modules to their latest versions?](https://stackoverflow.com/questions/3727795/how-do-i-update-all-my-cpan-modules-to-their-latest-versions).


Install on Debian:
```sh
apt install cpanminus
```
