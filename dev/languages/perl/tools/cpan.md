# cpan

See also `cpanm`.

 * [Comprehensive Perl Archive Network](https://cpan.metacpan.org/).

CPAN Perl modules installer.

Install a module:
```sh
cpan -i XML::Feed
```

Install a module from sources:
```sh
wget https://cpan.metacpan.org/.../.../foo-0.10.tar.gz
tar xzf foo-0.10.tar.gz
cd foo-0.10
perl -I . Makefile.PL
make test
make install
```
