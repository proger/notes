# Assertions
<!-- vimvars: b:markdown_embedded_syntax={'js':'javascript','html':''} -->

Import:
```js
import * as assert from 'assert/strict';
```

Test equality:
```js
assert.equal(n, 3);
assert.notEqual(n, 4);
```

Custom message:
```js
assert.equal(n, 3, 'My custom message');
```

Test equality of content:
```js
assert.deepEqual(arr, [1, 2, 3]);
assert.notDeepEqual(arr, [1, 2, 3]);
```

Fail on purpose:
```js
assert.fail();
```

## Exceptions

Assert that an exception is thrown:
```js
assert.throws(
    () => {
        null.prop;
    }
);
```

Assert that a particular exception is thrown (using type):
```js
assert.throws(
    () => {
        null.prop;
    },
    TypeError
);
```

Check exception message using an regex:
```js
assert.throws(
    () => {
        null.prop;
    },
    /^TypeError: Cannot read properties of null \(reading 'prop'\)$/
);
```

Check exception using an object description:
```js
assert.throws(
    () => {
        null.prop;
    },
    {
        name: 'TypeError',
        message: "Cannot read properties of null (reading 'prop')",
    }
);
```
