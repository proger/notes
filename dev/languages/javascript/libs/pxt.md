# pxt

Microsoft Programming Experience Toolkit (PXT).

 * [Blinking The Micro:bit Using JavaScript](https://www.hackster.io/pxt/blinking-the-micro-bit-using-javascript-2df45f). Flash micro:bit card with `pxt` command.
 * [pxt-core](https://www.npmjs.com/package/pxt-core).
 * [pxt-microbit](https://www.npmjs.com/package/pxt-microbit).
   + [pxt-microbit GitHub](https://github.com/microsoft/pxt-microbit/). Includes API documentation.

Install:
```sh
npm install pxt
```

Setup the microbit target:
```sh
node_modules/pxt/pxt target microbit
```

Get help:
```sh
node_modules/pxt/pxt help all
```

Make project:
```sh
mkdir myproject
cd myproject
../node_modules/pxt/pxt init
../node_modules/pxt/pxt install
```
Then edit `main.ts` file.

To compile, run:
```sh
../node_modules/pxt/pxt build
```
It will generate a `binary.hex` file inside a `built` folder.
