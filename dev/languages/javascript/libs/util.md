# util

Node.js built-in util module.

## promisify

Transforms a callback-based function into a Promise-based one:
```js
const myAsyncFct = util.promisify(myFctWithCallback);
```
See [fs.readFile](https://nodejs.org/dist/latest-v6.x/docs/api/fs.html#fs_fs_readfile_file_options_callback) for an example of callback-based function.
