# Mocha
<!-- vimvars: b:markdown_embedded_syntax={'js':'javascript','html':'','sh':''} -->

Test framework.

A test file name `foo_test.mjs`:
```js
suite('foo_test.mjs');

import * as assert from 'assert/strict';
import * as foo from './foo.mjs';

test('My Test', () => {
    assert.equal(foo.fct1(), 2);
});
```

Running the tests:
```sh
npm test ./foo_test.mjs
```

## Asynchronous tests

# Via callbacks

The function to test, that accepts a callback:
```js
function divideCallback(x, y, callback) {
  if (y === 0) {
    callback(new Error('Division by zero'));
  } else {
    callback(null, x / y);
  }
}
```

The test:
```js
test('divideCallback', (done) => {
  divideCallback(8, 4,

    // The callback
    (error, result) => {
        if (error) {
          done(error);
        } else {
          assert.strictEqual(result, 2);
          done();
        }
    }
  );
});

```

# Via Promises

The function (division operation):
```js
function dividePromise(x, y) {
  return new Promise((resolve, reject) => {
    if (y === 0) {
      reject(new Error('Division by zero'));
    } else {
      resolve(x / y);
    }
  });
}
```

The test:
```js
test('dividePromise 1', () => {
  return dividePromise(8, 4)
  .then(result => {
    assert.strictEqual(result, 2);
  });
});
```

# Using test "bodies"

```js
test('dividePromise 2', async () => {
  const result = await dividePromise(8, 4);
  assert.strictEqual(result, 2);
  // No explicit return necessary!
});
```
