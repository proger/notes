# fs
<!-- vimvars: b:markdown_embedded_syntax={'js':'javascript','html':''} -->

Node.js built-in file system module.

```js
import * as fs from 'fs';
```

## readFile

```js
fs.readFile('myfile.txt');
```
