# JavaScript
<!-- vimvars: b:markdown_embedded_syntax={'js':'javascript','html':''} -->

 * [Exploring JS: JavaScript books for programmers](https://exploringjs.com/).Various books on TypeScript and JavaScript.
   + [JavaScript For Impatient Programmers](https://exploringjs.com/impatient-js/toc.html). Use **online** version, because PDF and EPUB versions contain only half of the book.
   + [Exploring ES6](https://exploringjs.com/es6/index.html#toc_ch_generators).

 * [Eloquent JavaScript](https://eloquentjavascript.net/).

 * [Electron](https://www.electronjs.org/). Framework to build crossplatform app using JavaScript, HTML and CSS, and running on Chromium and Node.js.

Client-side Javascript:
 * [The JavaScript language](https://javascript.info/js).
 * [JavaScript Introduction](https://www.w3schools.com/js/js_intro.asp).
 * [JavaScript — Dynamic client-side scripting](https://developer.mozilla.org/en-US/docs/Learn/JavaScript).
 * [JavaScript and HTML: possibilities and caveats](https://www.cs.tut.fi/~jkorpela/forms/javascript.html).
 * [Using keyboard shortcuts in JavaScript](http://www.catswhocode.com/blog/using-keyboard-shortcuts-in-javascript).

 * [Nifty Corners Cube](http://www.html.it/articoli/niftycube/index.html).

ECMAScript is the name of the Javascript standard.
Versions are: ECMAScript 1 to 6, then ECMAScript 2016, 2017, ..., ECMAScript 2022 (ES2022).
 * [Compatibility engines](http://kangax.github.io/compat-table/es5/).

## Node.js

 * [Node.js](http://nodejs.org).

Runtime engine for running javascript as a standalone application (server usually).

Node.js is a platform built on Chrome's JavaScript runtime for easily building fast, scalable network applications.
Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient, perfect for data-intensive real-time applications that run across distributed devices.

### npm

Node.js package manager.

## Execution

### Inside terminal

```sh
node myscript.js
```
or
```sh
nodejs myscript.js
```

On command line:
```sh
node -e "console.log('Hello')"
```

Interactive session:
```sh
node -i
```

### Shebang

```js
#!/bin/sh
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"
```

### JavaScript inside HTML

Write javascript in HTML:
```html
<script>
function myFunc() {
}
</script>
```

## Line ending

Statements should end with a semicolon.

If omitted at the end of a line, it will be most of the time automatically
added by the engine (ASI, Automatic Semicolon Insertion), but not always, and
it sometimes leads to silent bugs.

In some cases, JavaScript inserts always a semicolon, like for the `return`
statement if it is not followed by anything on the same line:
```js
function foo() {
    return
    { n:10 };
}
```
`foo()` will return nothing, or the engine will raise an error because the code
`{ n:10 };` is unreachable.

## Comments

One line comment:
```js
// My comment
```

Multiple lines comment:
```js
/* My
  multiple
  lines
  comment
 */
```

## Strict mode

Strict mode has been introduced in ECMAScript 5.

It is enabled by default inside modules and classes.

To enable it inside the whole content of a script file, put the following line
at the top:
```js
`use strict`;
```

To enable for a single function:
```js
function foo() {
    `use strict`;
}
```

## Types

Hierarchy of types:
                        (any)
                          ^  
                          |  
                -------------------
                |                 |
        (primitive value)      (object)
                ^                 ^   
                |                 |   
                |               Object
                |                 |
    undefined---|         Array---|                     
         null---|           Map---|
      boolean---|           Set---|
       number---|      Function---|
       bigint---|        RegExp---|
       string---|          Date---|
       symbol---|

*Primitive values* (or *primitives*):
 * are passed by value and compared by value.
 * have *properties*.
 * are *immutable*: we cannot change their properties (`s.length = 1` yields a `TypeError` exception).

*Objects*:
 * are passed by reference (*identity*) and compared by reference (*identity*).
 * have *properties*.
 * are *mutable*.

See `typeof` operator to get an indication on the type of a variable or value,
and `instanceof` operator to get the class of an object.

### undefined

*undefined* means *not initialized* for a variable and *not existing* for an object's property.
It is the way of the language to tell there is something wrong.

*undefined* has no properties.

### null

*null* is an intentional absence of value.

*null* has no properties.

Test if a variable is null:
```js
if (myvar === null) {
}
```

### Booleans

```js
true;
false;
```

Conversion:
```js
Boolean(x);
x ? true : false;
!! x;
```

### Big integers

 * [18 Bigints – arbitrary-precision integers [ES2020]](https://exploringjs.com/impatient-js/ch_bigints.html).

Bigint are integers with no limit:
```js
i = 15n;
// or
i = BigInt(15);
```

Supported bases:
```js
i = 0b0110n;
i = 0o7547n;
i = 0x5f7en;
```

Compatible operators:
 * `+`, `-`, `*`, `**`.
 * Results of `/` and `%` are rounded toward 0.
 * `<`, `<=`, `>`, `>=`.
 * `~`, `|`, `&`, `^`, `>>`, `<<`, `>>>`.
 * `~!=`, `==`, `===`, `!==`.
Attention with *btiwise* operators.
Bigints having no highest bit, their sign is encoded outside their value.
Two's complement is thus handled differently.
See [18.4.3 Bitwise operators](https://exploringjs.com/impatient-js/ch_bigints.html#bitwise-operators-1).

Casting to finite range integer:
```js
n = BigInt.asIntN(64, 123n); // 64 bits signed integer.
n = BigInt.asUintN(64, 123n); // 64 bits unsigned integer.
```

Arrays:
```js
a = new BigInt64Array();
b = new BigUint64Array();
```

### Numbers

Integer and float numbers.

```js
x = 12;
// or
x = Number(12);
```

Test:
```js
Number.isInteger(n);
Number.isSafeInteger(n);
```
A safe integer is one that is in the safe range of the number internal
representation.
You can make computation on them safely, as long as you stay inside the safe
range (see `Number.MAX_SAFE_INTEGER` and `Number.MIN_SAFE_INTEGER`).

```js
a = 0b101; // Binary
b = 0o764; // Octal
c = 25;    // Decimal
d = 0x9f;  // Hexadecial
```

`_` separator:
```js
n = 1_000_000; // 1 million
b = 0b0110_1100;
```

Convert to string:
```js
s = 3.14.toString();
```
Convert from string:
```js
x = Number("3.14");
```

NaN values:
```js
NaN;
Number.isNaN(x);
```

Infinity:
```js
Infinity;
```

### Strings

JavaScript characters that compose strings are 16bits.
==> Some Unicode characters like emojis are stored inside two JavaScript characters.

Define a string:
```js
var s = "foo";
var s2 = 'foo';
```

Get a character:
```js
c = s[0]; // Get character 0
```

Concatenate strings:
```js
u = s + t;
u += v;
```

Convert to string:
```js
s = String(45);
s = 65n.toString();
```

Get length:
```js
s.length;
```

Get a sub-string:
```js
s.substr(start, length);
s.substring(start, end); // End is excluded. If start > end, both values are swapped.
s.slice(start, end); // End is excluded. Indices start at zero.
```

Find:
```js
b = s.include(t);
b = s.startsWith(t);
b = s.endsWith(t);
i = s.indexOf(t);
i = s.lastIndexOf(t);
```

Split/join:
```js
arr = s.split(/, ?/);
s = arr.join(', ');
```

Pad/trim:
```js
t = s.padStart(4, '0');
t = s.padEnd(5, ' ');
t = s.trim(); // Remove all whitespaces at start and end
t = s.trimStart();
t = s.trimEnd();
```

Repeat a string:
```js
t = s.repeat(10);
```

Case:
```js
s = s.toUpperCase();
s = s.toLowerCase();
```

Comparing: operators `<`, `<=`, `>=`, `>`.

#### String literals

 * [21 Using template literals and tagged templates](https://exploringjs.com/impatient-js/ch_template-literals.html).

Interpolated values:
```js
var s = `Some string with values: ${64} ${false}`;
```

A string literal can span multiple lines:
```js
const s = `Hi
John`;
```

A *tagged template* is a *string literal* passed to a function.
Here is the syntax:
```js
ret = myfct`The ${object} costs ${price} EUR.`;
```
The function (called a *tagged function*) needs to accept a variable number of arguments:
```js
function myfct(...args) {
    // ...
}
```
This is because the literal string is passed as an array whose first element is an array of plain strings and other elements are the interpolated values:
```js
[['The ', ' costs ', ' EUR.'], 'book', 30]
```

Some tagged template libraries: `lit-html` (evaluates code), `re-template-tag` (regex), `graphql-tag` (GraphQL queries).

#### Accessing characters and codepoints

Create a string a code:
```js
s = String.fromCodePoint(0x1452d) // Unicode codepoint
s = String.fromCharCode(0xD83D); // JavaScript char code (16 bits)
```

Getting a code from a string:
```js
'€'.codePointAt(0).toString(16); // Gets the Unicode codepoint at index 0
'€'.charCodeAt(0).toString(16); // Gets the JavaScript char code at index 0
```

Iterate over Unicode codepoints of a string:
```js
for (const codePointChar of str) {
  console.log(codePointChar);
}
```

Count codepoints:
```js
unicode_length = Array.from(s).length;
```

### Symbols

Creating symbols:
```js
const a = Symbol();
const b = Symbol('Optional description');
```

Symbols can be used as property keys:
```js
const color = Symbol();
const obj = {
  [color]: 'blue',
};
```

Symbols can be used in swith/case statement:
```js
const RED = Symbol('Red');
const BLUE = Symbol('Blue');
switch (color) {
  case RED: do_something(); break;
  case BLUE: do_something_else(); break;
  default:
    do_default_action();
}
```

### Object literals

 * [Objects](https://exploringjs.com/impatient-js/ch_objects.html).

Object literals is like a dictionary.
Keys must be either `strings` or `symbols`, only.
Values can be anything: primitives or objects. It thus includes functions.
Before ES6, which introduced Maps, JavaScript did not have proper dictionary
structure, and thus objects were used as dictionaries.

```js
const RED = Symbol('RED');

const myobj = {

    // Properties
    n: 10,        // ID key (ID is converted into a string internally).
    s: 'smith',   // ID key
    'a': 12n,     // A property defined using an explicit string as the key.
    [key]: 45.10, // The expression inside the square brackets is evaluated to a string that will be the key.
    ['n_' + i]: 89n,
    [RED]: '#ff0000', // Use the symbol RED as a key: `Symbol(RED)`.

    // Method
    foo() { return this.n * 10; }
    'foo2'() { return this.n * 20; } // Method names can also be written as literal strings.

    // Property getter
    get date() {
        return /*get the date from a file modification date*/
    }

    // Property setter
    set date(d) {
        // change the modification date of the file
    }
};
```

Get and set an object property:
```js
let m = myobj.n; // Fixed property getting
let m = myobj['n']; // Dynamic property getting
myobj.n = 5;
let m = myobj?.n; // Optional fixed property getting. Returns `undefined` if `myobj` is `undefined` or `null`.
let m = myobj?.['n']; // Optional dynamic property getting. Returns `undefined` if `myobj` is `undefined` or `null`.
```

List object keys:
```js
const keys = Object.keys(myobj); // Returns all keys.
const keys = Reflect.ownKeys(myobj); // Exclude inherited keys.
const keys = Object.getOwnPropertyNames(myobj); // Returns string keys.
const keys = Object.getOwnPropertySymbols(myobj); // Returns symbol keys.
```

Get values of all string keys:
```js
const values = Object.values(obj);
```

Check if a key exists:
```js
if ('mykey' in myobj) ...;
```
Attention to truthiness check, as it can return `false` when property is
defined and set to `false`:
```js
if (myobj.mykey) ...;
```

Deleting a property:
```js
delete myobj.mykey;
```

Get key/value pairs for all string keys:
```js
entries = Object.entries(obj);
```

Create an object from key/value pairs:
```js
obj = Object.fromEntries([['a', 1], [symbolKey, 2]]);
```

It is possible to create an object without naming keys, by using only variables
as values.
The name of the variables will be used as key names:
```js
const obj = {a, b};
```

Define a custom String conversion:
```js
const myobj = {
    a: 5,
    toString() {
        return '[a=' + this.a + ']';
    }
};
```

Get the attributes of a property:
```js
Object.getOwnPropertyDescriptor(myobj, 'mykey');
```
The attributes are: `value`, `writable`, `enumerable`, `configurable`.
Change an attribute:
```js
Object.defineProperty(myobj, 'mykey', { writable: false });
```

Freezing (shallow freeze) an object (make it immutable):
```js
const myobj2 = Object.freeze(myobj1);
```

#### Enumerability

How to define a non-enumerable property:
```js
const obj = { a: 1}; /// Object with an enumerable property

// Set a non-enumerable property:
Object.defineProperties(obj, {
  b: {
    value: 10,
    enumerable: false,
  }
});
```

Enumerable properties are not spreaded:
```js
const obj2 = {...obj};

obj2.a; // OK
obj2.b; // Undefined
```

#### Destructuring

 * [Destructuring](https://exploringjs.com/impatient-js/ch_destructuring.html).

When destructuring, if a value is not found, it is set to `undefined`.

Getting a subset of values:
```js
const [x, y] = [1, 2, 3]; // x=1 and y=2
```

Extracting values from object and setting them to variables:
```js
const obj = {a:1, b:2, c:3};
const {a: x, b:y} = obj; // x is set to obj.a (1) and y to obj.b (2).
const {f: z = 'defaultvalue'} = obj; // Use of a default value.
const {a, b} = obj; // This time the variables will be a and b.
const {a: x, ...z} = obj; // x is set to obj.a (1) and z to {b:2, c:3};
({c: z} = obj); // Use parenthesis for assignment to existing variable, since starting a line with `{` means the start of block of code, not a object-destructuring statement.
```

Getting value from an object/dictionary:
```js
const a = { n: 10, s: 'abc' };
const {s = 'defaultValue'} = a; // `s` will be 'abc' since `a` has a property `s`
foo(s);
```

Array-destructuring:
```js
const [a] = ['x'];
let [b] = ['y'];
[b] = ['z'];
const [x='defaultValue'] = []; // Use of a default value.
const [, a] = ['x', 'y']; // a is set to 'y'.
const [a, b] = mySet; // Works with iterable. Takes first and second values.
const [a, b, ...z] = mySet; // z contains all remaining elements.
[x, y] = [y, x]; // Swap.
const [, year, month, day] = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/.exec('2999-12-31'); // Destructure the returned array.
```

With a function:
```js
const f = ([x]) => x;
f(['a']); // Returns 'a'
```

Loop on array with indices:
```js
for (const [i, v] of arr.entries()) {}
```

Extact values from an array:
```js
const {0:a, 5:b} = arr; // Extract values at indices 0 and 5 and write them into variables a and b.
```

It works with properties of the primitives:
```js
const {length: len} = 'abc';
```

Mixing array and object destructuring:
```js
const arr [{a:1, b:2}, {a:5, c:10}];
const [, {a}] = arr; // a is set to 5.
```

#### Assign

The `assign()` method copies properties from some objects into another one, overwriting if necessary:
```js
const target = {};

Object.assign(target, {a: 5, c:20}, {a:10, b:50});
```

#### Spreading (...)

The spreading syntax `...expr` evaluates the expression following the three
dots `...` and iterates over the result.
Thus the expression must evaluate into an *Iterator*.

Given an object `myobj`:
```js
const myobj = {
  n: 1,
  opt: {
    readonly: true,
  },
};
```

We can make a shallow copy of an object:
```js
const copy = {...myobj};
```
Be careful, that this is **not** a deep copy.
Thus objects are shared.
As a consequence, the following code:
```js
copy.n = 10;
copy.opt.readonly = false;
```
will not modify `myobj.n`, but will modify `myobj.opt.readonly`.

It is possible to add new properties while making the shallow copy:
```js
const copy = {...myobj, mynewfield: 'abc'};
```

If the new property exists inside the parent, it is overwritten.

#### Prototype chains

This is the *inheritance* mechanism of JavaScript.
Each object has `null` or an `object` as its prototype.

Create an object with an explicit `null` prototype:
```js
const obj = Object.create(null);
```

Get the prototype of an object:
```js
const prot = obj.getPrototypeOf(obj);
```

Inheritance:
```js
const obj_a = {
  a: 10,
};

const obj_b = {
  __proto__: obj_a,
  b: 'abc',
};
```
Object `obj_b` has two properties `a` and `b`.
To get a list of keys only defined by `obj_b`:
```js
const keys = Reflect.ownKeys(obj_b);
```

If we modify the property `a` that was inherited:
```js
obj_b['a'] = 20;
```
then a new property `a` is created inside `obj_b` that hides the property `a` of `obj_a`.

Check if a property is defined but no inherited:
```js
Object.hasOwn(obj_b, 'a');
```

### Arrays

 * [Arrays](https://exploringjs.com/impatient-js/ch_arrays.html).

In Javascripts, arrays are used as lists, stacks, queues, tuples, etc.
Arrays are objects, their indices are normal string keys (not numeric,
numbers are converted to strings) and values are stored as normal object
properties.
In practice, engines implement real arrays behind for optimization.

Due to their nature (objects whose properties are used to store values), arrays
can be sparse in JavaScript:
```js
const arr = [];
arr[0] = 5;
arr[2] = 10;
```
There is not element at index `1` in this array.
Another way to write this:
```js
const arr = [5, , 10];
```

Define an array:
```js
const arr = [1, 2, 3, 4, 5];
const arr = new Array(5).fill(0); // Array of zeros.
const arr = Array.from({length: 2, 0:'a', 1:'b'}); // ['a', 'b'] array. The object passed is a called an *Array-like*.
const arr = Array.from({length: 4}, () => ({})); // Array of empty objects.
const arr = Array.from({length: 10}, (_, i) => i); // Integer range 0 to 9.
```

Access/set an element:
```js
x = arr[0]; // indices >= 0
y = arr.at(-2); // negative indices are accepted
arr[0] = 12;
```

Get/set length:
```js
len = arr.length;
arr.length = 2; // Remove elements by cutting length.
arr.length = 0; // Clear array destructively (impacts others referencing this array).
arr = []; // Clear array non-destructively.
```

Loop on values:
```js
for (const value of arr) {
}
```

Loop on indices & values:
```js
for (const [i, value] of arr.entries()) {
}
```

Loop on indices:
```js
for (let i = 0; i < arr.length; ++i) {
}
// or
for (const i of arr.keys()) {
}
```

Get content:
```js
arr.keys();
arr.entries(); // Get key/value pairs
```

Type:
```js
Array.isArray(arr);
typeof(arr); // Returns `object`
```

Append and insert values:
```js
arr.push(10);
arr.push(...[11,15]); // Appends another array
new_arr = [...arr1, ...arr2, 13, 20];
arr.unshift(5); // Inserts at start.
x = arr.shift(); // Removes first element.
x = arr.pop(); // Removes last element.
```

Processing:
```js
arr.slice(1, 3); // Extracts range
arr.filter(x => x !== 13); // Filters values
arr.map(x => x+x); // Applies function
arr.flatMap(x => [x,x]); // Inserts/replaces elements inside the array.
arr.reverse(); // Reverses order of elements.
arr.fill(10); // Replaces all elements.
```

Remove elements:
```js
arr.filter((_, i) => i !== 1); // Removes non-destructively.
arr.splice(1, 1); // Removes destructively.
```

Sorting:
```js
x = arr.sort();
x = arr.sort((a,b) => a-b);
```

Test and search:
```js
arr.includes('myvalue');
arr.some(x => x === 4);
arr.every(x => x === 4);
arr.indexOf(13);
arr.lastIndexOf(13);
arr.find(x => x === 20);
arr.findIndex(x => x === 30);
```

Join:
```js
arr.join('-');
```

Reduce:
```js
arr.reduce((result, x) => result + x, 0);
arr.reduceRight((result, x) => result + x, 0);
```

### Typed Arrays

 * [Typed Arrays: handling binary data](https://exploringjs.com/impatient-js/ch_typed-arrays.html).

Class diagram:

     DataView--------------
                 buffer   |
                          v
                      ArrayBuffer
                          ^
                   buffer |
     TypedArray<T>---------
       ^
       |
       |--Int8Array
       |--Uint8Array
       |--Uint8ClampedArray
       |--Int16Array
       |--Uint16Array
       |--Int32Array
       |--Uint32Array
       |--Float32Array
       |--Float64Array

New buffer of 4 bytes (initialized with zeros):
```js
const buf = new ArrayBuffer(4);
```

Creating typed arrays:
```js
const arr = new Int8Array(3);
const arr = new Int8Array([0, 1, 2]);
const arr = Int8Array.from([0, 1, 2]);
const arr = Int8Array.of(0, 1, 2);
```

Accessing values:
```js
x = arr[10];
for (const x of arr) {}
arr[3] = 50;
arr.set(other_arr, offset); // Copy the content of another array at an offset position.
```

The class `ArrayBuffer` is used internally by `DataView` and `TypeArray<T>`:
```js
const arr = new Int8Array([3, 8, 10]);
arr.buffer; // The ArrayBuffer containing the data.
```

DataView provides a way to write various number types to a binary buffer:
```js
const view = new DataView(new ArrayBuffer(32));
i = view.getInt16(5);
i = view.getUint32(2);
view.setUint8(0, 5);
```

### Maps

 * [Maps](https://exploringjs.com/impatient-js/ch_maps.html).

A map accepts any value as a key.
`NaN` is treated exceptionaly as a single value, equal to itself, so it can be
used as a key.

Creating a Map object:
```js
const m = new Map();
const m = new Map([[1, 'a'], [2, 'b']]);
const m = new Map().set(1, 'a').set(2, 'b');
const m = new Map(anotherMap);
```

Convert to/from array:
```js
const arr = Array.from(myMap);
const myMap = new Map(arr);
```

Convert to/from object:
```js
const obj = Object.fromEntries(myMap);
const myMap = new Map(Object.entries(obj));
```

Get a value:
```js
const x = myMap.get('mykey');
const x = myMap.get('mykey') ?? 'Default Value'; // Returns a default value if the key does not exist.
```
Returns `undefined` if the key does not exist.

Test if a key exists:
```js
myMap.has(myKey);
```

Delete a key:
```js
myMap.delete(myKey); // Returns `true` if the key existed.
```

Get all keys, values and entries:
```js
myMap.keys(); // iterable
myMap.values();
myMap.entries();
```

Map function on entries:
```js
const map2 = new Map(Array.from(map1).map(([k. v]) => [ k*2, 'id_' + v]));
```

Filter entries:
```js
const map2 = new Map(Array.from(map1).filter(([k. v]) => k < 100));
```

Loop on indices and values:
```js
for (const entry of myMap.entries()) {}
// or
for (const [k, v] of myMap.entries()) {}
```

Merge two maps:
```js
const map = new Map([...map1, ...map2]);
```

Get size:
```js
const sz = myMap.size;
```

Delete all content:
```js
myMap.clear();
```

### WeakMaps

 * [WeakMaps](https://exploringjs.com/impatient-js/ch_weakmaps.html).

Keys needs to be objects. Primitive values are forbidden.

Keys are *weakly held*. Object references can still be garbage-collected
outside of the map, making the key invalid.

Keys are hidden. You must know the key to get the value.

You cannot:
 * get keys, values or entries,
 * get the size,
 * clear an instance.

Usage:
 * Store object properties outside the object.
 * Store private properties of instances.

Storing a private property of a class inside a WeakMap object:
```js
const _n = new WeakMap();

class A {
  constructor(n) {
    _n.set(this, n);
  }
}
```
If stored in a module, the class `A` can be exported while the `WeakMap` is
not.
It is thus impossible to access `_n` from outside the module, since `_n` is
unknown to outsiders.

### Sets

 * [Sets](https://exploringjs.com/impatient-js/ch_sets.html).

Creating a set:
```js
const set = new Set();
const set = new Set(['a', 'b', 'c']);
const set = new Set().add('a').add('b');
```

Basic properties and methods:
```js
set.has('a'); // Checks if an element exists.
set.delete('a'); // Deletes an element.
set.size;
set.clear();
set.values(); // Returns all values.
```

Loop:
```js
for (const x of set) {}
```

Convert to/from array:
```js
const arr = Array.from(set);
const set = new Set(arr);
```

Union. intersection and difference:
```js
const union = new Set([...a, ...b]);
const intersection = new Set(Array.from(a).filter(x => b.has(x)));
const difference = new Set(Array.from(a).filter(x => ! b.has(x)));
```

Map function on elements:
```js
const b = new Set(Array.from(a).map(x => x*2));
```

Apply a function on each element:
```js
set.forEach(x => foo(x));
```

Filter:
```js
const b = new Set(Array.from(a).filter(x => x < 100));
```

### WeakSets

 * [WeakSets](https://exploringjs.com/impatient-js/ch_weaksets.html).

WeakSets only accept objects.
Iterating is not possible.
WeakSets weakly hold references to objects.

Allowed methods are:
```js
const ws = new WeakSet();
ws.add(x);
ws.has(x);
ws.delete(x);
```

## Identifiers

Identifiers for variable names, function names, etc. may use only characters in:
 * ASCII letters A-Z, a-z and Unicode letters.
 * ASCII digits 0-9 and Unicode digits.
 * `$`, `-` and `_`
They cannot start with a digit.

Identifiers starting with an underscore (`_`) have a special meaning.
In functions, a parameter starting with `_` means it is not used:
```js
function foo(_x, a) {
    return a;
}
```
In classes, a property starting with `_` is considered private:
```js
class A {
    constructor(n) {
        this._n = n;
    }
}
```

## Variables

Declares a local scope variable:
```js
let a = 1;
```

Declares an immutable variable binding:
```js
const a = 1;
```
We cannot affect another value to `a` after this declaration.
However, the value itself may be changed, for instance in the case of an object.
Example:
```js
const arr = [1, 2, 3];
arr[1] = 10;
```

`const` can be used in `for-of` loops:
```js
for (const e of arr) {
}
```
But not in plain loops:
```js
for (let i=0 ; i < n ; ++i) {
}
```

### The Global Object

Declaring a property inside the global object `globalThis`:
```js
var a = 1;
assert.equal(globalThis.a, a);
```
Using this object for storing global scope variables is deprecated, use `let`
instead at the global scope level.

Older ways of accessing the Global Object can be observed, depending on the
platform: `window`, `self`, `global`.
It is a better practice to access the properties of the Global Object directly,
since it works on all JavaScript platforms:
```js
encodeURIComponent(str);
// instead of
window.encodeURIComponent(str);
```

### Scope

Declaration and assignment:
```js
function f() {
  // Partial early activation: (x is already declared at function scope, but unassigned.
  assert.equal(x, undefined);
  if (true) {
    var x = 123; // The assignment is executed at this line
    assert.equal(x, 123);
  }
  assert.equal(x, 123); // x is defined and part of function scope.
}
```

Closures (implementation of static scope variables):
```js
function createInc(startValue) {
  // A closure is created: function + variables that exist at its birth place.
  // Here `startValue` is memorized at this place
  let index = -1; // Like `startValue`, the scope variable `index` is memorized at this place.
  return (step) => {
    startValue += step;
    return startValue;
  };
}
const inc1 = createInc(5);
const inc2 = createInc(0);
assert.equal(inc1(2), [0, 7]);
assert.equal(inc1(2), [1, 9]);
assert.equal(inc2(1), [0, 1]);
```

## Operators

 * Operators **coerce** their operands to appropriate types.
 * Most operators only work with *primitive values*.

```js
x = '3' * '5'; // `x` is a primitive number (15).
myobj[true] = 54; // `[]` Converts `true` to the string `"true"' before searching for this property.
                  // `[]` only accepts strings and symbols.
```

The `+` operator works only on integers and strings. Objects will be coerced to strings:
```js
[1,2,3] + 's'; // Gives `"1,2,3s"`
```

The `,` (comma) operator evaluates both its operands and returns the second one:
```js
a, b
```

The `void` operator evaluates its operand and returns `undefined`:
```js
void(x=1)
```

Comparison:
```js
n < 2;
n <= 12;
'a' === s;
'a' !== s;
1 === s; // May be true only if s is a number.
1 == s; // May be true if s is either a string or a number.
```

`===` tests value and type, `==` makes conversion before testing.

### Logical operators

| Operator | Description                            |
| ---      | ---                                    |
| `!`      | Not                                    |
| `?:`     | Ternary                                |
| `&&`     |                                        |
| `||`     |                                        |
| `??`     | nullish coalescing operator            |
| `==`     | Converts before testing                |
| `===`    | Compares types & values                |
| `!=`     | Converts before testing                |
| `!==`    | Compares types & values                |
| `>`      |                                        |
| `<`      |                                        |
| `>=`     |                                        |
| `<=`     |                                        |
| `||=`    |                                        |
| `&&=`    |                                        |
| `??=`    | nullish coalescing assignment operator |

Returns `b` only if `a` is `null` or `undefined`:
```js
a ?? b
```

### Bitwise operators

| Operator | Description             |
| -------- | ----------------------- |
| `&=`     |                         |
| `^=`     |                         |
| `|=`     |                         |
| `<<=`    |                         |
| `>>=`    |                         |
| `>>>=`   |                         |

### Arithmetic operators

| Operator | Description             |
| -------- | ----------------------- |
| `+`      |                         |
| `-`      |                         |
| `*`      |                         |
| `/`      |                         |
| `%`      |                         |
| `**`     |                         |
| `++`     |                         |
| `--`     |                         |
| `=`      |                         |
| `+=`     |                         |
| `-=`     |                         |
| `*=`     |                         |
| `/=`     |                         |
| `%=`     |                         |
| `**=`    |                         |

### Maps

 * [Maps](https://exploringjs.com/impatient-js/ch_maps.html).

## Statements

### if

```js
if (i > 10) {
} else if (i < 40) {
} else {
}
```

Any value can be used inside a `if` condition, not just booleans:
```js
if (value) {
}
```
*falsy* values: `undefined`, `null`, `false`, `0`, `0n`, `NaN`, `''`.
All other values are *truthy*.

### for

```js
for (let i = 0 ; i < 10 ; ++i) {
}
```

### for-of

Iterates over an *iterable*.

An *iterable* may be an *array*, a *set*, etc.

```js
for (const x of my_set) {
}
```

With an index:
```js
for (const [i, x] of my_arr.entries()) {
}
```

### for-await-of

Iterates over an *asynchronous* iterable:
```js
for await (const item of my_iterable) {
}
```
Must be used inside an *async* generator or an *async* function.

### for-in

Visits all enumerable property keys of an object.

Iterate over the indices of an array:
```js
for (const key arr) {
}
```

### break / label

```js
for (const a in arr) {
    const b = 2;
    my_label: {
        for (const c in arr2) {
            for (const d in get_items(c)) {
                if (foo(c))
                    break my_label;
            }
        }
    }
    delete obj;
}
```

### while & do-while

```js
while (i < n) {
    ++i;
    if (foo(i))
        continue;
    if (foo2(i))
        break;
}
```

```js
do {
    x = foo();
    if (foo2(x))
        break;
} while (x !== null);
```

### switch

```js
switch (expr) {
    case 1:
        // do something
        break;
    case true:
        // do something
        break;
    case 'foo':
        // do something
        break;
    default:
        // do something
}
```

## Function

*Ordinary* functions:
 * Real function.
 * Method.
 * Constructor function.

*Specialized* functions:
 * *Arrow function* --> real function.
 * *Method*         --> method.
 * *Class*          --> constructor function.

Declaring a function using the statement way:
```js
function product(a,b) {
    return a*b;
}
```
The function may be called before its declaration, inside the same scope.

Another way to declare a function, is to use a variable and a *nameless* function object:
```js
const product = function(a,b) {
    return a*b;
}
```
The variable `product` cannot be used before its declaration.
An other way to create the same variable, using the `new` operator:
```js
const product2 = new Function('a', 'b', 'return a*b');
```
**Attention**, by default functions created with `new Function()` are in *non-strict* mode.
This *strict* mode must be switched manually. <!-- TODO How ? -->

Optional method call:
```js
foo?.(a, b);
```
If `foo()` is not defined, returns `undefined`, otherwise calls `foo()`.

### bind

Use `bind()` to create a call back method for event listening:
```js
class ClickHandler {
  constructor(id, elem) {
    this.id = id;
    const listener = this.handleClick.bind(this);
    elem.addEventListener('click', listener);
  }
  handleClick(event) {
    alert('Clicked ' + this.id);
  }
}
```

## `=>` arrow function

*Named* function object can be used for recursivity:
```js
const foo = function foo(a,b) {
    // ...
    return foo(a-1,b-2);
}
```
A name declared this way can only be used inside the function itself.

Note that all function objects have a name property.

An *ordinary* function `foo()` may be used in three different roles.
Real function:
```js
foo();
```
Method:
```js
const obj = { do_foo: foo};
obj.do_foo();
```
Constructor:
```js
const instance = new foo(); // instance of `foo`.
```

Function objects have the following methods:
 * `call()`: `foo.call(thisValue, a, b, ...)`. Useful to pass an explicit `this` object.
 * `apply()`: `foo.apply(thisValue, [a, b, ...])`.
 * `bind()`: `const foo2 = foo.bind(thisValue, a)`. Create a new function object with fixed parameters for the original function. `foo2(12)` is the same as `foo(thisvalue, a, 12)`.

### `=>` arrow function

A *arrow* function is a function *specialized* to be only an ordinary *real* function:
```js
const add1 = (a, b) => { return a + b }; // body is a code block
const add2 = (a, b) => a + b;            // body is an expression
const neg = x => -x;                     // with a single parameter
```

Contrary to ordinary functions, an arrow function does not define a `this` object.
Thus it can the use the one of the scope when inside an object:
```js
const obj = {
    foo() {
        // this is defined to be the current object obj.

        const myfct = () => { return this.myfield; }

        return myfct();
    }
}
```

### Method

A *method* is a function *specialized* to be only an ordinary *method* function:
```js
const obj = {
    foo() {
        // do something
    }
};
```

### Class

A *class* is a function *specialized* to be only an ordinary *constructor* function:
```js
class A {
}
const inst = new A();
```

## Iterable

An iterable is built by implementing both `Iterable` and `Iterator` interfaces.
Built-in iterables are: Arrays, Strings, Maps, Sets and for browsers: DOM data structures.

An iterable must have a key `@@iterator` (represented by `Symbol.iterator`)
whose value is the method that returns the iterator on an object.

Iterating using `next()` method:
```js
const arr = [1, 2, 3, 4];
const i = arr[Symbol.iterator]();
while (true) {
    const {value, done} = i.next();
    if (done)
        break;
    console.log(value);
}
```

Iterating using `for-of`:
```js
for (const x of arr) {
}
```

Example of an iterable custom class? <!-- TODO -->

Synchronous iteration:
```js
const [x,y] = iterable; // Destructuring.
fct(...iteratble); // Spreading
const arr = [...iterable]; // Spreading
for (const x of iterable) {} // for-of
function* generatorFunction() { // yield*
    yield* iterable;
}
```

Turning iterables into data structures:
```js
const obj = Object.fromEntries(iterableOverKeyValuePairs);
const arr = Array.from(iterable);
const m  = new Map(iterableOverKeyValuePairs);
const wm = new WeakMap(iterableOverKeyValuePairs);
const s  = new Set(iterableOverElements);
const ws = new WeakSet(iterableOverElements);
```

Two types of iteration exist: **External** and **Internal**.
**External iteration** (pull) is done through the iteration protocol.
**Internal iteration** (push) is done through a method for which we provide a
*callback* function.
Illustration:
```js
for (const x of ['a', 'b']) { console.log(x); } // External iteration.
['a', 'b'].forEach( (x) => { console.log(x); } ); // Internal iteration.
```

### Async iteration

 * [Async iteration](https://exploringjs.com/impatient-js/ch_async-iteration.html).

Async generator to convert a synchronous iterable to asynchronous one:
```js
async function* syncToAsyncIterable(syncIterable) {
  for (const elem of syncIterable) {
    yield elem;
  }
}
```

Direct usage:
```js
const arr = syncToAsyncIterable(['a', 'b']); // Get asynchronous iterable
const i = arr[Symbol.asyncIterator]();       // Get asynchronous iterator

i.next().then(res => {/*...*/}).then(res => {/*...*/})
```

Usage inside an async function:
```js
async function foo() {
  const arr = syncToAsyncIterable(['a', 'b']); // Get asynchronous iterable
  const i = arr[Symbol.asyncIterator]();       // Get asynchronous iterator

  x = await i.next();
  y = await i.next();
  /*...*/
}
```

Using for-await-of:
```js
for await (const x of syncToAsyncIterable(['a', 'b'])) {
  console.log(x);
}
```
for-await-of supports also synchronous iterables and synchronous iterables over
values that are wrapped in Promises.

## Generators

 * [Synchronous generators](https://exploringjs.com/impatient-js/ch_sync-generators.html).
 * [Generators](https://exploringjs.com/es6/ch_generators.html).

A generator is a function or method that returns an iterable.
More precisely it returns an iterator that is iterable.
At this point the body of the generator has not yet been executed.
On the first call to the `next()` method of the returned iterator, the
generator function is executed up to the first `yield` statement.
The execution of the generator function is paused and the value of the `yield`
statement is returned.
On each subsequent call to the `next()` method, the execution of the generator
function is resumed up to the next `yield` statement.

Example:
```js
function* genFct() {
  yield 10;
  yield 20;
}
```
`yield` pauses the execution of the generator. On the next iteration

In a class:
```js
class A {
  * genMethod() {}
}
```

Usage:
```js
for (const x of genFct()) {} // Looping.
const arr = Array.from(genFct()); // Converting into an array.
```

Example of a generator map function:
```js
function* mapGen(iter, fct) {
  for (const x of iter)
    yield fct(x);
}
```
Contrarily to the `Array.map()` method, this function will only process the input iterable on demand.

Calling another generator from a generator (`yield*` operator):
```js
function* gen1() {
  yield 10;
  yield 20;
}

function* gen2() {
  yield* gen1();
}
```
See the `binary_tree.js` example.

Generators are an elegant way to write and use data traversal algorithms.
A traditional way would be to use a callback:
```js
function visitData(data, fct) {
  // Do traversal
    fct(item);
  // Continue traversal
}

visitData(myData, myFct); // Usage
```

With a generator:
```js
function* iterData(data) {
  // Do traversal
    yield item;
  // Continue traversal
    yield* iterData(subdata); // Possibly use recurrence.
}

for (const item of iterData(myData)) {} // Usage
```

### Asynchronous generators

Simple example:
```js
async function* yield123() {
  for (let i=1; i<=3; i++) {
    yield i;
  }
}
```

Async generator to convert a synchronous iterable to asynchronous one:
```js
async function* syncToAsyncIterable(syncIterable) {
  for (const elem of syncIterable) {
    yield elem;
  }
}
```

## Run-to-completion semantics (setTimeout)

`setTimeout()` appends a task (i.e.: function) to the tasks queue, after
waiting n milliseconds:
```js
setTimeout(() => { /*...*/ }, 5);
```

`clearTimeout()` cancels a task inside the tasks queue:
```js
clearTimeout(hd);
```

Due to run-to-completion semantics (tasks are always run one after the other),
the next task on the queue is executed only after completion of the current
one:
```js
console.log('This is printed first.');
setTimeout(() => {
  console.log('This is printed last.');
}, 0);
console.log('This is printed second.');
```
## Asynchronous programming

 * [Asynchronous programming](https://exploringjs.com/impatient-js/ch_async-js.html).

### Events

 * [Delivering asynchronous results via events](https://exploringjs.com/impatient-js/ch_async-js.html#event-pattern).

Example with `IndexedDB` (browser built-in database):
```js
// Try to open database: the operation is added to the tasks queue and will be
// run only after the section is finished executing and the event listeners are
// declared.
const openRequest = indexedDB.open('MyDatabase', 1);

// Set event listener
openRequest.onsuccess = (event) => {
  const db = event.target.result;
  // ···
};

// Set event listener
openRequest.onerror = (error) => {
  console.error(error);
};
```

`XMLHttpRequest` example:
```js
const xhr = new XMLHttpRequest();
xhr.open('GET', 'http://.com/myfiletodownload.txt');
xhr.onload = () => {
  if (xhr.status == 200) {
    processData(xhr.responseText);
  } else {
    throw new Error(xhr.statusText);
  }
};
xhr.onerror = () => { };
xhr.send(); // Activate
```

DOM example:
```js
const element = document.getElementById('my-link');
element.addEventListener('click', clickListener);

function clickListener(event) {
  event.preventDefault(); // Cancel default action.
  console.log(event.shiftKey);
}
```

### Callbacks

Example with `readFile`:
```js
readFile('some-file.txt', {encoding: 'utf8'},
  (error, data) => {
    if (error)
      throw error;
    // Process returned data
  });
```

### Promises

 * [Promises for asynchronous programming](https://exploringjs.com/impatient-js/ch_promises.html).

Using a promise-based function:
```js
myPromise(a).then(result => foo(result)).catch(err => handle_error(err));
```
A `Promise` object has at least two callbacks registered: one for success,
another for failure.
All code in `myPromise(a)` is executed now.
The declared callbacks are append to the tasks queue and executed later.

We can also declare a `finally()` callback that is always executed after
`then()` or `catch()`:
```js
myPromise(a).then(result => foo(result)).catch(err => handle_error(err)).finally(() => {/*...*/});
```
The `finally()` callback always return the settled `Promise` object on which it
was called, unless it throws an exception.
Be careful with the order of declaration of the callback. If `finally()` is
declared before `then()` or `catch()`, it will executed first

Implementing a promise-based function:
```js
function myPromise(param1, param2) {
  return new Promise(
    (resolve, reject) => {
      if (param1 === undefined || param2 === undefined) {
        reject(new Error('My error message')); // Error will be sent to catch().
      } else {
        resolve(/* Do processing here. The result will be sent to then(). */);
      }
    });
}
```

Do not throw exceptions from `Promise` methods.
Only rejections are expected to be returned as errors from Promise-based
functions.
That means do not use synchronous methods that may throw exceptions.
One solution is to use a try/catch statement:
```js
function asyncFunc() {
  try {
    doSomethingSync();
    return doSomethingAsync().then(result => { /*...*/ });
  } catch (err) { return Promise.reject(err); }
}
```
Another solution is to move synchronous code into the `then()` callback, using
the fact that `then()` converts all exceptions to rejections:
```js
function asyncFunc() {
  return Promise.resolve()
    .then(() => { doSomethingSync(); return doSomethingAsync(); })
    .then(result => { /*...*/ });
}
```
The `new Promise()` declaration style also converts exceptions to rejections:
```js
function asyncFunc() {
  return new Promise((resolve, reject) => {
      doSomethingSync();
      resolve(doSomethingAsync());
    })
    .then(result => { /*...*/ });
}
```

States of a `Promise` object:
 * Pending
 * Fulfilled (settled)
 * Rejected  (settled)

Once settled, a `Promise` is never changed.
Result is cached, so callbacks can run at any moment.

The `Promise.resolve()` method returns either the `this` `Promise` object if
the `then()` method returns a non-Promise value, or the `Promise` object
returned by `then()`.
As a consequence, in *nested calls* like the following one:
```js
asyncFunc1().then(result1 => { /*...*/ asyncFunc2().then(result2 => { /*...*/}); });
```
The outside `then()` method will return the `Promise` created by
`asyncFunc2()`, not the one created by `asyncFunc1()`.
It thus equivalent to write the same code, flattened:
```js
asyncFunc1().then(result1 => { /*...*/ return asyncFunc2(); }).then(result2 => { /*...*/ });
```
The second `then()` will effectively be called on the `Promise` created by
`asyncFunc2()`.

Any exception thrown by `then()` leads to the execution of the `catch()` callback.

Example (reading a file), in Node.js:
```js
import * as fs from 'fs';
import * as util from 'util';

const readFileAsync = util.promisify(fs.readFile); // Converts fs.readFile() into a Promise-based function.

readFileAsync('myfile.txt')
  .then(text => { /* Parse the text. */ })
  .catch(err => { /* Handle error. */ });
```

`XMLHttpRequest` example:
```js
function httpGet(url) {
  return new Promise(
    (resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = () => {
        if (xhr.status === 200) {
          resolve(xhr.responseText); // (A)
        } else {
          // Something went wrong (404, etc.)
          reject(new Error(xhr.statusText)); // (B)
        }
      }
      xhr.onerror = () => {
        reject(new Error('Network error')); // (C)
      };
      xhr.open('GET', url);
      xhr.send();
    });
}
```

Browsers provide `Fetch`, a Promise-based API:
```js
fetch('http://example.com/textfile.txt')
  .then(response => response.text())
  .then(text => {
    assert.equal(text, 'Content of textfile.txt\n');
  });
```

#### Promise combinators

Primitive functions: `Promise.resolve()`, `Promise.reject()`.
Combinators: `Promise.all()`, `Promise.race()`, `Promise.any()`, `Promise.allSettled()`.

Example with `Promise.all()`:
```js
const promises = [
  Promise.resolve('result a'),
  Promise.resolve('result b'),
  Promise.resolve('result c'),
];
Promise.all(promises).then((arr) => /* Handle array of results. */)
  .catch((err) => /* Handle error. */);
```

Using a single Promise-based function applied on an array of data:
```js
function timesTwoAsync(x) {
  return new Promise(resolve => resolve(x * 2));
}
const arr = [1, 2, 3];
const promiseArr = arr.map(timesTwoAsync);
Promise.all(promiseArr).then(result => { /* Process results. */ });
```

### Asynchronous functions

 * [Async functions](https://exploringjs.com/impatient-js/ch_async-functions.html).

Async functions provide better syntax for promises.

Using a try/catch clause:
```js
async function foo(a) {
  try {
    // Functions prefixed with `await` are put one after the other inside the
    //tasks queue.
    // They are thus executed sequentially later.
    const result1 = await myPromise1(a); // async call
    const result2 = await myPromise2(a); // async call   

    return foo(result1, result2); // sync call
  } catch (err) {
    handle_error(err);
  }
}

// Usage
foo('value').then(obj => { /* Process returned obj */ })
  .catch(err => { /* Handle error */ });
```

Async functions always return a `Promise`.
All returned values by an async function are used to fulfill the promise.
Any returned value is wrapped into a `Promise` object unless it is a `Promise`
object itself.
All thrown exceptions are used to reject the promise.

Various forms of declaration:
```js
async function func1() {}           // Function declaration
const func2 = async function () {}; // Function expression
const func3 = async () => {};       // Arrow function
const obj = { async m() {} };       // Method in object literal
class MyClass { async m() {} }      // Method in class
```

Example of downloading URLs:
```js
async function downloadContent(urls) {
  const promiseArray = urls.map(
    url => httpGet(url));
  return Promise.all(promiseArray); // Functions are executed concurrently. They are all started at the time.
}
```

## Web Workers

 * [Web Workers API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API).

Web Workers allow to run a script inside a separate background thread.

## I/O

Print on stdout:
```js
console.log("My text");
console.log("My text", x, "abc", true); // Print all values concatenated
console.log("Some text: %s %s", 12, true); // Uses string substitution
```
Substittuion tags:
 * `%s` converts to a string.
 * `%o` inserts the string representation of an object.
 * `%j` converts to a JSON string.
 * `%%` inserts a percent character.

Print on stderr:
```js
console.error(" my text");
```

Alert:
```js
alert("hello");
```

## Modules

 * [Modules](https://exploringjs.com/impatient-js/ch_modules.html).
 * [Module specifiers](https://exploringjs.com/impatient-js/ch_modules.html#module-specifiers): absolute paths, relative paths, URL, ... How modules are found in Node.js and browser.
 * [Polyfills: emulating native web platform features](https://exploringjs.com/impatient-js/ch_modules.html#polyfills). A polyfill is a module that provides a certain feature only when it is not available on the current browser. This allow to run modern code on old browsers.

A module is a file that export objects.

Legacy code modularization techniques:
 * *Scripts*: code fragments run in **global scope** by browsers.
 * *CommonJS modules*: module format used mainly on *Node.js*.
 * *AMD modules*: module format mainly used in browsers.
Now *ECMAScript modules* (ES6) supersedes all these formats.

|                   | Target               | Loaded | ext          |
| ---               | ---                  | ---    | ---          |
| Script            | browsers             | async  | `.js`        |
| CommonJS module   | servers              | sync   | `.js` `.cjs` |
| AMD module        | browsers             | async  | `.js`        |
| ECMAScript module | browsers and servers | async  | `.js` `.mjs` |

The `import` statement is only usable inside a module file.

A `mymodule1.mjs` file:
```js
export function foo() { // Inline export
    // ...
}

export const N = 10; // Inline export

export {add, SIZE as S}; // Export clause. `SIZE` is renamed as `S`.
```

Default export:
```js
export default function f() {}
```
For `const` it is slightly different:
```js
const N = 10;
export default N;
```
Used when a module contains only a single function or class.
To import it:
```js
import f from './mymodule1.mjs';
```

Re-exporting:
```js
export {a, foo as f} from './another_module.mjs';
export * from './another_module.mjs';
export * as am from './another_module.mjs'; // All exported object are put inside namespace `am`.
```

We import the whole content of the module in another file:
```js
import * as mm from './mymodule1.mjs';
import {foo} from './mymodule1.mjs';
import {foo as f} from './mymodule1.mjs'; // `foo` is renamed as `f`.
```

### Dynamic import

Dynamic import:
```js
import('./mymodule1.mjs').then((ns) => {
    ns.foo();
});
```

Useful inside browser to react on events:
```js
button.addEventListner('click', event => {
  import('./dialogBox.mjs')
    .then(dialogBox => {
      dialogBox.open();
    })
    .catch(error => {
      /* Error handling */
    })
});
```

Loading asynchronously a module:
```js
const mm = await import(`./mymodule${param}.mjs`);
```

Use a module fallback:
```js
let mm1;
try {
    mm1 = await import('https://server1/mymodule1.mjs');
} catch {
    mm1 = await import('https://server2/mymodule1.mjs');
}
```

Use fastest to load:
```js
const mm1 = await Promise.any([
  fetch('https://server1/mymodule1.mjs').then(response => response.text()),
  fetch('https://server2/mymodule1.mjs').then(response => response.text()),
]);
```

### Script system in ES5

In the HTML file:
```html
<script src="mymodule1.js"></script>
<script src="mymodule2.js"></script>
```

Inside one `mymodule2.js`:
```js
var myModule2 = (function () { // Open IIFE

    // Import
    var foo = myModule1.foo;

    function a() {
    }

    function b() {
    }


    // Export
    return {
        a: a,
        b: b,
    };
})(); // Close IIFE
```
IIFE = Immediately Invoked Function Expression.

### CommonJS module

A module `mymodule2.js`:
```js
// Import
var foo = require('./mymodule1.js').foo;

function a() {
}

function b() {
}

module.exports = {
  a: a,
  b: b,
};
```

### AMD module

Most popular implementation: `RequireJS`.

A module `mymodule2.js`:
```js
define(['./mymodule1.js'],
  function (mm1) {

    // Import
    var foo = mm1.foo;

    function a() {
    }

    function b() {
    }

    return {
      a: a,
      b: b,
    };
});
```

## npm packages

 * [npm packages](https://exploringjs.com/impatient-js/ch_modules.html#npm-packages).

A package is a directory with:
 * `.mjs` files.
 * One `package.json` file.

## OOP

 * [Classes](https://exploringjs.com/impatient-js/ch_classes.html).

Classes are functions.

Class inheritance is implemented as prototype chains.

An example of a class:
```js
class A {
    #p; // Private fields must be declared
    #q = 120; // Private fields may be assigned a value when declared
    x = 0;   // A public field, declared and set outside the constructor
    'y' = 1; // Another public field, declared using a string for the key.
    ['n_' + z] = 2; // A public field, with a computed key.
    [someSymbol] = 3; // Another computed key using a Symbol value.
    constructor(n) {
        this.n = n; // n is a property
        this.#p = 'abc'; // #p is a private field
    }
    describe() {
        return `An object of class A where n=${this.n}`;
    }

    // Accessors
    get myfield() {}
    set myfield(value) {}
    get #myfield() {} // Private getter
    set #myfield(value) {} // Private setter
    
    // Methods
    foo() {} // Synchronized method
    async foo() {} // Asynchronized method
    #foo() {} // Private synchronized method
    async #foo() {} // Private asynchronized method

    // Generators
    * gen() {} // Synchronized generator method
    async * gen() {} // Asynchronized generator method
    * #gen() {} // Private synchronized generator method
    async * #gen() {} // Private asynchronized generator method

    // Using strings as keys
    get 'myfield'() {}
    async 'foo'() {}

    // Using computed keys
    get ['my' + 'field']() {}
    get [someSymbol]() {}
}
```

Extends a class:
```js
class B extends A {
    constructor(n, x) {
        super(n);
        this.x = x;
    }
    describe() {
        return super.describe() + `, x=${this.x}`;
    }
}
```

Create a new instance:
```js
const obj = new B(10, 5.4);
obj instanceof B; // Returns true
obj instanceof A; // Returns true
```

Static method:
```js
class A {
    static foo() {
        // ...
    }
}

A.foo();
```

Class expression:
```js
const B = class {...};           // Anonymous class expression
const B = class SomeClass {...}; // Named class expression
```

### Class internals

A Class is implemented as two objects.

Let us take the following class definition:
```js
class A {

    constructor(n) {}

    describe() {
        return "..."; // A description of the instance.
    }

    static someStaticMethod() {}
}
```

The first object is the class object `A` itself.
It has at leat 3 properties:
 1. `length`: The number of arguments of the constructor (`1` in our case).
 2. `name`: The name of the class (`A` in our case).
 3. `prototype`: points to object where functions are defined as properties (`constructor`, `describe`).
 4. `someStaticMethod`: The static method we have defined in `A`.
 5. Other static methods...

All instances of a class, have their special field `__proto__` set with the
value of the class' field `prototype`.
That means they all have the same prototype object defined by the class.

The `constructor` property/method of the prototype, points in fact to the class
itself.
We can thus create a new object, using the constructor from an object's
prototype:
```js
const new_obj = new myobj.constructor();
```
We can also get the name of the class:
```js
const class_name = myobj.constructor.name;
```

### Methods calling

A *dispatched* method call as `obj.foo()` will work as following:
 1. The `obj` instance is searched for the `foo()` method.
 2. If not found, then the prototype of `obj` is searched for the method, and we proceed to prototype parents until to find a `foo()` method.
 3. Once found and store inside a variable `fct`, we call `fct.call(obj)`.

It is also possible to make a direct call to a method:
```js
MyClass.prototype.foo.call(obj);
```

### Before ECMAScript 6

Classes are functions.

Before ES6, functions were used to create classes:
```js
// Class/constructor A
function A(s) {
  this.str = s;
}

// Method of class A
A.prototype.add = function(s) {
    this.str += s;
    return this;
}


// Usage
const s = new A('a');
s.add('b').add('c');
```

### Private slots

Private slot/field:
```js
class A {
    #field;
    constructor() {
        this.#field = 'abc';
    }
}
```
A private field cannot be accessed directly outside the class.
A private field cannot be accessed by subclasses.

Checking if a private slot exists:
```js
if (#myslot in obj) {
}
```

Private method:
```js
class A {
  #foo() {}
}
```

Private static slots:
```js
class A {
  static #x = 1;
  static #foo() {}
}
```

### Construction

Before ES6, JavaScript only defined *constructor functions* for object factories.
*constructor functions* are ordinary functions that return *instances* of themselves when invoked through the *new* operator.
*Classes* were introduced in ES6. They are just a better syntax for *constructor functions*.

*Constructor functions* for primitives are `BigInt`, `Number`, `Boolean`, `String` and `Symbol`.
They:
 * can be used to create a new *primitive value*: `Number(123)`.
 * can be used to create an *object* with the `new` operator: `new Number(123)` returns a `Number` object.
 * provide properties: `Number(123).toString`. Properties are stored inside `Number.prototype`.
 * are used as namespaces for tool functions: `Number.isInteger(123)`.

Conversion between types:
```js
x = Number('123.45');
o = Object(true); // `o` is a `Boolean` instance, not a Boolean primitive.
```

### Static members

 * [Static members of classes](https://exploringjs.com/impatient-js/ch_classes.html#static-members-of-classes).

```js
class A {
  static n = 1;
  static 'm' = 2;
  static [x] = 3;
  static #p = 10;
  static foo() { return A.#p; } // In order to work for sub-classes we must not use `this` for accessing private members.
  static get field() {}
  static set field(value) {}

  static { // Static code block, executed when the class is created.
    this.#p = compute_p(); // `this` refers to the class object, not an instance of the class.
  }
}

i = A.n;
j = A['m'];
k = A[x];
A.foo();
x = A.field;
A.field = x;
```

### Mixins

 * [Mixin classes](https://exploringjs.com/impatient-js/ch_classes.html#mixin-classes-advanced).

```js
const S1 = (Sup) => class extends Sup { /*...*/ };
const S2 = (Sup) => class extends Sup { /*...*/ };

class C extends S2(S1(Object)) {
  /*...*/
}
```

### Introspection

Get the class of an object:
```js
myobj.constructor.name
```

See `instanceof` to test the class of an object.

### Simulating private, protected and friend visibility with WeakMap global objects

 * [ES6 and later: private instance data via WeakMaps](https://exploringjs.com/impatient-js/ch_classes.html#es6-and-later-private-instance-data-via-weakmaps).

We may also handle private data using global WeakMap objects:
```js
const _p = new WeakMap(); // A global map that will contain all private fields `p` of all instances.

class A {
  constructor(p) {
    _p.set(this, p);
  }
}
```
The `_p` object is only visible inside the module where it is defined as long as it is not exported,
thus offering a better protection from outside access.
We may even protect its access from other part of its module, by declaring it
at class scope:
```js
let A;
{ // Class scope
  const _p = new WeakMap();

  class A {
    constructor(p) {
      _p.set(this, p);
    }
  }
}
```

We cannot implement private methods the same way, but we may use module
functions (not exported):
```js
function foo(_this) {
  let p = _p.get(_this);
  // modify p
  _p.set(_this, p);
}
```

We can simulate *protected* visibility by putting the class and all its
subclasses inside the **same module**.
Similarly we can simulate *friend* visibility by putting the class and all its
friends inside the **same module**.

## Exceptions

 * [Exception handling](https://exploringjs.com/impatient-js/ch_exception-handling.html).

Throw and catch an exception:
```js
try {
    throw new Error('An error occured.');
} catch (err) {
    if (err instanceof Error) {
        // ...
    }
} finally {
    // Always executed after `try` or `catch`, event if there is a `return` statement.
}
```

Chaining errors:
```js
try {
    // ...
} catch(err) {
    throw new Error(`Error at line ${n} of file ${file}`, {cause: err});
}
```

`Error` is the top exception class.
See [The built-in subclasses of Error](https://exploringjs.com/impatient-js/ch_exception-handling.html#error-subclasses) for its subclasses:
 * `AggregateError`
 * `RangeError`
 * `ReferenceError`
 * `SyntaxError`
 * `TypeError`
 * `URIError`

We may also add properties dynamically:
```js
const err = new Error('My message');
err.n = 100;
throw err;
```

We may also subclass `Error` ourselves:
```js
class MyError extends Error {
    constructor(message, options) {
        super(message, options);
    }
}
```

Any value can be thrown in JavaScript.

Getting the call stack:
```js
const error = new Error();
console.log(error.stack);
```

## eval

Evaluate code in a string:
```js
eval('let a=1;');
```
The code is evaluated inside the *current* context.

However evaluating inside the *global* context is safer, the code having access to fewer internals.
Here are the way to evaluation the code in the *global* context:
```js
eval.call(undefined, ...);
eval?.()();
(0, eval)(...);
globalThis.eval(...);
const e = eval; e(...);
```

## instanceof

Checks if array:
```js
myvar instanceof Array
```

## typeof

Get the type of a variable or expression:
```js
typeof(myvar);
```

| type tested   | `typeof` return value |
| ---           | ---                   |
| `undefined`   | `'undefined'`         |
| `null`        | `'object'`            |
| `Boolean`     | `'boolean'`           |
| `Number`      | `'number'`            |
| `Bigint`      | `'bigint'`            |
| `String`      | `'string'`            |
| `Symbol`      | `'symbol'`            |
| `Function`    | `'function'`          |
| Other objects | `'object'`            |

Note the mistakes for `null` and `Function`, the former should gives `'null'` and the latter `'object'`.
These were errors that can no more be changed in the language.

## Date

 * [Dates](https://exploringjs.com/impatient-js/ch_dates.html).

## JSON

 * [Creating and parsing JSON](https://exploringjs.com/impatient-js/ch_json.html).

Converts nested objects into a JSON string:
```js
let s = JSON.stringify({a:12, b:-1.4});
```

## Math

 * [17 Math](https://exploringjs.com/impatient-js/ch_math.html).

The `Math` object provides miscellaneous functions.

| Property       | Description |
| ---            | ---         |
| `Math.E`       |             |
| `Math.LN10`    |             |
| `Math.LN2`     |             |
| `Math.LOG10E`  |             |
| `Math.LOG2E`   |             |
| `Math.PI`      |             |
| `Math.SQRT1_2` |             |
| `Math.SQRT2`   |             |

| Function        | Description |
| ---             | ---         |
| `Math.floor()`  |             |
| `Math.ceil()`   |             |
| `Math.round()`  |             |
| `Math.trunc()`  |             |
| --------------  | ----------- |
| `Math.abs()`    |             |
| `Math.clz32()`  |             |
| `Math.max()`    |             |
| `Math.min()`    |             |
| `Math.random()` |             |
| `Math.sign()`   |             |
| --------------  | ----------- |
| `Math.cbrt()`   |             |
| `Math.exp()`    |             |
| `Math.expm1()`  |             |
| `Math.log()`    |             |
| `Math.log1p()`  |             |
| `Math.log10()`  |             |
| `Math.log2()`   |             |
| `Math.pow()`    |             |
| `Math.sqrt()`   |             |
| --------------  | ----------- |
| `Math.acos()`   |             |
| `Math.acosh()`  |             |
| `Math.asin()`   |             |
| `Math.asinh()`  |             |
| `Math.atan()`   |             |
| `Math.atanh()`  |             |
| `Math.atan2()`  |             |
| `Math.cos()`    |             |
| `Math.cosh()`   |             |
| `Math.hypot()`  |             |
| `Math.sin()`    |             |
| `Math.sinh()`   |             |
| `Math.tan()`    |             |
| `Math.tanh()`   |             |

## RegExp

 * [Regular expressions](https://exploringjs.com/impatient-js/ch_regexps.html).
 * [Syntax](https://exploringjs.com/impatient-js/ch_regexps.html#syntax).

Static regex (compiled at load time):
```js
/abc/ui;
```

Dynamic regex (compiled at runtime):
```js
new RegExp('abc', 'ui');
new RegExp(my_regex, ''/*new flags*/); // Clone an existing regex
```

Match groups:
```js
const groups = /([0-9]+) (.*)/d.exec(myString);
a = groups[1];
b = groups[2];
```

## Javascript inside HTML

Use the `<script>` tag to include javascript code inside HTML:
```html
<html>
    <head>
        <title>
            JavaScript Example
        </title>
        <script type="text/javascript">
            <!-- // to hide script contents from old browsers
            some javascript code...
            // end hiding contents from old browsers  -->
        </script>
    </head>
    <body>
    </body>
</html>
```

### document

Define a function to run when the document is ready:
```js
$(document).ready(function() {
    ...
}
```
See [$( document ).ready()](https://learn.jquery.com/using-jquery-core/document-ready/).

```js
document.write(Message);
```

### Redirect

```js
window.location = "http://www.lemonde.fr/"
```

### Elements

Get an element property:
```js
document.getElementById("menu1").offsetHeight;
```

Possible properties:
    className (nom de classe de feuille de style d'un élément)
    dataFld (champ de données pour la liaison de données)
    dataFormatAs (type de données lors d'une liaison de données)
    dataPageSize (nombre d'enregistrements lors d'une liaison de données)
    dataSrc (source de données lors d'une liaison de données)
    id (nom d'identification d'un élément)
    innerHTML (contenu HTML d'un élément)
    innerText (contenu texte d'un élément)
    isTextEdit (possibilité d'éditer un élément)
    lang (langue d'un élément)
    language (langage Script pour un élément)
    length (nombre d'éléments)
    offsetHeight (hauteur d'un élément)
    offsetLeft (valeur gauche du coin supérieur gauche)
    offsetParent (position de l'élément parent)
    offsetTop (valeur du haut du coin supérieur gauche)
    offsetWidth (largeur d'un élément)
    outerHTML (contenu d'un élément avec contexte HTML)
    outerText (contenu texte d'un élément avec texte)
    parentElement (élément parent)
    parentTextEdit (possibilité d'éditer un élément parent)
    recordNumber (numéro d'enregistrement lors d'une liaison de données)
    sourceIndex (numéro d'ordre d'un élément)
    tagName (repères HTML de l'élément)
    title (titre de l'élément)

Methods:
    click() (cliquer sur l'élément)
    contains() (chaîne de caractères contenue dans l'élément)
    getAttribute() (rechercher l'attribut d'un élément)
    insertAdjacentHTML() (insérer un élément)
    insertAdjacentText() (insérer du texte)
    removeAttribute() (retirer l'attribut de l'élément)
    scrollIntoView() (faire défiler à l'élément)
    setAttribute() (insérer l'attribut d'un élément)
