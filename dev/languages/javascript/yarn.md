# yarn

 * [yarn](https://classic.yarnpkg.com/).
 * [CLI Introduction](https://yarnpkg.com/en/docs/cli/).

Dependency management.

List installed modules:
```sh
yarn list
```
