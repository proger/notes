#!/bin/sh
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

class BinaryTree {

  constructor(value, left=null, right=null) {
    this.value = value;
    this.left = left;
    this.right = right;
  }
  
  * [Symbol.iterator]() {
    yield this.value;
    
    if (this.left)
      yield* this.left; // = this.left[Symbol.iterator]()

    if (this.right)
      yield* this.right;
  }
}

const tree = new BinaryTree(1, new BinaryTree(2), new BinaryTree(3, null,
  new BinaryTree(4)))

for (const x of tree)
  console.log(x);
