# Node.js

 * [Node.js](https://nodejs.org/).

Javascript runtime environment.
Includes the `npm` package manager.

Install:
```sh
apt install nodejs npm # On Debian/Ubuntu
```

Interactive interpreter:
```sh
node
```
Exit with `Ctrl-C` twice or with `.exit`.
