# npm

 * [npm Docs](https://docs.npmjs.com/).
 * [package.json](https://docs.npmjs.com/cli/v9/configuring-npm/package-json).
 * [Versions](https://github.com/npm/node-semver#versions).

Package manager for Node.js.

Install modules from `package.json` file:
```sh
npm install # Install in $HOME/node_modules
# or to install globally:
npm install -g 
```
Latest version (which version?) of npm install all dependencies of all workspaces.

Set heap size:
```sh
NODE_OPTIONS=--max_old_space_size=8192 npm install
```

Update npm:
```sh
npm i -g npm@latest
```

Force install when conflict of versions exist between dependencies of workspaces and root:
```sh
npm install --force
```

List installed modules:
```sh
npm list
```
