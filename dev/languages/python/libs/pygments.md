# pygments

Module and program (`pygmentize`) for syntax highlighting.

List all stylesheets:
```sh
pygmentize -L styles
```

List all handled languages:
```sh
pygmentize -L lexers
```

List all styles:
```sh
pygmentize -L styles
```

Highlight syntax of a file for a console with 256 colours, choosing a style:
```sh
pygmentize -O style=native -f console256 setup.py
```
