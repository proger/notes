# rrdtool

 * [Round Robin Database tool library](https://pythonhosted.org/rrdtool/index.html).

Resize an RRA inside an RRD file:
```python
rrdtool.resize("myfile.rrd", str(rra_index), "GROW", str(9)) # Add 9 rows. Output to "resize.rrd" file.
rrdtool.resize("myfile.rrd", str(rra_index), "SHRINK", str(8)) # Remove 8 rows. Output to "resize.rrd" file.
```
See <https://en.wikipedia.org/wiki/RRDtool>.
