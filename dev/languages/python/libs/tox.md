# tox

Automate Python code testing.

 * [TOX home page](https://testrun.org/tox/latest/).

To install:
```bash
pip install tox
```
or
```bash
easy_install tox
```

To launch tests, you need a `tox.ini` file and run:
```bash
tox
```

Example of `tox.ini` file:
```dosini
[tox]
envlist = py38, py39

[testenv]
deps = pytest
commands = pytest -v
```

Run in verbose mode:
```sh
tox -v
# or
tox -vv
```

Recreate virtual environment:
```sh
tox -r
```

Tun run tests for only one Python version:
```bash
tox  -e py38
```

To run tests in only one file:
```bash
tox  -e py38 -- tests/my_tests.py
```

To run tests in only one class or function:
```bash
tox  -e py38 -- tests/my_tests.py::MyClass
tox  -e py38 -- tests/my_tests.py::myFct
```

To run tests in only one class' method:
```bash
tox  -e py38 -- tests/my_tests.py::MyClass::myMethod
```
