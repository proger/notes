# locale

```python
import locale
locale.setlocale(locale.LC_ALL, ’English_United States.1252’)
conv = locale.localeconv() # get a mapping of conventions
```

Set locale using value of `LANG` env var:
```python
import locale
locale.setlocale(locale.LC_ALL, '')
```

Printing an integer number:
```python
locale.format("%d", x, grouping=True)
```

Printing an amount of money:
```python
locale.format("%s%.*f",
		(conv[’currency_symbol’], conv[’frac_digits’], x),
		grouping=True)
```
