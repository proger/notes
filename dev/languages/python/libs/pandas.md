# pandas
<!-- vimvars: b:markdown_embedded_syntax={'python':''} -->

 * [Pandas](https://pandas.pydata.org/).
 * [Data frames](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.html).
 * [Indexing and selecting data](https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html).
  + [Returning a view versus a copy](https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy).

Import:
```python
import pandas
```

Create data frame from a dictionary:
```python
df = pandas.DataFrame({'a':[0,0,0,0,0]})
```

Create data frame from a numpy array:
```python
df = pandas.DataFrame(numpy.array([[0],[0],[0],[0],[0]]), columns=['a'])
```

Read from CSV:
```python
df = pandas.read_csv(myfile, sep='\t', index_col=False)
```

Write to CSV:
```python
df.to_csv(myfile, sep='\t', index=False)
```

Test if a data frame contains a column:
```python
if mydf.keys().contains('mycol'):
	pass
```
or
```python
if 'mycol' in mydf:
	pass
```

Get a column:
```python
mydf['mycol'] # Returns a NumPy array.
```

Get number of rows of data frame:
```python
n = len(df.index)
```

Get number of columns of data frame:
```python
n = len(df.columns)
```

Get column names (as a `pandas.Index`):
```python
df.columns
```
As a `list`:
```python
df.columns.to_list()
```

Modify an element of a data frame:
```python
df.loc[i, j] = 1.0
df.loc[i, 'mycolname'] = 1.0
```

Add new column:
```python
df['mynewcol'] = 1
```

Iterate over rows:
```python
for index, row in df.iterrows():
    x = row['a'] + row['b']
```

Print data frame column types:
```python
df.dtypes
```

Test for NaT (Not a Type):
```python
for index, row in df.iterrows():
	if pandas.isnull(row['a']):
		# ...
```

Sort the rows of a data frame:
```python
df = df.sort_values('mycol', ascending=True)
df = df.sort_values(['mycol1', 'mycol2'], ascending=[True, False])
```

Sort on all columns:
```python
df = df.sort_values(list(df.columns))
```

Sort columns order:
```python
df = df.reindex(sorted(df.columns), axis=1)
```

Test if two dataframes are equal:
```python
df1.equals(df2)
```

Compare two dataframes for testing:
```python
pandas.testing.assert_frame_equal(a, b, check_exact=False)
```
