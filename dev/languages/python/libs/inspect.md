# inspect

Get the names of the arguments of a class' constructor:
```python
import inspect
class_args = inspect.getfullargspec(MyClass).args
```

Get the arguments of a method:
```python
fct_args = inspect.getfullargspec(myFunction).args
```

Tests if a class is abstract:
```python
inspect.isabstract(MyClass)
```
