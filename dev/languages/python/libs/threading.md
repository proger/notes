# threading

 Threads run with this module are slowed down by the [GIL](https://docs.python.org/2/glossary.html#term-global-interpreter-lock), which authorize only one thread at a time to run for accessing OS API or Python objects. Prefer the multiprocessing module, which is as simple to use and is not submitted by the GIL since it runs processes and not threads.

 * [threading — Thread-based parallelism](https://docs.python.org/3/library/threading.html).

```python
import threading

class MyThread(threading.Thread):
	def run(self):
		# ...

thread_instance = MyThread()
thread_instance.start()
#_ ...
thread_instance.join()
```

The `threading` module provides: locks, events, condition variables and semaphores.

An easy way to synchronize threads on data is to use the queue module.
