# dask (HPC)

 * [Dask](https://dask.org/).

Scale python libs like NumPy, pandas, and scikit-learn.
Dask deploys on Kubernetes, cloud, or HPC.
