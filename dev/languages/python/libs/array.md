# array

Arrays are not available by default. They are plain C-arrays, provided by the array standard library:
```python
from array import array
```

Create an array from a range:
```python
range(start, stop[, step])    # step = 1 by default
```
or
```python
range(length) --> = range(1, length, 1)
```

Create an array of 2 byte unsigned integers (instead of the usual 16 byte int objects):
```python
a = array(’H’, [4000, 10, 700, 22222])
```

Get last element:
```python
a[-1]
```

Get size/length:
```python
n = len(A)
```

Summing an array:
```python
s = sum(a)
```

Summing two arrays of same length:
```python
[A[i]+B[i] for i in range(len(A))]
```

max/min:
```python
m = max(a)
n = min(a)
```

Append:
```python
a.append(4)
```

Pop elements:
```python
a.pop()     # pop last item
a.pop(3)    # pop 3 last items
a.pop([i])  # remove and return element i
```

Replace range of elements:
```python
a[i:j] = x
a[i:j] = [1, 10, 20]
```

Remove elements or range of elements:
```python
a[i:j] = []
del a[i:j]
```

Reverse array:
```python
a.reverse()
```

Insert element:
```python
s.insert(i, x)
s[i:i] = x
```

Count elements:
```python
s.count(x)      # return number of elements such that s[i]==x
```

Search for an element:
```python
s.index(x[, i[, j]])    # return smallest k such that s[k] == x and i <= k < j
```
