# PyQt
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

Install on MacOS-X with Homebrew:
```bash
brew install pyqt
```

Hello World sample:
```python
from PyQt4 import Qt

app=Qt.QApplication(["Hello World App"])
hello = Qt.QLabel("Hello, World") # we must store the widget in a variable because it's a TOP widget, otherwise it will be destroyed when reaching app.exec_() call
hello.show()
app.exec_()
```
