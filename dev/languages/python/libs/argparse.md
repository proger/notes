# argparse
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

 * [argparse](https://docs.python.org/3/library/argparse.html).
 * [rich-argparse](https://github.com/hamdanal/rich-argparse).

Parse command line.

```python
import argparse
```

Create parser:
```python
parser = argparse.ArgumentParser(description='Some description.')
```
The description text is printed at the beginning of the help page.

Add an epilog text that will be printed at the end of the help page.
```python
parser = argparse.ArgumentParser(description='Some description.',
    epilog='Some end note.')
```

Allow the use of new line characters inside the description text:
```python
parser = argparse.ArgumentParser(description='First line\nsecond line\n.',
    formatter_class=argparse.RawDescriptionHelpFormatter)
```

Print the default values of arguments in the help page:
```python
parser = argparse.ArgumentParser(description='Some description.',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
```

Combine two formatters:
```python
class MyFormatter(argparse.RawDescriptionHelpFormatter,
        argparse.ArgumentDefaultsHelpFormatter):
    pass
parser = argparse.ArgumentParser(description='First line\nsecond line\n.',
        formatter_class=MyFormatter)
```

Add an optional positional argument:
```python
parser.add_argument('box_file', type=str, help="Box file.", nargs="?")
```
`nargs` can be set to `"*"` (>=0), `"+"` (>=1), `?` (0 or 1) or an integer `1`, `2`, etc.

Add a required optional argument:
```python
parser.add_argument('-p', dest='cell_prefix', required=True)
    help='prefix for cell names')
```

Ask for multiple values for an option argument:
```python
parser.add_argument('-a', dest='mylist', nargs='*')
```
0 or more values can be set after the flag:
    myprog -a value1 value2 value3 ...

Allow to use multiple times the same flag to add multiple values:
```python
parser.add_argument('-a', dest='mylist', action='append')
```
0 or more values can be set this way:
    myprog -a value1 -a value2 -a value3 ...

Add a flag:
```python
parser.add_argument('-g', '--debug',  help='Set debug mode.', dest='debug',
    action='store_true')
```

Count multiple use of the same argument:
```python
parser.add_argument('-v', dest='verbose', action='count',
    help='Set verbose level.')
```

| Argument | Description
| -------- | ---------------------
| metavar  | The name of the argument in the help message.
| help     | The help message.
| nargs    | Number of time the argument value may appear: `"?"` means 0 or 1, `"*"` means 0 or plus, `"+"` means 1 or plus, or a number for an exact number of times.

Parsing the arguments:
```python
args = parser.parse_args()  # args is an object of type argparse. To access an argument type args.myargname
args_dict = vars(args)      # transform args into a dictionary. To access an argument type args_dict['myargname']
```

Get remaining arguments after `--`:
```python
parser = argparse.ArgumentParser()
parser.add_argument('remaining', nargs=argparse.REMAINDER)
args = parser.parse_args()
# args.remaining contains ['--', ...]
```

Define a group:
```python
dest = parser.add_argument_group("DESTINATION")
dest.add_argument(...)
```

## "Dash in value" issue

 * [Can't get argparse to read quoted string with dashes in it?](https://stackoverflow.com/questions/16174992/cant-get-argparse-to-read-quoted-string-with-dashes-in-it)

`argparse` cannot parse correctly a command line when a value starts with a dash:
```sh
myprog -z -myvalue
```
`-myvalue` will be interpreted as a flag, because `argparse` works in two phases:
 1. First detect all option flags (all arguments that start with `-` or `--`).
 2. Then look for positional arguments.

The work arround is for the caller to use the following sort notation:
```sh
myprog -z-myvalue
```
Or the long notation with the equal sign:
```sh
myprog --myflag=-myvalue
# or
myprog --myflag='-myvalue'
```
