# Flask

A micro framework for developing a web site.

To make web site available externally, set host to `0.0.0.0`:
```python
import flask
app = flask.Flask('MyApp', static_folder='static', static_url_path='')
app.run(host="0.0.0.0")
```
