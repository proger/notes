# uFlash

 * [uFlash](https://uflash.readthedocs.io/en/latest/).

Tool to flash python program onto the micro:bit board.

Install:
```sh
pip install uflash
```

Usage:
```sh
uflash myprg.py
```
