# random
<!-- vimvars: b:markdown_embedded_syntax={'python':''} -->

 * [random — Generate pseudo-random numbers](https://docs.python.org/3/library/random.html).

```python
import random
```

Random seed:
```python
random.seed()
```

Fixed seed:
```python
random.seed(0)
```

Integer:
```python
i = random.randint(1, 1000) # in [1, 1000]
i = random.randrange(1, 1000) # in [1, 1000)
```
Float:
```python
x = random.random() # in [0.0, 1.0)
```

Shuffle a list:
```python
random.shuffle(mylist)
```

Create list with random numbers:
```python
import random
my_randoms = random.sample(range(100), 10)
```
