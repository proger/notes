# NumPy
<!-- vimvars: b:markdown_embedded_syntax={'python':''} -->

Import module:
```python
import numpy
```

NaN value:
```python
x = numpy.nan
```

Test if NaN:
```python
numpy.isnan(x)
```

Sort an array:
```python
my_sorted_array = numpy.sort(my_arr)
```

Get unique values in an array:
```python
my_uniq_elem = numpy.unique(my_arr)
```

Element-wise comparison of two arrays:
```python
numpy.equal(array_1, array_2)
```

Compute absolute value:
```python
numpy.abs(x)
```

Compute mean:
```python
x = numpy.array([[1, 2], [3, 4]])
numpy.mean(x) # Mean on all values
numpy.mean(x, axis=0)
numpy.mean(x, axis=1)
```

## Arrays

 * [numpy.ndarray](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html#numpy.ndarray).

The numpy array class is `numpy.ndarray`:
```python
if isinstance(x, numpy.ndarray):
	pass
```

Convert an array into a list:
```python
my_list = my_array.tolist()
my_list = list(my_array)
```

To build an array of arrays:
```python
a = []
# Append all arrays to list a.
# ...
# Convert list a to array:
a = numpy.asarray(a)
```

Test that all values of a boolean array are TRUE:
```python
numpy.all(myarr)
```

Test that at least one value of a boolean array is TRUE:
```python
numpy.any(myarr)
```

Get the total number of elments of an array (i.e.: product of all dimensions):
```python
myarr.size
```

Get the number of dimensions of an array:
```python
myarr.ndim
```

Get the shape:
```python
myarr.shape
```

Create an empty array (i.e.: from nothing):
```python
numpy.empty(12) # Array of 12 elements
```

Create an array from a list:
```python
numpy.array([1, 2, 3, 4])
```

Flatten an array (turns it into a 1D tensor):
```python
x.ravel()
```

## Assert

Check that two arrays are equal:
```python
numpy.testing.assert_array_equal(x, y)
```

Check that two arrays are almost equal:
```python
numpy.testing.assert_allclose(x, y)
numpy.testing.assert_almost_equal(a, b)
numpy.testing.assert_almost_equal(a, b, decimal=9)
```
