# microbit

 * [micro:bit Micropython API](https://microbit-micropython.readthedocs.io/en/v1.1.1/microbit_micropython_api.html).

Install:
```sh
python -m pip install microbit-3
```
