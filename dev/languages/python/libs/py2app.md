# py2app

Program that builds a macOS application bundle.
See [py2app](https://pythonhosted.org/py2app/).

Make `setup.py`:
```bash
py2applet --make-setup MyApplication.py
```

Example of `setup.py` file:
```python
#!/usr/bin/env python
from setuptools import setup
import py2app

setup(
	app=['myApp.py'],
	options=dict(
	    py2app=dict(
	        packages=['mechanize'],
	        site_packages=True,
	    ),
	),
)
```

Clean up build directories:
```bash
rm -rf build dist
```

Building with alias mode builds an application bundle that uses your source and data files in-place:
```bash
python setup.py py2app -A
```

Build for deployment:
```bash
python setup.py py2app
```
---> ERROR
	> /opt/local/Library/Frameworks/Python.framework/Versions/2.6/lib/python2.6/site-packages/macholib/ptypes.py(48)from_str()
	-> return cls.from_tuple(struct.unpack(endian + cls._format_, s), **kw)
	(Pdb)

Running the application:
```bash
./dist/MyApplication.app/Contents/MacOS/MyApplication
```
or
```bash
open -a dist/MyApplication.app
```

To see stdout and stderr, open the console:
```bash
open -a Console
```
