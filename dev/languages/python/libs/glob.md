# glob

 * [glob — Unix style pathname pattern expansion](https://docs.python.org/3/library/glob.html).

Glob:
```python
import glob
glob.glob('./[0-9].*')
```
