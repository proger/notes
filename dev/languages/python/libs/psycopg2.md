# psycopg2

PostgreSQL connector module.

 * [psycopg2](https://pypi.org/project/psycopg2/).
 * [Python PostgreSQL Tutorial Using Psycopg2](https://pynative.com/python-postgresql-tutorial/).

Open a connection:
```python
try:
	conn = psycopg2.connect(host=self.db_server, database=self.db_name, user=self.db_user, password=self.db_password, port=self.db_port)
	do_something()
finally:
	conn.close()
```

Send a modification request:
```python
cur = conn.cursor()
cur.execute("alter table mytable add mycol integer;")
conn.commit()
cur.close()
```

Run a select request:
```python
cur = conn.cursor()
cur.execute("select * from mytable;")
do_something()
cur.close()
```

Iterator over all records of a cursor:
```python
for record in cur:
	do_something()
```

Fetch all records from a cursor:
```python
single_record = cur.fetchall()
```

Fetch n records from a cursor:
```python
single_record = cur.fetchmany(4)
```

Fetch a single record:
```python
single_record = cur.fetchone()
```
