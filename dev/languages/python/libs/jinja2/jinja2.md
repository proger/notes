# Jinja
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','python':'','jinja':''} -->

 * [Jinja](https://jinja.palletsprojects.com/).

To run on command line, install `jinja2-cli`:
```sh
python3 -m pip install jinja2-cli
```

## Running

Run with a template file and a JSON data file:
```sh
jinja2 helloworld.tmpl data.json
```

Use stdin to read data file:
```sh
cat data.json | jinja2 helloworld.tmpl
```

Set the format of the data file:
```sh
jinja2 helloworld.tmpl data.json --format=json
```
Formats available: json, yaml, xml, toml, ...

Redirect to a file:
```sh
jinja2 helloworld.tmpl data.json --format=json -o myfile.txt
```

## Library

```python
import jinja2

temp = jinja2.Template("Hello {{ firstname }} !")
print(temp.render({'firstname': 'John'}))
```

Loop on a list and quote each item, print them separated by commas:
```jinja
blabla: [{% for host in es_hosts %}{% if not loop.first %}, {% endif %}"{{ host }}"{% endfor %}]
```

## Coments

```jinja
{# My comment #}
```

## Dictionary

Use key if defined in dict, otherwise use default value:
```jinja
{{ mydict.mykey | default('mydefaultvalue') }}
```

## Ternary condition

```jinja
X: {{ a if a else 'FOO' }}.
```
