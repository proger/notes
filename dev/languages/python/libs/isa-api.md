# isa-api

 * [ISA-tools/isa-api](https://github.com/ISA-tools/isa-api).

I have written the `isatab2w4m` module in this library.

Run tests:
 * See <https://isa-tools.org/isa-api/content/installation.html#running-tests>.
 * See in tox.ini which version of Python is used for testing (3.7 the 2022-05-04).
 * Run `get_test_data.sh` to download the test files.
 * Use `pyenv` to select the Python version (or `module` tool).
 * Create a virtual environment, install the packages and run the tests:
```sh
python3 -m venv myvenv
. myvenv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
python3 -m unittest tests/test_isatab2w4m.py
```
