# pydocstyle

Checks Docstring documentation style in source files:
```sh
pydocstyle myfolder
```
