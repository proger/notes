# PyTorch
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

 * [torch.nn](https://pytorch.org/docs/stable/nn.html).
 * [Welcome to PyTorch Tutorials](https://pytorch.org/tutorials/index.html).
   + [Deep Learning with PyTorch: A 60 Minute Blitz](https://pytorch.org/tutorials/beginner/deep_learning_60min_blitz.html).

 * [Saving and Loading Models](https://pytorch.org/tutorials/beginner/saving_loading_models.html).

 * [Text classification with the torchtext library](https://pytorch.org/tutorials/beginner/text_sentiment_ngrams_tutorial.html).

Save a PyTorch model:
```sh
torch.save(mymodel.state_dict(), "/my/path/to/my/model/file.pth")
```

Load a PyTorch model:
```sh
model = MyModelClass(*args, **kwargs)
model.load_state_dict(torch.load("/my/model.pth"))
model.eval()
```

## Tensor

 * [Tensors](https://pytorch.org/tutorials/beginner/blitz/tensor_tutorial.html).

Tensor is an array/matrix that can be run on GPUs.

Create:
```python
x = torch.Tensor([1.1, 1.2]) # From 1D list
x = torch.Tensor([[1.1, 1.2], [2.0, 3.4]]) # From 2D list
x = torch.from_numpy(numpy.array([[1.1, 1.2], [2.0, 3.4]])) # From NumPy array
rand_tensor = torch.rand((2, 3,)) # Random tensor 2x3 (rows x cols)
ones_tensor = torch.ones((2, 3,)) # Ones tensor
zeros_tensor = torch.zeros((2, 3,)) # Zeros tensor
x_ones = torch.ones_like(x_data) # Creates a ones tensor with same properties as x_data
x_rand = torch.rand_like(x_data, dtype=torch.float) # Create a random tensor with same properties as x_data with with floats
```

Get info:
```python
tensor.shape  # Dimensions
tensor.dtype  # Data type (float, int, ...)
tensor.device # Device on which resides the tensor (CPU or GPU)
```

Convert to numpy array:
```python
t.numpy() # NumPy arrays can share their memory with tensors on CPU.
x.cpu().detach().numpy()     # TODO cpu().detach()
y = x.detach().cpu().numpy() # TODO or detach().cpu() ?
```

Move a tensor on CUDA GPU:
```python
if torch.cuda.is_available():
  t = t.to('cuda')
```

Set a column to zeros:
```python
t[:,1] = 0
```

Concatenate tensors:
```python
t1 = torch.cat([t, t, t], dim=1)
```

Multiplying tensors:
```python
tensor.mul(tensor) # Element-wise product
tensor * tensor    # Element-wise product
tensor.matmul(tensor.T) # Matrix product
tensor @ tensor.T       # Matrix product
```

In-place operations end with an underscore (`_`):
```python
tensor.add_(5)
tensor.t_()
tensor.copy_(y)
```

Compute a mean:
```python
x = torch.Tensor([[1, 2], [3, 4]])
torch.mean(x)        # Mean on all values
torch.mean(x, dim=0) # Mean of columns
torch.mean(x, dim=1) # Mean of rows
```

Compute the absolute value:
```python
torch.abs(x)
```

Compute the maximum value:
```python
torch.max(x) # --> Returns a tensor containing one scalar.
torch.max(x, dim=0) # Max of columns WARNING Returns a torch.return_types.max()
torch.max(x, dim=0).values # OK, returns a tensor.
torch.max(x, dim=1).values # Max of rows
```

Split tensor, by cutting slices along a dimension:
```python
torch.unbind(x, dim=0) # Returns a tuple of tensors
```

Join tensors along a new dimension:
```python
y = torch.stack([x1, x2, x3, ...], dim=1)
```

Flatten a tensor (turns it into a 1D tensor):
```python
x.ravel()
```

## Autograd

 * [A Gentle Introduction to torch.autograd](https://pytorch.org/tutorials/beginner/blitz/autograd_tutorial.html).

## Neural Networks

 * [Neural Networks](https://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html).

## Training

 * [Training a Classifier](https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html).

## Testing & asserts

Assert two tensors are close:
```python
torch.testing.assert_close(x, y)
```

Assert two tensors are equal:
```python
torch.testing.assert_close(x, y, atol=0.0, rtol=0.0)
```

## ONNX

 * [torch.onnx](https://pytorch.org/docs/master/onnx.html).
  + [Limitations](https://pytorch.org/docs/master/onnx.html#limitations).
 * [Exporting a Model from PyTorch to ONNX and Running it using ONNX Runtime](https://pytorch.org/tutorials/advanced/super_resolution_with_onnxruntime.html).
 * [Transfering a Model from PyTorch to Caffe2 and Mobile using ONNX](https://pytorch.org/tutorials/advanced/super_resolution_with_caffe2.html).
