# bisect

Bisect is a module for working on a sorted list:
```python
import bisect
scores = [(100, ’perl’), (200, ’tcl’), (400, ’lua’), (500, ’python’)]
bisect.insort(scores, (300, ’ruby’))
```
