# pymongo
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

 * [How to Use Python with MongoDB](https://www.mongodb.com/languages/python).
 * [API Documentation](https://pymongo.readthedocs.io/en/stable/api/index.html).
 * [Tutorial](https://pymongo.readthedocs.io/en/stable/tutorial.html).

Search an item by object id:
```python
import pymongo
import bson.objectid

coll = mydbconn['mycoll']
item = coll.findOne({'_id': bson.objectid.ObjectId(myid)})
```

Export a Mongo object in JSON:
```python
import bson.json_util
myjson = bson.json_util.dumps(my_obj_returned_by_Mongo, indent=2)
```
This use of BSON library is necessary because ObjectId objects are specific to
PyMongo and are not converted to JSON by for instance the `json` library.

Several methods have been deprecated in version 4:
The connector instance does not support anymore the `authenticate()` method. All
must be done inside the initializer call:
```python
conn = pymongo.MongoClient(host=, port=, authSource=, authMechanism=, username=,
    password=)
```
On a database instance:
    collection_names    --> list_collection_names
On `Collection` class:
    insert              --> insert_one, insert_many
    save                --> 
    update              --> update_one, update_many, replace_one
    remove              --> delete_one, delete_many
    find_and_modify     --> find_one_and_delete, find_one_and_replace, and find_one_and_update
    ensure_index        --> create_index
