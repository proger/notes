# pegasus-wms.api

 * [Workflow API](https://pegasus.isi.edu/documentation/reference-guide/api-reference.html).
 * [Pegasus.api package](https://pegasus.isi.edu/documentation/python/Pegasus.api.html).

Install with pip:
```sh
python -m pip install pegasus-wms.api
```
