# PyTest
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

 * [pytest: helps you write better programs](https://docs.pytest.org/).

Running tests:
```sh
cd tests
pytest
```

Running with a specific version of Python:
```sh
python3.8 -m pytest
```

Open `pdb` on failure:
```sh
python -m pytest --pdb ...
```
Open `pdb` at the start of each test:
```sh
python -m pytest --trace ...
```

Open `pdb` at the start of one particular function:
```sh
python -m pytest --trace -k test_my_test_fct ...
```

To run with `ipdb`:
```sh
python -m pytest --trace -k test_my_test_fct --pdbcls=IPython.terminal.debugger:Pdb ...
```

To run with `pudb`:
```sh
pytest --pdbcls pudb.debugger:Debugger --pdb --capture=no ...
```

Enable displaying of stdout and stderr:
```bash
pytest -s
```

To skip a test:
```python
@pytest.mark.skip()
def test_something(self):
    # ...
```

To test only one class:
```python
pytest -k MyClass
```

Test classes must not define an `__init__()` method or they will be ignored by PyTest.

Enable logging:
```sh
PYTHONPATH=src python -m pytest --log-cli-level debug tests
```

## Exceptions

```python
import pytest

def test_foo():
    with pytest.raises(ValueError):
        pass
```

## Fixture

 * [pytest fixtures: explicit, modular, scalable](https://docs.pytest.org/en/6.2.x/fixture.html).

Test fixtures are used to initialize test functions.
When calling a test function, if it has parameters then each parameter is set
using the existing fixture function whose name is the same as the parameter.

Example:
```python
@pytest.fixture
def someparam():
    # ...
    return somevalue

def test_something(someparam):
    # ...
```
