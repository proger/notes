# weakref (weak references)

```python
import weakref, gc
```

General explanation of behaviour:
```python
obj = SomeClass()
d = weakref.WeakValueDictionary()
d['akey'] = obj		# doesn't create a reference
d['akey']		# returns the object obj
del obj			# remove the unique reference
gc.collect()		# ensure that the reference and data are removed from memory
d['akey']		# <-- Raises a KeyError exception
```

Creating a single weak reference:
```python
my_weak_ref = weakref.ref(myobj)
```
