# PIL (Python Image Library)
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

```python
from PIL import Image, ImageDraw
```
