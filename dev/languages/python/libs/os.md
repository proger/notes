# os
<!-- vimvars: b:markdown_embedded_syntax={'python':''} -->

 * [os — Miscellaneous operating system interfaces](https://docs.python.org/3/library/os.html).
   + [os.path — Common pathname manipulations](https://docs.python.org/3/library/os.path.html).

## system

 * [`os.system()`](https://docs.python.org/3/library/os.html#os.system).
 * [`os.waitstatus_to_exitcode(status)`](https://docs.python.org/fr/3/library/os.html#os.waitstatus_to_exitcode).

Run a command:
```python
import os
status = os.system("C:\\Temp\\a b c\\Notepad.exe")
assert os.WIFEXITED(status)
assert os.WEXITSTATUS(status) == 0
```

## Environment variables

Get all env vars:
```python
os.environ
```

Get one env var:
```python
os.environ['HOME']
```
Access to array => raises an error if env var does not exist (i.e.: no key).
Use the `get` function, which returns `None` if env var is not defined:
```python
os.environ.get('HOME')
```

Get one env var, using default value instead of `None`:
```python
os.getenv('MYVAR', 'My default value')
```

Set an env var:
```python
os.environ['MYVAR'] = 'My value'
```
`os.putenv()` also modifies an environment variable, but without updating `os.environ`.
On the other hand, the modification of `os.environ` implies a call to `os.putenv()`.
Variables set with one of these ways will be passed to subprocesses.
