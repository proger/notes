# bioservices
<!-- vimvars: b:markdown_embedded_syntax={'python':''} -->

 * [bioservices](https://pypi.org/project/bioservices/).

No unified API to use connectors. See this table of some methods and in which
connectors they are implemented:

| Field or Method    | ChEBI | KEGG | UniProt |
| ------------------ | ----- | ---- | ------- |
| `getUserAgent()`   |       |  X   |    X    |
| `search()`         |       |      |    X    |
| `list()`           |       |  X   |         |
| `requests_per_sec` |   X   |  X   |    X    |

Search in UniProt:
```python
from bioservices import UniProt
u = UniProt(verbose=False)
data = u.search("zap70+and+taxonomy:9606", frmt="tab", limit=3,
	columns="entry name,length,id, genes")
print(data)
```

Retrieve entity from ChEBI:
```python
from bioservices import ChEBI
ch = ChEBI()
res = ch.getCompleteEntity("CHEBI:27732")
print(res.mass)
# The database links are stored as a list of DataItem objects, not easy to
# search for a field:
[x[0] for x in res.DatabaseLinks if print(x[1].startswith("KEGG")])
```

Search in KEGG:
```python
from bioservices.kegg import KEGG
k = KEGG()
k.list("organism")
```
