# sys

Get max integer:
```python
import sys
sys.maxsize
```

Exit from program (better than built-in `quit()` and `exit()`):
```python
import sys
sys.exit(1)
```

Standard streams:
```python
sys.stdin
```

Command line arguments are stored inside sys.argv list:
```python
sys.argv[0] # contains the program's name
```

Search path for modules:
```python
sys.path
```
