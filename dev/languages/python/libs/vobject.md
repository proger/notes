# vobject

 * [vobject](https://eventable.github.io/vobject/).
 * [vobject doc](https://www.adamsmith.haus/python/docs/vobject).

Read vCard and vCal objects from files.

```python
import vobject
```
