# tqdm
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

 * [tqdm](https://tqdm.github.io/).

Display a progress bar when using enumeration.

```python
import tqdm

for x in tqdm.tqdm(myList):
	pass

for i, x in tqdm.tqdm(enumerate(myList))
	pass

for i in tqdm.tqdm(range(100)):
	pass

for i in tqdm.trange(100):
	pass
```

Add a description:
```python
for variant in tqdm.tqdm(variants_generator, desc="Load variants"):
```

Decrease display rate:
```python
for variant in tqdm.tqdm(variants_generator, miniters=None, mininterval=2.0):
```
`mininterval` is in seconds. Setting `miniters` to `None` enables dynamic
computing of `miniters` based on `mininterval`.

Add total number of elements:
```python
for ref_ex in tqdm.tqdm(ref_exons.find(), total=nb_ref_items)
```
