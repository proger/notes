# datetime

 * [datetime — Basic date and time types](https://docs.python.org/3/library/datetime.html).

Import module:
```python
import datetime
```

Parse from string:
```python
d = datetime.datetime.strptime(s, "%d/%m/%Y %H:%M:%S.%f")
```

Convert to timestamp:
```python
d.timestamp()
```
