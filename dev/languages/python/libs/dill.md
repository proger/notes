# dill
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

Export a dictionary:
```python
with open(outputFile, "wb") as file:
	dill.dump(mydict, file)
```

Load a dill file:
```python
with open(inputFile, "rb") as file:
    mydict = dill.load(file)
```
