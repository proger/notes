# itertools

Flatten a list of lists:
```python
list(itertools.chain.from_iterable(range(i, 10*i) for i in range(10)))
```

## product

Build a list of tuples from lists:
```python
product(range(10), range(5))
```

