# gzip

 * [gzip — Support for gzip files](https://docs.python.org/3/library/gzip.html).

Open a `.gz` file for reading:
```python
import gzip
with gzip.open("myfile.gz") as f:
    ...
```
