# SQLAlchemy
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

 * [SQLAlchemy 1.3 Documentation](https://docs.sqlalchemy.org/en/13/).
 * [Working with Engines and Connections](https://docs.sqlalchemy.org/en/13/core/connections.html).

Instantiate an SQLAchemy engine:
```python
address = db_user+':'+db_password+'@'+db_server+':'+db_port+'/'+db_name
engine = sqlalchemy.create_engine('postgresql+psycopg2://'+address)
```

SQLAlchemy can be used with pandas to write a whole table:
```python
mydf.tosql("my_table", sqlachemy_engine, if_exists="replace", ...)
```

To read a table and get a data frame:
```python
mydf = pandas.read_sql_table("my_table", con=engine)
```

To get a data frame from a select request:
```python
mydf = pandas.read_sql("select * from my_table where id = 3", con=engine)
```
