# math

Import module:
```python
import math
```

Ceil:
```python
y = math.ceil(x)	# takes a float and returns a float
```

Square root:
```python
y = math.sqrt(x)
```

Maximum value of a float:
```python
sys.float_info.max
```

Test for NaN value:
```python
math.isnan(x)
```
