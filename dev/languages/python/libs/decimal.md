# decimal

The decimal module offers a Decimal datatype for decimal floating point arithmetic.
```python
from decimal import *

Decimal(’0.70’) * Decimal(’1.05’) # gives Decimal("0.7350")
.70 * 1.05 # gives 0.73499999999999999

Decimal(’1.00’) % Decimal(’.10’) # gives Decimal("0.00")
1.00 % 0.10 # gives 0.09999999999999995

sum([Decimal(’0.1’)]*10) == Decimal(’1.0’) # is True
sum([0.1]*10) == 1.0 # is False
```

Precision can be setup:
```python
getcontext().prec = 36
Decimal(1) / Decimal(7) # gives Decimal("0.142857142857142857142857142857142857")
```
