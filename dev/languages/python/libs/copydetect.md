# copydetect

 * [copydetect](https://github.com/blingenf/copydetect).
 * [plagiarismchecker 0.1.2](https://pypi.org/project/plagiarismchecker/).

Installation:
```sh
python -m pip install copydetect
```

Usage:
```sh
copydetect -t test_dir -r ref_dir -e py
```
