# unittest
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash'} -->

 * [unittest — Unit testing framework](https://docs.python.org/3/library/unittest.html).

Running tests from files in tests subfolder:
```sh
python -m unittest tests.my_filename
```

Write a test:
```python
class MyTest(unittest.TestCase):

	def test_myTest(self):
		self.assertTrue(...)
```

Assertions:
```python
self.assertTrue(True)
self.assertFalse(False)
self.assertEqual(x, y)
self.assertAlmostEqual(1.000000001, 1.000000002)

with self.assertRaises(TypeError):
	myfct(1, 2)

with self.assertWarns(DeprecationWarning):
	myfct(1, 2)
```
