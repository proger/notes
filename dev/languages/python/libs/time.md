# time

 * [time](https://docs.python.org/3/library/time.html).

Get current time in seconds:
```python
import time
time.time()
```

Compute elapsed time seconds:
```python
start = time.time()
# ...
elapsed = time.time() - start
```

Compute elapsed in fractions of seconds (float):
```python
start = time.perf_counter()
# ...
elapsed = time.perf_counter() - start
```
