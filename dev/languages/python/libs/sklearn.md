# sklearn (scikit-learn)

Module for machine learning.

 * [scikit-learn](https://scikit-learn.org/stable/).
 * [5.1. Pipelines and composite estimators](https://scikit-learn.org/stable/modules/compose.html).
 * [Choosing the right estimator](https://scikit-learn.org/stable/tutorial/machine_learning_map/index.html).

 * [Gradient Boosting regression](https://scikit-learn.org/stable/auto_examples/ensemble/plot_gradient_boosting_regression.html#sphx-glr-auto-examples-ensemble-plot-gradient-boosting-regression-py).
 * [Model persistence](https://scikit-learn.org/stable/modules/model_persistence.html).
