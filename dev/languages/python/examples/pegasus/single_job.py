import Pegasus.api as pg
# Requires GitPython module

# Write input file
with open("a.txt", 'w') as f:
    f.write("ABCD\n")

# Define input and output files
fin = pg.File("a.txt").add_metadata(creator="bob")
fout = pg.File("z.txt").add_metadata(final_output="true")

# Create Workflow
wf = pg.Workflow("single_job")
job1 = pg.Job("cat").add_args(fin).set_stdout(fout)
wf.add_jobs(job1)

# Export workflow definition as YAML file
wf.write("single_job.yml")

# Create and export the Transformation Catalog (TC)
tc = pg.TransformationCatalog()
cat = pg.Transformation("cat", site="local", pfn="/usr/bin/cat",
        is_stageable=False)
tc.add_transformations(cat)
tc.write()

# Plan execution
try:
    properties = {"pegasus.mode": "development"}
    wf.plan(sites=['local'], verbose=3, submit=True,
            **properties).wait().analyze().statistics()
except pg.PegasusClientError as e:
    print(e.output)

# Execute
wf.run()
