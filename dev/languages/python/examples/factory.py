class A:
    @staticmethod
    def create(v):
        if isinstance(v, int):
            return B(v)
        elif isinstance(v, float):
            return C(v)
        raise TypeError("Cannot instantiate a concrete A instance with v (value: {}).".format(v))

class B(A):
    
    def __init__(self, n):
        self.n = n
        
class C(A):
    
    def __init__(self, x):
        self.x = x

if __name__ == '__main__':
    
    print("\nCreate an A object with an integer:")
    print(A.create(10))
    
    print("\nCreate an A object with a float:")
    print(A.create(5.4))
    
    print("\nTry to create an A object with a string:")
    print(A.create('mystring'))
