def foo(a):
    a['ddd'] = 123

print("Setting dictionary variable:")
x = {}
print(x)
print()

print("Modifying dictionary variable inside a function:")
foo(x)
print(x)
print()
