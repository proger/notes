import torch

# Create tensor
x = torch.Tensor([[1, 2], [3, 4]])
print(x)

# Compute mean
print(torch.mean(x)) # Mean on all values
print(torch.mean(x, dim=0)) # Mean of columns
print(torch.mean(x, dim=1)) # Mean of rows

# Replace each elements with its square
print(x ** 2)
