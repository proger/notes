#!/usr/bin/env python

import os
import paramiko

class GssProxy(paramiko.proxy.ProxyCommand):
    '''
    Reimplementation of ProxyCommand to behave like a mix between a Paramiko Channel and a socket.
    '''
    def __init__(self, command_line):
        paramiko.proxy.ProxyCommand.__init__(self, command_line)

    def send(self, content):
        print("SENDING: " + repr(content))
        return paramiko.proxy.ProxyCommand.send(self, content)

    def recv(self, size):
        msg = paramiko.proxy.ProxyCommand.recv(self, size)
        print("RECEIVING: " + repr(msg))
        return msg

    def get_name(self):
        return "GssProxyCommand"

hostname = 'localhost'
user = os.environ['USER']
port = 22

# Open default ssh
client = paramiko.SSHClient()
client.load_system_host_keys()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname, username=user)
sftp = client.open_sftp()
print(sftp.listdir())
sftp.close()

# Use our own ssh subsystem command
sshexe = 'ssh'
proxy = GssProxy("%s -s -p%d %s@%s sftp" % (sshexe, port, user, hostname))
sftp = paramiko.sftp_client.SFTPClient(proxy)
print(sftp.listdir())
sftp.close()
