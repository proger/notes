import logging
import sys

logger = logging.getLogger(__name__)

logger.setLevel(logging.INFO)
logger.info("This message is not printed by default, even if level is set to INFO.")

handler = logging.StreamHandler(sys.stderr)
logger.addHandler(handler)
logger.info("This message is printed because we configured a handler on stderr.")

logger.debug("Not printed because DEBUG level is disabled.")
logger.setLevel(logging.DEBUG)
logger.debug("Printed because DEBUG level is enabled.")

fmt = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
handler.setFormatter(fmt)
logger.debug("A new formatter with time and level.")
