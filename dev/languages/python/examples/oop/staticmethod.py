class A:
    
    @staticmethod
    def staticFoo():
        print("This is staticFoo() from class A.")
        
    def foo(self):
        print("This is foo() method from class %s" % type(self))
        type(self).staticFoo()

class B(A):
    pass

class C(A):
    
    @staticmethod
    def staticFoo():
        print("This is staticFoo() from class C.")

a=A()
a.foo()
b=B()
b.foo()
c=C()
c.foo()
