# Python
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

 * [Python 3 latest documentation](https://docs.python.org/3/).
 * [Cheat Sheet: Writing Python 2-3 compatible code](http://python-future.org/compatible_idioms.html).
 * [PEP 8 — the Style Guide for Python Code](https://pep8.org/).
 * [Built-in Functions](https://docs.python.org/3/library/functions.html).

 * [Intermediate python courses](https://github.com/sib-swiss/intermediate-python-training): data manipulation and representation, faster code, multiprocessing, multithreading.
 * [Python 3 for Absolute Beginners](https://theswissbay.ch/pdf/Gentoomen%20Library/Programming/Python/Python%203%20for%20Absolute%20Beginners%20%282009%29.pdf).
 * [Cours de Python - Introduction à la programmation Python pour la biologie](https://python.sdv.univ-paris-diderot.fr/cours-python.pdf).

## Install

 * [How to Install Python 3.11 on Ubuntu 22.04](https://vegastack.com/tutorials/how-to-install-python-3-11-on-ubuntu-22-04/).

Debian deadsnakes repository for old versions of Python:
```sh
sudo add-apt-repository ppa:deadsnakes/ppa -y
```

### MicroPython

 * [MicroPython documentation](http://docs.micropython.org/).

Small subset of the Python standard library, optimised to run on
microcontrollers and in constrained environments

Install on Ubuntu:
```sh
apt install micropython
```


## Running

 * [Command line and environment](https://docs.python.org/3/using/cmdline.html).
 * [Environment variables](https://docs.python.org/3/using/cmdline.html#environment-variables).
 * [__main__ — Top-level code environment](https://docs.python.org/3/library/__main__.html).

It is possible to write a python file that can be either imported as a module
or run as a script. For this you have to test the `__name__` built-in variable:
```python
if __name__ == '__main__':
    # Running as a script
```

On Windows OS:
 1. `.py` files are associated with `python.exe`.
 2. `.pyw` extension specifies that this is a Python GUI app, so the console is hidden.

Interactive mode:
```python
python
```
`sys.argv[0]` is set to "-".
Variable `_` stores the last result.

Getting the current version:
```sh
python --version
```

Run some code on command line:
```sh
python -c "print('Hello')"
```

Run a module as a script:
```sh
python -m mymodule arg1 arg2 ...
```

Do not use Python libraries installed in user local folder (`~/.local`):
```sh
python -s ...
```
or
```sh
export PYTHONNOUSERSITE=1
python ...
```

Disable generation of `*.pyc` files in `__pycache__` folder:
```sh
python -B ...
# or
export PYTHONDONTWRITEBYTECODE=1
python ...
```

Run interactive mode:
```sh
python -i
```
For syntax highlighting, use [ipython](https://ipython.org/):
```sh
ipython
```

## Source file encoding

[PEP 263 -- Defining Python Source Code Encodings](https://www.python.org/dev/peps/pep-0263/).

To set encoding of the source code in a Python script, put one of the following lines (compatible with popular editors like emacs and vim) at the first or second line of the file:
```python
 # -*- coding: <encoding> -*-
 # coding=<encoding>
 # vim: set fileencoding=<encoding> :
```
Where `<encoding>` can be any valid encoding name (`iso-8859-15`, `utf8`, ...).
The line must match the regular expression `^[ \t\v]*#.*?coding[:=][ \t]*([-_.a-zA-Z0-9]+)`.

If your editor supports saving files as UTF-8 with a UTF-8 byte order mark (aka BOM), you can use that instead of an encoding declaration.

## Types & variables

 * [Built-in Types](https://docs.python.org/3/library/stdtypes.html).

Getting the type of a variable:
```python
type(myvar)
```
Returns an object of type `type`.

Print the type name (or class name) of an object:
```python
type(myvar).__name__
```

Testing the type:
```python
if isinstance(myvar, str):
    do_something()
```
or
```python
if type(myvar) is str:
    pass
```

Testing if an object is a function:
```python
if callable(myfct):
    pass
```

### Constants

There are no constants in Python.

### Mutability

 * [PYTHON OBJECTS: MUTABLE VS. IMMUTABLE](https://codehabitude.com/2013/12/24/python-objects-mutable-vs-immutable/).

### None

None is the equivalent of NULL in Python:
```python
myvar = None
```

A None value is of type NoneType.

Testing if a variable is None:
```python
if myvar is None:
    do_something()
```

Testing if a variable is NOT None:
```python
if myvar is not None:
    do_something()
```

### Boolean
    
```python
flag = True
on = False
bool_val = bool(v)
```
    
### Complex

Complex numbers are written with j or J as suffix for the imaginary part, or with the constructor `complex()`:
```python
z1 = 1j
z2 = 3+2J
z3 = complex(3,4)
```

Get real and imaginary parts:
```python
z1.real
z1.imag
```
    
### Integer

Convert a string into an integer:
```python
i = int("-120")
```

Get max integer:
```python
import sys
sys.maxsize
```

### Strings

 * [Text Sequence Type — str](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str).

Unicode string:
```python
u'My unicode string: voilà \u1234 !';
```
Encoding a unicode string into utf-8:
```python
u"äöü".encode('utf-8') # gives '\xc3\xa4\xc3\xb6\xc3\xbc'
```
Decoding from utf-8 to unicode:
```python
unicode('\xc3\xa4\xc3\xb6\xc3\xbc', 'utf-8') # gives u'\xe4\xf6\xfc'
```

Bytes literal:
```python
b'My bytes literal string.'
```

Convert a bytes like object into a string:
```python
mystr.decode("utf-8")
```

Raw string (no backslash interpretation):
```python
r'My raw \\\ string.'
```

Formatted string:
```python
f'My formatted string {myvar}.'
```
Variables are replaced automatically when string is parsed.
See [Formatted string literals](https://docs.python.org/3/reference/lexical_analysis.html#formatted-string-literals).

Writing a string on multiple lines using backslash:
```python
hello = "This is a rather long string containing\n\
     several lines of text just as you would do in C.\n\
     Note that whitespace at the beginning of the line is\ significant."
```

Multi-lines string:
```python
s = """
Usage: thingy [OPTIONS]
    -h          Display this usage message
    -H hostname     Hostname to connect to
"""
```

Joined stings:
```python
s = ("My string "
     "split in several"
     "parts.")
```

Raw string (no backslashes are interpeted):
```python
s = r"ABCD\n\EFG"
```
can be used in conjuncion with multi-lines string:
```python
s = r"""ABC\n
EFG"""
```

Convert a value/object into a human readable string:
```python
str(v)
```

Convert a value/object into a interpreter-compatible string:
```python
repr(v)
```
For instance, for a string repr() adds quotes and backslashes.
There exists another version of repr which abbreviates the display in case of
large container:
```python
import repr
repr.repr(set(’supercalifragilisticexpialidocious’))
```

Strings comparison:
```python
s1 == s2
s1 != s2
s1 > s2
s1 < s2
```

Concatenation:
```python
s = 'AB' + 'CD'
s += 'EF'
```

Append characters to the end of a string:
```python
s += 'blabla'
```

Repeat a string:
```python
s = s * 10
```

Substring:
```python
s[3] # index of char, indices start at zero.
s[2:5] # two indices defining a substring (last index is excluded)
s[2:] # From character number 2 to the end.
s[:5] # From start to characer 5 excluded.
```
Negative indices start counting from the right:
```python
s[-1] # last char
s[-3]
s[-2:]
s[:-3]
```

Strings are read-only:
```python
s[2] = 'z' # ILLEGAL !
```

Get the size of a string:
```python
len(s)
```

Search for a substring:
```python
s.find(t, start, end) # Returns -1 if not found, index otherwise.
s.index(t, start, end) # Same as find, but raises an exception.
s.startswith('ABC')
s.endswith('ABC')
```

Split a string into a list:
```python
mylist = s.split(',')
```

Split all characters of a string (i.e.: transform a string into a list of its characters):
```python
chars = list('My string text')
```

Join:
```python
import string
s = ','.join(mylist)
```

Template strings:
```python
from string import Template

t = Template('${village}folk send $$10 to $cause.')
s = t.substitute(village='Nottingham', cause='the ditch fund')

t = Template('Return the $item to $owner.')
d = dict(item='unladen swallow')
s = t.substitute(d) # Raises an KeyError exception because no value is provided for the key 'owner'.
s = t.safe_substitute(d) # Raises no exception.
```

Template subclasses can specify a custom delimiter:
```python
class MyTemplateClass(Template):
    delimiter='%'
t = MyTemplateClass("Hello %name !")
```

Stripping white spaces (trip spaces, tabs, new lines, ...):
```python
s2 = s.strip()  # strip at both end
s2 = s.lstrip() # strip at left
s2 = s.rstrip() # strip at right
```

Padding a string:
```python
s2 = s.rjust(6) # right justify a string in a field of width 6
s2 = s.ljust(5) # left justify a string
s2 = s.center(4)    # center a string
```

Padding a numeric string:
```python
s = '12'.zfill(5)   # Pads 3 zeros before the number 12
s = '-3.14'.zfill(7)
```

Uppercase and lowercase:
```python
s2 = s.lower()
```

To string function `str()`:
```python
def __str__(self):
    # ...
    return some_string
```

Char functions:
```python
i = ord('A') # return the unicode code point of the character
c = unichr(i) # return the unicode character corresponding to the unicode code ppoint
c = chr(115) # return the characters corresponding to the ASCII code specified
```

Convert integer to string:
```python
s = str(10)
```

#### Formatting

 * [String Formatting Operations](https://docs.python.org/2.4/lib/typesseq-strings.html).

`%` replacement (old style):
```python
'The value of PI is approximately %5.3f.' % math.pi
"My object %r" % obj # obj converted with `repr()`
"My object %s" % obj # obj converted with `str()`
"My obj1 %s and my obj2 %s" % (obj1, obj2)
"My obj1 %(a)s and my obj2 %(b)s" % {'a':obj1, 'b':obj2}
```

Format (new style):
```python
s = '{0:2d}'.format(x)
s = '{0} {1}'.format(s1, s2)
s = '{tag} {0}'.format(v, tag='mystring')
s = '{0:.3f}'.format(x)
s = '{0:10} {1:8d}'.format(s, i)    # field 0 is 10 columns wide, and field 1 is 8 columns wide.
```

Giving a dictionary to format:
```python
table = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 8637678}
s = ('Jack: {0[Jack]:d}; Sjoerd: {0[Sjoerd]:d}; Dcab: {0[Dcab]:d}'.format(table))
```
OR
```python
table = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 8637678}
s = 'Jack: {Jack:d}; Sjoerd: {Sjoerd:d}; Dcab: {Dcab:d}'.format(**table)
```

### Lists

 * [Data Structure](https://docs.python.org/3/tutorial/datastructures.html).

A list, as a string, is a sequence.

Declare a list:
```python
a = [’spam’, ’eggs’, 100, 1234]
```

Use the `range` function to create a list:
```python
range(10)           # --> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
range(1, 11)        # --> [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
range(0, 30, 5)     # --> [0, 5, 10, 15, 20, 25]
range(0, 10, 3)     # --> [0, 3, 6, 9]
range(0, -10, -1)   # --> [0, -1, -2, -3, -4, -5, -6, -7, -8, -9]
range(0)            # --> []
range(1, 0)         # --> []
```
In Python 2 `range()` returns a list. However in Python 3 it returns an
iterator (same as function `xrange()` in Python 2). See [Python’s range()
Function Explained](http://pythoncentral.io/pythons-range-function-explained/).

Access:
```python
a[0]
a[-2] # start from end
a[1:3] # both are indices
```
    
Test if an element is in a list:
```python
if x in my_list:
    pass
```

Concatenate:
```python
a + b
```
    
Repeat:
```python
a * 10
```

Assignment:
```python
a[3] = 18
```

Compare:
```python
if [1, 2] == [2, 3]:
    # ...
```

Replace:
```python
a[1:3] = [3, 4]
```

Remove elements:
```python
a[0:2] = []
a.remove(x) # remove the first item whose value is x
a.pop()     # removes and returns last element
a.pop(i)    # removes and returns element at index i
del a[i]    # removes element at index i
del a[3:7]  # removes range
del a[:]    # clear list
del a       # deletes variable
```

Append:
```python
a[len(a):] = [x]
a.append(a_single_item)
a.extend(another_list)
a[len(a):] = another_list
```

Insert:
```python
a[1:1] = ['ABC', 14]
a.insert(1, x)
```

Clear:
```python
a[:] = []
```

Search:
```python
a.index(x)  # returns the index of the first item whose value is x
a.count(x)  # returns the number of times x appears in the list
```

Length:
```python
len(a)
```

Test if a list is empty:
```python
if a:
    doSomething()
```

Sort a list and return a copy:
```python
b = sorted(a)
b = sorted(a, key = lambda x: foo(x))
```

Modifyng functions:
```python
a.sort()    # sort, in place
a.reverse() # reverse list, in place
```

Filter values from a list:
```python
b = filter(lambda x: x < 10, a)
```

Filter:
```python
import functools
import operator
map(myfunc, mylist) # returns a list of elements myfunc(mylist[i])
functools.reduce(myfunc, mylist, starting_value)    # returns the result of applying myfunc on all the list
functools.reduce(operator.add, range(1,11)) # returns 1+2+3+...+11
```

Stack & queue:
```python
a.pop() a.append(x) # functions to use for a stack
a.pop(0) a.append(x)    # functions to use for a queue
```

Nested lists:
```python
q = [2, 3]
p = [1, q, 4]
```
`p[1]` and `q` point to the same object.

Create a list of numbers:
```python
range(stop)
range(start, stop, step)
range(5, 10) # [5, 6, 7, 8, 9]
range(0, 10, 3) # [0, 3, 6, 9]
range(-10, -100, -30) # [-10, -40, -70]
```

Iterate over indices of a list:
```python
for i in xrange(len(data)):
    data[i] = ...
```

Iterate over indices and elements of a list:
```python
for i, x in enumerate(data):
    data[i] = "something" if x > 24 else "something else"
```

Iterate over indices of a list with enumerate but forget about the elements:
```python
for i, _ in enumerate(data):
    data[i] = "something"
```

List comprehension is a concise way to transform a list:
```python
[3*x for x in vec]
[s.strip() for s in mylist]
```
Nested list comprehensions for swapping rows and columns:
```python
mat = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
      ]
[[row[i] for row in mat] for i in [0, 1, 2]] [[1, 4, 7], [2, 5, 8], [3, 6, 9]]
```

Filtering out elements using list comprehension:
```python
[e for e in mylist if e > 2]
```
or with filter and lambda:
```python
filter(lambda x: x != 4, mylist)
```

Flatten a list of lists:
```python
import itertools

x = list(itertools.chain.from_iterable(my_list))
```

#### Yield

The `yield` statement creates a generator function to iterate over a list.
The function issuing the `yield` is frozen and will be resumed after the end of the iteration or if something wrong happens and if the `yield` is put inside a try statement, the finally clause will be executed.

 * [What does the yield keyword do in Python?](http://stackoverflow.com/questions/231767/what-does-the-yield-keyword-do-in-python).
 * [The yield statement](https://docs.python.org/2/reference/simple_stmts.html#yield).
    
Generators are a simple and powerful tool for creating iterators.
Defining a generator consists only in defining a function which uses the yield statement instead of the return statement:
```python
def reverse(data):
    for index in range(len(data)-1, -1, -1):
        yield data[index]
```
Usage:
```python
for char in reverse(’golf’):
    print char,
```
This code will print "flog".
    
A generator expression defines a generator inplace. It looks a bit like list comprehensions, but with parentheses instead of brackets:
```python
sum(i*i for i in range(10))     # sum of squares
sum(x*y for x,y in zip(xvec, yvec)) # dot product
sine_table = dict((x, sin(x*pi/180)) for x in range(0, 91))
unique_words = set(word for line in page for word in line.split())
valedictorian = max((student.gpa, student.name) for student in graduates)
list(mystring[i] for i in range(len(,mystring)-1,-1,-1)) # make a list of characters of mystring in reverse order
```

### Set

Construct a set from a list, eliminating all duplicates:
```python
myset = set(mylist)
```

Construct a set of characters from a string:
```python
myset_of_chars = set('astring')
```

Test presence of an element in a set:
```python
myelem in myset
```

```python
myset.add(elem)    # Add element
myset.remove(elem) # Remove element
```

Add a set to another set:
```python
myset |= otherset
```

Add all elements of a list to a set:
```python
myset.update(mylist)
```

Operations of set:
```python
a - b # elements in a but not in b
a | b # elements in either a or b
a & b # elements in both a and b
a ^ b # elements in a or b but not both
a.difference(b) # Same as `a - b`
```

Difference between two sets (gives all elements that are either in a or in b but
not in both):
```python
a.symmetric_difference(b)
```

### Deque
    
```python
from collections import deque
```
    
Deque are like list objects, but with faster append and pop from the left, and slower lookups in the middle.
```python
d = deque(["task1", "task2", "task3"]) >>> d.append("task4")
print "Handling", d.popleft()
```

### Tuples

Tuples are small sequences that are not modifiable:
```python
t = 12345, 54321, ’hello!’
u = t, (1, 2, 3, 4, 5)
empty = ()
singleton = ’hello’,    # <-- note trailing comma
x, y, z = t
```

They can be accessed like lists:
```python
coord = (10, 11) # x,y coordinates
coord[0]
```

### Dictionaries

Keys can be string, number of tuple (if the tuple contains only strings, numbers and tuples)

Creation:
```python
mydict = {}
mydict = {'a': 234, 'b': 121, 'c': 1244}
mydict = dict([('a', 23), ('b', 34)]) # use tuples with dict() constructor
mydict = dict([(x, x**2) for x in (2, 4, 6)]) # use a list comprehension
mydict = dict(sape=4139, guido=4127, jack=4098) # use keyword arguments in dict() constructor
mydict = dict(zip([0, 5, 8], ['a', 'b', 'c'])) # From list of keys and values
mydict = dict.fromkeys(['a', 'b', 'c'], 10) # From list of keys and one value
```

Deep copy:
```python
import copy
x = copy.deepcopy(my_dict)
```

Test if a dict is empty:
```python
if not mydict:
    do_something()
```

Get size:
```python
len(mydict)
```

Access:
```python
mydic['b'] # raise exception
mydic.get('b') # Default to None
mydic.get('b', 4) # With a default value
```

Modify:
```python
mydict['c'] = 455
```

Remove:
```python
del mydict['a']
```

Get a the keys:
```python
mydict.keys() # Obtain a dict_keys object, which a sort of set
list(mydict) # Get a list object containing keys
```

Get a the values:
```python
mydict.values() # Obtain a dict_values object, which is a sort of set
list(mydict.values()) # Get a list object containing values
```

Print dictionary's elements sorted:
```python
print(sorted(mydict.items()))
```

Search for a key:
```python
'e' in mydict
```

Looping on keys:
```python
for k in mydict:
    # ...
```

Looping on keys and values:
```python
for k, v in mydict.items():
    print k, v
```

Taking a subset of a dictionary:
```python
myDict = { k: myDict[k] for k in myListOfKeys } 
```

Split a string of key values into a dictionary:
```python
s = 'a=3,b=9,c=10'
mydict = dict(u.split("=") for u in s.split(","))
```

Merge two dictionaries:
```python
z = {**x, **y}
```
The second dictionary (`y`) will replace any key already defined by the first
(`x`).

### Structure

To create a struture, one uses an empty class:
```python
class Employee:
    pass
john = Employee()
john.name = 'John'
john.dept = 'compute lab'
```

`struct` module provides pack() and unpack() functions.
`"H"` and `"I"` represent two and four byte unsigned intergers.
`"<"` means little-endian byte order.
```python
import struct
 # ...
fields = struct.unpack('<IIIHH', string_with_binary_data[offset:offset+16])
```

## Statements

Indentation, instead of curling braces in C, is used to control the grouping of blocks.

### `pass`

The pass statement does nothing. It can be used when a statement is required syntactically but the program requires no action (No-op or NOP).

```python
def myfunc(param1):
    pass
```

### Operators

 * [operator — Standard operators as functions](https://docs.python.org/3/library/operator.html).

| Operator | Description
| -------- | -----------------------
| `+`      | Addition (unary plus), or concatenation for strings.
| `-`      | Subtraction (unary minus).
| `*`      | Multiplication.
| `**`     | Exponentiation (raise to the power).
| `/`      | Division.
| `//`     | Integer division (floor).
| `%`      | Modulo.
| `<`      | Less than.
| `>`      | Greater than.
| `<=`     | Less than or equal to.
| `>=`     | Greater than or equal to.
| `==`     | Equal to.
| `!=`     | Not equal to.
| `<>`     | An obsolete form of 'not equal to'.

No `--` and `++` operators exist.

Ternary expression:
```python
value = MyVarIsTrue and 'yes' or 'no'
```
In more modern version (>=2.5 ?):
```python
value = 'yes' if MyVarIsTrue else 'no'
```

### Functions

 * [Function definitions](https://docs.python.org/3/reference/compound_stmts.html#function-definitions).

Defining a function:
```python
def myfunc(n):
    # ...
    return some_value
```

For returning several values, use a tuple:
```python
def foo():
    return a, b
#_ ...
x, y = foo()
```

Default values for arguments:
```python
def foo(n = 2):
    # ...
```
Default values are evaluated at the point of function definition, so they don't
need to be constants:
```python
i = 5
def foo(n = i):
    # ...
```
However, complex types must not be used, as the exact same instance will be
used for the default value for each function call:
```python
def foo(x=[]):
    x.append(1)
foo() # x is [1]
foo() # x is [1 1]
foo() # x is [1 1 1]
```
Instead, use `None` as the default value and test it inside the function:
```python
def foo(x=None):
    if x is None:
        x = []
    # ...
```

Arbitrary number of arguments:
```python
def foo(arg1, arg2, *args):
    # ...
```

Argument names can be used as keys to set explicitly each or part of them:
```python
def foo(arg1, arg2):
    # ...

foo(100, arg2='yesh')
foo(arg2='no', arg1=5)
```

To put all keyword arguments (not associated with a formal parameter) inside a dictionary:
```python
def myfunc(**keywords):
    # ...
myfunc(n=1, name='otto')
```
The dictionary can be put after a list, so the list get first all non-keywords arguments first:
```python
def myfunc(arg1, *args, **keywords)
    # ...
```

Passing arguments from a list/tuple as individual arguments:
```python
args = [3, 6]
range(*args)    # The * operator unpack the elements of the list
somefunc(**some_dictionary) # The ** operator unpack the elements of the dictionary, passing them as keyword/value couples to the function
```

Lamdba function:
```python
lambda a,b: a+b

def make_incrementor(n):
    return lambda x: x + n
f = make_incrementor(42)    # return a lambda function
f(0)    # gives 42
f(1)    # gives 43
```

### Conditions

Boolean operators:
```python
a or b
a and b
not a
```

Tests can be chained:
```python
a < b == c  # tests if a < b and b == c
```

Sequences can be compared:
```python
list1 < list2
list1 == list2
tuple1 == tuple2
string1 > string2
```

Presence of an element in a sequence:
```python
elem in mylist
elemen not in mylist
```

Testing if two objects are the same object (useful for mutable objects like lists):
```python
obj1 is obj2
obj1 is not obj2
```

### if elif else

```python
if x < 0:
    z = -1
elif x == 0:
    z = 0
else:
    z = 1
```

Ternary operator:
```python
x if y > 1 else -x;
```

### For loop

```python
for x in a: # a is a list
    print x
```

If you need to modify elements, it's safer to make a copy before:
```python
for x in a[:]:
    if len(x) > 6: a.insert(0, x)
```

Iterating on indices:
```python
for i in range(len(a)):
    print i, a[i]
```

to work on a copy:
```python
for x in a[:]: # make a slice copy of the entire list
    if len(x) > 6: a.insert(0, x)
```
    
The break statement, like in C, breaks out of the smallest enclosing for or while loop.
 The continue statement, also borrowed from C, continues with the next iteration of the loop.
    
Loop statements may have an else clause; it is executed when the loop terminates through exhaustion of the list (with for) or when the condition becomes false (with while), but not when the loop is terminated by a break statement.
    
Looping on index and value at the same time:
```python
for i, v in enumerate([’tic’, ’tac’, ’toe’]):
    print i, v
```

Looping on several sequences at the same time:
```python
for i, j in zip(list1, list2):
    # ...
```

Loop in reverse oder:
```python
for i in reversed(range(1,10,2)):
    print i
```

Loop in sorted order:
```python
for f in sorted(set(basket)):
    print f
```

### While loop

```python
while i < 100:
    # do something
```

Break, continue and else:
```python
while i < 100: # or a for statement
    # do something
    if condition:
        break
    elif other_condition:
        continue
    # do something else
else:
    # do something when while loop finishes normally (not because of a break statement)
```

### Exceptions

 * [Errors and Exceptions](https://docs.python.org/3/tutorial/errors.html).
 * [Built-in Exceptions](https://docs.python.org/3/library/exceptions.html).

Try block:
```python
try:
    # ...
except ValueError: # catch exception whose type is ValueError
    print "Some error occured !"
except (RuntimeError, TypeError, NameError):
    pass
except: # catch all exceptions
    print "Unexpected error"
    raise # throw the exception again
else:
    # put here some code that will be executed only if the try clause doesn't raise an exception
```

Rethrowing and adding additional information in the message:
```python
try:
    # ...
except SomeError as e:
    raise SomeOtherError(str(e) + "Some more information.")
```

The finally clause (clean-up) is always executed, if exceptions occurred in the try clause and aren't caught by the except clauses, if no exceptions occurred, or if an exception occurred inside an except clause or the else clause.
```python
try:
    raise KeyboardInterrupt
finally:
    print 'Goodbye, world!'
```

One can associate multiple arguments to an exception. However a better practice is to pass only one argument, which may be a tuple. 
```python
try:
    raise Exception('spam', 'eggs')
except Exception as inst:
    print type(inst)
    print inst.args # arguments stored in .args
    print inst  # __str__ allows args to be printed directly
    x,y=inst    # __getitem__ allows args to be unpacked directly
```

Raising an exception:
```python
raise NameError('This is an error')
raise SomeClass, instance   # where instance must of class SomeClass or class derived from SomeClass.
raise instance  # calls `raise instance.__class__, instance`
```

User-defined exceptions:
```python
class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self): # Conversion to string
        return repr(self.value)
```
OR? --> what is the best?
```python
class MyError(RuntimeError):
    def __init__(self, value1, value2, ...):
        msg = f"Blabla {value1}..."
        super().__init__(msg)
```

### With

 * [The with statement in Python 2](https://docs.python.org/2/reference/compound_stmts.html#with).

```python
with open(myfilepath, 'rb') as csvfile:
    ...
```

The expression between `with` and `as` must return a [context manager](https://docs.python.org/2/reference/datamodel.html#context-managers).

### Switch / case

From Python 3.10, the `match` statement exists for pattern matching:
```python
match myvar:
    case 10:
        # ...
    case 20:
        # ...
    case _:
        # default
```

Switch statement did not exist in Python <=3.9.
We used a dictionnary instead, or if/elif:
```python
def f(x):
    return {
        'a': 1,
        'b': 2
    }.get(x, 9)  # 9 is default.
```

### assert

```python
assert len(arr) > 0
```

Use a message
```python
assert len(arr) > 0, "Array arr is empty."
```

## globals

 * [globals()](https://docs.python.org/3/library/functions.html#globals).

Get function reference:
```python
my_func = globals().get('my_func_name')
my_func(arg1, arg2)
```
When called from a function or method `globals()` returns the dictionary of
variables defined in the namespace of the module to which belongs the function
or method.

## Built-in vars

Get folder of current file:
```python
os.path.dirname(__file__)
```

## Built-in functions

 * [Built-in Functions](https://docs.python.org/3/library/functions.html).

### input

Read some text input by user:
```python
the_user_input = input('Input some text:')
```

## I/O

Open a file:
```python
f = open('/tmp/workfile', 'w')
f = open('/tmp/workfile', 'w', encoding='utf8')
```
See [open](https://docs.python.org/3/library/functions.html#open).
Modes are `r`, `w`, `a` and `r+` (both reading and writing)
By default files are opened in text mode, but there is also a binary mode specfied with the `b` modifier (`rb`, `wb`, `r+b`) in which content is read/write into/from bytes objects without any decoding/encoding.
In text mode, a `newline` option is available to control the reading and writing of end of line (EOL) characters. By default all EOL characters are replaced by a single `"\n"` when reading, and `"\n"` characters are replaced by `os.linesep` when writing.

Close a file:
```python
f.close()
f.closed    # Is set to true when file is closed
```

With statement:
```python
with open('/my/file', 'r') as f:
    read_data = f.read()
```
File is automatically closed when leavin with block.

Read a number of bytes/chars:
```python
f.read(n)
f.read()    # read all file
```
Returns an empty string if EOF is reached.

Read a single line:
```python
s = f.readline()
s1 = s.strip()  # strip new line char (i.e.: chomp)
```
It doesn't eat the newline character.
Return an empty string when EOF is reached.

Read all lines into a `list`, EOL characters are preserved:
```python
mylist = f.readlines()
```

Read lines one by one:
```python
for line in f:
    print(line)
```

Write:
```python
f.write(s)
f.write(str(42))
f.writelines(['a', 'b'])
```

Position in a file:
```python
f.seek(n)   # moves
f.tell()    # gives position
```

Pickling: this means the use of the standard module pickle to serialize automatically some arbitrary object:
```python
pickle.dump(x, f)   # serialize object x inside f
x = pickle.load(f)  # unserialize object x from f
```

Standard streams:
```python
sys.stdin
```

Read string chunks from stdin:
```python
for read_bytes in iter(lambda: sys.stdin.read(chunk_size), b''):
    pass
```

Read binary chunks (bytes-like objects) from stdin:
```python
for read_bytes in iter(lambda: sys.stdin.buffer.read(chunk_size), b''):
    pass
```

Compare files:
```python
import filecmp
filecmp.cmp('a.txt', 'b.txt')
```

## Tail call recursion

 * [Tail recursion in python](https://apoorvtyagi.tech/tail-recursion-in-python).

If the recursion call is well written, a jump can be used instead of a call.
This way there's no stack growth.
In the following example, this is not entirely. Local variables of
the `binary_search` function are removed from the stack before making the
recursive call `return binary_search(...)` because the interpreter knows there
will be no use for them anymore, but still the function call is put on
the stack and thus makes the stack grow.
```python
def binary_search(x, lst, low=None, high=None) :
    (low, high) = (low or 0, high or len(lst)-1)
    mid = low + (high - low) // 2
    if low > high :
        return None
    elif lst[mid] == x :
        return mid
    elif lst[mid] > x :
        return binary_search(x, lst, low, mid-1)
    else :
        return binary_search(x, lst, mid+1, high)
```

With the factorial example.
A recursive implementation that makes the interpreter put both function call and
variable on the stack:
```python
def function(n):
    if n == 0: 
        return 1
    return factorial(n-1) * n
```
`n` must be kept on the stack because we will need it after the `factorial(n-1)`
call.
However with the following implementation the `n` variable will not be put on
the stack, because the interpreter knows it will not need it anymore:
```python
def function(n, var=1):
    if n == 0: 
        return var
    return factorial(n-1, var * n)
```
However the function call is still put on the stack, so the stack grows.
Using the `tail_recursion` module we can implement real *Tail (Call) Recursion
Optimization*:
```python
import tail_recursion

@tail_recursion.tail_recursive
def factorial(n, var=1):
    if n == 0:
        return var
    tail_recursion.recurse(n-1, var=var*n)
```
Now, nothing will be put on the call stack (neither the function call, nor the
local variables). We call the `factorial()` recursive function with any number
with risking a stack overflow.

## Reduce

```python
def factorial(num):
    return reduce(lambda x, y: x*y, xrange(1,num), 1)
```

## Command line

Command line arguments are stored inside sys.argv list:
```python
sys.argv[0] # contains the program's name
```

## Printing

In Python v3 the parenthesis are compulsory for print function.
In Python version 2.7.2, `print` is a statement and doesn't need parenthesis. However it supports them and they are needed in Python 3, since print becomes a function.
Thus it is always preferable to put parenthesis.
```python
print(mylist, sep=',', end="\n")
```
To get the behaviour of the Python 3 `print()` function into Python 2, use the `__future__` library:
```python
from __future__ import print_function
print(mylist, sep=',', end="\n")
```

Space is inserted automatically between two comma separated items:
```python
print 'The value of i is', i
```
OUTPUT: The value of i is 234

a comma at the end avoid automatic insertion of a new line:
```python
i = 0
while i < 2:
    print "yesh",
```
OUTPUT: yesh yesh

Prints an object with indentation and line breaks:
```python
import pprint
pprint.pprint(mycontainer, width=30)
```

Text WRAP:
```python
import textwrap
print textwrap.fill(mystring, width=40)
```


## dir

View the directory of an object (i.e.: members):
```python
dir(myobj)
```

## exit

Terminate the program:
```python
exit()
exit(12)
```

## hash

 * [`object.__hash__(self)`](https://docs.python.org/3/reference/datamodel.html#object.__hash__)
 * [PYTHONHASHSEED](https://docs.python.org/3/using/cmdline.html#envvar-PYTHONHASHSEED).

Compute the hash of an object:
```python
h = hash(myobj)
```

For strings the hash is computed with a random component. The seed used to
generate the random numbers is changed on each Python session.
To fix the seed and get the same hash values for the sames strings on each
Python session, set the environment variable `PYTHONHASHSEED` when running
Python:
```sh
export PYTHONHASHSEED=0
python myscript.py
```

## help

Get help on a function/class/type:
```python
help(str)
```

Printed documentation is taken from `__doc__` member: `myobj.__doc__`.

## input

Get text input from console:
```python
input("Write your name: ")
```

## print

Print without ending newline character:
```python
print("Hello", end='')
```

For writing:
```python
print("Hello", flush=True)
```

## quit

Terminate the program:
```python
quit()
quit(12)
```

## Modules and import

 * [Modules](https://docs.python.org/3.10/tutorial/modules.html).
 * [How To Write Modules in Python 3](https://www.digitalocean.com/community/tutorials/how-to-write-modules-in-python-3).

A module is a file with `.py` and whose name is the module's name.

Within a module, the module's name can be access thanks to the variable `__name__`.

Importing a module:
```python
import mymodule
```

Call a function from a module:
```python
mymodule.myfunc()
```

Get the name of a module:
```python
mymodule.__name__
```

Get the path of a module:
```python
mymodule.__file__
```

Importing a function of a module directly into the local symbol table:
```python
from mymodule import myfunc, myfunc2
from mymodule import * # import all names except those beginning with an underscore
```
Then the functions are directly accessible, and the imported module itself isn't included inside the local symbol table:
```python
myfunc()
```

Renaming a function of a module:
```python
myfunc_localname = mymodule.myfunc
```

Reload a module:
```python
reload(mymodule)
```

List of defined names in a module:
```python
dir(sys)    # list names of sys library
dir()       # list names of current library
dir(__builtin__)    # list names of built-in functions and variables
```

### Using a module as a main program

It's possible to run a module as program with the command:
```python
python mymodule.py <arguments>
```
In that case it is possible for the module to know that it's running as a script by testing the `__name__` variable which will be set to `__main__`:
```python
if __name__ == "__main__":
    import sys
    myfunc(int(sys.argv[1]))
```

### Search path

When importing a module the directories contained in `sys.path` are searched.
`sys.path` is initialized to:
 * `.` (local directory).
 * Directories in env var `PYTHONPATH` (list of folders separated by `:`).
 * Python installation directory (e.g.: `/usr/local/lib/python`).
`sys.path` is modifyable at run-time.

Compiled version of a module:
Module files `.py` are compiled as `.pyc`. If a `.pyc` is present, and up-to-date compared to the `.py`, then it is loaded instead of the `.py`.
Code run at the same speed, but the module loading is faster with `.pyc` than with a `.py`.
It is possible to distribute `.pyc` instead of `.py`, in order to avoid easy reading of the code.
The module `compileall` can create `.pyc` files for all modules in a directory.

### import

Import a module:
```python
import mymodule
```
Get the installation path of the imported module:
```python
mymodule.__path__
```

To import a module for testing, just run import with the relative path to the module folder or module file:
```python
import my.path.to.my.module
```

```python
from .MyModule import * # Import module MyModule from the same directory as the current module.
from ..MyModule import * # Import module from parent directory.
from ...MyModule import * # Import module from two levels up.
```

By default all names not starting with an underscore are exported.
To control which names are exported in a module, define `__all__` in the module:
```python
__all__ = ['MyClass', '_myFct']
```

## Testing code

 * [Testing Your Code](https://docs.python-guide.org/writing/tests/).

