# pyenv
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','bash':'sh'} -->

 * [pyenv](https://github.com/pyenv/pyenv).
 * [Installation](https://github.com/pyenv/pyenv#installation).
 * [pyenv installer](https://github.com/pyenv/pyenv-installer).

Install on Archlinux:
```sh
pacman -S pyenv
```
Install using automatic installer:
```sh
curl https://pyenv.run | bash
```

The `pyenv` tool allows to install multiple versions of Python.
After installing the `pyenv` package, add the following line to the `.profile`:
```sh
if ! command -v pyenv >/dev/null ; then
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
fi
```
It modifies the `PATH` env var to supersede the system Python binary, and
defines the `pyenv()` function that servers as a front-end to the `pyenv` script
and allows modification of the current shell session while using `activate` and
`deactivate` commands.

Get Python installed versions:
```sh
pyenv versions
```

Getting the current version:
```sh
pyenv version
```

Install a version of Python:
```sh
pyenv install 3.6.10
```
The new version will be installed inside `~/.pyenv/versions/...`.
To avoid error if version is already installed:
```sh
pyenv install -s 3.6.10
```

To get the list of available versions:
```sh
pyenv install -l
```

Enable versions in local folder:
```sh
pyenv local 3.9.9 3.10.0
```

List local versions:
```sh
pyenv local
```

To use one of the versions inside current shell session:
```sh
pyenv shell 3.6.10
# Do my stuff...
pyenv shell --unset
```
or
```sh
export PYENV_VERSION=3.6.10
# Do my stuff...
unset PYENV_VERSION
```

Run a command with the selected version of Python:
```sh
pyenv exec python myscript.py
```

Another way is to use the `.python-version` file.
The first `.python-version` found (inside current folder or any parent folder),
will be used.
To define a `.python-version` file inside current use:
```sh
pyenv local 3.6.10
```
To remove the file:
```sh
pyenv local --unset
```

The current version can also be set on global level with the command:
```sh
pyenv global 3.6.10
```

## Errors

Tk (tkinter) error:
```
Downloading Python-3.8.14.tar.xz...
-> https://www.python.org/ftp/python/3.8.14/Python-3.8.14.tar.xz
Installing Python-3.8.14...
Traceback (most recent call last):
  File "<string>", line 1, in <module>
  File "/home/pierrick/.pyenv/versions/3.8.14/lib/python3.8/tkinter/__init__.py", line 36, in <module>
    import _tkinter # If this fails your Python may not be configured for Tk
ModuleNotFoundError: No module named '_tkinter'
WARNING: The Python tkinter extension was not compiled and GUI subsystem has been detected. Missing the Tk toolkit?
Installed Python-3.8.14 to /home/pierrick/.pyenv/versions/3.8.14
```

Solution: install Tk developement files:
```sh
apt install tk-dev # Ubuntu
yum install tk-devel # CentOS
```
