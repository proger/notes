# Symfony\Component\Yaml

```php
<?
try {
	$this->cfg = Symfony\Component\Yaml\Yaml::parseFile($file);
} catch (Symfony\Component\Yaml\ParseException $e) {
	throw new ExhalobaseException("YAML parsing error in file \"$file\". " . $e->getMessage());
}
```
