# MongoDB

 * [MongoDB PHP library](https://docs.mongodb.com/php-library/current/).
  - [MongoDB\Client::listDatabases()](https://docs.mongodb.com/php-library/v1.2/reference/method/MongoDBClient-listDatabases/).
  - [MongoDB\GridFS\Bucket Class](https://docs.mongodb.com/php-library/v1.2/reference/class/MongoDBGridFSBucket/).
  - [db.collection.find()](https://docs.mongodb.com/manual/reference/method/db.collection.find/).
  - [Query and Projection Operators](https://docs.mongodb.com/manual/reference/operator/query/).
  - [MongoDB\Model\BSONDocument](https://docs.mongodb.com/php-library/current/reference/bson/#phpclass.MongoDB\Model\BSONDocument).
 * [MongoDB driver library](http://php.net/manual/en/book.mongo.php).
  - [executeQuery](http://php.net/manual/en/mongodb-driver-manager.executequery.php).

Installation with pecl:
```sh
pecl install mongodb
```

Installation on ArchLinux with pacman:
```sh
pacman -S php-mongodb
```

BSON classes:
 * `MongoDB\Model\BSONArray` extends [ArrayObject](https://www.php.net/arrayobject).
 * `MongoDB\Model\BSONDocument` extends `ArrayObject` too.

Create a connector:
```php
<? $dbConn = new MongoDB\Client("mongodb://$host/", [ 'username' => $user, 'password' => $pwd, 'authSource' => $authdb ]);
```

Get info about all users:
```php
<? $cursor = $dbConn->command(['usersInfo'=>1, 'filter'=>new MongoDB\Model\BSONDocument([])]);
```

Get users info:
```php
<? $cursor = $dbConn->command(['usersInfo'=>1, 'filter'=>['user'=>'some_user']]);
```

Get info about one user (works when the user itself asks info about its account):
```php
<? $cursor = $dbConn->command(['usersInfo'=>$user]);
```

Get a reference to a collection:
```php
<? $coll = $dbConn->selectCollection("myColl");
```

Get all objects of a collection:
```php
<? $result = $coll->find();
```

Search for objects with one field set to a particular value:
```php
<? $result = $coll->find(['myfield' => 'myvalue']);
```

Search for objects with two fields set particular values:
```php
<? $result = $coll->find(['myfield1' => 'myvalue1', 'myfield2' => 'myvalue2']);
```

Filter on a set of possible values:
```php
<? $result = $coll->find(['myfield' => ['$in' => ['myvalue1', 'myvalue2']]);
```

Filter on field existence:
```php
<? $result = $coll->find(['myfield' => ['$exists'=>true]]);
```

Use two filters together with AND operator:
```php
<? $result = $coll->find(['$and'=>[['myfield1' => 'myvalue1'], ['myfield2' => ['$in' => ['myvalue2', 'myvalue3']]]]]);
```
`$and` can take more than 2 arguments, but not just one.

Use two filters together with OR operator:
```php
<? $result = $coll->find(['$or'=>[['myfield1' => ['$exists'=>false]], ['myfield1' => ['$in' => ['myvalue1', 'myvalue2']]]]]);
```
`$or` can take more than 2 arguments, but not just one.

Insert one object implementing `MongoDB\BSON\Persistable`:
```php
<? $coll->insertOne($myObj);
```

Insert many objects implementing `MongoDB\BSON\Persistable`:
```php
<?  $coll->insertMany($myObjects);
```

Update one object implementing `MongoDB\BSON\Persistable`:
```php
<?  $coll->replaceOne(['myId' => 'myValue'], $myObject, ['upsert' => true]);
```

Update some fields on a selected set of objects:
```php
<?  $coll->updateMany(['myfield' => 'myvalue'], ['$set' => ['myOtherField' => 'myOtherVale']], ['upsert' => true]);
```
