# MySQL

Connecting:
```php
<?php
mysql_connect("hostname","username","password");
?>
```

Selecting database:
```php
<?php
mysql_select_db("database name", $link_id);
?>
```

List databases:
```php
<?php
$result = mysql_list_dbs($link_id);
?>
```

List tables:
```php
<?php
$result = mysql_list_tables("database name", $link_id);
?>
```

Fields info:
```php
<?php
$field = mysql_list_fields("database name", "table name", $link_id);
mysql_field_flags($field);
mysql_field_len($field);
mysql_field_name($field);
mysql_field_type($field);
?>
```

Query:
```php
<?php
$query_id = mysql_query("MySQL Query", $link_id);
mysql_fetch_row($query_id);
mysql_fetch_array($query_id);
mysql_data_seek($query_id, $row_number); // first row has index 0
?>
```

Query Statement Statistics:
```php
<?php
mysql_num_rows($query_id);
mysql_num_fields($query_id);
mysql_affected_rows($query_id);
mysql_insert_id($query_id); // gives back the automatically inserted id in the new row, if there's one.
?>
```

Escape query string before using it:
```php
<?php
$string = addslashes($string);
?>
```

Error:
```php
<?php
mysql_error($link_id);
?>
```

Create/remove a database:
```php
<?php
mysql_create_db("database name", $link_id);
mysql_drop_db("database_name", $link_id);
?>
```
