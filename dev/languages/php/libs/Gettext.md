# Gettext

Internationalization of applications.

 * [Gettext](https://www.php.net/manual/en/book.gettext.php).
 * [How to Build a Multilingual App: A Demo With PHP and Gettext](https://www.toptal.com/php/build-multilingual-app-with-gettext).

How to install gettext module for PHP on macos?

Do we need to enable the following line inside `php.ini`?:
```
;extension=gettext
```
