# SOAP

 * [Massbank PHP example](http://www.massbank.jp/manuals/api-doc_en/getRecordInfo.php).

Getting message sent:
```php
<?php
	$soap = new SoapClient(my_wsdl_url, array('trace' => 1));
	$res = $soap->SomeFunction($params);
	echo "REQUEST:\n" . $soap->__getLastRequest() . "\n";
	echo "REQUEST HEADERS:\n" . $soap->__getLastRequestHeaders() . "\n";
?>
```
