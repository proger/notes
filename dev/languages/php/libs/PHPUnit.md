# PHPUnit

[PHPUnit](https://phpunit.de) is a testing framework.

 * [Getting Started with PHPUnit 8](https://phpunit.de/getting-started/phpunit-8.html).
 * [Assertions](https://phpunit.readthedocs.io/en/8.4/assertions.html).

Install PHPUnit with PEAR system:
```bash
pear config-set auto_discover 1
pear install pear.phpunit.de/PHPUnit
```

Run PHPUnit in project where it is installed with Composer:
```sh
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests
```
By default `phpunit` will look for files `*Test.php` inside the specified folder (`tests`).

Get all options:
```sh
./vendor/bin/phpunit --bootstrap vendor/autoload.php -h
```

Writing a test class:
```php
<?php
use PHPUnit\Framework\TestCase;

final class MyTest extends TestCase {

	public function testSomething(): void {
	}
}
```

Miscellaneous assertions:
```php
<?php
self::assertNull($myvar);
self::assertNotNull($myvar);
self::assertFalse($myvar);
self::assertEquals(10, 11);
self::assertGreaterThan(10, 12);
self::assertGreaterThanOrEqual(10, 10);
self::assertTrue($myvar);
self::assertInstanceOf(MyClass::class, $myvar);
self::assertNotInstanceOf(MyClass::class, $myvar);
```

File assertions:
```php
<?php
self::assertFileExists($myfile);
self::assertFileNotExists($myfile);
self::assertDirectoryExists($mydir);
```

Array assertions:
```php
<?php
self::assertIsArray($myvar);
self::assertContains(1, [1, 2, 3]);
self::assertNotContains(4, [1, 2, 3]);
self::assertArraySubset([2,4], [5,4,2,1], true/*strict*/);
self::assertArrayNotSubset([2,4], [5,4,2,1], true/*strict*/);
self::assertArrayHasKey('a', ['a'=>1]);
self::assertArrayNotHasKey('a', ['b'=>1]);
self::assertEmpty([]);
self::assertNotEmpty(['a']);
self::assertCount(1, ['a']);
self::assertNotCount(2, ['a']);
```

Assert exception:
```php
<?php
final class MyTest extends TestCase {

	public function testMyExcept(): void {
		$this->expectException(MyException::class);
		call_my_method_to_test();
		# After that no code will be executed since exception is thrown.
	}
}
```
