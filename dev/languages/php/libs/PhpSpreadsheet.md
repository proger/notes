# PhpSpreadsheet

 * [PhpSpreadsheet](https://github.com/PHPOffice/PhpSpreadsheet).
 * [Welcome to PhpSpreadsheet's documentation](https://phpspreadsheet.readthedocs.io/en/latest/#learn-by-documentation).
 * [Accessing cells](https://phpspreadsheet.readthedocs.io/en/latest/topics/accessing-cells/).

Read and write MS Excel xlsx and xls documents.

```php
<?php
$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("05featuredemo.xlsx");
$sheet = $spreadsheet->getSheet(0);
```
