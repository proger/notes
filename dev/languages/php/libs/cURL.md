# cURL

The [Client URL Library](http://us3.php.net/curl).

 * [cURL Functions](https://www.php.net/manual/en/ref.curl.php).
 * [curl_setopt](https://www.php.net/manual/en/function.curl-setopt.php).

Transfering a file with cURL:
```php
<?php
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$res = curl_exec($ch); # Returns `false` in case of failure, otherwise returns the received string.
curl_close($ch);
```

Ignore certificate checking:
```php
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
```
