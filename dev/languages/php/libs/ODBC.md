# ODBC

Open & close connection:
```php
<?php
$connect = odbc_connect("mydb", "mylogin", "mypasswd");
odbc_close($connect);
?>
```

Perform a query:
```php
<?php
$result = odbc_exec($connect, "SELECT name, surname FROM users");
?>
```

Fetch the data from the database:
```php
<?php
while(odbc_fetch_row($result)) {
	$name = odbc_result($result, 1);
	$surname = odbc_result($result, 2);
	print("$name $surname\n");
}
?>
```
