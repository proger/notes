# Artichow

[Artichow](http://www.artichow.org/) is a librairy using GD2 for drawing graphics: curves, histograms, anti-spam images, ...

To change background color of a graph, don't forget to put the grid transparent:
```php
<?php
$graph->setBackgroundColor($bkg_color);
$group->grid->setBackgroundColor(New Color(0, 0, 0, 100)); // make grid transparent
?>
```

Make background transparent using color replancement in GD:
```php
<?php
$color_gd = imagecolorallocate($graph->getDriver()->resource, 0xff, 0xff, 0xff);
imagecolortransparent($graph->getDriver()->resource, $color_gd);
?>
```
