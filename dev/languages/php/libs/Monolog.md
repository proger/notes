# Monolog

Composer:
```json
{
	"require": {
		"monolog/monolog": "*"
	}
}
```

```php
<?
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('name');
$log->pushHandler(new StreamHandler('path/to/your.log', Logger::WARNING));

$log->warning('Foo');
$log->error('Bar');
```
