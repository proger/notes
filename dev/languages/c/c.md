# C
<!-- vimvars: b:markdown_embedded_syntax={'c':'','cpp':''} -->

 * [The C Progamming Language, Second Edition](https://neilklingensmith.com/teaching/loyola/cs264-s2020/readings/cbook.pdf).
 * [Rationale for International Standard— Programming Languages— C](http://www.open-std.org/jtc1/sc22/wg14/www/C99RationaleV5.10.pdf).

Books:
 * The C Programming Language, Second Edition: ANSI C Version de Brian W. Kernighan, Dennis Ritchie
 * The C Answer Book (Second Edition): Solutions to the Exercises in the C Programming Language, Second Edition, by Brian W. Kernighan and Dennis M. Ritchie

## Preprocessor

 * [The GNU C Preprocessor](http://tigcc.ticalc.org/doc/cpp.html).

### Error & warning messages

Error:
```c
#error "An error !"
```

Warning (non standard):
```c
#warning "Some warning message."
```

### Macros

Concatening strings in a macro:
```c
#define TOTO(index, name) name##_##index
```

Transform a macro value into a C string:
```c
#define TOTO(param) # param
#define TOTO(param) #param
```

Use of semi-colon with multi-lines macros:
```c
#define CMDS \
 do { \
   a = b; \
   c = d; \
 } while (0)

if (var == 13)
	CMDS;
```

### Compiler built-in macros

Macro          | Compiler | Description
-------------- | -------- | -----------------------
`__clang__`    | Clang    | Clang compiler.
`__APPLE__`    | Clang    | Apple computer.
`__APPLE_CC__` | Clang    | Apple C compiler version.

### Conditions (if else endif)

```c
 #ifdef MY_MACRO
 #elif
 #else
 #endif

 #ifndef MY_MACRO
 #endif

 #if defined MY_MACRO
 #endif

 #if ! defined MY_MACRO
 #endif

 #if defined MACRO_1 && defined MACRO_2
 #endif
```

## Built-in types

### Declaring constant

```c
1234; /* int */
123456789l; /* long */
123456789L; /* long */
037; /* octal, starts with 0 */
0x1f; /* hexadecimal int, starts with 0x or 0X */
0X1f; /* hexadecimal int, starts with 0x or 0X */
0xFUL; /* unsigned long with decimal value 15 */
123.4; /* double */
123.4f; /* float */
123.4F; /* float */
123.4l; /* long double */
123.4L; /* long double */
```

### Number if bits in one byte and sizeof(char)

`char` is always one byte.
One byte is most often 8 bits, except on particular CPU architecture.
This can be checked at compile time with:
```c
static_assert(CHAR_BIT == 8)
```

## Variables & types

### Initialization

An automatic variable is one that is automaticaly created and destroyed.
Function parameters and other local variables are automatic variables.

Automatic variables are never initialized (Except in debug mode of some compilers ; be careful !).
Extern and static variables are initialized to zero by default.
Extern and static variables are only initialized once, and before program starts executing.

Array initialization:
```c
int arr[10]; /* not initialized */
int arr[10] = {0}; /* first and all following values are initialized to 0 */
```

### Volatile

A *volatile* specified is a hint to a compiler that an object may change its value in ways _NOT_ SPECIFIED BY THE LANGUAGE so that aggressive optimizations must be avoided. For example a real time clock might be declared:
```c
extern const volatile clock;
```

### Enum

Defining an enumeration:
```c
enum days {mon, tues, wed, thu, fri, sat, sun};
```

Declaring a variable:
```c
enum day a_day;
```

### Struct

Define a structure:
```c
struct my_struct {
	int a;
	float x;
};
```

Define a structure and name it with typedef:
```c
typedef struct my_struct {
	int a;
	float x;
} tMyStruct;
```

Initialize members in C99:
```c
struct MyType {
    int n;
};
struct MyType s = { .n = 0, };
```

### String

Get length of zero terminated string:
```c
#include <string.h>
size_t len = strlen(s);
```

Search for a character:
```c
#include <string.h>
char *p = strchr(s, 'a'); 
```

Convert string to integer:
```c
#include <inttypes.h>
int i = strtoi();
```

## Statements

### switch

```c
switch (n) {
    case 1:
        do_something();
        break;
    default:
        do_something_else();
}
```

### inline

 * [Inline Functions In C](https://www.greenend.org.uk/rjk/tech/inline.html).

### restrict

C99 standard.

`restrict` tells the compiler that the memory accessed through that pointer
will not be accessed by other pointers for the current scope:
```c
{
    char *restrict p;
    /* ... */
}
```

### Operators

Precedence and associativity of operators.
From high precedence (top) to low (bottom).

Operators                                                                | Associativity
------------------------------------------------------------------------ | -------------
`()`, `[]`, `->`, `.`                                                    | Left to right.
Unary ops:`!`, `~`, `++`, `--`, `+`, `-`, `*`, `&`, `(type)`, `sizeof()` | Right to left.
`*`, `/`, `%`                                                            | Left to right.
`+`, `-`                                                                 | Left to right.
`<<`, `>>`                                                               | Left to right.
`<`, `<=`, `>`, `>=`                                                     | Left to right.
`==`, `!=`                                                               | Left to right.
`&`                                                                      | Left to right.
`^`                                                                      | Left to right.
`|`                                                                      | Left to right.
`&&`                                                                     | Left to right.
`||`                                                                     | Left to right.
`?:`                                                                     | Right to left.
`=`, `+=`, `-=`, `*=`, `/=`, `%=`, `&=`, `^=`, `|=`, `<<=`, `>>=`        | Right to left.
`,`                                                                      | Left to right.

Unary `+`, `-`, and `*` have higher precedence than the binary forms.

#### Bitwise operators

 * [Bitwise operations in C](https://en.wikipedia.org/wiki/Bitwise_operations_in_C).

### Extern

Accessing outside global variables:
```c
int max;

void foo() {
	int i;
	extern int max; /* declare outside of function.
	                 * This is necessary if 'max' is defined in another source
	                 * file, or in the same source file but after this
	                 * function. */

	/* ... */
}
```

Using header file to declare extern variables:
In `file.c`:
```c
int max;
```

In `file.h`:
```c
extern int max;
```

### Static

Static keyword is only used inside a .c source file.

A static external (i.e.: global) variable is invisible outside the source file where it is defined:
```c
static int my_global_var = 3;
```

A static internal (i.e.: local) variable is kept alive between all the calls of the function:
```c
int foo() {
	static int my_static_var = 1; /* created only once */
	/* At each call of foo, my_static_var keeps the same value it had at the last call of foo. */
}
```

A static function cannot be seen outside the source file where it is defined:
```c
static int foo() {}
```

### Label

Using a label:
```c
goto my_label;
/*...*/
my_label:
```

### Functions

```c
Function pointers:
void foo(int(*fct_ptr)(char), char c) {
	int i = (*fct_ptr)(c);
}
```

### main function

The `main()` function is the entry point of the program.

Skeleton for a main function:
```c
int main(int argc, char *argv[]) {
    return 0;
}
```

`argc` is the number of arguments.
`argv` contains the argument values.
Argument 0 is the program's name.
