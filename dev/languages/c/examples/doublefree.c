#include <stdio.h> 
#include <stdlib.h> 

int main() 
{ 
	char *str; 

	/* Initial memory allocation */ 
	str = (char *) malloc(15); 
	strcpy(str, "CallFreeTwice"); 

	free(str); 

	//str=NULL; //if i UN-commnet, there won't be any crash 

	free(str); 

	return(0); 
} 
