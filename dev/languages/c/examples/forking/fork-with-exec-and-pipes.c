#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int child(int in, int out) {
    
    close(0); /* close standard input  */
    close(1); /* close standard output */

    if (dup(in) != 0) {
        fprintf(stderr, "Error when binding socket %d to file descriptor 0"
                " (stdin).", in);
        exit(10);
    }

    if (dup(out) != 1) {
        fprintf(stderr, "Error when binding socket %d to file descriptor 1"
                " (stdout).", out);
        exit(11);
    }

    fprintf(stderr, "This is the child process with PID %d.\n", getpid());

    if (execlp("cat", "cat", "-n", "-E", (char*)NULL)) {
        fprintf(stderr, "Error %d when running command: %s.\n", errno,
                strerror(errno));
        exit(12);
    }

    return 0;
}

int main(int argc, char *argv[]) {

    int ret = 0;

    // Open pipes:
    int in_pipe[2];
    int out_pipe[2];
    if (pipe(in_pipe)) {
        fprintf(stderr, "Error %d when trying to open a pipe: %s.\n", errno,
                strerror(errno));
        exit(2);
    }
    if (pipe(out_pipe)) {
        fprintf(stderr, "Error %d when trying to open a pipe: %s.\n", errno,
                strerror(errno));
        exit(3);
    }

    // Fork
    int child_pid = fork();
    
    // Check error
    if (child_pid < 0) {
        fprintf(stderr, "Call to fork() failed with err %d: %s.\n", errno,
                strerror(errno));
        exit(1);
    }

    // Parent
    else if (child_pid) {
        printf("This is the parent process with PID %d, child PID is %d.\n",
                getpid(), child_pid);

        // Send message to child
        dprintf(in_pipe[1], "A message from parent %d.\n", getpid());

        // Allocate buffer
        int buf_len = 128;
        char* buf = (char*)malloc(buf_len * sizeof(char));

        // Read message from child
        while (true) {
            int c = read(out_pipe[0], buf, buf_len);
            if (c < 0) {
                fprintf(stderr, "Error %d when trying to read message from"
                        " child process %d: %s.\n", errno, child_pid,
                        strerror(errno));
                exit(4);
            }
            if (c > 0) {
                printf("Received message \"%s\" from child %d.\n", buf,
                        child_pid);
                break;
            }
        }

        // Free buffer
        free(buf);
    }

    // Child
    else
        ret = child(in_pipe[0], out_pipe[1]);

    return ret;
}
