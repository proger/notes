#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

    printf("Hello, This is the main process starting, with PID %d.\n",
            getpid());
    
    // Fork
    int pid = fork();
    
    // Check error
    if (pid < 0) {
        fprintf(stderr, "Call to fork() failed with err %d: %s.\n", errno,
                strerror(errno));
        return 1;
    }
    
    // Parent
    if (pid) {
        printf("Hello, I am the parent process with PID %d.\n", getpid());
        printf("My child process has PID %d.\n", pid);
    }

    // Child
    else {
        printf("Hello, I am the child process with PID %d.\n", getpid());
    }

    return 0;
}
