#define FIRSTNAME_FILE "Prenoms.csv"

#include <errno.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

/* CSV reading library
 * http://sourceforge.net/projects/libcsv/
 * https://aur.archlinux.org/packages/libcsv
 */
#include <csv.h>

#define FEMALE 1
#define MALE   2

typedef struct {
	char* firstname;
	int gender;
} tFirstname;

typedef struct {
	size_t n_firstnames;
	size_t max_name_length;
	tFirstname *firstnames;
} tInputData;

struct csv_index {
	size_t col;
	size_t row;
	tInputData *id;
	tFirstname *ifn;
};

typedef struct {

	/* Neurons */
	int   nbNeurons;
	float *activation;
	float *threshold;

	/* Links */
	int   nbLinks;
	float *weight;
} tNetwork;

void csv_column_value_callback (void *s, size_t len, void *data) {
	struct csv_index *idx = (struct csv_index *)data;
	if (idx->row > 0) {

		// Parse firstname
		if (idx->col == 0) {

			// We remove indices " (<number>)" that indicate duplicates
			// aeron (1);m,f;welsh;0
			char *end = (char*)s + len;
			for (char* p = (char*)s ; p != end ; ++p)
				if (*p == '(') {
					len = p - (char*)s - 1;
					break;
				}

			char* fn = (char*)malloc((len+1) * sizeof(char));
			strncpy(fn, (char*)s, len);
			fn[len] = 0;
			idx->ifn->firstname = fn;

			if (len > idx->id->max_name_length)
				idx->id->max_name_length = len;
		}

		// Parse gender
		if (idx->col == 1) {
			if (strncmp("m", (char*)s, 1) == 0)
				idx->ifn->gender = MALE;
			else if (strncmp("f", (char*)s, 1) == 0)
				idx->ifn->gender = FEMALE;
			else
				idx->ifn->gender = MALE | FEMALE;
		}

		++idx->col;
	}
}

void cvs_end_of_row_callback (int end_record_char, void *data) {
	struct csv_index *idx = (struct csv_index *)data;
	if (idx->row > 0)
		++idx->ifn;
	++idx->row;
	idx->col = 0;
}

size_t get_nb_lines(FILE *fp) {

	size_t n = 0;
	int c;
	bool in_line = false;

	while((c = fgetc(fp)) != EOF) {
		if (c == '\n')
			in_line = false;
		else if ( ! in_line) {
			++n;
			in_line = true;
		}
	}

	fseek(fp, 0, SEEK_SET);

	return n;
}

void read_firstnames(const char* file, tInputData *input_data) {

	struct csv_parser p;
	char buf[1024];
	size_t bytes_read;
	struct csv_index idx = {0, 0, NULL};

	if (csv_init(&p, 0) != 0)
		exit(EXIT_FAILURE);
	csv_set_delim(&p, ';');

	// Open file
	FILE * fp = fopen(file, "rb");
	if ( ! fp)
		exit(EBADF);

	// Get number of lines
	input_data->n_firstnames = get_nb_lines(fp) - 1;

	// Allocate table
	input_data->firstnames = (tFirstname*)malloc(input_data->n_firstnames * sizeof(tFirstname));
	idx.id = input_data;
	idx.ifn = input_data->firstnames;

	// Read data
	while ((bytes_read=fread(buf, 1, 1024, fp)) > 0) {
		if (csv_parse(&p, buf, bytes_read, csv_column_value_callback, cvs_end_of_row_callback, &idx) != bytes_read) {
			fprintf(stderr, "Error while parsing file: %s\n", csv_strerror(csv_error(&p)) );
			exit(EXIT_FAILURE);
		}
	}

	// Close file
	fclose(fp);

	csv_free(&p);
}

const char* get_alphabet(tInputData input_data) {

	int chars[256];
	memset(chars, 0, sizeof(int) * 256);

	tFirstname *p = input_data.firstnames;
	tFirstname *end = input_data.firstnames + input_data.n_firstnames;
	for ( ; p != end ; ++p) {
		const char *q_end = p->firstname + strlen(p->firstname);
		for (const char* q = p->firstname ; q != q_end ; ++q) {
			int i = (unsigned char)*q;//(int)*q + 0x80;
			chars[i] = 1;
		}
	}

	// Build string
	int n = 0;
	for (int i = 0 ; i < 256 ; ++i)
		n += chars[i];
	char *s = (char*)malloc(sizeof(char) * (n+1));
	char *ps = s;
	for (int i = 0 ; i < 256 ; ++i)
		if (chars[i]) {
			*ps = (char)i;
			++ps;
		}
	s[n] = 0;

	return s;
}

int main(int argc, char* argv[]) {

	tInputData input_data = {0, 0, NULL};

	/* Read input data */
	read_firstnames(FIRSTNAME_FILE, &input_data);
	printf("There are %d firstnames, including duplicates.\n", input_data.n_firstnames);
	printf("The longest firstname has %d characters.\n", input_data.max_name_length);

	/* Get alphabet */
	const char* alphabet = get_alphabet(input_data);
	printf("The alphabet contains %d characters: %s\n", strlen(alphabet), alphabet);

	return EXIT_SUCCESS;
}
