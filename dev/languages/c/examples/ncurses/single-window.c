// vi: fdm=marker

#include <curses.h>
#include <stdlib.h> // for EXIT_SUCCESS
#include <unistd.h> // for sleep()

// Main {{{1
////////////////////////////////////////////////////////////////

int main(int argc, char* argv[]) {

	// Initialize ncurses
	WINDOW *screen = initscr();

	// Print text
	addstr("blabla");
	refresh();

	// Wait
	sleep(3);

	// Terminate ncurses
	delwin(screen);
	endwin();

	return EXIT_SUCCESS;
}
