// test_createprocess.cpp : Defines the entry point for the console application.
//

#include <stdlib.h>
#include <Windows.h>
#include <stdio.h>
#include <tchar.h>

void ascii_version() {
	STARTUPINFOA si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

	if ( ! CreateProcessA(NULL, "dir", NULL, NULL, FALSE, 0,
		NULL, NULL, &si, &pi))
		printf("FAILED ! ERR = %d\n", GetLastError());

	// Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );}

void normal_version() {
	STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

	TCHAR tempCmdLine[MAX_PATH * 2];
	_tcscpy_s(tempCmdLine, MAX_PATH *2, L"dir"); /* CreateProcessW must be able to write into command line argument 
												 otherwise there'll be an access violation. */
	if ( ! ::CreateProcess(NULL, tempCmdLine, NULL, NULL, FALSE, 0,
		NULL, NULL, &si, &pi))
		wprintf(L"FAILED ! ERR = %d\n", GetLastError());

	// Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );}

int _tmain(int argc, _TCHAR* argv[])
{
	ascii_version();
	normal_version();
	return 0;
}

