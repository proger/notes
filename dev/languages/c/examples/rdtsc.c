/* rdtsc() (read TSC) function. Reads Time Stamp Counter (which is a CPU register) which gives a count of elapsed CPU cycles.
  To convert this number in time, divide it by the CPU clock frequency. */

#if defined(__i386__)

static __inline__ unsigned long long rdtsc(void)
{
  unsigned long long int x;
  __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
  return x;
}
#elif defined(__x86_64__)

// typedef unsigned long long int unsigned long long;

static __inline__ unsigned long rdtsc(void)
{
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( (unsigned long )lo)|( ((unsigned long )hi)<<32 );
}

#elif defined(__powerpc__)

typedef unsigned long long int unsigned long long;

static __inline__ unsigned long long rdtsc(void)
{
  unsigned long long int result=0;
  unsigned long int upper, lower,tmp;
  __asm__ volatile(
		   "0:                  \n"
		   "\tmftbu   %0           \n"
		   "\tmftb    %1           \n"
		   "\tmftbu   %2           \n"
		   "\tcmpw    %2,%0        \n"
		   "\tbne     0b         \n"
		   : "=r"(upper),"=r"(lower),"=r"(tmp)
		   );
  result = upper;
  result = result<<32;
  result = result|lower;

  return(result);
}

#endif
