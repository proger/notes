// vi: fdm=marker

// Sound file downloaded from https://freesound.org/people/willc2_45220/sounds/75134/.
// Code written following example from https://gist.github.com/armornick/3447121.

#include <SDL2/SDL.h>

#define MUS_PATH "gong.wav"

typedef struct my_user_data my_user_data;
struct my_user_data {
	Uint8 *audio_pos; // global pointer to the audio buffer to be played
	Uint32 audio_len; // remaining length of the sample we have to play
};

// Audio callback {{{1
////////////////////////////////////////////////////////////////

void my_audio_callback(void *userdata, Uint8 *stream, int len) {

	if (((my_user_data*)userdata)->audio_len ==0)
		return;

	len = ( len > ((my_user_data*)userdata)->audio_len ? ((my_user_data*)userdata)->audio_len : len );

	SDL_memcpy (stream, ((my_user_data*)userdata)->audio_pos, len);                   // simply copy from one buffer into the other
	// OR
	//SDL_memset(stream, 0, len);
	//SDL_MixAudio(stream, audio_pos, len, SDL_MIX_MAXVOLUME);// mix from one buffer into another

	((my_user_data*)userdata)->audio_pos += len;
	((my_user_data*)userdata)->audio_len -= len;
}

// Main {{{1
////////////////////////////////////////////////////////////////

int main(int argc, char* argv[]) {

	// Initialize SDL.
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		return 1;

	// local variables
	Uint32 wav_length; // length of our sample
	Uint8 *wav_buffer; // buffer containing our audio file
	SDL_AudioSpec wav_spec; // the specs of our piece of music
	my_user_data userdata;

	/* Load the WAV */
	// the specs, length and buffer of our wav are filled
	if (SDL_LoadWAV(MUS_PATH, &wav_spec, &wav_buffer, &wav_length) == NULL)
		return 1;

	// set user data
	userdata.audio_pos = wav_buffer; // copy sound buffer
	userdata.audio_len = wav_length; // copy file length

	// set the callback function
	wav_spec.callback = my_audio_callback;
	wav_spec.userdata = (void*)&userdata;

	/* Open the audio device */
	if (SDL_OpenAudio(&wav_spec, NULL) < 0) {
		fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
		exit(-1);
	}
	
	/* Start playing */
	SDL_PauseAudio(0);

	// wait until we're done playing
	while (userdata.audio_len > 0)
		SDL_Delay(100); 
	
	// shut everything down
	SDL_CloseAudio();
	SDL_FreeWAV(wav_buffer);

	return EXIT_SUCCESS;
}
