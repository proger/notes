# Windows API

 * [Making client and server using win32 socket](http://www.go4expert.com/articles/client-server-using-win32-socket-t19761/).

## API version

To use the highest available Windows platform:
```c
 #include <SDKDDKVer.h>
```

To use another version:
```c
 #include <WinSDKVer.h>
 #define _WIN32_WINNT <myversion>
 #include <SDKDDKVer.h>
```

## CreateProcess

Makes a system call.

[CreateProcess function](https://msdn.microsoft.com/en-us/library/windows/desktop/ms682425(v=vs.85).aspx).

```c
 #include <Windows.h> /* for Winbase.h */
```

DLL: `Kernel32.dll`.

```c
if (CreateProcess(NULL, "/the/path/to/my/programm")) {
	/*...*/
}
```

## Error reporting

When an application crashes, the default behaviour of Windows is to display the [error reporting](https://msdn.microsoft.com/en-us/library/bb513641.aspx) (one of the window popups that appear on the screen).

To avoid the display of this window, you can call the [`WerAddExcludedApplication()`](https://msdn.microsoft.com/en-us/library/bb513617.aspx) function from within your application.

## GetLastError

Returns code of last error met by the Windows API:
```c
DWORD err = GetLastError();
```

A string message associated to the error code can be retrived using the `FormatMessage` function:
```c
LPTSTR errorText = NULL;
DWORD len = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&errorText, 0, NULL);
if ( NULL != errorText ) {
	/*...*/
	LocalFree(errorText);
}
```
See [How should I use FormatMessage() properly in C++?](http://stackoverflow.com/questions/455434/how-should-i-use-formatmessage-properly-in-c).

Macro                     | Code      | Description
------------------------- | --------- | ----------------------------------
`ERROR_INVALID_PARAMETER` | 87 (0x57) | The parameter is incorrect.
