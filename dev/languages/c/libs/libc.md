# libc / C POSIX library
<!-- vimvars: b:markdown_embedded_syntax={'c':'','cpp':''} -->

The C POSIX library is a specification that is a superset of libc.

 * [The C Progamming Language, Second Edition](https://neilklingensmith.com/teaching/loyola/cs264-s2020/readings/cbook.pdf).
 * [C POSIX library](https://en.wikipedia.org/wiki/C_POSIX_library).
 * [C standard library](https://en.wikipedia.org/wiki/C_standard_library#Header_files).

Using libc headers in C++:
```cpp
#include <climits>  // instead of limits.h
#include <cfloat>   // instead of float.h
#include <cassert>  // instead of assert.h
```

## assert.h

```c
#include <assert.h>
assert(i == 2);
```

If macro `NDEBUG` is defined, then all assert macro are disabled.

## ctypes.h

Lower/upper case:
```c
#include <ctype.h>
char C = 'A';
char c = tolower(C); /* c = 'a' */
char D = toupper(c); /* D = 'A' */
```

Special escaped characters:
```c
'\012'; /* character of octal code 012 */
'\x4b'; /* character of hexadecimal code 0x4b */
'\a'; /* alert (bell) */
'\b'; /* backspace */
'\f'; /* formfeed */
'\n'; /* newline */
'\r'; /* carriage return */
'\t'; /* horizontal tab */
'\v'; /* vertical tab */
'\\'; /* backslash */
'\?'; /* question mark */
'\''; /* single quote */
"\""; /* double quote */
```

## errno.h

Header:
```c
#include <errno.h>
```

Provides `EXIT_SUCCESS` and `EXIT_FAILURE` values, and many other error numbers.

See `errno` tool in `moreutils` package to get description of error numbers.

## float.h

Limits:
```c
#include <float.h>
FLT_MAX;
DBL_MAX;
```

## getopt.h

Header:
```c
#include <getopt.h>
```

`getopt_long()`:
```c
#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
    int c;
    int iterations = 0;
    float decay = 0.0f;
    int option_index = 0;
    static struct option long_options[] = {
        {"decay",  required_argument, 0, 'd'},
        {"iteration_num",  required_argument, 0, 'i'},
        {0, 0, 0, 0}
    };

    while ((c = getopt_long (argc, argv, "d:i:",
        long_options, &option_index)  ) !=-1) {
        /* getopt_long stores the option index here. */

        switch (c) {
            case 'i':
                // ...
                break;

            case 'd':
                // ...
                break;
        }
    }
    return 0;
}
```

## limits.h

Maximum number of characters a path can have:
```c
 #include <limits.h>
char file[MAX_PATH];
```

Compute maximum of built-in types:
```c
#define MAX_UINT (~(unsigned int)0)
#define MAX_INT ((int)(MAX_UINT >> 1))
#define MIN_INT (- MAX_INT - 1)
```
In a 32-bit machine:
 * `MAX_UINT = 2^32 - 1`
 * `MAX_INT = 2^31 - 1`
 * `MIN_INT = -2^31`

Limits:
```c
#include <limits.h>
LONG_MAX;
LONG_MIN;
```

## stdio.h

Getting a char from STDIN:
```c
#include <stdio.h>
int c = getchar();
if (c == EOF)
    do_something_on_end_of_file();
```

Putting/writing a char on STDOUT:
```c
 #include <stdio.h>
if (putchar(c) == EOF)
    something_wrong_happened();
```
`putchar()` returns the character written if everything went right, otherwise it returns EOF.

Open a file for reading:
```c
 #include <stdio.h>
fd = fopen(file, "r");
```

Close a file:
```c
fclose(fd);
```

Read all float values from a file:
```c
 #include <stdio.h>
while (EOF != fscanf(fd, "%f", p++))
    ;
```

Open a file for writing:
```c
 #include <stdio.h>
fd = fopen(file, "w");
```

Character position in file:
```c
#include <stdio.h>
fpos_t file_pos;
```

write to a file:
```c
 #include <stdio.h>
fprintf(fd, "%.3f\n", x);
```

Flag | Description
---- | -----------
`c`  | Character.
`s`  | String.
`d`  | Decimal.
`h`  | Hexadecimal.
`e`  | Double with exponent style.
`f`  | Plain double (no exponent).
`g`  | Double, choose between e and f, which is the best.

`L` is a modifier to `e`, `f` and `g`, for `long double`:
```c
printf("%Lf", mylongdouble);
```

`l` and `ll` are modifiers for `d` and `h`, for respectively `long` and `long long`:
```c
printf("%ld", mylong);
printf("%lld", mylonglong);
```

## stdlib.h

 * [C Standard Library](http://www.cs.unibo.it/~sacerdot/doc/C/stdlib.html).

```c
#include <stdlib.h>
```

`sizeof` operator:
```c
int n;
size_t sz = sizeof(n);
```

Create an array of n int:
```c
int *v = (int*)malloc(sizeof(int)*n);
```

Get an environment variable value:
```c
char *myenvvar_value = getenv("MYENVVAR");
```

Create an environment variable (no value):
```c
if ( ! putenv("MYENVVAR"))
    something_went_wrong();
```

Set an environment variable with a value:
```c
if ( ! setenv("MYENVVAR", "MYVALUE", 1 /*overwrite*/))
    something_went_wrong();
```

Get a random number between `0` and `RAND_MAX` included:
```c
#include <stdlib.h>
int n = rand();
```
    
Set the seed, using current time in seconds (`unsigned integer`):
```c
#include <time.h>
#include <stdlib.h>
srand(time(NULL));
```
    
`random()` gives better random numbers than `rand()`.
```c
#include <stdlib.h>
long n = random(); /* [0, (2**31)-1] */
```
`random()&01` will produce a random binary value.

### atoi

Convert a string to an integer:
```c
int i = atoi(s);
```
Returns `0` in case of error.

### free

**Double freeing** a block of memory leads to unpredictable result (implementation specific).
The application may crash, or send an error message like the following one:
    doublefree(79833,0x7fff77cc3310) malloc: *** error for object 0x7fb3ebc03a50: pointer being freed was not allocated, set a breakpoint in malloc_error_break to debug.

### strtol

Transform string to integer:
```c
long i = strtol(s, (char **)NULL, 10/*base*/);
```
Check `errno` for errors.

## string.h

### memset

Write n time the same byte value at pointer p:
```c
#include <string.h>
memset(p, byte_value, n);
```

### strchr

Search for a character in a string:
```c
#include <string.h>
char* p = strchr(s, 'a');    /* strchr() is the current standard, and is recognized by MSVC. */
```

### strerror

Print message describing error number:
```c
#include <string.h>
printf("ERROR: %s", strerror(errno));
```

### strcmp

Compare strings.

## strings.h

Search for a character in a string:
```c
#include <strings.h>
char* p = index(s, 'a');
```
`index()` is not standard anymore, and isn't recognized by MSVC.

## sys/stat.h

Test if a folder exists:
```c
#include <sys/stat.h>
#include <sys/types.h>

struct stat info;

if (stat(mypath, &info) == 0 && (info.st_mode & S_IFDIR))
    /* Is a directory */
```

## time.h

Header:
```c
#include <time.h>
```

`clock()` measures the amount of processor time used since
the invocation of the process. It doesn't take idle time
into account.
To take into account idle time (like when blocking in a
thread, or waiting for network answer, etc), one must
measure wall clock time (see `gettimeofday()`).
```c
clock_t t = clock();
```

Compute the number of seconds elapsed since the start of the program:
```c
float seconds = t / CLOCKS_PER_SEC;
```

For measuring wall clock time, one must use the `time()` function. It returns the value of time in seconds since 0 hours, 0 minutes, 0 seconds, January 1, 1970, Coordinated Universal Time, without including leap seconds. It returns -1 if an error occured.
```c
time_t t = time(NULL);
```
or
```c
time(&t);
```

To get microseconds resolution, use `gettimeofday()` function:
```c
#include <sys/time.h>
struct timeval tp;
gettimeofday(&tp, NULL);
time_t      seconds_since_1st_jan_1970 = tp.tv_sec;
suseconds_t microseconds               = tp.tv_usec;
```

## unistd.h

Header:
```c
#include <unistd.h>
```

Get current working directory (works on Linux):
```c

char *cwd = get_current_dir_name();
/* ... */
free(cwd);
```

Get current working dir with `getcwd()` (BSD & XOPEN), deprecated):
```c
#define MYBUFSIZE 1024
char *mybuf = malloc(sizeof(char) * MYBUFSIZE);
char *cwd = getcwd(mybuf, MYBUF_SIZE);
if ( ! cwd)
    error_occured(); /* buf is not big enough ! */
```

Get current working dir with getwd():
```c
char *cwd = getwd(buf); // size of buf must be at least PATH_MAX
if ( ! cwd)
    error_occured(); /* buf is not big enough ! */
```

`getopt()` (with short options):
```c
#include <unistd.h>

int main(int argc, char *argv[]) {
    int c;
    int flag;
    char *c_param = NULL; 

    while ((c = getopt(argc, argv, "ab:c:")) != -1)
        switch (c) {
            case 'a':
                flag = 1;
                break;

            case 'b':
                printf("a string argument: %s\n", optarg);
                break;

            case 'c':
                c_param = (char*)malloc(strlen(optarg) + 1);
                strcpy(c_param, optarg);
                break;
        }
    argc -= optind;
    argv += optind;

    /* Read remaining arguments */
}
```

Sleep n seconds:
```c
 #include <unistd.h>
int n = 10;
if (int unslept = sleep(n))
    printf("Sleep has been interrupted by a signal after %d seconds.",
        n - unslept);
```

Sleep n micro-seconds sleep:
```c
 #include <unistd.h>
if (usleep(300))
    /* some error occured */;
```
