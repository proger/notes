MATHGL
======

[Official Site](http://mathgl.sourceforge.net/).

Languages: C++, C, Fortran, Python and Octave.

Math Graphing library.

# LPSolve

Mixed Integer Linear Programming (MILP) solver.

Install in OSX using [homebrew-science](https://github.com/Homebrew/homebrew-science):
```bash
brew tap homebrew/science
brew install lp_solve
```

OPENCV
======

C/C++ computer vision and machine learning library.
Multiplatform (Windows, Linux/Mac, iOS, Android).
Python and Java connectors.
Focused on real-time processing.

See [official site](http://opencv.org).

GSL
===

[GNU Scientific Library](http://www.gnu.org/software/gsl/).

C/C++.

## MS Visual Studio 2010
Use gsl-1.14-vc10.zip with 1.14 sources to compile GSL under VS-2010.

Declare macro GSL_DLL when compiling & linking with another project, otherwise global variables won't be correctly exported.

XERCES
======

[Xerces-C++ XML Parser](https://xerces.apache.org/xerces-c/).

<!--********************************************************************************-->
<!--********************************************************************************-->
<!--********************************************************************************-->
===================== GTK+.txt
MacOS-X version
===============
http://gtk-osx.sourceforge.net/
===================== SWIG.txt
http://www.swig.org/

Connects programs written in C and C++ with high-level programming languages.

Languages supported
===================
Perl, PHP, Python, Tcl and Ruby.
C#, Common Lisp (CLISP, Allegro CL, CFFI, UFFI), Java, Lua, Modula-3, OCAML, Octave and R.
===================== libpng.txt
COMPILING UNDER WINDOWS
=======================
First edit zlib.props inside :
C:\Documents and Settings\pierrick\My Documents\Visual Studio 2010\Projects\libpng-1.4.4\projects\vstudio

and set the right values for ZLib1Dir and ZlibSrcDir.

Then open the Visual Studio 2010 solution file inside the same directory.

Don't forget to set preprocessor macros :
ZLIB_WINAPI							--> for use of ZLIB
===================== zlib.txt
COMPILING UNDER WINDOWS
=======================
First compile the assembler code :
C:\Documents and Settings\pierrick\My Documents\Visual Studio 2010\Projects\zlib-1.2.5\contrib\masmx86

Then use the Visual Studio 2010 project to compile the library under Windows :
C:\Documents and Settings\pierrick\My Documents\Visual Studio 2010\Projects\zlib-1.2.5\contrib\vstudio\vc10

Declare ZLIB_WINAPI macro when using the library.
===================== MathGL/axis.txt
// -*- mode:c++ -*-

// to draw axis labels/values
gr->Axis();

// to set ranges of axes
gr->SetRanges(min_x, max_x, min_y, max_y);

// to set axis labels/values
gr->SetTicksVal('x', 6, -M_PI, "-\\pi", -M_PI/2, "-\\pi/2", 0., "0", 0.886, "x^*", M_PI/2, "\\pi/2", M_PI, "\\pi");
===================== MathGL/image.txt
// -*- mode:c++ -*-

// Set image size
gr->SetSize(800 /*width*/, 600 /*height*/);

/* ShowImage
   show image using external viewer.
   by default use 'kuickshow' application (KDE ?)
   So the viewer must be change for other environments
*/
gr->ShowImage("mspaint", true); // under Windows
gr->ShowImage("xv"); // under MacOS-X (with MacPorts) and other UNIX systems

/* Write file */
gr->WritePNG("filename");
gr->WritePNG("filename", "description", true/*alpha*/);
===================== MathGL/install.txt
svn co https://mathgl.svn.sourceforge.net/svnroot/mathgl mathgl

Compiling under MS Visual Studio 2010
=====================================
Don't forget to set preprocessor macros :
GSL_DLL									--> for use of GSL
ZLIB_WINAPI							--> for use of ZLIB
MATHGL_DLL_EXPORT				--> or another of your choice in order to control setting of __declspec(dllexport)

Macos-X
=======
Install gsl from MacPorts.
Compile and install sources using configure script.

Windows MinGW (without MSYS)
============================
Install from binaries, for MinGW compiler.
Download and install MinGW inside C:\MinGW.
Download gsl binaries and developers files (gsl-<version>-bin.zip and gsl-<version>-lib.zip) and unpack them inside C:\MinGW.
Download MathGL binaries for MinGW (mathgtl-<version>-mingw.i686.zip) and unpack it inside C:\MinGW.

For running install giflib, jpeg and libpng inside C:\MinGW
giflib-4.1.4-1-bin.zip
giflib-4.1.4-1-dep.zip
jpeg-6b-4-bin.zip
jpeg-6b-4-dep.zip
libpng-1.2.37-bin.zip
libpng-1.2.37-lib.zip

When compiling add the following directories to the path:
C:\MinGW\bin;C:\MinGW\libexec\gcc\mingw32\4.5.0

Windows MinGW / MSYS
====================
Run the MinGW Shell.

export LIBRARY_PATH=/usr/local/lib
export CPATH=/usr/local/lib

1) GSL

2) zlib
use zlib123

3) libpng

4) giflib

5) jpeg-6b
In "make install", correct missing creation of directory /usr/local/man/man1

6) MathGL
patch -p1 <...
--> ERROR ! REJECTED !

./configure --disable-pthread --disable-shared

Linux
=====
===================== MathGL/labels.txt
// -*- mode:c++ -*-

// set labels for x and y axis
gr->Label('x',"x",0);
gr->Label('y', "y=\\sqrt{1+x^2}",0);

// add legend for a curve (listed in order)
gr->AddLegend("sin(\\pi {x^2})","b");
gr->AddLegend("sin(\\pi x)","g*");
gr->Legend();
// Couleurs par défaut: Hbgrcmyhlnqeup

// draw a string above or under a curve
gr->Text(y,"This is very long string drawn along a curve",":k");
gr->Text(y,"Another string drawn above a curve","T:r");
===================== MathGL/plot.txt
// -*- mode:c++ -*-

===================== MathGL/sample.txt
// -*- mode:c++ -*-

// simple function drawing from formula
mglGraph *gr = new mglGraphZB;
mglData y(50);
y.Modify("sin(2*pi*x)");
gr->Box();
gr->Plot(*this->y);
delete gr;

// plot values (with regular repartition on x axis)
mglGraph *gr = new mglGraphZB;
mglData y(3);
y.Put(0.2, 0);
y.Put(0.5, 1);
y.Put(0.9, 2);
gr->SetSize(640, 480);
gr->Box();
gr->SetRanges(-2.0, 1.5, 0.0, 1.0);
gr->Axis();
gr->Plot(y);
gr->WritePNG("toto.png", "",  false);
delete gr;

// plot points (x,y)
mglGraph *gr = new mglGraphZB;
mglData y(3), x(3);
x.Put(0.1, 0);
x.Put(0.2, 1);
x.Put(0.3, 2);
y.Put(0.2, 0);
y.Put(0.5, 1);
y.Put(0.9, 2);
gr->SetSize(640, 480);
gr->Box();
gr->SetRanges(-2.0, 1.5, 0.0, 1.0);
gr->Axis();
gr->Plot(x, y);
gr->WritePNG("toto.png", "",  false);
delete gr;
===================== Qt/button.txt
// -*- mode:c++ -*-

// add a button
QPushButton hello("Button title");
hello.resize(100, 30);
hello.show();
===================== Qt/connect.txt
// -*- mode:c++ -*-

// static method for connecting a method with a signal
QObject *sender, *receiver;
QObject::connect(sender, SIGNAL(valueChanged(int)), receiver, SLOT(setNum(int)));
===================== Qt/event.txt
// -*- mode:c++ -*-

// send an event
QEventType myevent = (QEvent::Type)0xd000;
QEvent e(myevent);
QApplication::sendEvent(main_window, &e);

// receive an event
bool MyClass::event(QEvent *e) {
}
===================== Qt/install.txt
Installing qt 4.7.1

install package Xext
===================== Qt/main.txt
// -*- mode:c++ -*-

// How to start a Qt application
#include <QtGui/QApplication.h>

int main (int argc, char *argv[]) {
	QApplication app(argc, argv);

	...

	return app.exec();
}
===================== Qt/moc file.txt
// -*- mode:c++ -*-

// Meta-Object Code generated by the Meta-Object Compiler
/* From Wikipedia:
  The moc tool reads a C++ header file. If it finds one or more class declarations that contain the Q_OBJECT macro, it produces a C++ source file containing the meta-object code for those classes. Among other things, meta-object code is required for the signals and slots mechanism, the run-time type information, and the dynamic property system.

  The C++ source file generated by moc must be compiled and linked with the implementation of the class.
*/

// in a MyClass.cpp file
#include "MyClass.hpp"
#include "MyClass.moc"
===================== Qt/string.txt
// -*- mode:c++ -*-

// header file of QString
#include <QString>

// library: libQtCore.a
===================== wxwidgets/image.txt
// vi: ft=cpp

wxBitmap image;
wxInitAllImageHandlers(); // initialize all image handler types
image.LoadFile(img_file.wstring(), wxBITMAP_TYPE_TIFF);

// To initialize only some of the handlers:
wxImage::AddHandler(new wxJPEGHandler);
