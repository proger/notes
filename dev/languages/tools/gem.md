# gem

Ruby package manager.

Install in user folder (`$HOME/.local/share/gem/ruby/...`):
```sh
gem install --user-install mypkg
```

```sh
sudo gem install mypackage
sudo gem uninstall mypackage
```

Quick help:
```sh
gem help
```

List all commands:
```sh
gem help commands
```

Help about one command:
```sh
gem help the_command
```
