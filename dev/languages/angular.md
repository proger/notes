# Angular

 * [Angular 7 CRUD — Part -3— Creating Web Services using HttpClient](https://medium.com/ramsatt/angular-7-crud-part-3-creating-web-services-using-httpclient-128f1947913c).

Angular is a web application framework.
It targets desktop and mobile browsers.
Angular is based on TypeScript (an improved JavaScript) by Microsoft.
Angular apps are sent to the browser's user to be executed there.
