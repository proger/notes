MATLAB
======

 * [How to run R code in Matlab](http://neurochannels.blogspot.fr/2010/05/how-to-run-r-code-in-matlab.html).
 * [R-Matlab Interface](http://www.omegahat.net/RMatlab/outline.pdf).

## Memory optimization

 * [In-place Operations on Data](http://blogs.mathworks.com/loren/2007/03/22/in-place-operations-on-data/).
 * [Resolve "Out of Memory" Errors](https://fr.mathworks.com/help/matlab/matlab_prog/resolving-out-of-memory-errors.html).
 * [Strategies for Efficient Use of Memory](https://fr.mathworks.com/help/matlab/matlab_prog/strategies-for-efficient-use-of-memory.html).

OCTAVE
======

 * [GNU Octave](https://www.gnu.org/software/octave/) is a free alternative to Matlab.
 * [Differences between Octave and MATLAB](https://en.wikibooks.org/wiki/MATLAB_Programming/Differences_between_Octave_and_MATLAB).

RUNNING
=======

In UNIX, to run Matlab as command line interpreter, run:
```bash
matlab -nojvm
```

To run a matlab command:
```bash
matlab -r 'my_script1;my_script2;quit'
```

License manager. Run as common user:
```bash
<matlab_root>/etc/lmstart
```

Matlab starts with blank grey window.
This is a bug with Java when desktop effects are enabled.
Under Debian, got to System -> Preferences -> Appearance -> Visual Effects -> None
Note: matlab shell works fine, run:
```bash
matlab -nojvm.
```

PATH
====

Add a directory to the top of the search path:
```matlab
addpath('my/dir');
path('my/dir', path);
old_path = addpath 'some/path'; % returns old path before adding new one.
```

Add a directory to the bottom of the search path:
```matlab
path(path, 'my/dir');
```

View search path:
```matlab
path
```

Set path:
```matlab
path('newpath'); % newpath is a string array of folders.
```

SCRIPT
======

```matlab
run(myscript)
```

STRINGS
=======

Remove white spaces:
```matlab
t = strtrim(s);
```

Concatenate:
```matlab
s = strcat(s1, s2, s3);
s = [s1 s2 s3];
```

FILE SYSTEM
===========

Path separator (':' in UNIX, ';' in Windows):
```matlab
pathsep
```

File separator ('/' in UNIX, '\' in Windows):
```matlab
filesep
```

Build a path from strings:
```matlab
my_file_path = fullfile('some_path', 'some_dir', 'some_other_dir', '...', 'file_name');
my_dirs_path = fullfile('some_path', 'some_dir', 'some_other_dir', '...', ''); % set empty file name for building a path of directories only
```

Current directory:
```matlab
pwd
```

Change directory:
```matlab
cd 'new/path'
```

List directory:
```matlab
dir     % Windows style
ls      % UNIX style
```

Get Matlab root directory:
```matlab
matlabroot
```

Getting full pathname of a function:
```matlab
my_string = which('function_name');
my_cell_array = which('function_name', '-ALL'); % returns all functions matching the name
```

Getting full pathname of current M-file:
```matlab
full_pathname = mfilename('fullpath');
class_name = mfilename('class'); % returns class name when called from a method
```

Splitting path:
```matlab
[path, name, ext] = fileparts('some_fullpath');
path = fileparts('some_fullpath');
```

I/O
===

Write a matrix into a CSV file, with a maximum of 5 significant digits of precision:
```matlab
csvwrite('mymatrix.csv', m)
```

Read from a CSV file:
```matlab
m = csvread('mymatrix.csv')
```

Write a matrix into a CSV file, using a custom precision:
```matlab
dlmwrite('mymatrix.csv', m, 'precision', '%.6f')
```

DISPLAY
=======

To display all digits:
```bash
format long
```

To display all digits, and do not use exponential format:
```bash
format long g
```

DIFF & COMPARE
==============

Compare two matrices/vectors:
```matlab
b = isequal(u, v);
```

IF
==

```matlab
if i == 0
	% ...
elseif i == 1
	% ...
else
	% ...
end
```

ASSERT
======

```matlab
assert(isequal(a, b));
assert(isequal(a, b), 'A and B are different.');
```

VARIABLES
=========

Clear workspace:
```matlab
clear
```

List all variables contained in the workspace:
```matlab
who
whos    % List with size and type.
```

Get type (class) of a variable:
```matlab
class(myvar)
```

TABLE
=====

Equivalent of R data frame.

To write a table into a CSV file:
```matlab
writetable(mytable, 'mytablefile.csv');
```

MATRIX
======

Construct a matrix from scalars:
```matlab
M = [1,2,3;4,5,6;7,8,9];
```

Construct a matrix from line vectors:
```matlab
M = [V1;V2;V3];
```

Inverse:
```matlab
N = inv(M);
```

Transpose:
```matlab
A = B';
A = B.';
A = transpose(B);
```

FUNCTIONS
=========

One function per file ; the name of the file must be the same as the function's name.
```matlab
function [output_arg1, output_arg2] = my_function(arg1, arg2, arg3)
```

The function file must be inside current directory, or in a directory of the path. See path.txt.

Be carefull when passing arguments to functions, parenthesis must be used when writing a function call to set a parameter:
```matlab
addpath 'new/path';     % OK
addpath('new/path');    % OK
addpath strcat('..', filesep, 'src');       % WRONG !
addpath(strcat('..', filesep, 'src'));      % OK
```

Getting multiple returned values:
```matlab
[v1, v2, v3] = my_func();
v1 = my_func();
```
`v1` will be set to the first value returned by `my_func()`, if other values are returned in an array, they will be discarded.
Even some returned values are not asked by the caller, they are computed anyway.

Arguments passing scheme
------------------------

Suppose we have the following functions:
```matlab
function B = foo_1(A)
	B = A + 1

function B = foo_2(A)
	A = A + 1
	B = A

function A = foo_3(A) % We declare that input A is also an output, and thus will be modified in-place.
	A = A + 1
```

If we call:
```matlab
D = foo_1(C)
```
A isn't modified, so Matlab makes A refers to the same data as C (i.e.: no copy of C).
B is a new matrix. And D refers to the same data as B (i.e.: no copy of B).

If we call:
```matlab
C = foo_1(C)
```
C is replaced with data from B (i.e.: no copy of B), but original data of C is deleted.

If we call:
```matlab
D = foo_2(C)
```
C is copied, and A refers to this copy, since A is modified.
B refers to the same data as A (i.e.: no copy of A)
D refers to the same data as B, and thus A (i.e.: no copy).

If we call:
```matlab
C = foo_2(C)
```
C is replaced with data from B, and thus A (i.e.: no copy of B), but original data of C is deleted.
 
If we call:
```matlab
D = foo_3(C)
```
C is copied, and A refers to this copy, since A is modified.
D refers to the same data as A (i.e.: no copy of A).

If we call:
```matlab
C = foo_3(C)
```
C isn't copied, since C is set to the result of foo_3(), which is defined to be the parameter A to which C is assigned.
So on input, A refers to data of C
and on output, C refers to data of A, which is the data of C itself.
INSIDE ANOTHER FUNCTION: This code behaves exactly as a passing by reference.
INSIDE COMMAND WINDOW: A copy is created, and C internal data is a new array.

Calling a function in a loop
----------------------------

```matlab
function A = inc(A)
	A = A + 1;

for n = 1:5
	M = inc(M);
end
```

INSIDE COMMAND WINDOW: two structures are used alternatively when calling inc, there isn't creation of a new structure at each step of the loop.
INSIDE ANOTHER FUNCTION: only one structure is used, the one of M.
To test it, use "format debug" mode, and run without the semicolons and a small matrix M.

JAVA
====

 * Java-to-Matlab. MatLab can be called from Java code with the JMI `Java-to-Matlab` interface delivered with MatLab. Look for `jmi.jar` inside MatLab installation directory.
 * [Matlab Control](https://code.google.com/p/matlabcontrol/).

OOP
===

Some kind of object oriented programming can be used in Matlab. See <http://www.mathworks.fr/help/techdoc/matlab_oop/ug_intropage.html>.

Using R from Matlab (under UNIX)
================================

RMatlab is a wonderful package I just discovered.  It interfaces R and Matlab.  I use it to make sexy figure from within Matlab.  (The alternative is to export data, import it to R, and make a figure.  It's possible, but a big pain.)  The package works great, but I found it slightly difficult to get working.  Here are the steps I took.

Prerequisites

A recent version of R; I'm using R 2.9.2 from the R debian repositories.
A working mex compiler.
A unix environment.
Installation

Download the file `RMatlab_0.2-5.tar.gz` from <http://www.omegahat.org/RMatlab/>.

Unzip
```bash
tar xzvf RMatlab_0.2-5.tar.gz 
```

Download apply the provided patch <http://www.stanford.edu/~dgleich/notebook/WindowsLiveWriter/RMatlabUsingRfromMatlab_CD8A/RMatlab.patch>.
```bash
cd RMatlab
patch -p1 < ../RMatlab.patch 
```

Configure and install:
```bash
export R_HOME=/usr/lib64/R
export R_INCLUDE_DIR=/usr/share/R/include
./configure 
cd ..
R CMD INSTALL RMatlab
```

Running:
```bash
source RMatlab/inst/scripts/RMatlab.sh
matlab -nojvm -nodisplay
```
					  
```matlab
initializeR({'RMatlab'})    
x=callR('runif',10)
```

===================== Windows-rcom.txt
% vi: ft=Matlab

% In R:
% 1) install rcom package (should automatically install rscproxy package as well).
% 2) load rcom: library('rcom')
% 3) install statconnDCOM: installstatconnDCOM()
% 4) run: comRegisterRegistry()

% Unzip MATLAB_RLINK.zip

% In Matlab:
addpath('C:\my\path\to\MATLAB_RLINK\folder');
Rdemo

openR; %Open connection to R server 
x=[1:50]; %create x values in Matlab 
putRdata('x',x); %put data into R workspace 
evalR('y<-sqrt(x)'); %evaluate in R 
evalR('plot(x,y)') %plot in R 
closeR; % close the connection to R, and the graphs opened from R.
===================== appel de Matlab depuis C-C++.txt
3 solutions pour intégrer MATLAB à du code C :
_ appel du matlab engine (engine.h et libengine.lib) --> appel de code matlab directement depuis le C ou de fonction matlab (.m présent dans le répertoire courant).
_ création d'une DLL. Chargement de la DLL et utilisation du MCR (Matlab C... Runtime). On peut aussi générer une librarie pour Java, .NET et une macro pour Excel, ou encore un objet COM.
_ génération de code C et C++: toutes les toolbox ne sont pas disponibles.
===================== cell array.txt
% vi: ft=matlab

% Getting a sub-array
a(1:3,2:4)

% Accessing a cell array content
a{i}	% for a 1D cell array
a{i,j}	% for a 2D cell array
a{i}{j}	% for a cell array of cell arrays
===================== comments.txt
% vi: ft=matlab

% Comments are written using % operator.

% Line continuation
% A line can be splitted using ... operator. Everything that is after the 3 points is considered comment.
A = B + ... blablabla
	C;
===================== debugging.txt
% vi: ft=matlab

% displaying the address of variable:
format debug
% then when a displaying a variable, it will also display it address.
% It outputs the following information (all variables are considered to be matrices in Matlab):
% Structure address = 6bc1ab0               ---> the adress in memory of the variable structure
% m = 3                                     ---> number of rows
% n = 3                                     ---> number of columns
% pr = d8dccf0                              ---> pointer to the real part of the data in memory
% pi = 0                                    ---> pointer to the imaginery part of the data in memory 
%      8     1     6                        |
%      3     5     7                        |--> data
%      4     9     2                        |
===================== error handling.txt
% vi: ft=matlab

% try catch
try
	% ...
catch my_exception
	% ...
end

% error: throws an exception
error('my message');

% assert
% throws an exception if condition evaluates to false
assert(condition, 'my message');
===================== help.txt
% vi: ft=matlab

% To get help about a function
help myfunc
% If multiple functions exist with the same name in the path, only the first one is returned.

% To get help about a function inside a precise path
help mypath/myfunc

% To get about all functions in a directory
help mydir

% To get help about a class
help classname

% To get help about a method of a class
help classname.methodname

% To get help about Matlab syntax
help('syntax')

% To open GUI help (help browser), and get more information (including graphics, ...)
doc myitem

% To get help about writing our own help descriptions
doc help

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WRITING HELP FOR A FUNCTION %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ret_val] = my_func(param1, param2)
% MY_FUNC Do something very interesting.
%
% ret_val = my_func(param1, param2) does ...
%
% param1    This is ...
% param2    This is ...
%
% See also ANOTHER_FUNC, YET_ANOTHER_FUNC.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WRITING HELP FOR A DIRECTORY %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create a file Contents.m inside the directory, and write inside a short describing text for each function.
%
% The file should look like the following file taken from /Applications/MATLAB_R2012a.app/toolbox/matlab/plottools/Contents.m:

% Graphical plot editing tools 
%
% Plottools functions.
%   figurepalette		- Show or hide the palette for a figure.
%   plotbrowser			- Show or hide the plot browser for a figure.
%   plottools			- Show or hide the plot-editing tools for a figure.
%   propertyeditor		- Show or hide the property editor for a figure.
   
% Utilities
%   adddatadlg			- Show a dialog that asks the user to add a data trace to an axes.
%   addsubplot 			- Creates a new subplot and adds it to the figure at the given location. 
%   enableplottoolbuttons	    - Check to see if all the plot tool components are either hidden or showing, and enable/disable the toolbar buttons accordingly.
%   getfigurefordesktopclient	- loop over all the figures and find out which one has this DTClientBase.
%   javaAddLsnrsToFigure	    - This is a utility function used by the plot tool.
%   javaGetHandles		 - Returns a cell array of Java handles to all the objects in the argument array.
%   makedisplaynames	 - Creates a cell array of display names that can be assigned to the DisplayName of the plot of a 2D matrix.
%   plottoolfunc		 - Support function for the plot tool.
%   setupplotbrowser	 - This is a utility function used by the plot browser.
%
% Helper functions.
% 
% Grandfathered functions.

% Obsolete functions.

%   Copyright 1984-2011 The MathWorks, Inc. 
%      

===================== loop.txt
% vi: ft=matlab

% FOR
for n = 3:32
	% ...
	if mycond
		continue
	end
	if another_cond
		break
	end
end

for e = myvector
	% ...
end

% WHILE
while i < N
	% ...
	if i == 5
		continue
	end
end
===================== memory.txt
% vi: ft=matlab

% Variables are stored as C/Fortran structure containing the following fields:
% m : number of rows
% n : number of columns
% pr: pointer to real part of data
% pi: pointer to imaginery part of data
% ==> So everything is considered to be a matrix in Matlab.
===================== mex.txt
To compile a C/C++ mex file, go into Matlab, and run:
mex <your_file.c>

If the mex command yields errors, like it can't find standard include files (math.h, stdlib.h, etc), then it may be that your mex configuration isn't set properly.

UNDER MAC-OSX
=============
First check that you have a mexpots.sh file inside ~/.matlab/R2012b/. (or the directory corresponding to your version of Matlab).
If not then copy it from the main application:
cp /Applications/MATLAB_R2012b.app/bin/mexopts.sh ~/.matlab/R2012b/.

Then edit it and check/change the following lines in the switch case 'maci64':

CC and CXX must point to the right compiler:
CC='gcc'
CXX='g++'
or
CC='llvm-gcc-4.2'
CXX='llvm-g++-4.2'

SDKROOT must point to the right MacOS-X SDK. For instance, for MacOS-X 10.7 and XCode 4.6:
SDKROOT='/Applications/Xcode.app/Contents//Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.7.sdk'

The right MacOS-X version must be specified:
MACOSX_DEPLOYMENT_TARGET='10.7'

===================== operators.txt
% vi: ft=matlab

% test equality
a == b
eq(a, b)

% test inequality
a ~= b

% Matrix testing
A == B
% or
eq(A, B)
% If one element is scalar, then a matrix is built from it.
% If size of A is different from size of B, then an error is issued.
% Otherwise returns a matrix of same size with ones where A and B are equal and zeros where they are different.

% Testing if a matrix/vector contains zeros
all(V)      % returns true if all elements of V are non-zeros.
all(M)      % returns a row vector containing 1/0 values indicating if each column of A contains only non-zero values or not.
all(all(M)) % returns true if all elements are non-zeros.
===================== random.txt
% vi: ft=matlab

v = rand(100,1);
===================== regexp.txt
% vi: ft=matlab

% regexp parses a string or a cell array of strings
% it returns a vector of indices where the string is matched
matches = regexp(str_vector, my_regexp);

% Case insensitive
regexpi

% get matched text
[m s e] = regexp(str, '\w*x\w*', 'match', 'start', 'end')

% get matched groups
[groups, match] = regexp(str, '(.*)ABC([A-E]*)', 'tokens', 'match')
groups{:}	% all groups
groups{1}	% first group		--> returns an element of type 'cell'
cast(groups{1}, 'char')	% to get a string

% split
regexp(s1, '\^', 'split');
===================== sum.txt
% vi: ft=matlab

% Sum a vector's elements
s = sum(V);

% Sum a matrix column by column
v = sum(M); % returns a row vector whose elements are the sum of the corresponding column.

% Sum all elements of a matrix
s = sum(sum(M));

% Choose along which dimension to sum
S = sum(M, 2);  % sum along the second dimension
===================== switch.txt
% vi: ft=matlab

switch i
	case 1
		% ...
	otherwise
		% ...
end
===================== types.txt
% vi: ft=matlab

% Get type of a variable
class(myvar)

% Convert to a string
cast(myvar, 'char')
===================== variables.txt
% vi: ft=matlab

% displaying the address of a variable: see debugging.txt

% how variables are stored inside Matlab: see memory/memory.txt
===================== vectors.txt
% vi: ft=Matlab

% Construct a row vector
V = [1,2,3];

% Construct a column vector
V = [1;2;3];

% Cross product
W = cross(U, V);

% Normalize a vector
normalized_V = V / norm(V);

% size of a vector
size(v)
size(v,1)
size(v,2)
