//You can include any C libraries that you normally use
#include "mex.h"   //--This one is required



void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    
    //DECLARATIONS
    //if using a C compiler, declaration must be in the very beginning
    //with C++, they can be anywher in the functions
    //Matlab uses C compiler for .c files, and C++ for .cpp files
    const mxArray *inMat;
    double *in, *out;
    double lambda,valt;
    int i, j, ind;
    int nrow, ncol;
    unsigned int memsize_in, memsize_out;
    
    if (nrhs!=2) {
        mexErrMsgTxt("The function needs exactly 2 inputs:\n- a matrix.\n- the threshold.\n");
        return;
    }
    
    
    //get threshold
    lambda = (double) *mxGetPr(prhs[1]);
    mexPrintf("Threshold value is: %f.\n",lambda);
    
    //Get matrix x
    inMat = prhs[0];
    in = (double*)mxGetPr(inMat);
    nrow = mxGetM(inMat);
    ncol = mxGetN(inMat);
    
    ///////////////////////////////////////////////////////////////////////
    //                          First output                             //
    ///////////////////////////////////////////////////////////////////////
    
    //create an output
    plhs[0] = mxCreateDoubleMatrix(nrow, ncol, mxREAL);
    out = (double*)mxGetPr(plhs[0]);
    
    //fill the output
    for(i=0;i<nrow;i++)//loop on rows
    {
        for(j=0;j<ncol;j++)//loop on columns
        {
            ind=i+j*nrow;
            valt=abs(in[ind])-lambda;
            //compute the soft-thresholded value
            //(here using ternary operators)
            out[ind]= (valt>0?valt:0) * (in[ind]>0?1:-1);
        }
    }
    
    ///////////////////////////////////////////////////////////////////////
    //                     Second (optional) output                      //
    ///////////////////////////////////////////////////////////////////////
    
    //to test the order in which the coefficients are set
    //fill the second output with 10*i+j
    if (nlhs>1) {
        //create an output
        plhs[1] = mxCreateDoubleMatrix(nrow, ncol, mxREAL);
        out = (double*)mxGetPr(plhs[1]);
        
        //fill the output
        for(i=0;i<nrow;i++) {
            for(j=0;j<ncol;j++) {
                ind=i+j*nrow;
                out[ind]=10*i+j;
            }
        }
    }
    

    
}


// //to get an integer:
// int Num;
//
// //Get the Integer
// Num = (int)(mxGetScalar(prhs[2]));
//
// //print it out on the screen
// mexPrintf("Your favorite integer is: %d",Num);

