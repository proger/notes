# TypeScript
<!-- vimvars: b:markdown_embedded_syntax={'ts':'typescript','sh':'','json':''} -->

 * [TypeScript official site](https://www.typescriptlang.org/).
 * [TypeScript Documentation](https://www.typescriptlang.org/docs/handbook/).
 * [The TypeScript Handbook](https://www.typescriptlang.org/docs/handbook/intro.html).

 * [Node.js with TypeScript](https://nodejs.dev/en/learn/nodejs-with-typescript/).

Superset of JavaScript with static typing.

## Install

```sh
npm install -g typescript # Node.js
apt install -g node-typescript # Debian
```

## Run

Compile a TypeScript file into JavaScript.
```sh
tsc myfile.ts
```
Outputs a file `myfile.js`.

By default `tsc` output a JavaScript file even if errors are met.
To disable this, run:
```sh
tsc --noEmitOnError ...
```

By default `tsc` rewrites code to target ES5.
Target a more recent ECMAScript:
```sh
tsc --target es2022 ...
```

Enable strict checking:
```sh
tsc --strict ...
```

## Configuration file

 * [What is a tsconfig.json](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html).

When running `tsc` with no input files, it searches for a `tsconfig.json` or
`jsconfig.json` in the current folder or a parent folder.
An arbitrary folder can be set with the `-p` option:
```sh
tsc -p my_project_folder
```

Example of configuration file:
```json
{
  "compilerOptions": {
    "strict": true,
    "module": "foo"
  },
  "files": [
    "file1.ts",
    "file2.ts",
    "file3.ts"
  ]
}
```

## Interface

Declaring an interface:
```ts
interface User {
  name: string;
  id: number;
}
```

Using an interface with an object literals:
```js
const user: User = {
  name: "Hayes",
  id: 0,
};
```

Using an interface with a class:
```ts
class UserAccount {
  name: string;
  id: number;
 
  constructor(name: string, id: number) {
    this.name = name;
    this.id = id;
  }
}
 
const user: User = new UserAccount("Murphy", 1);
```

Using an interface with a function:
```ts
function deleteUser(user: User) { }

function getAdminUser(): User { }
```

## Types

New types defined by Typescript:
 * `any`
 * `unknown`
 * `never`
 * `void`

## Structural Type System

TypeScript compares shapes of types to check if they are compatible.

```ts
interface Point {
  x: number;
  y: number;
}
 
function logPoint(p: Point) {
  console.log(`${p.x}, ${p.y}`);
}

const point = { x: 12, y: 26 };
logPoint(point); // OK

const point3 = { x: 12, y: 26, z: 89 };
logPoint(point3); // OK, z is not used
```

## Union

```ts
type WindowStates = "open" | "closed" | "minimized";
```

```ts
function getLength(obj: string | string[]) {
  return obj.length;
}
```

## Generics

```ts
type StringArray = Array<string>;
type ObjectWithNameArray = Array<{ name: string }>;
```

```ts
interface Backpack<Type> {
  add: (obj: Type) => void;
  get: () => Type;
}

declare const backpack: Backpack<string>;
```
