# Arch Linux

 * [VirtualBox/Install Arch Linux as a guest](https://wiki.archlinux.org/title/VirtualBox/Install_Arch_Linux_as_a_guest).
 * [Installation guide](https://wiki.archlinux.org/title/Installation_guide).

 * [Install Arch Linux in Virtualbox with UEFI Firmware](https://www.linuxbabe.com/virtualbox/install-arch-linux-uefi-hardware-virtualbox).
 * Installing on a [Mac](https://wiki.archlinux.org/index.php/Mac).

Encrypting the whole disk or a partition:
 * [Installing Arch Linux with Full Disk Encryption](https://dev.to/mjnaderi/installing-arch-linux-with-full-disk-encryption-16e9).
 * [Disk encryption](https://en.wikipedia.org/wiki/Disk_encryption#Full_disk_encryption).
 * [dm-crypt/Encrypting an entire system](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LUKS_on_a_partition).

In VirtualBox with EFI:
 * Enable EFI in VirtualBox VM: Settings -> System -> Enable EFI.
 * Boot on ArchLinux ISO CD install.

```bash
fdisk -l # Look for the device name of the harddrive (should be `/dev/sda`).
fdisk /dev/sda # Make 3 partitions:
  # Press `g` for creating a GPT partition
  # /dev/sda1: EFI System (type 1), 512MB
  # /dev/sda2: Linux filesystem (type 20)
  # /dev/sda3: Linux swap (type 19)
  # Press `w` to write partition table.
mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mkswap /dev/sda3
mount /dev/sda2 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
swapon /dev/sda3
vi /etc/pacman.d/mirrorlist
pacstrap /mnt base base-devel efibootmgr grub linux linux-firmare sudo vi vim bash-completion iw iwd dhcpcd openssh
genfstab -U -p /mnt >> /mnt/etc/fstab
arch-chroot /mnt
vi /etc/locale.gen
echo LANG=en_IE.UTF-8 > /etc/locale.conf
locale-gen
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc
echo "archlinux.host.local" > /etc/hostname
passwd # Define root password
systemctl enable dhcpcd
UUID=$(blkid /dev/sda2 | sed 's/^.*PARTUUID="\(.*\)"$/\1/') # Get Linux disk UUID

 # EFI boot, see https://wiki.archlinux.org/index.php/EFISTUB
echo "\\vmlinuz-linux root=PARTUUID=$UUID rw initrd=\\initramfs-linux.img" >/boot/archlinux.nsh # Write script for booting from EFI shell.
cp /boot/archlinux.nsh /boot/startup.nsh

 # For booting on LVM ? Is bootloader like GRUB necessary ?

exit
reboot
```

## Install ArchLinux with encrypted disk

 * [Arch install with full disk encryption](https://medium.com/hacker-toolbelt/arch-install-with-full-disk-encryption-6192e9635281).

Verify if boot mode is EFI:
```sh
ls /sys/firmware/efi/efivars
```
Otherwise use BIOS (see web page).

See normal instructions about network configuration.

Update system clock
```sh
timedatectl set-ntp true
```

Prepare for LUKS encryption:
```sh
modprobe dm-crypt
modprobe dm-mod
```

Choose disk:
```sh
lsblk
```

With fdisk create 3 partitions, /dev/sda1 EFI (256M, type EFI System), /dev/sda2 boot (512M) and /dev/sda3 root.
In fdisk, if asked, choose "gpt".
```sh
fdisk /dev/sda
```
Encrypt root partition:
```sh
cryptsetup luksFormat -v -s 512 -h sha512 /dev/sda3
cryptsetup open /dev/sda3 luks_root
```
Format all partitions:
```sh
mkfs.vfat -n “EFISYSTPART” /dev/sda1
mkfs.ext4 -L boot /dev/sda2
mkfs.ext4 -L root /dev/mapper/luks_root
```
Mount them:
```sh
mount /dev/mapper/luks_root /mnt
mkdir /mnt/boot
mount /dev/sda2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/sda1 /mnt/boot/efi
```
Create SWAP file:
```sh
cd /mnt
dd if=/dev/zero of=swap bs=1M count=1024
mkswap swap
swapon swap
chmod 0600 swap
```

Select mirrors:
```sh
vim /etc/pacman.d/mirrorlist
```

```sh
pacstrap /mnt base base-devel efibootmgr grub linux linux-firmare sudo vi vim bash-completion iw iwd dhcpcd openssh
```

Generate a fstab file:
```sh
genfstab -U /mnt >> /mnt/etc/fstab
```

Change root to new system:
```sh
arch-chroot /mnt
```

Time zone:
```sh
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc
```

Locale:
Edit /etc/locale.gen and select the desired locales.
```sh
locale-gen
echo ‘LANG=en_US.UTF-8’ > /etc/locale.conf
# echo ‘KEYMAP=pt-latin9’ > /etc/vconsole.conf
```

Network:
```sh
echo ‘your-host-name’ > /etc/hostname
```
Edit `/etc/hosts`:
```
127.0.0.1 localhost
::1 localhost
127.0.1.1 your-host-name.localdomain your-host-name
```

Edit GRUB config `/etc/default/grub`.
```
GRUB_CMDLINE_LINUX=”cryptdevice=/dev/sda3:luks_root”
```

Add `encrypt` to the `HOOKS` list (before `filesystems`) inside `/etc/mkinitcpio.conf` and run:
```sh
mkinitcpio -p linux
```

Set root password:
```sh
passwd
```

Install boot loader.
```sh
grub-install --boot-directory=/boot --efi-directory=/boot/efi /dev/sda2
grub-mkconfig -o /boot/grub/grub.cfg
grub-mkconfig -o /boot/efi/EFI/arch/grub.cfg
```

Exit and reboot:
```sh
exit
reboot
```

Create user and add to sudo list:
```sh
useradd -m username
visudo # Enable sudo for all members of wheel
usermod -a -G wheel username
passwd username
```
Log out root and login with the new user.

Enable and start dhcpcd and iwd:
```sh
sudo systemctl enable --now iwd
sudo systemctl enable --now dhcpcd
```
Run `iwctl` to setup the WiFi.

Check CPU type in `/proc/cpuinfo`, and install either AMD or Intel microcode.

Install AMD microcode:
```sh
sudo pacman -S amd-ucode
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

Install Intel microcode:
```sh
sudo pacman -S intel-ucode
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

Install TLP (battery management for laptop):
```sh
sudo pacman -S tlp
sudo systemctl enable --now tlp.service
```
Install the TLP Radio Device Wizard (**What is it?**):
```sh
sudo pacman -S tlp tlp-rdw
sudo systemctl mask systemd-rfkill.service
sudo systemctl mask systemd-rfkill.socket
```

**??? Useful ?**
Install apparmor:
```sh
sudo pacman -S apparmor
sudo systemctl enable --now apparmor.service
```
Add in `/etc/default/grub` parameters to enable AppArmor as default security model on every boot:
```
apparmor=1 lsm=lockdown,yama,apparmor
```
Regenerate grub config file:
```sh
sudo grub-mkconfig -o /boot/grub/grub.cfg
```
