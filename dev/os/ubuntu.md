# Ubuntu

Package manager: `apt`, `apt-get`.

Upgrade distribution:
```sh
sudo apt update && sudo apt upgrade
sudo reboot
sudo apt --purge autoremove
sudo apt install update-manager-core
sudo do-release-upgrade
sudo reboot
```

## Display manager

Getting current display manager:
```sh
systemctl status display-manager.service
```

Changing of display manager:
```sh
dpkg-reconfigure current_display_manager
```
`current_display_manager` being `gdm3`, `lightdm`, ...

## DisplayLink driver

Displays are not detected through USB-C docking station.
A special driver (DisplayLink) needs to be installed.

First install `evdi-dkms`:
```sh
apt install evdi-dkms
```
If the computer is equipped with Secure Boot, a password will have to be chosen
and input at reboot (enter key MOK) in order to install a key.

Then download the DisplayLink driver at
<https://www.synaptics.com/products/displaylink-graphics/downloads/ubuntu>.
Unzip and execute the `.run` as root, or configure the Synaptics APT Repository (explained on the download page):
```sh
wget https://www.synaptics.com/sites/default/files/Ubuntu/pool/stable/main/all/synaptics-repository-keyring.deb
sudo apt install synaptics-repository-keyring.deb
sudo apt update
sudo apt install displaylink-driver
```
