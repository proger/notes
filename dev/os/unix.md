# UNIX
<!-- vimvars: b:markdown_embedded_syntax={'sh':'bash','bash':'','r':'','hog':'','conf':'','systemd':''} -->

These notes refer to UNIX and Linux operating systems.

 * [LSB (Linux Standard Base)](https://en.wikipedia.org/wiki/Linux_Standard_Base).
 * [POSIX (Portable Operating System Interface)](https://en.wikipedia.org/wiki/POSIX).
 * [Debian](https://www.debian.org).
 * [CentOS](https://www.centos.org).
 * [Arch Linux](https://www.archlinux.org/).
 * [Gentoo](https://www.gentoo.org/).
   + [Handbook:Main Page](https://wiki.gentoo.org/wiki/Handbook:Main_Page).

## System

### Booting, starting and stoping

Display boot menu on macmini 2,1 : press `Alt` when starting computer.

### dmesg

Display kernel ring buffer.


### Machine name

Under macOS, to set the name of a machine, run:
```bash
sudo scutil --set HostName lucy
sudo scutil --set LocalHostName lucy
sudo scutil --set ComputerName lucy
```

### Run levels

 * [Run levels](http://www.tldp.org/LDP/sag/html/run-levels-intro.html).

To get current level:
```bash
runlevel
```

Change run level:
```bash
init 5
```

The booting run level is defined inside the file `/etc/inittab`.

Services starting scripts are located inside `/etc/rc.d/init.d`.
They are run according to the runlevel. A symbolic is created inside the relevant runlevel folder for a service that we want to stop or start.
The runlevel folder is of the form `/etc/rc.d/rc?.d`, where `?` is replaced the runlevel number.
The name of the symbolic link begin by `K` for stopping the service and `S` for starting it, and is followed by a number. The number allows to sort the services in the order we want them to stop or to start.

LOGIN

Remove login manager:
```sh
update-rc.d -f gdm remove
```

Restore login manager:
```sh
update-rc.d -f gdm defaults
```


### macos dseditgroup

Edit groups in the Directory Service:
```bash
dseditgroup -o edit -p -a <username> -t user <group_name> 
```
-u : specify admin-user login
-p : prompt for admin-user password

Create a group:
```bash
dseditgroup -p -o create Dev
```

Add a user to a group:
```bash
dseditgroup -p -o edit -a pierrick -t user Dev
```

### macos environment variables

`~/.MacOSX/environment.plist` file is for defining ENV VARS for the session.
A MacOS-X application won't see ENV VARS defined from the terminal if run from the Windows Manager.
See <Https://developer.apple.com/library/mac/#documentation/MacOSX/Conceptual/BPRuntimeConfig/Articles/EnvironmentVars.html>.

### Hardware

Get list of PCI devides in Debian:
```bash
lspci
```

Get list of USB devices in Debian:
```bash
lsusb
```


#### macos optical disk handling

To make a disk image from an optical disk:
	Open "Applications->Utilities->Disk Utility"
	Then select the disk and click on "New Image".
	Select "DVD/CD master" in order to create an ISO/CDR file, or "read-only" or "compressed" to create a dmg file.

To convert a dmg to an iso:
```bash
hdiutil convert /path/to/filename.dmg -format UDTO -o /path/to/savefile.iso
```

To convert a cdr (CD/DVD Master) to an iso:
```bash
hdiutil makehybrid -iso -joliet -o CLIMB_APP.iso CLIMB_APP.cdr
```

To unmount a disk:
```bash
diskutil umount <disk name>
```

To eject optical disk:
```bash
drutil eject
```

#### Display

 * Control the screen brightness on Linux: [Backlight](https://wiki.archlinux.org/index.php/backlight).

After install Debian on an iMac 27'', I had to turn off ACPI backlight in the kernel in order to get control of the screen brightness:
```
acpi_backlight=none
```
See [Kernel parameters](https://wiki.archlinux.org/index.php/Kernel_parameters) for setting kernel parameters at boot time.

#### iPad/iPhone

##### Charging

 * [iPad ‘Not Charging’ from USB on Ubuntu? Here’s How to Fix It](https://www.omgubuntu.co.uk/2014/04/ipad-iphone-not-charning-usb-on-ubuntu-fix)
 * [Charge iPad / iPhone 4s / iPod Touch in Ubuntu](http://ubuntuguide.net/charge-ipad-iphone-4s-ipod-touch-in-ubuntu).

For charging an iPad under Linux, see 
Under Ubuntu:
```bash
sudo apt-get install libusb-1.0-0 libusb-1.0-0-dev git git-core
git clone https://github.com/mkorenkov/ipad_charge.git
cd ipad_charge
make
sudo make install
```
On ArchLinux:
```sh
yay -S ipad_charge
```

Then to start charging connected devices:
```bash
ipad_charge
```

And to stop charging connected devices:
```bash
ipad_charge -0
```

#### Keyboard

 * [Configure Apple keyboard under Debian](https://wiki.debian.org/MacBook#Keyboard).
 * [Compose key](https://en.wikipedia.org/wiki/Compose_key).

The ~/.XCompose file list user configuration for key combination. See `man compose`.

To see current keyboard configuration under Ubuntu:
```bash
localectl status
```

To configure the keyboard for the session only, run:
```bash
sudo loadkeys us
```
or for French AZERTY keyboard:
```bash
sudo loadkeys fr
```

To configure keyboard permanently under CentOS:
```bash
sudo system-config-keyboard
```

To configure keyboard permanently under Ubuntu, it will open an X window:
```bash
sudo dpkg-reconfigure keyboard-configuration 
```
Under Ubuntu, to configure permanently from command line, and make it also available at boot time (i.e.: at login console), edit the file `/etc/rc.local` and put the line `loadkeys fr` just before the line `exit 0`. `fr` is for french keyboard layout, replace it with whatever keyboard layout you want.

Keyboard brightness: see `brightnessctl` or inside `/sys` (e.g.: `/sys/class/leds/dell\:\:kbd_backlight/brightness`).

### /sys

To list available LEDs on the computer:
```sh
ls /sys/class/leds
```

To get current keyboard backlight intensity:
```sh
cat /sys/class/leds/dell\:\:kbd_backlight/brightness
```

To get maximum keyboard backlight intensity:
```sh
cat /sys/class/leds/dell\:\:kbd_backlight/max_brightness
```

To set current keyboard backlight intensity (as root):
```sh
echo 1 | /sys/class/leds/dell\:\:kbd_backlight/brightness
```

### brightnessctl

List devices:
```sh
brightnessctl --list
```

Get info on one device:
```sh
brightnessctl --device='dell::kbd_backlight' info
```

Set brightness:
```sh
brightnessctl --device='dell::kbd_backlight' set 1
```

### macos Finder

Show all files and directories:
```bash
Defaults write com.apple.Finder AppleShowAllFiles YES
```

To change a folder icon:
Edit an icon with gimp, making its background transparent.
Maximum size must be 255x255 or the icon will not be clickable (only the folder title will be clickable).
Copy the image from gimp.
Open information panel of the folder (cmd-i), click once on the icon in top/left corner of the information window. Paste (cmd-v) the image.

Set hidden attribute:
```bash
sudo SetFile -a "v" /private # Show '/private' in the Finder.app
sudo SetFile -a "V" /private # Hide '/private' from the Finder.app
```

### locale

Display current locale:
```bash
locale
```

Display available locales:
```bash
locale -a
```

Add a new locale:
```bash
locale-gen fr_FR.UTF-8
```

To force to English:
```sh
export LC_ALL=C
```

### Virtual consoles

It is possible to open additional consoles with Ctrl+Alt+F?. On archlinux, F1 to F6 are available.
Still under Archlinux, it seems also to work when X is started on one of the consoles.

### /proc

Get wifi link quality:
```sh
cat /proc/net/wireless
```

### lspci

List PCI devices.

Get wifi cards:
```sh
lspci | egrep -i 'wifi|broadcon|wlan|wireless'
```

Get card information:
```sh
lspci -vv -s 02:00.0 # card index 02:00.0
```

## Network & web

 * ArchLinux [Network configuration](https://wiki.archlinux.org/index.php/Network_configuration).
 * ArchLinux network: [systemd-networkd](https://wiki.archlinux.org/index.php/Systemd-networkd).
 * ArchLinux [iPhone tethering](https://wiki.archlinux.org/index.php/IPhone_tethering).

 * [Build your own DNS server on Linux](https://opensource.com/article/17/4/build-your-own-name-server).
The DNS address is stored inside `/etc/resolv.conf`.

### Firewall & ports

Get port rules:
```bash
sudo iptables -L
```

If `ufw` (firewall) is running:
```bash
sudo ufw status
```

Under macos 10, for listing the rules:
```bash
sudo pfctl -s rules
```

### USB tethering & bluetooth (iPhone Hotspot)

 * [iPhone tethering](https://wiki.archlinux.org/index.php/IPhone_tethering).

List network devices:
```sh
networkctl list
```

Get the link name of the iPhone, and then create the `.network` file `/etc/systemd/network/30-tethering.network`:
```conf
[Match]
Name=enp0s26u1u2c4i2

[Network]
DHCP=yes
```

### Wifi

 * ArchLinux wifi: [WPA supplicant](https://wiki.archlinux.org/index.php/WPA_supplicant).

Monitor wifi signal quality:
```sh
wavemon
```

Create configuration file `/etc/wpa_supplicant/wpa_supplicant.conf` with:
```
ctrl_interface=/run/wpa_supplicant
update_config=1
```

See interface name with:
```sh
ip addr
```

Start daemon:
```sh
sudo wpa_supplicant -B -i wlp2s0 -c /etc/wpa_supplicant/wpa_supplicant.conf
```

On ArchLinux with WPA supplicant:
```sh
sudo wpa_cli -i wlp2s0
```
Then:
```
scan
scan_results
list_networks # List networks already defined
add_network # Add a new network
set_network 1 ssid "mynetwork"
set_network 1 psk "mypassword"
list_networks
enable_network 1
save_config
```

Stop daemon:
```bash
sudo wpa_cli -i wlp2s0
```
Then:
```
terminate
```

Get wifi status on Debian:
```bash
nmcli radio wifi
```

Turn off wifi on Debian:
```bash
nmcli radio wifi off
```

Get help on Debian:
```bash
nmcli radio wifi help
```

## Password managers

 * [pwsafe](https://github.com/nsd20463/pwsafe). Works in command line.

 * `keepass` password manager. Has an interactive command line interface: `kpcli`.
 * [KeePassC](http://raymontag.github.io/keepassc/). A command line interface to keepass.

`pass` password manager (too much complex, needs GPG setup):
 * [Pass](https://www.passwordstore.org/).
 * [Pass tutorial](http://www.tricksofthetrades.net/2015/07/04/notes-pass-unix-password-manager/).

Settitg a password inside the orkeychain in macOS:
```bash
sudo /usr/bin/security -v add-internet-password -a pierrick.rogermele@icloud.com -s mail.icloud.com -w 'mypassword' 
```

Getting a password stored in the keychain on macOS:
```bash
security find-internet-password -w -a pierrick.rogermele@icloud.com
```
## Terminals and consoles

### Enabling 256 colors in Linux kernel console

Install `kmscon`.
On ArchLinux:
```sh
sudo pacman -S kmscon
```

The in console:
```sh
sudo kmscon
```
Which will start `login` process and once logged in, `TERM` will be defined as `xterm-256colors` even if not in X.

Starting X from kmscon:
	`startx` won't work.
	See <https://github.com/dvdhrm/kmscon/issues/103>. Maybe something like `startx -- vt8`.

## Terminal common key shortcuts

	^ = CTRL
	
	^C      Terminate process
	^A      Go to beginning of line
	^E      Go to end of line
	^K      Cut from current cursor position to end of line
	^U      Cut from current cursor position to beginning of line
	^W      Same as ^U ?



## Installing another UNIX like system on a Mac

 * [The rEFInd Boot Manager](http://www.rodsbooks.com/refind/).
 * [Single boot Linux on an Intel Mac Mini](https://major.io/2011/01/26/single-boot-linux-on-an-intel-mac-mini/).
 * [MacMiniIntel, Installing Debian on a Mac Mini](https://wiki.debian.org/MacMiniIntel)
 * [Single boot Gentoo mac mini](https://forums.gentoo.org/viewtopic-p-7128354.html).

Use CD install with non-free firmware included: <https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/amd64/iso-cd/>.

Bootloader:
 * Macs use EFI bootloader.
 * Standard Grub and LILO don't do EFI.
 * Grub2 may do EFI -> to check.
 * See [rEFIt](http://refit.sourceforge.net/).
 * See also [rEFInd](http://www.rodsbooks.com/refind/) that is based on rEFIt with Macs consideration.
 * More precisely the MacMini 3,1 boots into EFI mode when booting from USB device, and into BIOS Compatibility mode when booting from CD/DVD.

Gentoo:
 * [Gentoo Linux in Mac Mini 2009, soundcard issue](https://forums.gentoo.org/viewtopic-t-749525-start-0.html).



## Books & PDFs

 * [Where are Amazon Kindle ebooks on my Linux PC after I download them for offline reading w/“Kindle Cloud Reader” Chrome app, & how to convert to PDF?](https://askubuntu.com/questions/1011989/where-are-amazon-kindle-ebooks-on-my-linux-pc-after-i-download-them-for-offline).

 * [DeDRM_tools](https://github.com/apprenticeharper/DeDRM_tools/blob/master/FAQs.md).

### bibutils

A package for converting between bibliographic citation formats.

Convert from `.ris` to `.bib`:
```sh
ris2xml mycitation.ris | xml2bib >mycitation.bib
```

