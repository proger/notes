# AMSDOS

 * [Amstrad CPC user manuals](http://www.cpcwiki.eu/index.php/User_Manual).

Change to disk drive A:
```
|A
```
To drive B:
```
|B
```

List files:
```
|DIR
```

List BASIC files:
```
|DIR,"*.BAS"
```

Run file "DISC":
```
RUN "DISC"
```

If it doesn't work, first list files:
```basic
cat
```
Then call the right file:
```basic
run "myfile"
```
