# iOS

 * [iOS version by device](https://iosref.com/ios).

## Erasing a device

 * [How to erase your iPhone, iPad, or iPod touch](https://support.apple.com/en-us/HT201274).

Factory reset your iPhone without the passcode
 1. Turn your phone off and plug it into a computer.
 2. Press and release Volume Up, press and release Volume Down, then press and hold the Side button.
 3. Keep holding down the button until you see the Recovery Mode screen.
 4. Depending on your computer, open either Finder® or iTunes®.

Remarque : Comme toujours, si vous envisagez de donner votre appareil à
quelqu’un d’autre, assurez-vous d’effacer tout le contenu et les réglages avant
de le céder au nouveau propriétaire. Ceci supprimera l’appareil de votre compte
pour permettre au nouveau propriétaire de l’activer. Pour plus d’informations,
veuillez consulter l’article Procédure à suivre avant de vendre ou de céder
votre iPhone, iPad ou iPod touch :

<http://support.apple.com/kb/HT5661?viewlocale=fr_FR>.

## Find my iphone

<http://support.apple.com/kb/HT5818?viewlocale=fr_FR>.

Vous n’avez rien à faire, à part maintenir Localiser mon iPhone activé et vous
souvenir de votre identifiant Apple et de votre mot de passe. Pour plus
d’informations, veuillez consulter la Foire aux questions :

## To "force quit" an app

in OS 1 & 2: hold down home button durring 6 seconds
is OS 3 & 4: hold down sleep/wake button until display of "Power off" screen, then hold down home button durring 6 seconds.

## Jailbreaking

<http://helpmejailbreak.com/>.

## Shark.txt

Shark is a tool used for profiling

Using Shark with an iPhone:
 1. compile iPhone app using -pg option: look for "Generate Profiling Code" option in the Target Info window under Xcode.
 2. run the program in the iphone
 3. start Shark
 4. enable Sampling->Network/iPhone profiling
 5. start profiling
 6. do whatever you want with the app
 7. stop profiling and wait for end of analysis
 8. Add symbole information: go to File->Symbolicate and select the debug version of your application (normally under <myapp>/build/Debug-iphoneos
 9. explore the functions in the table, your application name should be shown in the Library column for all your application functions.
