# macos

 * [How to Install macOS in VirtualBox](https://www.maketecheasier.com/install-macos-virtualbox/).
 * [How To Fix No Disk To Select Issue When Install Mac OS On MacBook Or Virtual Machine](https://www.dev2qa.com/how-to-fix-no-disk-to-select-issue-when-install-mac-os-on-virtual-machine/).

 * [How to reinstall macOS](https://support.apple.com/en-us/HT204904).
 * [Create a bootable USB stick on macOS](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-macos?_ga=2.233519964.315784058.1525944550-1342623189.1521968110#0).
 * [Radeon kernel modesetting for r600 or later requires firmware-amd-graphics](https://joshtronic.com/2017/11/06/fixed-radeon-kernel-modesetting-for-r600-or-later-requires-firmware-amd-graphics/).

To upgrade to macOS High Sierra on a computer running with mac OS X Lion, one must first upgrade to OS X El Capitan. Use this [link](https://support.apple.com/en-us/HT206886) to download OS X El Capitan.

 * [Mac startup key combinations](https://support.apple.com/en-us/HT201255).

macOS boot key combinations:

| Command          | Description                                                                            |
| ---------------- | -------------------------------------------------------------------------------------- |
| Cmd-R            | Recovery mode. Reinstall your current version of macOS.                                |
| Alt-Cmd-R        | Install the latest version of macOS compatible with your computer.                     |
| Shift-Alt-Cmd-R  | Reinstall your computer's original version of macOS (including available updates).     |
| C                | Boot on DVD or USB device.                                                             |
| D or Alt-D       | Run hardware tests.                                                                    |
| Alt              | Display possible boot devices.                                                         |
| Eject key or F12 | Eject optical media.                                                                   |
| Cmd+V            | Boot in verbose mode.                                                                  |
 
To always boot in verbose mode on macos:
```bash
sudo /usr/sbin/nvram boot-args="-v"
```

To boot in console mode on macos, edit file /etc/ttys and find the lines:
```
 #console        "/usr/libexec/getty std.9600"   vt100   on secure
Console "/System/Library/CoreServices/loginwindow.app/Contents/MacOS/loginwindow"
```

Preventing system sleep on macOS:
```bash
caffeinate -i myprog
```
Or by using PID:
```bash
caffeinate -i -w 4521
```

Shutdown a macOS machine:
```bash
shutdown -h now
```

To boot in 32 bit mode:
```bash
sudo systemsetup -setkernelbootarchitecture i386
```
To get back to 64 bits, replace `i386` by `x86_64`.
Or press keys 3 and 2 during startup for 32-bit or keys 6 and 4 for 64-bit.

To get boot architecture setting:
```bash
sudo systemsetup -getkernelbootarchitecturesetting
```

Disable App signature check:
```sh
sudo spctl --master-disable
```

Mount an ISO file:
```sh
hdiutil attach myfile.iso -mountpoint my/folder
```
Unmount:
```sh
hdiutil detach my/folder
```

## open

Launch application:
```sh
open -a "My Application"
```

LSOpenURLsWithRole() error -10810:
Not executable. Look into application folder in Contents/MacOS for the executable file.

## login window

Set login window message:
```bash
sudo defaults write /Library/Preferences/com.apple.loginwindow LoginwindowText "Hello There"
```

Hide accounts:
```bash
sudo defaults write /Library/Preferences/com.apple.loginwindow HiddenUsersList -array-add shortname1 shortname2 shortname3
```

Show all accounts:
```bash
sudo defaults write /Library/Preferences/com.apple.loginwindow HiddenUsersList -array-add
```

Changing Apple logo. Be careful to back up the original file before. The tiff image must be 90x90:
```
/System/Library/CoreServices/SecurityAgent.app/Contents/Resources/applelogo.tif
```

## Screen saver, locking and energy saving

 * [Session lock](https://wiki.archlinux.org/index.php/Session_lock).

```bash
/System/Library/CoreServices/"Menu Extras"/User.menu/Contents/Resources/CGSession -suspend
```
## Take screenshots

 * [Take pictures of the screen on your Mac](https://support.apple.com/en-sa/KM204852?cid=acs::applesearch).

macOS Command     | Description
----------------- | -----------------
Shift-Cmd-3       | Whole screen.
Shift-Cmd-4       | Zone.
Shift-Cmd-4-Space | Window.

The PNG picture is written on the desktop.
