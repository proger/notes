# Profiling code

## Python

 * [The Python Profilers](https://docs.python.org/3/library/profile.html).
 * [How can you profile a Python script?](https://stackoverflow.com/questions/582336/how-can-you-profile-a-python-script).
 * [RunSnakeRun](http://www.vrplumber.com/programming/runsnakerun/), a cProfile
   dump file visualizer. Not working neither in Python2, nor in Python3.
 * [How to visualize Python profile data with SnakeViz](https://codeyarns.com/2015/02/23/how-to-visualize-python-profile-data-with-snakeviz/).
 * [yappi](https://github.com/sumerc/yappi) for multi-threaded environment.
 * [line_profiler](https://github.com/rkern/line_profiler).

 * [The Python Profilers](https://docs.python.org/3/library/profile.html).

See <https://github.com/MordicusEtCubitus/CoursPython/blob/master/profiling/python_profiling.ipynb> about KCachegrind et pyprof2calltree.
See also [Scalene](https://github.com/plasma-umass/scalene).

### Measuring memory usage

```python
import os, psutil
process = psutil.Process(os.getpid())
print(process.memory_info().rss) # in Bytes 
```

### cProfile

Profiling inside code:
```python
import cProfile
cProfile.run('foo()')
```

Profiling a script from command line:
```python
python -m cProfile -o myapp.pstat myscript.py
```

Running snakeviz to analyse a .pstat file:
```sh
snakeviz myapp.pstat
```

Command line:
```sh
python -m cProfile -o mystats.prof my_prog -a -b -c ...
```

Visualize with `snakeviz`:
```sh
snakeviz mystats.prof
```

cProfile cannot profile parallel execution. You need to run a `cProfile.run()`
inside each thread/process and write the output inside a different file:
```python
cProfile.run('p.start()', 'prof%d.prof' %i)
```

### yappi

yappi, on the other hand, works with multi-threading (but does not seem to work
with multi-processing).

Installing yappi:
```sh
pip install yappi
```

Profiling with yappi:
```sh
yappi -o myapp.pstat myscript.py
```

### guppy

Prints heap content.

```python
import guppy
h = guppy.hpy()
print(h.heap())
```

### Memory profiler

Prints total memory usage of a function according to time.

```python
import memory_profiler
memory_profiler.memory_usage(my_fct, (value1, value2), {'param3': value3})
```

```sh
python -m mprof run myprog
python -m mprof plot
```
## gprof
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

`gprof` doesn't work on Mac computers running an Intel processor.

### Flat profile

Use the `-pg` flag when calling the linker to get a flat profile:
```sh
gcc -c -o foo.o foo.c
gcc -pg -o foo foo.o
./foo
gprof foo gmon.out -p
```

### Call graph

Use the `-pg` flag when calling the compiler **and** the linker to get a
call-graph profile:
```sh
gcc -pg -c -o foo.o foo.c
gcc -pg -o foo foo.o
./foo
gprof foo gmon.out -q
```

### Annoted source

Get "annoted source":
```sh
gcc -g -pg -c -o foo.o foo.c
gcc -pg -o foo foo.o
./foo
gprof foo gmon.out -A
```

### Line-by-line profiling

### Convert into GraphViz

Convert profiling data into GraphViz graph:
```sh
gprof2dot.py 
```
<!-- TODO Where to find this program? -->
## Intel Inspector

 * [Intel Inspector](https://www.intel.com/content/www/us/en/developer/tools/oneapi/inspector.html).
OpenMP debugger.
Formerly known as Intel thread checker.

## Intel® VTune™ Profiler

 * [Intel® VTune™ Profiler](https://www.intel.com/content/www/us/en/developer/tools/oneapi/vtune-profiler.html).
## MARMOT

 * [MARMOT: An MPI analysis and checking tool](https://www.sciencedirect.com/science/article/abs/pii/S0927545204800637).

MPI Debugger.
## papi
<!-- vimvars: b:markdown_embedded_syntax={'c':''} -->

Under Linux, there exists a library called PAPI which allows accessing counters of logical units:
 * floating operations on x87     `PAPI_FP_OPS`
 * vectorial operations (SIMDSP)  `PAPI_VEC_SP` --> simple precision
 * vectorial operations (SIMDDP)  `PAPI_VEC_DP` --> double precision

```c
##include <papi.h>
PAPI_library_init...
/* ... */
```
## pprof (gperftools)
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

 * [gperftools](https://github.com/gperftools/gperftools).

Originally Google Perl Tools.

On MacOS-X:
```sh
brew install google-perftools
```
Documentation: `file:///usr/local/Cellar/google-perftools/2.0/share/doc/gperftools-2.0/cpuprofile.html`.

Usage:
```sh
cc ... -lprofiler
CPUPROFILE=myprog.prof ./myprog
pprof --text ./myprog myprog.prof
```
## Valgrind
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','c':''} -->

`valgrind` is an emulator. So it doesn't need to instrument code, but is very very slow.
`helgrind` is used for detection of synchronization errors between threads in the POSIX pthreads.

### Memory checking

```sh
valgrind --tool=memcheck <program>
valgrind <program>  # memcheck is the default tool
valgrind  --leak-check=full <program> # to get a detailed list of leak errors
```

Best options:
```sh
valgrind --tool=memcheck --leak-check=full --track-origins=yes --error-exitcode=200 ./myprog
```
Compile with `-g -Og` to get line numbers.

### Callgrind

Profiling:
```sh
valgrind --tool=callgrind <program>
valgrind --tool=callgrind --cache-sim=yes <myprog> # Simulate cache
```

For visualizing an output file from Valgrind, use `kcachegrind` or
`qcachegrind`:
```sh
kcachegrind callgrind.out.<pid>
qcachegrind callgrind.out.<pid>
```
If you want a text output, use `callgrind_annotate`:
```sh
callgrind_annotate  # Text output
```

For profiling the cache:
```sh
valgrind --tool=cachegrind <myprog>
cg-annotate cachegrind.out.<PID>
```

Instrumentation of only a part of the code:
```c
##include <callgrind.h> /* normally inside /usr/include/valgrind */
/* ... */
	CALLGRIND_START_INSTRUMENTATION
	/* ... */
	CALLGRIND_END_INSTRUMENTATION
/* ... */
```

And then use :
```sh
valgrind --tool=callgrind --instr-atstart=no <myprog>
```

Compile with debug information to get line granularity.
