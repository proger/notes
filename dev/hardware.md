# Hardware

## Processors

 * [RENESAS M16C/60, M16C/20, M16C/Tiny Series](https://www.renesas.com/us/en/document/mah/m16c60-m16c20-m16ctiny-series-software-manual).

### Intel

 * [Intel® Intrinsics Guide ](https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html#).

 * `__m256i _mm256_cmpeq_epi8(__m256i a, __m256i b)`: Compare each byte of a with each byte of b (same position) and set 0xFF for this byte on the output or 0.
 * `int  _mm256_movemask_epi8(__m256i a)`: Create a mask from a vector. For byte i of a, if its most significant bit is 1 set the ith bit of output to 1, otherwise set it to 0. Output is a 32 bits integer.
 * `__mmask32 _mm256_cmpeq_epi8_mask(__m256i a, __m256i b)`: equivalent to `_mm256_movemask_epi8(_mm256_cmpeq_epi8(a, b))`.
 * `_mm256_castsi128_si256(a)`: cast 128 register into 256. Upper 128 bits are undefined. Does not generate any instruction. Only used for compilation.
 * `_mm256_castsi256_si128(a)`: cast 256 register into 128. Does not generate any instruction. Only used for compilation.
 * `_mm256_inserti128_si256(a, b, imm8)`: Copy a (256 bits) to dst. Copy b (128 bits) into dst in lower part (imm8 == 0) or upper part (imm8 == 1).
 * `_mm256_extracti128_si256(a, imm8)`: Copy lower part (imm8 == 0) or upper part (imm8 == 0) of a into 128bits reg dst. 
 * `_mm_cmpestrm(a, la, b, lb, imm8)`: a, b and dst are 128 bits registers. Compare packed strings in a and b with lengths la and lb. Store the generated mask in dst. imm8 contains bit option flags:
   + By default: 8 bits characters.
   + `_SIDD_SBYTE_OPS`: signed 8-bit characters
   + `_SIDD_CMP_EQUAL_ANY`: compare equal any   (???)
   + `_SIDD_MASKED_NEGATIVE_POLARITY`: negate results only before end of string (???)
   + `_SIDD_UNIT_MASK`:  mask only: return byte/word mask
   + ...

## DELL

 * [Motherboard Flash Boot CD from Linux Mini HOWTO](http://www.nenie.org/misc/flashbootcd.html).

 * F2 for entering setup.
 * F12 for entering boot menu.
Press key when DELL logo appears.
## lshw

Get hardware information:
```sh
sudo lshw
```

List network devices:
```sh
sudo lshw -C network
```

List GPU info:
```sh
sudo lshw -C display
```
## lsusb

Install:
```sh
pacman -S usbutils # Archlinux
```
## Mac

 * [Startup key combinations for Mac](https://support.apple.com/en-us/HT201255).
 * [Macbook air: Debian HOWTO](http://www.math.tamu.edu/~comech/tools/macbook-air-debian-howto/). Some tips about the non-free WiFi firmware files b43/ucode11.fw and b43-open/ucode11.fw.
 * [Programme de réparation du clavier pour MacBook, MacBook Air et MacBook Pro](https://support.apple.com/fr-fr/keyboard-service-program-for-mac-notebooks).

For choosing boot media on startup, press option key (alt key).

To get back a forgotten WiFi password from an AirPort or Time Capsule: go to
KeyChain and look for network name, then open and click on "Show Password".
## Apple USB SuperDrive (DVD player)

 * [Use Apple’s USB SuperDrive with Linux](https://cmos.blog/use-apples-usb-superdrive-with-linux/).
 * [Use Apple’s USB SuperDrive with Linux](https://christianmoser.me/use-apples-usb-superdrive-with-linux/).
 * [Using Apple’s SuperDrive on Linux](https://kuziel.nz/notes/2018/02/apple-superdrive-linux.html)

To enable (wake up) Apple Super Drive, install sg3-utils:
```sh
pacman -S sg3_utils # ArchLinux
apt install sg3-utils # Ubuntu
```
package and check which device is your drive (`/dev/sr0` or
`/dev/sr1` usually). Then run:
```sh
sg_raw /dev/sr0 EA 00 00 00 00 00 01
```

To make it automatic, create the file
`/etc/udev/rules.d/60-apple-superdrive.rules` as root with the following
content:
```hog
## Apple's USB SuperDrive
ACTION=="add", ATTRS{idProduct}=="1500", ATTRS{idVendor}=="05ac", DRIVERS=="usb", RUN+="/usr/bin/sg_raw /dev/$kernel EA 00 00 00 00 00 01"
```
## sysctl

Get CPU exact description on macos:
```bash
sysctl -n machdep.cpu.brand_string
```
