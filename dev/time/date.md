# date
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Print current date/time:
```sh
date
```

Print current Epoch value:
```sh
date +%s
```

Print formatted date (%Y-%m-%d):
```sh
date +%F
```

Print formatted date + time:
```sh
date +%F-%H%M%S
```

Convert a date to Epoch time:
```sh
date -j '01171200' +%s # BSD
```

Get date of yesterday formatted:
```sh
date -d yesterday +%Y-%m-%d
```

Print UTC date/time:
```sh
date -u
```

Print date/time for a different zone:
```sh
TZ=America/New_York date
TZ=Asia/Tokyo date
```

Flag        | Description
----------- | -----------------------
`-j`        | Do not try to set date.
`-f '...'`  | set another format for input date. Default is [[[mm]dd]HH]MM[[cc]yy][.ss] (mm=month, cc=century).
