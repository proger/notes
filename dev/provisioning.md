# Provisioning
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','yaml':''} -->

## ansible inventory

 * [How to build your inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html).

Check an inventory file and output it in JSON:
```sh
ansible-inventory -i inventory --list
```

Check an inventory file and output it in YAML:
```sh
ansible-inventory -i inventory -y --list
```

### Inventory simple format

For a single machine:
```
default ansible_ssh_host=127.0.0.1
```

### Inventory YAML format

For a single machine:
```yaml
---
all:
  children:
    ungrouped:
      hosts:
        default:
          ansible_ssh_host: 127.0.0.1
```

Use inventory file path:
```yaml
---
all:
  children:
    dbservers:
      hosts:
        my.host.fr:
          my.var: "{{ inventory_dir }}/my.file.json"
```
## ansible

 * [Ansible Documentation](https://docs.ansible.com/ansible/latest/index.html).
 * [Module Index](https://docs.ansible.com/ansible/latest/modules/modules_by_category.html).
  + [Ansible.Builtin](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html).
 * [Ansible](http://www.ansible.com/).
 * [Ansible as a provisioner for Vagrant](https://docs.vagrantup.com/v2/provisioning/ansible.html).
 * [Multi-Machine Vagrant Ansible Gotcha](http://blog.wjlr.org.uk/2014/12/30/multi-machine-vagrant-ansible-gotcha.html).
 * [How to unify package installation tasks in ansible?](https://serverfault.com/questions/587727/how-to-unify-package-installation-tasks-in-ansible).
 * [Directory Layout](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#directory-layout).
 * [Jinja](https://jinja.palletsprojects.com/).

 * [Provisioning Windows Machines via Ansible](https://brycematheson.io/controlling-windows-machines-via-ansible/).
 * [Windows Remote Management](https://docs.ansible.com/ansible/latest/user_guide/windows_winrm.html).
 * [Ansible.Windows](https://docs.ansible.com/ansible/latest/collections/ansible/windows/index.html).

 * [How to Install and Configure latest version of Ansible on Ubuntu Linux](https://www.cyberciti.biz/faq/how-to-install-and-configure-latest-version-of-ansible-on-ubuntu-linux/).

 * [Installer ansible (python3) sur windows avec CygWin](https://blog.stephane-robert.info/post/installer-ansible-cygwin/).

 * [Using Ansible collections](https://docs.ansible.com/ansible/latest/collections_guide/index.html).
 * [Getting Started With Molecule](https://ansible.readthedocs.io/projects/molecule/getting-started/). `molecule` helps testing ansible roles/collections.

To install:
```bash
pip install ansible
```

`ansible` runs a single task on selected hosts.
Example:
```sh
ansible -m ping -i inventory all
```

Ansible needs an inventory file which lists all remote hosts:
```bash
cat >myinventory.txt <<EOF
my.remote.host.fr
EOF
ansible -i myinventory.txt all -m ping # Will ping all hosts listed inside the inventory file.
```

Choose user for connection:
```bash
ansible all -u myuser ...
ansible all -u myuser -b ... # For running as super user (-b => sudo)
```

To run a command:
```bash
ansible all -a "/my/command/to/run"
```

To get a list of system variables:
```yaml
- hosts: all
  become: yes
  tasks:
      - debug: var=ansible_facts
```

### Configuration

 * [Configuring Ansible](https://www.redhat.com/sysadmin/configuring-ansible).

Order followed by Ansible to look for a configuration file:
 1. `$ANSIBLE_CONFIG` if the environment variable is set.
 2. `ansible.cfg` if it’s in the current directory.
 3. `~/.ansible.cfg` if it’s in the user’s home directory.
 4. `/etc/ansible/ansible.cfg`, the default config file.

### Inventory file

See [ansible-inventory](./ansible-inventory.md) notes.

### Playbook

See [ansible-playbook](./ansible-playbook.md) notes.

A playbook lists the affected hosts and the associated directives.
Example:
```yaml
- hosts: all
  become: yes
  tasks:
    - name: Task 1
      ...
    - name: Task 2
      ...
```

### block

The `block` statement is used to group tasks together and handled error
handling.

```yaml
- name: My block
  block:
    - name: Task 1
      ...

    - name: Task 2
      ...

  rescue:
    - name: Task executed only if one the block's tasks fails
      ...

  always:
    - name: Task always executed after failure or not
      ...
```

### Tasks

Tasks may have a `name` field that will be printed during execution.

```yml
- name: The task name/title
  my.task:
    arg1: ...
    arg2: ...
  become: true                     # Run as super-user.
  when: my.flag and my.other.flag  # Run task only when this clause is true
  delegate_to: localhost           # To run the task on host instead of inside the targeted machine
  register: myvar                  # Register result and command output into variable
  ignore_errors: true
  changed_when: false              # false => OK message in green. true => Changed message in yellow.
```

#### apt

Run the Debian apt command.

Update the package repository:
```yaml
- name: Update pkg repos
  ansible.builtin.apt:
    update_cache: yes
  become: yes
```

#### command

 * [ansible.builtin.command – Execute commands on targets](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html).

Run a command directly (not inside a shell).

#### debug

Print the content of a variable:
```yaml
- name: Print myvar
  ansible.builtin.debug:
    var: myvar
```

#### import\_tasks

 * [import\_tasks](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/import_tasks_module.html).

#### include

**DEPRECATED**.

#### include\_tasks

 * [include\_tasks](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/include_tasks_module.html).

#### lineinfile

 * [ansible.builtin.lineinfile module – Manage lines in text files](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html).

#### package

A module for installing packages in a generic manner.

Issue on CentOS/7: the module requires Python2 and does not work with Python3.
See [CentOS 7, Ansible and the end of Python 2](https://britishgeologicalsurvey.github.io/devops/centos7-python2-end-of-life/).

```yaml
- name: Install Apache Tomcat host-manager and manager web applications
  package:
    name: tomcat-admin-webapps
    state: present
  become: yes
```

#### replace

 * [replace – Replace all instances of a particular string in a file using a back-referenced regular expression](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/replace_module.html#ansible-collections-ansible-builtin-replace-module).

#### shell

 * [ansible.builtin.shell – Execute shell commands on targets](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html).

Run a command through a shell:
```yaml
- name: Run my cmd
  ansible.builtin.shell: mycmd
```

Register output of a command and use it inside a conditional:
```yaml
- name: Check if systemd is enabled
  ansible.builtin.shell: "systemctl >/dev/null"
  register: systemctl_output
  ignore_errors: True

- name: Start MongoDB daemon (no systemd) # No systemd inside a Docker image
  ansible.builtin.shell: "mongod --fork --syslog"
  when: systemctl_output.rc != 0
```
The `.rc` suffix gives the returned code. See [Return Values](https://docs.ansible.com/ansible/latest/reference_appendices/common_return_values.html) for a list of returned values.

## ansible-playbook

 * [Best practices](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#best-practices). Includes directory layout.

`ansible-playbook` runs a playbook on selected hosts.

### Running

Run a playbook:
```sh
ansible-playbook -i inventory.yml provisioning/playbook.yml
```

Run with a formatted output on error:
```sh
ANSIBLE_STDOUT_CALLBACK=yaml ansible-playbook provisioning/playbook.yml
```

Pass a variable by key/value:
```sh
ansible-playbook -e "myvar=myvalue myvar2=myvalue2" provisioning/playbook.yml
```
Attention, when passing a boolean value as values are always interpreted as
strings, you must pass an empty string for a False value and any non-empty
string for a True value:
```sh
ansible-playbook -e "myboolvar1= myboolvar2=1" provisioning/playbook.yml
```

Pass variable values using JSON:
```sh
ansible-playbook -e '{"myvar":"myvalue","myvar2:myvalue2"}' provisioning/playbook.yml
```

Pass variables using JSON or YAML file:
```sh
ansible-playbook -e '@mycfg.yml' provisioning/playbook.yml
```

Check syntax, but does not execute anything:
```sh
ansible-playbook --syntax-check provisioning/playbook.yml
```

Run on localhost:
```sh
ansible-playbook --connection=local --inventory localhost, --limit localhost provisioning/playbook.yml
```

Set the Python interpreter to use (by default it is `/usr/bin/python`):
```sh
ansible-playbook -e ansible_python_interpreter=/usr/bin/python3 provisioning/playbook.yml
```

### roles

 * [Ansible Roles and Variables](https://www.dasblinkenlichten.com/ansible-roles-and-variables/).

Inside a role, variables can be defined inside `defaults/main.yml` or `vars/main.yml`.
`vars` takes precedence on `defaults`.

### evaluation of expressions/code {{ }}

Double curly braces delimit an expression that will be evaluated.
It can contain variables:
```yaml
---
- mymodule: "{{ some_var }}"
```

Remove 4 last characters of a string:
```yaml
---
- mymodule: "{{ some_var[:-4] }}"
```


### variables

 * [Using Variables](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html).
 * [Ansible Roles and Variables](https://www.dasblinkenlichten.com/ansible-roles-and-variables/).
 * [Understanding variable precedence](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#understanding-variable-precedence).

Declare variable:
```yaml
foo:
  field1: one
  field2: two
```
Use it either as `foo['field1']` or `foo.field1`.

Use a variable:
```yaml
mytask:
    args: {{foo.field1}}
```

To define a variable for a role, use either `roles/myrole/defaults/main.yml`
(overwritten by everything) or `roles/myrole/vars/main.yml` (overwritten by
`-e` argument):
```yaml
---
myvar: myvalue
```

Getting the value of an environment variable:
```yaml
{{ ansible_env.HOME }}
```

### built-in variables

 * [Magic variables](https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html#magic-variables).

Get the folder containing the current playbook:
```yaml
---
- mymodule: "{{ playbook_dir }}"
```

### Modules

#### apache2_module

Works only on Debian platforms (because it needs `a2enmod` and `a2dismod`).
==> Does not work on RedHat platforms.
```yaml
- name: Enable rewrite module
  apache2_module:
    state: present
    name: rewrite
```

On RedHat, one must install the package explicitly:
```yaml
- package:
    name: mod_ssl
    state: present
```

#### blockinfile

 * [blockinfile – Insert/update/remove a text block surrounded by marker lines](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/blockinfile_module.html#ansible-collections-ansible-builtin-blockinfile-module).

#### debug

Print debug messages.

 * [debug – Print statements during execution](https://docs.ansible.com/ansible/latest/modules/debug_module.html).

Print variable content:
```yaml
- name: Print myvar
  debug:
      msg: "{{ myvar }}"
```

Print the output of a command:
```yaml
- name: Run mycmd
  shell: mycmd
  register: mycmd_output

- name: Print mycmd output
  debug:
      msg: "{{ mycmd_output }}"
```

#### lineinfile

 * [lineinfile – Manage lines in text files](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html).

Insert or modify a line inside a file.

Insert a line:
```yaml
- name: Disable Postgresql package from the updates repository
  lineinfile:
      path: '/etc/yum.repos.d/CentOS-Base.repo'
      line: '[updates]'
      insertafter: '^exclude=postgresql*'
```

#### locale_gen

 * [locale_gen – Creates or removes locales](https://docs.ansible.com/ansible/latest/modules/locale_gen_module.html).

```yaml
- name: Add FR locale
  locale_gen:
    name: fr_FR.UTF-8
    state: present
```

#### file

 * [file – Manage files and file properties](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html).

Create a directory (mkdir):
```yaml
- name: Create directory for SSL certificate
  file:
    path: /etc/apache2/ssl
    state: directory
    mode: '0755'
```

Create a symbolic link (symlink):
```yaml
- name: Create a symbolic link for SSL conf
  file:
    src: /my/original/file
    dest: /path/to/symlink
    state: link
```

#### service

 * [service – Manage services](https://docs.ansible.com/ansible/latest/modules/service_module.html).

```yaml
- name: Ensure MongoDB is running
  service:
    name: mongod
    state: started
    enabled: yes
```

#### stat

 * [stat – Retrieve file or file system status](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/stat_module.html#return-values).

Tests if a file exists to condition the running of a task:
```yaml
- name: Check if PostgreSQL has been initialized
  stat:
    path: /var/lib/pgsql/10/data/postgresql.conf
  register: psql_conf_file
  become: yes
  become_user: postgres
  
- name: Init PostgreSQL
  command: postgresql-10-setup initdb
  when: not psql_conf_file.stat.exists
```

### Directives

#### become (privilege escalation)

The `become` directive can be used inside a playbook, a role or a task.

At the task level:
```yaml
- name: My task
  command: some_cmd
  become: yes
  become_user: john # Default is root
```

At the playbook level:
```yaml
- hosts: all
  become: yes
  roles:
    - role1
    - role2
```

Privilege escalation can also be set on each role inside playbook:
```yaml
- hosts: all
  roles:
    - { role: role1, become: yes }
    - role2
```

On the role level, create a file `tasks/task.yml` for all your tasks, and then
create the `tasks/main.yml` with:
```yaml
---
- { include: tasks.yml, become: yes }
```

#### when, `changed_when` and `failed_when`

 * [Ansible `changed_when` and `failed_when` examples](https://www.middlewareinventory.com/blog/ansible-changed_when-and-failed_when-examples/).

### `openssl_*`

Generate a private key:
```yaml
- name: Generate an OpenSSL private key with the default values (4096 bits, RSA)
  openssl_privatekey:
    path: /etc/apache2/ssl/server.pem
```

Generate a CSR file:
```yaml
- name: Generate an OpenSSL Certificate Signing Request with Subject information
  openssl_csr:
    path: /etc/apache2/ssl/server.csr
    privatekey_path: /etc/apache2/ssl/server.pem
    country_name: FR
    organization_name: CEA
    email_address: pierrick.roger@cea.fr
    common_name: www.cea.fr
```

Generate a self signed certificate:
```yaml
- name: Generate a Self Signed OpenSSL certificate
  openssl_certificate:
    path: /etc/apache2/ssl/server.crt
    privatekey_path: /etc/apache2/ssl/server.pem
    csr_path: /etc/apache2/ssl/server.csr
    provider: selfsigned
```

### script

 * [script – Runs a local script on a remote node after transferring it](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/script_module.html).

### `win_shell`

 * [ansible.windows.win_shell – Execute shell commands on target hosts](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_shell_module.html#ansible-collections-ansible-windows-win-shell-module).

### Jinja 2 extensions

 * [Filter plugins](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html#filter-plugins).
 * [Using filters to manipulate data](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_filters.html#using-filters-to-manipulate-data).

#### ternary

```yaml
foo: "{{ (status == 'needs_restart') | ternary('restart', 'continue') }}"
```

```yaml
foo: "{{ enabled | ternary('no shutdown', 'shutdown', omit) }}"
```

#### lookup

 * [Lookup plugins](https://docs.ansible.com/ansible/latest/plugins/lookup.html).

Try to open a JSON file and parse it:
```yaml
db_users: "{{ lookup('file', db_users_file, errors='ignore') | default('{}', true) | from_json }}"
```
If the file does not exist, content defaults to an empty dictionary.
## Puppet

Configuration management utility.

 * [Puppet Wikipedia page](https://en.wikipedia.org/wiki/Puppet_%28software%29).
