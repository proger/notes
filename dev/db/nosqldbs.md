# NoSQL databases
<!-- vimvars: b:markdown_embedded_syntax={'sh':'bash','js':'javascript'} -->

## RRDtool

 * [RRDtool](https://en.wikipedia.org/wiki/RRDtool).

## Triplestore

Made to store RDF (Resource Description Framework) data.

 * [Triplestore](https://en.wikipedia.org/wiki/Triplestore).
 * [Resource Description Framework](https://en.wikipedia.org/wiki/Resource_Description_Framework).
   - [Notation3](https://en.wikipedia.org/wiki/Notation3).

A Triplestore stores information like "Bob is 35".

