# elasticsearch

 * [Elasticsearch official site](https://www.elastic.co/elasticsearch/).
 * [Elasticsearch on Wikipedia](https://en.wikipedia.org/wiki/Elasticsearch).
 * [Past Releases](https://www.elastic.co/downloads/past-releases).

 * [Data in: documents and indices](https://www.elastic.co/guide/en/elasticsearch/reference/current/documents-indices.html_).

 * [Java REST Client](https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-high-supported-apis.html).
 
 * [Elasticsearch Guide](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html).
   + [REST APIs](https://www.elastic.co/guide/en/elasticsearch/reference/current/rest-apis.html).
   + [Set up minimal security for Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/security-minimal-setup.html).

 * Elasticsearch Python module:
   + https://www.elastic.co/guide/en/elasticsearch/client/python-api/current/overview.html
   + https://elasticsearch-py.readthedocs.io/en/latest/api.html

 * Search:
   + [The search API](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-your-data.html).

   + [Script query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-script-query.html).

 * [Elastic Stack](https://www.elastic.co/elastic-stack/) (aka *ELK Stack*): Elasticsearch, Kibana, Beats and Logstash.

 * [Service accounts](https://www.elastic.co/guide/en/elasticsearch/reference/current/service-accounts.html).

 * [Built-in roles](https://www.elastic.co/guide/en/elasticsearch/reference/current/built-in-roles.html).

Role configuration is in `config/roles.yml`.

A cluster ES is composed of some master nodes, some worker nodes and/or data nodes. <!-- TODO Difference between worker and data nodes -->

Distributed search engine using *lucene*.
Its interface is a REST API with JSON for data exchange.
Based on Apache Lucene.


Indices can be divided into shards and each shard can have zero or more
replicas.
Each node hosts one or more shards and acts as a coordinator to delegate
operations to the correct shard(s). 
Related data is often stored in the same index, which consists of one or more
primary shards, and zero or more replica shards.
Once an index has been created, the number of primary shards cannot be changed.

Sharding : we devide an index into multiple shards.
One it is done, it is fixed.
A new index however can be divided into new shards. Example: with machines log,
news logs are created each day, we create a new index for each day, and thus
create a new sharding.

Each shard is an Apache Lucene instance.

Get cluster status:
```sh
```

Create a new index `products`:
```sh
curl -X PUT 'http://localhost:9200/products'
```

## Service tokens

 * [Service accounts](https://www.elastic.co/guide/en/elasticsearch/reference/current/service-accounts.html).
 * [elasticsearch-certutil](https://www.elastic.co/guide/en/elasticsearch/reference/current/certutil.html). Creates certicates for TLS.

Generate a service token with the CLI:
```sh
/usr/share/elasticsearch/bin/elasticsearch-service-tokens create elastic/kibana mytoken
```
The generated token is only valid for this particular node.

To generate a token that will be broadcast to all nodes, use the API:
```sh
curl -X POST -k -u "elastic:mypassword" https://localhost:9200/_security/service/elastic/kibana/credential/token/mytoken
```

## Roles

 * [Defining roles](https://www.elastic.co/guide/en/elasticsearch/reference/current/defining-roles.html).
 * [Create or update roles API](https://www.elastic.co/guide/en/elasticsearch/reference/current/security-api-put-role.html).

## Users

 * [Create or update users API](https://www.elastic.co/guide/en/elasticsearch/reference/current/security-api-put-user.html).

## Indexes

Get info about all indexes:
```sh
curl http://elasticsearch.server:9200/_all?pretty
```

Storing metadata about an index: see [`_meta` field](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-meta-field.html).

### Mapping

 * [fields (multi-fields)](https://www.elastic.co/guide/en/elasticsearch/reference/current/multi-fields.html).

## Data streams

 * [Data streams](https://www.elastic.co/guide/en/elasticsearch/reference/current/data-streams.html).

A Data stream is regroupment of several indexes for searching and appending data.

## test server / info

Test if server is running:
```sh
curl http://elasticsearch.server:9200/
```

Test in SSL with username/password:
```sh
curl -u "username:password" https://elasticsearch.server:9200
```

## health (server status)

```sh
curl 'http://elasticsearch.server:9200/_cat/health?v'
```

## `_msearch`

Multiple searches in one request.

## `_search`

 * [The search API](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-your-data.html).
 * [Aggregations](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations.html).

By default returns 10 results max. Set `"size": 100` to get 100 documents.

Search for all elements in index `myindex` (`_index` field) and sort:
```sh
curl "http://elasticsearch.server:9200/myindex/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": { "match_all": {} },
  "sort": [
    { "n": "asc" }
  ]
}'
```

Search for all elements in all data streams and indices:
```sh
curl 'http://elasticsearch.server:9200/_search?pretty'
curl 'http://elasticsearch.server:9200/_all/_search?pretty'
curl 'http://elasticsearch.server:9200/*/_search?pretty'
```

Retrieve only one field (`pos`), and filter on index:
```sh
curl "http://elasticsearch.server:9200/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "_source": ["pos"],
  "query": {
    "match": { "_index": "bex_canvas_output.vcf.gz"}
  }
}'
```
