# SQLite
<!-- vimvars: b:markdown_embedded_syntax={'sql':'','sh':'bash','python':''} -->

 * [Datatypes In SQLite Version 3](https://www.sqlite.org/datatype3.html).
 * [Getting Started with SQLite Full-text Search](https://www.sqlitetutorial.net/sqlite-full-text-search/).

Full-text search with `MATCH`:
 * Boolean operators: `NOT`, `OR`, and `AND`.
 * Parenthesis.

Run SQL statement on SQLite file with `sqlite3` command line:
```sh
sqlite3 myfile.sqlite "select * from mytable;"
```

List tables:
```
.tables
```

## Types

 * [Datatypes In SQLite Version 3](https://sqlite.org/datatype3.html).

## Transaction (BEGIN TRANSACTION)

```sql
BEGIN TRANSACTION;
-- ...
COMMIT TRANSACTION;
-- ...
ROLLBACK TRANSACTION;
```

## Concurrent writing (BEGIN CONCURRENT)

 * [BEGIN CONCURRENT](https://www.sqlite.org/cgi/src/doc/begin-concurrent/doc/begin_concurrent.md).

## Copy elements between tables

```sql
INSERT INTO bibrefsTmp (key, title, link) SELECT key, title, ref FROM bibrefs;
```

## Altering a table

It is not possible to alter the structure of table in SQLite.
The solution is to create a new table and copy the elements in it.

However it is possible to rename a table:
```sql
ALTER TABLE mytable RENAME TO newname;
```

## Python

 * [sqlite3](https://docs.python.org/3/library/sqlite3.html).
