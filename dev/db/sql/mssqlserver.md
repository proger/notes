# MS SQL Server
<!-- vimvars: b:markdown_embedded_syntax={'sql':'','sh':'bash'} -->

## Creating a new database

Launch "Management Studio".

Right clik on "Databases", then "New database".

## Set access rights for a database

Right click on the database, then `Property` --> `Authorisations` --> `Add...`
Select or unselect authorized actions: `Connect`, `Select`, ...

## List tables

```sql
select table_name from information_schema.tables where table_catalog='ratppcc' and table_schema='public';
```


