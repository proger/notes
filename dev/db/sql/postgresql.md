# PostgreSQL
<!-- vimvars: b:markdown_embedded_syntax={'sql':'','sh':'bash'} -->

## Installation

 * [PostgreSQL](https://wiki.archlinux.org/index.php/PostgreSQL#Installation).
 * [YUM Installation](https://wiki.postgresql.org/wiki/YUM_Installation).

Before running commands with postgresql, one must first become the postgres user:
```bash
sudo -iu postgres
```

## Starting server

Start server from shell manually:
```bash
postgres -D /usr/local/var/postgres
```

or start service with systemd:
```bash
systemctl start postgresql
```

## Interpreter

```bash
psql
```

Connect onto specific database (prompt for password):
```bash
psql -h hostname -p port -U username -W database
```

To connect automatically without being prompt for password,
put password into `~/.pgpass` file in the form
`hostname:port:database:username:password`. Restrict access rights to u+rw.
Then:
```sh
psql -h hostname -p port -U username -w database
```

Run as `postgres` user:
```bash
sudo -u postgres psql
```

## Initializing a repository

`initdb` initializes a database repository (i.e.: the folder where all databases are stored).

## Creating a new database

Create a new database for the running user:
```bash
createdb dbname
```

From SQL:
```sql
create database mydb;
```

## Selecting a database

```
\connect mydb
\c mydb
```

## Deleting a database

```sql
drop database dbname;
```

## `pg_database`

 * [pg_database](https://www.postgresql.org/docs/current/catalog-pg-database.html).

This table gathers various information about existing databases:
```sql
SELECT * FROM pg_database;
```

## Running SQL commands

For running SQL commands, you need to start the `psql` command:
```bash
psql -h hostname -p port -U username database
```

To run an SQL file:
```bash
psql -h hostname -p port -U username -W -f file.sql database
```

`psql` special commands:

Command         Description
------------    --------------------
\list           List databases.
\du             List users.
\dt             List all tables of a database.
\q              Quit.
\d+ mytable     Get information about a table.

## Users

Create user
```sql
create user myuser with encrypted password 'mypass';
grant all privileges on database mydb to myuser;
```

Reset user's password:
```sql
ALTER USER ratpuser WITH PASSWORD 'ratp';
```

## Dumping and restoring

For dumping a database into a file:
```bash
pg_dump -C -h host -U user dbname
```

For copying a database:
```bash
pg_dump ... | psql ...
```

To restore a database from a binary file dumped by `pg_dump`.
```bash
pg_restore -d dbname filename
```

## Table

Show table info:
```
\d mytable
```

## Copy

 * [COPY](https://www.postgresql.org/docs/9.5/static/sql-copy.html).

Output a CSV file:
```sql
\copy (select * from mytab) To myfile.csv With csv

Output a CSV file with a header:
```sql
\copy (select * from mytab) To myfile.csv With csv header
```

## Data types

 * [Chapter 8. Data Types](https://www.postgresql.org/docs/9.5/datatype.html).

Type                     | Description
------------------------ | ------------------
timestamp                | 
timestamp with time zone | 
text                     | variable unlimited length
