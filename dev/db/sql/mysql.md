# MySQL
<!-- vimvars: b:markdown_embedded_syntax={'sql':'','mysql':'','sh':'bash'} -->

 * [9.3.1 How MySQL Uses Indexes](http://dev.mysql.com/doc/refman/5.7/en/mysql-indexes.html).

## Install & run

Installing on macOS:
```bash
brew install mysql
```

Running mysql:
```bash
mysql.server start
```

Installing MariaDB on Archlinux:
```bash
sudo pacman -S mariadb
sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl enable --now mariadb
```

## Command line

Default port is 3306.

Enter a root session:
```bash
mysql -u root
```

Change root password:
```bash
mysqladmin -u root password NEWPASSWORD
```

Check that the server is running:
```sh
mysqladmin version
mysqladmin variables
```

List databases:
```sh
mysqlshow -uroot
```

List containt of a database:
```sh
mysqlshow -uroot mydatabase
```

Connect:
```sh
mysql --host=127.0.0.1 --port=53306 --user=ratpuser --password=ratp ratp_subwayline
```

To run a command or start a SQL shell:
```sh
mysql [--user=<user_name> --password=<password>] [-e "<SQL command>"] <database>
```

Connect to mysql shell:
```sh
mysql -h host -u user -p=password
```
or if you want mysql to prompt for password
```sh
mysql -h host -u user -p
```

Run an SQL command:
```sh
mysql -e "SELECT Host,Db,User FROM db" mysql
```

Run an SQL script file:
```sh
mysql db_name < text_file
```

## Shell commands

Quit shell:
```mysql
quit
```

## Security

To get a list of users:
```mysql
select user, host, password from mysql.user;
```

Add a user account:
```mysql
create user myuser identified by 'mypassword';
```

Remove user account:
```mysql
drop user myuser
```

Grant access from localhost only:
```mysql
grant all privileges on mydb.* to 'myuser'@'localhost' identified by 'mypassword';
```

Grant access from any machine:
```mysql
grant all privileges on mydb.* to 'myuser'@'%' identified by 'mypassword';
```

Grant a user the right to create databases matching a pattern:
```mysql
grant all privileges on `mydbprefix\_%` . * to 'myuser'@'localhost';
```

Set password:
```mysql
set password for 'myuser'@'localhost' = password(''); -- no password
set password for 'myuser'@'localhost' = password('mypassword');
```

To check privileges, first login:
```bash
mysql -y myuser
```
Then run:
```mysql
show grants;
```

## Creating and using a database

Create a database:
```mysql
create database mydb;
```

Create a database if it not already exists:
```mysql
create database if not exists mydb;
```

Print list of databases:
```mysql
show databases;
```

Use a database:
```mysql
use mydb;
```

Recreate a database and use it:
```mysql
drop database if exists mydb;
create database mydb character set utf8;
use mydb;
```

## Tables

List table:
```mysql
show tables;
```

Show columns of a table:
```mysql
show columns from mytable;
```

View the execution plan of the database engine:
```mysql
explain myquery;
```

Optimize a table:
```mysql
optimize table mytable;
```

Analyze a table:
```mysql
analyze table mytable;
```

## REGEXP

 * [MySQL REGEXP: Search Based On Regular Expressions](https://www.mysqltutorial.org/mysql-regular-expression-regexp.aspx).

## Import/export CSV file

 * [Import CSV File Into MySQL Table](http://www.mysqltutorial.org/import-csv-file-mysql-table/).

## Strings

By default MySQL is installed with latin1 encoding.

To set the encoding for a whole database:
```mysql
create database foreign_sales character set utf8;
```

To set the encoding of a field of a table:
```mysql
create table my_table
(
	some_field  varchar(20) character set utf8
);
```

When connecting to a database, the following command must be run before issuing commands (insert, select or update) with UTF8 characters:
```mysql
set names utf8;
```

## Case statement

```mysql
case id
	when 1 then 0
	when 4 then 1
	else id
end
```


