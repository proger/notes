# Oracle database
<!-- vimvars: b:markdown_embedded_syntax={'sql':'','sh':'bash'} -->

PL/SQL (Procedural Language/Structured Query Language): Oracle's extension of SQL.

## Case statement

In Oracle, there is an equivalent of case statement: `decode()`.
