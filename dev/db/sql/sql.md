# Standard SQL

 * [SQL Tutorial](https://www.tutorialspoint.com/sql/index.htm).

## Comments

```sql
-- a comment on one line
```

```sql
/* a comment
   on several
   lines */
```

## Data types

 * [Standard SQL Data Types](https://cloud.google.com/bigquery/docs/reference/standard-sql/data-types).

Type        | Description
----------- | -------------------------
float64     | 8 bytes float.
numeric     | 16 bytes float.
text        |
int64       |
bool        |
string      | variable length unicode character string
date        |
varchar(255)| 

## create table

Create a table with an auto-incremented column:
```sql
create table mytable (
	   id int not null auto_increment);
```

Create a table with a primary key:
```sql
create table mytable (
	   id int not null,
	   primary key(id));
```

Create a table with a reference to a primary key of another table:
```sql
create table mytable (
	   local_id int not null,
	   other_id int not null,
	   foreign key(other_id) references other_table(id));
```

```sql
CREATE TABLE mytable (
	id INT PRIMARY KEY NOT NULL,
	s text,
	key text NOT NULL,
	n INTEGER NOT NULL
);
```

## drop table

rop a table:
```sql
drop table if exists mytable;
```

Show tables of current database:
```sql
show tables;
```

## alter table

Drop a column:
```sql
ALTER TABLE table_name DROP COLUMN column_name;
```

Drop multiple columns:
```sql
ALTER TABLE table_name DROP COLUMN col1, DROP COLUMN col2, DROP COLUMN col3;
```

## insert

Insert into a table:
```sql
INSERT INTO mytable (col1, col2, col3, ...) VALUES (val1, val2, val3, ...);
```

## delete

```sql
delete from mytable where mycol = 'somevalue';
```

## index

Create an index:
```sql
create index indexname on tablename(columnname);
```

Show all indices of a table:
```sql
show index from tablename;
```

Remove an index:
```sql
drop index indexname on tablename;
```

Indices implementation: usually they are implemented as tree structures to allow fast insertion, update and deletion.

Use of a function when accessing an index must be prohibited. For instance, writing the following where clause:
```sql
where f(indexed_column) = 'some value'
```
will disable the indexed search since the function f() has no reason to respect the sorting order of `indexed_column`. The RDBMS will thus execute a sequential data scan.
A classical example is when looking for a substring inside an index:
```sql
where substr(name, 3, 1) = 'R'
```
Another classical example is when looking for a date. Suppose you have an indexed column `date_entered` which contains a date+time value, and you want to retrieve all rows of a particular day (whatever is the time). If you run something like this:
```sql
where date_entered = to_date('18-JUN-1815', 'DD-MON-YYYY')
```
you would only retrieve the rows whose date is exactly the 18th June 1815 at 00:00.
So you would tempted to use the trunc() function on the date column in order to get rid of the time and write:
```sql
where trunc(date_entered) = to_date('18-JUN-1815', 'DD-MON-YYYY')
```
Of course this query will work, and thus correct the previous mistake, but you will also go without the index on `date_entered`.
It would be better to use inequalities:
```sql
where date_entered >= to_date('18-JUN-1815', 'DD-MON-YYYY') and date_entered < to_date('19-JUN-1815', 'DD-MON-YYYY')
```

Attention with implicit conversions by RDBMS. For instance Oracle will convert the column to integer implicitly in order to execute the following where clause:
```sql
where account_number = 12345
```
The implicit conversion is done on the column because we would like the comparison '0000012345' = 12345 to work, and also because we would like to detected checking of the column value (if the conversion of a value in the column is not possible, it will raise an error). Such implicit conversions prevent the use of indexes.

### Key generation

How to generate a new unique key value ?

See "The Art of SQL", p. 70.

 0. Use unique identifier if already available instead of generating new key value, which has moreover no link whatsoever to the data.

 1. Looking for the greatest numerical value and incrementing by one, can be subject to issues in a concurrent environment.

 2. Using a "next value" that has to be locked each time we need to get a new key (and increase the "next value") serializes and slows down accesses.

 3. Using a system-generated key (auto-increment key) in a concurrent environment, leads to generate numbers that are in close proximity to each other. Consequently all the processes, when inserting these new key values, will converge on the same index page and RDBMS will have to serialize.

 4. Some RDBMS, like Oracle, propose the use of REVERSE INDEXES, i.e. indexes in which the sequence of bits are inversed transparently. This solves the issue of point 3, since integer or string keys won't be stored in natural order inside the tree (close numbers won't be stored next to each other). However, using a string reversed index, will prohibit us of using a search syntax like:
```sql
where name like 'M%'
```
since the RDBMS won't be able to use the index to optimize this request. This is because the tree is no more constructed using an alphabetical comparison function. Same issue arises for timestamp indexes.

 5. Some RDBMS propose the use of hash indexes. A hash index uses a hash function to transform its value into a meaningless randomly distributed numeric key. Two close values will be transformed into two distant values, thus avoiding collisions. This doesn't prevent two values to be transformed into the same hash value, but reduces drastically such a probability. However hash index prohibits range searches.

### Index and order of filtering conditions

From "The Art of SQL", p.91:
```sql
select distinct c.custname
from customers c
	join orders o
		on o.custid = c.custid
	join orderdetail od
		on od.ordid = o.ordid
	join articles a
		on a.artid = od.artid
where c.city = 'GOTHAM'
	and a.artname = 'BATMOBILE'
	and o.ordered >= somefunc;
```
STEPS followed by the RDBMS
 1. Look for rows in customers table where city is GOTHAM.
 2. Search the orders table for rows belonging to customer custid. Which means that custid column should be indexed in orders table.
 3. A clever optimizer will see the condition on order date in where clause, and will filter lines from table orders before performing the join on custid. A less clever optimizer won't see the condition. To help this less clever optimizer, we can move the condition from the where clause to the join clause:
```sql
	join orders o
		on o.custid = c.custid
		and o.ordered >= somefunc
```
 4. If the primary key of orderdetail table is (ordid, artid), then an index is available on ordid and the RDBMS can make use of that. If not the primary key is (artid, ordid) then no index is present on ordid, and the join will be slow. A solution is to provide a separate indexing of ordid column.

### IOT and heap structure --

By default tables are organized in heap structures.

In Oracle, one can specify a table to be an IOT (Index-Organized Table). This means that all of the data of the table will be stored into an index built on the primary key. This saves storage and time, since data is stored with the index.
In a IOT one must be careful about insertion. If the table holds few columns then insert will be fast, otherwise it will be slower than in regular tables organized in heap structures.

## Select

Limit size of selection:
```sql
select * from mytable limit 100;
```

Count all rows of a request:
```sql
select count(*) from mytable;
```

Select specific columns:
```sql
select mycol from mytable;
```

If a column contains special characters like `.`, use backsticks around column name:
```sql
select `my.col` from mytable;
```
The backsticks can also be used in join and where clauses:
```sql
select `my.col` from mytable join table2 on table2.id = mytable.`my.id.col` where table2.`my.other.col` = `my.col`;
```

## order by

Order results by a column in ascending order:
```sql
select mycol from mytable order by myothercol asc;
```

Order in descending order:
```sql
select mycol from mytable order by myothercol desc;
```

## join

### Old join syntax

This syntax is equivalent to the explicit "inner join" notation.

```sql
select distinct c.custname
from customers c,
	 orders o,
	 orderdetail od,
	 articales a
where c.city = 'GOTHAM'
	and c.custid = o.custid
	and o.ordid = od.ordid
	and od.artid = a.artid
	and a.artname = 'BATMOBILE'
	and o.ordered >= somefunc;
```

### Inner join

```sql
select distinct c.custname
from customers c
inner join orders o on c.custid = o.custid
inner join orderdetail od on o.ordid = od.ordid
inner join articales a on od.artid = a.artid
where c.city = 'GOTHAM'
	and a.artname = 'BATMOBILE'
	and o.ordered >= somefunc;
```

## Boolean

```sql
select * from mytab where mycol = TRUE;
```

## Pattern matching

 * [PostgreSQL pattern matching](http://www.postgresql.org/docs/8.3/static/functions-matching.html).

```sql
select * from mytab where mycolumn like 'n%';
```

## Strings

Put in uppercase/lowercase:
```sql
SELECT * FROM mytab WHERE upper(mycolumn) = 'ZAP';
```

Since strings are delimited with apostrophe characters,
apostrophes need to be escaped (using `''`):
```sql
SELECT * FROM mytab WHERE city = 'L''Aquila';
```

## WHERE

Testing if equal:
```sql
SELECT * FROM mytab WHERE mycol = 2;
```

Testing if different:
```sql
SELECT * FROM mytab WHERE mycol != 'zap';
```

## distinct

Remove duplicated rows in results.

From "The Art of SQL", p.91. The following query could return a "wrong" or unexpected result, by removing customer homonyms. See `exists` and `in` for better solutions.
```sql
select distinct c.custname
from customers c
	join orders o
		on o.custid = c.custid
	join orderdetail od
		on od.ordid = o.ordid
	join articles a
		on a.artid = od.artid
where c.city = 'GOTHAM'
	and a.artname = 'BATMOBILE'
	and o.ordered >= somefunc;
```

## Exists

From "The Art of SQL", p.93:
```sql
select c.custname
from customers c
where c.city = 'GOTHAM'
	and exists (select null
	            from orders o,
	            	orderdetail od,
					articles a
				where a.artname = 'BATMOBILE'
					and a.artid = od.artid
					and od.ordid = o.ordid
					and o.custid = c.custid -- <-- correlated subquery, the inner select makes reference to the outer select.
					and o.ordered >= somefunc);
```
The use of the exists keyword in our case, creates a link between the outer query and the inner query. If executed as it is, we would first loop on all customers of GOTHAM city and for each of them execute the inner query in order to know if he/she has bought a batmobile recently. A clever optimizer would/might rewrite the query so it performs better. But a less clever one won't.
Note that it is important to have the custid column of the orders table indexed.

See `in` for a solution with an uncorrelated subquery.

## Combined updates

It is often more efficient to run a single update command for multiple fields, even we reset some fields to the same values, than to run multiple updates, one for each field.
Example with the use of the case statement (from "The Art of SQL", p. 44):
```sql
update tbo_invoice_extractor
	set pga_status = (case pga_status
		                when 1 then 0
		                when 3 then 0
		                else pga_status
		              end),
		rd_status  = (case rd_status
		                when 1 then 0
						when 3 then 0
						else rd_status
		              end)
	where (pga_status in (1,3) or rd_status in (1,3)) and inv_type = 0;
```

## In

Check if a value is in a list:
```sql
select * from mytab where myfield in ('zap', 'hop', 'plouf');
```

Check if a value isn't in a list:
```sql
select * from mytab where myfield not in ('zap', 'hop', 'plouf');
```

From "The Art of SQL", p.94:
```sql
select c.custname
from customers
where city = 'GOTHAM'
	and custid in
		(select o.custid
	     from orders o,
	   	      orderdetail od,
		      articles a
		      where a.artname = 'BATMOBILE'
		      and a.artid = od.artid
		      and od.ordid = o.ordid
		      and o.ordered >= somefunc);
```
The inner query doesn't depend on the outer query, it is an uncorrelated subquery and so will be executed only once.

The outer query performs a test of existence of a custid inside the result list of custids of the inner query. In case the list of custids is very long, we could use the same solution (i.e.: use of the in keyword) inside the inner query:
```sql
select c.custname
from customers
where city = 'GOTHAM'
	and custid in
		(select o.custid
	     from orders
	     where ordered >= somefunc
	     	and ordid in (select od.ordid
						  from orderdetail od,
						       articles a
		                  where a.artname = 'BATMOBILE'
		                  and a.artid = od.artid));
```


