# Relational databases
<!-- vimvars: b:markdown_embedded_syntax={'sql':''} -->

 * [Introduction to Relational Database Design](http://www.edm2.com/0612/msql7.html).
 * [SQL Tutorial](https://www.w3schools.com/sql/default.asp).
 * [SQL 92](http://www.contrib.andrew.cmu.edu/~shadow/sql/sql1992.txt).

Books:
 * [The Art of SQL](https://www.amazon.com/Art-SQL-Stephane-Faroult/dp/0596008945/ref=sr_1_1?s=books&ie=UTF8&qid=1472635113&sr=1-1&keywords=the+art+of+SQL).
 * [Learning SQL]( https://www.amazon.com/Learning-SQL-Alan-Beaulieu/dp/0596520832/ref=sr_1_1?s=books&ie=UTF8&qid=1472635257&sr=1-1&keywords=learning+SQL).
 * [Database Design for Mere Mortals: A Hands-On Guide to Relational Database Design (3rd Edition)](https://www.amazon.com/Database-Design-Mere-Mortals-Hands-/dp/0321884493/ref=sr_1_1?s=books&ie=UTF8&qid=1472635186&sr=1-1&keywords=database+design+for+mere+mortals).

## Design

### Database schema symbols

	⎯┼⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯┼⎯     one-to-one
	
	                  /
	⎯┼⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯     one-to-many
	                  \
	
	\                 /
	⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯     many-to-many
	/                 \
	
	⎯┼┼⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯     mandatory participation
	
	
	                  /
	⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯O⎯     optional participation
	                  \

## Lookup table

A lookup table = a validation table.

It's a table where values (often string values) that are constant or quasi-constant during the lifetime of the database are stored.

Example 1: a table for states that store the two letter state code, and the state name.

Example 2: a categories table that store a category ID and the category name.

## Self referencing table

A self referencing table is one which has a foreign key that uses its own primary key.

Example 1:
A table members with fields: MemberID, MbrFirstName, MbrLastName, SponsorID, etc. Where the SponsorID is a foreign key that uses MemberID.

Example 2 (in a many-to-many relationship):
A table parts with fields: PartID, PartName, etc.
And a table PartComponents with fields: PartID, ComponentID. Where PartID and ComponentID are foreign keys that use parts.PartID.

## Subset table

A subset table is a table that adds information to a main table.

Example:
A table employee with fields EmplID, EmpFirstName, EmpLastName, HomePhone, etc.
With a table compensation with fields EmplID, HourlyRate, CommissionRate, etc.

## Normalization

From "The Art of SQL, Stéphane Faroult, Ed. O'Reilly".

AIM: application of logical rigor.
	
### 2nd Normal Form (2NF)

This norm consists in two points:
1. Ensure atomicity. Attributes must be atomic. Atomicity allows to respect:
	_ the ability to perform an efficient search
	_ data correctness.
   The idea is to do not put different items in one field. In other words: not make too much general fields. Car example: don't make a general column safety_equipment with ABS and airbags inside, but make two columns (ABS and Airbags).

2. Check dependence on the whole key. If we have a table car that lists all existing cars, we need to create a models table to store information that is common for one model and not repeat this information for each instance of a model inside the car table.
  This is for:
	_ avoiding data redundancy
	_ ensure query performance (because a table with too many columns will be slow to query)
	
### 3rd Normal Form (3NF)

The most common of norms.

Imposes 2NF + a 3rd point:
	
3. Check attribute independence. A field's value must not be inferable from another field's value.

A common case is when two fields cannot contain values at the same time. When field A has a value, we know field B is NULL. This violates the 3NF.
	
### Boyce-Codd Normal Form (BCNF)

Usually a database in 3NF is also in BCNF.
It is not BCNF if in one table several columns are all unique identifiers of rows.
	
### 5th Normal Form (5NF)

Usually a database in 3NF is also in 5NF.

## Partitioning

From "The Art of SQL", p. 115

Large tables and indices can be partitioned. This leads to more scalable architectures, by allowing increased concurrency and parallelism.

### Round-robin partitioning

The tables have one or more partitions assigned to it (on different disks).
Each new insertion is done on one of the partitions, in a round-robin fashion. So the data is scattered arbitrarily.

### Data-driven partitioning

The values in one or more columns determine the partition into which insert the row.

 1. The partitioned view solution
For instance a table figures is spliced into 12 tables corresponding to the months (jan_figures, feb_figures, ..., dec_figures).
Then the tables are reunited inside a single figures table ("partitioned view", or "merge table" (MySQL)).

Drawbacks:
 - The DBMS is not informed of the logical relationship which links the different partition tables.
 - We have no way to enforce uniqueness properly across tables, except using a check constraint on each table in order to check the month.
 - We have to build dynamically the insert statements.
 - When querying for the past 30 days, we have most often to query two tables. If the column that rules the placement of rows is part of the definition of the partitioned view, then the DBMS will be able to treat a query on the view and limit the scope to the proper subset of tables. If not, then we will have to build the query dynamically or run a global query on the view.

 2. True data-driven partitioning
One or several columns are defined as "partition key".

   a. Hash-partitioning: an hash algorithm is run on the partition key. So it does not take into account the distribution of data values. However it ensures very fast access to rows.

   b. Range-partitioning: gather data into groups according to continuous data ranges. Ideally suited for dealing with historical data.	An "else" partition is set up for catching everything that might slip through the net.

   c. List-partitioning: this is a tailor-made (manual) solution. For each partition, a set of values of the partition key for this partition is defined manually.

Sub-partitions (partitions inside a partition) are possible, and can involve several types of partitioning (e.g.: hash-partitions within a range-partition).

Data-driven partitioning is not a panacea for resolving concurrency problems.
For instance, if we partition a table by weeks, and so have 52 partitions. Then during each week, everybody will insert in the same table. On the other hand, if we partition geographically, the inserts should be spread over all partitions.
Thus partitioning is useful to speed up either retrieval or insertion, but not both at the same time, or at least not all type of retrievals and insertions.

Partitioning is not always a solution. For instance in a database of purchase orders, when one big customer represent 80% of the sells, it's no use to partition according to the customer ID.
In consequence, partitioning is well suited when data is uniformly spread in respect to the partitioning key.

Avoid partitioning on values which may change in future, since an "update" of a partition key value inside a partitioned database means a "delete" + an "insert", which are costly. However exceptions can exist, and having a partition key which can be updated isn't necessarily bad (see the process queue example with partition key on type or status, inside "The Art of SQL", p. 121).

