# MongoDB
<!-- vimvars: b:markdown_embedded_syntax={'js':'javascript','javascript':'','sh':'bash'} -->

 * [The MongoDB 4.4 Manual](https://docs.mongodb.com/manual/).
 * [MongoDB Tutorials](https://docs.mongodb.com/manual/tutorial/).
 * [MongoDB Tutorial](https://www.tutorialspoint.com/mongodb/).

RDBMS       | MongoDB
----------- | -------
Database    | Database
Table       | Collection
Tuple/Row   | Document
column      | Field
Primary Key | Primary Key (Default key `_id` provided by MongoDB itself)

 * Documents are written in JSON.
 * No concept of relationship between collections.
 * Data is stored and transferred using [BSON](https://fr.wikipedia.org/wiki/BSON) format.

## Installation

 * [Install MongoDB Community Edition on Red Hat or CentOS](https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-red-hat/).

On ArchLinux:
```sh
yay -S mongodb
```

Installing on CentOS.
Create a `/etc/yum.repos.d/mongodb-org-6.0.repo` file with:
```
[mongodb-org-6.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/6.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-6.0.asc
```
Then run:
```sh
sudo yum install -y mongodb-org
```
Or for a specific release:
```sh
sudo yum install -y mongodb-org-6.0.1 mongodb-org-database-6.0.1 \
    mongodb-org-server-6.0.1 mongodb-mongosh-6.0.1 mongodb-org-mongos-6.0.1 \
    mongodb-org-tools-6.0.1
```

For Ubuntu, see <https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/>.
For `mongosh` client only install `mongodb-mongosh` package.

Configuration file is `/etc/mongodb.conf`.

Default data store path is `/data/db`. Create it with 'rwx' permissions for user mongodb.

## Start daemon

For starting `mongod` manually, run:
```sh
mongod
```

If you want to set a custom database path:
```sh
mkdir my/db/folder
mongod --dbpath my/db/folder
```

Running as a daemon:
```sh
mongod --fork --logpath mongo.log
```

## Interpreter

  * [Download MongoDB Shell](https://www.mongodb.com/try/download/shell?jmp=docs).

`mongosh` replaces the deprecated `mongo` shell.

Install the latest `mongosh` and call it:
```sh
curl -LO "https://downloads.mongodb.com/compass/mongosh-1.5.4-linux-x64.tgz"
tar -xzf mongosh-1.5.4-linux-x64.tgz
mongosh-1.5.4-linux-x64/bin/mongosh ...
```

To start interpreter:
```sh
mongosh
mongosh -u admin -p admin --authenticationDatabase admin
mongosh --host localhost --port 27017
```

Run javascript on command line:
```sh
mongo --quiet --eval "db.collection.find()" mydb
```

Other commands like `show collections` must be run through stdin:
```sh
mongo --quiet mydb <<<"show collections"
```

Run javascript from stdin:
```sh
mongo --quiet -u admin -p admin --authenticationDatabase admin <<END_JS
"use strict";
use mydb;
db.collection.find();
END_JS
```

Run quietly (less verbose, but still output results):
```sh
mongo --quiet ...
```

Authenticate from inside the interpreter:
```js
db.auth('myuser', 'mypassword');
db.auth('myuser', passwordPrompt());
```

To get help:
```js
db.help()
```

Print version:
```js
db.version()
```

The interpreter can also be run in script mode. Script are written in Javascript, so the syntax is different from the shell.
See [Write Scripts for the mongo Shell](https://docs.mongodb.com/manual/tutorial/write-scripts-for-the-mongo-shell/).
Example of running a Javascript:
```bash
mongo mylocaldb myscript.js
```

Exit:
```js
exit
```

Select database in the interpreter:
```
use admin
```
or in Javascript:
```js
conn = new Mongo();
db = conn.getDB("myDatabase");
```

## Server status

Get server status:
```js
use admin
db.serverStatus()
```

Get connections info:
```js
use admin
db.serverStatus().connections
```

## Security

 * [Introduction to MongoDB](https://docs.mongodb.com/manual/introduction/).
 * [User Management Methods](https://docs.mongodb.com/manual/reference/method/js-user-management/).
 * [Manage Users and Roles](https://docs.mongodb.com/manual/tutorial/manage-users-and-roles/).
 * [Collection-Level Access Control](https://docs.mongodb.com/manual/core/collection-level-access-control/).
 * [Role-Based Access Control](https://docs.mongodb.com/manual/core/authorization/).
 * [Privilege Actions](https://docs.mongodb.com/manual/reference/privilege-actions/).
 * [db.auth()](https://docs.mongodb.com/manual/reference/method/db.auth/).

Create an admin user:
```js
use admin
db.createUser(
	{
		user: "myUserAdmin",
		pwd: passwordPrompt(), // or cleartext password
		roles: [{ role: 'root', db: 'admin' }, { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
	}
)
```

Inside the configuration file `/etc/mongodb.conf` set:
```cfg
security:
 authorization: enabled

setParameter:
  enableLocalhostAuthBypass: false
```

Authenticate when connecting with `mongo`:
```bash
mongo -u admin -p admin --authenticationDatabase "admin"
```
Or from inside `mongo`:
```js
db.auth('myuser', 'mypassword');
```

List users:
```js
db.getUsers()
```

Create read-only user for a database mydb:
```js
use mydb
db.createUser({user:'pierrick',pwd:'cea', roles:[ "read"]})
```
Roles can be defined for other database as well:
```js
use mydb
db.createUser({user:'pierrick',pwd:'cea', roles:[ { role: "read", db: "fake_exhalobase"}]})
```

Create read/write user:
```js
use mydb
db.createUser({user:'pierrick',pwd:'cea', roles:["readWrite"]})
```

### Roles

 * [Create a User-Defined Role](https://docs.mongodb.com/manual/tutorial/manage-users-and-roles/#create-a-user-defined-role).

Print defined roles:
```js
db.getRoles({showPrivileges:1})
```

Create a role:
```js
db.createRole(
   {
     role: "manageOpRole",
     privileges: [
       { resource: { cluster: true }, actions: [ "killop", "inprog" ] },
       { resource: { db: "", collection: "" }, actions: [ "killCursors" ] }
     ],
     roles: []
   }
)
```

"Except for roles created in the admin database, a role can only include privileges that apply to its database and can only inherit from other roles in its database."

To grant a role (from another database) to a user:
```js
db.grantRolesToUser("<username>", { role: "<role>", db: "<database>" })
```

## Cloning a database

 * [Copy/Clone a Database](https://docs.mongodb.com/manual/reference/program/mongodump/#mongodump-example-copy-clone-database).
 * [How do I copy a database from one MongoDB server to another?](https://stackoverflow.com/questions/5495137/how-do-i-copy-a-database-from-one-mongodb-server-to-another).

Copying a database from one server to another:
```javascript
db.copyDatabase(<from_db>, <to_db>, <from_hostname>, <username>, <password>);
```
You must be on the destination server.

Cloning with dump/restore in two steps:
```sh
mongodump --archive="mongodump-test-db" --db=test
mongorestore --archive="mongodump-test-db" --nsFrom='test.*' --nsTo='examples.*'
```
and in one step:
```sh
mongodump --archive --db=test | mongorestore --archive  --nsFrom='test.*' --nsTo='examples.*'
```

There is an option `--dumpDbUsersAndRoles` for `mongodump` for dumping users and roles, and an option `--restoreDbUsersAndRoles` for `mongorestore` for restoring users and roles. However restoring users does not work when restoring to a different destination database (`--toDb`), it seems only to work when restoring to the same database name.

## Managing databases

Getting statistics about a database:
```js
db.stats()
```

Create a database, or switch to it:
```js
use mydb
```

Print current database:
```js
db
```

List all databases:
```js
show dbs
```
Note: to exist, a database must at least contain one document.

Drop a database:
```js
db.dropDatabase()
```

## Collections

Collections are automatically created when inserting documents. However it is possible to create an empty collection, in order to define particular options, using the following command:
```js
db.createCollection("mycol", { capped : true, autoIndexId : true, size : 6142800, max : 10000 } )
```

List collections:
```js
show collections;
// OR
db.getCollectionNames();
```

Test if a collection is defined:
```js
db.getCollectionNames().includes('mycoll');
```

Access a collection by name:
```js
db.mycol
// or
db.getCollection('mycol')
```

To insert a document into a collection:
```js
db.mycol.insert({"myfield" : "myvalue"})
```

To drop a collection:
```js
db.mycol.drop()
```

### find

 * [db.collection.find()](https://docs.mongodb.com/manual/reference/method/db.collection.find/).

To list the content of a collection:
```js
db.mycoll.find()
```
or
```js
db.mycoll.find().pretty()
```
to have JSON printed with new lines.

Search with a filter:
```js
db.mycoll.find({myfield:'myvalue'})
```

Search for document having a certain field:
```js
db.mycoll.find({myfield: {$exists: true}})
```

Search for a value in a list (same as single value):
```js
db.mycoll.find({myfield:'myvalue'})
```

Search for several values in a list:
```js
db.mycoll.find({myfield: {$all: ['val1', 'val2']}})
```

Regex search:
```js
db.mycoll.find({myfield: {$regex: 'myregex'}})
```

Numerical comparison:
```js
db.mycoll.find({myfield: {$gt: 100}})
```

Loop on results:
```js
let mycursor = db.mycoll.find();
while (mycursor.hasNext()) {
   let doc = mycursor.next();
}
```

## findOne

Find the first doc:
```js
db.mycoll.findOne()
```

Find using criteria:
```js
db.mycoll.findOne({$or: [{myfield1: 'myvalue'}, {myfield2: 'myvale'}]})
```

## findOneAndUpdate

Find a doc and update it or create one:
```js
db.mycoll.findOneAndUpdate({myfield:'myvalue'}, {$set: {field1:'value1', field1:'value2'}}, {upsert: true});
```

## Datatypes

See  [MongoDB - datatypes](https://www.tutorialspoint.com/mongodb/mongodb_datatype.htm).

## Transferring files

 * [A primer for GridFS using the MongoDB driver](http://mongodb.github.io/node-mongodb-native/api-articles/nodekoarticle2.html).
 * [GridFS](https://docs.mongodb.com/manual/core/gridfs/).

## Errors

MongoDB 6.0.5 start failure:
    https://www.mongodb.com/docs/manual/release-notes/6.0/#6.0.5---mar-13--2023
    Remove 'fork: true' in /etc/mongod.conf
