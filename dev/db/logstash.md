# Logstash

 * [logstash input plugins](https://www.elastic.co/guide/en/logstash/current/input-plugins.html).

Configuration is in `config/logstash.yml`.
JVM config is in `config/jvm.options`.
log4j config is in `config/log4j2.properties`.

Default pipeline config is `config/pipelines.yml`.
To set a different pipeline, use `-f` option:
```sh
logstash -f my/pipeline/config.yml
```

Console test pipeline (echo on output what is input with keyboard on input):
```sh
logstash -e "input{stdin{}}output{stdout{}}"
```

Test logstash is running:
```sh
curl "http://localhost:9600"
```

Various plugins allow to take inputs from various sources: SQL, log files, CSV files, ...
Plugins written in Java <!-- TODO look for info -->

List available plugins:
```sh
logstash-plugin list
```

It is possible to write output data into files, see `path.queue` configuration parameter.
