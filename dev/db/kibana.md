# Kibana

 * [Kibana Guide](https://www.elastic.co/guide/en/kibana/current/index.html).
   + [Install Kibana](https://www.elastic.co/guide/en/kibana/current/install.html).

Config file `/etc/kibana/kibana.yml`.

Port:
```yml
server.port: 5601
```

Elastic search hosts:
```yml
elasticsearch.hosts: ["http://localhost:9200"]
```

To configure manually the connection between Kibana and Elasticsearch,
get the Kibana token from Elasticsearch:
```sh
sudo /usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token --scope kibana
```
Insert it in Kibana using the browser, or run:
```sh
sudo /usr/share/kibana/bin/kibana-setup --enrollment-token <enrollment-token>
```
Then get the Kibana verification code:
```sh
sudo /usr/share/kibana/bin/kibana-verification-code
```
Insert it too in Kibana.
