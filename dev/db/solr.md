# solr

 * [Solr](https://solr.apache.org/).

Apache search engine. Equivalent to *elasticsearch*.
Uses *lucene* as backend, like *elasticsearch*.
