# Chromium

Run Chromium headless (i.e.: no GUI) and convert HTML to PNG:
```sh
chromium-browser --headless --screenshot=myfile.png --window-size=300,200 myfile.html
```
