# apache2.conf

The name and location of the Apache configuration file varies on platforms
(e.g.: `/etc/httpd/conf/httpd.conf` on ArchLinux, `/etc/apache2/apache2.conf` on
Debian).

On CentOS, disable SELINUX in `/etc/selinux/config`:
```
SELINUX=disabled
```

## Alias

Add an alias (`http://localhost/myalias`) to a path situated outside
`DocumentRoot`:
```apache
Alias /myalias "/Users/someuser/dev/my/project/site"
```
Be careful to use a trailing slash if you want to force the user to set it:
```apache
Alias /myalias/ "/Users/someuser/dev/my/project/site/"
```

For the alias to work, the target directory needs to be declared in a
`Directory` element.
In Apache 2.2:
```apache
<Directory "/Users/someuser/dev/my/project/site">
	Options FollowSymLinks
	AllowOverride all
	Order allow,deny
	Allow from all
</Directory>
```

In Apache 2.4:
```apache
<Directory "/Users/pierrick/dev/exhalobase/site">
	AllowOverride all
	Options Indexes FollowSymLinks
	Require all granted
</Directory>
```
`Options Indexes` allows to browse folders.
Make sure that the path `/Users/someuser/dev/my/project/site` is allowed for
access by everyone, as well as the files contained in the folder.


## ProxyPass and ProxyPassReverse

 * [Reverse Proxy Guide](https://httpd.apache.org/docs/2.4/en/howto/reverse_proxy.html).

## Use password files

```bash
htpasswd /var/www/passwords <user name>
```

Dans `httpd.conf`:
```apache
<Directory /var/www/html/upload>
    require user fichier
</Directory>
```

Put `.htaccess` file in each directory you want to protect with following
content:
```apache
AuthUserFile /var/www/passwords
AuthGroupFile /dev/null
AuthName "Restricted access"
AuthType Basic
<LIMIT GET POST>
Require valid-user
</LIMIT>
```

autres possibilité pour les restrictions et autorisations de la balise LIMIT:
Order Allow, Deny
Require user toto
Require user toto titi tata
Deny from all
Allow from .free.fr

## Upload file size limit

```apache
post_max_size = 100M
upload_max_filesize = 100M
```

## Make Apache parse HTML files for PHP code

Add the following inside `70_mod_php5.conf` or `httpd.conf`:
```apache
AddType application/x-httpd-php .php .html
```
At the following place:
```apache
 <IfModule mod_mime.c>
    AddType application/x-httpd-php .php
    AddType application/x-httpd-php .phtml
    AddType application/x-httpd-php .php3
    AddType application/x-httpd-php .php4
    AddType application/x-httpd-php .php5
    AddType application/x-httpd-php-source .phps
  </IfModule>
```
