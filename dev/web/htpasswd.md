# htpasswd
<!-- vimvars: b:markdown_embedded_syntax={'sh':'bash'} -->

Manage Apache password files.

Create a new password file inside server configuration directory:
```sh
htpasswd -cb /etc/httpd/users myUserName myPassword
```
The configuration directory varies according to platform (e.g.: `/etc/httpd` on ArchLinux, `/etc/apache2` on Debian).

The use of the created password file must be declared inside the Apache configuration file.

Add a user to an existing password file (prompts for password):
```sh
htpasswd /etc/httpd/users myUserName
```

Add a user with password provided on command line to an existing password file:
```sh
htpasswd -b /etc/httpd/users myUserName myPassword
```

Create a new password file:
```sh
htpasswd -c /etc/httpd/myNewPasswordFile myFirstUser
```
