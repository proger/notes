# apachectl

To list enabled modules:
```sh
apachectl -M
```

To check syntax of configuration files:
```sh
apachectl -t
```

