# Tomcat

 * [SSL/TLS Configuration How-To](https://tomcat.apache.org/tomcat-9.0-doc/ssl-howto.html).

Install:
```sh
yum install tomcat tomcat-admin-webapps # RedHat
```

Manage web apps:
```sh
curl -u admin:admin http://localhost:8080/manager/text/reload?path=/geckk-api # Restart
curl -u admin:admin http://localhost:8080/manager/text/start?path=/geckk-api # Start
curl -u admin:admin http://localhost:8080/manager/text/stop?path=/geckk-api # Stop
curl -u admin:admin http://localhost:8080/manager/text/list # List
```

Configure user in `/etc/tomcat/tomcat-users.xml`.

`.war` files are deployed inside `/var/lib/tomcat/webapps/`.
