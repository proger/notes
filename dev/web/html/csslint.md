# csslint

Install in a local `npm` folder:
```sh
npm install -g --prefix npm csslint
```

Usage:
```sh
npm/bin/csslint mystyle.css
```
