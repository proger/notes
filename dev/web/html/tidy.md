# tidy

Install:
```sh
apt install -y tidy
```

Clean and reformat an HTML file:
```sh
tidy myfile.html >out.html
```
