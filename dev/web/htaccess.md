# .htaccess

 * [Apache HTTP Server Tutorial: .htaccess files](https://httpd.apache.org/docs/2.4/howto/htaccess.html).
 * [Apache Core Features](https://httpd.apache.org/docs/2.4/mod/core.html).
 * [Expressions in Apache HTTP Server](https://httpd.apache.org/docs/2.4/expr.html).

Set the following line inside Apache config file to allow .htaccess in a directory and all its sub-folders:
```
AllowOverride All
```

 * [.htaccess](https://fr.wikipedia.org/wiki/.htaccess).
 * [How can I redirect and rewrite my URLs with an .htaccess file?](https://help.dreamhost.com/hc/en-us/articles/215747748-How-can-I-redirect-and-rewrite-my-URLs-with-an-htaccess-file-).
 * [How to deny access to a file in .htaccess](https://stackoverflow.com/questions/11728976/how-to-deny-access-to-a-file-in-htaccess).
 * [RewriteRule Flags](https://httpd.apache.org/docs/2.4/rewrite/flags.html).
 * [Expressions in Apache HTTP Server](https://httpd.apache.org/docs/2.4/expr.html). Variables (`HTTP_POST`, `DOCUMENT_ROOT`, ...).

Rewrite rule example:
```
RewriteRule test/(.+)$ $1?cfg=test [QSA,NC,L]
```
 * `QSA`: .
 * `NC`: No Case. Match in case insensitive mode.
 * `L`: Last rule to be processed.

