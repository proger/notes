# firefox

 * [Keyboard shortcuts - Perform common Firefox tasks quickly](https://support.mozilla.org/en-US/kb/keyboard-shortcuts-perform-firefox-tasks-quickly).

| Key shorcut              | Description                      |
| ------------------------ | -------------------------------- |
| CTRL + K                 | Go into search bar.              |
| ALT + UP/DOWN            | Switch search engine.            |
| CTRL + [ / LEFT          | Back.                            |
| CTRL + ] / RIGHT         | Forward.                         |
| ALT + HOME               | Home.                            |
| CTRL + O                 | Open file.                       |
| CTRL + R                 | Reload.                          |
| CTRL + SHIFT + R         | Reload (override cache).         |
| CTRL + TAB               | Previous tab (last used order).  |
| CTRL + SHIFT + TAB       | Next tab (last used order).      |
| CTRL + PAGE UP           | Previous tab (left).             |
| CTRL + PAGE DOWN         | Next tab (right).                |
| ALT + 1 .. 8             | Go to tab 1 .. 8.                |
| ALT + 9                  | Go to last tab.                  |
| CTRL + SHIFT + PAGE UP   | Move current tab to the left.    |
| CTRL + SHIFT + PAGE DOWN | Move current tab to the right.   |
| CTRL + SHIFT + HOME      | Move current tab to the start.   |
| CTRL + SHIFT + END       | Move current tab to the end.     |
| CTRL + M                 | Mute/unmute audio of Tab.        |
| CTRL + W                 | Close Tab.                       |
| CTRL + SHIFT + W         | Close Window.                    |
| CTRL + SHIFT + T         | Undo close Tab.                  |
| CTRL + SHIFT + N         | Undo close Window.               |
