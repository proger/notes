# pdftk

A PDF tool.

Reverse order of pages:
```sh
pdftk my.pdf cat end-1 output my_reversed.pdf
```

Merge a PDF containg odd pages with a PDF containg even pages:
```sh
pdftk A=odd.pdf B=even.pdf shuffle output merged.pdf
```

Remove passwords from a PDF:
```sh
pdf2ps myfile.pdf - | ps2pdf - myfile_without_passwords.pdf
```
