# pdfunite

Concatenate multiple PDF files:
```sh
pdfunite a.pdf b.pdf c.pdf out.pdf
```
