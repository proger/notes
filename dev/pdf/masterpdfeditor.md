# masterpdfeditor

A powerful GUI PDF editor.
Version 4 is free.
 
 * [Télécharger Master PDF Editor 4 (édition gratuite) pour Linux](https://codepre.com/fr/download-master-pdf-editor-4-free-edition-for-linux.html). Debian and RedHat packages.
 * OR <https://code-industry.net/public/master-pdf-editor-4.3.89_qt5.amd64.deb>.

On debian, install the package with:
```sh
dpkg -i master-pdf-editor-4.3.89_qt5.amd64.deb
apt --fix-broken install
```
