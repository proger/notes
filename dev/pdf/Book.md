# Book

Downloaded books are stored in `~/Library/Mobile
Documents/iCloud~com~apple~iBooks/Documents/` for iCloud stored books and in
`~/Library/Containers/com.apple.BKAgentService/Data/Documents/iBooks`
otherwise.

The books are stored as .epub folders.

To find purchases of other family members, go to menu `Store` and then first
choice `Library...`. The main panel should now display the store books.
At the top of the column on the right of the panel, click `Purchases`, then
choose a family member and download the book you want.
