# ebook-convert

Install:
```sh
pacman -S calibre
```

Convert from epub to txt:
```sh
ebook-convert myfile.epub myfile.txt
```
