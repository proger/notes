# pdfcrack

Tool for cracking user password and/or owner password in PDF files.

```bash
pdfcrack myfile.pdf
```
