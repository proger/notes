# qpdf

Remove encryption from a PDF:
```sh
qpdf --password=yourpass --decrypt input.pdf output.pdf
```
