# pdfimages

From package xpdf or poppler.

Extract images from PDF:
```bash
mkdir myfolder
pdfimages -j myfile.pdf myfolder/myprefix
```
