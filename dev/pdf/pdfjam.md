# pdfjam

Extract pages 17 and 18 and put them on the same sheet together side by side
in landscape orientation:
```sh
pdfjam myfile.pdf 17-18 --nup 2x1 --landscape
```
Output is written to a new file.
