# Compiling
<!-- vimvars: b:markdown_embedded_syntax={'c':'','sh':''} -->

 * [An In-Depth Look into the Win32 Portable Executable File Format - Part 1](http://www.delphibasics.info/home/delphibasicsarticles/anin-depthlookintothewin32portableexecutablefileformat-part1).

## Intel

 * [Intel® Intrinsics Guide](https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html).
 * [Intel® C++ Compiler Classic Developer Guide and Reference](https://www.intel.com/content/www/us/en/docs/cpp-compiler/developer-guide-reference/2021-8/overview.html).
   + [Intrinsics](https://www.intel.com/content/www/us/en/docs/cpp-compiler/developer-guide-reference/2021-8/intrinsics.html).
     - [Details about Intrinsics (Registers & Data Types)](https://www.intel.com/content/www/us/en/docs/cpp-compiler/developer-guide-reference/2021-8/details-about-intrinsics.html).

## gcc

 * [GCC SSE code optimization](https://stackoverflow.com/questions/7919304/gcc-sse-code-optimization).
 * [x86 Built-in Functions](https://gcc.gnu.org/onlinedocs/gcc/x86-Built-in-Functions.html).

Test CPU features:
```c
if (__builtin_cpu_supports("sse2"))
    ...
```

Print default include dirs:
```sh
echo | gcc -xc -E -Wp,-v -
echo | gcc -xc++ -E -Wp,-v -
```

Using SSE instructions, we can write a matrix sum example that uses vectorization:
```c
#include <xmmintrin.h>

mm_c = (__m128*)c;
mm_b = (__m128*)b;
mm_a = (__m128*)a;
for (int i = 0 ; i < N / 4 ; ++i) {
	*mm_c = _mm_add_ps(*mm_a, *mm_b);
	mm_c++;
	mm_a++;
	mm_b++;
}
```

## MSVC

 * [`__cpuid`, `__cpuidex`](https://learn.microsoft.com/en-us/cpp/intrinsics/cpuid-cpuidex?view=msvc-170): to get the CPU capabilities.

## cpuarch

Get CPU architecture compatibility:
```sh
cpuarch -l
```

## Python

 * [Codon, un compilateur Python avec une performance qui serait équivalente à celles de C ou de C++](https://hpc.developpez.com/actu/342464/Codon-un-compilateur-Python-avec-une-performance-qui-serait-equivalente-a-celles-de-C-ou-de-Cplusplus-et-parfois-superieure-selon-ses-concepteurs/).
 * [Codon: A Compiler for High-Performance Pythonic Applications and DSLs](https://regmedia.co.uk/2023/03/11/mit_codon_paper.pdf).

## C++ 11

To enable C++ 11 in clang compiler:
```bash
clang -std=c++11 ...
```

To enable some special C++ extensions, not in the standard (like compact declaration of namespaces in header files: `namespace org::openscience::finnigan`), in MacOS-X clang:
```bash
clang -Wno-c++1z-extensions ...
```

## Dependencies

### MacOS-X

To get a list of dynamic dependencies of a binary file, use `otool`:
```bash
otool -L mybinary
```

The dynamic loader is `dyld`. It can displays loaded libraries at runtime:
```bash
DYLD_PRINT_LIBRARIES=1 myprogram
```
See `man dyld` for a complete list of env vars controling `dyld`.

	PATH
	DYLD_FRAMEWORK_PATH             list of frameworks paths
	DYLD_FALLBACK_FRAMEWORK_PATH    searched if framework is not found in DYLD_FRAMEWORK_PATH
	DYLD_LIBRARY_PATH               list of paths to libraries
	DYLD_FALLBACK_LIBRARY_PATH      searched if library is not found in DYLD_LIBRARY_PATH
	
	Error with dyld
	dyld: Library not loaded: libstlportg.5.2.dylib
	  Referenced from: /Users/eatoni/v3/integrations/windows-demo-abc-english/build/test-api/./test-api
	  Reason: image not found
	--> set DYLD_LIBRARY_PATH env var

Change the install of a library:
```bash
install_name_tool -id "/new/path/to/lib/libtoto.dylib" libtoto.dylib
```

Change path to a linked shared library:
```bash
install_name_tool -change "/old/path/to/some/lib/libtoto.dylib" "/new/path/to/some/lib/libtoto.dylib" mybinary
```

### WINDOWS

#### Dependency Walker (depends.exe)

To explore a DLL and get a list of exported symbols, use `depends.exe`.
Run it from `Start -> Run...` in older version of Visual Studio & Windows.
Or download it from <http://www.dependencywalker.com/>.

#### DUMPBIN

In Visual Studio 2010, use `dumpbin.exe` to get a list of exported symbols.
Start a "Visual Studio Command Prompt" from within Visual Studio 2010 IDE.
Available from MSVC->Tools->"Visual Studio Command Prompt".

Then run dumpbin from command line:
```dosbatch
dumpbin /export <your file>
```
The file can be a DLL or a LIB.

List public symbols:
```dosbatch
dumpbin /LINKERMEMBER mybinary.lib
```

## UNSORTED

```
===================== binaries/ar.txt

# used to build a static library (i.e. an archive of object files)
ar -rcs libMyLib.a $(objects)

# to merge two static libraries into one
ar -x libA.a # extract object files
ar -x libB.a # extract object files
ar -c libC.a *.o # create new library

# -r: insert members into archive
ar -r libMyLib.a $(members)

# -u: only insert members that are newer than existing members of the same name
ar -ru libMyLib.a $(members)

# -s: Write an object-file index into the archive, or update an existing one, even if no other change is made to the archive.
ar s libMyLib.a # equivalent to `ranlib libMyLib.a`

# print object files contained in a library
ar -t libMyLib.a
===================== binaries/ld.txt
# -*- Shell-script -*-

# SEE ALSO: ar

# used to link objects together to make a program or a shared library

# to build a static library, see ar.

# libraries must appear _after_ object files on the command line
# otherwise the linker will not use them to resolve references
ld -o myprog toto.o foo.o -lmylib -lmath
===================== binaries/ldd.txt
# -*- mode:shell-script -*-

# To get the dependencies of a binary
ldd <binary_file>

# env var for places where finding shared libs to load
LD_LIBRARY_PATH=/my/path/to/libs
===================== binaries/libraries.txt
UNIX
====
LD_LIBRARY_PATH: env var to define in which additional folders the system must look for shared libraries.

building a static library: see ar
building a shared library: see ld or gcc
===================== binaries/otool.txt
# vi: ft=sh
# MacOS-X binary inspector.

# Getting library/program dependencies
otool -L myprg

# Getting install name
otool -D mylib
# See install_name_tool.txt for an explanation on how to change this install name.
===================== binaries/readelf.txt
# -*- mode:shell-script -*-

# list symbols of a shared library
readelf -Ws mylib.so
===================== binaries/undname.txt
REM vi: ft=dosbatch

REM Display the human readable display of a declaration (function, variable, ...) from its decorated name.
undname ?status@detail@filesystem@boost@@YA?AVfile_status@23@AEBVpath@23@PEAVerror_code@system@3@@Z
===================== compilers/optimizations.txt
Typical optimizations done by compilers:
_ unrolling of loops
_ inlining
_ vetorization
_ aliasing

************
* ALIASING *
************

Aliasing = plusieurs pointeurs accèdent à la même case mémoire.
Ceci important par exemple dans une boucle, lorsqu'une valeur qui est constante dans la boucle est accédée, mais que le compilateur ne peut pas savoir s'il est accédée/modifiée par un pointeur. Normalement, le compilateur mettrait la valeur dans un registre, mais s'il a un doute sur le fait qu'elle puisse être modifiée pendant la boucle, il ne le fera pas et fera donc des accès répété à la mémoire pour accéder à la valeur.

Il est possible de définir qu'une variable n'est pas modifiée par un accès pointeur (i.e.: non-aliasée):
	_ En C : mot clef restrict --> le compilateur utilisera alors un registre
	_ En CUDA : const & restrict (--> le compilateur utilisera alors l'instruction __ldg() qui chargera l'instruction dans le cache texture).
===================== compilers/C++_standard_library/STLPort.txt
http://www.stlport.org
===================== compilers/C++_standard_library/libc++.txt
A new C++ standard library has been developed by LLVM team: libc++.
It is targeted toward C++ 11.

See http://libcxx.llvm.org
===================== compilers/C++_standard_library/libstdc++.txt
GNU

http://gcc.gnu.org/libstdc++/

GPL2 up to version 4.2. GPL3 license now.
===================== compilers/C++_standard_library/stdcxx.txt
http://stdcxx.apache.org

Stopped in 2008.
===================== compilers/MSVC/CL.txt
REM -*- BAT -*-
REM vi: ft=dosbatch

REM get help
CL /?

REM compile C code
CL /Tc

REM compile C++ code
CL /Tp

REM add macro
CL /Dmymacro

REM don't link
CL /c

REM add debug information compatible with "Edit and Continue"
CL /ZI

REM suppress display of banner
CL /nologo

REM specify output object file
CL /FoB:\OBJECT\ THIS.C

REM ***********
REM * WCHAR_T *
REM ***********

REM For disabling the definition of wchar_t as a built in type, and instead define it as a typedef on unsigned short.
CL /Zc:wchar_t-

REM ============================================================================
REM RUN-TIME LIBRARY

REM Use DLL version of multi-threaded run-time library
REM Defines _MT and _DLL
CL /MD

REM Use debug DLL version of multi-threaded run-time library
REM Defines _DEBUG, _MT and _DLL
CL /MDd

REM Use static version of multi-threaded run-time library
REM Defines _MT
CL /MT

REM Use debug static version of multi-threaded run-time library
REM Defines _DEBUG and _MT
CL /MTd

REM ============================================================================
REM DEBUG

REM add debug information
CL /Zi

REM Enables stack frame run-time error checking
CL /RTCs

REM Reports when a variable is used without having been initialized.
CL /RTCu

REM 1 = u + s
CL /RTC1

REM ============================================================================
REM Exceptions and RTTI

REM exception handling
REM several model of exception handling are available
REM 'sc' tells the compiler that only C++ code throws exceptions. Extern C functions never throw.
CL /EHsc

REM Enable runtime type information (RTTI)
CL /GR

REM ============================================================================
REM WARNINGS AND ERRORS

REM Enable all warnings
CL /Wall

REM Warning level
CL /Wn
REM n = 0 - 4

REM Disable a warning
CL /wd4267

REM Display warnings and errors on a single line
CL /WL
===================== compilers/MSVC/DLL.txt
exporting and importing symbols
===============================
When creating a DLL, each function to export must be preceded by :
__declspec(dllexport)
And each exported global variables must be declared with :
extern __declspec(dllexport)

Then when using the DLL inside another project, each function must be preceded by :
__declspec(dllimport)
and each global variable must be declared with :
extern __declspec(dllimport)

In C++ the same method is used to :
_ export a whole class: class __declspec(dllexport) MyClass
_ export a function of the class.
===================== compilers/MSVC/LINK.txt
REM -*- BAT -*-

REM Linker: link.exe

REM env var for link options.
set LINK=...

REM env var for libraries and objects.
set LIB=...

REM Debug information
LINK /DEBUG

REM suppress display of banner
LINK /NOLOGO

REM set output file
LINK /OUT:<filename>

REM Builds a DLL
LINK /DLL

REM Adds a lib path
LINK /LIBPATH:<dir>

REM Turn off incremental linking
LINK /INCREMENTAL:NO

REM Link with an external library
LINK myobj.obj ... mylibrary.lib

REM ============================================================================
REM ERRORS
REM ============================================================================

REM LIBCMT and MSVCRT conflict
REM LIBCMT is the static version of the runtime library, while MSVCRT is the dynamic version. Both can't be mixed.
REM MSVCMRT : C Runtime static library. Used for mixed managed/native code.

REM unresolved external symbol __imp___CrtDbgReportW
REM Link error triggered when using std::vector.
REM Check that no _DEBUG flag macro is defined inside the macro preprocessor list on compiler command.
===================== compilers/MSVC/PATHs.txt
WINDOWS 7 & .NET MSVC 10.0
==========================
PATH must contain:
C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin      for cl.exe and nmake.exe
C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE for mspdb100.dll which is required by cl.exe
C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin         for mt.exe

INCLUDE must contain:
C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\include
C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Include

VS2010Express doesn't include 64 bits C++ compiler.

LIB must contain:
C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\lib
C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\lib
===================== compilers/MSVC/VC10.txt
PATH for DLLs
=============
Project -> Properties -> Configuration Properties -> VC++ Directories -> Executable Directories
PATH=$(VisualStudioDir)\gnulibs;$(Path)
===================== compilers/MSVC/VS2010.txt
C++ 2010 Express registration key
=================================
pierrick.roger@free.fr / 6VPJ7-H3CXH-HBTPT-X4T74-3YVY7

Files contained in a solution
=============================
MySolution.sln          solution file
MySolution.sdf          intellisense database file
MySolution.suo					binary hidden file containing user preferences

Files contained in a project
============================
MyProject.vcxproj               project file
MyProject.vcxproj.filters       list files contained inside the project
MyProject.vcxproj.user					text hidden file containing user preferences
===================== compilers/MSVC/debugging.txt
To debug an executable not part of a Visual Studio Solution
===========================================================
Open -> Project/Solution -> open exe file

To produce debugging code
=========================
CL /Zi /Fdfile.pdb ...
LINK /DEBUG /PDB:file.pdb ...
===================== compilers/MSVC/mt.txt
The Mt.exe file is a tool that generates signed files and catalogs.

Under Windows 7, it's located inside C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin.
===================== compilers/MSVC/preprocessor.txt
/* vi: se ft=C : */

/* To include only once a file */
#pragma once

/* compiler version */
if _MSC_VER >= 1700
#ifdef __MSVC__         /* Compiler macro. */
#if _MSC_VER >= 1600		/* Compiler version. 1600 for Visual Studio 2010 ==> version 16.00 */
#ifdef _WIN32           /* Defined for Win32 and Win64 applications. Always defined. */
#ifdef _WIN64           /* Defined for Win64 applications. */

/* disabling a warning */
#pragma warning(disable: 4761 3216)
===================== compilers/MSVC/vcvars.txt
variables d'environnement nécessaire à l'exécution en ligne enregistrée dans VCVARS32.BAT, situé dans le répertoire BIN.
===================== compilers/gcc/gcc.txt
# -*- mode:shell-script -*-
# vi: ft=sh

################
# OPTIMIZATION #
################

gcc -O0 # not optimizations
gcc -O1
gcc -O2
gcc -O3

gcc -funroll-loops

################################################################################
# INSTALL

# First, install: GMP, MPFR and MPC.

################################################################################
# INCLUDES

# includes
CPATH env var has precedence over -I

# system includes
# The 3 following env vars have, for each respective language, precedence over -isystem
C_INCLUDE_PATH
CPLUS_INCLUDE_PATH
OBJC_INCLUDE_PATH

# to know which folders are searched for includes
`gcc -print-prog-name=cc1` -v
`gcc -print-prog-name=cc1plus` -v

################################################################################
# Creating a shared library
gcc -fPIC -c mycode.c
gcc -shared -o libmycode.so mycode.o

################################################################################
# LIBRARIES
LIBRARY_PATH env var has precedence over -L

# linking with libraries
# libraries must appear _after_ object or source files on the command line
# otherwise the linker will not use them to resolve references
gcc -o myprog toto.o foo.o -lmylib -lmath

################################################################################
# CYGWIN

# __GNUC__ -- all gnu Compiler versions (egcs = 2)
# __GNUC_MINOR__ -- all gnu Compiler versions (egcs = 91)
# __CYGWIN__ -- CYGWIN
# __CYGWIN32__ -- old Cygwin (pre b20, now deprecated)

# __GNUC_MINOR__ >= 90 implies EGCS as opposed to FSF GCC-2.x.x. When EGCS
# becomes GCC 3, then __GNUC__ will become 3 and __GNUC_MINOR__ will reset.

################################################################################
# OBJECTIVE-C

# linking Objective-C files
gcc -o my_executable <my_object_files> -l objc

# linking with a framework
gcc -framework Foundation ...

# using static libraries
gcc -static ... # links against static libraries rather that against shared libraries

# -Wno-import and #import
# The -Wno-import switch tells the compiler not to give a warning if you have a #import statement in your code. For some reason, Richard Stallman (the GNU Project founder) doesn't like the #import construction.
===================== compilers/gcc/preprocessor.txt

# to know which macros are defined
touch a.cpp
g++ -v a.cpp
--> does not show everything. In particular under Mac OS, does not show __APPLE_CC__ macro.
v
# OR
gcc -E -dM empty_file.c
# -E		Stop after the preprocessing stage;
# -dM		Dump.

# system & version macros
under MacOS-X: __APPLE_CC__
under Windows: __WINT, __WINNT__, _WIN32, __WIN32, __WIN32__

# general (to know we are running gcc)
__GNUC__
===================== compilers/llvm/LLVM.txt
http://llvm.org/

LLVM is an infrastructure/framework for building compilers.

LLVM consists today of three compilers: C, C++ and Objective C.

Xcode4 uses LLVM technology.

The core of the LLVM is built arround the LLVM IR (LLVM intermediate representation) which is a well specified code representation.

LLVM team designed a new C++ standard library. See ../C++_standard_library/libc++.txt

CODING STANDARDS
================
http://llvm.org/docs/CodingStandards.html
===================== compilers/llvm/clang.txt
# vi: ft=sh

# Enable exceptions
clang++ -fexceptions ...

############
# LIBCLANG #
############

# clang is distributed with a library (libclang) that allows parsing of C++ code using an AST visitor.

# CLANG API
# =========
# http://clang.llvm.org/doxygen/index.html

# clang offers two APIs:
# 	_ one in C: clang-c/Index.h
# 	_ one in C++: clang/*
# 
# CLANG C++ PARSING EXAMPLE USING CLANG C LIB
# ===========================================
# See https://github.com/sabottenda/libclang-sample.
git clone https://github.com/sabottenda/libclang-sample
```
