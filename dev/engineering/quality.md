# Quality, testing and integration
<!-- vimvars: b:markdown_embedded_syntax={'yaml':'','sh':'','python':'','r':''} -->

## CI (Continuous integration)

 * [Continuous Delivery - Patterns and Anti-Patterns in the Software Lifecycle](https://dzone.com/refcardz/continuous-delivery-patterns).

### Travis-CI

#### R language

Caching R packages:
```yaml
language: r
cache: packages
```

Setting R version:
```yaml
language: r
r: release
```

#### Branches

Restrict to branch "master":
```yaml
branches:
  only:
    - master
```

#### Using Docker

 * [Using Docker in Builds](https://docs.travis-ci.com/user/docker/).

```yaml
sudo: required

services:
  - docker
```

#### CRON jobs

 * [Cron Jobs](https://docs.travis-ci.com/user/cron-jobs/).

When a build is triggered by `cron`, the environment variable `TRAVIS_EVENT_TYPE` is set to `"cron"`.

#### Running locally

 * [travis-ci/travis-build](https://github.com/travis-ci/travis-build).

Running Travis-CI tests locally can be done through Docker. For instance for a Travis-CI test configured with Python language:
```bash
docker run -it quay.io/travisci/travis-python /bin/bash
su - travis
```
Then clone your repository, and run the commands of your `.travis.yml` file manually.

### GitLab-CI

File is `.gitlab-ci.yml`.

To enable Git submodule recursive extraction:
```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
```

### Jenkins

 * [Remote access API](https://wiki.jenkins-ci.org/display/JENKINS/Remote+access+API).

For instance for PhenoMeNal project:
```bash
wget -O phn-tools.xml http://phenomenal-h2020.eu/jenkins/api/xml?depth=1&tree=jobs
```

Some graphical or monitor views, provided by plugins:

 * [Global Build Stats Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Global+Build+Stats+Plugin).
 * [Dashboard View](https://wiki.jenkins-ci.org/display/JENKINS/Dashboard+View).
 * [Build Monitor Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Build+Monitor+Plugin).

## Coding standards & coding styles

 * [GNU coding standards](https://www.gnu.org/prep/standards/).

 * [EditorConfig](https://editorconfig.org/). `.editorconfig` file defining EOL and indenting styles for languages. Supported by numerous editors: VisualStudio, IntelliJIDEA, PyCharm, Neovim, ...

## Software development processes

### Waterfall

The [Waterfall model](https://en.wikipedia.org/?title=Waterfall_model) in which all phases are completed in order: requirements, design, implementation, etc.

### Agile

The [Agile method](https://en.wikipedia.org/wiki/Agile_software_development) is in fact a group of software development methods.
It promotes adaptive planning, evolutionary development, early delivery, continuous improvement, and encourages rapid and flexible response to change.

Example of Agile methods:

 * Extreme programming.
 * Scrum.

## Static code analysis

 * [List of tools for static code analysis](https://en.wikipedia.org/wiki/List_of_tools_for_static_code_analysis).
 * [Cyclomatic complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity).

### Shell

#### shellcheck

 * [shellcheck](https://github.com/koalaman/shellcheck).

Static code checking of bash scripts.

Install:
```sh
# On RedHat:
yum -y install epel-release
yum install ShellCheck
# On Debian:
apt-get install -y shellcheck
```

Check a script program:
```sh
shellcheck myprg
```

Check a bash library file:
```sh
shellcheck -s bash myprg
```

Ignore warning:
```sh
# Ignore for the whole file:
# shellcheck disable=SC2034

# Ignore for the next function:
# shellcheck disable=SC2317
function lg_enable_verbose {
    # Enables verbose mode
    lg_unset_quiet
    lg_set_debug_level 1
    return 0
}

# Ignore for next line:
# shellcheck disable=SC2153
echo -e "$(st_join "\t" "${fields[@]}")" >"$OUTPUT_FILE"
```
Put:
 * at top of file to ignore for the whole file
 * at top of function to ignore for this function.
 * otherwise ignore for the next line.

### C

 * [Frama-C](https://frama-c.com/).

### C++

 * cppcheck.
 * Klockwork.
 * Code Analysis.
 * `clang --analyze`.
 * Coverity.
 * AQtime.

#### cppcheck

 * [Manual](https://cppcheck.sourceforge.io/manual.pdf).

Set highest checking level (from version 2.14):
```sh
cppcheck --check-level=exhaustive ...
```

Static analysis of code:
```sh
cppcheck --error-exitcode=100 --enable=all myfile.cpp
```
Do not forget to use `--error-exitcode=...` in order for `cppcheck` to return a status code > 0.

```sh
cppcheck --enable=all --inline-suppr --std=c++17 --error-exitcode=99 --suppress=missingIncludeSystem src/sftp_transfer.cpp
```
`--enable=all`: enable all checks
`--error-exitcode=99`: leave with status code != 0 if there is at least one warning
`--inline-suppr`: enable inline check suppression:
```cpp
// cppcheck-suppress variableScope
// cppcheck-suppress anotherWarning
std::string verb;

std::string verb; // cppcheck-suppress variableScope
std::string verb; // cppcheck-suppress[variableScope]
std::string verb; // cppcheck-suppress[variableScope,anotherWarning]
```

### Python

`mypy`, `bandit`, `flake8` and `pylint` are complementary tools.

#### mypy

 * [Running mypy and managing imports](https://mypy.readthedocs.io/en/stable/running_mypy.html).
 * [typing — Support for type hints](https://docs.python.org/3/library/typing.html).
 * [Typing (numpy.typing)](https://numpy.org/doc/stable/reference/typing.html#module-numpy.typing).
 * [Type annotations for \*args and \*kwargs](https://stackoverflow.com/questions/37031928/type-annotations-for-args-and-kwargs).
 * `__getitem__()`:
   + [Function/method overloading](https://peps.python.org/pep-0484/#function-method-overloading).
   + [Check that overloaded functions have an implementation [no-overload-impl]](https://mypy.readthedocs.io/en/stable/error_code_list.html#code-no-overload-impl).
   + See `../examples/python_typehints/getitem.py`.

```sh
python -m mypy --strict --pretty --show-error-code-links --show-error-context --show-column-numbers my_dir
```

If mypy issues `found module but no type hints or library stubs` error:
```
python -m mypy --install-types
```

Ignore an error code in Python code:
```sh
import schema # type: ignore[import-untyped]
```

Custom package makes mypy issue an error:
```
src/ccctrans/main.py:5:1: error: Skipping analyzing "parsftp": module is installed, but missing library stubs or py.typed marker  [import-untyped]
    import parsftp
    ^
```
Solution:
 * Create an empty marker file `py.typed` inside the same folder as `__init__.py` in the custom package.
 * Add the following lines in the `pyproject.toml` of the custom project:
```toml
[tool.setuptools.package-data]
"mypackagename" = ["py.typed"]

[tool.setuptools.packages.find]
where = ["src"]
```

```
src/vplayer/Game.py|2 col 1 error| Skipping analyzing "climage": module is
|| installed, but missing library stubs or py.typed marker  [import-untyped]
||     import climage
||     ^
```

Examples:
```python
a: tuple[int, str, int] = (1, 'a', 2) # Each element of the tuple must have a type defined
b: list[int] = [1, 2, 3] # Typed lists can have only one type of arguments.
c: dict[str, int | str] # Values of dictionaries can have mixed types.
```

Type alias (alias and real type are equivalent), new in Python 3.12:
```python
type MyType = dict[tuple[str, int], list[str]]
```
Or in old versions of Python:
```python
import typing

MyType: typing.TypeAlias = list[str]
```

New type (the new type is considered a sub-type of the original type, and thus
is treating as different):
```python
import typing

MyNewType = typing.NewType('MyNewType', str)
```

Union:
```python
def foo(x: str | int) -> None
    pass
```
OR
```python
import typing

def foo(x: typing.Union[str, int]) -> None
    pass
```

Optional (accepts the defined type or `None`):
```python
import typing

def foo(x: typing.Optional[str] = None) -> None
    pass
```

Returning always the same literal value:
```python
import typing

def foo() -> typing.Literal[True]:
    return True
```

Casting:
```python
y = typing.cast("typing.Sequence[int]", x)
```
The function `typing.cast` does nothing at runtime (it returns its value,
as-is), but is used for type checking.

Abstract types:
```python
import typing

typing.Any
typing.AnyStr   # str or bytes
typing.LiteralString   # accept only literal strings (i.e.: refuses f"" strings) --> to protect from language injection attack
typing.Dict  # For dict
typing.Mapping  # For a read-only dict, etc
typing.MutableMapping  # For a modifiable dict, etc
typing.Sequence # For list, ...
typing.Iterable # For list, ...
```

Class variable:
```python
import typing

class A:
    my_class_variable: typing.ClassVar[int] = 0 # Variable is part of the class, not the instances.
```

Final variables (i.e.: constants):
```python
import typing

MY_CONST: typing.Final = 100
```

Callables:
```python
import typing

typing.Callable
typing.Awaitable
typing.Protocol
typing.Concatenate
```

The `Self` type:
```python
import typing

class A:
    def __iter__(self) -> typing.Self
        return self
```

Indicate a function that never returns (e.g.: because it always raises a exception)
```python
import typing

def foo() -> typing.Never:
    raise RuntimeError("Stop!")
```
`typing.NoReturn` is identical to `Never`, for versions of Python prior to
3.11.

Generic function, using *type parameter syntax*:
```python
def first[T](x: typing.Sequence[T]) -> T:
    return x[0]
```

Generic function, using `TypeVar`:
```python
T = TypeVar('T')
def first(x: typing.Sequence[T]) -> T:
    return x[0]
```

Generic class:
```python
class MyClass[T]:
    def __init__(self, v: T) -> None:
        pass

    def foo(self, x: T) -> T:
        pass
```
Or in Python 3.11 and lower:
```python
import typing

T = typing.TypeVar('T')
class MyClass(typing.Generic[T]):
```

#### ruff

Linter and formatter.

 * [ruff](https://docs.astral.sh/ruff/).
 * To run automatic formatting before committing, see:
   + [pre-commit](https://pre-commit.com/). Package for managing Git hooks.
   + [ruff-pre-commit](https://github.com/astral-sh/ruff-pre-commit).

Create a `ruff.toml` file to setup options:
```toml
[lint]
select = ["ALL"]
ignore = ["D203", "D212"]
```
Or setup configuration inside the `pyproject.toml` file:
```toml
[tool.ruff.lint]
select = ["ALL"]
ignore = ["D203", "D212"]
```

In *VSCode*, install the *Ruff* extension. It uses the `ruff.toml` file.

Ignore rules in whole file:
```python
# ruff: noqa: D100,S311
```

Ignore rule on one line:
```python
# Key press
if event.type == pygame.KEYDOWN: # noqa: SIM102
```

#### flake8

 * [Flake8 Rules](https://www.flake8rules.com/).

A Python code checker. Mainly useless after `pylint`, only signal minor or irrelevant white space issues.

See also:
 * [Black](https://black.readthedocs.io/en/stable/index.html).
 * [isort](https://pypi.org/project/isort/).

Running:
```sh
python -m flake8 --max-line-length 80 src/ tests/
```

Disable a warning:
```python
from .genvcf import vcfgen_cli  # noqa: F401
```

#### pylint

 * [Pylint](https://pylint.readthedocs.io/en/stable/).

Static code checking:
```sh
pylint myfile.py
```

Disable warnings inside code:
```python
# pylint: disable=too-many-positional-arguments,missing-module-docstring,unknown-option-value

import random # pylint: disable=wrong-import-order

# pylint: disable-next=too-many-arguments
def foo(...):

def foo(...):
    # pylint: disable=...
```

#### bandit

 * [bandit](https://github.com/PyCQA/bandit).

Search for common security issues.

### R

 * [The tidyverse style guide](https://style.tidyverse.org/index.html).
 * [lintr](https://cran.r-project.org/web/packages/lintr/vignettes/lintr.html).
 * [styler](https://github.com/r-lib/styler).

Disable a warning:
```r
foo <- function() { # nolint: cyclocomp_linter
```

### Ansible

```sh
ansible-lint -s myfolder
```

Default configuration file is `.ansible-lint` inside current working directory.

See `ansible-lint -h` for options.

## Parameters checking

### R chk

 * [chk](https://github.com/poissonconsulting/chk).

Assertions for testing function arguments.

Check that a variable is a valid string (character vector of length one, not
`NA`):
```r
chk::chk_string(myvar)
```

Check length of object (list or vector):
```r
chk::chk_length(x, 8)
```

Check that a variable points to a valid directory:
```r
chk::chk_dir(myvar)
```

Check that a variable is a logical flag (length one, logical, either `TRUE` or
`FALSE`, but not `NA`):
```r
chk::chk_flag(myvar)
```

Single-number (vector of size 1) integer or numeric:
```r
chk::chk_number(x)
```

Single-number (vector of size 1) integer or numeric which is an integer:
```r
chk::chk_whole_number(n)
```

Check that a variable is either NULL of a valid string:
```r
chk::chk_null_or(myvar, vld=chk::vld_string)
```

Check that a variable is a string:
```r
chk::chk_list(entries)
```

Check that a list contains only NULL elements or object inheriting from a class:
```r
chk::chk_all(entries, chk::chk_null_or, chk::chk_is, 'MyClass')
```

Check that a list contains a subset:
```r
chk::chk_subset(myvalues, mylist)
```

Check that a string matches a regex:
```r
chk::chk_match(cache.id, '^.+-[0-9a-f]{16,}$')
```

## Deprecation

### R lifecycle

 * [lifecycle](https://cran.r-project.org/web/packages/lifecycle/index.html).

Flexible handling of deprecated methods.

Check deprecation inside tests:
```r
lifecycle::expect_deprecated(x <- cache$getFolder(sub.folder = 'foo'))
```

## Test coverage

 * [Cobertura](http://cobertura.github.io/cobertura/).
 * [Codecov](https://about.codecov.io/).
 * [CLI from Codecov: Coverage insights in your terminal](https://about.codecov.io/blog/new-cli-from-codecov-new-features-and-coverage-in-your-terminal/?mkt_tok=MzMyLUxWWC03NDEAAAGMrHGTQEAMy59iE6fftKuAvQjNlj9lmeIQpfMTfbbuINXhS0nOAv5x64I2jimWfpDWpPfujJ5OydTJQmmH9NDMzCxWl-igEDiE3wdF5MEKPQ).

### R

 * [covr](https://cran.r-project.org/web/packages/covr/index.html).

### Python

#### coverage

 * [coverage](https://coverage.readthedocs.io/).

Run tests and measure coverage.

Install:
```sh
python -m pip install coverage
```

Run:
```sh
python -m coverage run -m pytest
python -m coverage report -m    # List missing lines (i.e.: not executed).
```

To get other formats:
```sh
python -m coverage html
python -m coverage xml # Cobertura, see pycobertura
```

#### pycobertura

Display and convert coberture XML reports.

Convert a cobertura XML file into HTML:
```sh
pycobertura show -f html -o cobertura.html cobertura.xml
```

### lcov

Front-end for gcov.

### gcov (C/C++)

 * [Introduction to gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov-Intro.html).

Test coverage with gcc.

Line-by-line profiling with gprof.

### bashcov

 * [Bashcov](https://github.com/infertux/bashcov).

### kcov (bash, Python, compiled programs)

 * [kcov](https://github.com/SimonKagstrom/kcov).

Install on Ubuntu:
```sh
apt install kcov
```

Run test coverage on bash script:
```sh
kcov --exclude-path=$(pwd)/bash-lib $(pwd)/kcov bash-lib/testthat tests
```
