# Versioning

 * [Semantic Versioning 2.0.0-rc.2](https://semver.org/). How to handle version numbers.

## Cloud

 * [Codebase](http://www.codebasehq.com/packages). Payant.
 * [Googlecode](http://code.google.com). Pas de possibilité de repos privé.
 * [Unfuddle](https://unfuddle.com). Payant.

### GitHub

 * [Permission levels for an organization](https://help.github.com/articles/permission-levels-for-an-organization/).
 * [What happens to forks when a repository is deleted or changes visibility?](https://help.github.com/articles/what-happens-to-forks-when-a-repository-is-deleted-or-changes-visibility/).

 * [GitHub markup](https://github.com/github/markup).
 
 * [GitHub pages](https://pages.github.com): website for an account (personal or organization), or for a project.
 * [About GitHub Wikis](https://help.github.com/articles/about-github-wikis/): wiki page for one repository.
 * [Mastering Wikis](https://guides.github.com/features/wikis/).
 * [How To Use GitHub Wikis For Collaborative Documentation](https://nerds.inn.org/2014/05/19/applying-git-to-github-wikis/).



## GitHub

 * [Closing Issues via Pull Requests](https://github.com/blog/1506-closing-issues-via-pull-requests).

Use GitHub API to get size and other metadata about a repos:
```
https://api.github.com/repos/pkrog/biodb-cache
```
Size is expressed in KB.

## GitLab

 * [How To Set Up Continuous Integration Pipelines with GitLab CI on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-continuous-integration-pipelines-with-gitlab-ci-on-ubuntu-16-04).

GitLab CI:
 * A runner must be attached to a project. For that jobs must be tagged with the same tag as the runner inside the YAML file.
 * Check first the tags of the runners inside Project / Settings / CI/CD page.
 * Then edit your `.gitlab-ci.yml` file and inside each job section, add a `tags` section. Example:
```yaml
install_dependencies:
  stage: build
  script:
    - npm install
  tags:
    - bash

test_with_lab:
  stage: test
  script: npm test
  tags:
    - bash
```
