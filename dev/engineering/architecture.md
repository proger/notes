SOFTWARE ARCHITECTURE
=====================

 * [OO Design Principles & Metrics](http://pagesperso.lina.univ-nantes.fr/~molli-p/pmwiki/uploads/Main/oometrics.pdf).
 * [OO Design Quality Metrics An Analysis of Dependencies](https://linux.ime.usp.br/~joaomm/mac499/arquivos/referencias/oodmetrics.pdf).
 * [Software metrics for Java and C++ practices Référencement des métriques utiles à la caractérisation des pratiques sensibles pour Java et C++](http://rmod.inria.fr/archives/reports/Balm09a-Squale-deliverable11-Metrics.pdf).

Books:
 * [Software Architecture in Practice (3rd Edition) (SEI Series in Software Engineering)](https://www.amazon.com/Software-Architecture-Practice-3rd-Engineering/dp/0321815734).
 * [Building Reliable Component-Based Software Systems](https://www.amazon.com/Building-Reliable-Component-Based-Software-Systems/dp/1580533272/ref=sr_1_1?s=books&ie=UTF8&qid=1472636054&sr=1-1&keywords=building+reliable+component-based+software+systems).
 * [Refactoring: Improving the Design of Existing Code](https://www.amazon.com/Refactoring-Improving-Design-Existing-Code/dp/0201485672/ref=sr_1_1?s=books&ie=UTF8&qid=1472635620&sr=1-1&keywords=refactoring).
 * Generative Programming, Methods, Tools and Applications, Krzysztof Czarnecki & Ulrich W. Eisenecker, 0201309777
 * Design Patterns, Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides, 0201633612
 * Object-Oriented Modeling And Design, James Rumbaugh, Michael Blaha, 0136300545
 * The Unified Modeling Language Reference Manual, James Rumbaugh, Ivar Jacobson, Grady Booch, 020130998X

## UML

Docs:
 * [Unified Modeling Language: Infrastructure](https://www.omg.org/spec/UML/2.0/Infrastructure/PDF).
 * [Learning UML 2.0, O'Reilly](http://jti.polinema.ac.id/wp-content/uploads/2019/02/Buku-Learning-UML-2.0.pdf).
 * [UML 2.0, cours IUT](http://alienfamily.free.fr/uml/Cours-UML.pdf).

Software:
 * [PlantUML](https://plantuml.com/).
 * [Dia](https://wiki.gnome.org/Apps/Dia/).
 * [Umbrello](https://uml.sourceforge.io/).
 * [StarUML](https://staruml.io/).
 * [UMLet](https://www.umlet.com/).
 * [Violet UML Editor](http://alexdp.free.fr/violetumleditor/page.php).

## Design patterns

 * [Design Principles and Design Patterns](http://mil-oss.org/resources/objectmentor_design-principles-and-design-patterns.pdf).

### Creational patterns

#### Abstract Factory
An abstract factory is a class which centralizes the instanciation of related or dependent objects (hierarchy of objects).
Each derived factory will instanciate a different kind of objects (in a different hierarchy tree).

See `examples/c++/patterns/abstract_factory.cpp`.
See `examples/java/patterns/AbstractFactory.java`.

#### Builder

A builder pattern consists in 2 classes. One director which directs the construction of an object, using an instance of a builder interface.

And a builder class which is reponsible for the details of the construction.

So the director instance doesn't know which builder instance is going to create the object.

The builder interface can present the construction in several steps (using several functions), and the director chooses which functions to call. So the high level logic of the creation process is captured inside the director class and the builder interface.

See `examples/c++/patterns/builder.cpp`.

#### Factory Method

A creator abstract mother class defines a virtual function for creating an object.

Subclasses implement this function, deciding which object to create.

See `examples/c++/patterns/factory_method.cpp`.

#### Prototype

A prototype class, defines a function for cloning itself. Any subclass will override this function in order to clone itself.

See `examples/c++/patterns/prototype.cpp`.

#### Singleton

Centralizes and protects access to a single instance of a class.

It ensures that the class will have one and only one instance.

See `examples/c++/patterns/singleton.cpp`.

#### Singleton hierarchy.

In this example, the Singleton class is derived to create a subclass SubSingleton. This subclass registers itself using the function register.
Then the singleton used at runtime is found using a name chosen among the registered singletons.

See `examples/c++/patterns/singleton_hierarchy.cpp`.

### Structural patterns

#### Adapter

##### Class adapter

A class adapter is a class inheriting privately from another class, and implementing a specific interface.

The adapter has the same role as the mother class, except that it presents another interface than its mother class. So it is used to integrate an existing class into an existing framework, which expects a different interface from this class.

See `examples/c++/patterns/class_adapter.cpp`.

##### Object adapter

An object adapter has the same function as a class adapter. However instead of inheriting from the Adaptee class, it declares an instance of the Adaptee as a private member.

See `examples/c++/patterns/object_adapter.cpp`.

#### Bridge

In this pattern, a new class is created in order to hide another class, as a private member.

User of the new class will not know which instance of the hidden class is created (possibly any derived class can be used).

Functions are created in this new class, to call the hidden class functions.

The new class can then be derived, and thus gives birth to a new tree of inheritance.

Benefits:

 * Allows to swith of object implementation at run-time.
 * Changes in the implementation doesn't impact clients.
 * In C++, allows to hide your interface code (implementation).

See `examples/c++/patterns/bridge.cpp`.

#### Composite

The composite pattern is used in creation of tree or graph structures.

An interface providing functions to add children to a component is inherited by all components:

 * Leaf component (doesn't accept children).
 * Composite components (made for accepting children).

See `examples/c++/patterns/composite.cpp`.

#### Decorator

A decorator class adds a new behaviour to an existing class, by inheriting from a common interface, while including as a member an instance of the existing class.

The main interest is that the decorator is itself part of the hierarchy of the existing class, and is used at run-time to add a functionality to an object.

When called on a function of the interface, the decorator will run its own code as well as the code of the member instance of the old class.

See `examples/c++/patterns/decorator.cpp`.

#### Facade

A Facade class centralizes other objects creation and access, and factorizes operations run on other objects, by presenting a much simpler interface, hiding all details of the sub-system.

It can be viewed as the API of a system.

See `examples/c++/patterns/facade.cpp`.

#### Flyweight

A Flyweight class is used when one needs to create a huge amount of instances of the same class, but can't do it because of memory/system limitations.

The aim is to create only a small number of these instances and share them between all client classes that use them.

The Flyweight, being shared by different clients which have different needs, can only store general information. Specific information linked to the use of the Flyweight class, is stored by the client itself and passed to the Flyweight class when neeed (i.e.: when calling one of its functions).

A Factory class is responsible for creating and accessing the Flyweight instances, when needed by the clients.

See `examples/c++/patterns/flyweight.cpp`.

#### Proxy

A Proxy class instance controls the access to another class.

In the typical situation, the Proxy class derives from the same mother class as the objects it wants to control access to.

The controled object is stored as a private member.

See `examples/c++/patterns/proxy.cpp`.

#### CRTP

 * [Curiously recurring template pattern (CRTP)](https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern).

```cpp
template<class T> class Base {
	// methods within Base can use template to access members of Derived
};

class Derived : public Base<Derived> {
	// ...
};
```

```java
public class Item implements Comparable<Item> {

	private String name;

	@Override
	public int compareTo(final Item other) {
		return name.compareTo(other.name);
	}
}
```

### Behavioral patterns

#### Chain of Responsibility

In this pattern, the classes inherit a common interface which contains functions for handling requests.

When asking to handle a request an instance will eventually delegate to another instance, making in that the chain of reponsibility.

See `examples/c++/patterns/chain_of_responsibility.cpp`.

#### Command
Encapsulate a request as an object, thereby letting you parameterize clients with different requests, queue or log requests, and support undoable operations.

A Command class hides the logic of a command that is applied on one or more receiver objects.

It permits to factorizes the logic of operations into objects, allowing by that operations like undo, redo, or other complex operations commonly applied on a group of objects.

See `examples/c++/patterns/command.cpp`.

#### Interpreter

The Interpreter pattern is used to evaluate an expression of a language, given its grammar.

See `examples/c++/patterns/interpreter.cpp`.

#### Iterator

The Iterator pattern is used to access sequentially elements of an object.

See `examples/c++/patterns/iterator.cpp`.

#### Mediator

The Mediator class encapsulates objects and their relations, letting each object ignore others.

The relation between the set of objects is then centralized in one place.

A Mediator hierarchy class tree can then be created, letting us choose between different mediators implementing different interactions between the set of objects.

See `examples/c++/patterns/mediator.cpp`.

#### Memento

The Memento pattern is used to implement a backup/restore (externalization) system.

See `examples/c++/patterns/memento.cpp`.

#### Observer

The Observer pattern is used to get callback from objects on a set of events.

See `examples/c++/patterns/observer.cpp`.

#### State

A abstract State class defines a function for changing its state according to a context.

"Changing its state" means that a concrete State object is exchanged with another concrete State object of a different class.

It's the Context class, which has a reference to the concrete State object that, at the end, receive notification for exchanging the State object for another.

See `examples/c++/patterns/state.cpp`.

#### Strategy

Presents algorithms as a hierarchy tree of classes. Making them interchangeable.

See `examples/c++/patterns/strategy.cpp`.

#### Template Method

The Template Method is a method which defines an algorithm but lets sub-classes with the opportunity to add personalised behaviour at some points of the algorithm.

The overall structure of the algorithm is thus preserved, but sub-classes are able to refine it.

See `examples/c++/patterns/template_method.cpp`.

#### Visitor

The Visitor pattern allows an external class (Visitor) to visit each element (executing an operation for each of them) of a complex structure (tree, graph, ...) without knowing anything about its internal representation.

This is a non-intrusive inspection. The structure knows how to scroll through all of its element, and the visitor is only called for each element.

See `examples/c++/patterns/visitor.cpp`.

## Mixing layers

 * [Implementing Layered Designs with Mixin Layers](https://yanniss.github.io/templates.pdf).

## Generative programming

The idea comes from the analogy with industry, and in particular with capacity for interchanging components/parts and produce targeted products, like in:

 * Weapons (rifles).
 * Computers (PC).
 * ...

Enable transition from "handcrafting one-of-a-kind solutions" towards "automated manufacturing of large varieties of software products optimally satisfying customer needs".

## Architectural patterns

 * [Architectural Patterns Revisited – A Pattern Language](http://eprints.cs.univie.ac.at/2698/1/ArchPatterns.pdf).

### Layered view

LAYERS
INDIRECTION LAYERS: hides a sub-system from a top layer

### Data flow view

#### Batch sequential
Processing steps that take data from the previous step, process it completely and give it to the next step.

#### Pipes and filters
There is a constant data flow through the pipes and filters.
Pipes act as data buffers between adjacent filters.
Filters retain no state between invocations.
Pipes and filters can be flexibly composed.
Filter can be executed in parallel.

### Data centered view
 * How is the shared data store created, accessed, and updated?
 * How is data distributed?
 * Is the data store passive or active, i.e. does it notify its accessors or are the accessors responsible of finding data of interest to them?
 * How does the data store communicate with the elements that access it? • Do the accessor elements communicate indirectly through the shared data or also directly with each other?
 * How are the quality attributes of scalability, modifiability, reusability, and integrability supported?

#### Shared repository
Clients share a common source of data.

#### Active repository
Clients need to be immediately informed of specific events in the shared repository, such as changes of data or access of data.

#### Blackboard
The blackboard is a shared repository where clients take information to process and put the result.
Each client perfoms a different task, and is activated by a Controler which inspects the blackboard in order to know which clients have work to do.

### Adaptation view
Made of a static core part and a adaptable part that either changes over time or in different versions of a system.

 * How can a system adapt better to evolution over time or to multiple different versions of a basic architecture?
 * What is the system functionality that is more likely to change and what will possibly remain invariable?
 * How do the invariable parts communicate with the adaptable parts? • How are the quality attributes of modifiability, reusability, evolvability, and integrability supported?

#### Microkernel
Internal Servers are used to implement different version-specific services.
External Servers offer APIs and user interfaces to clients.
External Servers access the MicroKernel, hiding it from clients, and the MicroKernel uses Internal Servers.

Thus it is easy to produce different versions of the product, for different targeted systems.

#### Reflection
Meta-object is used to describe the system. So application logic components can use the meta-object description in order to get information about itself.
It allows easier evolution for unforseen needed features.

#### Interceptor
An *interceptor* is a mechanism for transparently updating the services offered by the framework in response to incoming events.

### Language extension view

The system is viewed as a part that is native to the software/hardware environment and another part that is not.

 * how can a part of the system that is written in a non-native language be integrated with the software system?
 * how can the non-native part be translated into the native environment?
 * how are the quality attribute of portability and modifiability supported?

#### Interpreter
a language syntax and grammar needs to be parsed and interpreted within an application. The language needs to be interpreted at runtime (i.e. using a compiler is not feasible).

#### Virtual machine
an efficient execution environment for a programming language is needed. The architecture should facilitate portability, code optimizations, and native machine code generation. Runtime interpretation of the language is not necessarily required.

#### Rule-based system
It consists mainly of three things: facts, rules, and an engine that acts on them. Rules represent knowledge in form of a condition and associated actions. Facts represent data.
A *rule-based system* applies its rules to the known facts. The actions of a rule might assert new facts, which, in turn, trigger other rules.

### User interaction view

The system is viewed as a part that represents the user interface and a part that contains the application logic, associated with the user interface.

 * what is the data and the application logic that is associated to the user interface?
 * how is the user interface decoupled from the application logic?
 * How are the quality attributes of usability, modifiability, and reusability supported?

#### Model-view-controller
A Model that encapsulates some application data and the logic that manipulates that data, independently of the user interfaces.
One or multiple Views that display a specific portion of the data to the user.
A Controller associated with each View that receives user input and translates it into a request to the Model.

The Model notifies all different user interfaces about updates.

#### Presentation-abstraction-control

The system is decomposed into a tree-like hierarchy of agents: the leaves of the tree are agents that are responsible for specific functionalities, usually assigned to a specific user interface; at the middle layers there are agents that combine the functionalities of related lower-level agents to offer greater services; at the top of the tree, there is only one agent that orchestrates the middle-layer agents to offer the collective functionality. Each agent is comprised of three parts: a Presentation takes care of the user interface; an Abstraction maintains application data and the logic that modifies it; a Control intermediates between the Presentation and the Abstraction and handles all communication with the Controls of other Agents.

The various agents usually need to propagate changes to the rest of the agent hierarchy, and this can be achieved again through the *publish-subscribe* pattern. Usually higher-level agents subscribe to the notifications of lower-level agents.

#### C2
An interactive system is comprised of multiple components such as GUI widgets, conceptual models of those widgets at various levels, data structures, renderers, and of course application logic. The system may need to support several requirements such as: different implementa- tion language of components, different GUI frameworks reused, distribution in a heterogeneous network, concurrent interaction of components without shared address spaces, run-time recon- figuration, multi-user interaction. Yet the system needs to be designed to achieve separation of
concerns and satisfy its performance constraints.

The system is decomposed into a top-to-bottom hierarchy of concurrent components that interact asynchronously by sending messages through explicit connectors. Components submit request messages upwards in the hierarchy, knowing the components above, but they send notification messages downwards in the hierarchy, without knowing the components lying beneath. Components are only connected with connectors, but connectors may be connected to both components and other connectors. The purposes of connectors is to broadcast, route, and filter messages.

### Component interaction view

The system is viewed as a number of independent compo- nents that interact with each other in the context of a system.

 * How do the independent components interact with each other?
 * How are the individual components decoupled from each other?
 * How are the quality attributes of modifiability and integrability supported?

#### Explicit invocation
An *explicit invocation* allows a client to invoke services on a supplier, by coupling them in various respects. The decisions that concern the coupling (e.g. network location of the supplier) are known at design-time. The client provides these design decisions together with the service name and parameters to the *explicit invocation* mechanism, when initiating the invocation. The *explicit invocation* mechanism performs the invocations and delivers the result to the client as soon as it is computed. The *explicit invocation* mechanism may be part of the client and the server or may exist as an independent component.

 * The *fire and forget* pattern describes best effort delivery semantics for asynchronous operations but does not convey results or acknowledgments.
 * The *sync with server* pattern describes invocation semantics for sending an acknowl- edgment back to the client once the operation arrives on the server side, but the pattern does not convey results.
 * The *poll object* pattern describes invocation semantics that allow clients to poll (query) for the results of asynchronous invocations, for instance, in certain intervals.
 * The *result callback* pattern also describes invocation semantics that allow the client to receive results; in contrast to *poll object*, however, it actively notifies the requesting client of asynchronously arriving results rather than waiting for the client to poll for them.

#### Implicit invocation
In the *implicit invocation* pattern the invocation is not performed explicitly from client to supplier, but indirectly and rather randomly through a special mechanism such as *publish-subscribe*, *message queuing*, or broadcast, that decouples clients from suppliers. All ad- ditional requirements for invocation delivery are handled by the *implicit invocation* mech- anism during the delivery of the invocation.

#### Client-server (variant of the *explicit invocation*)
The *client-server* pattern distinguishes two kinds of components: clients and servers. The client requests information or services from a server. To do so it needs to know how to access the server, that is, it requires an ID or an address of the server and of course the server’s interface. The server responds to the requests of the client, and processes each client request on its own. It does not know about the ID or address of the client before the interaction takes place. Clients are optimized for their application task, whereas servers are optimized for serving multiple clients.

Using the *client-server* pattern we can build arbitrarily complex architectures by introducing multiple client-server relationships: a server can act itself as a client to other servers. The result is a so-called *n-tier-architecture*. A prominent example of such architectures is the 3-tier- architecture (see TODO Figure 20), which consists of:

 * A client tier, responsible for the presentation of data, receiving user events, and controlling the user interface.
 * An application logic tier, responsible for implementing the application logic (also known as business logic).
 * A backend tier, responsible for providing backend services, such as data storage in a data base or access to a legacy system.

#### Peer-to-peer (variant of the *explicit invocation*)
In the *peer-to-peer* pattern each component has equal responsibilities, in particular it may act both as a client and as a server. Each component offers its own services (or data) and is able to access the services in other components. The *peer-to-peer* network consists of a dynamic number of components. A *peer-to-peer* component knows how to access the network. Before a component can join a network, it must get an initial reference to this network. This is solved by a bootstrapping mechanism, such as providing public lists of dedicated peers or broadcast messages (using *implicit invocation*) in the network announcing peers.

#### Publish-subscribe
*Publish-subscribe* allows event consumers (subscribers) to register for specific events, and event producers to publish (raise) specific events that reach a specified number of consumers. The *publish-subscribe* mechanism is triggered by the event producers and automatically executes a callback-operation to the event consumers. The mechanism thus takes care of decou- pling producers and consumers by transmitting events between them.

In the local context the *publish-subscribe* can be based on the *observer* pattern.

In the remote context *publish-subscribe* is used in *message queuing* implementations or as a remoting pattern of its own. *Publish-subscribe* makes no assumption about the order of processing events.

### Distribution view

The system is viewed as a number of components that are distributed among network nodes (or different processes).

 * How do the distributed components interact with each other?
 * How are the distributed components decoupled from each other?
 * How are the quality attributes of interoperability, location-transparency, performance, and modifiability supported?

#### Broker
A *broker* separates the communication functionality of a distributed system from its application functionality. The *broker* hides and mediates all communication between the objects or components of a system. A *broker* consists of a client-side *requestor* to construct and forward invocations, as well as a server-side *invoker* that is responsible for invoking the operations of the target remote object.
A MARSHALLER on each side of the communication path handles the transformation of requests and replies from programming language native data types into a byte array that can be sent over the transmission medium.

Additional features:
_ A *client proxy* represents the remote object in the client process. This proxy has the same interface as the remote object it represents.
_ An *interface description* is used to make the remote object’s interface known to the clients.
_ *Lookup* allows clients to discover remote objects.

#### Remote procedure calls
*Remote procedure calls* extend the well-known procedure call abstraction to distributed systems. They aim at letting a remote procedure invocation behave as if it were a local invoca- tion. Programs are allowed to invoke procedures (or operations) in a different process and/or on a remote machine.

#### Message queuing
Messages are not passed from client to server application directly, but through intermediate message queues that store and forward the messages. This has a number of consequences: senders and receivers of messages are decoupled, so they do not need to know each other’s location (perhaps not even the identity). A sender just puts messages into a particular queue and does not necessarily know who consumes the messages. For example, a message might be consumed by more than one receiver. Receivers consume messages by monitoring queues.

## TO CLEAN

	%%%%%%%%%% OS/process
	A process is composed of :
	_ the program (usually on disk) in executable machine language
	_ a block of memory composed of :
		_ executable code
		_ call stack
		_ heap
		_ constants
		_ ...
	_ descriptors of resources (file descriptors, ...)
	_ security information (which hardware and software resources the process can access)
	_ state of the process :
		_ ready to run or waiting for a resource
		_ content of registers
		_ information about process' memory
	%%%%%%%%%% OS/thread
	In most OS, it's faster to switch between threads than to switch between processes.
	This is because threads are "lighter" than processes :
		_ they share the same executable code
		_ they share the same memory
		_ they share the same I/O devices
	Each thread has :
		_ its own program counter and registers
		_ its own call stack
	%%%%%%%%%% refactoring/limitations
	Databases
	---------
	Your object model is linked to the database design.
	Database design is hard or impossible to change, since database instances are used in production.
	For non-objet databases, it's easier to refactor your objet model if you put an intermediate layer between the database and your model. So you can adapt the layer to follow your changes in the objet model.
	For object databases, it exists techniques to (included in database system or that you must code) in order to make new versions of objects compatible with current databases.
	
	Exceptions
	----------
	On a published interface, changing in a non-throwing function into one that throws an exception breaks the interface. Refactoring this way can be a problem.
	Similarly changing the exception a throwing function declares is a problem.
	
	Refactoring code that doesn't work
	----------------------------------
	Refactoring code that is full of bugs or that doesn't pass tests, is a often a waste of time. Better rewrite all from scratch instead.
	%%%%%%%%%% refactoring/pratical cases
	Duplicated Code
	===============
	
	Long Method
	===========
	
	Large Class
	===========
	Split classes when: methods have too much lines of code, or when it contains too much instance variables.
	Look at usage of the class by clients, to identify the different interfaces that can be extracted from it.
	
	Long Parameter List
	===================
	
	Divergent Change
	================
	
	Shotgun Surgery
	===============
	
	Feature Envy
	============
	One method uses lots of data of another class ==> move this method (or part of it) into this other class.
	If it uses data of several classes, try to break it in several methods that go in each class. If this isn't possible then change the behaviour and isolate the code into another class (like in Strategy or Visitor patterns).
	
	Data Clumps
	===========
	
	Primitive Obsession
	===================
	
	Switch Statements
	=================
	
	Parallel Inheritance Hierarchies
	================================
	
	Lazy Class
	==========
	
	Speculative Generality
	======================
	
	Temporary Field
	===============
	
	Message Chains
	==============
	
	Middle Man
	==========
	One class hides/encapsulates another class, but half the methods delegate to this other class.
	Maybe it's best to remove the Middle Man, and access directly the other class.
	If there's additional behavior defined in the Middle Man, we can make it inherits from the other class.
	
	Inappropriate Intimacy
	======================
	Two classes use each other too much.
	Separate the classes.
	Try to make the link unidirectional.
	Introduce new classes for common code of necessary.
	
	Alternative Classes with Differente Interfaces
	==============================================
	
	Incomplete Library Class
	========================
	An external library lacks some features. Unfortunately we can't methods easily in such a library.
	For adding one or two methods, see "Introduce  Foreign Method (162)".
	For adding a lot of stuff, see "Introduce Local Extension (164)".
	
	Data Class
	==========
	
	Refused Bequest
	===============
	
	Comments
	========
	Often (detailed) comments are used to hide bad coding.
	Clean the code first, and the comments should become thiner.
	
	In addition to describing what is going on, use comments to:
	_ explain why you do something ;
	_ write your doubts about a choice or whatever you whish to comment ;
	_ explain why you didn't do something ;
	_ explain why you choose to do something this way and not the usual way or expected way.
	%%%%%%%%%% software/CRC cards
	CRC stands for “Class-Responsibility-Collaborator”. It names a brainstorming technique that works with scenario walkthroughs to stress-test a design.
	
	In a CRC exercise, a card or piece of paper is made to represent an instance of an object type. On each card is written two lists:
	_ one of the reponsibilities (the roles of this class) ;
	_ one of the collaborators (other classes with which this class interacts).
	
	Its responsibility is identified, either by invention or writing it from the object type definition. A use case scenario is begun. Someone talks through the scenario, and one or more people show the objects that work together to deliver the scenario. When one object uses another, the second object is said to be the first object’s collaborator. The names, the responsibilities, and the collaborations summarize the design at a low but accurate level of precision.
	%%%%%%%%%% software/Knowledge base
	A knowledge base is a database of object inside memory, representing the knowledge of a system. It uses classes to represent nodes as well as links between these nodes.
	
	Links can be hiddren from the public interface, so it's easier to alter the model, since there are several ways to link the nodes together.
	%%%%%%%%%% software/Models Models Models (Stephen J Mellor)
	1) models should be executable
					a model which is not executable is like a car without engine
	2) a model is abstract
					--> abstract = the number of things that we are ignoring
	                     we are independent from the underlying technology
	3) adoption ?
	   Who is going to adopt models ?
	   Innovators, Early minority...
	
	He doesn't consider the question "Do models work ?", he just believes they work.
	
	Hurdles
	=======
	Issues that people oppose you when you try to present your solution:
	
	Software engineering ?
					--> yes from the point of view of models 
	I have Java (so I don't have a problem with models)
					--> 
	I don't have a problem
					--> what problems are these people ignoring currently ?
	        --> ask them questions about what they plan to do, what they can't do and made a cross on it, ...
	It's just an example
					--> they argue they want the real thing
	        --> oppose that: it's not just an example but something that works
	You don't solve my problem
					--> it's not their case
	They won't accept it
	 
	Answers given ususally by people in industry:
	Technicians: my manager won't let me do it.
	Managers: technicians won't do it.
	
	
	We have far too long passed too much time on graphics, boxes, ... I don't care if it's a cloud or a box.
	
	One big issue is: no standard. For a long time it has been an issue to interchange models.
	Another big issue was the meaning of graphics (clouds, boxes, ...).
	
	Business
	========
	Pricey
					Compared to other fields, hardware like ships for instance, people think software engineering must cost few. Why ?
	Risky
	        People think Code generation is risky. They fear the code generated will be wrong.
	Inertia
	        "Hard work has a future payoff, laziness pays off NOW !"
	Broken promesses
					Number of fields, tools didn't keep their promesses: AI (Articifial Intelligence), ...
	
	
	All programmers have good low coding skills.
	But not all programmers have high abstraction skills.
	
	There's a difference between building software and building a software that is extensible, understandable by people, ...
	A problem is that many people, using Excel VB, think they can program and make software, or at least understand how software are done or must be done. But software building is something _hard_.
	
	Second biggest problem
	======================
	What is abstraction ?
	Somewhere on the line we lost the way of writing small program that does one thing properly.
	Working on abstraction level allows to solve problems with small effort and to produce small code that is easily understandable: you don't care about low level coding.
	
	Is There hope ?
	===============
	Progress in standards:
	Methods  : before we had 30 models and 30 methods.
	UML      : now 1 model but no method (what does he mean exactly ?)
	Interchange : ...
	
	What lacks: execution and translation.
	
	StephenMellor@StephenMellor.com
	
	%%%%%%%%%% software/architecture
	http://www.handbookofsoftwarearchitecture.com/
	login: email free / perso alphanum
	%%%%%%%%%% software/conceptual modeling
	Content-Type: text/enriched
	Text-Width: 70
	
	From Appendix A of Generative Programming by Czarnecki & Eisenecker
	
	
	The Three Major Views of Concepts
	=================================
	
	
	Classical View
	--------------
	A concept ia defined by a list of <italic>necessary</italic> and <italic>sufficient</italic> properties.
	
	
	This works well for mathematical concepts which are defined precisely.
	It doesn't for natural concepts. Like for "game" :
	_ it can be played by several person or just one ;
	_ there can be a winner or none ;
	_ it can involve pleasure or not (professional sport).
	However it seems misplaced to want to modelize natural concepts, they cover effectively very different concepts, that can individually be defined precisely (at least with the precision we desire for our system).
	
	
	
	Probabilistic View
	------------------
	A concept is defined by a list of properties. To each property is associated a likelihood (a weight ∈ R, between 0 and 1).
	
	
	Exemplar View
	-------------
	A concept is defined by its exemplars.
	New objects are categorized according to their similarity to the stored exemplars.
	A common implementation is with a <italic>neural network</italic>. Each exemplar is stored by adjusting the weights associated with the connections between neurons, using for instance the <italic>back-propagation algorithm</italic>.
	
	
	Important Issues Concerning Concepts
	====================================
	
	
	Stability of Concepts
	---------------------
	knowledge associated with a concept:
	_ varies from one person to another ;
	_ evolves over time ;
	_ depends on the context.
	
	
	Concept Core
	------------
	
	It's constituted by its essential properties (without which the concept would not be what it is).
	
	However, in the case of the probabilistic view, an essential property doesn't need to be shared by all instances of a concept. It only needs to be present in most of the instances.
	
	
	Informational Contents of Features
	
	----------------------------------
	
	We describe concepts by listing features (e.g.: a daisy is inanimate, has a stem and is white).
	
	But features themselves are viewed as concepts, when we try to define them.
	
	So the distinction between concept and feature is relative, and depends on our point of view.
	
	
	Feature Composition and Relationships between Features
	
	------------------------------------------------------
	
	
	<italic>Constraints on feature combinations:</italic> some combinations may lead to a contradiction (e.g.: a matrix can't be non-square and diagonal at the same time).
	
	
	<italic>Translations between features:</italic> certain features imply some other features (e.g.: a diagonal matrix is always square).
	
	
	Quality of Features
	
	-------------------
	
	The following structural qualities tell us how “economical” a set of features is in describing relevant concepts.
	
	
	<italic>Primitiveness:</italic> Features express differences and similarities between concepts. A feature is primitive if it does not have to be decomposed in order to show some relevant differences among concept instances.
	
	
	<italic>Generality: </italic>A feature is more general if it applies to a large number of concepts. A set of featur
	
	es is general if it describes a large number of concepts with a minimal number of features.
	
	
	<italic>Independency: </italic>The fewer constraints on feature combinations that apply to a set of features, the larger number of concepts that can be described by combining the features.
	
	
	Abstraction and Generalization
	
	------------------------------
	
	
	<italic>Generalization:</italic> An existing description pf a set of objects can be further <italic>generalized</italic> by adding new objects to the set and modifying the description to take these new objects into account.
	
	
	<italic>Specialization:</italic> The inverse operation to generalization is <italic>specialization</italic>. Specializing the description of a set of objects involves the reduction of the set to a subset.
	
	
	<italic>Abstraction:</italic> An existing description of a set of objects can be further <italic>abstracted</italic> using a new focus and filtering away all parts of the decription that are not relevant with respect to the new focus.
	
	
	<italic>Concretization:</italic> The inverse operation to abstracion os <italic>concretization</italic> (also called <italic>refinement</italic>). Concretization results in the increase of detail per object.
	
	
	Abstraction and Generalization usually occur together during the process of building a concept.
	
	%%%%%%%%%% software/cross-platform
	--> programmation générative
	%%%%%%%%%% software/embedded
	attention aux processeurs avec différentes caractérisques:
	          _ big/little endian
	          _ 16 bits avec un 'char' sur 16 bits (au lieu de 8 bits).
	%%%%%%%%%% software/good practices
	_ the less code the better
					_ each line of code which isn't written, will _not_ :
									_ give bugs
	                _ block needed evolutions
	                _ constrain architectural choices
	
	_ coding is not difficult, what are difficult are the choices for the future. You must keep in mind that a software needs: 
					_ maintenance in the future
					_ evolutions
					--> and all this has a cost. So architecture has to lower the price of future developpements.
	%%%%%%%%%% software/object oriented languages
	Objective-C
	===========
	Smalltalk style: sends messages. Target is resolved at runtime.
	
	Reflective: can check if a method (identified by a selector (SEL)) is supported by a delegate.
	
	No multiple inheritance. Protocols ("interfaces") are used instead.
	
	Categories are used to structure code writing, by sorting methods of a Class into different files. Being added at runtime, they can also be used to extend existing class from external code.
	
	C++
	===
	Simula-style: call methods. Target is resolved at compile-time (unless virtual).
	
	Multiple inheritance is allowed.
	
	Templates.
	%%%%%%%%%% software/overloading
	Creating several versions of a method, with the same name but different signature (nb parameters and/or different return type).
	%%%%%%%%%% software/overriding
	In OOP:
	Implementing a specific version of a method in a sub-class.
	%%%%%%%%%% software/systems
	Systems thinking can be summarized as follows (Minger and White, 2010): 
	_ Viewing the situation holistically, as opposed to reductionistically, as a set of diverse interacting elements within an environment.
	_ Recognising that the relationships or interactions between elements are more important than the elements themselves in determining the behaviour of the system.
	_ Recognising a hierarchy of levels of systems and the consequent ideas of properties emerging at different levels, and mutual causality both within and between levels.
	_ Accepting, especially in social systems, that people will act in accordance with differing purposes or rationalities.
	
	REF: Mingers, J., White L. 2010. A review of the recent contribution of systems thinking to operational research and management science. European Journal of Operational Research, 207(3), pp. 1147-1161. [Recent developments in main systems theories and methodologies are reviewed in the context of operational research and management science].
	%%%%%%%%%% hardware/Von Neumann & improvements/Von Neumann
	The Control Unit decides which instructions is executed.
	The ALU executes instructions.
	
	             CPU
	-----------------------------
	|     ALU     |   Control   |
	|  registers  |  registers  |
	|     ---     |     ---     |
	|     ---     |     ---     |
	|     ---     |     ---     |
	-----------------------------
	       |             |
	       |             |
	       V             V
	-----------------------------
	|       Interconnect        |
	-----------------------------
	       |             |
	       |             |
	       V             V
	-----------------------------
	|    Address    Contents    |
	|     -----      ------     |
	|     -----      ------     |
	|     -----      ------     |
	|     -----      ------     |
	|             .             |
	|             .             |
	|             .             |
	-----------------------------
	         Main memory
	
	Central Processing Unit (CPU)
	Arithmetic and Logic Unit (ALU)
	
	
	The bottleneck of Von Neumann is the Interconnect, which is realised as a bus (i.e.: a collection of parallel wires). The time of transfer of data or instructions from Main Memory to the CPU is longer than the time of processing of these data or instructions by the CPU.
	%%%%%%%%%% hardware/Von Neumann & improvements/caching
	This is one of the most basic solutions for solving the Interconnect bottleneck issue.
	CPU access Main Memory through several level of cache systems (L1, L2, L3).
	L1 is smaller and faster, L3 is bigger and slower.
	
	Depending on hardware implementation, an instruction or data fetched from Main Memory can be present on the 3 levels of caches, or just on one.
	
	locality
	--------
	Principle of locality: after accessing one memory location, a program will typically access a nearby location (spatial locality) in the near future (temporal locality).
	Data is read by block from Main Memory, so when accessing a single data, a whole line of the following data is read. It reduces time for fetching a line of instruction or data in the Von Neumann architecture. Thus it also speeds up access to arrays.
	
	When checking for information, we call "Ln cache hit" or "Ln hit" a success for retrieving information on level n. A failure is called "Ln cache miss" or "Ln miss".
	
	Read and write accesses are both possible on caches. Write accessed can be made in two ways:
		_ write-through: write is made in synchronisation with Main Memory (i.e.: when writing to the cache, the information is also written to the Main Memory at the same time).
		_ write-back: write to Main Memory is differed (i.e.: updated information is marked as dirty. When the cache line is replaced by a new cache line from memory, the diry line will be written to the Main Memory).
	
	Cache mappings
	==============
	When retrieving a line from Main Memory, we must decide where to put this line in the cache (i.e.: which already present line of memory to evict).
	The Main Memory is divided in lines (blocks of memory). At each line is attributed an index number.
	If the cache contains k lines of memory, here are the different of deciding when goes a line i from the Main Memory :
		_ direct mapping: i % k gives the index in the cache where to put the line. So there is no choice where to put the line.
		_ fully associative mapping: any line in the cache can be used. So there is a choice between n lines.
		_ n-way set associative mapping: each line can be put to one of n locations. We group the k lines of the cache in k/n groups. i % (k/n) gives us the index of the group, we then have n choices of lines indexes in this group. For instance, in a 2-way set associative mapping with a cache of 4 lines, we can have group A with lines 0 and 1, and group B with lines 2 and 3.
	
	Each time there's a choice of several lines where to put the line retrieved from Main Memory, the system has to retain information about unused lines and about which line is the oldest one.
	
	2-Dimensional array
	===================
	When looping on 2-dim array, order of loops is important.
	For instance the two following codes have different performances:
	A)
	for (i = 0 ; i < n ; ++i)
		for (j = 0 ; j < n ; ++j)
			y[i] += A[i][j] * x[j];
	
	B)
	for (j = 0 ; j < n ; ++j)
		for (i = 0 ; i < n ; ++i)
			y[i] += A[i][j] * x[j];
	
	Loops A) can be 3 time faster as loops B). Since is because in loops B), the same memory data are read several time, since data are not accessed in a row. Loops A) accessed the data in the order they were created in the array, so each data is loaded only once in the cache.
	%%%%%%%%%% hardware/Von Neumann & improvements/hardware multithreading
	Thread-level parallelism (TLP) attempts to provide parallelism through the simultaneous execution of different threads.
	
	Hardware multithreading 
	-----------------------
	Hardware multithreading allows CPU to continue doing useful work by switching to another thread when a thread has stalled (e.g.: when waiting for data to be loaded from memory). This suppose to implement very fast switching between threads, in order to be efficient.
	
	Fine-grained multithreading
	---------------------------
	Switch between threads after each instruction.
	Drawback: a thread ready to execute a long series of instruction will have to wait.
	Switching must be instantaneous.
	
	Coarsed-grained multihtreading
	------------------------------
	Only switch threads that are stalled waiting for a time-consuming operation to complete.
	Switching doesn't have to be nearly instantaneous.
	
	Simultaneous multithreading (SMT)
	---------------------------------
	Is a variation of fine-grained multithreading.
	It attemps to exploit superscalar processors by allowing multiple threads to make use of the multiple functional units.
	If we designate "preferred" threads -- threads that have many instructions ready to execute -- we can somewhat reduce the problem of thread slowdown.
	%%%%%%%%%% hardware/Von Neumann & improvements/instruction-level parallelism
	Instruction-level parallelism (or ILP) attemps to improve performance by having multiple processor components or functional units simultaneously executing instructions.
	
	Note that ILP is of no help for certain loops like when calculating the Fibonacci numbers:
	f[0] = f[1] = 1;
	for (i = 2 ; i <= n ; ++i)
		f[i] = f[i-1] + f[i-2];
	
	Pipelining
	==========
	Instructions executed in sequence are connected to each other (output/input) so they can be executed in parallel. The data enters as a flow. There's a overhead the time the piple line is filled, but then the instructions are applied on the data as in a chain.
	
	For instance for an addition of two floats:
	Time  Operation
	0     Fetch operands
	1     Compare exponents
	2     Shift one operand (shift exponent)
	3     Add
	4     Normalize result
	5     Round result
	6     Store result
	
	If each operation takes 1ns.
	For a loop on 1000 elements, it should take 7000 operations so 7000ns:
	for (i = 0 ; i < 1000 ; ++i)
		z[i] = x[i] + y[i]
	
	With pipelining, the output of one instruction is the input of the following instruction. So after the overhead time, all instrutions are executed in parallel at each step. So in all it will take 1006ns, so 7 times less.
	
	Time    Fetch   Compare  Shift ... Store
	0       0
	1       1       0        
	2       2       1        0
	3       3       2        1
	4       4       3        2
	5       5       4        3
	6       6       5        4         0
	.       .       .        .         .
	.       .       .        .         .
	.       .       .        .         .
	999     999     998      997       993
	1000            999      998       994
	1001                     999       995
	1002                               996
	1003                               997
	1004                               998
	1005                               999
	
	In practive, however, a pipeline with k stages, won't get a k-fold improvement in performance. This is because instructions don't take the same amount time to execute. So the stages will run at the speed of the slowest functional unit.
	
	Multiple issue
	==============
	Multiple issue processors replicate functional units and try to simultaneously execute different instructions in a program.
	
	For instance, if a CPU contains two complete floating point adders, we halve the time it takes to execute the loop:
	for (i = 0 ; i < 1000 ; ++i)
		z[i] = x[i] + y[i];
	
	static multiple issue
	---------------------
	If the functional units are scheduled at compile time, the multiple issue system is said to use static multiple issue.
	
	dynamic multiple issue
	----------------------
	If functional units are scheduled at run-time, the system is said to use dynamic multiple issue. A processor that supports dynamic multiple issue is sometimes said to be SUPERSCALAR.
	
	Speculation
	-----------
	This is the most important technique used to find instructions to be executed simultaneously.
	The system makes a guess about an instruction, and then executes instructions on the basis of the guess.
	
	In the following the system can make a guess about the sign of z and execute one of the two instructions. If it appears it's wrong, it will discard what it has done and execute the other instructions.
	z = x + y;
	if (z > 0)
		w = x;
	else
		w = y;
	
	In this other example, the system can make a guess that a_p doesn't point to z. If it happens that a_p points to z, then the instruction will be executed again.
	z = x + y;
	w = *a_p;
	
	Either the compiler ot the processor can make a guess.
	
	Compiler speculation
	--------------------
	The compiler inserts code that tests whether the speculation was correct, and, if not, takes corrective action.
	
	Optimizing compilers can reorder instructions. This can have important consequences for shared-memory programming.
	
	Processor speculation
	---------------------
	The processor stores the result of the speculative execution in a buffer.
	When it's known that the speculation is correct, it transfers the content of the buffer to registers or memory.
	If the speculation was incorrect, it discards the content of the buffer and the instruction is re-executed.
	%%%%%%%%%% hardware/Von Neumann & improvements/virtual memory
	In OS using virtual memory, the Main Memory is viewed as a cache system in order to access huger "virtual memory".
	Virtual memory is divided in blocks (called pages). Each page can be either in the Main Memory (physical memory) or on a secondary storage system called swap space (often put on a part of the main storage system (i.e.: disk)).
	The same principle of locality used for cache systems, is used for the virtual memory. So it keeps in physical memory only the active parts (pages) of the running programs.
	On most systems, pages size is fixed and can range from 4 to 16 kB.
	
	When a program is compiled its page are assigned virtual page numbers. When running, a table is created that maps the virtual address to a physical address.
	
	Translation-lookaside buffer (TLB)
	----------------------------------
	Use of a page table system can double the time to access a location in Main Memory.
	A virtual address is divide in two parts:
		_ high order bits are used for the index of the page
		_ low order bits are used for the address inside this page
	So from a virtual address, we can deduce the page number. But then we must use the page table to translate it into a physical page. If the required part of the page table isn't in the cache (L1,L2,L3) then it must be loaded from memory.
	In order to address this issue, CPUs have a special address translation cache called a translation-lookaside buffer or TLB.
	The TLB caches a small number of entries (16-512) from the page table into very fast memory.
	When looking for an entry, a success is called a "TLB hit" and a failure a "TLB miss".
	
	Accessing a page that's not in the Main Memory is called a "page fault".
	
	Writing to virtual memory
	-------------------------
	Virtual memory only uses a write-back scheme. Modified pages are flagged as dirty. They are written to disk only when they myst be evicted.
	%%%%%%%%%% software/UML/Activity Diagram
	Transition
	==========
	It can labelled as follow :
	event-signature ‘[’ guard-condition ‘]’ ‘/’ action-expression
	
	The event-signature describes an event with its arguments:
	event-name ‘(’ comma-separated-parameter-list ‘)’
	%%%%%%%%%% software/UML/extensibility constructs
	constraints
	===========
	See UML Ref. Manual 1st edition p37.
	Constraints on a mother class are inherited.
	stereotypes
	===========
	See UML Ref. Manual 1st edition p37.
	
	tagged values
	=============
	See UML Ref. Manual 1st edition p37.
	
	class-in-state
	==============
	See UML Ref. Manual 1st edition p139.
	
	class [state]
	OR
	class
	[state]
	
	A class instance with a variable state used in activity diagram to describe lifetime of an object through its different states.
	%%%%%%%%%% software/UML/views & diagrams
	Interaction View
	================
	
	Sequence diagram
	----------------
	Messages in timesequence for roles or objects.
	Lifetime of objects can be shown.
	
	Collaboration diagram
	---------------------
	Messages between objects or roles for a specific interaction.
	Messages are numbered in order.
	
	State machine view
	==================
	State chart diagram, different of states of _one_ object.
	
	Activity view
	=============
	Variant of State machine.
	Shows computationnal activities.
	Shows concurrent operations/activities.
	
	Physical views
	==============
	
	Component diagrams
	------------------
	Shows components and their interfaces and interactions between them with actors.
	
	-----O provides
	-----> requires
	O IListener     = interface IListener
	
	Deployment view
	---------------
	Arrangement of run-time components instances on node instances.
	A node = a computer, device or memory.
	
	Deployment diagram (instance level)
	-----------------------------------
	Shows a particular instance of the deployment diagram (descriptor level).
	
	Model management view
	=====================
	Models the organization of the model itself.
