# Building an MSI for Windows

## msitools

Gnome package to build or access MSI packages.

 * [msitools](https://wiki.gnome.org/msitools).

## MSI Packager

 * [MSI Packager](https://github.com/mmckegg/msi-packager).

Relies on `msitools`.
