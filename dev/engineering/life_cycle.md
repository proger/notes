# Life cycle

pre-alpha: still in development.
alpha: not all features included. Not fully tested by developers.
beta: all features included, but contains bugs.
gamma: last version before release.
