# Regular expressions
<!-- vimvars: b:markdown_embedded_syntax={'cpp':'','python':'','php':'','r':''} -->

 * [Regular expression](https://en.wikipedia.org/wiki/Regular_expression). Listing of different syntax.
 * [PERL regular expressions](https://perldoc.perl.org/perlre).
 * [ICU Regular Expressions](https://unicode-org.github.io/icu/userguide/strings/regexp.html). PERL style, unicode, C++.
 * [PCRE - Perl Compatible Regular Expressions](https://pcre.org/).

## C++

 * [Boost.Regex 7.0.1](https://www.boost.org/doc/libs/1_84_0/libs/regex/doc/html/index.html).

```cpp
#include <boost/regex.hpp>

static const boost::regex re("^uid=(\\d+)\\(.+\\) gid=(\\d+)";
boost::cmatch m;
if (boost::regex_match("uid=1000(john) gid=1000(john)", m, re)) {
    // m[0] is the whole match
    // m.size() is the number of groups
    auto uid = m[1];
    auto gid = m[2];
    // We can also iterate:
    for (auto s: m)
        // ...
}
```

Get multiple matches of the same group:
```cpp
#include <boost/regex.hpp>

static const boost::regex re("(\\d+)\\((\\w+)\\)");
std::string ids("uid=1000(john) gid=1000(john) groups=1000(john),50(games),108(vboxusers)");

const int subs[] = {1, 2,}; // Tells to retrieve sub-groups 1 & 2 in this order. Group 0 is the whole match.
boost::sregex_token_iterator iter(ids.begin(), ids.end(), re, subs);
boost::sregex_token_iterator end;
for( ; iter != end; ++iter)
    std::cout<<*iter<<"\n";
```

## Python

 * [re — Regular expression operations](https://docs.python.org/3/library/re.html).

```python
import re
```

Match --> checks for a match only at the beginning of the string:
```python
re.match(r"o", "foo")  # No match
re.match(r"fo", "foo")  # Match
```
Returns either `None` or a `match.object`.
Thus you can test the result in a `if` statement.

Search --> checks for a match anywhere in the string (like in Perl):
```python
re.search(r"c", "abcdef") # Match
re.search(r"^c", "abcdef") # No match
re.search(r"^[a-z]", "abcdef") # Match
re.search(r"^[a-z]+", "abcdef") # Match
re.search(r"^[a-z]+$", "abcdef") # Match
```

Grouping:
```python
m = re.search(r'([A-Z]+)([0-9]+)', 'HMDB00002')
m.group(2)	# second group
m.group(1)	# first group
m.group(0)	# whole matched string
```

Symbolic group name:
```python
m = re.search(r'(?P<type>[A-Z]+)(?P<index>[0-9]+)', 'HMDB00002')
m.group('type')
m.group('index')
```

Find all matching in a string:
```python
re.findall(r'HMDB([0-9]+)', 'HMDB00002 HMDB00046')
re.findall(r'([A-Z]+)([0-9]+)', 'HMDB00002 G00046')
```

Replacing:
```python
re.sub(r"a", "e", "banana") # Replaces all occurences.
re.sub(r"a", "e", "banana", count=1) # Replaces first occurence.
```

Operators `*`, `+` and `?`  are greedy by default in Python.
To make them lazy, append `?` to them:
```python
re.findall(r'<(.+?)>', '<First text><Second text>')
```

Matching on multiple lines:
```python
re.findall(r'<((.|\n)*)>', "<A text on \ntwo lines>")
```

Compile --> compile a regexp into an object, so evaluation is faster:
```python
compiled_re = re.compile(pattern)
result = compiled_re.match(string)
```

Ignore case:
```python
compiled_re = re.compile(pattern, flags = re.IGNORECASE)
# or 
result = my_re.match(string, flags = re.IGNORECASE)
```

## PHP

```php
<?php declare(strict_types=1);

var_dump(preg_replace('/^(immuno|zina).*$/i', "$1", "ZIMMUNO_aaaaaa"));
var_dump(preg_replace('/^(immuno|zina).*$/i', "$1", "IMMUNO_aaaaaa"));
var_dump(preg_replace('/^(immuno|zina).*$/i', "$1", "ImmUNO_aaaaaa"));
```

# R

```r
if (grepl("^<!toto", "<!toto", perl=TRUE))
    print("FOUND toto")

if (grepl("^<!toto", c("tagada", "zou", "<!toto"), perl=TRUE))
    print("FOUND toto")
```
