# Deployment
<!-- vimvars: b:markdown_embedded_syntax={'nsis':'','dosbatch':'','sh':''} -->

## NSIS software installer

 * [Nullsoft Scriptable Install System](https://nsis.sourceforge.io/)
 * [Java Launcher with automatic JRE installation](https://nsis.sourceforge.io/Java_Launcher_with_automatic_JRE_installation).
 * [A simple installer with start menu shortcut and uninstaller](https://nsis.sourceforge.io/A_simple_installer_with_start_menu_shortcut_and_uninstaller).

For creating installers for Windows.
Only works on Windows.
Successfully installed with *wine*.

Run command line:
```dosbatch
makensis script.nsi
```

By default `makensis` change current directory to its installation directory. To disable this feature use `/NOCD` flag:
```dosbatch
makensis /NOCD script.nsi
```

## Set icon

```nsis
section "install"
	file "my\path\to\my_icon.ico"
	CreateShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}.lnk" "${MYAPP}" '${MY_FLAGS}' "$INSTDIR\my_icon.ico"
```

The `.ico` file can be created from a PNG using ImageMagick:
```sh
convert myimage.png -define icon:auto-resize=16,24,32,48,64,72,96,128,256 myicon.ico
```
