# Imaging

books:
 * 3D Game Engine Design, David JH. Eberly, 1558605932
 * Advanced Animation and Rendering Techniques, Alan Watt & Mark Watt, 0201544121
 * Compression et cryptage des données multimédias, Xavier Marsault

 * [The Incredibly challenging task of sorting colours](https://www.alanzucconi.com/2015/09/30/colour-sorting/).
 * [Math behind colorspace conversions, RGB-HSL](https://www.niwa.nu/2013/05/math-behind-colorspace-conversions-rgb-hsl/).
