# Building a .dmg for macos

 * [How do I create a DMG file on linux Ubuntu for MacOS](https://askubuntu.com/questions/1117461/how-do-i-create-a-dmg-file-on-linux-ubuntu-for-macos).
 * [Bringing your Java Application to Mac OS X Part Three](https://www.oracle.com/technical-resources/articles/javase/javatomac3.html).

Packaging a script can be made with just the script:
 * Create the app folder using the name of your script: `MyScript.app`.
 * Then inside this folder put your script and name it `MyScript`, without any extension.
 * The script must have a shebang.
 * The script must be made executable for everyone.
 * Folder and all files inside must have ownership set to `root:wheel`.

## Make .icns file

First create all PNG files of the different sizes:
```sh
convert -resize 16x16 myicon.png icon_16x16.png
convert -resize 32x32 myicon.png icon_16x16@2x.png
convert -resize 32x32 myicon.png icon_32x32.png
convert -resize 64x64 myicon.png icon_32x32@2x.png
convert -resize 128x128 myicon.png icon_128x128.png
convert -resize 256x256 myicon.png icon_128x128@2x.png
convert -resize 256x256 myicon.png icon_256x256.png
convert -resize 512x512 myicon.png icon_256x256@2x.png
convert -resize 512x512 myicon.png icon_512x512.png
convert -resize 1024x1024 myicon.png icon_512x512@2x.png
```

then use `png2icns`:
```sh
png2icns myicon.icns icon_*.png
```
