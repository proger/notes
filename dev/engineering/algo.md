# Algorithmic

Books:
 * Introduction to Algorithms, Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest
 * The Art of Computer Programming (Relié) de Donald Ervin Knuth, ~140€ : attendre la réédition (4ème édition) pour l'acheter
 * Handbook of Mathematics and Computational Science, John W. Harris & Horst Stocker, 0387947469
 * Les réseaux neuromimétiques, Jean-François Jodouin
 * Numerical Methods for Mathematics, Science, and Engineering, John H. Mathews, 0136249906

## Dynamic programming

 * [Dynamic programming](https://en.wikipedia.org/wiki/Dynamic_programming#Algorithms_that_use_dynamic_programming).

[From Wikipedia, 21/02/2013]
The key idea behind dynamic programming is quite simple. In general, to solve a
given problem, we need to solve different parts of the problem (subproblems),
then combine the solutions of the subproblems to reach an overall solution.
Often, many of these subproblems are really the same. The dynamic programming
approach seeks to solve each subproblem only once, thus reducing the number of
computations: once the solution to a given subproblem has been computed, it is
stored or "memo-ized": the next time the same solution is needed, it is simply
looked up. This approach is especially useful when the number of repeating
subproblems grows exponentially as a function of the size of the input.

## Image processing

### Morphological image processing

See [Morphological Image Processing](https://www.cs.auckland.ac.nz/courses/compsci773s1c/lectures/ImageProcessing-html/topic4.htm).
See [Encyclopedia of Computer Science and Technology: Volume 38 - Supplement 23](https://books.google.fr/books?id=PySs1uQ2l3gC&pg=PA169&lpg=PA169&dq=image+processing++find+boundaries+of+objects+in+black+and+white&source=bl&ots=q6UE2ii1Yw&sig=7tq537t9pfkDeIBWYJj8d6dg9-4&hl=en&sa=X&ei=WY0UVcuzA-Ov7AbFkIHwAQ&redir_esc=y#v=onepage&q=image%20processing%20%20find%20boundaries%20of%20objects%20in%20black%20and%20white&f=false).

Technics:
 * Edge detection
 * Thresholding
 * Boundaries object detection
 * Erosion
 * Dilation is the inverse of erosion.
 * Open: erosion followed by dilation.
 * Close: dilation followed by erosion.

For Edge detection, see [Sobel
operator](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/image-processing/edge_detection.html).

## RCPSP (Resource-Constrained Project Scheduling Problem)

 * [PSPSolver](http://sourceforge.net/projects/pspsolver).
 * <http://kti.mff.cuni.cz/~bartak/PLANSIG2007/papers/paper06.pdf>.

## Scheduling algorithms

In operating systems, scheduling algorithms are used to choose which process, thread or data flow have to be executed now and which have to wait.

For instance, the Round-robin algorithm assigns a fixed time unit per process, and cycles through them.

## Strings/Sequences distance

### Distance de Hamming

Distance entre deux séquences de symboles de même longueur.
C'est le nombre de positions où les deux suites diffèrent.

### Distance de Levenshtein

Nombre minimal de symbols qu'il faut supprimer, insérer ou remplacer pour passer d'une série à l'autre.
Les deux séries ne sont donc pas obligatoirement de même longueur.

## NP-Problems

NON-DETERMINISTIC POLYNOMIAL TIME PROBLEMS

An NP ("nondeterministic polynomial time.") problem is one for which proofs that the answer is "yes" for a solution have to be verifiable in polynomial time by a deterministic Turing machine.
The "yes"-instances (i.e.: the set of solutions) can be decided in polynomial time by a non-deterministic Turing machine.

So the resolution algorithm of an NP problem consists of 2 phases:
1) guess the solution by using a non-deterministic way
2) use a deterministic algorithm to verify or reject the guess as a valid solution to the problem.

For simplicity, the theory of NP-completeness restricts attention to decision problems (see abstract-decision-problems.txt and problems.txt).

### Verification algorithm

A verification algorithm is a two-argument algorithm A(x,y), where x is an ordinary input string and y is a binary string called the _certificate_.
A two-argument algorithm A _verifies_ an input string x if there exists a certificate y such that A(x,y)=1.

The language verified by a verification algorithm A is:
	L={x∈{0,1}*: there exists y∈{0,1}* such that A(x,y)=1}.

Intuitively: an algorithm A verifies a language L if for any string x∈L, there is a certificate y that A can use to prove x∈L.
Moreover, for any string x∉L, there must be no certificate proving that x∈L.

### Language view definition of NP complexity class

So from the theory of formal-languages we rephrase the definition as:

The complexity class NP is the class of languages that can be verified by a polynomial-time algorithm.
More precisely, a language L belongs to NP if and only if there exists a two-input polynomial-time algorithm A and constant c such that:
	L={x∈{0,1}*: there exists a certificate y with |y|=O(|x|^c) such that A(x,y)=1}.

### NP-Complete

See problem-reducibility.txt.

NP-complete languages are the hardest problems in NP.
A language `L⊆{0,1}*` is NP-complete if
  1. L∈NP, and.
  2. L'≤pL for every L'∈NP.

If a language L satisfies property 2, but not necessarily property 1, we say that L is NP-hard.

NPC = class of NP-complete languages.

If any one NP-complete problem can be solved in polynomial time, then every problem in NP has a polynomial-time solution (i.e.: P=NP). As of today, no NP-complete problem has been solved in polynomial time.

### Finding a solution for an NP-Complete problem

If the actual inputs are small, an algorithm with exponential running time may be perfectly satisfactory.

Otherwise we must use an approximation algorithm (see approx-algo.txt).

### Approximation algorithms

Assume that we are working on an optimization problem, in which each potential solution has a positive cost.
We wish to find a near-optimal solution.
The problem may be a maximization or minimization problem.

#### Ratio bound

An approximation algorithm for the problem has a ratio bound of ρ(n) if:
	∀ n, max(C/C*, C*/C) ≤ ρ(n).
	n = size of input
	C = cost of produced solution
	C* = cost of optimal solution

The formula is for both maximization and minimization problems, since:
	for a maximization problem 0<C≤C*
	for a minimization problem 0<C*≤C

So ρ(n)≥1 always.
An optimal algorithm has ρ(n)=1.

#### Relative error bound

The relative error of an approximation algorithm is :
	|C-C*|/C*

An approximation algorithm has a relative error bound of ε(n) if:
	|C-C*|/C* ≤ ε(n).

⇒  ε(n) ≤ ρ(n) - 1

#### Fixed ratio bound

For many problems, approximation algorithms have been developed that have a fixed ratio bound, independent of n.

#### Approximation scheme

An approximation scheme for an optimization problem is an approximation algorithm that takes as input:
_ an instance of the problem
_ a value ε>0
such that for any fixed ε, the scheme is an approximation algorithm with relative error bound ε.

##### Polynomial-time approximation scheme

We say that an approximation scheme is a polynomial-time approximation scheme if for any fixed ε>0, the scheme runs in time polynomial in the size n of its input instance.

The running time of a polynomial-time approximation scheme should not increase too rapidly as ε decreases.
Ideally, if ε decreases by a constant factor, the running time to achieve the desired approximation should not increase by more than a constant factor.
In other words, we would like the running time to be polynomial in 1/ε as well as in n.

We say that an approximation scheme is a fully polynomial-time approximation scheme if its running time is polynomial both in 1/ε and in n.

For example: the scheme might have a running time of (1/ε)²n³. With such a scheme, any constant-factor decrease in ε can be achieved with a corresponding constant-factor increase in running-time.

### Examples of NP Problems

#### 3-CNF Satisfiability

The 3-CNF statisfiability problem is NP-complete.

For a formula Φ, is this formula 3-CNF satisfiable ?

A literal = occurrence of a variable or its negation (¬).
CNF=Conjunctive Normal Form.
A boolean formula is in CNF if it is expressed as an AND (∧) of clauses, each
of which is the OR (∨) of one or more literals.
A boolean formula is in 3-CNF if each clause has exactly 3 distinct literals.

For example: (x₁ ∨ ¬x₁ ∨ ¬x₂) ∧ (x₃ ∨ x₂ ∨ x₄) ∧ (¬x₁ ∨ ¬x₃ ∨ ¬x₄)

#### Circuit satisfiability

The circuit satisfiability problem is NP-hard.

A boolean combinational circuit composed of AND, OR and NOT gates is
satisfiable, if there exists a set of inputs (assignment) that gives an output
of 1.
CIRCUIT-SAT = {<C>: C is satisfiable boolean combinational circuit}
If there are k inputs, there are 2^k possible assignments. When the size of C
is polynomial in k, checking each assignment leads to a superpolynomial-time
algorithm.

#### Clique problem

The clique problem is NP-complete.

A clique in an undirected graph G=(V,E) is a subset V'⊆V of vertices, each pair of which is connected by an edge in E.
In other words, a clique is a complete subgraph of G.
The size of a clique is the number of vertices it contains.

The clique problem is the optimization problem of finding a clique of maximum size in graph.
As a decision problem, we ask simply whether a clique of a given size k exists in the graph.

#### Formula satisfiability

The formula satisfiability problem is NP-complete.

Similar to circuit satisfiability but for a boolean formula.
For a set :
1. input variables x1,x2,... 
2. binary operators AND, OR, NOT, =>, <=>
3. parentheses
A formula is satisfiable if there exists a set of input variables for which the
formula output is 1.

#### Hamiltonian cycle problem

A hamiltonian cycle (from W. R. Hamilton) of an undirected graph G=(V,E) is a simple cycle that contains each vertex in V.

A graph that contains a hamiltonian cycle is said to be hamiltonian, otherwise it is nonhamiltonian.

We can define the hamiltonian-cycle problem, "Does a graph G have a hamiltonian cycle ?" as a formal language:
HAM-CYCLE = {<G>: G is a hamiltonian graph}.

##### Decision algorithm

How might an algorithm decide the language HAM-CYCLE ?

Given a problem instance <G>, one possible decision algorithm lists all permutations of the vertices of G and then checks each permutation to see if it is a hamiltonian path.

If we use the "reasonable" encoding (see encodings.txt) of a graph as its adjacency matrix, the number m of vertices in the graph is Ω(√n) where n=|<G>| is the length of the encoding of G.

There are m! possible permutations of the vertices, and therefore the running time is Ω(m!)=Ω(√n!)=Ω(2^(√n)) which is not O(n^k) for any constant k.

In fact the hamiltonian-cycle problem is NP-complete.

##### Verification algorithm

However given a list of vertices, it is easy to check that it forms a hamiltonian cycle.
The verification algorithm can be implemented in O(n²) time, where n is the length of the encoding of G.

#### Set covering problem

[From "Introduction to Algorithms", chapter 37.3, by Cormen, Leiserson and Rivest)

The set-covering problem is an optimization problem that models many resource-selection problems.

It generalizes the NP-complete vertex-cover problem and is therefore also NP-hard.

An instance (X,F) of the set-covering problem consists of a finite set X and a family F of subsets of X, such that every element of X belongs to at least one subset of F:

X =  ∪ S
    S∈F

We say that a subset S∈F covers its elements. The problem is to find a minimum-size subset C⊆F whose members cover all of X:

X = ∪ S        (E.1)
   S∈C

We say that any C satisfying equation (E.1) covers X.

In the decision version of the set-covering problem, we ask whether or not a covering exists with size at most k.

Example:
	X = {x₁, ..., x₁₂}
	S₁ = {x₁, ..., x₆}
	S₂ = {x₅, x₆, x₈, x₉}
	S₃ = {x₁, x₄, x₇, x₁₀}
	S₄ = {x₂, x₅, x₇, x₈, x₁₁}
	S₅ = {x₃, x₆, x₉, x₁₂}
	F = {S₁, S₂, S₃, S₄, S₅}
	A minimum-size set cover is C = {S₃, S₄, S₅}.
	The greedy algorithm presented below produces a cover of size 4 by selecting the sets S₁, S₄, S₅ and S₃ in order.

Example of application:
	Suppose that X represents a set of skills that are needed to solve a problem and that we have a given set of people available to work on the problem.
	We wish to form a committee, containing as few people as possible, such that for every requisite skill in X, there is a member of the committee having that skill.

##### Greedy approximation algorithm

The greedy method works by picking, at each stage, the set S that covers the most remaining uncovered elements.

GREEDY-SET-COVER(X,F)
U← X
C← ∅
while U≠∅           # (1)
	do select an S∈F that maximizes |S∩U|       # (2)
		U← U-S
		C← C∪{S}
return C

The algorithm can easily be implemented to run in time polynomial in |X| and |F|.
The loop (2) makes at most min(|X|,|F|) iterations.
The loop (1) can be implemented to run in time O(|X||F|).
Thus there is an implementation that runs in time O(|X||F|min(|X|,|F|)).

The algorithm has a ratio bound of H(max{|S|:S∈F}),
                d   1
    where H(d)= ∑  ---
               i=1  i

    which leads to a ratio bound of ln(|X|)+1    (since H(n) ≤ ln(n)+1).

#### Subset-sum problem

In the subset-sum problem, we are given a finite set S⊂N and a target t∈N.
We ask whether there is a subset S'⊆S whose elements sum to t.

For example if S={1,4,16,64,256,1040,,1041,1093,1284,1344} and t=3754, then the
subset S'={1,16,64,256,1040,,1093,1284} is a solution.

We assume that the input integers are coded in binary.

The subset-sum problem is NP-complete.

In pratical applications, we wish to find a subset of S={x₁, x₂, ..., xn} whose
sum is as large as possible but not larger than t.

For example, we may have a truck that can carry no more than t pounds, and n
different boxes to ship, the ith of which weighs xᵢ pounds. We wish to fill the
truck as full as possible without exceeding the given weight limit.

##### An exponential-time algorithm

If L is a list of positive integers and x another positive integer, we let L+x
the list of integers derived from L by increasing each element of L by x.
We use also this notation fro sets:
S+x = {s+x: s∈S}.

MERGE-LISTS(L,L') is a procedure that merges the two sorted lists L and L' and
returns the resulting list sorted. So this is the same sub-procedure used in
merge-sort algorithm. It runs in O(|L|+|L'|).

The EXACT-SUBSET-SUM algorithm returns the maximum sum of elements xᵢ, that is
lower of equal to t.

EXACT-SUBSET-SUM(S,t)
n← |S|
L₀← <0>
for i← 1 to n
	Lᵢ← MERGE-LISTS(Lᵢ₋₁, Lᵢ₋₁+xᵢ)
	remove from Lᵢ every element that is greater than t
return the largest element in Ln.

It forms successively the lists:
0, x₁
0, x₁, x₂, x₁+x₂
0, x₁, x₂, x₃, x₁+x₂, x₁+x₃, x₂+x₃, x₁+x₂+x₃
...
and retains only the sums that are lower or equal to t.

The length of Lᵢ can be as much as 2ⁱ, so EXACT-SUBSET-SUM is an
exponential-time algorithm in general.
However it is a polynomial-time algorithm in the special cases in which t is
polynomial in |S| or all the numbers in S are bounded by a polynomial in |S|.

##### A fully polynomial-time approximation scheme

The idea is to trim each list Lᵢ after it is created.

We use a trimming parameter δ such that 0<δ<1.
To trim a list L by δ means to remove as many elements from L as possible, in
such a way that if L' is the result of trimming L, then for every element y
that was removed from L, there is an element z≤y still in L' such that:

y - z
----- ≤ δ
  y

or, equiavlently,
(1-δ)y ≤ z ≤ y.

In a sense, z "reprensts" y in the new list L'.

For example, if δ=0.1 and
L = <10, 11, 12, 15, 20, 21, 22, 23, 24, 29>
then we can trim L to obtain
L' = <10, 12, 15, 20, 23, 29>

The TRIM algorithm trims an input list L=<y₁, y₂, ..., ym> in time Θ(m),
assuming that L is sorted into nondecreasing order.
The output is the trimmed, sorted list.

TRIM(L, δ)
m← |L|
L'← <y₁>
last← y₁
for i← 2 to m
	do if last < (1-δ)yᵢ
		then append yᵢ onto the end of L'
			last←yᵢ
return L'

The APPROX-SUBSET-SUM algorithm takes an additional parameter ε which
represents an "approximation parameter".

APPROX-SUBSET-SUM(S,t,ε)
n← |S|
L₀← <0>
for i← 1 to n
	Lᵢ← MERGE-LISTS(Lᵢ₋₁, Lᵢ₋₁+xᵢ)
	Lᵢ← TRIM(Lᵢ, ε/n)
	remove from Lᵢ every element that is greater than t
return the largest element in Ln.

APPROX-SUBSET-SUM is a fully polynomial-time approximation scheme for the
subset-sum problem.

===================== NP-problems/traveling-salesman-problem.txt
[From "Introduction to Algorithms", chapter 37.2, by Cormen, Leiserson and Rivest)

In the traveling-salesman problem, which is closely related to the hamiltonian-cycle problem, a salesman must visit n cities.
Modeling the problem as a complete graph with n vertices, we can say that the salesman wishes to make a tour, or hamiltonian cycle, visiting each city exactly once and finishing at the city he starts from.

There is an integer cost c(i,j) to travel from city i to city j, and the salesman wishes to make the tour whose total cost is minimum, where the total cost is the sum of the individual costs along the edges of the tour.

The formal language for the traveling-salesman problem is:
TSP = {<G,c,k>: G=(V,E) is a complete graph,
                c is a function from VxV→Z,
                k∈Z, and
	            G has a traveling-salesman tour with cost at most k}.

The traveling-salesman problem is NP-complete.

APPROXIMATION ALGORITHM
=======================
To solve the traveling-salesman problem, we must first make the assumption of the triangle inequality:
c(u,w) ≤ c(u,v) + c(v,w)        where c(i,j) is the cost to go from city i to city j.
which in the problems met in practice is true.

APPROX-TSP-TOUR(G,c)
1. select a vertex r∈V[G] to be a "root" vertex
2. grow a minimum spanning tree T fro G from root r using MST-PRIM(G,c,r) (see ../trees/mst.txt)
3. let L be the list of vertices visited in a preorder tree walk of T
4. return the hamiltonian cycle H that visits the vertices in the order L

The running time is Θ(E)=Θ(V²).
The ratio bound is 2, assuming triangle inequality.
===================== NP-problems/vertex-cover-problem.txt
[From "Introduction to Algorithms", chapter 37.1, by Cormen, Leiserson and Rivest)

A vertex cover of an undirected graph G=(V,E) is a subset V'⊆V such that if (u,v)∈E, then u∈V' or v∈V' (or both).
That is, each vertex "covers" its incident edges, and a vertex cover for G is a set of vertices that covers all the edges in E.
In other words: a vertex cover of a graph is a set of vertices such that each edge of the graph is incident to at least one vertex of the set.
The size of a vertex cover is the number of vertices in it.

For example the following graph has a vertex cover {w,z} of size 2.
       v
      / \
     /   \
    /     \
   /       \
u-/---------w
 /         /
z         /
 \       x
  \
   \
    y


The vertex-cover problem is to find a vertex cover of minimum size in a given graph.
Restating this optimization problem as a decision problem, we wish to determine whether a graph has a vertex cover of a given size k.
As a language, we define:
VERTEX-COVER={<G,k>: graph G has vertex cover of size k }.

The vertex-cover problem is NP-complete.

APPROXIMATION ALGORITHM
=======================

C← ∅
E'← E[G]
while E'≠∅
	do let (u,v) be an arbitrary edge of E'
		C← C ∪ {u,v}
		remove from E' every edge incident on either u or v
return C

The running time is O(E).
The ratio bound is 2.
===================== basis/P-problems.txt
POLYNOMIAL TIME PROBLEMS

A P problem (or PTIME) is a problem which can be solved by a deterministic Turing machine using a polynomial amount of computation time, or polynomial time.
In other words: the complexity class P is the set of concrete decision problems (see concrete-problems.txt) that are solvable in polynomial time.

---------------------------------------------------------------------------------------------------------
| Using formal language theory (see formal-language.txt), we can also define the complexity class P as: |
| P = {L⊆{0,1}*: there exists an algorithm A that decides L in polynomial time}                         |
| OR                                                                                                    |
| P = {L: L is accepted by a polynomial-time algorithm}                                                 |
---------------------------------------------------------------------------------------------------------

Generaly P is considered to be the class of computational problems which are "efficiently solvable". In practice, however, some problems not known to be in P have practical solutions, and some that are in P do not.

The worst-case running time of a P problem is O(n^k) for some constant k.

Problems that can be solved by polynomial-time algorithms are called "tractable". Those that require superpolynomial time are called "intractable". Why ?

1) This is arguable, since a problem in O(n^100) can hardly be considered as "tractable". However such an order is rare in polynomial time algorithms, the value k is rather low in general.

2) A problem that can be solved in polynomial time in one model of computation, is generally solvable in another model of computation. For instance the class of problems solvable in polynomial time by a serial random-access machine, is the same as a the class problems solvable in polynomial time on a parallel computer, even if the number of processors grows polynomially with the input size.

3) The class of polynomial-time solvable problems has nice closure properties since polynomials are closed under addition, multiplication and composition. For example:
	_ if the output of one polynomial-time algorithm is fed into the input of another, the composite algorithm is polynomial.
	_ if a polynomial-time algorithm makes a constant number of calls to polynomial-time subroutines, the running time of the composite algorithm is polynomial.

POLYNOMIAL-TIME SOLVABILITY & ENCODINGS
=======================================
We would like to extend the definition of polynomial-time solvability from concrete problems to abstract problems using encodings as the bridge (see concrete-problems.txt and encodings.txt), but we would like the definition to be independent of any particular encoding.

However the efficiency of solving a problem depends heavily on how the problem is encoded.

For example:
An algorithm takes an integer k as input and runs in Θ(k).
If the integer is provided in unary encoding (see encodings.txt) then the running time is O(n) on length-n inputs, which is polynomial time.
If the integer is provided in binary encoding, then the input length is n = log2(k), and the running time is Θ(k)=Θ(2^n).
So the algorithm runs either in polynomial time or in exponential time, depending on the encoding of the input.

However in practice the encoding make little difference to wether the problem can be solved in polynomial time.
In practice we don't use expensive encodings such as unary encodings, and for common encodings such as base 2 and base 3, there exist conversions base2->base3 and base3->base2 which execute in polynomial time. So whatever the encoding we use in practice, if a problem is solvable in polynomial time in one encoding, it will also be solvable in polynomial time in another.

Polynomial-time computable function
-----------------------------------
A function f:{0,1}*->{0,1}* is polynomial-time computable if there exists a polynomial-time algorithm A that, given any input x∈{0,1}*, produces as output f(x).

Polynomial-time related encodings
---------------------------------
Two encodings e₁ and e₂ are polynomially related for some set I of problem instances, if there exist two polynomial-time computable functions f₁₂ and f₂₁ such that:
∀i∈I f₁₂(e₁(i))=e₂(i) and f₂₁(e₂(i))=e₁(i)

Lemma
-----
Let Q be an abstract decision problem on an instance set I, and let e₁ and e₂ be polynomial related encodings on I.
Then, e₁(Q)∈P ⟺  e₂(Q)∈P.
===================== basis/Turing machine.txt
An ordinary (deterministic) Turing machine (DTM) is like a simple computer that reads and writes symbols one at a time, by following a set of rules.
In a DTM the set of rules prescribes at most one action to be performed for any given situation. Example: "If you are in state 2 and you see an 'A', change it to 'B' and move left.".

A non-deterministic Turing machine (NTM) may have a set of rules that prescribes more than one action for a given situation. For example an NTM could have the two following rules:
"If you are in state 2 and you see an 'A', change it to a 'B' and move left"
"If you are in state 2 and you see an 'A', change it to a 'C' and move right"

How an NTM choose the rule to apply ?
_ "luckiest possible guesser": it always picks the transition which eventually leads to an accepting state,
_ "branches":  the machine "branches" into many copies, each of which follows one of the possible transitions.

A DTM has a single "computation path" that it follows.
An NTM has a "computation tree". 
===================== basis/abstract-decision-problems.txt
ABSTRACT DECISION PROBLEMS

The decision problems are those having a yes/no solution.

An abstract decision problem can be seen as a function that maps the instance set I to the solution set {0,1}.

For instance, a decision problem PATH related to the shortest-path problem is:
	_ given a graph G=(V,E)S
	_ two vertices u,v ∈ V
	_ k > 0
	_ does a path exist in G between u and v whose length is at most k ?
If i=<G,u,v,k> is an instance of this shortest-path problem, then:
	_ PATH(i) = 1 if a shortest path from u to v has length at most k
	_ PATH(i) = 0 otherwise

For polynomial time solvability, see P-problems.txt.

VIEW AS FORMAL-LANGUAGE
=======================
From the point of view of language theory (see formal-language.txt), the set of instances for any decision problem Q is simply the set Σ*, where Σ={0,1}.
Since Q is entirely characterized by those problem instances that produce a 1 (yes) answer, we can view Q as a language L over Σ={0,1}, where:
L = {x∈Σ*: Q(x)=1}.

For example, the decision problem PATH has the corresponding language:
PATH = {<G,u,v,k>: G = (V,E) is an undirected graph,
	               u,v ∈ V,
	               k≥0 is an integer,
	               and there exists a path from u to v in G whose length is at most k }.

The language PATH can be accepted on polynomial time.

One polynomial-time accepting algorithm computes the shortest path from u to v in G, using breadth-first search, and the compares the distance obtained with k. If the distance is at most k, the algorithm outputs 1 and halts. Otherwise, the algorithm runs forever.

This algorithm does not decide PATH, however, since it does not explicitly output 0 for instances in which the shortest path has length greater than k.

A decision algorithm for PATH must explicitly reject binary strings that do not belong to PATH.

For a decision problem such as PATH, such a decision algorithm is easy to design.
For other problems, such as Turing's Halting Problem, there exists an accepting algorithm, but no decision algorithm exists.
===================== basis/abstract-problems.txt
ABSTRACT PROBLEMS

An abstract problem Q is a binary relation on as set I of problem instances and a set S of problem solutions.

Example:
Consider the problem SHORTEST-PATH of finding a shortest path between two given vertices in a unweighted, undirected graph H = (V,E).
An instance of SHORTEST-PATH is a triple consisting of a graph and two vertices.
A solution is a sequence of vertices in the graph (empty if no solution).
The problem SHORTEST-PATH itself is the relation that associates each instance with a solution.

An instance may have more than one solution.

===================== basis/analyzing.txt
[From "Introduction to Algorithms", chapter 1, by Cormen, Leiserson and Rivest)

Analyzing an algorithm or measuring its performance, is dependent on hardware consideration. Is the algorithm running on:
_ a one-processor, random-access machine (RAM) ;
_ parallel computers ;
_ digital hardware.
==> depending on the targeted model, the choice of the algorithm won't be the same.

What we want to analyze about an algorithm can be:
_ memory ;
_ communication bandwidth ;
_ logic gates ;
_ computational time.

Computational time is by far the one we most often want to evaluate.

COMPUTATIONAL TIME EVALUATION
=============================
To evaluate computational time, we need to define two important terms:
_ input size. This notion can vary depending on the input we need to process:
	_ in a Fourier transform: the number of items in the input
	_ in a multiplication of two integers: the total number of bits
	_ in a graph: a couple of numbers made of the number of vertices, and the number of edges
_ running time:
	We consider for a pseudo code, that each line executes in constant time (ie: the ith line will take time ci to execute).
	If the line is call to a subroutine (for instance a sort algorithm), we don't take into account the amount of time taken by the subroutine but only the time for calling the subroutine which a constant time.
	For a given algorithm we count how much time each line is executed, and then we compute total time by multiplying these figures by ci constants.

	Example of the insertion-sort algorithm:
	-------------CODE-------------      COST    TIMES
	for j = 2 to n                      c1      n
		key = A[j]                      c2      n-1
		i = j - 1                       c3      n-1
		while i > 0 and A[i] > key      c4      sum(tj, j=2..n)
			A[i + 1] = A[i]             c5      sum(tj - 1, j=2..n)
			--i                         c6      sum(tj - 1, j=2..n)
		A[i + 1] = key                  c7      n-1

	Total time T(n) = c1.n + c2.(n-1) + c3.(n-1) + c4.sum(tj) + c5.sum(tj - 1) + c6.sum(tj - 1) + c7.(n-1)

	The best-case running time is when the list to sort is already sorted. In this case tj = 1 for all values of j.
	So T(n) = (c1 + c2 + c3 + c4 + c7) . n - c2 - c3 - c4 - c7
	        = a . n + b

	The worst-case running time is when the list is already sorted in reverse order. In this case tj = j, and sum(tj) = n(n+1)/2 - 1 and sum(tj - 1) = n(n-1)/2.
	So T(n) = a . n^2 + b .n + c

	The average-case running time is when the list is in random order. In this case tj = j/2 and we find again a quadratic time:
	T(n) = a . n^2 + b .n + c

In general this is the worst-case that interests us, since we want to have a measure that tells us that an algorithm will never run more time than T.
The average-case can sometimes be interesting, but it can be hard to determine what is an average-case for an algorithm.

Order of growth
---------------
What interests us is in the worst-case is the order of growth or rate of growth. So in a formula we're only interested in the leading term. (e.g.: an^2 in the insertion-sort algorithm worst-case).
The rate of growth is noted with the Θ-notation: Θ(n^2).
===================== basis/asymptotic notation.txt
[From "Introduction to Algorithms", chapter 2, by Cormen, Leiserson and Rivest)

When studying algorithm running time, we usually look at running time of large inputs.
This is why we often talk of _asymptotic_ efficiency of algorithms: how the running time of an algorithm increases with the size of the input /in the limit/.

Θ-NOTATION
==========
The Θ-notation asymptotically bounds a function from above and below.

f(n) is part of Θ(g(n)),
	if ∃ c₁>0, ∃ c₂>0 and ∃ n₀>0 such that
	∀ n>=n₀ : 0 <= c₁.g(n) <= f(n) <= c₂.g(n).

We write f(n)=Θ(g(n)) or f(n)∈Θ(g(n)),
and we say that g(n) is an asymptotically tight bound for f(n).

highest-order term notation
---------------------------
For a polynomial expression of g(n), we can consider only the highest order term in the Θ-notation. This is because for a polynome we can always find a couple (c₁,c₂) and a n₀ large enough that will do well.
For instance for f(n) = a.n² + b.n + c, with a > 0. We can choose c₁=a/4 and c₂=7a/4 and n₀=2*max((|b|/a), √(|c|/a)),
and we will have 0 <= c₁.n² <= a.n² + b.n + c <= c₂.n² for n>=n₀.

However there's an abuse of the highest-order term notation with Θ(n^0) or Θ(1) which refers to a constant. In this case it is not clear what variable is tending to infinity.

O-NOTATION
==========
The O-notation asymptotically bounds a function from above (asymptotic upper bound).

f(n) is part of O(g(n)),
	if ∃ c>0 and ∃ n₀>0 such that
	∀ n>=n₀ : 0 <= f(n) <= c.g(n).

Note: f(n)=Θ(g(n)) => f(n)=O(g(n)).
And thus: Θ(g(n))⊆O(g(n))

The running time of an algorithm is often expressed in term of O-notation. However O-notation only describes an upper bound, so when we say that an algorithm running time is O(n²), we mean that the worst-case running time is O(n²).
In other words, it is an abuse to say that the running time is O(n²), since for a given n, the actual running time depends on the particural input of size n.

This O-notation is an upper bound, we can write things like:
n = O(n²)
Here the equal sign must be understand as "is membership of":
n ∈ O(n²)

For instance for the insertion sort algorithm:
_ The worst-case running time is in Θ(n²).
_ So the worst-case running time is also in O(n²).
_ But when the input is already sorted, insertion sort runs in Θ(n) time.

Ω-NOTATION
==========
Ω-notation provides an asymptotic lower bound.

f(n) is part of Ω(g(n)),
	if ∃ c>0 and ∃ n₀>0 such that
	∀ n>=n₀ : 0 <= c.g(n) <= f(n).

Note: f(n)=Θ(g(n)) => f(n)=Ω(g(n)).
And thus: Θ(g(n))⊆Ω(g(n))

When we say that the running time of an algorithm is Ω(g(n)), we mean that for sufficiently large n (n>=n₀), the running time of the algorithm is at leasta constant times g(n).

Example:
The running time of insertion sort falls between Ω(n) and O(n²).

Θ, O, Ω THEOREM
===============
F(n)=Θ(g(n)) ⟺  f(n)=O(g(n)) and f(n)=Ω(g(n))

ASYMPTOTIC  NOTATION IN EQUATIONS
=================================

2n² + 3n + 1 = 2n² + Θ(n) means that 2n² + 3n + 1 = 2n² + f(n) where f(n)∈Θ(n).

Using asymptotic notation can help eliminate inessential detail and clutter in an equation. Like in the recurrence equation of the worst-case running time of merge sort:
T(n) = 2.T(n/2) + Θ(n)

In an expression, the number of anonymous functions f(n) is equal to the number of times the asymptotic notation appears. For instance :
 n                          n
 ∑ O(i) is translated into  ∑ f(i) and not into O(1)+O(2)+...+O(n).
i=1                        i=1

Asymptotic notation can appear on the left-hand side of an equation:
2.n² + Θ(n) = Θ(n²)
This equation means:
∀ f(n) ∈ Θ(n), ∃ g(n) ∈ Θ(n²) : ∀n 2.n² + f(n) = g(n)

Thus we can also write:
2n² + 3n + 1 = 2n² + Θ(n)
             = Θ(n²)

o-NOTATION
==========
The asymptotic upper bound provided by O-notation may or may not be asymptotically tight.
The bound 2n²=O(n²) is asymptotically tight, but the bound 2n=O(n²) is not.

o-notation is used to denote an upper bound that is not asymptotically tight.

f(n) is part of o(g(n)) if:
	∀ c>0, ∃ n₀>0 such that
	∀ n>=n₀ : 0 <= f(n) < c.g(n).

So 2n=o(n²), but 2n²o≠(n²).

The difference between O-notation and o-notation, is that in o-notation the bound of f(n) holds for all constants c>0.
The function f(n) becomes insignificant relative to g(n) as n approaches infinity:

    f(n)
lim ---- = 0
n→∞ g(n)

ω-NOTATION
==========
ω-notation is used to denote an lower bound that is not asymptotically tight.

f(n) is part of ω(g(n)) if:
	∀ c>0, ∃ n₀>0 such that
	∀ n>=n₀ : 0 <= c.g(n) < f(n).

For example n²/2=ω(n), but n²/2≠ω(n²).
                   f(n)
f(n)=ω(g(n)) ⇒ lim ---- = ∞
               n→∞ g(n)

COMPARISON OF FUNCTIONS
=======================

Transitivity
------------
f(n) = X(g(n)) and g(n) = X(h(n)) ⇒ f(n)=X(h(n))
where X can be Θ, O, Ω, o or ω.

Reflexivity
-----------
f(n) = X(f(n))
where X can be Θ, O, or Ω.

Symmetry
--------
f(n) = Θ(g(n)) ⟺  g(n) = Θ(f(n))

Transpose symmetry
------------------
f(n) = O(g(n)) ⟺  g(n) = Ω(f(n))
f(n) = o(g(n)) ⟺  g(n) = ω(f(n))

EXAMPLES WITH COMMON FUNCTIONS
==============================

Polynomials
-----------
       d
p(n) = ∑ a(i).n^i     with a(d) > 0
      i=0

p(n) = Θ(n^d)

A function f(n) os polynomially bounded if f(n)=n^(O(1)), which is equivalent to f(n)=O(n^k) for some constant k.

Exponentials
------------

∀ a∈R and b∈R, with a > 1:
    n^b
lim --- = 0
n→∞ a^n

⟹  n^b = o(a^n)

Which means that any exponential function grows faster than any polynomial.

Logarithms
----------
We say that a function f(n) is polylogarithmically bounded if f(n)=logO(1)(n).
      logb(n)          logb(n)
lim ------------ = lim ------- = 0
n→∞ 2^(a.log(n))   n→∞   n^a

⟹  logb(n) = o(n^a) ∀a>0.

Which means that any positive polynomial function grows faster than any polylogarithmic function.

Factorials
----------

From Stirling's approximation :  n! = √(2πn).(n/e)^n.(1+Θ(1/n))
we can prove:
n! = o(n^n)
n! = ω(2^n)
log(n!) = Θ(n.log(n))
===================== basis/concrete-problems.txt
CONCRETE PROBLEMS

See encodings.txt.

A concrete problem is a problem whose instance set is the set of binary strings.

An algorithm solves a concrete problems in time O(T(n)) if, when it is provided a problem instance i of length n=|i|, the algorithm can produce the solution in at most O(T(n)).
Therefore, a concrete problem is polynomial-time solvable if there exists an algorithm to solve it time O(n^k) for some constant k.

Mapping of abstract problems to concret problems
------------------------------------------------
Given an abstract decision problem Q mapping an instance I to {0,1}, an encoding
e : I → {0,1}*
can be used to induce a related concrete decision problem, which we denote by e(Q).

If the solution to an abstract problem instance i∈I is Q(i)∈{0,1}, then the solution to the concret problem instance e(i)∈{0,1}* is also Q(i).
It may be that some binary strings reprenset no meaningful abstract-problem instance. We assume that such strings are mapped to 0.
===================== basis/encodings.txt
ENCODINGS OF INPUTS

For a computer program to solve an abstract problem, problem instances must be represented in a way that the program understands. This is what we call an encoding.

An encoding of a set S of abstract objects is a mapping of e from S to the set of binary strings.
For example the set of natural numbers N is mapped to binary strings as follow:
0 -->  0
1 -->  1
2 --> 10
3 --> 11
...

We write e(17) = 10001.

Even a compound object (polygon, graph, function, ordered pair, program) can be encoded as a binary string by combining the representations of its constituent parts.

UNARY ENCODING
==============
n (non-negative)    n (strictly positive)   Unary code      Alternative
0                   1                       0               1
1                   2                       10              01
2                  	3                       110             001
3                  	4                       1110            0001
4                  	5                       11110           00001
5                  	6                       111110          000001
6                  	7                       1111110         0000001
7                  	8                       11111110        00000001
8                  	9                       111111110       000000001
9                  	10                      1111111110      0000000001

STANDARD ENCODING
=================
In practice we use reasonable, concise fashion encodings (i.e. standard), such as binary or base-3.
We note the standard encoding of an object O : <O>.
So for example, the standard encoding of a graph G will be noted <G>.
===================== basis/formal-language.txt
FORMAL-LANGUAGE THEORY

An alphabet Σ is a finite set of symbols.

A language L over Σ is any set of strings made up of symbols from Σ.
For example: if Σ={0,1}, the set L={10,11,101,111,...} is the language of binary representations of prime numbers.

We denote the empty string by ε and the empty language by ∅.

The language of all strings over Σ is denoted Σ*.
For example, if Σ={0,1}, then Σ*={ε,0,1,00,01,10,11,000,...} is the set of all binary strings.

Every language L over Σ is a subset of Σ*.

                       _
The complement of L is L = Σ* - L.

The concatenation of two languages L₁ and L₂ is the language L = {x₁x₂: x₁∈L₁ and x₂∈L₂}.

The closure or Kleene star of a language L is the language L*={ε} ∪ L ∪ L² ∪ L³ ∪ ..., where L^k is the language obtained by concatenating L to itself k times.

LANGUAGES AS INPUTS TO ALGORITHMS
=================================
An algorithm A _accepts_ a string x∈{0,1}* if, given input x, the algorithm outputs A(x)=1.
The language _accepted_ by an algorithm A is the set L = {x∈{0,1}*: A(x)=1}.
An algorithm A _rejects_ a string x if A(x)=0.

A language L is _decided_ by an algorithm A if every binary string is either accepted or rejected by the algorithm.

A language L is accepted in polynomial time by an algorithm A if for any length-n string x∈L, the algorithm accepts x in time O(n^k) for some constant k.

A language L is decided in polynomial time by an algorithm A if for any length-n string x∈{0,1}*, the algorithm decides x in time O(n^k) for some constant k.

Thus, to accept a language, an algorithm need only worry about strings in L, but to decide a language, it must accept or reject every string in {0,1}*.

COMPLEXITY CLASS
================
A complexity class is a set of languages, membership in which is determined by a complexity measure, such as running time, on an algorithm that determines wether a given string x belongs to language L.
===================== basis/halting problem.txt
The halting problem: "Given a description of an arbitrary computer program, decide whether the program finishes running or continues to run forever".

Alan Turing proved in 1936 that a general algorithm to solve the halting problem for all possible program-input pairs cannot exist. To be more precised: the halting problem is undecidable over Turing machines.
===================== basis/optimization-problems.txt
OPTIMIZATION PROBLEMS

Many abstract problems are optimization problems in which some value must be minimized or maximized.

An optimization problem can be recast as a decision problem by imposing a bound on the value to be optimized.
The PATH example above uses the bound k to transform the optimization problem SHORTEST-PATH into a decision problem.

Since NP-completeness (see NP-problems.txt) theory deals with decision problems, we have to recast any optimization problem into a decision problem when we want to use this theory.
This is not an issue, since if we can provide evidence that a decision problem is hard, we also provide evident that its related optimization problem is hard.

===================== basis/problem-reducibility.txt
A problem Q can be reduced to another problem Q' if any instance of Q can be "easily rephrased" as an instance of Q', the solution to which provides a solution to the instance of Q.

For example:
the problem of solving ax+b=0 can be transformed into 0x²+ax+b=0, whose solution provides a solution for ax+b=0.

We say that a language L₁ is polynomial-time reducible to a language L₂, written L₁≤pL₂, if there exists a polynomial-time computable function
f:{0,1}*⟶ {0,1}* such that for all x∈{0,1}*,
   x∈L₁ ⟺  f(x)∈L₂.

We call the function f the reduction function, and a polynomial-time algorithm F the computes f is called a reduction algorithm.

Lemma: if L₁,L₂⊆{0,1}* ar languages such that L₁≤pL₂, then L₂∈P implies L₁∈P.
===================== bio/NCBI.txt
http://www.ncbi.nlm.nih.gov

Entrez Programming Utilities Help
=================================
http://www.ncbi.nlm.nih.gov/books/NBK25501/

List all available databases:
http://eutils.ncbi.nlm.nih.gov/entrez/eutils/einfo.fcgi
# 2014-01-21: pubmed protein nuccore nucleotide nucgss nucest structure genome assembly genomeprj bioproject biosample blastdbinfo books cdd clinvar clone gap gapplus dbvar epigenomics gene gds geoprofiles homologene medgen journals mesh ncbisearch nlmcatalog omia omim pmc popset probe proteinclusters pcassay biosystems pccompound pcsubstance pubmedhealth seqannot snp sra taxonomy toolkit toolkitall toolkitbook unigene unists gencoll

Getting information about a database:
http://eutils.ncbi.nlm.nih.gov/entrez/eutils/einfo.fcgi?db=gene

Getting full information in XML format:
http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=gene&id=3627&rettype=xml&retmode=text


http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&rettype=fasta&retmode=text&id=224589816
http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&rettype=fasta&retmode=text&id=224589816&from=76941905&to=76945051

NUCCORE DATABASE
================
http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&rettype=fasta&retmode=text&id=224589816&from=76941905&to=76945051

http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&id=224589816&strand=1&seq_start=76941905&seq_stop=76945051&rettype=fasta&retmode=text

http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&id=224589816&strand=2&seq_start=76941905&seq_stop=76945051&rettype=fasta&retmode=text

strand option : Strand of DNA to retrieve. Available values are "1" for the plus strand and "2" for the minus strand.
===================== bio/Transcriptomic.txt
Transcriptomic (o?) = détection des ARNm.

Les appareils (appelés scanners) pour un set de gènes (codés sur une puce) sortent un tableau comme suit (on passe chq échantillon sur une seule puce):

			Échantillon de contrôle     Échantillon d'expérience
			Signal  Pvalue              Signal      Pvalue
gène1       150     10^-5               300         10^-1
gène2       ...     10^-1               2           10^-4
...         ...     ...                 ...         ...
gène45000   ...     ...                 ...         ...

--> On ne garde que les gènes dont le Pvalue < 10^-3 ou 5.10^-2.
La Pvalue, calculée par l'appareil, est considérée comme une variable indépendante de l'appareil. Elle n'est plus utilisée par la suite ==> elle est uniquement utilisée pour le filtrage de départ.

Après filtrage Pvalue, on se retrouve en général avec 2000~4000 gènes.

On utilise ensutie MeV pour clusturiser les gènes, puis fais une PCA sur les clusters.
===================== design/divide-and-conquer.txt
1) DIVIDE the problem into a number of subproblems.
2) CONQUER the subproblems by solving them recursively.
   If the subproblems sizes are small enough, however, just solve the subproblems in a straightforward manner.
3) COMBINE the solutions to the subproblems into the solution for the original problem.

The merge-sort algorithm is an example of divide-and-conquer algorithm.
===================== design/incremental.txt
An incremental algorithm is an algorithm that does its job on a subpart of data (say data from 1..i-1) and then take datum ith to process it and form subpart (1..i), until all n elements are processed.

The insertion-sort and the direct-sort algorithms are examples of incremental algorithms.
===================== maths/calc.txt
===================== maths/matrix_inversion.txt
2014-01-22 LISTE CALCUL
=======================
votre matrice est-elle creuse ou dense?
Si dense: un lapack (séquentiel) ou scalapack (parallèle) utilisant un BLAS optimisé si possible (mkl pour intel, acml pour amd, veclib pour apple, ESSL pour IBM, ...)
Si creuse: MUMPS, PasTIX, SuperLU, HSL sont des solutions //,     umfpack est pas mal est séquentiel.
===================== multi-agent/GAMA.txt
plateforme de simulation multi-agent GAMA (info par GIL-QUIJANO Javier, CEA)
http://code.google.com/p/gama-platform/

Vidéo de démonstration: http://youtu.be/6m_-UY8UBuk
===================== recursion/recursion.txt
A recursion algorithm can always be flattened into an iterative algorithm.

The recursion mechanism uses the stack to store the state of each recursion call. Thus to remove recursion we just have to handle ourselves a stack to record the state at each step of the iteration.
===================== recursion/tail recursion.txt
Tail recursion (récursivité terminale en français)

Tail recursion is an elegant way for doing iteration programming inside a
functional language.

The idea is that the last instruction executed inside the function must be a
call to itself only. This way no variable has to be memorized on to the stack,
and the current stack memory used by the current function can be reused when it
calls itself. The stack is thus never growing, and one hasn't to worry about
stack overflow.
In practice the recursive call is rewritten by the compiler or the interpreter
into an iteration (so a linear code). Thus in a procedural language, tail
recursion has no sense, since one can directly write itself the iteration loop.

Languages implementing tail recursion optimization:
_ functional languages
_ Python

Examples in scheme:
factorial.scm
factorial_no_tail.scm
fibo_tail.py
fibo_iter.py
===================== trees/mst.txt
[From "Introduction to Algorithms", chapter 24, by Cormen, Leiserson and Rivest)

MINIMUM SPANNING TREES

Assume a graph G=(V,E), where V is a set of vertices, and E is a set of edges between pairs of vertices,
and a weight w(u,v) specifying the cost to connect u and v.

We wish to find a acyclic subset T⊆E that connects all of the vertices and whose total weight

w(T) =    ∑    w(u,v)
	   (u,v)∈T

is minimized.

T is acyclic => it forms a tree
T connects all vertices => it spans the graph G
So we call it a spanning tree.

The problem is called minimum-spanning-tree problem.

GREEDY METHOD
=============

MST-KRUSKAL
===========

Runs in O(E.log2(V)) using ordinary binary heap.

MST-PRIM
========

Runs in O(E.log2(V)) using ordinary binary heaps,
  or in O(E + V.log2(V)) using Fibonacci heaps, which is better if |V|<|E|.
===================== uncertainties/cadna.txt
http://www-pequan.lip6.fr/cadna//

http://www.lip6.fr/production/logiciels-fiche.php?RECORD_KEY(logiciels)=id&id(logiciels)=1
===================== uncertainties/openturns.txt
http://www.openturns.org
===================== uncertainties/uranie.txt
http://www.sciencedirect.com/science/article/pii/S1877042810013078

CEA/DEN
