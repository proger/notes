# Licensing

 * [Comparison of free and open-source software licenses](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses).
 * [Licenses](http://choosealicense.com/licenses/).

 * [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html). Forced to publish source code if the application is accessible on the web. See [GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.en.html).
