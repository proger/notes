# Visual Studio Code (VSCode)

 * [Download](https://code.visualstudio.com/download#).
 * [Hex Editor](https://marketplace.visualstudio.com/items?itemName=ms-vscode.hexeditor).

 * GitHub Copilot: VSCode AI. Linked with GitHub account.

The System Installation package can be installed on wine, however the program crashes at startup.
Packages for RedHat and Debian are provided, as well as macOS.

Install:
```sh
yay -S visual-studio-code-bin
```

## Bash

 * [ShellCheck](https://marketplace.visualstudio.com/items?itemName=timonwong.shellcheck).
 * [Bash Debug](https://marketplace.visualstudio.com/items?itemName=rogalmic.bash-debug).

 * [Terminal Shell Integration](https://code.visualstudio.com/docs/terminal/shell-integration).
