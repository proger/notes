# Booting

## BIOS

 * [Motherboard Flash Boot CD from Linux Mini HOWTO](http://www.nenie.org/misc/flashbootcd.html).

DELL computer:
 * F2 for entering setup.
 * F12 for entering boot menu.
Press key when DELL logo appears.

## EFI

 * [EFI 1.1 Shell Command Reference Manul](https://manuais.iessanclemente.net/images/a/a6/EFI-ShellCommandManual.pdf).
 * [UEFI Shell commands](https://techlibrary.hpe.com/docs/iss/proliant_uefi/UEFI_TM_030617/GUID-D7147C7F-2016-0901-0A6D-000000000E1B.html).

Display attributes of directory:
```bash
attrib fs0:\
```

Change current filesystem:
```sh
fs0:
```

List directory content:
```sh
ls
```

Change current directory:
```bash
cd mydir
```

Get help:
```sh
help
help mycommand
```

Remove a file:
```sh
rm myfile
```

Redirect an output:
```sh
help > help.txt
```

### Booting with EFI

mkdir /mnt/boot
mount /dev/sda1 /mnt/boot

```sh
UUID=$(blkid /dev/sda2 | sed 's/^.*PARTUUID="\(.*\)"$/\1/') # Get Linux disk UUID
```

EFI boot, see <https://wiki.archlinux.org/index.php/EFISTUB>.
```sh
echo "\\vmlinuz-linux root=PARTUUID=$UUID rw initrd=\\initramfs-linux.img" >/boot/archlinux.nsh # Write script for booting from EFI shell.
cp /boot/archlinux.nsh /boot/startup.nsh

```

### EFI in VirtuaBox

In VirtualBox with EFI:

 * Enable EFI in VirtualBox VM: Settings -> System -> Enable EFI.
 * Boot on ArchLinux ISO CD install.
```sh
fdisk -l # Look for the device name of the harddrive (should be `/dev/sda`).
fdisk /dev/sda # Make 3 partitions:
  # Press `g` for creating a GPT partition
  # /dev/sda1: EFI System (type 1), 512MB
  # /dev/sda2: Linux filesystem (type 20)
  # /dev/sda3: Linux swap (type 19)
  # Press `w` to write partition table.
mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mkswap /dev/sda3
mount /dev/sda2 /mnt
mkdir /mnt/boot 
mount /dev/sda1 /mnt/boot
swapon /dev/sda3
vi /etc/pacman.d/mirrorlist
pacstrap /mnt base
genfstab -U -p /mnt >> /mnt/etc/fstab
arch-chroot /mnt
vi /etc/locale.gen
echo LANG=en_IE.UTF-8 > /etc/locale.conf
locale-gen
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc
echo "archlinux.host.local" > /etc/hostname
passwd # Define root password
systemctl enable dhcpcd
UUID=$(blkid /dev/sda2 | sed 's/^.*PARTUUID="\(.*\)"$/\1/') # Get Linux disk UUID

 # EFI boot, see https://wiki.archlinux.org/index.php/EFISTUB
echo "\\vmlinuz-linux root=PARTUUID=$UUID rw initrd=\\initramfs-linux.img" >/boot/archlinux.nsh # Write script for booting from EFI shell.
cp /boot/archlinux.nsh /boot/startup.nsh

 # For booting on LVM ? Is bootloader like GRUB necessary ?

exit
reboot
```

## grub

Config file is `/etc/default/grub`.

Once modified run:
```sh
grub-mkconfig -o /boot/grub/grub.cfg
```

To show boot progress, remove `quiet` option from `GRUB_CMDLINE_LINUX_DEFAULT`.

To enable resume from hibernation inside a swap file on a dm-crypt encrypted
root partition:
```conf
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 resume=/dev/mapper/luks_root resume_offset=34816"
```
The offset is the physical offset of the swap file on the encrypted partition
mapped on `/dev/mapper/luks_root`.
