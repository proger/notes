# System

Get system info:
```sh
neofetch
```

## Interruptions

 * <https://faculty.cs.niu.edu/~hutchins/csci480/signals.htm>
 * <https://en.wikipedia.org/wiki/Signal_(IPC)>

 - `SIGKILL` **--> cannot be caught**
 - `SIGSTOP` & `SIGCONT`? **--> cannot be caught**
 - `SIGQUIT` --> terminate + core dump
 - `SIGHUP` --> when controling terminal is closed.
 - `SIGPIPE` --> when writing on a broken pipe (No more procress is reading the pipe).
 - `SIGINT`: terminal interrupt signal (CTRL+C).
 - `SIGTERM`: termination signal
