# Packaging & dependency managers
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','r':''} -->

## R
### CRAN

Submitting to [CRAN](http://cran.r-project.org/):
 * [Getting your R package on CRAN](http://kbroman.org/pkg_primer/pages/cran.html).
 * [CRAN Repository Policy](https://cran.r-project.org/web/packages/policies.html).
  + [Submit a package to CRAN](https://cran.r-project.org/submit.html).
 * [Writing R Extensions](https://cran.r-project.org/doc/manuals/r-release/R-exts.html).

**RECOMMENDATIONS**
Do **not change** user's:
 * options (see `options()` function)
 * par (see `par()` function).
 * working directory.

During tests, examples and vignettes, do **not write** into:
 * Home directory
 * Cache directory.
 * Config directory.
Use temporary folder instead.

Network access during tests:
 * Do not fail (neither warning nor error) if an Internet resource is not available ==> check availability of Internet resource before running test.
 * Good practice is to test without network inside a Docker container: `RUN --network=none make test`.
 * How to simulate network failure on GitLab CI?

Write an example for each class and method.

For `par()` and `options()`, use `on.exit()` to reset old configuration before
leaving a function:
```r
x <- par(...)
on.exit(par(x))
...
par(x)
```

Select CRAN mirror:
```r
chooseCRANmirror()
chooseCRANmirror(graphics = FALSE) # Avoid opening of X11 window when selecting mirror site:
```
CRAN mirror can be set permanently inside `.Rprofile` configuration file. See <https://stackoverflow.com/questions/8475102/set-default-cran-mirror-permanent-in-r>.

#### Creating packages

 * [Programmation en R : incorporation de code C et création de packages, Sophie Baillargeon, Université Laval](http://www.math.univ-montp2.fr/~pudlo/documents/ProgR_AppelC_Package_210607.pdf).
 * [Creating R Packages: A Tutorial, Friedrich Leisch](https://cran.r-project.org/doc/contrib/Leisch-CreatingPackages.pdf).
 * [Writing R Extensions](https://cran.r-project.org/doc/manuals/r-release/R-exts.html).
 * [Who Did What? The Roles of R PackageAuthors and How to Refer to Them](https://journal.r-project.org/archive/2012-1/RJournal_2012-1_Hornik~et~al.pdf).

 * [How to Install R Packages using devtools on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-r-packages-using-devtools-on-ubuntu-16-04).

 * `R CMD check`: [Automated checking](http://r-pkgs.had.co.nz/check.html).
 * How to write tests for a package: [Writing tests](http://kbroman.org/pkg_primer/pages/tests.html)
 * [Package Development Prerequisites](https://support.rstudio.com/hc/en-us/articles/200486498-Package-Development-Prerequisites).

##### DESCRIPTION file

 * [DESCRIPTIONfile](https://r-pkgs.org/description.html).

Authors:
```r
Authors@R: c(person("John", "Doe",
    email="john.doe@someplace.zz", role=c("aut")),
    person("Jack", "Smith", email="jack.smith@someplace.zz",
    role=c("aut", "cre")))
```
Roles are:
 * `aut` First author, and other people that did main contributions like
   building the package, translating the code, etc.
 * `cre` Maintainers.
 * `ctb` Minor contributors.
 * `trl` Translators of code into R language.

#### Vignettes

 * [Writing package vignettes](http://cran.fhcrc.org/doc/manuals/R-exts.html#Writing-package-vignettes).
 * [R Markdown Cookbook](https://bookdown.org/yihui/rmarkdown-cookbook/).
 * [bookdown: Authoring Books and Technical Documents with R Markdown](https://bookdown.org/yihui/bookdown/).
 * [Vignettes](https://r-pkgs.org/vignettes.html).
 * [Package vignettes](https://cran.r-project.org/web/packages/knitr/vignettes/knitr-markdown.html).

Create a vignette in a package:
```
R -e 'usethis::use_vignette("my-vignette")'
```

See package `knitr` for including code inside a vignette.

On macOS, for building vignettes with `R CMD build .`, install first the
[MacTeX LaTeX](http://www.tug.org/mactex/) distribution.

Example of header for an Rmd vignette using knitr package and BiocStyle package:
```rmd
 ---
 title: "Configuring biodb"
 author: "Pierrick Roger"
 output:
   BiocStyle::html_document:
 #    toc_float: true    # TOC implies enabling a theme.
     theme: null         # A theme uses around 700KB.
 #    highlight: null     # Highlighting uses around 60KB.
 #    mathjax: null
 package: biodb
 abstract: |
   How to configure biodb package.
 vignette: |
   %\VignetteIndexEntry{Configuring biodb}
   %\VignetteEngine{knitr::rmarkdown}
   %\VignetteEncoding{UTF-8}
 ---
```

Include other `.Rmd` files inside main `.Rmd` using fake empty inserted code: `{r, child=c('one.Rmd', 'two.Rmd')}`.

Do not evaluate R code: `{r, eval=TRUE}`.

####  2023-10-13 CRAN reviewer message

```mail
From: Victoria Wimmer <vwimmer@wu.ac.at>
To: Pierrick Roger <proger@cnrgh.fr>
Date: Fri, 13 Oct 2023 17:41:06 +0200
Subject: Re: CRAN Submission sched 0.1.0
Dear Pierrick,

If there is no specific reference describing your package, it is fine if 
you don't add one.

If you want to add a reference to a Bioconductor package, you can simply 
add it by putting the link into the DESCRIPTION text in the form:
authors (year) <https:...>
with no space after 'https:' and angle brackets for auto-linking.
(If you want to add a title as well please put it in quotes: "Title")

Kind regards,
Victoria Wimmer

Am 12.10.2023 um 10:30 schrieb Pierrick Roger:
> Hello Victoria,
>
> Thank you for your answer.
> I will add an example for each of the methods of the Rule class.
>
> I do not have any article reference to add. However this package is
> going to be used by a Bioconductor package I maintain.
> Indeed this `sched` package I propose is only an extraction of code
> included in the Bioconductor package. This code is better suited to be
> inside CRAN and reused for another package I have in mind that will be
> in CRAN too.
> Is it possible to put a reference to the Bioconductor package inside the
> DESCRIPTION file? How?
>
> Best regards,
> Pierrick
>
> On Thu 12 Oct 23  5:45, Victoria Wimmer wrote:
>> Thanks,
>>
>> If there are references describing the methods in your package, please
>> add these in the description field of your DESCRIPTION file in the form
>> authors (year) <doi:...>
>> authors (year) <arXiv:...>
>> authors (year, ISBN:...)
>> or if those are not available: <https:...>
>> with no space after 'doi:', 'arXiv:', 'https:' and angle brackets for
>> auto-linking.
>> (If you want to add a title as well please put it in quotes: "Title")
>>
>> Please add small executable examples in your Rd-files to illustrate the
>> use of the exported function but also enable automatic testing.
>>
>> Please fix and resubmit.
>>
>> Best,
>> Victoria Wimmer
>>
>> Am 11.10.2023 um 13:55 schrieb CRAN Package Submission Form:
>>> [This was generated from CRAN.R-project.org/submit.html]
>>>
>>> The following package was uploaded to CRAN:
>>> ===========================================
>>>
>>> Package Information:
>>> Package: sched
>>> Version: 0.1.0
>>> Title: Request Scheduler
>>> Author(s): Pierrick Roger [aut, cre]
>>>     (<https://orcid.org/0000-0001-8177-4873>)
>>> Maintainer: Pierrick Roger <pierrick.roger@cea.fr>
>>> Depends: R (>= 4.1)
>>> Suggests: roxygen2, testthat (>= 2.0.0)
>>> Description: Offers classes and functions to contact webservers while
>>>     enforcing scheduling rules required by the sites.
>>> License: AGPL-3
>>> Imports: R6, chk, lgr
>>>
>>>
>>> The maintainer confirms that he or she
>>> has read and agrees to the CRAN policies.
>>>
>>> =================================================
>>>
>>> Original content of DESCRIPTION file:
>>>
>>> Package: sched
>>> Title: Request Scheduler
>>> Version: 0.1.0
>>> Authors@R: c(person("Pierrick", "Roger", email="pierrick.roger@cea.fr", role=c("aut", "cre"), comment=c(ORCID="0000-0001-8177-4873")))
>>> Maintainer: Pierrick Roger <pierrick.roger@cea.fr>
>>> Description: Offers classes and functions to contact webservers while enforcing scheduling rules required by the sites.
>>> URL: https://gitlab.com/dbapis/r-sched
>>> BugReports: https://gitlab.com/dbapis/r-sched
>>> Depends: R (>= 4.1)
>>> License: AGPL-3
>>> Encoding: UTF-8
>>> Suggests: roxygen2, testthat (>= 2.0.0)
>>> Imports: R6, chk, lgr
>>> NeedsCompilation: no
>>> Roxygen: list(markdown = TRUE)
>>> RoxygenNote: 7.2.3
>>> Collate: 'Rule.R' 'package.R'
>>> Packaged: 2023-10-11 10:15:26 UTC; pierrick
>>> Author: Pierrick Roger [aut, cre] (<https://orcid.org/0000-0001-8177-4873>)
>>>
```

### renv

 * [renv: Project Environments for R](https://posit.co/blog/renv-project-environments-for-r/).

Install:
```r
install.packages("renv")
# Then quit R and relaunch it
# and init your project:
renv::init()
```

Activate the current directory as the project:
```r
renv::activate()
```
Must restart R session.

Status of the project:
```r
renv::status()
```

Record the current state of the project:
```r
renv::snapshot()
```

Restore the state of the project:
```r
renv::restore()
```

Install a package:
```r
renv::install(...)
```

Analyse dependencies from code files:
```r
renv::dependencies()
```
Get list of dependencies:
```r
unique(renv::dependencies()$Package)
```

## Homebrew / brew

 * <http://mxcl.github.com/homebrew/>.
 * <https://github.com/mxcl/homebrew/>.
 * [Homebrew Multi User Setup](https://medium.com/@leifhanack/homebrew-multi-user-setup-e10cb5849d59). --> Old versions of brew, does not work anymore.

On ARM HomeBrew is installed in `/opt/homebrew` by default, thus MacPorts can
safely be installed along since it installs inside `/opt/local/`.

For installing in `/usr/local`:
```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.github.com/gist/323731)"
```

For installing elsewhere:
```bash
mkdir homebrew && curl -L https://github.com/mxcl/homebrew/tarball/master | tar xz --strip 1 -C homebrew
```

Install a package:
```bash
brew install <package>
brew install --HEAD <package>	# to install with the developpement version (HEAD) of the brew formulae (ruby file)
brew install --build-from-source <package> # force installing from sources, even if a bottle version is available.
```

List installed packages:
```sh
brew list
```

Get installation prefix:
```sh
brew --prefix
```

Installing from another formulæ repository:
```bash
brew install homebrew/science/myformula
```
or
```bash
brew tap homebrew/science
brew install myformula
```

Extract sources into a new local dir:
```bash
brew unpack my.formula
brew unpack -p my.formula   # Apply also patches.
```

Get cache path:
```bash
brew --cache
```

Get all installed packages that depend on another package:
```bash
brew uses --installed mypkg
```

List all files of a package:
```bash
brew list mypkg
```

Install Xcode command line tools (needed):
```bash
xcode-select --install
```

### Maintenance tasks

Clean unused old packages:
```bash
brew cleanup
```

Run diagnostics:
```bash
brew doctor
```

Update packages information:
```bash
brew update
```

Upgrade installed packages:
```bash
brew upgrade
```

### Cask

Install third party applications, in binary (Goole Earth, VLC, ...)

Clean cask:
```bash
brew cask cleanup
```

### Writing a formula

 * [Contributing to Homebrew](https://github.com/Homebrew/homebrew-core/blob/master/.github/CONTRIBUTING.md).
 * [Formula Cookbook](https://github.com/Homebrew/brew/blob/master/share/doc/homebrew/Formula-Cookbook.md).
 * [Class: Formula](http://www.rubydoc.info/github/Homebrew/brew/master/Formula).
 * [How To Open a Homebrew Pull Request (and get it merged)](https://github.com/Homebrew/brew/blob/master/share/doc/homebrew/How-To-Open-a-Homebrew-Pull-Request-(and-get-it-merged).md).

You may encounter the following error while running `brew tests`:
```
1) Failure:
LanguageModuleRequirementTests#test_good_ruby_deps [/usr/local/Library/Homebrew/test/test_language_module_requirement.rb:51]:
Expected #<LanguageModuleRequirement: "languagemodule" [:ruby, "date", nil]>
 to be satisfied?
```
In that case, check if you have installed ruby using Homebrew. If that is the case, uninstall it. `brew` will then use the macOS ruby.

## Conda

 * [Installation](https://conda.io/docs/user-guide/install/macos.html).
 * [Installing and updating conda build](https://conda.io/docs/user-guide/tasks/build-packages/install-conda-build.html#install-conda-build).
 * [Conda-forge](https://conda-forge.org/).

For installing miniconda see [Miniconda](https://conda.io/miniconda.html). On macos you can also use Homebrew:
```sh
brew cask install miniconda
```

Installing anaconda:
```sh
brew cash install anaconda
```

Updating conda:
```sh
conda update -n base -c defaults conda
```

Create a new environment, installing a package
```sh
conda create -n <myenv> <mypkg>
conda create -n test_r3.4 r=3.4
```

Delete an environment:
```sh
conda env remove -n my_env
```

Enter environment:
```sh
source activate myenv
```

Quit environment:
```sh
source deactivate
```

Installing a precise version of R:
```sh
conda install -c r r=3.3.2
```

Searching for packages:
```sh
conda search -c bioconda r-biodb
```

Generating a new recipe with the `skeleton` command:
```sh
conda install conda-build # You first need to install *conda-build*.
conda skeleton cran mypkg
```

### Bioconda recipes

 * [Contributing a recipe](https://bioconda.github.io/contribute-a-recipe.html).

## pixi

 * [Getting Started](https://pixi.sh/latest/).

A package manager / virual environment / project manager for development.
Uses conda behind.
VSCode Python plugin selects pixi automatically when available.

## Python

 * [How to manage multiple Python versions and virtual environments](https://www.freecodecamp.org/news/manage-multiple-python-versions-and-virtual-environments-venv-pyenv-pyvenv-a29fb00c296f/).

Getting list of installed modules:
```sh
python -c "help('modules')"
```

Old way:
```sh
easy_install <package>
```

### poetry

A pure Python project manager / package manager / virtual environment.

 * [Poetry](https://python-poetry.org/).

Install:
```sh
pipx install poetry
```

Create a new project:
```sh
poetry new myproject
```
The folder `myproject` must not exist or be empty.
It will create the following hierarchy:
```
myproject
├── myproject
│   └── __init__.py
├── pyproject.toml
├── README.md
└── tests
    └── __init__.py
```

To initialize (interactively) a non-empty folder:
```sh
cd myproject
poetry init
```
It creates only the `pyproject.toml` file.

### venv

 * [venv — Creation of virtual environments](https://docs.python.org/3/library/venv.html).

The `venv` package is included in Python since version 3.3.
Creating a virtual environment with `venv`:
```sh
python3 -m venv myvenv
source myvenv/bin/activate
...
deactivate
```
Only the current installed version of Python can be used.

To use a specific version of Python, `pyenv` must be installed.
Example of usage:
```sh
pyenv shell 3.6.10
python3 -m venv myvenv
source myvenv/bin/activate
...
deactivate
pyenv shell --unset
```

### pyenv-virtualenv

Used to create virtual environment with Python 2.

### virtualenv

 * [Installing virtualenv](http://virtualenv.readthedocs.org/en/latest/installation.html).
 * [Elegantly activating a virtualenv in a Dockerfile](https://pythonspeed.com/articles/activate-virtualenv-dockerfile/).

A more powerful version of `venv`, that detects automatically installed
versions of Python.

```sh
pip install virtualenv
```

Create a virtual env:
```sh
virtualenv my/venv/dir
```

Specify a python version:
```sh
virtualenv -p python2.7 my/venv/dir
```

Activating a virtual env:
```sh
source my/venv/dir/bin/activate
```

Deactivating a virtual env:
```sh
deactivate
```

### Requirements file `requirements.txt`

Add a GitHub repos:
```
goodmeasure@git+http://10.0.238.20/pr228844/goodmeasure
```

To specify a GitHub repos with a particular branch in `requirements.txt`, for 
editing mode (module is extracted inside `src` subfolder):
```
-e git://github.com/myaccount/myrepos.git@mybranch#egg=mypkg
```

About the format of the version specification, see
[pip install](https://pip.pypa.io/en/stable/cli/pip_install/).

### pip

New way with `pip`:
```sh
easy_install pip
pip install <package>
pip uninstall <package>
```

It is better to explicitly run pip through the python executable.
This ensures that packages are installed for the right version and installation
of python:
```sh
python -m pip install mypkg
```

In Homebrew, type `brew info python` for more information.

Upgrade `setuptools` & `pip`:
```sh
pip install --upgrade setuptools
pip install --upgrade pip
```

Install all modules in a requirements file:
```sh
pip install -r requirements.txt
```

See [Installing Python Modules](https://docs.python.org/2/install/) for installing packages in the user home directory. For installing in `~/.local`, use `--user` option:
```sh
pip install --user mymodule
```

How to install from a GitHub repos:
```sh
pip install --user git+https://github.com/ISA-tools/isa-api@develop
```

Install a specific version of module:
```sh
pip install mymodule==1.1.4
```

To install a package with its dependencies from inside its source folder:
```sh
python -m pip install .
```
To install only the dependencies of a package:
```sh
python -m pip install -e .
```
To install optional dependencies of a package, use the name of the optional dependencies:
```sh
python -m pip install -e .[tests]
```
when in `pyproject.toml`:
```toml
[project.optional-dependencies]
tests = [
    'pytest'
]
```

List installed packages:
```sh
pip list
```

Installing a package from Test PyPI, with dependencies from PyPI:
```sh
python -m pip install -i https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple/ mypkg
```

### pipx

 * [pipx](https://pypi.org/project/pipx/).

Install Python applications from PyPI (using pip) inside isolated environments.

Each application has its own isolated environment and thus is not affected by python or pip upgrades.

Installing a package from Test PyPI, with dependencies from PyPI:
```sh
python -m pipx install -i https://test.pypi.org/simple/ --pip-args "--extra-index-url https://pypi.org/simple/" mypkg
```

Install a Python application for local user only (install in `~/.local`):
```sh
pipx install mypkg
```

Install a Python application (from a standard package) for all users:
```sh
sudo PIPX_HOME=/opt/pipx PIPX_BIN_DIR=/usr/local/bin pipx install mypkg
sudo chmod -R a+rX /opt/pipx
``
To uninstall:
```sh
sudo PIPX_HOME=/opt/pipx PIPX_BIN_DIR=/usr/local/bin pipx uninstall mypkg
```

### Building a package

#### Writing setup.py and setup.cfg

`setup.py`:
 * [Writing the Setup Script](https://docs.python.org/3/distutils/setupscript.html).
 * [Differences between distribute, distutils, setuptools and distutils2?](https://stackoverflow.com/questions/6344076/differences-between-distribute-distutils-setuptools-and-distutils2).

`setup.cfg`:
 * [Writing the Setup Configuration File](https://docs.python.org/3/distutils/configfile.html).
 * [Configuring setup() using setup.cfg files](https://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files).

Several packages exist to write a `setup.py` script:
 * `distutils.core`: standard, included with Python.
 * `setuptools`: not standard, not included with Python, created to overcome `distutils` limitations, come with `easy_install` script.

When writing a package, one must define a `setup.py` in which options are set:
```python
from setuptools import setup
setup(
    name='sampleproject',
    version='1.3.1',
    ...
)
```
Or define a minimalist `setup.py`:
```python
from setuptools import setup
setup()
```
And put everything inside a `setup.cfg` file:
```cfg
[metadata]
name = cea
version = 0.1.0
...
```

#### setup.py

Installing a new module with `setup.py`:
```sh
tar -xzf some-package.tar.gz
cd some-package
python setup.py install
```
!!! --> Install a `.egg` in `site-packages` folder, but not the module folder, and `import` does not work.
Installing with `pip` works:
```sh
python3 -m pip install .
```

Running tests (**DEPRECATED**: use `tox` instead):
```sh
python setup.py test
python setup.py test --addopts '-k MyClass' # With arguments
```

#### Packages

A package is a directory containing module files or other package directories.
It also contains a `__init__.py` file, which in its simplest form is an empty file.

Importing a module defined inside a package:
```python
import pkg.subpkg.mymodule
pkg.subpkg.mymodule.myfunc() # myfunc must be fully referenced
```
or
```python
from pkg.subpkg import mymodule
mymodule.myfunc()
```
or
```python
from pkg.subpkg.mymodule import myfunc
myfunc()
```

Importing all:
```python
from pkg import * # import everything in the __all__ list defined inside __init__.py
```
This behaviour is explained by the fact that under Microsoft OS like Windows 95
or MS-DOS, there are ambiguities about file names, and thus about module names,
so they need to be properly defined somewhere.
```python
__all__ = ["echo", "surround", "reverse"]
```

Crossreferences: two modules can import each other. In fact it is so common in
Python that before looking on disk, the module importer looks among already
loaded modules.

Using a relative path when importing:
```python
from . import module
from .. import module2
from ..package import module3
```

## alien

Convert an RPM package into a DEB package:
```bash
apt-get install alien
alien -d mypkg.rpm
```
## apk

Alpine package management.

Install a package:
```sh
apk add make
```
## apt-add-repository

Adding a ppa (normally, add also the key):
```sh
sudo apt-add-repository ppa:gwibber-daily/ppa
```

Removing a ppa:
```sh
add-apt-repository -r ppa:colingille/freshlight
```

List repositories:
```sh
add-apt-repository -L
```
## apt-cache

Searching for packages inside package names and full description:
```sh
apt-cache search qt4 # One single regex pattern.
apt-cache search ^perf tool # Multiple patterns (logical AND).
```

Getting info about a package:
```sh
apt-cache show qt4
```
Equivalent to `dpkg --print-avail ...`

Display information about multiple packages (available versions, dependencies, etc):
```sh
apt-cache showpkg qt4 make gcc
```

## apt-file

Install:
```sh
apt install apt-file
```

## apt-get

### install

Install a package
```sh
apt-get install myplg
```

Answer Yes to all questions:
```sh
apt-get install -y myplg
```

Fix missing dependencies:
```sh
apt-get install -f
## or
apt-get install --fix-broken
```

Install a `.deb` archive:
```sh
apt-get install mypkg.deb
```

### autoremove

To uninstall a package:
```sh
apt-get remove mypkg
```
To uninstall a package and all its dependencies:
```sh
apt-get --purge autoremove mypkh
```

### purge

Remove a package, include configuration files:
```sh
apt-get purge mypkg
```

### remove

Remove a package:
```sh
apt-get remove mypkg
```

### update

Updating repositories database:
```sh
apt-get update
```

### upgrade

Only shows what it would do, but don't do it:
```sh
apt-get upgrade -s
```
## aptitude

Get from which other packages a package was installed:
```sh
aptitude why mypkg
```
## apt-key

To install a missing public key:
```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 2EA8F35793D8809A
```
## apt

 * [Ubuntu Packages Search](http://packages.ubuntu.com).

Front-end command line for `apt-cache` and `apt-get` and `apt-cache`:
`search`:  `apt-cache search`
`show`:    `apt-cache show`
`upgrade`: `apt-get   upgrade`
`update`:  `apt-get   update`
`purge`:   `apt-get   purge`
`remove`:  `apt-get   remove`
`install`: `apt-get   install`

List installed packages:
```sh
apt list --installed
```
## cnf

Suse tool.

Looking for the package to which a command belongs:
```bash
cnf <command>
```
## conda
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

[Conda](http://conda.pydata.org/docs/) is a crossplatform package management
system and environment management system, capable of handling multiple versions
of software packages and their dependencies.
See also [Mamba](https://mamba.readthedocs.io/).

### Installation

 * [Installing on Linux](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html).
 * [Anaconda Distribution](https://www.anaconda.com/products/distribution). Huge, complete installation with tones of packages (numpy, pandas,jupyter, ...).
  + [How to install Anaconda on Ubuntu 22.04 LTS Jammy](https://www.how2shout.com/linux/how-to-install-anacondaon-ubuntu-22-04-lts-jammy/).
 * [Miniconda Linux installers](https://docs.conda.io/en/latest/miniconda.html). Minimalist installation of conda.

After installation with installer script, initialize conda for the targeted shell:
```sh
conda init bash
```

### Creating an environment

Create a new environment by name:
```sh
conda create -n myenv
```

The prefix of the environment is stored in `CONDA_PREFIX` env var.

To install package when creating environment:
```sh
conda create -n myenv pkg1 pkg2 ...
```

Use channel:
```sh
conda create -c conda-forge -n myenv pkg1 pkg2 ...
```

To automatically answer "yes" at all questions:
```sh
conda create -y ...
```

### Activating and deactivating

To activate the conda environement, run:
```sh
conda activate myenv
```

To deactivate:
```sh
conda deactivate
```

Activate and stack on current environment
```sh
conda activate --stack myenv
```

Set automatic stacking on top-level environment:
```sh
conda config --set auto_stack 1
```

### Run command inside environment

```sh
conda -n myenv /my/cm.sh
```

### List environments

To list environments:
```sh
conda env list
```

### Removing an environment

Remove an environment:
```sh
conda env remove -n myenv
```

### Defined environment variables

To known the name of the current environment:
```sh
echo $CONDA_DEFAULT_ENV
```

To get the path of the current environment:
```sh
echo $CONDA_PREFIX
```

### Managing packages

 * [Anaconda.org](https://anaconda.org/). To search for packages in all channels.

List packages installed in current environment:
```sh
conda list
```

Search for packages:
```sh
conda search bash
```

Install a package:
```sh
conda install bash=5.0.018
```

Uninstall a package:
```sh
conda uninstall bash
```
or
```sh
conda remove bash
```
## dnf

RedHat package manager, successor of yum.

```sh
dnf clean all
```

```sh
dnf update
```

Answer `yes` to all questions:
```sh
dnf -y ...
```
## dpkg

Install a package archive `.deb`:
```sh
dpkg -i mypkg.deb
```
If some dependencies are missing, you may run:
```sh
apt --fix-broken install
```

Compare versions:
```sh
dpkg --compare-versions "2.11" "lt" "3"
```

List files from package:
```sh
dpkg -L mypkg
```

Getting info about a package:
```sh
dpkg -s qt4 # Show information on installed package
```

Find package that provides a file:
```sh
dpkg -S /usr/bin/pdflatex
```
## emerge

Gentoo package manager.

Updating:
```bash
sudo emerge --sync
```

Searching:
```bash
equery mypkg
```

If you do not have `equery` you can install it with:
```bash
emerge -a gentoolkit
```

Installing:
```bash
sudo emerge mypkg
```


## gdebi

`gdebi` installs Debian packages (.deb).
To install it:
```bash
sudo apt-get install gdebi
```

To install a package with it:
```bash
sudo gdebi mypackage.deb
```


## makepkg

 * [makepkg](https://wiki.archlinux.org/title/makepkg).
 * [How to rebuild a package using the Arch Linux Build System](https://linuxconfig.org/how-to-rebuild-a-package-using-the-arch-linux-build-system).

Obtain the PKGBUILD of a package:
```sh
yay -G mypkg
```

Inside the PKGBUILD folder, run the following command to build and install:
```sh
makepkg -csrfi
```

## Msiexec

 * [Microsoft Standard Installer Command-Line Options](https://docs.microsoft.com/en-us/windows/win32/msi/standard-installer-command-line-options).

Install an MSI file automatically (no user dialog window):
```sh
Msiexec /package Application.msi /quiet
```
## paccache

Clean old files from the pacman cache folder (`/var/cache/pacman/pkg/`):
```sh
paccache -r
```
## pacman (ArchLinux)

 * [pacman](https://wiki.archlinux.org/title/pacman).
 * [Mirrors](https://wiki.archlinux.org/index.php/Mirrors).

Clean package cache: `sudo rm /var/cache/pacman/pkg/*`.

Run and accept all questions with default answer:
```sh
RUN pacman --noconfirm ...
```

Install or upgrade (synchronize) a package:
```sh
pacman -S mypkg
```

Query installed packages:
```sh
pacman -Q mypkg
```
Search in installed packages:
```sh
pacman -Qs foo
```

List files installed by a package:
```sh
pacman -Fl mypkg
```

Search for packages owning a file:
```sh
pacman -F myfile
```

Search for packages:
```sh
pacman -Ss mypkg
```

Get info on a package:
```sh
pacman -Si mypkg
pacman -Qi mypkg
```

List all files of an installed package:
```sh
pacman -Ql mypkg
```

Update databases:
```sh
pacman -Syy # The second `y` force refresh of all databases even if it appears up-to-date.
```
A file `/etc/pacman.d/mirrorlist.pacnew`

To select the fastest mirrors, use `rankmirrors` from `pacman-contrib` package:
```sh
cp /etc/pacman.d/mirrorlist mirrorlist.bkp
sed -i 's/^#Server/Server/' mirrorlist.bkp
rankmirrors -n 10 mirrorlist.bkp > mirrorlist.10fastest
```

Upgrade/update all packages:
```sh
pacman -Syu
```

In case of a message about corrupted package and invalid keys, do:
```sh
pacman -S archlinux-keyring
```

List out-of-date packages:
```sh
pacman -Qu
```

Install AUR (ArchLinux User Repository) packages:
```sh
yay -S mypkg
```
Run `yay` as normal user, not as root.

Search for packages containing a file:
```sh
pacman -Qo myfile
```

Remove a package and all its dependencies that are not used by any other package
and has not been installed explicitly:
```sh
sudo pacman -Rs mypkg
```

Downgrade packages (in case some packages are detected as newer than in repository):
```sh
sudo pacman -Suu
```

## paccache

Install:
```sh
yay -S pacman-contrib
```

Clean `/var/cache/pacman` folder that contains downloaded packages:
```sh
paccache -r
```

Enable periodic cleaning of unused packages:
```sh
sudo systemctl enable --now paccache.timer
```

## pkg\_add

Install package on OpenBSD:
```sh
pkg_add bash
```
## pkg\_info

Search for packages on OpenBSD:
```sh
pkg_info -Q python
```
## pkg

FreeBSD package manager.

Search for a package:
```bash
pkg search mypkg
```
## MacPorts / port

 * [MacPorts](https://ports.macports.org/).
 * [Installing MacPorts](https://www.macports.org/install.php).

For installing, MacPorts needs:
 1. [XCode](https://developer.apple.com/technologies/tools/).
 2. Accept license: `sudo xcodebuild -license`
 2. Apple's Command Line Developer Tools: `xcode-select --install`
 3. [XQuartz Project](https://www.xquartz.org/).
 4. Download MacPorts `.pkg` and install.

Upgrade:
```sh
sudo port -v selfupdate
```

Get help:
```sh
man port
```

List files installed by package:
```sh
port contents mypkg
```

Uninstall a package:
```sh
sudo port uninstall mypkg
```

List installed packages:
```sh
port installed
```
## repoquery

RedHat, CentOS, Rocky.

List files installed by a package:
```sh
repoquery --installed -l mypkg
```
## snap (Ubuntu, Debian)

Cross-distributions package manager.

Installing a package:
```bash
snap install kubectl --classic
```

Search for a package:
```sh
snap find somepkg
```

List installed packages:
```sh
snap list
```

Remove a package:
```sh
snap remove mypkg
```

Update packages:
```sh
snap refresh
```

### Errors

`Unable to update "Snap Store"`:
 * See [Unable to update "Snap Store": cannot refresh "snap-store": snap "snap-store" has running apps (ubuntu-software)](https://askubuntu.com/questions/1411104/unable-to-update-snap-store-cannot-refresh-snap-store-snap-snap-store-ha).
  + Solution : `sudo pkill snap-store && sudo snap refresh snap-store`.
## softwareupdate

Manages macos software updates on command line.

List updates:
```sh
softwareupdate --list
```

Update an application:
```sh
softwareupdate --install myapp
```
## yay (ArchLinux)

Package manager for AUR repository.

 * [How to Install Yay AUR Helper in Arch Linux and Manjaro](https://www.tecmint.com/install-yay-aur-helper-in-arch-linux-and-manjaro/).

Install:
```sh
cd /opt
sudo git clone https://aur.archlinux.org/yay-git.git
sudo chown -R myuser:myuser ./yay-git
cd yay-git
makepkg -si
```

`yay` uses `~/.cache/yay` as a cache folder. To clean it use:
```sh
yay -Sc --aur
```

Obtain the PKGBUILD of a package:
```sh
yay -G mypkg
```

How to solve this warning?:
```
WARNING: Skipping verification of source file PGP signatures.
```
## yum
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Package manger for CentOS, RedHat, Fedora.

Search for a package:
```sh
yum search mypkg
```

Install a package:
```sh
yum install mypkg
```

Installing a package and answering yes automatically to any question:
```sh
yum install -y mypkg
```

Uninstall a package:
```sh
yum remove mypkg
```

Find which package provides a particular file:
```sh
yum whatprovides myfile
```

Get information about a package:
```sh
yum info mypkg
```

List repositories:
```sh
yum repolist
```

List installed packages:
```sh
yum list installed
```
## zypper

Package manager for Suse.

Installing a package:
```bash
zypper install <package>
```

Getting information about a package:
```bash
zypper info <package>
```

adding a repository to package manager
```bash
zypper addrepo <URL> <alias>
```
or
```bash
zypper ar <URL> <alias>
```

refresh a repository
```bash
zypper refresh --repo <alias>
```

upgrade packages inside a repository
```bash
zypper dist-upgrade --repo <alias>
```

Packman repository.
Contains that are not distributed with Linux distributions,
like aMule.
<http://packman.jacobs-university.de/suse/11.2/>

installing kernel sources
kernel sources are in /usr/src
run uname to know the current kernel version (kernel-default, kernel-desktop, ...):
uname -r
run the following command to install last version of the corresponding kernel:
sudo zypper install kernel-default
run the following command to install the corresponding kernel sources:
sudo zypper install kernel-source
or
sudo zypper install --type srcpackage kernel-default
