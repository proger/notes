# Windowing systems

## brightness

`brightness` allows brightness control.
Badly done, remember a selection of displays, and then only ask for brightness.
Better try directly `ddcutil`.
Install `brightness`, that uses `ddcutil`:
```sh
yay -S brightness
```

The module `i2c_dev` is used by `ddcutil` and must be loaded into the kernel:
```sh
modprobe i2c_dev
```
For automatic loading:
```sh
echo "i2c_dev" | sudo tee /etc/modules-load.d/ddcutil.conf
sudo chmod a+r /etc/modules-load.d/ddcutil.conf
```

## Clipboard

### xclip

Read or write X server selection.

`xclip-copyfile`, `xclip-cutfile`, `xclip-pastefile` - copy and move files via the X clipboard.

### xsel

A clipboard manager.
Can be used in combination with urxvt, since urxvt has no copy&paste feature by default (it needs a perl extension). See [Configuring URxvt to Make It Usable and Less Ugly](https://addy-dclxvi.github.io/post/configuring-urxvt/).

Print primary selection (usually middle mouse button):
```sh
xsel -o
```

Print seconday selection (???):
```sh
xsel -s -o
```

Print the clipboard selection (usually CTRL+C):
```sh
xsel -b -o
```

Copy X primary selection into clipboard:
```sh
xsel -po | xsel -bi
```

Copy clipboard into X primary selection:
```sh
xsel -bo | xsel -pi
```

Exchange clipboard and X primary selection:
```sh
xsel -po | xsel -si ; xsel -bo | xsel -pi ; xsel -so | xsel -bi
```

## redshift

Control color temperature of display.

```bash
redshift -l geoclue2 -t 5700:3600 -g 0.8 -m randr -o
```
Use `-p` instead of `-o` to just print what the temperature will be set to.

Use `~/.config/redshift.conf` file to set configuration. See man page.

## xscreensaver

 * [XScreenSaver](https://wiki.archlinux.org/index.php/XScreenSaver).

To configure:
```sh
xscreensaver-demo
```

To run xscreensaver automatically, add the following line in `~/.xinitrc`:
```sh
xscreensaver -no-splash &
```

Once the daemon is started, call `xscreensaver-command` to control it.

To run immediatly:
```sh
xscreensaver-command -activate
```

To set folder of images to use, define `imageDirectory` inside `~/.xscreensaver`:
```
imageDirectory: /my/absolute/path/to/my/images/folder
```

To setup the number of tries for login, see the `deny` parameter in
`/etc/security/faillock.conf`.

## unclutter

 * [Auto Hide Mouse Pointer Using Unclutter After A Certain time](https://ostechnix.com/auto-hide-mouse-pointer-using-unclutter-after-a-certain-time/).

## xrandr

Setup external screens for use. Improvement of xinerama.

Get info:
```sh
xrandr # List available screens
xrandr --listmonitors # List defined monitors
xrandr --listactivemonitors # List active monitors
```

To use a screen:
```sh
xrandr --output HDMI-2 --auto --right-of eDP1
```

To disable a screen:
```sh
xrandr --output DP-1-2 --off
```

Set brightness to 100%:
```sh
xrandr --output HDMI-1 --brightness 1.0
```

Get information about monitors:
```sh
xrandr --verbose
```

Create a virtual monitor from two monitors:
```sh
xrandr --setmonitor MyMonitor auto HDMI-A-0,HDMI-A-1
xrandr --setmonitor evry1 3840/688x1080/194+1920+0 DVI-I-1-1,DVI-I-2-2
```
Remove virtual monitor:
```sh
xrandr --delmonitor MyMonitor
```
List monitors:
```sh
xrandr --listmonitors
```

Set mirroring with chosen resolution:
```sh
xrandr --output HDMI-A-0 --mode 1920x1080 --same-as DisplayPort-0
```

## xmodmap

 * [xmodmap]().

Key configuration file is `~/.Xmodmap`:
```
keycode 108 = Multi_key Alt_R Meta_R Alt_R Meta_R
```
This line defines `Alt_R` as the Compose Key.

## Xorg/X11/Wayland

### X coloring

 * [256 Xterm 24bit RGB color chart](http://www.calmar.ws/vim/256-xterm-24bit-rgb-color-chart.html).
 * [X colors rgb.txt decoded](http://sedition.com/perl/rgb.html).

### mimeapps.list

`~/.config/mimeapps.list` defines default applications for MIME types.
It defines the browser to use when clicking a link in the terminal.

### xdg-open 

Opens a file or URL with the preferred user's application.

See `xdg-settings` for configuration.

### xdg-settings

 XDG = X Desktop Group

 * [freedesktop.org](https://www.freedesktop.org/wiki/).
 * [xdg-utils](https://www.freedesktop.org/wiki/Software/xdg-utils/).
 * [XDG Base Directory](https://wiki.archlinux.org/index.php/XDG_Base_Directory).

Default trash directory: `$XDG_DATA_HOME/Trash`.

Get and set settings for the desktop environment.

Get default browser:
```sh
xdg-settings get default-web-browser
```

Set default browser:
```sh
xdg-settings set default-web-browser firefox.desktop
```
Needs to be a X Desktop application.

### xdg-mime

To set application for each mime type.

### xinit

 * [Difference between .xinitrc, .xsession and .xsessionrc](https://unix.stackexchange.com/questions/281858/difference-between-xinitrc-xsession-and-xsessionrc).
 * [Xsession](https://wiki.debian.org/Xsession), on Debian.

`.xinitrc` is part of X11. It is used when starting GUI from the console (using `startx`, that calls `xinit`, which parse `.xinitrc`).
To get a default `.xinitrc`, copy the one at `/etc/X11/xinit/xinitrc`.

`.xsession` is part of X11. It is read at GUI login, from a display manager. Beware that you must tell the display manager to invoke the "custom" session type.
On Debian the `xsession` is not parsed but executed in order to run the window manager.

`.xsessionrc` is specific to Debian and Ubuntu.

`.xprofile` is specific to GDM (Gnome).

### startx

Install on ArchLinux:
```sh
pacman -S xorg-xinit
```

### xorg.conf

#### Touchpad libinput

 * [libinput](https://wiki.archlinux.org/title/Libinput)
 * [Enable tap to click in i3 WM](https://cravencode.com/post/essentials/enable-tap-to-click-in-i3wm/). Touchpad.

Install `libinput` package:
```sh
pacman -S xf86-input-libinput
```

Edit or create `/etc/X11/xorg.conf.d/90-touchpad.conf`:
```conf
Section "InputClass"
        Identifier "touchpad"
        MatchIsTouchpad "on"
        Driver "libinput"
        Option "Tapping" "on"
EndSection
```
Make the file readable by everyone, and restart X11.

To check settings, first list devices:
```sh
xinput --list
```
and look for the ID number of the touchpad.
Then list the properties of the touchpad:
```sh
xinput --list-props <device_id>
```
Finally enable a property with:
```sh
xinput --set-prop <device_id> <property_id> 1
```

#### Touchpad Synaptics [DEPRECATED]

 * [Touchpad Synaptics](https://wiki.archlinux.org/title/Touchpad_Synaptics).

Install `synaptics` package:
```sh
pacman -Ss xf86-input-synaptics
```

### .Xresources

X11 configuration file `~/.Xresources`.
Must but loaded from `~/.xinitrc` using `xrdb`.

 * [X resources](https://wiki.archlinux.org/title/x_resources).

### xrdb

Load Xresources file.

Load values and merge with current settings:
```sh
xrdb -merge ~/.Xresources
```

Load values and replace current settings:
```sh
xrdb ~/.Xresources
```

Print current values:
```sh
xrdb -query -all
```

### xbindkeys

Bind commands to keys.
Put a call to `xbindkeys` inside `.xinitrc`.
It will read the file `~/.xbindkeysrc` for definitions.

Example of screenshot taken when pressing the "Print" key:
```conf
# Full screenshot 
"xfce4-screenshooter -f -s ~/Downloads/screenshot_$(date +%Y-%m-%d-%H:%M).png"
m:0x0 + c:107   # Print
```

### xinput

Query and modify X configuration (Wayland).

List devices:
```sh
xinput --list
```

List properties of a device (using ID number):
```sh
xinput --list-props 12
```

Modify the value of the property 346 of device 12:
```sh
xinput --set-prop 12 346 1
```

### xset

Set user preferences for X.

Get current settings:
```sh
xset q
```

Set DPMS (Display Power Management Settings / Energy Star) values:
```sh
xset dpms 240 480 720 # standby suspend off (in seconds)
```

DPMS and screensaver:
```sh
xset dpms  # Enable DPMS
xset -dpms # Disable DPMS
xset s off # turns off the screen saver
xset s noblank # turns off blanking
```

After installing a font run the following command in order to reload the fonts
list dynamically:
```sh
xset fp rehash
```

## Fonts

### xfd

Open a window and display characters of a font:
```bash
xfd -fn fontname
```

### xfontsel

Selecting a font using "X logical font description":
```sh
xfontsel
```

### xlsfonts

Install:
```sh
yay -S xorg-xlsfonts
```

Getting list of fonts:
```sh
xlsfonts
```

## Locking

### xlock

A locking program with screen saver feature.

On ArchLinux:
```sh
pacman -S xlockmore
```

### xflock4

Xfce4 screen locker:
```bash
xflock4
```

### xautolock

Runs a program after computer idle time.
```sh
xautolock -time 5 -locker "systemctl suspend-then-hibernate"
```
Time is in minutes.
Default locker program is `xlock`.

## Display managers

 * [Display manager](https://wiki.archlinux.org/title/display_manager).

### lemurs

 * [lemurs](https://github.com/coastalwhite/lemurs).

### ly

 * [ly](https://github.com/fairyglade/ly).

Console DM.

### tdm

 * [Console TDM](https://wiki.archlinux.org/title/Console_TDM).

Add `exec tdm --xstart` at the end of the `~/.xinitrc`.
The default session is defined by `~/.config/tdm/default`.

## Window managers

### Classic window managers

#### Xfce

 * [Install Xfce / Xubuntu desktop on Ubuntu 20.04 Focal Fossa Linux](https://linuxconfig.org/install-xfce-xubuntu-desktop-on-ubuntu-20-04-focal-fossa-linux).

Installing on Debian, see <https://wiki.debian.org/Xfce>:
```bash
apt-get install xfce4
#apt-get install xfce4-goodies
#apt-get install task-xfce-desktop
```

Installing on ArchLinux:
```bash
pacman -S xorg
pacman -S extra/xfce4
```

Packages/plugins:
```bash
extra/thunar # Thunar file manager.
extra/xfce4-battery-plugin # Battery plugin
extra/xfce4-pulseaudio-plugin # Audio
extra/xfce4-power-manager # Power management
extra/xfce4-screenshooter # Screen shots
```

How to modify key shortcuts for file manager Thunar:
 * Edit file `~/.config/Thunar/accels.scm`.
 * For changing rename key from F2 to Return, find line with F2 key, and replace it with `"<>Return"`.
 * Quit Thunar: `thunar -q`.
 * Re-open Thunar.

### Tiling window managers

 * [dwm](https://dwm.suckless.org/).

#### i3

 * [i3 (Official site)](https://i3wm.org/).
 * [i3 (Gentoo)](https://wiki.gentoo.org/wiki/I3).
 * [Enable tap to click in i3 WM](https://cravencode.com/post/essentials/enable-tap-to-click-in-i3wm/). Touchpad.
 * [how can i change look of windows](https://faq.i3wm.org/question/2071/how-can-i-change-look-of-windows.1.html). Changing colors.
 * [i3bar input protocol](https://github.com/i3/i3/blob/next/contrib/trivial-bar-script.sh).

Install on ArchLinux:
```sh
pacman -S i3-wm i3status i3lock xorg xorg-xinit dmenu
```

Install on Debian:
```sh
apt install i3
```

`$mod` key is usually `Windows` or `Apple` key.

Keys                | Command
------------------- | --------------------------------
$mod+<Enter>        | Open a terminal. Default terminal set with `TERMINAL` env var.
$mod+d              | Open dmenu (text based program launcher).
$mod+shift+q        | Kill window (exit normaly if possible).
$mod+shift+c        | Reload config file.
$mod+shift+r        | Restart i3 in place.
$mod+shift+e        | Exit i3.
------------------- | --------------------------------
$mod+<arrows>       | Move to window.
$mod+shift+<arrows> | Move window in direction.
$mod+shift+h|j|k|l  | Move window in direction.
$mod+r              | Toggle resize mode.
------------------- | --------------------------------
$mod+a              | Move focus to parent.
$mod+h              | Horizontal split.
$mod+v              | Vertical split.
$mod+w              | Tabbed layout.
$mod+e              | Toggle vertical/horizontal layout.
$mod+s              | Stacked layout.
$mod+f              | Fullscreen.

##### Status bar

The status bar script is set with `status_command` in i3 config. Default is
i3status.

For the syntax of the output of the status bar script, see i3bar.

#### sway

 * [Linux’s SWAY Window Manager](https://medium.com/hacker-toolbelt/linuxs-sway-window-manager-c39abe0b7bc9).
 * [How to setup multiple monitors in sway](https://fedoramagazine.org/how-to-setup-multiple-monitors-in-sway/).

Window tiling manager.
i3-compatible Wayland compositor.

Install on Ubuntu:
```sh
apt install sway swaylock waybar swayimg swayidle
```

`python3-i3ipc`: Python library to control i3wm and sway.

#### xmonad

 * [xmonad](https://xmonad.org/).
 * [XMonad.Doc.Configuring](https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Doc-Configuring.html) --> how to write an xmonad configuration.
 * [Xmonad default bindings](https://wiki.haskell.org/File:Xmbindings.png).

`mod` is left Alt by default.

Default commands:
	mod-shift-return    Open terminal
	mod-shift-q         Quit xmonad
	mod-q               Compile and reload configuration
	mod-space           Cycles through tiling algorithms.
	mod-j               Move focus to previous window
	mod-k               Move focus to next window
	mod-,               Increase the number of windows in the master pane
	mod-.               Decrease the number of windows in the master pane
	mod-return          Swap focused window with window in master pane
	mod-shift-j         Swap focused window with next window
	mod-shift-k         Swap focused window with previous window
	mod-h               Increase/Decrease window size
	mod-l               Increase/Decrease window size
	mod-button1         Drag a window and make it float
	mod-t               Put back a floating window into the tiling layer.
	mod-button2         Bring floating window to the top
	mod-button3         Resize floating window
	mod-shift-c         Kill window
	mod-p               Open dmenu
	mod-n               Switch to workspace n (1-9)
	mod-shift-n         Move focused window to workspace n (1-9)

Compile configuration file:
```sh
xmonad --recompile
```

