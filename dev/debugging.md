# Debugging

## DDT

 * [Static Analysis](https://developer.arm.com/documentation/101136/latest/DDT/Source-code/Static-analysis).

 * OpenMP and MPI debugger.
 * Static code analyser.

## gdb

 * [cgdb](http://cgdb.github.io/) is a colored syntax-highlighting version of gdb. With window split in two. The upper part displays file code.

Keys         | Description
---          | ---
CTRL+UP/DOWN | Search for past commands.

Command      | Description
---          | ---
list         | List code.
---          | ---
bt           | Print backtrace
down         | Move down in the callstack (callee)
up           | Move up in the callstack (caller)
up 10        | Move up 10 frames in the callstack.
---          | ---
r, run       | Run or restart.
c, continue  | Continue up to next breakpoint or the end.
n, next      | Next line
s, step      | Step into
---          | ---
p, print     | Print an expression or variable
display      | Print an expression or variable at each stop.
display/16xh | Print string

To set command line arguments (including standard input and output redirection):
```
set args ...
```

To set env vars:
```
set environment
```

Watch variable:
```
awatch <expression>
```

Breakpoints:
`info breakpoints`								# list of breakpoints
`b <line>`
`b <function>`
`b <namespace::class::method>`
`del break <number>`										# delete breakpoint
`clear`														# clear current breakpoint
`clear <line>`										# clear breakpoint at specified line
`clear <function>`								# clear breakpoint at specified function

Condition on a breakpoint:
```
condition <breakpoint number> <expression>
```

Print variables:
```
x/20xh my_string
p my_var
```

Follow child in fork:
```
set follow-fork-mode child
```

## ddd

 * [DDD](https://www.gnu.org/software/ddd/).

## lldb

The debugger for the LLVM (clang) compiler.

 * [LLDB](http://lldb.llvm.org) official site.

Run:
```bash
lldb -f my_prog -- arg1 arg2 arg3 ...
```

Command     | Description
----------- | -------------------------------------
`h`         | Help.
`h b`       | Help on breakpoint command.
`b main`    | Set a breakpoint on main() function.
`br del 2`  | Delete breakpoint 2.
`r`         | Run.
`n`         | Step.
`s`         | Step in.

## Python

### pdb

 * [pdb - The Python Debugger](https://docs.python.org/3/library/pdb.html).

To run `pdb` on a script:
```sh
python -m pdb myscript.py
```

Command            | Description
---                | ---
`b`                | Set a breakpoint: `b my.namespace.MyClass.my_fct`.
`bt`, `w`, `where` | Print stack trace.
`h`, `help`        | Help.
`l`, `list`        | Print current code.
`n`                | Step over.
`s`, `step`        | Step into.
`r`                | Run the script.
`u`, `up`          | Move up into the stack trace.
`myvar`            | Print value of `myvar`.

### ipdb

A enhanced `pdb` with colors.

Install:
```sh
python -m pip install ipython
```

Start `ipdb` with `pytest` for a specific test function:
```sh
python -m pytest --trace --pdbcls=IPython.terminal.debugger:Pdb -k test_my_test_fct tests/test_050_my_test.py
```

To run `ipdb` with `poetry`:
```sh
poetry add -G dev ipdb
poetry install
poetry run python -m ipdb .venv/bin/myprog
```

### PuDB

 * [PuDB documentation](https://documen.tician.de/pudb/index.html).

Key    | Description
---    | ---
CTRL+P | Settings

## R

 * [Debugging R code](https://rstats.wtf/debugging-r).
 * [Debugging with the RStudio IDE](https://support.rstudio.com/hc/en-us/articles/205612627-Debugging-with-RStudio).

Using the RStudio debugger for debugging package tests:
 1. Insert a `browser()` inside code, it will act as a breakpoint.
 2. Run `devtools::test()` from inside Rstudio.

