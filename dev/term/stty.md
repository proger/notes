# stty

Controls terminal settings.

By default CTRL-S freezes the terminal, and CTRL-Q unfreezes it.

Turn off CTRL-S and CTRL-Q bindings for XON/XOFF flow control:
```sh
stty -ixon
```
