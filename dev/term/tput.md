# tput

 * [Colours and Cursor Movement With tput](https://tldp.org/HOWTO/Bash-Prompt-HOWTO/x405.html).

Get the number of columns of the current terminal:
```sh
tput cols
```

Get the number of lines of the current terminal:
```sh
tput lines
```

Hiding cursor:
```sh
tput civis
```

Showing cursor:
```sh
tput cnorm
```
