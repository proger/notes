# tmux
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','tmux':''} -->

For reloading the config file inside current tmux session, run the following
command in shell:
```sh
tmux source-file ~/.tmux.conf
```

Change window name/title from a bash script:
```sh
echo $'\ekMYTITLE\e\\'
```
Use script name as title:
```sh
echo $'\ek'$(basename $0)$'\e\\'
```
For this to work, you must first enable renaming:
```tmux
set -g allow-rename on
```

Prefix is by default `C-b`.
Setting prefix another prefix:
```tmux
unbind C-b
set -g prefix C-a
bind C-a send-prefix
```

## Run on command line

Get version of tmux:
```sh
tmux -V
```

### attach

Attach to an already attached session:
```sh
tmux attach
```

Reattach (detach and attach again) an already attached session:
```sh
tmux attach -d
```

Does not modify size (use size of other clients):
```sh
tmux attach -f ignore-size
```

### ls

List current sessions:
```sh
tmux ls
```

### new

Create a new session:
```sh
tmux new -s myNewSession
```

## = operator

Sets an environment variable (will be passed to sub-processes):
```tmux
MYENVVAR=myvalue
```

## %hidden

Sets a local variable (will **not** be passed to sub-processes):
```tmux
%hidden myvar=myvalue
```

## %if

Conditionnal parsing. Not available in old versions of tmux (1.8).

```tmux
%if "#{==:#{MYVAR},myvalue}"
set -g status-bg black
%elif "#{==:#{MYVAR},myvalue2}"
set -g status-bg 
%else
set -g status-bg green
%endif
```

Test hostname:
```tmux
%if "#{==:#{host},myhost}"
%endif
```

## #{} (format variables)

See manpage, section FORMATS.

Example of setting hostname with `host` format variable inside the status bar:
```tmux
set -g status-right '#{host}'
```

## #\[] (styles)

Set colors in status bar.
See manpage, section STYLES.

## #() (output of shell command)

Write output of shell command in status bar.

```tmux
set -g status-right '#(mycommand arg1 arg2)'
```

## Commands

### attach-session (alias: attach)

Attach to an already attached session:
```sh
tmux attach
```

Reattach (detach and attach again) an already attached session:
```sh
tmux attach -d
```

Does not modify size (use size of other clients):
```sh
tmux attach -f ignore-size
```

Attach a session by name:
```sh
tmux attach -t mysession
```

### bind-key (alias: bind)

Bind commands to key sequences.

Grabs the pane from the target window and joins it to the current:
```tmux
bind-key j command-prompt -p "join pane from:"  "join-pane -s '%%'"
```

Binding notations:
 * `C-j`: CTRL+J (lowercase or uppercase)
 * `M-j`: Option/Alt+J or Esc+J

#### Default key bindings

See manpage for a complete list.

| ------------------------- | ----------------------
| Key                       | Description
| ------------------------- | ----------------------
| `prefix d`                | Detach.
| `prefix ,`                | Rename current window.
| `prefix "`                | Split pane horizontally.
| `prefix %`                | Split pane vertically.
| `prefix arrow key`        | Switch pane.
| `prefix {`                | Swap current pane with previous pane.
| `prefix }`                | Swap current pane with next pane.
| `prefix c`                | Create a new window.
| `prefix n`                | Move to the next window.
| `prefix p`                | Move to the previous window.
| `prefix Ctrl-o`           | Rotate panes in increasing order.
| `prefix Alt-o`            | Rotate panes in decreasing order.
| `prefix q`                | Show pane number.
| `prefix &`                | Kill window.
| `prefix x`                | Kill pane.
| `prefix <space>`          | Cycle through the layouts.
| `prefix Esc-1`            | Select layout 1 even-horizontal
| `prefix Esc-2`            | Select layout 2 even-vertical
| `prefix Esc-3`            | Select layout 3 main-horizontal
| `prefix Esc-4`            | Select layout 4 main-vertical
| `prefix Esc-5`            | Select layout 5 tiled
| `prefix +`                | Break pane into window
| `prefix -`                | Restore pane from window
| `prefix [`                | Enter copy mode (space then enter to select).
| `prefix ]`                | Paste.
| ------------------------- | ----------------------

### display-message

Display a message on console.

```tmux
display-message -p "Default shell is #{SHELL}."
```

### if-shell (alias: if)

Execute command conditionnaly.

### join-pane

Split current pane and use window 4 as second pane:
```tmux
join-pane -s 4
```

<!-- TODO Test & confirm. Does not seem to work.-->
Move current pane alongside the pane number 2 inside the same window:
```tmux
join-pane -t 2
join-pane -ht 2 # Place panel below panel 2.
join-pane -bht 2 # Place panel above panel 2.
join-pane -vt 2 # Place panel at right of panel 2.
join-pane -bvt 2 # Place panel at left of panel 2.
```

Move current pane, if it is the single pane of the current
window, into window 3:
```tmux
join-pane -t 3
```

### list-sessions (alias: ls)

List sessions:
```sh
tmux ls
```

### new-session, new

Create a new session with a custom name:
```sh
tmux new -s mysession
```

### rename-session, rename

Rename a session:
```sh
tmux rename -t 1 newname
```

### resize-pane

Resize the current pane down:
```tmux
resize-pane -D
resize-pane -D 20
```
Use `-U` for upward, `-L` for left and `-R` for right.

### run-shell (alias: run)

Execut shell command

### set-option (alias: set)

Sets an option.

Sets the value of a global (-g) option:
```tmux
set -g status-fg black
```

### show-options (alias: show)

Gets the value of a global (-g) option:
```tmux
set -g status-fg
```

### swap-window

Swap two windows:
```tmux
swap-window -s 3 -t 1
```

Swap current window with a specific window:
```tmux
swap-window -t 1
```

## Plugins

 * [Battery plugin](https://github.com/tmux-plugins/tmux-battery).
