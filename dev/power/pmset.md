# pmset

BSD command.

Get battery information:
```bash
pmset -g batt
```
