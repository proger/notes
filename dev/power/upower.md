# upower

Get information about power devices (AC adapter, batteries).

List devices:
```sh
upower -e
```

Get information about one device:
```sh
upower -i /org/freedesktop/UPower/devices/battery_BAT0
```

Get informations about all devices:
```sh
upower -d
```
