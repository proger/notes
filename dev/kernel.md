# Kernel

 * [Kernel module](https://wiki.archlinux.org/title/Kernel_module).

## mkinitcpio

For adding a module to the kernel, edit `/etc/mkinitcpio.conf`:
```conf
HOOKS=(base udev autodetect modconf block encrypt filesystems resume keyboard fsck)
```
**Attention! the order is important.**
Then rebuild `initramfs`:
```sh
mkinitcpio -p linux
```

Under Archlinux, "WARNING possibly missing firmwares for module ...":
 - aci94xx: `yay -S aic94xx-firmware`
 - wd719x: `yay -S wd719x-firmware`
 - xhci\_pci: `yay -S upd72020x-fw`
 - ast: `yay -S ast-firmware`
 - qla1280, qed, bfa, qla2xxx: `pacman -S linux-firmware-qlogic`

Under Archlinux, "WARNING consolefont: no font found in configuration":
    Hook to install fonts for non-English languages.
