# Printing

## a2ps

Transform ASCII/text files into post script files.

Install on Archlinux:
```sh
pacman -S a2ps
```

```bash
a2ps -4 -A fill file1.txt file2.txt file3.txt -o output.ps
```
	-4 :		4 pages virtuelles par page
	-A fill :	mettre les fichiers les uns à la suite des autres sur une même page

To print a file with default printer:
```sh
a2ps <some_file.cpp
a2ps some_file.cpp some_other_file.cpp
```

To output to a file:
```sh
a2ps some_file.cpp -o output.ps
```

To print in portrait mode, on one column:
```sh
a2ps -R --columns=1 file.cpp
```

No headers:
```sh
a2ps -B ...
```

No border frame:
```sh
a2ps --borders=no ...
```

Enable colors:
```sh
a2ps --prologue=color ...
```
## cancel

Cancel a print job.

Cancel all jobs:
```sh
cancel -a
```
## cupsd

 * [CUPS](https://wiki.archlinux.fr/CUPS).
 * [Command-Line Printing and Options](http://localhost:631/help/options.html).
 * [Connecting Ubuntu client to Cups server](http://blog.delgurth.com/2009/01/06/connecting-ubuntu-client-to-cups-server/).

Install and start [Avahi](https://wiki.archlinux.org/index.php/Avahi#Hostname_resolution) for discovering printers automatically on the network.

Install on ArchLinux:
```sh
pacman -S cups ghostscript avahi
```

Start daemon:
```sh
systemctl enable --now cups.service
systemctl enable --now avahi-daemon
```

### server for other machines.

 * [Comment configurer un serveur d'impression?](https://qastack.fr/raspberrypi/980/how-can-i-set-up-a-print-server).
 * [CUPS/Printer sharing](https://wiki.archlinux.org/title/CUPS/Printer_sharing).

To configure the server for accepting printing requests from other machines,
you need to edit `/etc/cups/cupsd.conf`.
First allow the server to listen to its own IP address:
```
Listen localhost:631
Listen 192.168.1.7:631
```
Then allow machines on your local network:
```
<Location />
  Order allow,deny
  Allow localhost
  Allow 192.168.1.*
</Location>
```
Finally restart the server:
```sh
systemctl restart cups
```

To use the remote CUPS server system wide on another computer:
```sh
sudo bash -c "echo 'ServerName myserver:631'>/etc/cups/client.conf"
sudo hmod a+r /etc/cups/client.conf
```
For a different configuration for each user, use `~/.cups/client.conf`.
## lpadmin (CUPS)

Add a queue for a driverless (Apple AirPrint oo IPP Everywhere) printer:
```sh
lpadmin -p myqueue -E -v "ipp://my.ipp.printer/" -m everywhere
```

### Add a printer queue for a local network printer

In case you get the following error when you try to create
a queue for a local network printer, do the steps
described below:
	lpadmin: Unable to connect to "BRW402343686A92.local:631": Name or service not known

First list the printers:
```sh
lpinfo -v
```

For a network printer, takes the ipp address.
Install mDNS resolver:
```sh
pacman -S nss-mdns
```

Then edit Name Service Switch config:
```sh
sudo -e /etc/nsswitch.conf
```
On the `hosts:` line, insert `mdns_minimal [NOTFOUND=return]` before `resolve`.

Restart services:
```sh
systemctl restart cups.service
systemctl restart avahi-daemon
```

Now add the printer queue:
```sh
lpadmin -p myqueue -E -v "ipp://..." -m everywhere
```

Remove a printer queue:
```sh
lpadmin -x myqueue
```

## lpinfo (CUPS)

List printer devices:
```sh
lpinfo -v
```

List printer models:
```sh
lpinfo -m
```

## lp

Print files to default printer (`PRINTER` env var):
```sh
lp myfile.ps
```

Print to a specific printer:
```sh
lp -d myprinter myfile
```

Print to a specific server:
```sh
lp -h myserver -d myprinter myfile
```
See `cupsd` and `client.conf`.

Print 4 input pages on each output page:
```sh
lp -o number-up=4 ...
```
## lpr

Print file.
## lpstat

List all printer queues with their state:
```sh
lpstat -a
```

List printers & queues:
```sh
lpstat -e
```

List printers & queues using specific CUPS server:
```sh
lpstat -h myserver -e
```

List available printers:
```sh
lpstat -p -d
```

## lpoptions

Show printer queue information:
```sh
lpoptions -p my_queue_name
```

## scanimage

Scanner software.

Install wireless package:
```sh
pacman -S sane-airscan
```

List devices:
```sh
scanimage -L
```

Scanning:
```sh
scanimage --device airscan:w0:Brother --format=jpeg --resolution 200 --mode Color --output-file test.jpg
```
Use `--mode Gray` for black & white.

Scanning using document feeder:
```sh
scanimage --device airscan:w0:Brother --format=jpeg --resolution 200 --mode Color --source ADF --batch --batch-start 2
```
Default output format is `out%d.jpg`. To change it use `--batch=myfile_%d.jpg`.
Use `--batch-increment=2` or `--batch-double` to increment page index by 2.
Increment may be negative: `--batch-increment=-3`.

Get all options of the device:
```sh
scanimage --device airscan:w0:Brother -A
```
