void search_prime_numbers(int max) {
    
    int primes[max];
    int n = 0;
    int i = 2;
    
    while (n < max - 1) {
        
        int prime = 1;
        
        if (n > 0) {
            int *p = primes;
            int *end = primes + n - 1;
            while (p != end)
                if (i % *(p++) == 0) {
                    prime = 0;
                    break;
                }
        }
        
        if (prime) {
            primes[n++] = i;
            printf(" %d", i);
        }
        
        ++i;
    }
    printf("\n");
}

int main(int argc, char* argv[]) {

    search_prime_numbers(100);

    return 0;
}
