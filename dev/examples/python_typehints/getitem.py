import typing

class B:
    def __init__(self, n :int):
        self._n = n
    @property
    def n(self) -> int:
        return self._n
    def __repr__(self) -> str:
        return f"B{self._n}"

class A:

    def __init__(self) -> None:
        self._x: typing.List[B] = [B(1), B(2), B(3)]

    @typing.overload
    def __getitem__(self, k: int) -> B:
        pass

    @typing.overload
    def __getitem__(self, k: slice) -> typing.List[B]:
        pass

    # error: An overloaded function outside a stub file must have an
    # implementation  [no-overload-impl]
    # note: See https://mypy.rtfd.io/en/stable/_refs.html#code-no-overload-impl
    # for more info
    def __getitem__(self, k): # type: ignore
        if isinstance(k, int):
            return self._x[k]
        return self._x[k.start:k.stop]

a: A = A()
s: B = a[0]
print(a[0])
l: typing.List[B] = a[0:2]
print(a[0:2])
print(a[0].n)
