import io
import os
import py7zr
import tqdm

FILE_TO_COMPRESS = "foo.txt"
ARCHIVE_FILE = "myfile.7z"
N = 1024*1024*32

class FileProgress(io.FileIO):

    def __init__(self, file, mode, desc = ''):
        size = os.path.getsize(file) if 'r' in mode else 0
        self._bar = tqdm.tqdm(total = size, desc = desc)
        self._last_pos = 0
        io.FileIO.__init__(self, file = file, mode = mode)

    def _bar_update(self):
        cur_pos = self.tell()
        self._bar.update(cur_pos - self._last_pos)
        self._last_pos = cur_pos

    def read(self, size):
        self._bar_update()
        return super().read(size)

    def write(self, b):
        self._bar_update()
        return super().write(b)

# Create a big file to compress
with FileProgress(FILE_TO_COMPRESS, "w",
                  desc = f"Create file {FILE_TO_COMPRESS}") as f:
    for _ in range(N):
        f.write(b"0123456789ABCDEF")

# Create an archive
with py7zr.SevenZipFile(FileProgress(ARCHIVE_FILE, 'w',
                                     desc = f"Create archive {ARCHIVE_FILE}"),
                        'w'
                        ) as f:
    f.write(FILE_TO_COMPRESS)

# Uncompressing
with py7zr.SevenZipFile(
        FileProgress(ARCHIVE_FILE, 'r',
                     desc = f"Inflatting archive {ARCHIVE_FILE}"),
                    ) as f:
    f.extractall()
