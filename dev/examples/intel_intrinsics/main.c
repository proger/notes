#include <emmintrin.h>
#include <stdio.h>

int main(int argc, char* argv[]) {

  unsigned long n[] = {2, 0};
  printf("Unsigned long (%d bytes) memory content: ", sizeof(n));
  char* p = (char*)n;
  for (int i = 0 ; i < sizeof(n) ; ++i)
    printf("%02hhx", p[i]);
  printf("\n");

  __m128i i = _mm_loadu_si128((__m128i*)(n));

  char* x = (char*)&i;
  printf("__m128i register content: ");
  for(int i = 0; i < 16; ++i)
    printf("%02hhx", x[i]);
  printf("\n");

  return 0;
}
