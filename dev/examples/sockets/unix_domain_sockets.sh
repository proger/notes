set -e -o pipefail

# Open FIFOs
INPUT=my_input
[[ -e $INPUT ]] && unlink "$INPUT"
mkfifo -m 600 "$INPUT"
OUTPUT=my_output
[[ -e $OUTPUT ]] && unlink "$OUTPUT"
mkfifo -m 600 "$OUTPUT"

# Start comm
socket_file=$(mktemp -t "socket_example.XXXXXX")
(cat "$INPUT" | nc -U -u -s "$socket_file" "$socket_file" >"$OUTPUT")&
file "$socket_file"
ls -lah "$socket_file"
NCPID=$!
trap "kill $NCPID" EXIT

# Plug streams on FIFOs
exec 4>"$INPUT"
exec 5<"$OUTPUT"

# Send message
echo "Hello" >&4

# Receive reponse
read -u 5 -r RESPONSE
echo "Response: \"$RESPONSE\""

# Stop comm
kill $NCPID
trap - EXIT
unlink "$socket_file"
