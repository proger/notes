import argparse
import os
import socket

SERVER_SOCKET_FILE = "/tmp/socket_test_server_python"
CLIENT_SOCKET_FILE = "/tmp/socket_test_client_python"
HANDSHAKE_MSG = b"Hello"
HANDSHAKE_REPLY = b"Hi"

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--server', action='store_true',
                    help='Run as server.')
args = parser.parse_args()

# Server
if args.server:
    server_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

    if os.path.exists(SERVER_SOCKET_FILE):
      os.remove(SERVER_SOCKET_FILE)

    server_socket.bind(SERVER_SOCKET_FILE)

    msg, client = server_socket.recvfrom(len(HANDSHAKE_MSG))

    print("RECEIVED FROM CLIENT: " + str(msg))
    server_socket.sendto(HANDSHAKE_REPLY, client)

# Client
else:
    s = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    if os.path.exists(CLIENT_SOCKET_FILE):
      os.remove(CLIENT_SOCKET_FILE)
    s.bind(CLIENT_SOCKET_FILE)
    s.sendto(HANDSHAKE_MSG, SERVER_SOCKET_FILE)
    reply = s.recvfrom(len(HANDSHAKE_REPLY))
    print("RECEIVED FROM SERVER: " + str(reply))
