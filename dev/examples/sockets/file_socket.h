#include <sys/un.h>
#include <sys/socket.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#define BUF_SIZE 32

#define SV_SOCK_PATH "/tmp/ud_ucase"

#define ERROR(msg) { printf(msg); exit(1); }
#define ERROR_1(msg, arg1) { printf(msg, arg1); exit(1); }
