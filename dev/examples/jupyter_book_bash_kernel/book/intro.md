---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Bash
  language: bash
  name: bash
---

```{code-cell}
echo -e "ABC\nDEF\nGHI\n"
```

```{code-cell}
ls -1 /
```

```{code-cell}
cat ~/.bashrc
```
