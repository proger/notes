# Debian/Ubuntu
if command -v apt ; then
    sudo apt-get update
    sudo apt-get install -y python3.11-venv
fi

if command -v python ; then
    PYTHON=python
elif command -v python3 ; then
    PYTHON=python3
else
    echo "No Python installed."
    exit 1
fi
$PYTHON --version
[[ -d myvenv ]] || $PYTHON -m venv myvenv
source myvenv/bin/activate
python -m pip install \
    jupyterlab jupyter-book jupytext \
    bash_kernel \
    nbconvert==7.13.1 
    #jupyterlab-pygments
    #jupyterlab-pygments==0.2.2 
    #jupyter==1.0.0 jupyter_core==5.3.1 jupyter-events==0.7.0 \
    #jupyter-lsp==2.2.0 jupyter_server==2.7.3 jupyter_server_terminals==0.4.4 \
    #jupyterlab_server==2.25.0 \
    #jupyterlab-widgets==3.0.9 \
    #metakernel==0.30.1 \
    #nbformat==5.9.2 
    #notebook_shim==0.2.3
    #nbconvert==7.13.1   # OK
    #nbconvert==7.14.2  # Broken
    #nbconvert==7.15.0  # Broken
    #nbconvert==7.9.2  #OK
    #nbconvert==7.8.0  #OK
    #
    #jupyterlab==3.5.2 jupyter-book==0.15.1 jupytext==1.15.2 \
    #bash_kernel==0.9.1 \
    #nbconvert==7.13.1
python -m bash_kernel.install
jupytext -o - --to ipynb book/intro.md | jupyter nbconvert --execute \
    --to notebook --stdin --stdout >intro_executed.ipynb
cat intro_executed.ipynb
