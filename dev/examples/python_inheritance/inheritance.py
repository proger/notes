class A:
    def __init__(self):
        print("Init class A START")
        super().__init__()
        print("Init class A END")

class B:
    def __init__(self):
        print("Init class B START")
        super().__init__()
        print("Init class B END")

class C(A):
    def __init__(self):
        print("Init class C START")
        super().__init__()
        print("Init class C END")

class D(C, B):
    def __init__(self):
        print("Init class D START")
        super().__init__()
        print("Init class D END")

d = D()
