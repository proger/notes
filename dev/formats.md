# File formats
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','bash':'sh','yaml':'','python':''} -->

## BAM

 * [Binary Alignment Map](https://en.wikipedia.org/wiki/Binary_Alignment_Map).
 * [BAM File Format](https://support.illumina.com/help/BS_App_RNASeq_Alignment_OLH_1000000006112/Content/Source/Informatics/BAM-Format.htm).

Tools & libraries:
 * [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/).
 * [Picard Tools](http://broadinstitute.github.io/picard/). Java.

## BED

 * [UCSC Genome Browser's BED Format](https://genome.ucsc.edu/FAQ/FAQformat.html#format1).

 * [BED Tools](http://bedtools.readthedocs.org/).
 * [pybedtools](https://daler.github.io/pybedtools/).

## Coverage files

Format reference ?
Parser libraries ?

Dans FromAG on a un couple fichier CSV sans ligne d'en-tête et un
fichier à part contenant uniquement la ligne d'en-tête.

`chr1_50.bed_TOTAL.stats.gz`:
```
chr1	955503	39.1936	40	1	1	0.998812	0.988124	0.957245	0.891924	0.78266	0.513064	0.186461	0
chr1	955503	39.1936	40	1	1	0.998812	0.988124	0.957245	0.891924	0.78266	0.513064	0.186461	0
chr1	955503	39.1936	40	1	1	0.998812	0.988124	0.957245	0.891924	0.78266	0.513064	0.186461	0
```

`header_stats.gz`:
```
chr	pos	mean	mediane	1	5	10	15	20	25	30	40	50	100
```

Dans BEX on a un triplet:

`test_coverage_cnd.gz`:
```
chr1	0	0
chr2	1	0
chr3	2	0
```

`test_coverage_flags.txt.gz`:
```
chr1	0	1	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
chr2	1	2	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
chr3	2	3	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0
```

`test_coverage_flag_headers.txt.gz`:
```
chr	start	end	AGILENT_V4_TARGET	AGILENT_V4_TILED	AGILENT_V5_TARGET	AGILENT_V5_TILED	AGILENT_V5UTR_TARGET	AGILENT_V5UTR_TILED	AGILENT_V6_TARGET	AGILENT_V6_TILED	AGILENT_V6UTR_TARGET	AGILENT_V6UTR_TILED	AGILENT_V6COSMIC_TARGET	AGILENT_V6COSMIC_TILED	AGILENT_V7_TARGET	AGILENT_V7_TILED	RefSeq_37_Exons	RefSeq_37_CodingExons	GIAB_37
```
## EXIF

EXIF is an image file format created for digital cameras.
It can contain JPEG or TIFF, plus metadata like GPS location.
## FASTA

 * [FASTA format](https://en.wikipedia.org/wiki/FASTA_format).
 * [FASTA Format for Nucleotide Sequences](https://www.ncbi.nlm.nih.gov/genbank/fastaformat/).

Usually lines' length is 80 characters.
## FASTQ

 * [FASTQ format](https://en.wikipedia.org/wiki/FASTQ_format).
 * [FASTQ files explained](https://knowledge.illumina.com/software/general/software-general-reference_material-list/000002211). With explanation about paired FASTQ files.
 * [IUPAC-IUB codes](https://www.gendx.com/SBTengine/Help_220/hs310.htm).

Tools & libraries:
 * [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/).

Format for storing biological sequences.
Used in output of high-throughput sequencing instruments.
## HDF5

 * [SOFTWARE USING HDF5](https://support.hdfgroup.org/products/hdf5_tools/).
## JSON

Reindent JSON within VIM:
```vim
:%!python -m json.tool
```

### Python

 * [json](https://docs.python.org/fr/3/library/json.html).
 * [Schema](https://github.com/keleshev/schema).
 * [Validate YAML in Python with Schema](https://www.andrewvillazon.com/validate-yaml-python-schema/).

#### json module

Dump JSON into a string:
```python
import json
s  = json.dumps(myObj)
```

### R

#### jsonlite

 * [jsonlite](https://cran.r-project.org/web/packages/jsonlite/index.html).

Parse JSON:
```r
jsonlite::fromJSON(content, simplifyDataFrame=FALSE)
```

Build JSON string:
```r
jsonlite::toJSON(x, pretty=TRUE, digits=NA_integer_)
```
The `digits` parameter is set to max precision (`NA`).

### jq

A command-line JSON processor.

### Check JSON file

```bash
python -m json.tool <myfile.json
```

### Comments

Comments are not allowed inside JSON files.

### Lists / dictionaries

```json
{"success":false,"error":"token_required"}
```

### Array

```json
["val1", "val2"]
```
## NBIB

PubChem format for citations, inspired from RIS format.
## RIS

 * [RIS (file format)](https://en.wikipedia.org/wiki/RIS_(file_format)).

A citation format.
## SVG

 * [Scalable Vector Graphics](https://www.w3.org/TR/2001/REC-SVG-20010904/).
 * [Color names](https://www.w3.org/TR/SVG/types.html#ColorKeywords).
 * [SVG in HTML](http://www.w3schools.com/svg/svg_inhtml.asp).

### Markers

 * [Markers](http://wphooper.com/svg/examples/markers.php).

### Gradient

```svg
<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1 1" preserveAspectRatio="none">
    <linearGradient id="grad-ucgg-generated" gradientUnits="userSpaceOnUse" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" stop-color="#ffffff" stop-opacity="0"/>
        <stop offset="100%" stop-color="#ff0000" stop-opacity="1"/>
    </linearGradient>
    <rect x="0" y="0" width="1" height="1" fill="url(#grad-ucgg-generated)" />
</svg>
```
## TOML

 * [TOML](https://en.wikipedia.org/wiki/TOML).

## vcard

 * [vCard](https://en.wikipedia.org/wiki/VCard).
 * [vCard Format Specification](Based on https://tools.ietf.org/html/rfc6350).
## VCF
<!-- vimvars: b:markdown_embedded_syntax={'python':'','sh':'bash','bash':''} -->

Variant Call Format.

 * [SAM/BAM and related specifications](https://samtools.github.io/hts-specs/).
   + [The Variant Call Format (VCF) Version 4.2 Specification](https://samtools.github.io/hts-specs/VCFv4.2.pdf).
   + [The Variant Call Format (VCF) Version 4.5 Specification](https://samtools.github.io/hts-specs/VCFv4.5.pdf).

Tools & libraries:
 * `bcftools`
 * [PyVCF3](https://github.com/dridk/PyVCF3). OK.
 * [PyVCF](https://github.com/jamescasbon/PyVCF). Not maintained. Continued with PyVCF3.
 * [fuc.pyvcf](https://sbslee-fuc.readthedocs.io/en/latest/api.html#module-fuc.api.pyvcf).
 * [VCFPy](https://pypi.org/project/vcfpy/).
 * [VCF Parser](https://github.com/moonso/vcf_parser). Not maintained.
 * [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/).
 * [Picard Tools](http://broadinstitute.github.io/picard/). Java.

### bcftools

Merge two VCFs:
```sh
bcftools annotate -a a.vcf b.vcf
```

### PyVCF3

 * [PyVCF3](https://github.com/dridk/PyVCF3).

```python
import vcf
```

```python
vcf_reader = vcf.Reader(filename=file)
```
## xidel

Install on Archlinux:
```sh
yay -S xidel
```


## xmllint

On UNIX, to reindent an XML run:
```sh
xmllint --format myfile.xml >myfile_indented.xml
```

Do the same with an HTML file:
```sh
xmllint --format --html myfile.html >out.html
```

Reindent in VIM buffer:
```vim
%!xmllint --format -
```

To check syntax of an XML file:
```sh
xmllint --noout myfile.xml
```

To check using a schema:
```sh
xmllint --noout --schema myschema.xml myfile.xml
```
## XML
<!-- vimvars: b:markdown_embedded_syntax={'xml':'','bash':'sh','sh':'','xslt':''} -->

### xmlstarlet

 * [Adding a subnode to an XML file](https://www.technomancy.org/xml/add-a-subnode-command-line-xmlstarlet/).

#### sel (select)

Select all elements of a same type using XPath:
```sh
xmlstarlet sel -t -c '//b' -n myxml.xml 
```
	-n : new line
	-c : copy XPath selection

To print only the text (i.e.: values):
```sh
xmlstarlet sel -t -v '//b' -n myxml.xml 
```

Use a namespace:
```sh
xmlstarlet sel -N x="http://my.name/space" -t -v '//x:b' -n myxml.xml 
```

#### ed (edit)

Delete elements using XPath:
```sh
xmlstarlet ed -d '//my/tag' myxml.xml
## or
xmlstarlet ed --delete '//my/tag' myxml.xml
```

Edit in-place:
```sh
xmlstarlet ed -L ...
## or
xmlstarlet ed --inplace ...
```

Replace value:
```sh
xmlstarlet ed -N x="http://maven.apache.org/POM/4.0.0" -u '/x:project/x:version' -v $version "$SCRIPT_DIR/../pom.xml"
```

#### tr (transform)

Transform an XML file using XSLT:
```sh
xmlstarlet tr myxslt.xsl my.xml
```
Note that `xmlstarlet` is limited to XSLT 1.1.

### saxon

Java library and utilities for running XML tools (XSLT, XQuery, ...).
Latest version is able to run XSLT 2 and XSLT 3.

On ArchLinux, install package `saxon-he` to get the tools to process XSLT 2 and XSLT 3.

Running an XSLT:
```sh
saxon-xslt -s:my.xml -xsl:my_xslt.xsl
```

To put output in a file:
```sh
saxon-xslt -s:my.xml -xsl:my_xslt.xsl -o:my_output.xml
```

### XPath

 * [XPath Tutorial](http://www.w3schools.com/xsl/xpath_intro.asp).

To run an XPath command in a terminal on UNIX-like systems:
```bash
xmllint --xpath '//element/@attribute' file.xml
```

To look for a node anywhere in the tree:
```
//mynode
```

To look for a node using a namespace ns:
```
//ns:mynode
```

To look for all nodes under another one:
```
/my/path/*
/my/path//*
```

To search for all nodes:
```
//*
```

To search for a node with text:
```
//mynode[text()='abc']
//mynode[starts-with(.,'Test')]
//mynode[contains(text(), 'Yahoo')]
```

To search for a node with a specific attribute:
```
//title[@lang]
//title[@lang='eng']
```

To search for a node that does not have a certain attribute:
```
//title[not(@lang)]
```

Get an attribute's value:
```
//mynode/@myattrb
```

Search a tag and the go up to find next tag of the same type:
```
//td[text()='InChIKey']/../td[2]
```

### XSLT

 * [FunctX XSLT Functions](http://www.xsltfunctions.com/xsl/).
 * [XSL Transformations (XSLT) Version 2.0](http://www.w3.org/TR/xslt20/).
 * [Regular Expression Matching in XSLT 2](https://www.xml.com/pub/a/2003/06/04/tr.html).

XSL = eXtensible Stylesheet Language.
XSLT = XSL Tranformations.

An XSLT file is an XSL file with special elements in it. It start tag may be "<xsl:stylesheet>" as for an XSL but the "<xsl:transform>" equivalent (i.e.: synonym) might be preferable for clarity:
```xslt
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
...
</xsl:transform>
```

Identity in XLST 3.0:
```xslt
<xsl:transform version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:mode on-no-match="shallow-copy" />
</xsl:transform>
```

Identity in XSLT 1.0:
```xslt
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:transform>
```
Can also be written explicitly as:
```xslt
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="@*|*|processing-instruction()|comment()">
		<xsl:copy>
			<xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()"/>
		</xsl:copy>
	</xsl:template>
</xsl:transform>
```
Matches explanations:
 * `*`: any element.
 * `@*`: any attribute.
 * `text()`: element text content.
 * `processing-instruction()`: ???
 * `comment()`: comments.

Replace text in elements (XSLT 3):
```xslt
<xsl:transform version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:mode on-no-match="shallow-copy"/> <!-- Copy all non-matched elements -->
	<xsl:template match="b"> <!-- match an element -->
		<xsl:copy>
			<xsl:apply-templates select="*|@*"/> <!-- copy all children and attributes -->
			<xsl:value-of select='replace(., "Some","XXX")'/> <!-- replace text content -->
		</xsl:copy>
	</xsl:template>
</xsl:transform>
```

### XProc

### XQuery


### XML schema

 * [XML Schema Tutorial](https://www.w3schools.com/xml/schema_intro.asp).

XML Schema (aka XSD) is the language that is used to describe the structure of an XML.

Element with children:
```xml
<xs:element name="some_element_name">
  <xs:complexType>
    <xs:sequence> <!-- order indicator, see below -->

			<!-- children -->
				
    </xs:sequence>
  </xs:complexType>
</xs:element>
```

Order indicators for children:
```xml
<xs:sequence> <!-- order of children must be in the specified order -->
	<!-- children -->
</xs:sequence>

<xs:all> <!-- children can appear in any order, but can only appear once (or not at all) -->
	<!-- children -->
</xs:all>

<xs:choice> <!-- only one of the possible children can appear -->
</xs:choice>
```

Occurrence indicators.
They are used inside order indicators to define how often a child can occur.
```xml
<xs:sequence>
  <xs:element name="full_name" type="xs:string" maxOccurs="unbounded"/> <!-- this element can appear an unlimited number of times -->
  <xs:element name="child_name" type="xs:string" maxOccurs="10" minOccurs="0"/>
</xs:sequence>
```

Enumeration:
```xml
<xs:element name="car">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:enumeration value="Audi"/>
      <xs:enumeration value="Golf"/>
      <xs:enumeration value="BMW"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```

Group:
```xml
<xs:group name="some_group">
  <xs:sequence>
    <xs:element name="customer" type="xs:string"/>
    <xs:element name="orderdetails" type="xs:string"/>
    <xs:element name="billto" type="xs:string"/>
    <xs:element name="shipto" type="xs:string"/>
  </xs:sequence>
</xs:group>

<xs:element name="some_element">
	<xs:group ref="some_group"/>
</xs:element>
```

Header:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
	
</xs:schema>
```


Complex type:
```xml
<xs:complexType name="some_complextype">
  <xs:sequence>
		<xs:element name="an_element" type="xs:string"/>
		<xs:element name="another_element" type="xs:string"/>
	</xs:sequence>
</xs:complexType>

<xs:element name="some_element" type="some_complextype"/>
```

Whitespace restriction.
	preserve = don't touch anything
	replace = replace all whitespace chars by space
	collapse = "replace" + reduce sequence of spaces with one single space
```xml
<xs:element name="address">
  <xs:simpleType>
    <xs:restriction base="xs:string">
      <xs:whiteSpace value="preserve"/>
    </xs:restriction>
  </xs:simpleType>
</xs:element>
```
## YAML
<!-- vimvars: b:markdown_embedded_syntax={'yaml':'','sh':'bash'} -->

 * [YAML](https://en.wikipedia.org/wiki/YAML).
 * [Official YAML website](http://yaml.org/).
 * [YAML syntax validator](http://www.yamllint.com/).

See yq and shyaml for YAML command line processors.

A list:
```yaml
---
- item1
- item2
- item3
```
or
```yaml
---
[item1, item2, item3]
```

An associative array:
```yaml
---
key1: value1
key2: value2
```
or
```yaml
---
{key1: value1, key2: value2}
```

### shyaml

A Python command line tool to access YAML files.
Installing using pip:
```sh
pip install shyaml
```

Installing with yay under ArchLinux:
```sh
yay -S shyaml
```

Reading a field value (fails if no key):
```sh
shyaml get-value field1.field2.field3 <my.yml
```

Read a field value and return a default value if keys is not found:
```sh
shyaml get-value field1.field2.field3 myDefaultValue <my.yml
```

To test if key exists:
```sh
shyaml get-value field1.field2 <my.yml 2>/dev/null || echo NULL
```

### C++

 * [yaml-cpp](https://github.com/jbeder/yaml-cpp).

Compile, using cmake 3.19.1:
```sh
cmake -S . -B build -DCMAKE_INSTALL_PREFIX=/my/destination/folder -DCMAKE_BUILD_TYPE=Release -DYAML_BUILD_SHARED_LIBS=on
cmake --build build -j 8
cmake --install build
```

### Python

 * [Schema](https://github.com/keleshev/schema).
 * [YAML Schemas: Validating Data without Writing Code](https://www.codethink.co.uk/articles/2021/yaml-schemas/).
 * [Welcome to PyYAML](https://pyyaml.org/).
   + [PyYAML Documentation](https://pyyaml.org/wiki/PyYAMLDocumentation).
 * [yamlloader](https://github.com/Phynix/yamlloader).
   + [yamlloader Documentation](https://yamlloader.readthedocs.io/).

```python
import yaml
```

Load YAML file:
```python
import yaml
with open("myfile.yml", "r") as f:
    docs = yaml.safe_load_all(f)
```

Save YAML file:
```python
with open("myfile.yml", "w") as f:
    yaml.safe_dump_all([my_data], f, explicit_start=True)
```


`load()` method can load any type of Python objects from YAML file. It is thus unsafe if untrusted YAML is loaded.
Method `load_all()` allows to load YAML file containing multiple documents.
Prefer `safe_load()` and `safe_load_all()` methods that forbid Python objects.

To load and save while preserving order of keys, use `yamlloader` package:
```python
import pathlib
import yaml
import yamlloader

my_file = pathlib.Path("...")
with my_file.open("r") as f:
    my_doc = yaml.load(f, Loader = yamlloader.ordereddict.CSafeLoader)

with my_file.open("w") as f:
    yaml.dump(my_doc, f, sort_keys = False, explicit_start = True,
              Dumper = yamlloader.ordereddict.CSafeDumper)
```

### Ruby

```ruby
require 'yaml'
mydata = YAML.load_file('myfile.yml')
```

### Comments

```yaml
---
# mycomment
```

### Alignment

Attention at putting rightmost dashes without spaces before.
Use spaces to align and not tabs.
Align strictly the fields that are on a same level.
```yaml
--- 
- 
  hosts: galaxyservers
  roles: 
    - 
      role: ansible-galaxy
      sudo: true
  vars: 
    galaxy_changeset_id: tip
    galaxy_config_dir: /opt/galaxy/config
    galaxy_mutable_config_dir: /var/opt/galaxy/config
    galaxy_mutable_data_dir: /var/opt/galaxy/data
    galaxy_repo: "https://bitbucket.org/galaxy/galaxy-central/"
    galaxy_server_dir: /opt/galaxy/server
```

### Conversion from JSON

Converting a file from JSON to YAML:
```bash
python -c 'import sys, yaml, json; yaml.safe_dump(json.load(sys.stdin), sys.stdout, default_flow_style=False)' < file.json > file.yaml
```

### Split strings and multiline text

 * [How do I break a string in YAML over multiple lines?](https://stackoverflow.com/questions/3790454/how-do-i-break-a-string-in-yaml-over-multiple-lines).

In a multiline text, if you want to escape colon char (`:`):
```yaml
---
-
  field:
    - some text
    - some other text
    - |-
        "some text with a colon : inside it"
```
Quoting is not enough.

Split a string on multiple lines and get only one new line character at the end:
```yaml
myfield: >
    Some text
    On two lines.
```
The same without the ending new line character:
```yaml
myfield: >-
    Some text
    On two lines.
```

If you want to preserve the internal new lines use `|`:
```yaml
myfield: |
    Some text
    On two lines.
```
And if you want all the internal new lines but not the ending one:
```yaml
myfield: |-
    Some text
    On two lines.
```

### Null value

Write a null value in YAML:
```yaml
---
myfield1: ~
myfield2:
myfield3: null
myfield3: Null
myfield3: NULL
```

### Multiple sections

The `---` line indicates the start of a new data set. Thus you can store multiple YAML "files" inside a single YAML file:
```yaml
---
field1: value1
field2: value2
---
fieldOfAnotherDataset: someValue
```

### Anchor

 * [YAML anchors](https://support.atlassian.com/bitbucket-cloud/docs/yaml-anchors/).

An anchor allow to copy a subset at another place.

```yaml
---
a: &a
  i: 10
  ii: 20
  iii:50
b:
  - *a

```
## yq

 * [yq: Command-line YAML/XML/TOML processor - jq wrapper for YAML, XML, TOML documents](https://github.com/kislyuk/yq).

A YAML processor, that uses jq (JSON processor) behind.

Use `-Y` to preserve YAML and write YAML on output.

Read a value:
```sh
yq .my.path.to.the.value myfile.yml
```

Read two values:
```sh
yq -Y .first.value,.second.value myfile.yml
```

Change a value (new YAML is output on stdout):
```sh
yq -Y .my.key=newValue myfile.yml
yq -Y '.my.key="new value"' myfile.yml # with a space
yq -Y ".my.key=\"new value\"" myfile.yml # with a space
```

Write a value in-place:
```sh
yq -Yi .my.key=newValue myfile.yml
```
ATTENTION: The `-i` has failed on Ubuntu 20.04, writing an empty file.

Delete a key:
```sh
yq 'del(.my.key)' myfile.yml
```
