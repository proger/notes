# MPI

 * [MPI Tutorials](https://mpitutorial.com/tutorials/).

RUNNING
=======

`mpirun` (or `mpiexec` or `orterun`) is the executable used to run an MPI program.

mpirun runs an MPI program:
```bash
mpirun myprogram
```

Setting number of copies to execute:
```bash
mpirun -n 6 myprogram
```

Tag output line with rank prefix:
```sh
mpiexec --tag-output -n 6 myprogram
```

INIT & FINALIZE
===============

`MPI_Init` is used to onitialize the global communicator `MPI_COMM_WORLD`.
`MPI_Init` takes command line arguments, but we can give `NULL` pointers instead. It removes MPI arguments given to `mpirun`, and thus allows to see command line arguments as if the program was run without MPI.

```c
if (MPI_Init(&argc, &argv))
	/* error ! */;

/*...*/

if (MPI_Finalize())
	/* error ! */;
```

======================== Distributed-memory model.txt
In distributed-memory programs, the cores can directly access only their own, private memories. So they must use other way to communicate.

Message-passing
===============
A get_rank() function allows a process to know its rank.

Receive() and send() functions allows passing of messages from one processor to another.
One process must call a send function and the send must be matched by another process' call to a receive function.
Send() and receive() can both be blocking or not.

Usually broadcast is also possible.

The most widely used API is MPI (Message Passing Interface).

One-sided communication (remote memory access)
==============================================
A single process calls a function, which updates either local memory with a value from another process or remote memory with a value from the calling process.

Problems:
1) A process copying a value to another process' memory, must first know if it's safe to copy. Since is insured by synchronization.
2) A process must have some way to know when another process has updated its memory. This is insured by some flag updated by the other process, and repeatedly polled by the process.

Partitioned global address space languages
==========================================
Aim: use shared-memory programming techniques to program a distributed-memory system.

Private variables are allocated in the local memory of the core on which the process is executing, and the distribution of data in shared data structures is controlled by the programmer.
======================== IBM.txt
# vi: ft=sh

# On IBM clusters, commands are:
llsubmit  sub.ll    # for launching a job
llq                 # to list the job queue
llcancel            # to cancel a job
======================== MPI.txt
MPI is a standard for parallel computing.
It implements SPMD model.

http://www.mpi-forum.org/

See "Open MPI" for an implementation.
======================== Open MPI.txt
# vi: ft=sh

# http://www.open-mpi.org/

# MACOS-X INSTALLATION
#=====================
# update Xcode to latest version
# install command line tools of Xcode
brew install gfortran
export FC=/usr/local/bin/gfortran
# 2012-12-10. Make sure CUDA is version 4.0 and not 5.0 or higher. Open-mpi only support CUDA version 4.0 for its module vt. /usr/local/cuda must point to version 4.0.
brew install open-mpi
======================== barrier.txt
/* vi: se ft=C: */

/* MPI_Barrier synchronizes processes.
   The function insures that no process will return calling it until every process in the communicator has started calling it. */
MPI_Barrier(MPI_COMM_WORLD); 
======================== broadcast.txt
/* vi: se ft=C: */

/* The broadcast function allows to distribute the same data to all processors, using a tree structure for communication.
   MPI_Bcast() does the send and the receive, so recipients mustn't call MPI_Recv().
 */
int MPI_Bcast(
		void*         data,
		int           count,    /* dimension of data. 1 if scalar */
		MPI_Datatype  datatype,
		int           source,   /* the processor sending the data */
		MPI_Comm      communicator);
======================== communication_protocols.txt
_ rendez-vous protocol: The sender and the receiver shake hands before starting communication, so it introduces an overhead. It has high bandwidth, and so is good for big messages.
_ eager protocol: low bandwidth, good for small messages.

MPI can choose automatically for each case between the both communication solution. See "eager limit" and how to set it.
======================== communicator.txt
/* vim: set ft=C: */

/* A communicator (type MPI_Comm) is a collection of processes that can send messages to each other.

   The global communicator MPI_COMM_WORLD allows to send messages to all the processes. It is initialized by MPI_Init().
 */

/* Getting number of processes inside a communicator */
int comm_sz;
MPI_Comm_size(some_communicator, &comm_sz);

/* Getting the rank of current process inside a communicator */
int my_rank;
MPI_Comm_rank(some_communicator, &my_rank);
MPI_PROC_NULL; /* Special rank defined by MPI. When used inside a point-to-point communication, no communication will take place. */

/* intra-communicator: inside one group. */
/* inter-communicator: between groups. */
======================== derived datatypes.txt
/* vi: se ft=C: */

/* Create a structure */
int MPI_Type_create_struct(
		int           count
		int           blocklengths[], /* 1 if scalar, dim of array otherwise */
		MPI_Aint      displacements[], /* computed with MPI_Get_address */
		MPI_Datatype  types[],
		MPI_Datatype *new_type);

/* Get displacement for a variable */
int MPI_Get_address(
		void*     location,
		MPI_Aint* address);

/* Example */
#define N 3
MPI_Aint a_addr, b_addr, c_addr;
MPI_Get_address(a, &a_addr);
MPI_Get_address(b, &b_addr);
MPI_Get_address(c, &c_addr);
MPI_Aint displacements[N];
displacements[0] = 0;
displacements[1] = b_addr - a_addr;
displacements[2] = c_addr - a.addr;
int blocklengths[N] = {1, 1, 1};
MPI_Datatype types[N] = {MPI_DOUBLE, MPI_DOUBLE, MPI_INT};
MPI_Datatype my_datatype;
MPI_Type_create_struct(N, blocklengths, displacements, types, &my_datatype);
MPI_Type_commit(&my_datatype);
/* use of data type */
MPI_Bcast(&a, 1, my_datatype, 0, MPI_COMM_WORLD);
/* end of use of data type */
MPI_Type_free(&my_datatype);

/* Collection of contiguous elements (array) */
int MPI_Type_contiguous(
		int           count,
		MPI_Datatype  type,
		MPI_Datatype *new_type);

/* Collection of blocks of elements */
int MPI_Type_vector(
		int           count,
		int           blocklength,
		int           stride,
		MPI_Datatype  type,
		MPI_Datatype *new_type);
/* For instance to take 3 blocks of 2 elements spaced with a stride of 6,
   so that in a array will take elements at indexes 0,1,6,7,12,13 : */
MPI_Type_vector(3, 2, 6, MPI_DOUBLE, &my_type);

/* Take arbitrary elements from an array */
int MPI_Type_indexed(
		int           count, /* number of blocks */
		int           blocklengths[],
		int           displacements[], /* in sizeof(type) */
		MPI_Datatype  type,
		MPI_Datatype *new_type);
/* For instance to take the upper right triangular part of a 4x4 matrix : */
int blocklengths[] = {4, 3, 2, 1};
int displacements[] = {0, 5, 10, 15};
MPI_Type_indexed(4, blocklengths, displacements, MPI_DOUBLE, &my_type);

/* PACK & UNPACK
   Pack data into a buffer and unpack it.
 */
int MPI_Pack(
		void*         data,
		int           count,
		MPI_Datatype  type,
		void*         buf,
		int           but_size,
		int*          position,
		MPI_Comm      comm);
int MPI_Unpack(
		void*         buf,
		int           but_size,
		int*          position,
		void*         data,
		int           count,
		MPI_Datatype  type,
		MPI_Comm      comm);
/* Example: */
#define N 100
char pack_buf[N];
int position = 0;
if (my_rank == 0) {
	MPI_Pack(&a, 1, MPI_DOUBLE, pack_buf, N, &position, comm);
	MPI_Pack(&b, 1, MPI_DOUBLE, pack_buf, N, &position, comm);
	MPI_Pack(&n, 1, MPI_INT, pack_buf, N, &position, comm);
}
MPI_Bcast(pack_buf, N, MPI_PACKED, 0, comm);
if (my_rank != 0) {
	position = 0;
	MPI_Unpack(pack_buf, N, &position, &a, 1, MPI_DOUBLE, comm);
	MPI_Unpack(pack_buf, N, &position, &b, 1, MPI_DOUBLE, comm);
	MPI_Unpack(pack_buf, N, &position, &n, 1, MPI_INT, comm);
}
======================== non_blocking_messages.txt
/* vi: se ft=C : */

/*****************************
 * MPI non-blocking messages *
 *****************************/

MPI_Isend(...);
/* stuff */
if (MPI_Test(...)) /* Function that tests if a non-blocking message has been fully sent. */
	/* ... */

/* In non-blocking messaging, the message sending progresses only when inside the code of the MPI library. So an MPI function must be called in order for the message to be sent. It is thus important, after the call to MPI_Isend(), to call other functions like MPI_Test() in order to let a chance to the MPI library to send the message.
   This is the same issue on the receiver side.
 */
======================== process.txt
A Process consists of :
_ stack
_ heap
_ resource descriptors (e.g.: files)
_ security information (which hardware and software the process can access)
_ state (program counter, registers, iddle, ...)
======================== reduce.txt
/* vi: set ft=C: */

/* MPI_Reduce compute an operation distributed among the cores, using a tree structure.
   It makes the result avalaible only for the destination processor.
 */
int MPI_Reduce(
	void*        input,      /* input data */
	void*        output,     /* output data (result). Only useful for dest_process, others may set it to NULL. */
	int          count,      /* dimension of data. 1 if scalar. */
	MPI_Datatype datatype,   /* MPI_DOUBLE, ... */
	MPI_Op       operator,   /* The operator to use: MPI_MAX, MPI_SUM, ... It's possible to define your own operator. */
	int          dest_process,
	MPI_Comm     communicator);

/* Aliasing of output argument is forbidden.*/
MPI_Reduce(&x, &x, ...);
/* Which means that output and input can't point to the same memory location.
   This is because it's forbidden in Fortran, and MPI Forum wanted to make Fortran and C versions similar.
 */

/* Example with a sum */
MPI_Reduce(&local_sum, &total_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

/* Example with a vector */
double local_x[N], sum[N];
/*...*/
MPI_Reduce(local_x, sum, N, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

/* MPI_Allreduce makes the result avalaible for all processes.
   It may use:
      _ a REVERSE structure in which one processor compute the result and distribute it back to all other processor following a tree structure,
      _ a BUTTERFLY structure in which all processors exchange their partial sums, and compute each the total sum.
 */
int MPI_Allreduce(
	void*        input,
	void*        output,
	int          count,
	MPI_Datatype datatype,
	MPI_Op       operator,
	MPI_Comm     communicator);
======================== ring-pass.txt
The ring-pass communication algorithm is an algorithm in which processes pass messages to each other using a ring of communication.

For instance for N=4 processes :

	0 -----> 1
	^        |
	|        |
	|        |
	|        v
	3<-------2

So process 0 sends to 1 and receives from 3,
   process 1 sends to 2 and receives from 0,
   etc.

If each process sends & receives N times, resending the messages it receives, then at the end each message will have gone through all processes, returning to the process that emits it at the beginning.
======================== scan.txt
/* vi: se ft=C: */

/* MPI_Scan is used to compute prefix operations.
  For instance prefix sums are the following vector of sums:
  x0, x0+x1, x0+x1+x2, ..., x0+x1+...+xn-1 */
int MPI_Scan(
		void        *send_buf,
		void        *recv_buf,
		int          count,
		MPI_Datatype datatype,
		MPI_op       op,
		MPI_Comm     comm);
======================== scatter, gather.txt
/* vi: se ft=C: */

/* MPI_Scatter allows to divide data into memory block and distribute them among processes.
   It can only works if data size is a multiple of block size.
 */
int MPI_Scatter(
		/* only relevant for the source processor */
		void*        send_buf,
		int          send_count,  /* size of blocks */
		MPI_Datatype send_type,

		/* block of data received */
		void*        recv_buf,
		int          recv_count,
		MPI_Datatype recv_type,

		/* communication setup */
		int          source_processor,
		MPI_Comm     communicator);

/* MPI_Gather regroup scattered blocks */
int MPI_Gather(
		/* block of data sent */
		void*        send_buf,
		int          send_count,
		MPI_Datatype send_type,

		/* only relevant for the destination processor */
		void*        recv_buf,
		int          recv_count, /* the size of the blocks */
		MPI_Datatype recv_type,

		/* communication setup */
		int          destination_processor,
		MPI_Comm     communicator);

/* MPI_Allgather regroup scattered blocks inside a memory location for each processor. */
int MPI_Allgather(
		/* block of data sent */
		void*        send_buf,
		int          send_count,
		MPI_Datatype send_type,

		/* only relevant for the destination processor */
		void*        recv_buf,
		int          recv_count, /* the size of the blocks */
		MPI_Datatype recv_type,

		/* communication setup */
		MPI_Comm     communicator);

/* When data size is not a multiple of blocksize, we must use the function MPI_Scatterv and MPI_Gatherv. */ 
MPI_Scatterv(send_buf, 
		int *send_counts, /* array of size p containing the number of elements to send to each process */
		int *displs, /* array of size p containing the displacement relative to send_buf where to take the data to send to each process */
		send_type, recv_buf, recv_count, recv_type, source_processor, communicator);
MPI_Gatherv(send_buf, send_count, send_type, recv_buf, 
		int *recv_counts, /* array of size p containing number of elements received for each process */
		int *displs, /* array of size p containing the displacement relative to recv_buf where to put the received data of each process */
		recv_type, destination_processor, communicator);
/* p is the of processors in the communication group. */
======================== send _ receive.txt
/* vim: set ft=C: */

/* For a message to be successfully sent and received, the functions must set the same :
     _ message size
     _ type
     _ tag
     _ communicator
   and rank of destination and source must be correct.
 */

/* MPI standard requires that messages be nonovertaking. This means that if process q sends two messages to process r, then the first message sent by q must be available to r prior to the second message. */

/* Sending a message to another process */
if ( MPI_Send(buf, size, type/*MPI_CHAR, MPI_INT, MPI_PACKED, ...*/,
              dest_rank, tag, communicator))
	/* error ! */;
MPI_Ssend(); /* Synchronous send ==> will block until message is received. */

/* Receiving a message from a precise process */
if ( MPI_Recv(buf, size, type,
              source_rank, tag, communicator,
              MPI_STATUS_IGNORE /*status*/))
	/* error ! */;

/* If the source isn't important (e.g. we have a bunch of messages comming from all other processes, and we don't care in which order we receive them,
   we can use the MPI_ANY_SOURCE special flag instead of specifying the source rank. */
for (i = 1 ; i < comm_sz ; ++i)
	if ( ! MPI_Recv(buf, size, type, MPI_ANY_SOURCE, tag, communicator, MPI_STATUS_IGNORE))
		/* do something */;

/* If we don't care about the tag, we can set MPI_ANY_TAG flag */
	if ( ! MPI_Recv(buf, size, type, source_rank, MPI_ANY_TAG, communicator, MPI_STATUS_IGNORE))
		/* do something */;

/* Status.
   If status structure is passed to MPI_Recv, it will fill it with : the source rank, the tag, and the size of the message. */
MPI_Status status;
for (i = 1 ; i < comm_sz ; ++i)
	if ( ! MPI_Recv(buf, size, type, MPI_ANY_SOURCE, MPI_ANY_TAG, communicator, &status)) {
		int source_rank = status.MPI_SOURCE;
		int tag = status.MPI_TAG;
		int count;
		MPI_Get_count(&status, type, &count);
		/* do something */;
	}

/* MPI_Sendrecv(): Avoid blocking when exchanging messages in point-to-point communications. */
int MPI_Sendrecv(
		void         *send_buf,
		int           send_buf_size,
		MPI_Datatype  send_buf_type,
		int           dest,
		int           send_tag,
		void         *recv_buf,
		int           recv_buf_size,
		MPI_Datatype  recv_buf_type,
		int           source,
		int           recv_tag,
		MPI_Comm      comm,
		MPI_Status   *status);
/* If send and receive buffers are the same: */
int MPI_Sendrecv_replace(
		void         *buf,
		int           buf_size,
		MPI_Datatype  buf_type,
		int           dest,
		int           send_tag,
		int           source,
		int           recv_tag,
		MPI_Comm      comm,
		MPI_Status   *status);
======================== timing performance.txt
/* vi: se ft=C: */

/* MPI_Wtime() returns wall clock time in seconds, resolution is in milliseconds. */
double t = MPI_Wtime();

/* measure time elapsed */
double start, finish;
start = MPI_Wtime(); /* return number of seconds */
/* ... */
finish = MPI_Wtime();

/* in order to get the time elapsed for a collection of processors, we must compare all measures of time. */
double local_start, local_finish, local_elapsed, elapsed;
/* ... */
MPI_Barrier(comm);
local_start = MPI_Wtime();
/* ... */
local_finish = MPI_Wtime();
local_elapsed = local_finish - local_start;
MPI_Reduce(&locale_elapsed, &elapsed, 1, MPI_DOUBLE, MPI_MAX, 0, comm);

/* Variability of measures
   Running twice the program will never give the same measure of time, thus we need to run it
   several time and take the minimum (because it's what we will get on a "quiet" system) of the results. */
======================== types.txt
/* vim: set ft=C: */

MPI_CHAR		signed char;
MPI_SHORT		signed short int;
MPI_INT			signed int;
MPI_LONG		signed long int;
MPI_LONG_LONG		signed long long int;
MPI_UNSIGNED_CHAR	unsigned char;
MPI_UNSIGNED_SHORT	unsigned short int;
MPI_UNSIGNED		unsigned int;
MPI_UNSIGNED_LONG	unsigned long int;
MPI_FLOAT		float;
MPI_DOUBLE		double;
MPI_LONG_DOUBLE		long double;
MPI_BYTE;
MPI_PACKED;
======================== IO/ADIOS.txt
Work with its own file format, or with other formats (HDF5, NetCDF).

It's possible to redirect the data on the network, in order to display them instead of writing them to a file.

The APIs of reading of HDF5 and NetCDF formats, doesn't propose all the functionalities of the ADIOS format.

As of today (18/10/2013), this is a new library, a bit too young, and thus imperfect.

A drawback is that it's necessary to have the same number of processes to reread files and restart computing.
======================== IO/HDF5.txt
The HDF5 library uses MPI I/O.
The format of the file is a special one, and it is portable and stable.
======================== IO/MPI_IO.txt
MPI I/O is the standard MPI interface for accessing files.

It allows several processes to write together to the same file

As in separate_files.txt, there's no special format, and care must be taken of sizes and endianess.
======================== IO/NetCDF4.txt
Works with HDF5 files, but can also process NetCDF files.
======================== IO/Parallel_NetCDF.txt
Based on MPI I/O.

Uses the file format NetCDF (autodocumented and portable).
Parallel-NetCDF is separated development of NetCDF, splitted at a moment where NetCDF wasn't yet parallelized.

Documentation is pour.
======================== IO/lustre.txt
http://wiki.lustre.org/index.php/Main_Page

Lustre is a parallel file system.
======================== IO/parallel_IO.txt
On a cluster of CPUs, the way to write to files is to use a parallel file system.
There are several ones, the most used curently being LUSTRE (see lustre.txt).
======================== IO/separate_files.txt
In this solution, each process creates its own file.

The files are created the same way as on a normal machine (open, write, close), so the format is up to you.

Usually the files are written in binary format, for efficiency reason. So care must be taken about sizes of types (int, float, char, ...), and for integers about endianess.
