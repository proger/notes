/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

*************************************************************************
*
*	display.hc
*		read a 3D data base from a file with points and segments
*		display a 40 frame movie of the data base with a moving view point
*		Use a Bresenham algorithm
*/
#include <hyperc.hh>
#include <graphic.hh>
#include <stdio.hh>
#include <math.hh>

#define VX 256
#define VM (VX/2)
/*------ collection declaration -------------*/
collection point;			/* the collection of the points */
collection segment;			/* the collection of the segments */
collection [VX,VX] pixel;	/* the collection of the generated image */


pixel char image;			/* the generated image */

point float X,Y,Z;			/* the 3D position of the points */
point int PX,PY;			/* their projection on the screen */
segment link point P1,P2;	/* ends of the segments */
float X0 = 2.0 ,Y0 = 1.0 ,Z0 = 4.0;  	/* the eye position */
float focal = VM;						/* the focal */

/*----------------------------------------------------------------------------
 *	Function read_file :
 *		input : the name of the data base
 *		output : none
 *		side effect : load the data base.
 *			create the point collection and the segment collection
 *----------------------------------------------------------------------------*/
void read_file(char *name)
{
	FILE *f;
	int n_point,n_segment,i;
	char Line[80];

	f = fopen(name,"r");
	if(f == 0) {
		perror(name);
		exit(-1);
	}
	fgets(Line,80,f);
	sscanf(Line,"%d",&n_point);			/* reading the number of point */
		/* allocation of the point collection */
	HcAllocateCollection(&point,1,n_point);
	{
		point char Line[80];
		fgets(Line,80,f);
		/* reading the coordinates of all the points */
		sscanf(Line,"%g %g %g",&X,&Y,&Z);
	}
	fgets(Line,80,f);
	sscanf(Line,"%d",&n_segment);		/* reading the number of segments */
		/* allocation of the segment collection */
	HcAllocateCollection(&segment,1,n_segment);
	{
		segment char Line[80];
		segment int p1,p2,bad = 0;
		fgets(Line,80,f);
		/* reading the point number of the 2 sides of each segment */
		sscanf(Line,"%d %d",&p1,&p2);
		bad = 0;
		where(p1 < 0 || p1 >= n_point || p2 <0 || p2 >= n_point) {
			bad = 1;
		}
		if(|<- bad) {
			fprintf(stderr,"corrupted database\n");
			exit(-1);
		}
		/* P1 and P2 are links to the two points at the end of each segment */
		P1 = [p1]point;
		P2 = [p2]point;
	}
}

/*----------------------------------------------------------------------------
 *	Function perspective :
 *		input : none
 *		output : none
 *		side effect : projects the points on the screen.
 *			computes PX and PY from X,Y,Z.
 *			the center of the screen is always oriented to the 0,0,0 point
 *			of the data base
 *----------------------------------------------------------------------------*/
int perspective(void)
{
	double a,b,sina,sinb,cosa,cosb,d0;
	double c[9];

	a = atan(Y0/X0);
	b = atan(Z0/sqrt(X0*X0 + Y0 *Y0));
	d0 = sqrt(X0*X0 + Y0 *Y0 + Z0 * Z0);
	sina = sin(a);
	sinb = sin(b);
	cosa = cos(a);
	cosb = cos(b);
					
	/*

						cosa  -sina 0
						sina  cosa  0
						0     0     1
	1     0     0
	0     cosb -sinb
	0     sinb cosb
	*/

	c[0] = cosa;
	c[1] = -sina;
	c[2] = 0;
	c[3] = sina * cosb;
	c[4] = cosa * cosb;
	c[5] = -sinb;
	c[6] = sina * sinb;
	c[7] = cosa * sinb;
	c[8] = cosb;
	{
		point double px,py,pz;
		px = c[0] * X + c[1] * Y + c[2] * Z;
		py = c[3] * X + c[4] * Y + c[5] * Z;
		pz = c[6] * X + c[7] * Y + c[8] * Z;
		pz = focal/(pz+d0);
		px = px * pz;
		py = py * pz;
		PX = px + VM;
		PY = py + VM;
	}
}

/*----------------------------------------------------------------------------
 *	Function display :
 *		input : the screen where to display the object
 *		output : none
 *		side effect : generate the image and display it.
 *			draw the segments with a bresenham algorithm
 *----------------------------------------------------------------------------*/
void display(int screen)
{
	segment char col;
	segment int X0,Y0,x1,y1;
	segment int error,i,incmin,incmax,dmin,dmax,*segment max,*segment min;
	int j;
	col = 255;
	/* generates 40 frames of the movie */
	for(j=0;j<40;j++) {
		/* move the point of view */
		Z0 -= 0.2;
		/* compute the projection of the data base point */
		perspective();
		image = 0;
		/* gets the position of the ends of the segment */
		X0 = [P1]PX;
		x1 = [P2]PX;
		Y0 = [P1]PY;
		y1 = [P2]PY;
		{
			segment int incx,incy,dx,dy;
			dx = x1 - X0;	/* delta x */
			dy = y1 - Y0;	/* delta y */
			incx = incy = 1;
			where(dx < 0) {	
				dx = -dx;
				incx = -1;
			}	
			where(dy < 0) {
				dy = -dy;
				incy = -1;
			}
			/* incx is either 1 or -1 */
			/* incy is either 1 or -1 */
			where(dx > dy) {
				/* x will be augmented by incx at each pixel */
				incmax = incx;
				   max =   &X0;
				  dmax =   dx;
				incmin = incy;
				   min =   &Y0;
				  dmin =   dy;
			} elsewhere {
				/* y will be augmented by incy at each pixel */
				incmax = incy;
				   max =   &Y0;
				  dmax =   dy;
				incmin = incx;
				   min =   &X0;
				  dmin =   dx;
			}
		}
		/* max points either to X0 or Y0 depend on the octant */
		/* min points to the other coordinate */
		/* dmax is the number of pixel to draw */
		/* incmax is the increment associated to max */
		/* incmin is the increment associated to min */
		error = dmax >> 2;
		/* drawing dmax points */
		forwhere(i=0;i<dmax;i++) {
			[[X0,Y0]] image <- col;
			where(error < 0) {
				error += dmax;
				*min += incmin;
			}
			error -= dmin;
			*max += incmax;
		}
		/* drawing last point */
		[[X0,Y0]] image <- col;
		/* sending the image to the screen */
		HcFlash(screen,image,0,0,1,1);
	}
}


/*----------------------------------------------------------------------------
 *	Function main :
 *		input : program arguments 
 *		output : none
 *		side effect : parse arguments and call the display function
 *----------------------------------------------------------------------------*/
int main(int argc,char *argv[])
{
	int screen;

	if(argc != 2) {
		fprintf(stderr,"Usage : display <filename>\n");
		exit(1);
	}
	read_file(argv[1]);
	screen = HcOpenDisplay(256,256);
	HcSetCmap(screen,BlackAndWhite,0,0,-1);
	display(screen);
}
