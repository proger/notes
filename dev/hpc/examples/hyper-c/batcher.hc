/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

*/
#include <hyperc.hh>
#include <graphic.hh>
#include <stdio.hh>
HcDisplay screen; 		/* graphic window ID */
int zoom;				/* X zoom factor */
int zoomy;				/* Y zoom factor */
int j = 0;				/* used for display purpose... */

/*-------------------------------------------------------------
 *	Function batcher_sort :
 *		input : an parallel variable to be sorted and
 *			the y position where to display the sort
 * 		output : the sorted parallel variable
 *		side effect : display all the steps on the graphic screen
 *-------------------------------------------------------------*/
collection col int batcher_sort(col int k,int y)
{
	int n,p,q,r,d,t;
	col int i;
	col int sk;
	col int u;
	col char out;

	n = HcSizeof(col);
	i = HcCoord(0);
	for(t=1;n>(1 << t);t++) ;
	/*---- t is the power of two greater or equal to n ----*/
	for(p = 1 << t-1;p;p>>=1) {
		q = 1 << t-1;
		r = 0;
		d = p;
		while(1) {
			where(i<n-d) {
				where((i & p) == r) {
					sk = [[i+d]] k; 	
					where(k > sk) {
						u = k;			
						[[.+d]]k <- u;
						k = sk;
					}
				}
			}
			out = k;
			HcFlash(screen,out,y,zoomy*j++,zoom,zoomy);
			if(q != p)  {
				d = q - p;
				q >>= 1;
				r = p;
			} else break;
		}
		j++;
	}
	return k;
}

/*-------------------------------------------------------------
 *	Function bitonic_sort :
 *		input : an parallel variable to be sorted and
 *			the y position where to display the sort
 * 		output : the sorted parallel variable
 *		side effect : display all the steps on the graphic screen
 *	bitonic sort works only for power of two.
 *-------------------------------------------------------------*/
collection col int bitonic_sort(col int k,int y)
{
	int n,p,q,d,t;
	col int i;
	col int sk;
	col int u;
	col char out;

	n = HcSizeof(col);
	i = HcCoord(0);
	p = q = 0;
	for(t=1;n>(1 << t);t++); /* p = log(n) */
	for(q = 1;q <=t;q++) {
		for(p = q-1;p>=0;p--) {
			d = 1 << p;
			sk = [[.+d]]k;
			u = k;
			where(!(i & d) & i+d < n) {
				where((k < sk) ^ !(i & (1 << q))) {
					[[.+d]]k <- u;
					k = sk;
				}
			}
			out = k;
			HcFlash(screen,out,y,zoomy*j++,zoom,zoomy);
		}
		j++;
	}
	return k;
}

/*-------------------------------------------------------------
 *	Function bubble_sort :
 *		input : an parallel variable to be sorted and
 *			the y position where to display the sort
 * 		output : the sorted parallel variable
 *		side effect : display all the steps on the graphic screen
 *-------------------------------------------------------------*/
collection col int bubble_sort(col int k,int y)
{
	int n;
	int j = 0,l;
	col int i;
	col int sk;
	col int u;
	col char out;

	i = HcCoord(0);
	n = HcSizeof(col);
	out = k;
	l = 0;
	HcFlash(screen,out,y,5*l++,zoom,zoom);
	for(j=0;j<n;j++) {
		if(!(j & 1))  {
			where(!(i & 1)) {
				sk = [[.+1]] k;
				where(sk < k) {
					u = k;
					[[.+1]] k <- u;
					k = sk;
				}
			} 
		} else {
			where(!(i & 1)) {
				sk = [[.-1]] k;
				where(i != 0) {
					where(sk > k) {
						u = k;
						[[.-1]] k <- u;
						k = sk;
					}
				}
			} 
		}
		out = k;
		HcFlash(screen,out,y,zoomy*l++,zoom,zoomy);
	}
	return k;
}

collection sort;		/*---- collection used for sorting the variables ---*/

/*-------------------------------------------------------------
 *	Function calcul :
 *		input : none
 * 		output : none
 *		side effect : build a random parallel variable and
 *			sort it according to 3 algorithms, compare the results
 *-------------------------------------------------------------*/
void calcul(void)
{
	sort int in,out1,out2,out3;

	screen = HcOpenDisplay(512,257);	   /* opening the graphic display   */
	HcSetCmap(screen,BlackAndWhite,0,0,1); /* set a b&W color map */
	in = random(in);
	in >>= 12;
	in &= 0xff;
	where(in == 1) in = 0;	   /* suppressing the level 0 for display purpose */
	out1 = bubble_sort(in,0);
	out2 = batcher_sort(in,256);
	j += 5;
	out3 = bitonic_sort(in,256);
	if(|<- (out1 - out2) | |<- (out1 - out3)) {
		/* there is somewhere an error */
		in = HcCoord(0);
		where(out1 != out2 | out1 != out3) {
			/* report errors */
			aprintf("%3d : %3d, %3d, %3d\n",in,out1,out2,out3);
		}
	}
}

/*-------------------------------------------------------------
 *	Function main :
 *		input : arguments of the program
 * 		output : return code
 *		side effect : parse the arguments
 *			allocate the sort collection and call calcul function
 *-------------------------------------------------------------*/
int main(int argc,char *argv[])
{
	int size = 128;
	int l;
	if(argc == 2) size = atoi(argv[1]);
	if(size <= 0 || argc > 2) {
		fprintf(stderr,"Usage : batcher <size > 0>\n");
		exit(1);
	}
	for(l=0;size > 1 << l;l++);
	if(size != 1 << l) {
		fprintf(stderr,"the size must be a power of two\n");
		exit(1);
	}
	l = l * (l+3) + 5;
	/*---- setting the zoom factor for display purpose -----*/
	zoom = 256/size;
	zoomy = 256/l;
	if(zoomy > zoom) zoomy = zoom;
	/*--- allocate the sort collection ---*/
	HcAllocateCollection(&sort,1,size);
	calcul();
}

