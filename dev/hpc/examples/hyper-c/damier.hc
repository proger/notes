/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

*********************************************************************************
*	very simple program : print out a the result of HcCoord and HcPosition
*		for a 8 by 4 collection
*
*/
#include <hyperc.hh>
#include <stdio.hh>

collection [8,4]image;

int main(int argc,char *argv[])
{
	image int x;		/* x coordinate in the collection */
	image int y;		/* y coordinate in the collection */
	image int p;		/* position in the collection */
	image char r;		/* either ' ' or '\n' for display purpose */
	image char buf[20]; /* buffer for display purpose */

	x = HcCoord(image,0);
	y = HcCoord(image,1);
	p = HcPosition(image);
	r = ' ';
	where(x == HcDimof(image,0)-1) {
		r = '\n';
	}
	printf("HcCoord(image,0) = \n");
	sprintf(buf,"%2d%c",x,r);
	fputs(buf,stdout);
	printf("\n");

	printf("HcCoord(image,1) = \n");
	sprintf(buf,"%2d%c",y,r);
	fputs(buf,stdout);
	printf("\n");
	printf("HcPosition(image) = \n");
	sprintf(buf,"%2d%c",p,r);
	fputs(buf,stdout);
	printf("\n");
}
