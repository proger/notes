/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

*********************************************************************************
*	mountain.hc 
*
*		Draw a fracal mountain
*/
#include <hyperc.hh>
#include <graphic.hh>
#include <stdio.hh>
#define NO_MAIN
#include "nappe.hc"
#undef NO_MAIN

collection [256,256]pixel;
collection mountain;
mountain DOUBLE elevation;		/* elevation of the mountain */
int screen;

collection tempo;		/* temporary collection used to zoom the mountain */
tempo DOUBLE tempo_Z;
int dn = 0;

/*----------------------------------------------------------------------------
 *	Function normalise_nappe :
 *		input : 
 *			- res : where to store the resulting elevation
 *			- x, y : coordinate of a point on the mountain
 *			- color : where to store the color 
 *		output : none
 *		side effect : compute the mountain elevation from the map given
 *			by elevation. As x and y are double and not necessary on
 *			integers, the area are interpolated with a 2 order function.
 *			resulting elevation is computed from 9 points on the grid.
 *----------------------------------------------------------------------------*/
void normalise_nappe(
	collection col_nappe DOUBLE *packed res,
	col_nappe DOUBLE *packed x,
	col_nappe DOUBLE *packed y,
	col_nappe unsigned char *packed color)
{
	int taille_nappe = HcDimof(col_nappe,0);
	col_nappe int intx,inty;
	col_nappe DOUBLE fracx,fracy;
	col_nappe DOUBLE sum,sumpar;
	col_nappe DOUBLE g,a0,a1,a2,b0,b1,b2;

	fracy = *y + 0.5;
	inty = fracy;
	fracy -= inty;
	fracx = *x + 0.5;
	intx = fracx;
	fracx -= intx;

	a0 = 1 - fracx;
	a0 *= a0;
	a2 = fracx * fracx;
	a1 = 2.0 - a0 - a2;

	b0 = 1 - fracy;
	b0 *= b0;
	b2 = fracy * fracy;
	b1 = 2.0 - b0 - b2;

	g = [[intx-1,inty-1]]elevation;
	sumpar = g * a0;
	g = [[intx,inty-1]]elevation;
	sumpar += g * a1;
	g = [[intx+1,inty-1]]elevation;
	sumpar += g * a2;
	sum = sumpar * b0;

	g = [[intx-1,inty]]elevation;
	sumpar = g * a0;
	g = [[intx,inty]]elevation;
	sumpar += g * a1;
	g = [[intx+1,inty]]elevation;
	sumpar += g * a2;
	sum += sumpar * b1;

	g = [[intx-1,inty+1]]elevation;
	sumpar = g * a0;
	g = [[intx,inty+1]]elevation;
	sumpar += g * a1;
	g = [[intx+1,inty+1]]elevation;
	sumpar += g * a2;
	sum += sumpar * b2;
	*res = sum * 0.25;
	*color = *res *0.2 + 128.0;
}


/*----------------------------------------------------------------------------
 *	Function scale_times_2 :
 *		input : none
 *		output : none
 *		side effect : interpolate a mountain (elevation) from a
 *			temporary mountain (tempo_Z) of 2 times smaller resolution.
 *----------------------------------------------------------------------------*/
void scale_times_2(void)
{
	mountain DOUBLE Zmin,Zmax;
	mountain int Mx,My;

	Mx = HcCoord(0);
	My = HcCoord(1);
	where(!(My & 1)) {
		where(!(Mx & 1)) {
			elevation = [[Mx>>1,My>>1]]tempo_Z;
		} elsewhere {
			Zmin = [[ .-1,. ]]elevation;
			Zmax = [[ .+1,. ]]elevation;
			elevation = (Zmin + Zmax) * 0.5;
		}
	} elsewhere {
		Zmin = [[ .,.-1 ]]elevation;
		Zmax = [[ .,.+1 ]]elevation;
		elevation = (Zmin + Zmax) * 0.5;
	}
}

/*----------------------------------------------------------------------------
 *	Function double_resolution_tempo :
 *		input : none
 *		output : none
 *		side effect : copy a mountain (elevation) to a
 *			temporary mountain (tempo_Z) of same resolution.
 *----------------------------------------------------------------------------*/
void double_resolution_tempo(void)
{
	tempo int x0,y0;

	x0 = HcCoord(0);
	y0 = HcCoord(1);
	tempo_Z = [[x0,y0]]elevation;
}
/*----------------------------------------------------------------------------
 *	Function double_resolution:
 *		input : none
 *		output : none
 *		side effect : double the resolution of the mountain.
 *----------------------------------------------------------------------------*/
int double_resolution(int n,int *dim)
{
	HcAllocateCollection(&tempo,2,dim[0],dim[1]);
	double_resolution_tempo();
	HcFreeCollection(mountain);
	dim[0] <<= 1;
	dim[1] <<= 1;
	HcAllocateCollection(&mountain,2,dim[0],dim[1]);
	scale_times_2();
	HcFreeCollection(tempo);
}

/*----------------------------------------------------------------------------
 *	Function random_new_point:
 *		input : resolution
 *		output : none
 *		side effect : add random value to the elevation
 *			and display the result.
 *----------------------------------------------------------------------------*/
int random_new_point(i)
int i;
{
	mountain int r;
	mountain char out;
	pixel char face;
	int ZOOM = (1 << 7-i);
	int mask = (1 << 10-i)-1;
	int delta = 1 << 9-i;
	double size = 1 << i+1;

	r = random(r);
	r >>= 7;
	r &= mask;
	r -= delta;
	elevation += r;
	out = elevation * 0.2 + 128.0;
	HcFlash(screen,out,0,0,ZOOM,ZOOM);
	face = 1;
	if(dn) {
		/*---- defined in the nappe.hc module -----*/
		draw_nappe(0.0,size,0.0,size,-2000.0,2000.0,&face,normalise_nappe);
		HcFlash(screen,face,256,0,1,1);
	}
}

/*----------------------------------------------------------------------------
 *	Function calcul:
 *		input : size of the mountain
 *		output : none
 *		side effect : build the mountain by multiresolution technique
 *----------------------------------------------------------------------------*/
int calcul(int dim[2])
{
	int i;

	elevation = 0.0;
	for(i=0;i<8;i++) {
		double_resolution(i,dim);
		random_new_point(i);
	}
}

int main(argc,argv)
int argc;
char *argv[];
{
	int dim[2];
	int i;

	for(i=1;i<argc;i++) {
		if(argv[i][0] = '-') {
			switch(argv[i][1]) {
			case 'n' :
				dn = 1;
				break;
			}
		}
	}
	dim[0] = dim[1] = 1;
	screen = HcOpenDisplay(dn ? 512:256,256);
	HcSetCmap(screen,BlueRed,1,0,1);
	HcAllocateCollection(&mountain,2,dim[0],dim[1]);
	calcul(dim);
}
