/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.
*/
#include <hyperc.hh>
#include <math.hh>
#include <graphic.hh>
#include <stdio.hh>
local int sscanf(char *,...);
const float g=9.81;
const float delta_t=0.03;
const float damping=0.99;
const int graphic = 1;

collection point;
collection spring;
collection col;

int main(int argc,char *argv[])
{
	int NbPoint,NbRessort,i,j;
	char Line[80];
	int screen;
	FILE *f;

	if(argc != 2) {
		fprintf(stderr,"Usage : dynamic <database>\n");
		exit(1);
	}
	if(!(f = fopen(argv[1],"r"))) {
		perror(argv[1]);
		exit(1);
	}
	fgets(Line,80,f);
	sscanf(Line,"%d",&NbPoint);
	HcAllocateIrregularCollection(&point,NbPoint,-1);
	screen = HcOpenDisplay(512,512);
	HcSetCmap(screen,Rainbow,0,0,0);
	{
		point float x,y,m;
		point int fixed;
		point char PointLine[80];

		fgets(PointLine,80,f);
		sscanf(PointLine,"%g %g %g %d",&x,&y,&m,&fixed);
		fgets(Line,80,f);
		sscanf(Line,"%d",&NbRessort);
		HcAllocateCollection(&spring,1,NbRessort);
		{
			spring float raideur,longueur;
			spring int l,r;
			spring char RessortLine[80];
			spring link point left,right;
			spring float xl,xr,yl,yr;
			spring float size,F,Fx,Fy,fl,fr;
			point float fx,fy,vx,vy;

			vx = vy = 0.0;
			fgets(RessortLine,80,f);
			sscanf(RessortLine,"%d %d %g %g",&l,&r,&longueur,&raideur);
			left = [l]point;
			right = [r]point;

			HcClearDisplay(screen,0);           /* clean up the screen */
			for(i=0;i<511;i++) {                /*  several times */
				/* code for each spring */
				xl = [left]x; yl = [left]y;     /* gets the left and right */
				xr = [right]x; yr = [right]y;   /* coordinate from the bodies */
				fl = [left]fixed;				/* and whether they are fixed */
				fr = [right]fixed;
				HcShowVector(screen,100.0*xl,512-100.0*yl,100.0*xr,512-100.0*yr,i + 1);
				size = sqrt((xl-xr)*(xl-xr) + (yl-yr)*(yl-yr));
				F = raideur*(size - longueur);   /* The amplitude of the force*/
				Fx = (xl - xr) * F / size;       /* the force vector */
				Fy = (yl - yr) * F / size;

				/* code for each body */
				fx = 0.0;				/* bodies are subjected to gravity */
				fy = -m*g;
				[left] fx -<- Fx;		/* each spring pulls its left  body */
				[left] fy -<- Fy;
				[right] fx +<- Fx;      /*              and  its right body */
				[right] fy +<- Fy;
				where(!fixed) {			/* where the bodies are not fixed */
					vx += fx*delta_t/m; /*  speed is integrated from */
					vy += fy*delta_t/m; /*        acceleration */
					vx *= damping;      /* a viscuous break */
					vy *= damping;
					x += vx * delta_t;  /* position is integrated from speed */
					y += vy * delta_t;
				}
			}
		}
	}
	exit(0);
}
