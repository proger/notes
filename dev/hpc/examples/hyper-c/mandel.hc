/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

******************************************************************************
*
*	mandel.hc
*
*	explore a Mandelbrot set
*/
#include <hyperc.hh>
#include <graphic.hh>
#include <stdio.hh>
#define X_AXIS 0
#define Y_AXIS 1
int X_SIZE=7;
int Y_SIZE=7;
int X_SCREEN=8;
int Y_SCREEN=8;
#define Double float

Double X0,Y0,zoom;	/* coordinate of the view point */

/*--------------------------------------------------------------------------
 *	Function mandel :
 *		input : a packed pointer to the output picture
 *		output : none
 *		side effect : compute a mandelbrot set around the X0,Y0 location
 *-------------------------------------------------------------------------*/
int mandel(collection pixel unsigned char *packed res)
{
	pixel int x0,y0;
	pixel int color;
	int i,j;
	int n;
	Double zoomx,zoomy;

	zoomx = 2.0/HcDimof(pixel,0);
	zoomy = 2.0/HcDimof(pixel,1);
	x0 = HcCoord(0) - (HcDimof(pixel,0) >> 1);
	y0 = HcCoord(1) - (HcDimof(pixel,1) >> 1);
	{
		pixel register Double Z_r,c_r,z_r;
		pixel register Double Z_i,c_i,z_i,z_r2,z_i2;
		pixel Double norme;
		pixel int on;
		color = 255;
		z_r = 0.0;
		z_i = 0.0;
		c_r = x0;
		c_i = y0;
		c_r *= zoomx;
		c_i *= zoomy;
		c_r = c_r*zoom + X0;
		c_i = c_i*zoom + Y0;
		forwhere(color = 0; color<255 ;color++) {
			z_r2 = z_r * z_r;
			z_i2 = z_i * z_i;
			Z_i = z_r * z_i;

			norme = z_r2 + z_i2;
			Z_r = z_r2  - z_i2;
			Z_i += Z_i;

			where(norme > 4.0f) {
				break;
			}
			z_r = Z_r + c_r;
			z_i = Z_i + c_i;
		}
	}
	*res = color;
	fprintf(stderr,"%d iterations\n",+<- color);
}

collection pixel;

/*----------------------------------------------------------------------------
 *	Function explore :
 *		input : the screen where to draw the mandelbrot
 *		output : none
 *		side effect : explore the mandelbrot set
 *			Left button : zoom in
 *			Right button : zoom out
 *			Middle button : reset zoom
 *----------------------------------------------------------------------------*/
void explore(int screen)
{
	pixel char color;
	int sx = 1 << X_SCREEN - X_SIZE;
	int sy = 1 << Y_SCREEN - Y_SIZE;
	int cx = 1 << X_SIZE - 1;
	int cy = 1 << Y_SIZE - 1;
	Double Cx = 1.0/cx;
	Double Cy = 1.0/cy;
	int xm,ym,button;

	HcSetCmap(screen,Rainbow,1,0,255);
	while(1) {
		printf("X0 = %g, Y0 = %g, zoom = %g\n",X0,Y0,1.0/zoom);
		mandel((pixel unsigned char *packed)&color);
		HcFlash(screen,color,0,0,sx,sy);
		fprintf(stderr,"Select a location where to zoom in:...");
		button = HcMouse(screen,&xm,&ym);
		fprintf(stderr,"Thank You (%d,%d)\n",xm,ym);
		xm = xm/sx - cx;
		ym = ym/sy - cy;
		X0 += xm * Cx * zoom;
		Y0 += ym * Cy * zoom;
		switch(button) {
		case 1 :
			zoom *= 0.25;
			break;
		case 2 :
			zoom = 2.0;
			break;
		case 3 :
			zoom *= 4.0;
			break;
		}
	}
}


int main(int argc,char *argv[])
{
	int dim[2];
	static pref[2] = {1,1};
	int i;
	double atof();
	int speed=0;


	zoom = 2.0;
	X0 = -0.75;
	Y0 = 0.01;
	for(i=1;i<argc;i++) {
		if(*argv[i] == '-') {
			switch(argv[i][1]) {
			case 'W' : i++;
				break;
			case 'w' : 
				i++;
				if(i>= argc) {
					printf("argument is missing\n");
					goto mainerr;
				}
				X_SCREEN = atoi(argv[i]);
				Y_SCREEN = atoi(argv[i]);
				break;
			case 'S' : 
				speed = 1;
				break;
			case 's' : 
				i++;
				if(i>= argc) {
					printf("argument is missing\n");
					goto mainerr;
				}
				X_SIZE = atoi(argv[i]);
				Y_SIZE = atoi(argv[i]);
				break;
			case 'z' :
				i++;
				if(i>= argc) {
					printf("argument is missing\n");
					goto mainerr;
				}
				zoom = atof(argv[i]);
				if(zoom != 0.0) zoom = 1.0/zoom;
				break;
			case 'p' :
				i++;
				if(i>= argc) {
					printf("argument is missing\n");
					goto mainerr;
				}
				X0 = atof(argv[i]);
				i++;
				if(i>= argc) {
					printf("argument is missing\n");
					goto mainerr;
				}
				Y0 = atof(argv[i]);
				break;
			default :
				printf("unknown option \"-%c\"\n",argv[i][1]);
				goto mainerr;
			}
		}
	}
	if(X_SCREEN < X_SIZE) X_SCREEN = X_SIZE;
	if(Y_SCREEN < Y_SIZE) Y_SCREEN = Y_SIZE;
	dim[X_AXIS] = 1<<X_SIZE;
	dim[Y_AXIS] = 1<<Y_SIZE;
	HcAllocateCollection(&pixel,2,dim[0],dim[1]);
	explore(HcOpenDisplay(1 << X_SCREEN,1 << Y_SCREEN));
	exit(0);

mainerr :
	printf("Usage : mandel [-s[ize] 2|3...|8|9] [-w[indow] 6|7|8|9] [-p[osition] <x0> <y0>] [-z[oom] <zoom>]\n");
	exit(2);
}

