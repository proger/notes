/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

    This program is the property of HyperParallel Technologies,
    its contents are proprietary information and no part of it
    is to be disclosed to anyone except employees of HyperParallel Technologies
    or as agreed in writing signed by the President of HyperParallel
    Technologies

*/
static char *SCCSIDBIS="#   @(#) /home/blizzard/paris/code.hyperc/examples/SCCS/s.sin.hc 1.4 94/02/01 HyperParallel Technologies";
static char *SCCSID="1.4";
#define VERSION SCCSID
#include <hyperc.hh>
#include <math.hh>
#include <graphic.hh>
collection [512,256] array;
collection line = {{{512,HcBlock,HcGrid,0}}};
line int a;

int main()
{
	line int ymax,ymin,y1,y,x;
	line int col;
	array char b;
	line char black;
	int screen;

	black = 255;
	screen = HcOpenDisplay(512,256);
	x = HcCoord(0);
	col = 255;
	{
		line double i,j;
		i = x * 0.05;
		j = sin(i);
		y = j * 70.0;
		y = 128 - y;
	}
	ymax = ymin = y1 = y;
	[[.+1]] y1 <- y;
	where(x == 0) y1 = y;
	ymax >?= y1;
	ymin <?= y1;
	where(ymax > ymin) ymax--;
	b = 0;
	forwhere(y = ymin; y<=ymax;y++) {
		[[x,y]]b <- black;
	}
	HcSetCmap(screen,BlackAndWhite,0,0,-1);
	HcFlash(screen,b,0,0,1,1);
}
