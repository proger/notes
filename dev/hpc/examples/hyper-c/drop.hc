/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

**************************************************************************
*	drop.hc
*
*	modify an image with a drop of water
*/
#include <hyperc.hh>
#include <graphic.hh>
#include <math.hh>
#include <stdio.hh>
scalar {
#include <fcntl.h>
#include "rasterfile.h"
}
unsigned char red[256];
unsigned char green[256];
unsigned char blue[256];

#ifdef __alpha
int my_swap_int(int x)
{
	int y;
	char *s,*d;
	s = (char *) &x;
	d = (char *) &y;
	d[0] = s[3];
	d[1] = s[2];
	d[2] = s[1];
	d[3] = s[0];
	return y;
}
#else
#define my_swap_int(x) (x)
#endif


collection pixel;	/* collection of the original picture  */
pixel char picture; /* original picture */
collection small;	/* collection of the generated picture */
small char out;		/* generated picture */

/*----------------------------------------------------------------------------
 *	Function deform :
 *		input : 
 *			- t : iteration number (= time)
 *			- xm,ym : impact of the drop
 *		output : none
 *		side-effect : generate the out picture from the original picture
 *			at time t after the impact of the drop at xm, ym
 *----------------------------------------------------------------------------*/
int deform(int t,int xm,int ym)
{
	double diad,T;
	small int addr,y,x,x0,y0;

	T = (t * t + 500)/3000.0;
	diad = (HcDimof(small,0) >> 2)/6.0;
	x0 = HcCoord(0);
	y0 = HcCoord(1);
	{
		small double X,Y,d,d1,phi;
		int K1;

		X = x0 - xm;
		Y = y0 - ym;
		d = sqrt(X*X+Y*Y);				/* distance from the impact */
		d += 0.1;						/* distance always > 0 */
		phi = t*4 -d;					/* phasis of the wave */
		where(phi < 0.0) phi = 0.0;		/* no wave before t = 0 */
		d1 = sin(phi/diad);				/* wave is a sine form */
		x = X * d1/(d * T)+ (x0 << 2) + 0.5; /* with dumping */
		y = Y * d1/(d * T)+ (y0 << 2)+ 0.5;
	}
	/* out pixel come from the x,y pixel of the original picture */
	out = [[x,y]]picture;
}

/*----------------------------------------------------------------------------
 *	Function calcul :
 *		input : the picture file and its header and the window
 *		output : none
 *		side effect :
 *			ask for a drop (click of the mouse) and deforms the picture
 *----------------------------------------------------------------------------*/
void calcul(int f,struct rasterfile *h,int screen)
{
	pixel int x,y;
	pixel char in;
	int i;
	int xm,ym;
	int xn,yn;
	

	x = HcCoord(0);
	y = HcCoord(1);
	if(f == -1) {
		in = x;
	} else {
		/* read the picture */
		readall(f,&in,sizeof(char));
	}
	/* display the original picture */
	HcFlash(screen,in,0,0,1,1);
	picture = in;
	/* loop forever */
	while(1) {
		fprintf(stderr,"Please, select with the mouse the position of the drop\n");
		HcMouse(screen,&xm,&ym);
		fprintf(stderr,"Thanks! xm = %d,ym = %d\n",xm,ym);
		xm >>= 2;
		ym >>= 2;
		/* get a drop in the original picture */
		for(i=0;i<64;i++) {
			/* deforms it 64 times */
			deform(i,xm,ym);
			fprintf(stderr,".");
			HcFlash(screen,out,256,0,4,4);
			if(HcIsMouse(screen,&xn,&yn)) {
				/* a new drop.... */
				xm = xn;
				ym = yn;
				fprintf(stderr,"Thanks! xm = %d,ym = %d\n",xm,ym);
				xm &= 255;
				ym &= 255;
				xm >>= 2;
				ym >>= 2;
				i = 0;
			}
		}
		fprintf(stderr,"\n");
	}
}

/*-------------------------------------------------------------------------
 *	Function main :
 *		input : function arguments
 *		output : none
 *		side effect : parse the argument start the pixel collection
 *			to the correct size and start the small collection.
 *-------------------------------------------------------------------------*/
int main(int argc,char *argv[])
{
	static int dim[2]={256, 256};
	static int pref[2]={1, 2};
	int f=-1,n,i;
	int screen;
	static struct rasterfile H = {
	RAS_MAGIC, 256, 256, 8, 256*256, RT_STANDARD, RMT_EQUAL_RGB, 256*3
	};

	if(argc != 2) {
		for(n=0;n<256;n++) red[n] = green[n] = blue[n] = n;
	} else {
		f = open(argv[1],O_RDONLY);
		if(f == -1) {
			perror(argv[1]);
		}
		read(f,&H,sizeof(H));
	    H.ras_magic = my_swap_int(H.ras_magic);
    	H.ras_width = my_swap_int(H.ras_width);
    	H.ras_height = my_swap_int(H.ras_height);
    	H.ras_depth = my_swap_int(H.ras_depth);
    	H.ras_length = my_swap_int(H.ras_length);
    	H.ras_type = my_swap_int(H.ras_type);
    	H.ras_maptype = my_swap_int(H.ras_maptype);
    	H.ras_maplength = my_swap_int(H.ras_maplength);
		if(H.ras_magic != RAS_MAGIC) {
			fprintf(stderr,"%s is not a rasterfile\n",argv[1]);
			exit(-1);
		}
		if(H.ras_depth != 8) {
			fprintf(stderr,"%s is not an 8 bits depth rasterfile\n",argv[1]);
			exit(-1);
		}
		dim[0] = H.ras_width;
		dim[1] = H.ras_height;

		if(H.ras_width & 3) {
			fprintf(stderr,"%s is not an 4*n width rasterfile\n",argv[1]);
			exit(-1);
		}
		if(H.ras_height & 3) {
			fprintf(stderr,"%s is not an 4*n height rasterfile\n",argv[1]);
			exit(-1);
		}
		if(H.ras_maptype != RMT_EQUAL_RGB && H.ras_maplength != 3 * 768) {
			fprintf(stderr,"%s is not an RMT_EQUAL_RGB rasterfile\n",argv[1]);
		}
		i = H.ras_maplength/3;
		read(f,red,i);
		read(f,green,i);
		read(f,blue,i);
		for(;i<256;i++) {
			red[i] = green[i] = blue[i];
		}
	}
	HcAllocateCollection(&pixel,2,dim[0],dim[1]);
	dim[0] >>= 2;
	dim[1] >>= 2;
	HcAllocateCollection(&small,2,dim[0],dim[1]);
	screen = HcOpenDisplay(2*HcDimof(pixel,0),HcDimof(pixel,1));
	HcSetColor(screen,red,green,blue);
	calcul(f,&H,screen);
}
