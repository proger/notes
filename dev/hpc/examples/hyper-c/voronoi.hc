/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

    This program is the property of HyperParallel Technologies,
    its contents are proprietary information and no part of it
    is to be disclosed to anyone except employees of HyperParallel Technologies
    or as agreed in writing signed by the President of HyperParallel
    Technologies

*/
static char *SCCSIDBIS="#   @(#) /home/blizzard/paris/code.hyperc/examples/SCCS/s.voronoi.hc 1.5 94/02/01 HyperParallel Technologies";
static char *SCCSID="1.5";
#define VERSION SCCSID
#include <hyperc.hh>
#include <stdio.hh>
#include <math.hh>
#include <graphic.hh>
int MAX_RES=8;
int NB_POINTS=64;
int screen;

collection point;

point int X,Y;
point link point address;
point unsigned char color;


void build_random(void)
{
	X = HcCoord(0);
	address = [X]point;
	X = random(X);
	Y = X >> 20;
	X >>= 4;
	X &= (1 << MAX_RES) -1;
	Y &= (1 << MAX_RES) -1;
	color = 13 * X;
}

collection step;
step link point nearest;
step int distance;
step int x,y;

void calcul(int zoom)
{
	point int x1,y1,dist,x2,y2,rbdist,an;
	point link step ap;
	step link point cur;
	step int curdist,xc,yc;
	step unsigned char out;
	int j,k,ZOOM,unzoom;
	static xi[4] = { +1, 0, -1, 0};
	static yi[4] = { 0, +1, 0, -1};
	int mask;
	int max;
	int d1,d2,d3,d4;
	
	unzoom = MAX_RES - zoom;
	if(unzoom) mask = (1 << (unzoom-1));
	else mask = 0;
	ZOOM = 1 << (MAX_RES - zoom);

	x2 = X >> unzoom;
	y2 = Y >> unzoom;
	x1 = (x2 << unzoom) | mask;
	y1 = (y2 << unzoom) | mask;
	x1 -= X;
	y1 -= Y;
	dist = x1 * x1 + y1 * y1;
	ap = [x2,y2]step;
	[ap]distance <?<- dist;
	rbdist = [ap]distance;
	where(dist == rbdist) {
		 [ap]nearest <- address;
	}
	an = 0;
	[nearest]an |<- (step int) 1;
	fprintf(stderr,"%d x %d : %d/%d points :",1<<zoom,1<<zoom,+<-an,NB_POINTS);
	do {
		k = 0;
		for(j=0;j<4;j++) {
			curdist = [[.+xi[j],.+yi[j]]] distance;
			where(curdist < distance) {
				cur = [[.+xi[j],.+yi[j]]] nearest;
				xc = [cur]X ;
				yc = [cur]Y ;
				xc = xc - (x << unzoom);
				yc = yc - (y << unzoom);
				if(unzoom) {
					xc -= 1 << (unzoom -1);
					yc -= 1 << (unzoom -1);
				}
				curdist = xc * xc + yc *yc;
				where(curdist < distance) {
					k += +<- (step int) 1;
					nearest = cur;
					distance = curdist;
				}
			}
		}
		fprintf(stderr,"%d ",k);
		out =  [nearest]color;
		HcFlash(screen,out,0,0,ZOOM,ZOOM);
		out = sqrt((step double)distance);
		HcFlash(screen,out,1 << MAX_RES,0,ZOOM,ZOOM);
	} while(k);
	fprintf(stderr,"\n");
}

collection tempo;
tempo int tdist;
tempo link point tnearest;

void jump(int zoom)
{
	step int xc,yc;
	int unzoom;

	x = HcCoord(0);
	y = HcCoord(1);
	nearest = [[x>>1,y>>1]]tnearest;
	unzoom = MAX_RES - zoom;
	xc = [nearest]X ;
	yc = [nearest]Y ;
	xc = xc - (x << unzoom);
	yc = yc - (y << unzoom);
	if(unzoom) {
		xc -= 1 << (unzoom -1);
		yc -= 1 << (unzoom -1);
	}
	distance = xc * xc + yc *yc;
}

void zoomupbis(int *dim,int zoom)
{

	tdist = *(tempo int *packed)&distance;
	tnearest = *(tempo link point *packed)&nearest;
	dim[0] <<= 1;
	dim[1] <<= 1;
	HcFreeCollection(step);
	HcAllocateCollection(&step,2,dim[0],dim[1]);
	jump(zoom+1);
}
void zoomup(int zoom)
{
	int dim[2];
	dim[0] = dim[1] = 1 << zoom;

	HcAllocateCollection(&tempo,2,dim[0],dim[1]);
	zoomupbis(dim,zoom);
	HcFreeCollection(tempo);
}

int scalebis()
{
	int i,j;
	distance = 0x7fffffff;
	for(i=0;i<=MAX_RES;i++) {
		calcul(i);
		if(i == MAX_RES) break;
		zoomup(i);
	}
}
int scale()
{
	int dim[2];

	dim[0] = dim[1] = 1;
	HcAllocateCollection(&step,2,dim[0],dim[1]);
	scalebis();
}

int main(int argc,char *argv[])
{
	int i;


	for(i=1;i<argc;i++) {
		if(*argv[i] == '-') {
			switch(argv[i][1]) {
			case 's' :
			case 'S' :
				i++;
				if(i == argc) {
					printf("%s : integer expected\n",argv[0]);
					goto printusage;
				}
				MAX_RES = atoi(argv[i]);
				if(MAX_RES < 1 || MAX_RES > 9) {
					printf("%s : out of range parameter\n",argv[0]);
					goto printusage;
				}
				break;
			case 'P' :
			case 'p' :
				i++;
				if(i == argc) {
					printf("%s : integer expected\n",argv[0]);
					goto printusage;
				}
				NB_POINTS = atoi(argv[i]);
				break;
			default :
				printf("%s : unknown option %s\n",argv[0],argv[i]);
				goto printusage;
			}
		}
	}
	HcAllocateCollection(&point,1,NB_POINTS);
	HcStartCollection(point);
	build_random();
	screen = HcOpenDisplay(512,256);
	HcSetCmap(screen,Rainbow,0,0,0);
	sleep(1);
	scale();
	exit(0);

printusage :
	printf("Usage : %s [-s[cale] 1|...|9] [-p[oints] <nb points>]\n",argv[0]);
	exit(-1);
}
