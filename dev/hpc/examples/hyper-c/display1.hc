/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

*************************************************************************
*
*	display.hc
*		read a 3D data base from a file with points and segments
*		display a 40 frame movie of the data base with a moving view point
*		Use a Bresenham algorithm
*/
#include <hyperc.hh>
#include <graphic.hh>
#include <stdio.hh>
#include <scan.hh>
#include <math.hh>

#define VX 256
#define VM (VX/2)
/*------ collection declaration -------------*/
collection point;			/* the collection of the points */
collection segment;			/* the collection of the segments */
collection [VX,VX] pixel;	/* the collection of the generated image */


pixel char image;			/* the generated image */

point float X,Y,Z;			/* the 3D position of the points */
point int PX,PY;			/* their projection on the screen */
segment link point P1,P2;	/* ends of the segments */
float X0 = 2.0 ,Y0 = 1.0 ,Z0 = 4.0;  	/* the eye position */
float focal = VM;						/* the focal */

/*----------------------------------------------------------------------------
 *	Function read_file :
 *		input : the name of the data base
 *		output : none
 *		side effect : load the data base.
 *			create the point collection and the segment collection
 *----------------------------------------------------------------------------*/
void read_file(char *name)
{
	FILE *f;
	int n_point,n_segment,i;
	char Line[80];

	f = fopen(name,"r");
	if(f == 0) {
		perror(name);
		exit(-1);
	}
	fgets(Line,80,f);
	sscanf(Line,"%d",&n_point);			/* reading the number of point */
		/* allocation of the point collection */
	HcAllocateCollection(&point,1,n_point);
	{
		point char Line[80];
		fgets(Line,80,f);
		/* reading the coordinates of all the points */
		sscanf(Line,"%g %g %g",&X,&Y,&Z);
	}
	fgets(Line,80,f);
	sscanf(Line,"%d",&n_segment);		/* reading the number of segments */
		/* allocation of the segment collection */
	HcAllocateCollection(&segment,1,n_segment);
	{
		segment char Line[80];
		segment int p1,p2,bad = 0;
		fgets(Line,80,f);
		/* reading the point number of the 2 sides of each segment */
		sscanf(Line,"%d %d",&p1,&p2);
		bad = 0;
		where(p1 < 0 || p1 >= n_point || p2 <0 || p2 >= n_point) {
			bad = 1;
		}
		if(|<- bad) {
			fprintf(stderr,"corrupted database\n");
			exit(-1);
		}
		/* P1 and P2 are links to the two points at the end of each segment */
		P1 = [p1]point;
		P2 = [p2]point;
	}
}

/*----------------------------------------------------------------------------
 *	Function perspective :
 *		input : none
 *		output : none
 *		side effect : projects the points on the screen.
 *			computes PX and PY from X,Y,Z.
 *			the center of the screen is always oriented to the 0,0,0 point
 *			of the data base
 *----------------------------------------------------------------------------*/
int perspective(void)
{
	double a,b,sina,sinb,cosa,cosb,d0;
	double c[9];

	a = atan(Y0/X0);
	b = atan(Z0/sqrt(X0*X0 + Y0 *Y0));
	d0 = sqrt(X0*X0 + Y0 *Y0 + Z0 * Z0);
	sina = sin(a);
	sinb = sin(b);
	cosa = cos(a);
	cosb = cos(b);
					
	/*

						cosa  -sina 0
						sina  cosa  0
						0     0     1
	1     0     0
	0     cosb -sinb
	0     sinb cosb
	*/

	c[0] = cosa;
	c[1] = -sina;
	c[2] = 0;
	c[3] = sina * cosb;
	c[4] = cosa * cosb;
	c[5] = -sinb;
	c[6] = sina * sinb;
	c[7] = cosa * sinb;
	c[8] = cosb;
	{
		point double px,py,pz;
		px = c[0] * X + c[1] * Y + c[2] * Z;
		py = c[3] * X + c[4] * Y + c[5] * Z;
		pz = c[6] * X + c[7] * Y + c[8] * Z;
		pz = focal/(pz+d0);
		px = px * pz;
		py = py * pz;
		PX = px + VM;
		PY = py + VM;
	}
}

collection span;
/* the collection span is a dynamic collection. It size will be
	equivalent to the number of pixel to modify for each frame */



void draw_line(
	segment int X0,segment int Y0, /* first point */
	segment int dx,segment int dy,	/* delta */
	segment int start)				/* first element in span */
{
	span int sbit,x,y;
	span double slopex,slopey;
	segment double slope;
	segment double sign;
	segment link span lstart;

	lstart = [start]span;
	sbit = 0;
	[lstart]sbit <- 1;
	/* identification of the start of the segments in the span */
	slopey = 0.0;
	slopex = 0.0;
	where(dx <0) sign = -1.0;
	elsewhere sign = 1.0;
	where(dx) {
		slope = (dy + 0.0)/dx;
		where(-1.0 <= slope & slope <= 1.0) {
			slope *= sign;
			[lstart]slopey <- slope;
			[lstart]slopex <- sign;
		}
	}
	where(dy <0) sign = -1.0;
	elsewhere sign = 1.0;
	where(dy) {
		slope = (dx + 0.0)/dy;
		where(-1.0 <= slope & slope <= 1.0) {
			slope *= sign;
			[lstart]slopex <- slope;
			[lstart]slopey <- sign;
		}
	}
	/* each start element has the slopex, slopey set to the correct slope */
	slopey = HcScanWithAdd(slopey,0,0,0,sbit);
	slopex = HcScanWithAdd(slopex,0,0,0,sbit);
	/* slopex and slopey are propaged in each segment */
	[lstart]slopey <- Y0;
	[lstart]slopex <- X0;
	/* X0 and Y0 are put at the begin of each segment */
	slopey = HcScanWithAdd(slopey,0,0,0,sbit);
	slopex = HcScanWithAdd(slopex,0,0,0,sbit);
	/* each span contains the coordinate of its associated pixel */
	y = slopey;
	x = slopex;
	[[x,y]]image <- 255;
	/* lighting the pixel */
}

/*----------------------------------------------------------------------------
 *	Function display :
 *		input : the screen where to display the object
 *		output : none
 *		side effect : generate the image and display it.
 *			draw the segments with a scan algorithm
 *----------------------------------------------------------------------------*/
int display(int screen)
{
	segment int X0,Y0,x1,y1,sw;
	int j,size;
	for(j=0;j<40;j++) {
		Z0 -= 0.2;
		perspective();
		image = 0;
		X0 = [P1]PX;
		x1 = [P2]PX;
		Y0 = [P1]PY;
		y1 = [P2]PY;
		{
			segment int max,dx,dy,start;

			dx = x1-X0;
			dy = y1-Y0;
			max = dx >? -dx >? dy >? -dy;
					/* max is the size of each segment */
			max += 1;
			size = +<- max;
					/* size of the span collection */
			start = HcScanWithAdd(max,0,0,1);
					/* allocation of the span collection */
			HcAllocateCollection(&span,1,size);
			draw_line(X0,Y0,dx,dy,start);
					/* free of the span collection */
			HcFreeCollection(span);
		}
		HcFlash(screen,image,0,0,1,1);
	}
}

/*----------------------------------------------------------------------------
 *	Function main :
 *		input : program arguments 
 *		output : none
 *		side effect : parse arguments and call the display function
 *----------------------------------------------------------------------------*/
int main(int argc,char *argv[])
{
	int i,screen;

	if(argc != 2) {
		printf("Usage : display <filename>\n");
		exit(1);
	}
	read_file(argv[1]);
	screen = HcOpenDisplay(256,256);
	HcSetCmap(screen,BlackAndWhite,0,0,-1);
	display(screen);
}
