/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.
*/
#include <hyperc.hh>
#include <stdio.hh>


/*----- fact as a function on a generic collection ---------*/
collection col int fact(col int p)
{
	col register int n;
	col register int i;
	
	n = 1;
	forwhere(i=1;i<=p;i++) {
		n *= i;
	}
	return n;
}

/*------- abs as a local function ------------*/
local int abs(int n)
{
	if(n < 0) {
		return -n;
	}
	return n;
}

collection [16]pixel;

int main(argc,argv)
int argc;
char *argv[];
{
	pixel int n,p;
	pixel char buf[30];
	p = HcPosition(pixel) - 7;
	n = abs(p);
	n = fact(n);

	sprintf(buf,"%4d: %12d\n",p,n);
	fputs(buf,stdout);
}
