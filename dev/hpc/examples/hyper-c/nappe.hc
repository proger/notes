/*
    HyperC compiler - Copyright 1992 by HyperParallel Technologies.

******************************************************************************
*
*	nappe.hc
*
*	draw in elevation a 2 dimensional map.
*/
#ifndef NO_MAIN
#include <stdio.hh>
#include <hyperc.hh>
#include <graphic.hh>
#endif
#include <math.hh>
#define DOUBLE float
DOUBLE ax = 0.7,ay = -0.5;	
static DOUBLE CurrentTime;
#ifndef VX
#define VX 256		/* default resolution of the nappe */
#endif
collection [VX,VX/16]nappe;

/*----------------------------------------------------------------------------*
 *	Function draw_nappe :
 *		input :
 *			- x0, x1, y0, y1 : bounding box of the nappe
 *			- z0, z1 : minimum and maximum elevation of the nappe in the
 *				bounding box
 *			- ecran : where to store the result
 *			- f : the function used to find the elevation of a point
 *----------------------------------------------------------------------------*/
void draw_nappe(
	double x0, double x1,
	double y0, double y1,
	double z0, double z1,
	collection pixel char *packed ecran,
	void (*f)(collection c DOUBLE *packed ,c DOUBLE *packed ,c DOUBLE *packed,
		c unsigned char *packed))
{
	DOUBLE step;
	DOUBLE dx,dy,a,b,c,x2,y2;
	nappe DOUBLE X,Z,Y;
	nappe int z,x,y,yu,yd,horizon,ymin,ymax;
	nappe unsigned char color;
	int i;
	DOUBLE cosx,sinx;
	DOUBLE cosy,siny;
	DOUBLE scosx,scosy;

	int vx,vy;

	color = 255;
	vx = HcDimof(nappe,0);
	vy = HcDimof(pixel,1);
	siny = cosx = cos(ax);
	sinx = sin(ax);
	cosy = -sinx;
	a = sin(ay);
	sinx *= a * vy/(x1 - x0);
	siny *= a * vy/(y1 - y0);
	scosx = 1/cosx;
	scosy = 1/cosy;
	dx = ((x1 - x0) * cosx - (y1 - y0) * cosy)/vx;
	dy = vy/(z1 - z0);
	step = (y1 - y0) / (HcDimof(nappe,1) - 1);
	y = HcCoord(1);
	x = HcCoord(0);
	Y = y0 + y * step;
	z0 += (sinx * (x0+x1) + siny * (y0+y1)) *0.5/dy;
	X = x0 + (x * dx + (y1 - Y)*cosy)*scosx;
	where(X >= x0 & X < x1) {
		f(&Z,&X,&Y,&color);
		y = (Z - z0) * dy + sinx * X + siny * Y;
		where(y >= 0) {
			[[x,y]]*ecran <- color;
		}
	}
	color = 255;
	step = (x1 - x0) / (HcDimof(nappe,1) - 1);
	Y = x0 + HcCoord(1) * step;;
	x = HcCoord(0);
	X = y1 + (x * dx - (Y - x0)*cosx)*scosy;
	where(X >= y0 & X < y1) {
		f(&Z,&Y,&X,&color);
		y = (Z - z0) * dy + sinx * Y + siny * X;
		where(y >= 0) {
			[[x,y]]*ecran <- color;
		}
	}
}


#ifndef NO_MAIN
#define VY VX
collection [VX,VY]pixel;

int screen;
void my_nappe(collection nappe DOUBLE *packed res,nappe DOUBLE *packed x,nappe DOUBLE *packed y,nappe unsigned char *packed color)
{
	nappe DOUBLE t,z;
	nappe double d1,d2;

	d2 = *x * *x + *y * *y;
	d1 = sqrt(d2);
	z = d1;
	t = CurrentTime - z;
	where((t < 0.0) | (t > 6.28)) t = 0;
	d2 = t;
	d1 = sin(d2);
	*res = ((DOUBLE) -2.0) * d1 /(z + (DOUBLE) 1.0);
	*color = 255;
}

collection pixel;

int main()
{
	pixel char *packed res[16];
	double min,max;
	int i;
	for(i=0;i<16;i++) {
		res[i] = HcPackedAlloca(1);
	}
	ax = 0.3;
	ay = 0.7;
	min = -10.0;
	max = 10.0;
	screen = HcOpenDisplay(256,256);
	HcSetCmap(screen,BlackAndWhite,0,0,-1);
	for(i=0;i<16;i++) {
		*res[i] = 0;
		draw_nappe(min,max,min,max,min,max,res[i],my_nappe);
		HcFlash(screen,*res[i],0,0,1,1);
		CurrentTime += 1.0;
		fprintf(stderr,".");
	}
	for(i=0;i<=64;i++) {
		HcFlash(screen,*res[i & 15],0,0,1,1);
	}
}
#endif
