/* 
 * Copyright 2008 - 2010 CAPS entreprise. All rights reserved.
 */


#include <getopt.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// Number of execution
#define NB_RUNS 5

// Size of the matrix
#define SIZE 1024

// Initialization random value
#define SRAND_VALUE 5347

// Use to initialize the matrix
float randFloat(float low, float high)
{
  float t = (float)rand() / (float)RAND_MAX;
  return (1.0f - t) * low + t * high;
}

// Declaration of the group
#pragma hmpp <myGroup> group, target=CUDA

////////////////////////////////////////////////////////////////////////////////
/// sgemm_codelet
////////////////////////////////////////////////////////////////////////////////
#pragma hmpp <myGroup> mySgemm codelet, args[size;alpha;beta;b].transfer=atfirstcall, &
#pragma hmpp & args[c].transfer=manual , args[a,c].mirror
void mySgemm( int size, float alpha, float beta,
              const float a[size][size], const float b[size][size], float c[size][size] )
{
  int i,j,l;
  float ab;
#pragma hmppcg unroll i:4, j:4, jam, noremainder(i,j), split(i)
#pragma hmppcg gridify (j,i)  
  for( j = 0 ; j < size ; j++ ) {
    for( i = 0 ; i < size ; i++ ) {
      ab=0.0f;
      for( l = 0 ; l < size ; l++ ){
        ab += a[j][l] * b[l][i];
      }
      c[j][i] = alpha * ab + beta * c[j][i];
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
// convol_codelet
////////////////////////////////////////////////////////////////////////////////
#pragma hmpp <myGroup> conv codelet, args[size].transfer=atfirstcall, &
#pragma hmpp & args[c;d].transfer=manual, args[d].io=out , args[d,c].mirror
void convol( int size, const float c[size][size], float d[size][size]) {
  int i, j;
  for (i=1; i<size-1; i++) {
    for (j=1; j<size-1; j++) {
      d[i][j] = ( c[i-1][j-1] * c[i-1][j+0] * c[i-1][j+1]
                  +         c[i+0][j-1] - c[i+0][j+0] - c[i+0][j+1]
                  +         c[i+1][j-1] * c[i+1][j+0] * c[i+1][j+1] ) / 9.0f;
    }
  }
}

// Use to compute error between matrices
void computeError( int size, float cpu[size][size], float hwa[size][size], double *error, double *valueCPU, double *valueGPU, 
                   int *index_i,int *index_j, int begin_m, int end_m, int begin_k, int end_k) {
  int i, j;
  for (i=begin_m; i<end_m; ++i) {
    for (j=begin_k; j<end_k; ++j) {
      double lerror = fabs((hwa[i][j]-cpu[i][j])/cpu[i][j]);
      if (lerror > *error) {
        *error = lerror;
        *valueCPU = cpu[i][j];
        *valueGPU = hwa[i][j];
        *index_i = i;
        *index_j = j;
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
// Main program
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
  int size=SIZE;
  
  float *b=NULL, *c_hwa=NULL, *c_cpu=NULL, *a_hwa=NULL, *a_cpu=NULL;
  int i, j;
  
  // For timer measures
  struct timeval tv_global_begin, tv_global_end; // global timer (all iterations)
  struct timeval tv_begin, tv_end;  // local timer (1 iteration)
  
  unsigned long long int best_measure_GPU = 0;
  unsigned long long int sum_measure_GPU  = 0;
  
  unsigned long long int best_measure_CPU = 0;
  unsigned long long int sum_measure_CPU  = 0;
  
  unsigned long long int global_CPU_time  = 0;
  unsigned long long int global_GPU_time  = 0;
  
  unsigned long long int current;
  
  float alpha, beta;
  
  double error    = 0.0;
  int index_i     = 0.0;
  int index_j     = 0.0; 
  double valueCPU = 0.0;
  double valueGPU = 0.0;
  
  // Allocating CPU memory
  b = (float *)malloc(size * size * sizeof(float));
  c_hwa = (float *)malloc(size * size * sizeof(float));
  c_cpu = (float *)malloc(size * size * sizeof(float));
  a_hwa = (float *)malloc(size * size * sizeof(float));
  a_cpu = (float *)malloc(size * size * sizeof(float));
  
  if((b == NULL) || (c_hwa == NULL) || (c_cpu == NULL) || 
     (a_hwa == NULL) || (a_cpu == NULL)) {
    fprintf( stderr, "\n**** error : memory allocation failed ****\n");
    return 1;
  }

  // Buffer allocation on the GPU  
#pragma hmpp <myGroup> allocate, args[mySgemm::b].size={size,size}

  // Mirror allocation on the GPU 
#pragma hmpp <myGroup> allocate data["c_hwa"], size={size,size}, elementsize="4"
#pragma hmpp <myGroup> allocate data["a_hwa"], size={size,size}, elementsize="4"
  
  fprintf( stdout, "---- Initialization of the Matrices ----\n\n");
  srand(SRAND_VALUE);
  
  //Generate options set
  for(i = 0; i < size; i++){
    for(j = 0; j < size; j++){
      a_cpu[i*size+j] = randFloat(0.1f, 1.0f);
      a_hwa[i*size+j] = a_cpu[i*size+j] ;
      b[i*size+j] = randFloat(0.1f, 1.0f);
      c_cpu[i*size+j] = randFloat(0.1, 20.0f);
      c_hwa[i*size+j] =  c_cpu[i*size+j];
    }
  }
  
  alpha = 0.5;
  beta  = randFloat(1.0, 2.0);
  
  // initialize mirror on the GPU
#pragma hmpp <myGroup> advancedload data["c_hwa","a_hwa"]
  
  fprintf( stdout, "---- Running calculations ----\n");
  
  // run sgemm on GPU (NB_RUNS iterations)
  printf("Run on GPU\n");
  
  // Start timer
  gettimeofday(&tv_global_begin, NULL);
  
  for( i=0; i<NB_RUNS; i++ ) {
    printf("%d ",i);
    gettimeofday(&tv_begin, NULL);
    
#pragma hmpp <myGroup> mySgemm callsite
    mySgemm( size, alpha, beta, a_hwa, b, c_hwa );

#pragma hmpp <myGroup> conv callsite
    convol(size, c_hwa, a_hwa);

    gettimeofday(&tv_end, NULL);    
    current = (tv_end.tv_sec-tv_begin.tv_sec)*1e6 + tv_end.tv_usec-tv_begin.tv_usec;
  
    if( ( best_measure_GPU == 0 ) || ( best_measure_GPU > current ) ){
      best_measure_GPU = current;
    }
    sum_measure_GPU += current;   
  }

  // Download the mirror result in a_hwa
#pragma hmpp <myGroup> conv delegatedStore, data["a_hwa"], section={1:1022,1:1022}

  gettimeofday(&tv_global_end, NULL);
  global_GPU_time = (tv_global_end.tv_sec-tv_global_begin.tv_sec)*1e6 + tv_global_end.tv_usec-tv_global_begin.tv_usec;   

 // run sgemm & convol on CPU (NB_RUNS iterations)
  printf("\n\nRun on CPU\n");
  
  // Start timer
  gettimeofday(&tv_global_begin, NULL);
  
  for( i=0; i<NB_RUNS; i++ ) {
    printf("%d ",i);
    gettimeofday(&tv_begin, NULL);

    mySgemm( size, alpha, beta, a_cpu, b, c_cpu );
    convol(size, c_cpu, a_cpu);
  
    gettimeofday(&tv_end, NULL);
    current = (tv_end.tv_sec-tv_begin.tv_sec)*1e6 + tv_end.tv_usec-tv_begin.tv_usec;
    
    if( ( best_measure_CPU == 0 ) || ( best_measure_CPU > current ) ){
      best_measure_CPU = current;
    }  
    sum_measure_CPU += current;  
  } 
  
  gettimeofday(&tv_global_end, NULL);
  global_CPU_time = (tv_global_end.tv_sec-tv_global_begin.tv_sec)*1e6 + tv_global_end.tv_usec-tv_global_begin.tv_usec;
   
  // Compute error of convol between CPU and HWA
  error = valueCPU = valueGPU = 0.0;
  computeError(size, a_cpu, a_hwa, &error, &valueCPU, &valueGPU, &index_i, &index_j, 1, size-1, 1, size-1);
  if (error > 2e-06) {
    fprintf( stdout, "The error is is too big!\n");
    fprintf( stdout, "The error of the computation of convol is %e @ %e (CPU) / %e (GPU)\n", error, valueCPU, valueGPU);
    return -1;
  }
  
  fprintf( stdout, "\n\n---- Results ----\n\n");
  fprintf( stdout, "Sizes of matrices: M:%i  N:%i  K:%i\n\n", size, size, size);
  fprintf( stdout, "Best HWA time    : %f ms\n", best_measure_GPU / 1e3 );
  fprintf( stdout, "Mean HWA time    : %f ms\n", sum_measure_GPU / NB_RUNS / 1e3);
  fprintf( stdout, "\n");
  fprintf( stdout, "Best CPU time    : %f ms\n", best_measure_CPU / 1e3 );
  fprintf( stdout, "Mean CPU time    : %f ms\n", sum_measure_CPU / NB_RUNS / 1e3);
  fprintf( stdout, "\n");
  fprintf( stdout, "Global HWA time  : %f ms\n", global_GPU_time / 1e3 );
  fprintf( stdout, "Global CPU time  : %f ms\n", global_CPU_time / 1e3 );
  fprintf( stdout, "\n");
  fprintf( stdout, "Speed-up         : %f (computed on the best time)",
           ((float)best_measure_CPU)/best_measure_GPU); 
  
  fprintf( stdout, "\n");
  
  free(b);
  free(c_hwa);
  free(c_cpu);
  free(a_hwa);
  free(a_cpu);
  
#pragma hmpp <myGroup> release
  
  return 0;
}
