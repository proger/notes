/* 
 * Copyright 2008 - 2010 CAPS entreprise. All rights reserved.
 */


#include <getopt.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// Number of execution
#define NB_RUNS 5

// Size of the matrix
#define SIZE 1024

// Initialization random value
#define SRAND_VALUE 5347

// Use to initialize the matrix
float randFloat(float low, float high)
{
  float t = (float)rand() / (float)RAND_MAX;
  return (1.0f - t) * low + t * high;
}

////////////////////////////////////////////////////////////////////////////////
// sgemm_codelet
////////////////////////////////////////////////////////////////////////////////
#pragma hmpp mySgemm codelet, target=CUDA, args[*].transfer=atcall 
void mySgemm( int m, int n, int k, float alpha, float beta,
                float a[m][n],   float b[n][k], float c[m][k] )
{
  int i,j,l; // Induction variables
  float ab;  // Temporary result 
  
  for( j = 0 ; j < m ; j++ ) {
    for( i = 0 ; i < k ; i++ ) {
      ab=0.0f;
      for( l = 0 ; l < n ; l++ ){
        ab += a[j][l] * b[l][i];
      }
      c[j][i] = alpha * ab + beta * c[j][i];
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
// Main program
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
  
  int m=SIZE, n=SIZE, k = SIZE;
  
  float *a=NULL, *b=NULL, *c_hwa=NULL, *c_cpu=NULL;
  int i, j, ii;
  
  // For timer measures
  struct timeval tv_global_begin, tv_global_end; // global timer (all iterations)
  struct timeval tv_begin, tv_end;  // local timer (1 iteration)
  
  unsigned long long int best_measure_GPU = 0;
  unsigned long long int sum_measure_GPU  = 0;
  
  unsigned long long int best_measure_CPU = 0;
  unsigned long long int sum_measure_CPU  = 0;
  
  unsigned long long int global_CPU_time  = 0;
  unsigned long long int global_GPU_time  = 0;
  
  unsigned long long int current;
  
  float alpha, beta;
  
  double error    = 0.0;
  int index_i     = 0.0;
  int index_j     = 0.0;
  double valueCPU = 0.0;
  double valueGPU = 0.0;
  

         
  // Allocating CPU memory
  a = (float *)malloc(m* n * sizeof(float));
  b = (float *)malloc(n * k * sizeof(float));
  c_hwa = (float *)malloc(m * k * sizeof(float));
  c_cpu = (float *)malloc(m * k * sizeof(float));
  
  if((a == NULL) || (b == NULL) || (c_hwa == NULL) || (c_cpu == NULL)) {
    fprintf( stderr, "\n**** error : memory allocation failed ****\n\n");
    return 1;
  }
  
  fprintf( stdout, "---- Initialization of the Matrices ----\n\n");
  srand(SRAND_VALUE);
  
  //Generate options set
  for(i = 0; i < m; i++){
    for(j = 0; j < n; j++){
      a[i*n+j] = randFloat(0.0001f, 1.0f);
    }
  }
  
  for(i = 0; i < n; i++){
    for(j = 0; j < k; j++){
      b[i*k+j] = randFloat(0.0001f, 1.0f);  
    }
  }
  
  for(i = 0; i < m; i++){
    for(j = 0; j < k; j++) {
      c_cpu[i*k+j] = randFloat(1.0, 20.0f);
      c_hwa[i*k+j] = c_cpu[i*k+j]; 
    }
  }
  
  alpha = 0.5;
  beta  = randFloat(1.0, 2.0);
  

         
  fprintf( stdout, "---- Running calculations ----\n");
  
  //--------------------------------------
  // run sgemm on GPU (NB_RUNS iterations)
  //--------------------------------------

  printf("Run on GPU\n");
  
  // Start timer
  gettimeofday(&tv_global_begin, NULL);
  

  for( i=0; i<NB_RUNS; i++ ) {
    printf("%d ",i);
    gettimeofday(&tv_begin, NULL);
    
    // GPU call to mySgemm
#pragma hmpp mySgemm callsite
    mySgemm( m, n, k, alpha, beta, a, b, c_hwa );
    gettimeofday(&tv_end, NULL);
    
    current = (tv_end.tv_sec-tv_begin.tv_sec)*1e6 + tv_end.tv_usec-tv_begin.tv_usec;
    
    if( ( best_measure_GPU == 0 ) || ( best_measure_GPU > current ) ){
      best_measure_GPU = current;
    }
    sum_measure_GPU += current;   
  }  
  
  gettimeofday(&tv_global_end, NULL);
  global_GPU_time = (tv_global_end.tv_sec-tv_global_begin.tv_sec)*1e6 + tv_global_end.tv_usec-tv_global_begin.tv_usec;
  
  //--------------------------------------
  // run sgemm on CPU (NB_RUNS iterations)
  //--------------------------------------

  printf("\n\nRun on CPU\n");
  
  // Start timer
  gettimeofday(&tv_global_begin, NULL);
  
  for( i=0; i<NB_RUNS; i++ ) {
    printf("%d ",i);
    gettimeofday(&tv_begin, NULL);

    // CPU call to mySgemm
    mySgemm( m, n, k, alpha, beta, a, b, c_cpu );
    
    gettimeofday(&tv_end, NULL);
    current = (tv_end.tv_sec-tv_begin.tv_sec)*1e6 + tv_end.tv_usec-tv_begin.tv_usec;
    
    if( ( best_measure_CPU == 0 ) || ( best_measure_CPU > current ) ){
      best_measure_CPU = current;
    }  
    sum_measure_CPU += current;  
  } 
  
  gettimeofday(&tv_global_end, NULL);
  global_CPU_time = (tv_global_end.tv_sec-tv_global_begin.tv_sec)*1e6 + tv_global_end.tv_usec-tv_global_begin.tv_usec;
    
  // Compute error between GPU and CPU    
  for( ii = 0; ii < m; ii++){
    for(j = 0; j < k; j++){
      double lerror = fabs((c_hwa[ii*k+j]-c_cpu[ii*k+j])/c_cpu[ii*k+j]);
      if (lerror > error) {
        error = lerror;
        valueCPU = c_cpu[ii*k+j];
        valueGPU = c_hwa[ii*k+j];
        index_i = ii;
        index_j = j;
      }
    }
  }

  if (error > 2e-06) {
    fprintf( stdout, "\n\nThe error is %e with index %d:%d @ %e (CPU) / %e (GPU)\n", error, index_i, index_j, valueCPU, valueGPU);
    fprintf( stdout, "The error is is too big!\n");
    return -1;
  }

  fprintf( stdout, "\n\n---- Results ----");
  fprintf( stdout, "\n");
  fprintf( stdout, "Sizes of matrices: M:%i  N:%i  K:%i\n\n", m, n, k);
  fprintf( stdout, "Best HWA time    : %f ms\n", best_measure_GPU / 1e3 );
  fprintf( stdout, "Mean HWA time    : %f ms\n", sum_measure_GPU / NB_RUNS / 1e3);
  fprintf( stdout, "\n");
  fprintf( stdout, "Best CPU time    : %f ms\n", best_measure_CPU / 1e3 );
  fprintf( stdout, "Mean CPU time    : %f ms\n", sum_measure_CPU / NB_RUNS / 1e3);
  fprintf( stdout, "\n");
  fprintf( stdout, "Global HWA time  : %f ms\n", global_GPU_time / 1e3 );
  fprintf( stdout, "Global CPU time  : %f ms\n", global_CPU_time / 1e3 );
  fprintf( stdout, "\n");  
  fprintf( stdout, "Speed-up         : %f (computed on the best time)",
           ((float)best_measure_CPU)/best_measure_GPU);
  
  fprintf( stdout, "\n");

  free(a);
  free(b);
  free(c_hwa);
  free(c_cpu);

  return 0;
}
