#include <stdio.h>

/*********
 * INPUT *
 *********/

void read_args(int argc, char* argv[], char **input_file, char **output_file);
void read_header(FILE *fd, int *n);
void load_cities(FILE *fd, int n, int *travel_cost);

/********
 * TOUR *
 ********/

#define VISITED_TABLE

typedef struct  {
	int *cities; /* an array of visited cities, in order */
#ifdef VISITED_TABLE
	int *visited; /* a flag array to check quickly which city has already been visited in this tour */
#endif
	int size;
	int n;      /* how much cities are in the tour */
	int cost;
} tour_t;

#ifdef VISITED_TABLE
#define VISITED(tour, city) ((tour)->visited[(city)])
#else
#define VISITED(tour, city) visited(tour, city)
#endif

#define TRAVEL_COST(i,j) (travel_cost[(i) * n + (j)])

#define FEASIBLE(city, tour, best_tour, travel_cost, n) ( \
																   ( ! VISITED((tour), (city))) && \
																     ( \
																       ((best_tour)->cost < 0) || \
																       ( ((tour)->cost + TRAVEL_COST((tour)->cities[(tour)->n - 1], (city))) < (best_tour)->cost ) \
																     ) \
																 )

void alloc_tour(tour_t *tour, int n);
void free_tour(tour_t *tour);
void write_tour(FILE *fd, tour_t *tour);
void reset_tour(tour_t *tour, int start_city, int n);
void add_city(tour_t *tour, int city, int *travel_cost, int n);
void remove_last_city(tour_t *tour, int *travel_cost, int n);
void copy_tour(tour_t *dest, tour_t *src);
int feasible(int city, tour_t *tour, tour_t *best_tour, int *travel_cost, int n);
int visited(tour_t *tour, int city);

/**************
 * TOUR STACK *
 **************/

typedef struct {
	tour_t  *stack;
	int     size;
} tour_stack_t;

void alloc_tour_stack(tour_stack_t *stack, int n);
void free_tour_stack(tour_stack_t *stack);
void push_copy_tour(tour_stack_t *stack, tour_t *tour);
tour_t pop_tour(tour_stack_t *stack);
