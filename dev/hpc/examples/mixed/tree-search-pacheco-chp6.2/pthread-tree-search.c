#include "common.h"
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include <stdio.h>

extern int num_threads;

void* my_thread(void *data) {
	return NULL;
}

typedef struct {
	int n;
	int *travel_cost;
	tour_t *best_tour;
} thread_data_t;

void solve(int n, int *travel_cost, tour_t *best_tour, int start_city) {

	pthread_t thread[num_threads];
	tour_stack_t stack;
	tour_t tour;

	/* alloc, reset and init tours & stack */
	reset_tour(best_tour, -1/*start city*/, 0);
	alloc_tour_stack(&stack, n * (n+1) / 2 + 2 * n);
	alloc_tour(&tour, n);
	reset_tour(&tour, start_city, n);

	push_copy_tour(&stack, &tour);
	free_tour(&tour);
	/* make a breadth-first search to generate partial tours for threads */
	while (stack.size < num_threads) {
	}

	/* create all threads */
	if ( ! pthread_create(&thread, NULL, my_thread, NULL))
		if ( ! pthread_join(thread, NULL))
			;

	/* free */
	free_tour_stack(&stack);
}
