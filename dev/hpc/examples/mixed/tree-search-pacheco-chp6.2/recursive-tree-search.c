#include "common.h"
#include <stdlib.h>
#include <sys/time.h>

/*********************
 * SOLVE RECURSIVELY *
 *********************/

void do_solve_recursively(int n, int *travel_cost, tour_t *tour, tour_t *best_tour) {
	int next_city;

	/* we've found a complete tour */
	if (tour->n == n) {
		/* add start city to end of tour, in order to compute full cost. */
		add_city(tour, tour->cities[0], travel_cost, n); 

		/* best tour ? */
		if (best_tour->cost < 0 || tour->cost < best_tour->cost)
			copy_tour(best_tour, tour);

		/* remove start city from end of tour. */
		remove_last_city(tour, travel_cost, n);
	}

	/* we only have a partial tour */
	else {
		for (next_city = 0 ; next_city < n ; ++next_city) {
			if (FEASIBLE(next_city, tour, best_tour, travel_cost, n)) {
				add_city(tour, next_city, travel_cost, n);
				do_solve_recursively(n, travel_cost, tour, best_tour); /* recursive call */
				remove_last_city(tour, travel_cost, n);
			}
		}
	}
}

void solve(int n, int *travel_cost, tour_t *best_tour, int start_city) {

	tour_t tour;

	/* alloc & init */
	reset_tour(best_tour, -1/*start city*/, 0);
	alloc_tour(&tour, n);
	reset_tour(&tour, start_city, n);

	do_solve_recursively(n, travel_cost, &tour, best_tour);

	/* free memory */
	free_tour(&tour);
}
