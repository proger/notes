#include "common.h"
#include <stdlib.h>
#include <sys/time.h>

/*********************
 * SOLVE ITERATIVELY *
 * VERSION 1         *
 *********************/

#define NO_CITY (-1)

void print_stack(int *stack, int size) {
	int i;
	printf("STACK");
	for (i = 0 ; i < size ; ++i)
		printf(" %d", stack[i]);
	printf("\n");
}

void solve(int n, int *travel_cost, tour_t *best_tour, int start_city) {

	int *stack, stack_size, city, next_city;
	tour_t tour;

	/* alloc & init tour */
	reset_tour(best_tour, -1/*start city*/, 0);
	alloc_tour(&tour, n);
	reset_tour(&tour, start_city, n);

	/* alloc & fill stack
	   The stack is filled with cities to visit.
	   It is first initialized with all the cities. Then at each step we fill it with all the possible cities to visit next.
	   This its size can be up to : n + n-1 + n-2 + ... + 1 = n(n+1)/2.
	   We must add n more cells, since we must use markers (NO_CITY) between each set of cities to recognize each step.
	   For security we use a margin of n.
	   So total size is : n(n+1)/2 + 2n.
	 */
	stack = (int*)malloc(sizeof(int) * (n * (n+1) / 2 + 2 * n));
	stack_size = 0;
	for (city = n - 1 ; city >= 1 ; --city)
		if (city != start_city)
			stack[stack_size++] = city; /* push */

	while (stack_size > 0) {

		city = stack[--stack_size]; /* pop */

		if (city == NO_CITY) /* end of child list, backup */
			remove_last_city(&tour, travel_cost, n);

		else {
			add_city(&tour, city, travel_cost, n);

			/* we've found a complete tour */
			if (tour.n == n) {
				/* add start city to end of tour, in order to compute full cost. */
				add_city(&tour, tour.cities[0], travel_cost, n); 

				/* best tour ? */
				if (best_tour->cost < 0 || tour.cost < best_tour->cost)
					copy_tour(best_tour, &tour);

				/* remove start city from end of tour. */
				remove_last_city(&tour, travel_cost, n);

				/* remove last city of tour, in order to put next FEASIBLE instead. */
				remove_last_city(&tour, travel_cost, n);
			}

			/* we only have a partial tour */
			else {
				/* fill the stack with all possible next cities */
				stack[stack_size++] = NO_CITY;
				for (next_city = n - 1 ; next_city >= 0 ; --next_city)
					if (FEASIBLE(next_city, &tour, best_tour, travel_cost, n))
						stack[stack_size++] = next_city;
			}
		}
	}

	/* free */
	free_tour(&tour);
	free(stack);
}
