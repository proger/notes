#include "common.h"
#include <stdlib.h>
#include <sys/time.h>

void solve(int n, int *travel_cost, tour_t *best_tour, int start_city);

#ifdef PTHREAD
int num_threads;
#endif

/********
 * MAIN *
 ********/

int main(int argc, char* argv[]) {

	char *output_file, *input_file;
	FILE *fd;
	int n, *travel_cost;
	struct timeval start, finish;
	tour_t best_tour;
	int start_city = 0;

	/* read args & load input */
	read_args(argc, argv, &input_file, &output_file);
	fd = fopen(input_file, "r");
	if (fd == NULL) {
		printf("Can't open file \"%s\".\n", input_file);
		exit(1);
	}
	read_header(fd, &n);
	travel_cost = (int*)malloc(sizeof(int) * n * n);
	load_cities(fd, n, travel_cost);
	alloc_tour(&best_tour, n);
#   ifdef PTHREAD
	num_threads = atoi(getenv("NUM_THREADS"));
#   endif

	/* compute */
	gettimeofday(&start, NULL);
	solve(n, travel_cost, &best_tour, start_city);
	gettimeofday(&finish, NULL);
	printf("%f\n", (finish.tv_sec - start.tv_sec) + (finish.tv_usec - start.tv_usec)/1000000.0);
	fd = fopen(output_file, "w");
	write_tour(fd, &best_tour);
	fclose(fd);

	/* Free memory */
	free_tour(&best_tour);
	free(input_file);
	free(output_file);

	return 0;
}
