#include "common.h"
#include <stdlib.h>
#include <sys/time.h>

/*********************
 * SOLVE ITERATIVELY *
 * VERSION 2         *
 *********************/
 /* Push whole partial tours on the stack */

void solve(int n, int *travel_cost, tour_t *best_tour, int start_city) {

	int next_city;
	tour_stack_t stack;
	tour_t tour;

	/* alloc & init tour */
	reset_tour(best_tour, -1/*start city*/, 0);
	alloc_tour(&tour, n);
	reset_tour(&tour, start_city, n);

	/* alloc stack */
	alloc_tour_stack(&stack, n * (n+1) / 2 + 2 * n);

	push_copy_tour(&stack, &tour);
	free_tour(&tour);
	while (stack.size > 0) {
		tour = pop_tour(&stack);

		/* we've found a complete tour */
		if (tour.n == n) {
			/* add start city to end of tour, in order to compute full cost. */
			add_city(&tour, tour.cities[0], travel_cost, n); 

			/* best tour ? */
			if (best_tour->cost < 0 || tour.cost < best_tour->cost)
				copy_tour(best_tour, &tour);

			/* remove start city from end of tour. */
			remove_last_city(&tour, travel_cost, n);
		}

		/* we only have a partial tour */
		else {
			for (next_city = n - 1 ; next_city >= 0 ; --next_city)
				if (FEASIBLE(next_city, &tour, best_tour, travel_cost, n)) {
					add_city(&tour, next_city, travel_cost, n);
					push_copy_tour(&stack, &tour);
					remove_last_city(&tour, travel_cost, n);
				}
		}

		/* free current tour */
		free_tour(&tour);
	}

	/* free */
	free_tour_stack(&stack);
}
