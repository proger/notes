#include "common.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

/*************
 * READ ARGS *
 *************/

void read_args(int argc, char* argv[], char **input_file, char **output_file) {
	int c;

	while ((c = getopt(argc, argv, "i:o:")) != -1) {
		switch (c) {
			case 'i': /* input file */
				*input_file = (char*)malloc(sizeof(char*) * strlen(optarg) + 1);
				strcpy(*input_file, optarg);
				break;

			case 'o': /* output file */
				*output_file = (char*)malloc(sizeof(char*) * strlen(optarg) + 1);
				strcpy(*output_file, optarg);
				break;
		}
	}
}

/**************
 * LOAD INPUT *
 **************/

void read_header(FILE *fd, int *n) {
	fscanf(fd, "%d", n);
}

void load_cities(FILE *fd, int n, int *travel_cost) {

	int i, j, c;
	
	/* Read cities */
	for (i = 0 ; i < n ; ++i)
		for (j = 0 ; j < n ; ++j) {
			fscanf(fd, "%d", &c);
			travel_cost[i * n + j] = c;
		}
}

/******************
 * TOUR INIT/FREE *
 ******************/

void alloc_tour(tour_t *tour, int n) {

	tour->cities = (int*)malloc(sizeof(int) * (n+1));
#ifdef VISITED_TABLE
	tour->visited = (int*)malloc(sizeof(int) * n);
#endif
	tour->size = n;
	tour->n = 0;
	tour->cost = -1; /* undefined tour */
}

void free_tour(tour_t *tour) {
	free(tour->cities);
#ifdef VISITED_TABLE
	free(tour->visited);
#endif
}

/**********************
 * CHECK VISITED CITY *
 **********************/

int visited(tour_t *tour, int city) {
	int *p, *end;

	if (tour == NULL || tour->cost < 0)
		return 0;

	end = tour->cities + tour->n;
	for (p = tour->cities ; p < end ; ++p)
		if (*p == city)
			return 1;

	return 0;
}

/**************
 * WRITE TOUR *
 **************/

void write_tour(FILE *fd, tour_t *tour) {
	int i;
	
	fprintf(fd, "C%d ", tour->cost);
	for (i = 0 ; i < tour->n ; ++i)
		fprintf(fd, "%d ", tour->cities[i]);
	fprintf(fd, "\n");
}

/*******************
 * ADD/REMOVE CITY *
 *******************/

void reset_tour(tour_t *tour, int start_city, int n) {
#ifdef VISITED_TABLE
	int i;
#endif

	tour->n = 0;

	/* set a start city */
	if (start_city >= 0) {
		tour->cost = 0;
#ifdef VISITED_TABLE
		for (i = 0 ; i < n ; ++i)
			tour->visited[i] = 0;
#endif
		tour->size = n;
		add_city(tour, start_city, NULL, 0);
	}

	/* empty tour */
	else
		tour->cost = -1;
}

void add_city(tour_t *tour, int city, int *travel_cost, int n) {

	int last;

	/* compute travel cost */
	if (tour->n > 0 && travel_cost) {
		last = tour->cities[tour->n - 1];
		tour->cost += TRAVEL_COST(last, city);
	}
	tour->cities[tour->n++] = city;
#ifdef VISITED_TABLE
	++tour->visited[city]; /* use increment, so we can add start city at end of tour (so it's added twice), in order to compute real cost.*/
#endif
}

void remove_last_city(tour_t *tour, int *travel_cost, int n) {

	int last, city;

	city = tour->cities[--tour->n];
	tour->cities[tour->n] = -1; /* set as not visited */
	last = tour->cities[tour->n - 1];
	tour->cost -= TRAVEL_COST(last, city);
#ifdef VISITED_TABLE
	--tour->visited[city]; /* use decrement, so we can have twice the same city (useful for start city when it's added to end of tour) */
#endif
}

/*************
 * COPY TOUR *
 *************/

void copy_tour(tour_t *dest, tour_t *src) {
	int i;

	dest->n = src->n;
	dest->cost = src->cost;
	for (i = 0 ; i < src->n ; ++i)
		dest->cities[i] = src->cities[i];
#ifdef VISITED_TABLE
	dest->size = src->size;
	for (i = 0 ; i < src->size ; ++i)
		dest->visited[i] = src->visited[i];
#endif
}

/************
 * FEASIBLE *
 ************/

int feasible(int city, tour_t *tour, tour_t *best_tour, int *travel_cost, int n) {

	/* city already visited */
	if (VISITED(tour, city))
		return 0;

	if (best_tour->cost >= 0 && tour->cost + TRAVEL_COST(tour->cities[tour->n - 1], city) >= best_tour->cost)
		return 0;

	return 1;
}

/**************
 * TOUR STACK *
 **************/

void alloc_tour_stack(tour_stack_t *stack, int n) {

	stack->stack = (tour_t*)malloc(sizeof(tour_t) * n);
	stack->size = 0;
}

void free_tour_stack(tour_stack_t *stack) {
	free(stack->stack);
}


void push_copy_tour(tour_stack_t *stack, tour_t *tour) {
	tour_t *p;

	p = stack->stack + stack->size;
	alloc_tour(p, tour->size);
	copy_tour(p, tour);
	++stack->size;
}

tour_t pop_tour(tour_stack_t *stack) {
	return stack->stack[--stack->size];
}
