#include "common.h"
#include <mpi.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>

/*******
 * SUM *
 *******/

unsigned long long compute_sum(int start, int end) {
	int i;
	unsigned long s;

	s = 0;
	for (i = start ; i <= end ; ++i)
		s += i;

	return s;
}

/********
 * MAIN *
 ********/

int main(int argc, char *argv[]) {

	int n;
	char *output_file;
	unsigned long sum, local_sum;
	double start, finish, local_elapsed_time, elapsed_time;
	int my_rank, comm_sz;

	read_args(argc, argv, &n, &output_file);

	/* Initialize MPI */
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

	/* Compute sum */
	start = MPI_Wtime();
	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
	local_sum = compute_sum(n * my_rank / comm_sz + 1, n * (my_rank + 1) / comm_sz);
	MPI_Reduce(&local_sum, &sum, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
	finish = MPI_Wtime();
	local_elapsed_time = finish - start;
	MPI_Reduce(&local_elapsed_time, &elapsed_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

	/* Print time elapsed */
	if (my_rank == 0)
		printf("%f\n", finish - start);

	/* Print output */
	if (my_rank == 0) {
		write_sum(output_file, sum);
		free(output_file);
	}

	/* Terminate MPI */
	MPI_Finalize();

	return 0;
}

