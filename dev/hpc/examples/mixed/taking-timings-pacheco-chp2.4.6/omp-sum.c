#include "common.h"
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>

/********************
 * GLOBAL VARIABLES *
 ********************/

int num_threads;

/*******
 * SUM *
 *******/

unsigned long long compute_sum(int n) {
	int i;
	unsigned long s;

	s = 0;
#   pragma omp parallel for num_threads(num_threads) default(none) reduction(+:s) shared(n) private(i)
	for (i = 1 ; i <= n ; ++i)
		s += i;

	return s;
}

/********
 * MAIN *
 ********/

int main(int argc, char *argv[]) {

	int n;
	char *output_file;
	unsigned long sum;
	struct timeval start, finish;

	read_args(argc, argv, &n, &output_file);
	num_threads = atoi(getenv("NUM_THREADS"));
	gettimeofday(&start, NULL);
	sum = compute_sum(n);
	gettimeofday(&finish, NULL);
	printf("%f\n", (finish.tv_sec - start.tv_sec) + (finish.tv_usec - start.tv_usec)/1000000.0);
	write_sum(output_file, sum);
	free(output_file);

	return 0;
}
