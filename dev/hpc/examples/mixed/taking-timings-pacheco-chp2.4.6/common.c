#include "common.h"
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

/*************
 * READ ARGS *
 *************/

void read_args(int argc, char* argv[], int *n, char **output_file) {
	int c;

	while ((c = getopt(argc, argv, "n:o:")) != -1) {
		switch (c) {
			case 'n': /* sum from 1 to n included */
				*n = atoi(optarg);
				break;
			break;

			case 'o': /* output file */
				*output_file = (char*)malloc(sizeof(char*) * strlen(optarg) + 1);
				strcpy(*output_file, optarg);
				break;
		}
	}
}

/*************
 * WRITE SUM *
 *************/

void write_sum(char *output_file, unsigned long sum) {
	FILE *fd = fopen(output_file, "w");
	fprintf(fd, "%lu\n", sum);
	fclose(fd);
}
