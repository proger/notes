#include "common.h"
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

/*************
 * READ ARGS *
 *************/

void read_args(int argc, char* argv[], char **input_file, char **output_file) {
	int c;

	while ((c = getopt(argc, argv, "i:o:")) != -1) {
		switch (c) {
			case 'i': /* input file */
				*input_file = (char*)malloc(sizeof(char*) * strlen(optarg) + 1);
				strcpy(*input_file, optarg);
				break;

			case 'o': /* output file */
				*output_file = (char*)malloc(sizeof(char*) * strlen(optarg) + 1);
				strcpy(*output_file, optarg);
				break;
		}
	}
}

/****************
 * LOAD PLANETS *
 ****************/
void read_header(FILE *fd, int *n, int *t, double *dt) {
	fscanf(fd, "%d %d %lf", n, t, dt);
}

void load_planets(FILE *fd, int n, double *mass, vect_t *pos, vect_t *vel) {

	int i;
	
	/* Read planets */
	for (i = 0 ; i < n ; ++i)
		fscanf(fd, "%lf %lf %lf %lf %lf", &pos[i].x, &pos[i].y, &vel[i].x, &vel[i].y, &mass[i]);
}

/*****************
 * WRITE PLANETS *
 *****************/

void write_planets(FILE *fd, int step, int n, vect_t *pos) {
	int p;

	fprintf(fd, "%d", step);
	for (p = 0 ; p < n ; ++p)
		fprintf(fd, " %g,%g", pos[p].x, pos[p].y);
	fprintf(fd, "\n");
}

/***********
 * COMPUTE *
 ***********/

/* Compute the force exerted on a by b */
void compute_force_ab(double mass_a, vect_t *pos_a, double mass_b, vect_t *pos_b, vect_t *force_ab) {

	double x_diff, y_diff, dist, dist_cubed;

	x_diff = pos_a->x - pos_b->x;
	y_diff = pos_a->y - pos_b->y;
	dist = sqrt(x_diff * x_diff + y_diff * y_diff);
	dist_cubed = dist * dist * dist;

	force_ab->x = - G * mass_a * mass_b / dist_cubed * x_diff;
	force_ab->y = - G * mass_a * mass_b / dist_cubed * y_diff;
}

void compute_force_basic(int p, int n, double *mass, vect_t *pos, vect_t *force_p) {

	int q;
	vect_t force_pq;

	for (q = 0 ; q < n ; ++q)
		if (p != q) {
			compute_force_ab(mass[p], pos + p, mass[q], pos + q, &force_pq);
			force_p->x += force_pq.x;
			force_p->y += force_pq.y;
		}
}

void compute_force_reduced(int p, int n, double *mass, vect_t *pos, vect_t *force_p, vect_t *force) {

	vect_t force_pq;
	int q;

	for (q = p + 1 ; q < n ; ++q) {
		compute_force_ab(mass[p], pos + p, mass[q], pos + q, &force_pq);
		force_p->x += force_pq.x;
		force_p->y += force_pq.y;
		force[q].x -= force_pq.x;
		force[q].y -= force_pq.y;
	}
}

void sum_force(int p, int n, vect_t *force, int m, vect_t **local_force) {

	int i;
	
	for (i = 0 ; i < m ; ++i) {
		force[p].x += local_force[i][p].x;
		force[p].y += local_force[i][p].y;
	}
}

void update_planet(double dt, double mass, vect_t *pos, vect_t *vel, vect_t *force) {

	/* compute next position and velocity */
	pos->x += dt * vel->x;
	pos->y += dt * vel->y;
	vel->x += dt * mass * force->x;
	vel->y += dt * mass * force->y;
}
