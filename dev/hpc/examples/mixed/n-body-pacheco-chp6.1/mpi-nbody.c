/* This program solves the n-body problem, for a system of planets.
 * Example take from : An Introduction to Parallel Programming, Peter S. Pacheco, p271
 */

#include "common.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>
#include <mpi.h>

/*********
 * SOLVE *
 *********/

#ifdef REDUCED

/* The reduced solution uses a ring pass organization, in which each node communicate with a source node from which it receives data and a destination node to which it sends data.
 * The particles are distributed in a cyclic order among the nodes.
 */

void send_and_receive_buf(int n, vect_t *buf, MPI_Datatype vect_mpi_t, int dest, int source) {
	int my_rank;
	vect_t *tmp_buf;
	
	/* MPI_Send is a blocking send. So we must make sure that for each send there's a symetric recv.
	 * To solve this, we decide that processes with an even rank will send first, and those with an odd rank will receive first.
	 *
	 * Note: apparently MPI_Send isn't always blocking. It wasn't blocking for cases with N=100 and 2 or 4 processes. But for N=1000 and 2 processes, it was blocking. Why ?
	 */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	if (my_rank % 2 == 0) { /* send first for even cases. Necessary for case where there's only one process which sends to itself. */
		MPI_Send(buf, n, vect_mpi_t, dest, 99, MPI_COMM_WORLD);
		MPI_Recv(buf, n, vect_mpi_t, source, 99, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}
	else {
		tmp_buf = (vect_t*)malloc(sizeof(vect_t) * n);
		MPI_Recv(tmp_buf, n, vect_mpi_t, source, 99, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Send(buf, n, vect_mpi_t, dest, 99, MPI_COMM_WORLD);
		memcpy(buf, tmp_buf, sizeof(vect_t) * n);
		free(tmp_buf);
	}
}

int compute_owner_of_received_data(int my_rank, int comm_sz, int phase) {
	return (my_rank + phase) % comm_sz; /* Compute owner of the positions and forces we're receiving */
}

/* Compute global index for a cyclic scheduling */
#define GLOBAL_INDEX(i, rank, comm_sz) ((i) * (comm_sz) + (rank))

void update_pos(vect_t *pos, vect_t *loc_pos, int loc_n, int rank, int comm_sz) {
	int i;

	for (i = 0 ; i < loc_n ; ++i) {
		pos[GLOBAL_INDEX(i, rank, comm_sz)].x = loc_pos[i].x;
		pos[GLOBAL_INDEX(i, rank, comm_sz)].y = loc_pos[i].y;
	}
}

void solve_reduced(int my_rank, int comm_sz, MPI_Datatype vect_mpi_t, int loc_n, int n, int t, double dt, double *mass, vect_t *pos, vect_t *vel, FILE *fd) {

	int step, phase, p, q;
	int source, dest, owner;
	vect_t force_pq;
	vect_t *loc_force = (vect_t*)malloc(sizeof(vect_t) * loc_n);
	vect_t *loc_vel   = (vect_t*)malloc(sizeof(vect_t) * loc_n);
	vect_t *tmp_force = (vect_t*)malloc(sizeof(vect_t) * loc_n);
	vect_t *loc_pos   = (vect_t*)malloc(sizeof(vect_t) * loc_n);
	vect_t *tmp_pos   = (vect_t*)malloc(sizeof(vect_t) * loc_n);
	double *loc_mass  = (double*)malloc(sizeof(double) * loc_n);
	MPI_Status status;

	/* set loc_mass */
	for (p = 0 ; p < loc_n ; ++p)
		loc_mass[p] = mass[GLOBAL_INDEX(p, my_rank, comm_sz)];

	/* set loc_vel */
	for (p = 0 ; p < loc_n ; ++p)
		loc_vel[p] = vel[GLOBAL_INDEX(p, my_rank, comm_sz)];

	/* set local positions */
	for (p = 0 ; p < loc_n ; ++p) {
		loc_pos[p].x = pos[GLOBAL_INDEX(p, my_rank, comm_sz)].x;
		loc_pos[p].y = pos[GLOBAL_INDEX(p, my_rank, comm_sz)].y;
	}

	/* set source and destination with which to communicate */
	source = (my_rank + 1) % comm_sz;
	dest   = (my_rank - 1 + comm_sz) % comm_sz;

	/* Iterate over all time steps from 1 to t.
	   Time step 0 is the initial position.	 */
	if (my_rank == 0)
		write_planets(fd, 0, n, pos);
	for (step = 1 ; step <= t ; ++step) {

		/* copy positions */
		for (p = 0 ; p < loc_n ; ++p) {
			tmp_pos[p].x = loc_pos[p].x;
			tmp_pos[p].y = loc_pos[p].y;
		}

		/* reset forces */
		for (p = 0 ; p < loc_n ; ++p) {
			loc_force[p].x = loc_force[p].y = 0.0;
			tmp_force[p].x = tmp_force[p].y = 0.0;
		}

		/* Compute forces due to interactions among my particles */
		for (p = 0 ; p < loc_n ; ++p)
			compute_force_reduced(p, loc_n, loc_mass, loc_pos, loc_force + p, tmp_force);

		/* Ring pass algo.
		 * On each phase we get positions of a new set of particles from source, for which we computes forces.
		 * We update the tmp_force array that we pass to dest.
		 *
		 * At then end of the ring pass, we get back the first tmp_force array that we sent. It has traveled accross all nodes, and has been completed with all missing forces.
		 */
		for (phase = 1 ; phase < comm_sz ; ++phase ) {

			/* Send & receive tmp arrays */
			owner = compute_owner_of_received_data(my_rank, comm_sz, phase);
			send_and_receive_buf(loc_n, tmp_pos, vect_mpi_t, dest, source);
			send_and_receive_buf(loc_n, tmp_force, vect_mpi_t, dest, source);

			/* Compute forces due to interactions among my particles and owner's particles */
			for (p = 0 ; p < loc_n ; ++p) { /* my particles */
				for (q = 0 ; q < loc_n ; ++q) /* owner's particles */
					if (GLOBAL_INDEX(q, owner, comm_sz) > GLOBAL_INDEX(p, my_rank, comm_sz)) { /* condition of reduced algo */
						compute_force_ab(mass[GLOBAL_INDEX(p, my_rank, comm_sz)],
						                 loc_pos + p,
						                 mass[GLOBAL_INDEX(q, owner, comm_sz)],
						                 tmp_pos + q,
						                 &force_pq);
						loc_force[p].x += force_pq.x;
						loc_force[p].y += force_pq.y;
						tmp_force[q].x -= force_pq.x;
						tmp_force[q].y -= force_pq.y;
					}
			}
		}

		/* Send & receive tmp forces */
		send_and_receive_buf(loc_n, tmp_force, vect_mpi_t, dest, source);

		/* Add tmp_forces (owner = my_rank, since we get back first tmp_force we sent) to local_forces */
		for (p = 0 ; p < loc_n ; ++p) { /* my particles */
			loc_force[p].x += tmp_force[p].x;
			loc_force[p].y += tmp_force[p].y;
		}

		/* Update my positions */
		for (p = 0 ; p < loc_n ; ++p)
			update_planet(dt, loc_mass[p], loc_pos + p, loc_vel + p, loc_force + p);

		/* Update global pos array */
		if (my_rank == 0) {
			update_pos(pos, loc_pos, loc_n, my_rank, comm_sz); /* update global pos array */
			for (p = 1 ; p < comm_sz ; ++p)
				if ( ! MPI_Recv(tmp_pos, loc_n, vect_mpi_t, MPI_ANY_SOURCE, 97, MPI_COMM_WORLD, &status))
					update_pos(pos, tmp_pos, loc_n, status.MPI_SOURCE, comm_sz);
		}
		else
			MPI_Send(loc_pos, loc_n, vect_mpi_t, 0, 97, MPI_COMM_WORLD);

		/* Log positions */
		if (my_rank == 0)
			write_planets(fd, step, n, pos);
	}

	free(loc_force);
	free(loc_pos);
	free(loc_mass);
	free(tmp_force);
	free(tmp_pos);
}

#else

void solve_basic(int my_rank, MPI_Datatype vect_mpi_t, int loc_n, int n, int t, double dt, double *mass, vect_t *pos, vect_t *vel, FILE *fd) {

	int p, step;
	vect_t *loc_force = (vect_t*)malloc(sizeof(vect_t) * loc_n);
	vect_t *loc_vel = (vect_t*)malloc(sizeof(vect_t) * loc_n);

	MPI_Scatter(vel, loc_n, vect_mpi_t, loc_vel, loc_n, vect_mpi_t, 0, MPI_COMM_WORLD);
	
	/* Iterate over all time steps from 1 to t.
	   Time step 0 is the initial position.	 */
	if (my_rank == 0)
		write_planets(fd, 0, n, pos);
	for (step = 1 ; step <= t ; ++step) {

		for (p = 0 ; p < loc_n ; ++p)
			loc_force[p].x = loc_force[p].y = 0.0;

		for (p = 0 ; p < loc_n ; ++p)
			compute_force_basic(p + my_rank * loc_n, n, mass, pos, loc_force + p);

		for (p = 0 ; p < loc_n ; ++p)
			update_planet(dt,
						  mass[p + my_rank * loc_n],
						  pos + p + my_rank * loc_n,
						  loc_vel + p,
						  loc_force + p);

		MPI_Allgather(MPI_IN_PLACE, loc_n, vect_mpi_t, pos, loc_n, vect_mpi_t, MPI_COMM_WORLD);

		if (my_rank == 0)
			write_planets(fd, step, n, pos);
	}

	free(loc_force);
}

#endif /* REDUCED */

/********
 * MAIN *
 ********/

int main(int argc, char* argv[]) {

	int n, loc_n; /* number of planets */
	int t; /* the number of iterations, we stop simulation at t*dt */
	double dt; /* delta T, the size of the time step, in seconds */
	char *output_file, *input_file;
	double *mass = NULL;
	vect_t *pos = NULL;
	vect_t *vel = NULL;
	double start, finish, local_elapsed_time, elapsed_time;
	int my_rank, comm_sz;
	FILE *fd = NULL;

	/* MPI vect_mpi_t data type */
	MPI_Datatype vect_mpi_t;
	int array_of_blocklengths[1] = {2};
	MPI_Aint array_of_displacements[1] = {0};
	MPI_Datatype array_of_types[1] = {MPI_DOUBLE};

	/* Initialize MPI */
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

	/* read args */
	if (my_rank == 0)
		read_args(argc, argv, &input_file, &output_file);

	/* open input file and read header */
	if (my_rank == 0) {
		fd = fopen(input_file, "r");
		read_header(fd, &n, &t, &dt);
	}
	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&t, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&dt, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	loc_n = n / comm_sz;

	/* create vect_mpi_t data type */
	MPI_Type_create_struct(1, array_of_blocklengths, array_of_displacements, array_of_types, &vect_mpi_t);
	MPI_Type_commit(&vect_mpi_t);

	/* create arrays */
	mass = (double*)malloc(sizeof(double) * n);
	pos = (vect_t*)malloc(sizeof(vect_t) * n);
#ifdef REDUCED
	vel = (vect_t*)malloc(sizeof(vect_t) * n);
#else
	if (my_rank == 0)
		vel = (vect_t*)malloc(sizeof(vect_t) * n);
#endif

	/* load planets and close file */
	if (my_rank == 0) {
		load_planets(fd, n, mass, pos, vel);
		fclose(fd);
	}

	/* compute */
	start = MPI_Wtime();
	if (my_rank == 0)
		fd = fopen(output_file, "w");
	MPI_Bcast(mass, n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Bcast(pos, n, vect_mpi_t, 0, MPI_COMM_WORLD);
#ifdef REDUCED
	MPI_Bcast(vel, n, vect_mpi_t, 0, MPI_COMM_WORLD);
	solve_reduced(my_rank, comm_sz, vect_mpi_t, loc_n, n, t, dt, mass, pos, vel, fd);
#else
	solve_basic(my_rank, vect_mpi_t, loc_n, n, t, dt, mass, pos, vel, fd);
#endif
	if (my_rank == 0)
		fclose(fd);
	finish = MPI_Wtime();
	local_elapsed_time = finish - start;
	MPI_Reduce(&local_elapsed_time, &elapsed_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

	/* Print time elapsed */
	if (my_rank == 0)
		printf("%f\n", finish - start);

	/* Free memory */
	MPI_Type_free(&vect_mpi_t);
	if (my_rank == 0) {
		free(input_file);
		free(output_file);
		free(pos);
		free(vel);
		free(mass);
	}

	/* Terminate MPI */
	MPI_Finalize();

	return 0;
}
