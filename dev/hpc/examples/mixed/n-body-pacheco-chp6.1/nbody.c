/* This program solves the n-body problem, for a system of planets.
 * Example take from : An Introduction to Parallel Programming, Peter S. Pacheco, p271
 */

#include "common.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#ifdef _OPENMP
#include <omp.h>
#endif

/********************
 * GLOBAL VARIABLES *
 ********************/

#define BLOCK 0
#define CYCLIC 1
#define ALL_CYCLIC 2
#ifndef SCHEDULING
	#define SCHEDULING BLOCK
#endif

#ifdef _OPENMP
int num_threads;
#endif

/*********
 * SOLVE *
 *********/

void solve(int n, int t, double dt, double *mass, vect_t *pos, vect_t *vel, FILE *fd) {

	int p, step;
	vect_t *force = (vect_t*)malloc(sizeof(vect_t) * n);
	#ifdef _OPENMP
		int thread;
		vect_t **local_force;
	#endif
	
	#if defined _OPENMP && defined REDUCED
	local_force = (vect_t**)malloc(sizeof(vect_t**) * num_threads);
	for (thread = 0 ; thread < num_threads ; ++thread)
		local_force[thread] = (vect_t*)malloc(sizeof(vect_t) * n);
	#endif

	/* Iterate over all time steps from 1 to t.
	   Time step 0 is the initial position.	 */
	write_planets(fd, 0, n, pos);
	#pragma omp parallel num_threads(num_threads) default(none) shared(n, dt, mass, pos, vel, force, fd, t, num_threads, local_force) private(step, p, thread) /* allow reuse of the same set of threads for the following OMP for instructions. */
	for (step = 1 ; step <= t ; ++step) {

		/* reset array of forces */
		#pragma omp for schedule(static, SCHEDULING == ALL_CYCLIC ? 1 : n/num_threads)
		for (p = 0 ; p < n ; ++p) {
			force[p].x = force[p].y = 0.0;
			#if defined _OPENMP && defined REDUCED
				for (thread = 0 ; thread < num_threads ; ++thread)
					local_force[thread][p].x = local_force[thread][p].y = 0.0;
			#endif
		}

		/* compute forces */
		#pragma omp for schedule(static, SCHEDULING != BLOCK ? 1 : n/num_threads)
		for (p = 0 ; p < n ; ++p) {
			#ifdef REDUCED
				#ifdef _OPENMP
					thread = omp_get_thread_num();
					compute_force_reduced(p, n, mass, pos, local_force[thread] + p, local_force[thread]);
				#else
					compute_force_reduced(p, n, mass, pos, force + p, force);
				#endif
			#else
				compute_force_basic(p, n, mass, pos, force + p);
			#endif
		}

		/* sum forces */
		#if defined _OPENMP && defined REDUCED
			#pragma omp for schedule(static, SCHEDULING == ALL_CYCLIC ? 1 : n/num_threads)
			for (p = 0 ; p < n ; ++p)
				sum_force(p, n, force, num_threads, local_force);
		#endif

		/* update positions & velocities */
		#pragma omp for schedule(static, SCHEDULING == ALL_CYCLIC ? 1 : n/num_threads)
		for (p = 0 ; p < n ; ++p)
			update_planet(dt, mass[p], pos+p, vel+p, force+p);

		#pragma omp single
		write_planets(fd, step, n, pos);
	}

	/* Free memory */
	#if defined _OPENMP && defined REDUCED
	for (thread = 0 ; thread < num_threads ; ++thread)
		free(local_force[thread]);
	free(local_force);
	#endif
	free(force);
}

/********
 * MAIN *
 ********/

int main(int argc, char* argv[]) {

	int n; /* number of planets */
	int t; /* the number of iterations, we stop simulation at t*dt */
	double dt; /* delta T, the size of the time step, in seconds */
	char *output_file, *input_file;
	double *mass;
	vect_t *pos, *vel;
	struct timeval start, finish;

	/* read args & load input */
#   ifdef _OPENMP
	num_threads = atoi(getenv("NUM_THREADS"));
#   endif
	read_args(argc, argv, &input_file, &output_file);
	FILE *fd = fopen(input_file, "r");
	read_header(fd, &n, &t, &dt);
	mass = (double*)malloc(sizeof(double) * n);
	pos = (vect_t*)malloc(sizeof(vect_t) * n);
	vel = (vect_t*)malloc(sizeof(vect_t) * n);
	load_planets(fd, n, mass, pos, vel);
	fclose(fd);

	/* compute */
	gettimeofday(&start, NULL);
	fd = fopen(output_file, "w");
	solve(n, t, dt, mass, pos, vel, fd);
	fclose(fd);
	gettimeofday(&finish, NULL);
	printf("%f\n", (finish.tv_sec - start.tv_sec) + (finish.tv_usec - start.tv_usec)/1000000.0);

	/* Free memory */
	free(input_file);
	free(output_file);
	free(pos);
	free(vel);
	free(mass);

	return 0;
}
