#include <stdio.h>

/*************
 * CONSTANTS *
 *************/

/* Gravitational constant */
#define G 6.673e-11 /* in m^3/(kg.s^2) */

/**********
 * PLANET *
 **********/

typedef struct {
	double x, y; /* the problem is solved in 2D, not in 3D. */
} vect_t;

/*************
 * FUNCTIONS *
 *************/

void read_args(int argc, char* argv[], char **input_file, char **output_file);
void read_header(FILE *fd, int *n, int *t, double *dt);
void load_planets(FILE *fd, int n, double *mass, vect_t *pos, vect_t *vel);
void write_planets(FILE *fd, int step, int n, vect_t *pos);
void compute_force_ab(double mass_a, vect_t *pos_a, double mass_b, vect_t *pos_b, vect_t *force_ab);
void compute_force_basic(int p, int n, double *mass, vect_t *pos, vect_t *force);
void compute_force_reduced(int p, int n, double *mass, vect_t *pos, vect_t *force_p, vect_t *force);
void sum_force(int p, int n, vect_t *force, int m, vect_t **local_force);
void update_planet(double dt, double mass, vect_t *pos, vect_t *vel, vect_t *force);
