#define PROGRAM_FILE "matmul-v3.cl"
#define KERNEL_FUNC "mat_mul"
#define ARRAY_SIZE 255
#define ARRAY_GRID 256
/* For too high values (or wrong values ?) of TILE_WIDTH, OpenCL can output cryptic errors like:
   "Couldn't enqueue the kernel: Function not implemented"
   In this case, just lower the tile width until the error disappears.
 */
#define TILE_WIDTH 16
#define MAX_ITER 1
#define TIMING 1

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

/* Find a device associated with the first available platform */
cl_device_id create_device() {

   cl_platform_id platform;
   cl_device_id dev[10],device;
   cl_uint number,i;
   int err;

   /* Identify a platform */
   err = clGetPlatformIDs(2, &platform, &number);
   if(err < 0) {
      perror("Couldn't identify a platform");
      exit(1);
   } 
   char buffer[10240]; 
   cl_uint buf_uint,idims;
   cl_ulong buf_ulong;
   size_t wgsize,wisize[3];
   
   for(i=0; i<number; i++) {
       clGetPlatformInfo(platform,CL_PLATFORM_PROFILE,10240,buffer,NULL);
       printf("PROFILE=%s\n",buffer);
       clGetPlatformInfo(platform,CL_PLATFORM_VERSION,10240,buffer,NULL);
       printf("VERSION=%s\n",buffer);
       clGetPlatformInfo(platform,CL_PLATFORM_NAME,10240,buffer,NULL);
       printf("NAME=%s\n",buffer);
       clGetPlatformInfo(platform,CL_PLATFORM_VENDOR,10240,buffer,NULL);
       printf("VENDOR=%s\n",buffer);
       clGetPlatformInfo(platform,CL_PLATFORM_EXTENSIONS,10240,buffer,NULL);
       printf("EXTENSIONS=%s\n",buffer);
   }

   /* Access a device */
   err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 10, dev, &number);
   if(err == CL_DEVICE_NOT_FOUND) {
      perror("Couldn't access any devices");
      exit(1);   
   }
   for(i=0; i<number; i++) {
       printf("--%d--\n",i); 
       clGetDeviceInfo(dev[i],CL_DEVICE_NAME,10240,buffer,NULL);
       printf("DEVICE NAME=%s\n",buffer);
       clGetDeviceInfo(dev[i],CL_DEVICE_VENDOR,10240,buffer,NULL);
       printf("DEVICE VENDOR=%s\n",buffer);
       clGetDeviceInfo(dev[i],CL_DEVICE_VERSION,10240,buffer,NULL);
       printf("DEVICE VERSION=%s\n",buffer);
       clGetDeviceInfo(dev[i],CL_DEVICE_MAX_COMPUTE_UNITS,sizeof(buf_uint),&buf_uint,NULL);
       printf("DEVICE_MAX_COMPUTE_UNITS=%u\n",buf_uint);
       clGetDeviceInfo(dev[i],CL_DEVICE_MAX_WORK_GROUP_SIZE,sizeof(size_t),&wgsize,NULL);
       printf("DEVICE_MAX_WORK_GROUP_SIZE=%i\n",(int)wgsize);
       clGetDeviceInfo(dev[i],CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,sizeof(cl_uint),&idims,NULL);
       printf("DEVICE_MAX_WORK_ITEM_DIMENSIONS=%u\n",idims);
       clGetDeviceInfo(dev[i],CL_DEVICE_MAX_WORK_ITEM_SIZES,sizeof(size_t[3]),&wisize,NULL);
       printf("DEVICE_MAX_WORK_ITEM_SIZES=%lu %lu %lu\n",wisize[0],wisize[1],wisize[2]);
       clGetDeviceInfo(dev[i],CL_DEVICE_GLOBAL_MEM_SIZE,sizeof(cl_ulong),&buf_ulong,NULL);
       printf("DEVICE_GLOBAL_MEM_SIZE=%llu\n",buf_ulong);
   }
   device=dev[1]; 
   return device;
}

/* Create program from a file and compile it */
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename) {

   cl_program program;
   FILE *program_handle;
   char *program_buffer, *program_log;
   size_t program_size, log_size;
   int err;

   /* Read program file and place content into buffer */
   program_handle = fopen(filename, "r");
   if(program_handle == NULL) {
      perror("Couldn't find the program file");
      exit(1);
   }
   fseek(program_handle, 0, SEEK_END);
   program_size = ftell(program_handle);
   rewind(program_handle);
   program_buffer = (char*)malloc(program_size + 1);
   program_buffer[program_size] = '\0';
   fread(program_buffer, sizeof(char), program_size, program_handle);
   fclose(program_handle);

   /* Create program from file */
   program = clCreateProgramWithSource(ctx, 1, 
      (const char**)&program_buffer, &program_size, &err);
   if(err < 0) {
      perror("Couldn't create the program");
      exit(1);
   }
   free(program_buffer);

   /* Build program */
char options[1000];

   strcpy(options, "");
   strcat(options, "-cl-mad-enable ");
#ifdef MIC
   strcat(options, "-auto-prefetch-level=3 ");
#endif
   fprintf(stdout, "Build OpenCL (opts=\"%s\")\n", options);

   err = clBuildProgram(program, 0, NULL, options, NULL, NULL);
   if(err < 0) {

      /* Find size of log and print to std output */
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
            0, NULL, &log_size);
      program_log = (char*) malloc(log_size + 1);
      program_log[log_size] = '\0';
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 
            log_size + 1, program_log, NULL);
      printf("%s\n", program_log);
      free(program_log);
      exit(1);
   }

   return program;
}

int main() {

   /* OpenCL structures */
   cl_device_id device;
   cl_context context;
   cl_program program;
   cl_kernel kernel;
   cl_command_queue queue;
   cl_int i, j, err,counter;
   size_t local_size[2], global_size[2];

   /* Data and buffers */
   float data[ARRAY_SIZE*ARRAY_SIZE];
   float data2[ARRAY_SIZE*ARRAY_SIZE], total, sum[ARRAY_SIZE*ARRAY_SIZE];
   cl_mem input_buffer, input_buffer2,sum_buffer;

/* Initialize data */
   for(i=0; i<ARRAY_SIZE; i++)
       for(j=0; j<ARRAY_SIZE; j++) {
      data[j*ARRAY_SIZE+i] = (float)(i+1);
      data2[j*ARRAY_SIZE+i] = 1.0f/(float)(j+1);
      sum[j*ARRAY_SIZE+i] = 0.0f;
   }

   /* Create device and context */
   device = create_device();
   context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
   if(err < 0) {
      perror("Couldn't create a context");
      exit(1);   
   }

   /* Build program */
   program = build_program(context, device, PROGRAM_FILE);

   /* Create data buffer */
   global_size[0] = ARRAY_GRID;
   global_size[1] = ARRAY_GRID;
   local_size[0] = TILE_WIDTH;
   local_size[1] = TILE_WIDTH;
   input_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY |
         CL_MEM_COPY_HOST_PTR, ARRAY_SIZE*ARRAY_SIZE * sizeof(float), data, &err);
   input_buffer2 = clCreateBuffer(context, CL_MEM_READ_ONLY |
         CL_MEM_COPY_HOST_PTR, ARRAY_SIZE*ARRAY_SIZE*sizeof(float), data2, &err);
   sum_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE |
         CL_MEM_COPY_HOST_PTR, ARRAY_SIZE*ARRAY_SIZE*sizeof(float),sum, &err);
   if(err < 0) {
      perror("Couldn't create a buffer");
      exit(1);   
   };

   /* Create a command queue */
   queue = clCreateCommandQueue(context, device, 0, &err);
   if(err < 0) {
      perror("Couldn't create a command queue");
      exit(1);   
   };

   /* Create a kernel */
   kernel = clCreateKernel(program, KERNEL_FUNC, &err);
   if(err < 0) {
      perror("Couldn't create a kernel");
      exit(1);
   };

   /* Create kernel arguments */
   int asize=ARRAY_SIZE;
   err = clSetKernelArg(kernel, 0, sizeof(int), &asize);
   err = clSetKernelArg(kernel, 1, sizeof(int), &asize);
   err = clSetKernelArg(kernel, 2, sizeof(int), &asize);
   err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &input_buffer);
   err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &input_buffer2);
   err |= clSetKernelArg(kernel, 5, sizeof(cl_mem), &sum_buffer);
   if(err < 0) {
      perror("Couldn't create a kernel argument");
      exit(1);
   }


#ifdef TIMING
struct timeval t0,t1;
gettimeofday(&t0, NULL);
#endif
   /* Enqueue kernel */
   for(i=0; i<MAX_ITER; i++) {
   err = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, global_size, 
         local_size, 0, NULL, NULL); 
   if(err < 0) {
      perror("Couldn't enqueue the kernel");
      exit(1);
   }
   }
#ifdef TIMING
clFinish(queue);
gettimeofday(&t1, NULL);
double time_seconds=(t1.tv_sec-t0.tv_sec)+0.000001*(t1.tv_usec- t0.tv_usec);
long int NumOps = 2 * pow(asize,3);
double gflops = 1.0e-9 * (double)NumOps*(double)MAX_ITER / time_seconds;
printf("Computed time (sec.)= %lf.\n", time_seconds);
printf("Computed GFLOPS (sec.)= %lf\n", gflops);
#endif
//   /* Read the kernel's output */
   err = clEnqueueReadBuffer(queue, sum_buffer, CL_TRUE, 0, 
         sizeof(sum), sum, 0, NULL, NULL);
   if(err < 0) {
      perror("Couldn't read the buffer");
      exit(1);
   }
//
/* Check result */
   total = 0.0f;
   for(i=0; i<ARRAY_SIZE; i++)
       for(j=0; j<ARRAY_SIZE; j++) {
      total+=fabs(sum[i*ARRAY_SIZE+j]-(float)ARRAY_SIZE);
   }
   if(total > 0.001)
      printf("Check failed.\n");
   else
      printf("Check passed.\n");

   /* Deallocate resources */
   clReleaseKernel(kernel);
   clReleaseMemObject(sum_buffer);
   clReleaseMemObject(input_buffer);
   clReleaseCommandQueue(queue);
   clReleaseProgram(program);
   clReleaseContext(context);
   return 0;
}
