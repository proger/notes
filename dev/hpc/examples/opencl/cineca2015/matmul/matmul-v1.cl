__kernel void mat_mul(
 const int Mdim, const int Ndim, const int Pdim,
 __global float *A, __global float *B, __global float *C)
{
    int i, j, k;
    j = get_global_id(0);
    i = get_global_id(1);
    // C(i, j) = sum(over k) A(i,k) * B(k,j)
    for (k = 0; k < Pdim; k++) { 
      C[i*Ndim+j] += A[i*Ndim+k] * B[k*Pdim+j];
                               }
}
