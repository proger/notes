/* vi: se ft=c : */
/* A is PxN, B is MxP, C is MxN N is row number and M is column number for C */
__kernel void mat_mul(
 const int Mdim, const int Ndim, const int Pdim,
 __global float *A, __global float *B, __global float *C)
{
#define NB 16
#define get_offset(i, j, N)  ((i) * NB * (N) + (j) * NB)

   __local  float Asub[NB][NB];
   __local  float Bsub[NB][NB];

   // Block row and column 
   int ib = get_group_id(1);
   int jb = get_group_id(0);

   // Thread row and column within Csub 
   int it = get_local_id(1);
   int jt = get_local_id(0);

	__private float s = 0;

    /* C(i, j) = sum(over k) A(i,k) * B(k,j) */
   for (int kb = 0; kb < (Pdim / NB); ++kb) {

      // Get the starting address of Asub  and Bsub
      int a_offset = get_offset (ib, kb, Ndim);
      int b_offset = get_offset (kb, jb, Ndim);

      // Load Asub and Bsub from device memory to shared memory 
      // Each thread loads one element of each sub-matrix 
      Asub[it][jt] = A[a_offset + it*Ndim + jt];
      Bsub[it][jt] = B[b_offset + it*Ndim + jt];

	barrier(CLK_LOCAL_MEM_FENCE);

      // Multiply Asub and Bsub together 
      for (int k = 0; k < NB; ++k) {
         s += Asub[it][k] * Bsub[k][jt];
      }

	barrier(CLK_LOCAL_MEM_FENCE);
}

   // Get the starting address (c_offset) of Csub 
   int c_offset = get_offset (ib, jb, Ndim);  
  // Each thread block computes one sub-matrix Csub of C 
   C[c_offset + it*Mdim + jt] = s;

/*    for (k = 0; k < Pdim; k++)
      s += A[i*Ndim+k] * B[k*Mdim+j];

	C[i*Mdim+j] = s;*/
}
