/* vi: se ft=c : */
/* A is PxN, B is MxP, C is MxN N is row number and M is column number for C */
__kernel void mat_mul(
 const int Mdim, const int Ndim, const int Pdim,
 __global float *A, __global float *B, __global float *C)
{
    int i, j, k;
    j = get_global_id(0); // column index
    i = get_global_id(1); // row index
	if (i < Ndim && j < Mdim) {
    /* C(i, j) = sum(over k) A(i,k) * B(k,j) */
	__private float s = 0;
    for (k = 0; k < Pdim; k++)
      s += A[i*Ndim+k] * B[k*Mdim+j];

	C[i*Mdim+j] = s;
}
}
