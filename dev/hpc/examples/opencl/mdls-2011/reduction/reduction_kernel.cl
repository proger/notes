/* vi: se ft=C: */

/* Kernel for multipass reduction. */
__kernel void reduceM(__global int* buffer,
		      __const int length,
		      __global int* result,
		      __local int* scratch
		      ) 
{
	// get indices
	int global_idx = get_global_id(0);
	int local_idx  = get_local_id(0);

	// copy global buffer in shared memory
	scratch[local_idx] = global_idx < length ? buffer[global_idx] : 0;

	// synchronize threads
	barrier(CLK_LOCAL_MEM_FENCE);

	// compute reduction
	if (global_idx < length)
		scratch[global_idx] += scratch[global_idx + (length >> 1)];
}

/* Kernel for two pass reduction. */
__kernel void reduceT(__global int* buffer,
		      __const int length,
		      __global int* result,
		      __local int* scratch
		      ) 
{
  // TODO: write the kernel
}

