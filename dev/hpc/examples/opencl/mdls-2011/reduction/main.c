//
// (C) Guillaume.Colin-de-Verdiere at CEA.Fr
//
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <stdlib.h>
#ifndef __APPLE_CC__
#include <malloc.h>
#endif
#include <assert.h>
#include <limits.h>
#include <sys/time.h>
#include <float.h>
#include <math.h>

#include "ocltools.h"
#include "oclerror.h"

#define SWAP(a, b, t) do { (t) = (a); (a) = (b); (b) = (t); } while (0)

int main() {
  //
  int i;
  int verbose = 1;
  int theplatform = 0;
  int profiling = 1;
  int nbplat = 0; 
  int nbgpu = 0;
  int thegpu;
  cl_command_queue cq;
  cl_program pgm;
  cl_context ctx;
  int N=256*32;
  int M=2;
  int arr[N];
  int *p;
  cl_kernel ker;
  int wrksiz, wrkgrp;
  cl_mem arrdev, tmpdev1, tmpdev2, tmpdev;
  cl_int err;
  size_t gws[3], lws[3];
  int niter1;
  int niter2;
  int result = -1;
  cl_event event;
  double elapsk;
  

  // initial value
  for (i = 0; i < N; i++) arr[i] = i+1;

  nbplat = oclGetNbPlatforms(verbose);
  if (nbplat > 1) {
    printf("Adapt the current program for this situation\n");
    exit(1);
  }
  ctx = oclCreateCtxForPlatform(theplatform, verbose);
  nbgpu =  oclGetNbOfGpu(theplatform);
  if (nbgpu < 1) {
    printf("This program is tuned for a GPU\n");
    exit(1);
  }
  thegpu = oclGetGpuDev(theplatform, 0);
  cq = oclCreateCommandQueueForDev(theplatform, thegpu, ctx, profiling);
  pgm = oclCreatePgmFromCtx("reduction_kernel.cl", ".", ctx, theplatform, thegpu, verbose);
  CREATEKER(pgm, ker, reduceM);

  arrdev = clCreateBuffer(ctx, CL_MEM_COPY_HOST_PTR + CL_MEM_READ_WRITE, sizeof(cl_int) * N, arr, &err);
  oclCheckErr(err, "clCreateBuffer");

  // - - - - - Case of MULTIPASS reduction - - - - - 
  // first iteration on the original array
  
  // Evaluate the number of intermediate results
  wrksiz = (wrksiz + wrkgrp - 1) / wrkgrp;
  while (1/* TODO */) {
    // do reductions on partial results
  }

  result=-1;
  // get result
  printf("Result   =%d in %lgs\n", result, elapsk);
  printf("reference=%d\n", (int) (N*(N+1)/2));
  err = clReleaseMemObject(tmpdev1); oclCheckErr(err, "clReleaseMemObject");
  err = clReleaseMemObject(tmpdev2); oclCheckErr(err, "clReleaseMemObject");

  // - - - - - Case of TWO PASS reduction - - - - - 
  // create a buffer large enough to hold the temporary results 
  elapsk = 0;
  wrksiz = N;
  /* TODO */
  result=-1;
  // get result
  printf("Result   =%d in %lgs\n", result, elapsk);
  printf("reference=%d\n", (int) (N*(N+1)/2));
  err = clReleaseMemObject(tmpdev1); oclCheckErr(err, "clReleaseMemObject");
  err = clReleaseMemObject(arrdev); oclCheckErr(err, "clReleaseMemObject");
  return 0;
}

//EOF
