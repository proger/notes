#ifdef __APPLE_CC__
#include <sys/time.h>
#else
#include <time.h>
#endif

#include "cclock.h"

static int start = 0;
static struct timespec res;
static struct timespec initVal;
static double initValD, curValD;
static struct timespec curVal;
static struct timeval tp;

double cclock(void)
{
  int rc = 0;
  if (!start) {
    start = 1;
#ifdef CLOCK_REALTIME
    rc = clock_getres(CLOCK_REALTIME, &res);
    rc = clock_gettime(CLOCK_REALTIME, &initVal);
    initValD = (double) initVal.tv_sec + 1.e-9 * (double) initVal.tv_nsec;
#else
	gettimeofday(&tp, NULL);
	initValD = (double) tp.tv_sec + (double)tp.tv_usec / 1000 / 1000;
#endif
  }
#ifdef CLOCK_REALTIME
  rc = clock_gettime(CLOCK_REALTIME, &curVal);
  curValD = (double) curVal.tv_sec + 1.e-9 * (double) curVal.tv_nsec;
#else
	gettimeofday(&tp, NULL);
	curValD = (double) tp.tv_sec + (double)tp.tv_usec / 1000 / 1000;
#endif
  return curValD - initValD;
}
