//
// (C) Guillaume.Colin-de-Verdiere at CEA.Fr
//
#ifndef OCLERROR_H
#define OCLERROR_H

#ifdef __APPLE_CC__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define oclCheckErr(c, m) oclCheckErrF((c), (m), __FILE__, __LINE__)

#ifdef __cplusplus
extern "C" {
#endif
void oclPrintErr(cl_int rc, const char *msg, const char *file, const int line);
void oclCheckErrF(cl_int rc, const char *msg, const char *file, const int line);
//
#ifdef __cplusplus
};
#endif
//
#endif
//EOF
