/*
 * (C) CEA/DAM Guillaume.Colin-de-Verdiere at Cea.Fr
 */

#ifndef OCLTOOLS_H
#define OCLTOOLS_H

#ifdef __APPLE_CC__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include "oclerror.h"

#ifndef MIN
#define MIN(a, b) (((a) <= (b))? (a): (b))
#endif

typedef enum { NDR_1D = 1, NDR_2D = 2, NDR_3D = 3 } MkNDrange_t;
typedef size_t dim3[3];

#define OCLINITARG cl_uint narg = 0
#define OCLRESETARG narg = 0
#define OCLSETARG(k, a) do { oclSetArg((k), narg, sizeof((a)), &(a)); narg++; } while(0);

#define OCLSETARG01(k, a) { OCLINITARG ; OCLSETARG((k), (a)) ; }
#define OCLSETARG02(k, a, b) { OCLINITARG ; OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ; }
#define OCLSETARG03(k, a, b, c) { OCLINITARG ; OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ;  OCLSETARG((k), (c)) ; }
#define OCLSETARG04(k, a, b, c, d) { OCLINITARG ; OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ;  OCLSETARG((k), (c)) ; OCLSETARG((k), (d)) ; }
#define OCLSETARG05(k, a, b, c, d, e) { OCLINITARG ;			\
    OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ; OCLSETARG((k), (c)) ; OCLSETARG((k), (d)) ; \
    OCLSETARG((k), (e)) ; }
#define OCLSETARG06(k, a, b, c, d, e, f) { OCLINITARG ;			\
    OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ; OCLSETARG((k), (c)) ; OCLSETARG((k), (d)) ; \
    OCLSETARG((k), (e)) ; OCLSETARG((k), (f)) ; }
#define OCLSETARG07(k, a, b, c, d, e, f, g) { OCLINITARG ;		\
    OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ; OCLSETARG((k), (c)) ; OCLSETARG((k), (d)) ; \
    OCLSETARG((k), (e)) ; OCLSETARG((k), (f)) ; OCLSETARG((k), (g)) ; }
#define OCLSETARG08(k, a, b, c, d, e, f, g, h) { OCLINITARG ;		\
    OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ; OCLSETARG((k), (c)) ; OCLSETARG((k), (d)) ; \
    OCLSETARG((k), (e)) ; OCLSETARG((k), (f)) ; OCLSETARG((k), (g)) ; OCLSETARG((k), (h)) ; }
#define OCLSETARG09(k, a, b, c, d, e, f, g, h, i) { OCLINITARG ;	\
    OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ; OCLSETARG((k), (c)) ; OCLSETARG((k), (d)) ; \
    OCLSETARG((k), (e)) ; OCLSETARG((k), (f)) ; OCLSETARG((k), (g)) ; OCLSETARG((k), (h)) ; \
    OCLSETARG((k), (i)) ; }
#define OCLSETARG10(k, a, b, c, d, e, f, g, h, i, j) { OCLINITARG ;	\
    OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ; OCLSETARG((k), (c)) ; OCLSETARG((k), (d)) ; \
    OCLSETARG((k), (e)) ; OCLSETARG((k), (f)) ; OCLSETARG((k), (g)) ; OCLSETARG((k), (h)) ; \
    OCLSETARG((k), (i)) ; OCLSETARG((k), (j)) ; }
#define OCLSETARG11(k, a, b, c, d, e, f, g, h, i, j, l) { OCLINITARG ;	\
    OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ; OCLSETARG((k), (c)) ; OCLSETARG((k), (d)) ; \
    OCLSETARG((k), (e)) ; OCLSETARG((k), (f)) ; OCLSETARG((k), (g)) ; OCLSETARG((k), (h)) ; \
    OCLSETARG((k), (i)) ; OCLSETARG((k), (j)) ; OCLSETARG((k), (l)) ; }
#define OCLSETARG12(k, a, b, c, d, e, f, g, h, i, j, l, m) { OCLINITARG ; \
    OCLSETARG((k), (a)) ; OCLSETARG((k), (b)) ; OCLSETARG((k), (c)) ; OCLSETARG((k), (d)) ; \
    OCLSETARG((k), (e)) ; OCLSETARG((k), (f)) ; OCLSETARG((k), (g)) ; OCLSETARG((k), (h)) ; \
    OCLSETARG((k), (i)) ; OCLSETARG((k), (j)) ; OCLSETARG((k), (l)) ; OCLSETARG((k), (m)) ; }

#define CREATEKER(pgm, k, a) do {cl_int err = 0; (k) = clCreateKernel((pgm), #a, &err); oclCheckErr(err, #a); } while (0)

#ifdef __cplusplus
extern "C" {
#endif
  int oclMultiple(int N, int n);
  void oclMkNDrange(const size_t nb, const size_t nbthreads, const MkNDrange_t form, size_t gws[3], size_t lws[3]);
  double oclChronoElaps(const cl_event event);
  cl_uint oclCreateProgramString(const char *fname, char ***pgmt, cl_uint * pgml);
  cl_program oclCreatePgmFromCtx(const char *srcFile, const char *srcDir,
                                 const cl_context ctx, const int theplatform, const int thedev, const int verbose);

  int oclGetNbPlatforms(const int verbose);
  cl_context oclCreateCtxForPlatform(const int theplatform, const int verbose);
  cl_command_queue oclCreateCommandQueueForDev(const int theplatform,
                                               const int devselected, const cl_context ctx, const int profiling);
  int oclGetNumberOfDev(const int theplatform);
  int oclGetNbOfGpu(const int theplatform);
  int oclGetNbOfCpu(const int theplatform);
  int oclGetGpuDev(const int theplatform, const int gpunum);
  int oclGetCpuDev(const int theplatform, const int cpunum);
  char *oclGetDevNature(const int theplatform, const int thedev);
  cl_device_id oclGetDeviceOfCQueue(cl_command_queue q);
  cl_context oclGetContextOfCQueue(cl_command_queue q);
  size_t oclGetMaxWorkSize(cl_kernel k, cl_device_id d);
  size_t oclGetMaxMemAllocSize(int theplatform, int thedev);
  int oclFp64Avail(int theplatform, int thedev);
  void oclSetArg(cl_kernel k, cl_uint narg, size_t l, const void *arg);
  void oclSetArgLocal(cl_kernel k, cl_uint narg, size_t l);
  double oclLaunchKernel(cl_kernel k, cl_command_queue q, size_t nbobj, int nbthread);
  void oclNbBlocks(cl_kernel k, cl_command_queue q, size_t nbobj, int nbthread, long *maxth, long *nbblocks);
#ifdef __cplusplus
};
#endif

#endif
