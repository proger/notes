/*
 * (C) CEA/DAM Guillaume.Colin-de-Verdiere at Cea.Fr
 */
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
/*#include <malloc.h>*/
#include <assert.h>

// MacOS-X
#include <OpenCL/opencl.h>

/*
    The purpose of this program is to demonstrate how to use OpenCL.
    It is neither robust nor elegant.
*/

#define CHKERR(err) do {if ((err) != CL_SUCCESS) { printf("Error=%d (%s, %d)\n", (err), __FILE__, __LINE__); exit(1); }} while (0)

const char *pgm = "\n\
__kernel void tst(__global float *q, \n\
                  const size_t n)       \n\
{                                    \n\
   int i = get_global_id(0);         \n\
   if (i >= n) return;               \n\
   q[i] += 1.0f;                     \n\
}                                    \n";

#define Ko (1024.0)
#define Mo (1024.0 * 1024.0)
#define Go (1024.0 * 1024.0 * 1024.0)
#define S2NS (1000 * 1000 * 1000)	/* Seconds to Nanoseconds conversion factor */

/* Compute rate of transfert.
   start and end are in ns.
   nbelem is in bytes.
   returned value is in GB/s.
 */
double throughput(cl_ulong start, cl_ulong end, size_t nbelem) {
	double x = 0.0;

    if (end > start)
	    x = (double)nbelem * 1.0e9 / Go / (double)(end - start);

        return x;
}

cl_context gpuCtx;
cl_command_queue cqCommandQue;
cl_program cpProgram;
cl_kernel ckKernel;
size_t szgws, szlws;

// Decreased number of elements in order to make this program run on MacBook Air.
//size_t nbelem = 200 * Mo;
size_t nbelem = 200 * Ko;

int
main(int argc, char **argv)
{
    // OpenCL 
    cl_context_properties prop[3];
    cl_device_id *pDev, devId[20][20];
    cl_platform_id platId[20];
    cl_mem qDev;
    cl_int err;
	cl_uint nbplat, nbdevices;
    cl_event event;
    // 
    struct timeval tp;
    size_t szparm;
    size_t szK;
    float *q, *q2, *b;
    int i, j;
    int platidx;
    size_t msgl;
    cl_ulong start, end;
    double elaps;
    char *message;
    cl_int refcnt;
    size_t refcntsz;
	cl_device_type device_type;

    printf("%s\n", pgm);
    printf("%ld\n", strlen(pgm));
    szK = strlen(pgm);
     
    q = (float *) calloc(nbelem, sizeof(float));
    q2 = (float *) calloc(nbelem, sizeof(float));
     
    nbplat = 0;
    err = clGetPlatformIDs(0, NULL, &nbplat);
    CHKERR(err);
    err = clGetPlatformIDs(nbplat, platId, &nbplat/*can be set to NULL*/);
    CHKERR(err);
	printf("Nb plateforms = %u\n", nbplat);

    // TODO: get platform characteristics
	for (i = 0 ; i < nbplat ; ++i) {
		// NAME
		err = clGetPlatformInfo(platId[i], CL_PLATFORM_NAME, 0, NULL, &msgl);
		CHKERR(err);
		message = (char *) calloc(msgl, 1);
		err = clGetPlatformInfo(platId[i], CL_PLATFORM_NAME, msgl, (void*)message, NULL);
		CHKERR(err);
		printf("PLATEFORM NAME : %s\n", message);
		free(message);

		// EXTENSIONS
		err = clGetPlatformInfo(platId[i], CL_PLATFORM_EXTENSIONS, 0, NULL, &msgl);
		CHKERR(err);
		message = (char *) calloc(msgl, 1);
		err = clGetPlatformInfo(platId[i], CL_PLATFORM_EXTENSIONS, msgl, (void*)message, NULL);
		CHKERR(err);
		printf("PLATEFORM EXTENSIONS : %s\n", message);
		free(message);

		// TODO: get device characteristics
		err = clGetDeviceIDs(platId[i], CL_DEVICE_TYPE_ALL, 0, NULL, &nbdevices);
		CHKERR(err);
		err = clGetDeviceIDs(platId[i], CL_DEVICE_TYPE_ALL, nbdevices, devId[i], NULL);
		CHKERR(err);
		for (j = 0 ; j < nbdevices ; ++j) {
			// NAME
			err = clGetDeviceInfo(devId[i][j], CL_DEVICE_NAME, 0, NULL, &msgl);
			CHKERR(err);
			message = (char *) calloc(msgl, 1);
			err = clGetDeviceInfo(devId[i][j], CL_DEVICE_NAME, msgl, message, NULL);
			CHKERR(err);
			printf("DEVICE NAME : %s\n", message);
			free(message);
 
			// EXTENSIONS
			err = clGetDeviceInfo(devId[i][j], CL_DEVICE_EXTENSIONS, 0, NULL, &msgl);
			CHKERR(err);
			message = (char *) calloc(msgl, 1);
			err = clGetDeviceInfo(devId[i][j], CL_DEVICE_EXTENSIONS, msgl, message, NULL);
			CHKERR(err);
			printf("DEVICE EXTENSIONS : %s\n", message);
			free(message);
		}
	}

	// Select a (plateform, device) couple which is a GPU
	platidx = 0;
	for (i = 0 ; i < nbplat ; ++i)
		for (j = 0 ; j < nbdevices ; ++j) {
			// GET DEVICE TYPE
			err = clGetDeviceInfo(devId[i][j], CL_DEVICE_TYPE, sizeof(device_type), &device_type, NULL);
			CHKERR(err);
			if (device_type == CL_DEVICE_TYPE_GPU) {
				platidx = i;
				i = nbplat;
				break;
			}
		}

    // Here we assume the platform 0 has a GPU (not always true ; it could be platform 1)
    prop[0] = (cl_context_properties) CL_CONTEXT_PLATFORM;
    prop[1] = (cl_context_properties) platId[platidx];
    prop[2] = (cl_context_properties) NULL;
    // use a shortcut to create a GPU context. Beware !
    gpuCtx = clCreateContextFromType(prop, CL_DEVICE_TYPE_ALL, NULL, NULL, &err);
    CHKERR(err);

    // Get the list of GPU devices associated with context
    err = clGetContextInfo(gpuCtx, CL_CONTEXT_DEVICES, 0, NULL, &szparm);
    CHKERR(err);
    pDev = (cl_device_id *) malloc(szparm);
    err = clGetContextInfo(gpuCtx, CL_CONTEXT_DEVICES, szparm, pDev, NULL);
    CHKERR(err);

    // Create a command-queue for the first GPU
    cqCommandQue = clCreateCommandQueue(gpuCtx, *pDev, CL_QUEUE_PROFILING_ENABLE, &err);
    CHKERR(err);

    // TODO: Create & build the program
    // Create the program
    cpProgram = clCreateProgramWithSource(gpuCtx, 1, &pgm, &szK, &err);
    CHKERR(err);
    // Build the program
    err = clBuildProgram(cpProgram, 1, pDev, "", NULL, NULL);
    CHKERR(err);
    // Create the kernel
    ckKernel = clCreateKernel(cpProgram, "tst", &err);
    CHKERR(err);

    // TODO: Create and fill the buffer
    // create
	qDev = clCreateBuffer(gpuCtx, 0, sizeof(cl_float) * nbelem, NULL, &err);
    CHKERR(err);
    // fill
    err = clEnqueueWriteBuffer(cqCommandQue, qDev, CL_TRUE, 0, sizeof(cl_float) * nbelem, q, 0, NULL, &event);
    CHKERR(err);
    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);
    CHKERR(err);
    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
    CHKERR(err);
    elaps = (double)1.0e-9 * (end - start);
    err = clReleaseEvent(event);
    CHKERR(err);
    printf("WritBuf %lf (%lfGB/s)\n", elaps,
            throughput(start, end, sizeof(cl_float) * nbelem));

    // Create a 1D NDRange
    szlws = 32;
    szgws = (nbelem + szlws - 1) / szlws;
    szgws *= szlws;

    // Set the Argument values
    err = clSetKernelArg(ckKernel, 0, sizeof(cl_mem), &qDev);
    CHKERR(err);
    err = clSetKernelArg(ckKernel, 1, sizeof(size_t), &nbelem);
    CHKERR(err);

    // Launch kernel
    err = clEnqueueNDRangeKernel(cqCommandQue, ckKernel, 1, NULL, &szgws, &szlws, 0, NULL, &event);
    CHKERR(err);
    // Wait for the kernel to terminate
    err = clWaitForEvents(1, &event);
    CHKERR(err);
    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);
    CHKERR(err);
    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
    CHKERR(err);
    elaps = (double)1.0e-9 * (end - start);
    // cleanup for this run
    err = clReleaseEvent(event);
    CHKERR(err);
    printf("Enqueue %lf\n", elaps);

    // Method 1: retreive a copy of the buffer
    // Blocking read of results from GPU to Host
    err = clEnqueueReadBuffer(cqCommandQue, qDev, CL_TRUE, 0, sizeof(cl_float) * nbelem, q, 0, NULL, &event);
    CHKERR(err);
    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);
    CHKERR(err);
    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
    CHKERR(err);
    elaps = (double)1.0e-9 * (end - start);
    // cleanup for this run
    err = clReleaseEvent(event);
    CHKERR(err);
    printf("ReadBuf %lf (%lfGB/s)\n", elaps,
           throughput(start, end, sizeof(cl_float) * nbelem));

    // compare PCI-Express performances to memory copies
    start = clock() * S2NS / CLOCKS_PER_SEC;
    memcpy(q2, q, sizeof(cl_float) * nbelem);
    end = clock() * S2NS / CLOCKS_PER_SEC;
    printf("memcpy  %lf (%lfGB/s)\n", (double)(end - start) / S2NS,
            throughput(start, end, sizeof(cl_float) * nbelem));

    // verify the results
    for (i = 0; i < nbelem; i++) {
        assert((float) 1.0 == q[i]);
    }

    // Method 2: map the buffer for reading
    start = end = 0;
/*    gettimeofday(&tp);
    start = tp.tv_usec * 1000;
*/    b = (float *) clEnqueueMapBuffer(cqCommandQue, qDev, CL_TRUE, CL_MAP_READ, 0, sizeof(cl_float) * nbelem, 0, NULL, &event, &err);
    CHKERR(err);
 /*   gettimeofday(&tp);
    end = tp.tv_usec * 1000;
*/
  /*    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);
    CHKERR(err);
    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
    CHKERR(err);
    elaps = (double)1.0e-9 * (end - start);
*/
    // cleanup for this run
    err = clReleaseEvent(event);
    CHKERR(err);
    printf("MapBuf  %lf (%lfGB/s)\n", (double)(end - start) / 1000 / 1000,
           throughput(start, end, sizeof(cl_float) * nbelem));

    // verify the results
    for (i = 0; i < nbelem; i++) {
        assert((float) 1.0 == b[i]);
    }

    // see object behaviour REFERENCE_COUNT
    err = clGetMemObjectInfo(qDev, CL_MEM_REFERENCE_COUNT, sizeof(refcnt), &refcnt, NULL);
    CHKERR(err);
    printf("ref count %d\n", refcnt);

    // cleanup GPU memory
    err = clReleaseMemObject(qDev);
    CHKERR(err);

    printf("Pgm ended normally\n");
    return 0;
}
