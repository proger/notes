######################################################
# cavity2d.m: 2D cavity flow, simulated by a LB method            
######################################################
# Lattice Boltzmann sample, Matlab script
# Copyright (C) 2006-2008 Jonas Latt
# Address: Rue General Dufour 24,  1211 Geneva 4, Switzerland 
# E-mail: Jonas.Latt@cui.unige.ch
#
# Implementation of 2d cavity geometry and Zou/He boundary
# condition by Adriano Sciacovelli
######################################################
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public 
# License along with this program; if not, write to the Free 
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA  02110-1301, USA.
######################################################

# adapted to python by P. Kestener
# with the help of http://www.mail-archive.com/numpy-discussion@scipy.org/msg24429.html
# date May 13 2011

import numpy
import matplotlib
matplotlib.use('GtkAgg')
import matplotlib.pyplot as plt
plt.ion() # enables interactive mode

# GENERAL FLOW CONSTANTS 
lx = 256
ly = 256

uLid  = 0.05 # horizontal lid velocity 
vLid  = 0.0    # vertical lid velocity 
Re    = 100.0  # Reynolds number 
nu    = uLid *lx / Re     # kinematic viscosity 
omega = 1. / (3*nu+1./2.) # relaxation parameter 
maxT  = 200 # 40000 # total number of iterations 
tPlot = 10    # cycles for graphical output

# D2Q9 LATTICE CONSTANTS 
t   = numpy.array([4./9, 1./9, 1./9, 1./9, 1./9, 1./36, 1./36, 1./36, 1./36])
cx  = numpy.array([ 0, 1, 0, -1,  0, 1, -1, -1,  1])
cy  = numpy.array([ 0, 0, 1,  0, -1, 1,  1, -1, -1]) 
opp = numpy.array([ 0, 3, 4,  1,  2, 7,  8,  5,  6])
lid = numpy.arange(1, lx-1)

[y,x] = numpy.meshgrid(numpy.arange(ly),numpy.arange(lx))
obst = numpy.ones( (lx,ly) )
obst[lid,1:ly] = 0 
bbRegion = numpy.nonzero(obst)

# INITIAL CONDITION: (rho=0, u=0) ==> fIn(i) = t(i) 
fIn = t[:, numpy.newaxis, numpy.newaxis].\
		repeat(lx, axis = 1).\
		repeat(ly, axis = 2)

# MAIN LOOP (TIME CYCLES) 
for cycle in range(maxT):

    # MACROSCOPIC VARIABLES 
    rho = fIn.sum(axis=0)
    ux = (cx[:,numpy.newaxis,numpy.newaxis] * fIn).sum(axis = 0) / rho
    uy = (cy[:,numpy.newaxis,numpy.newaxis] * fIn).sum(axis = 0) / rho

    # MACROSCOPIC (DIRICHLET) BOUNDARY CONDITIONS 
    ux[lid,ly-1] = uLid #lid x - velocity 
    uy[lid,ly-1] = vLid #lid y - velocity 
    rho[lid,ly-1] = 1 / (1+uy[lid,ly-1]) * \
                    ( fIn[[0,1,3]][:,lid][:,:,ly-1].sum(axis=0) + \
                      2*fIn[[2,5,6]][:,lid][:,:,ly-1].sum(axis=0) )

    # MICROSCOPIC BOUNDARY CONDITIONS: LID (Zou/He BC)
    fIn[4,lid,ly-1] = fIn[2,lid,ly-1] - 2.0/3*rho[lid,ly-1] * uy[lid,ly-1] 
    fIn[8,lid,ly-1] = fIn[6,lid,ly-1] + \
                      1.0/2 * (fIn[3,lid,ly-1] - fIn[1,lid,ly-1]) + \
                      1.0/2 * rho[lid,ly-1] * ux[lid,ly-1] - \
                      1.0/6 * rho[lid,ly-1] * uy[lid,ly-1]
    fIn[7,lid,ly-1] = fIn[5,lid,ly-1] + \
                      1.0/2 * (fIn[1,lid,ly-1] - fIn[3,lid,ly-1]) - \
                      1.0/2 * rho[lid,ly-1] * ux[lid,ly-1] - \
                      1.0/6 * rho[lid,ly-1] * uy[lid,ly-1]

    # COLLISION STEP 
    fEq  = numpy.zeros( (9, lx, ly) )
    fOut = numpy.zeros( (9, lx, ly) )
    for i in xrange(0, 9): 
        cu = 3 * (cx[i]*ux + cy[i]*uy ) 
        fEq[i] = rho * t[i] * ( 1.0 + cu + 0.5 * cu**2 - \
                                    1.5 * (ux**2 + uy**2) ) 
        fOut[i] = fIn[i] - omega * ( fIn[i] - fEq[i] )

    # MICROSCOPIC BOUNDARY CONDITIONS: NO-SLIP WALLS (bounce-back)
    for i in xrange(0, 9):
        fOut[i,bbRegion[0],bbRegion[1]] = fIn[opp[i],bbRegion[0],bbRegion[1]]

    # STREAMING STEP 
    for i in xrange(0, 9):
        fIn[i] = numpy.roll( \
            numpy.roll(fOut[i], cx[i], axis = 0), \
                 cy[i], axis = 1)
        #print 'fIn['+str(i)+']'
        #print fIn[i]

     
    # VISUALIZATION
    if not cycle%tPlot:
        u = numpy.sqrt(ux**2+uy**2)
        u[bbRegion[0],bbRegion[1]] = numpy.nan
        print u.min(), u.max()
        plt.imshow(u)
        plt.draw()
        #plt.show()
        #imagesc(u(:,ly:-1:1)'./uLid);
        #colorbar
        #axis equal off; drawnow
        #plt.show()
