######################################################
# shanChen.m: Multi-component fluid, using a LB method,
#   based on the Shan-Chen model
# [X.Shan and H.Chen, http://dx.doi.org/10.1103/PhysRevE.47.1815].
#
######################################################
# Lattice Boltzmann sample, written in Matlab
# Copyright (C) 2008 Orestis Malaspinas, Andrea Parmigiani, Jonas Latt
# Address: EPFL-STI-LIN Station 9
# E-mail: orestis.malaspinas@epfl.ch
######################################################
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA  02110-1301, USA.
######################################################

# adapted to python by P. Kestener
# with the help of http://www.mail-archive.com/numpy-discussion@scipy.org/msg24429.html
# date May 13 2011

import numpy
import matplotlib
matplotlib.use('GtkAgg')
import matplotlib.pyplot as plt
plt.ion() # enables interactive mode

# GENERAL FLOW CONSTANTS
ly = 101
lx = 101

G = -1.2  # Amplitude of the molecular interaction force

omega1 = 1.  # Relaxation parameter for fluid 1
omega2 = 1.  # Relaxation parameter for fluid 2

maxT   = 1200    # total number of iterations
tPlot  = 20       # iterations between successive graphical outputs

# D2Q9 LATTICE CONSTANTS
tNS   = numpy.array([4./9, 1./9, 1./9, 1./9, 1./9, 1./36, 1./36, 1./36, 1./36])
cxNS  = numpy.array([   0,    1,    0,   -1,    0,     1,    -1,    -1,     1])
cyNS  = numpy.array([   0,    0,    1,    0,   -1,     1,     1,    -1,    -1])
oppNS = numpy.array([   0,    3,    4,    1,    2,     7,     8,     5,     6])

[y,x] = numpy.meshgrid(numpy.arange(ly),numpy.arange(lx))

drho = 0.001
delta_rho = -drho*(1-2.0*numpy.random.rand(lx,ly))

# INITIAL CONDITION FOR BOTH DISTRIBUTION FUNCTIONS: (T=0) ==> TIn(i) = t(i)
fIn = numpy.zeros( (9,lx,ly) )
gIn = numpy.zeros( (9,lx,ly) )
for i in xrange(0,9):
    fIn[i] = tNS[i] * (1.0 + delta_rho)
    gIn[i] = tNS[i] * (1.0 - delta_rho)

rho1  = fIn.sum(axis=0)
#imagesc(rho1');
#colorbar
#title('Fluid 1 density');
#axis equal off; drawnow

# MAIN LOOP (TIME CYCLES)
Gomega1 = G/omega1
Gomega2 = G/omega2
for cycle in range(maxT):

    # MACROSCOPIC VARIABLES
    rho1 = fIn.sum(axis=0)
    rho2 = gIn.sum(axis=0)
    jx1  = (cxNS[:,numpy.newaxis,numpy.newaxis] * fIn).sum(axis = 0)
    jy1  = (cyNS[:,numpy.newaxis,numpy.newaxis] * fIn).sum(axis = 0)
    jx2  = (cxNS[:,numpy.newaxis,numpy.newaxis] * gIn).sum(axis = 0)
    jy2  = (cyNS[:,numpy.newaxis,numpy.newaxis] * gIn).sum(axis = 0)
   
    rhoTot_OMEGA = rho1*omega1 + rho2*omega2
    uTotX = (jx1*omega1+jx2*omega2) / rhoTot_OMEGA
    uTotY = (jy1*omega1+jy2*omega2) / rhoTot_OMEGA
	
    rhoContrib1x = numpy.zeros( (lx,ly) )
    rhoContrib2x = numpy.zeros( (lx,ly) )
    
    rhoContrib1y = numpy.zeros( (lx,ly) )
    rhoContrib2y = numpy.zeros( (lx,ly) )

    for i in range(1,9):
        rhoContrib1x = rhoContrib1x + cxNS[i] * numpy.roll( \
            numpy.roll(rho1*tNS[i], cxNS[i], axis = 0), cyNS[i], axis = 1)

        rhoContrib1y = rhoContrib1y + cyNS[i] * numpy.roll( \
            numpy.roll(rho1*tNS[i], cxNS[i], axis = 0), cyNS[i], axis = 1)
        
        rhoContrib2x = rhoContrib2x + cxNS[i] * numpy.roll( \
            numpy.roll(rho2*tNS[i], cxNS[i], axis = 0), cyNS[i], axis = 1)

        rhoContrib2y = rhoContrib2y + cyNS[i] * numpy.roll( \
            numpy.roll(rho2*tNS[i], cxNS[i], axis = 0), cyNS[i], axis = 1)
        
        # rhoContrib1x = rhoContrib1x + circshift(rho1*tNS(i), [0,cxNS(i),cyNS(i)])*cxNS(i);
        # rhoContrib1y = rhoContrib1y + circshift(rho1*tNS(i), [0,cxNS(i),cyNS(i)])*cyNS(i);
        
        # rhoContrib2x = rhoContrib2x + circshift(rho2*tNS(i), [0,cxNS(i),cyNS(i)])*cxNS(i);
        # rhoContrib2y = rhoContrib2y + circshift(rho2*tNS(i), [0,cxNS(i),cyNS(i)])*cyNS(i);
    
    uTotX1 = uTotX - Gomega1 * rhoContrib2x #POTENTIAL CONTRIBUTION OF FLUID 2 ON 1
    uTotY1 = uTotY - Gomega1 * rhoContrib2y
    
    uTotX2 = uTotX - Gomega2 * rhoContrib1x #POTENTIAL CONTRIBUTION OF FLUID 2 ON 1
    uTotY2 = uTotY - Gomega2 * rhoContrib1y

    # COLLISION STEP FLUID 1 AND 2
    fEq  = numpy.zeros( (9, lx, ly) )
    fOut = numpy.zeros( (9, lx, ly) )
    gEq  = numpy.zeros( (9, lx, ly) )
    gOut = numpy.zeros( (9, lx, ly) )
    for i in range(0,9):
        cuNS1    = 3*(cxNS[i] * uTotX1 + cyNS[i] * uTotY1)
        cuNS2    = 3*(cxNS[i] * uTotX2 + cyNS[i] * uTotY2)
        
        fEq[i]   = rho1 * tNS[i] * \
            ( 1 + cuNS1 + 0.5*cuNS1**2 - 1.5 * (uTotX1**2 + uTotY1**2) )

        gEq[i]   = rho2 * tNS[i] * \
            ( 1 + cuNS2 + 0.5*cuNS2**2 - 1.5 * (uTotX2**2 + uTotY2**2) )
        
        fOut[i]  = fIn[i] - omega1 * (fIn[i]-fEq[i])
        gOut[i]  = gIn[i] - omega2 * (gIn[i]-gEq[i])

    # STREAMING STEP FLUID 1 AND 2
    for i in range(0,9):
        fIn[i] = numpy.roll( \
            numpy.roll(fOut[i], cxNS[i], axis = 0), \
                cyNS[i], axis = 1)
        gIn[i] = numpy.roll( \
            numpy.roll(gOut[i], cxNS[i], axis = 0), \
                cyNS[i], axis = 1)
        
    # VISUALIZATION
    if not cycle%tPlot:
        # rho1     = reshape(rho1,lx,ly);
        plt.imshow(rho1)
        plt.draw()
        # imagesc(rho1'); colorbar
        # title('Fluid 1 density');
        # axis equal off; drawnow
        
