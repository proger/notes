######################################################
# thermalLB.m: Rayleigh Benard Convection, using a LB method,
#   based on [Z.Guo, e.a., http://dx.doi.org/10.1002/fld.337].
#   Boussinesq approximation is used for the buoyancy term:
#     - Fluid is approximated with incompressible Navier-Stokes
#       equations including a body force term, and simulated
#       with a BGK model
#     - Temperature is approximated with advection-diffusion
#       equation and simulated with a BGK model
#
######################################################
# Lattice Boltzmann sample, written in matlab
# Copyright (C) 2008 Andrea Parmigiani, Orestis Malaspinas, Jonas Latt
# Address: Rue General Dufour 24,  1211 Geneva 4, Switzerland
# E-mail: andrea.parmigiani@terre.unige.ch
######################################################
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA  02110-1301, USA.
######################################################

# adapted to python by P. Kestener
# with the help of http://www.mail-archive.com/numpy-discussion@scipy.org/msg24429.html
# date May 13 2011

import numpy
import matplotlib
matplotlib.use('GtkAgg')
import matplotlib.pyplot as plt
plt.ion() # enables interactive mode

# GENERAL FLOW CONSTANTS

ly           = 51
aspect_ratio = 2
lx           = aspect_ratio*ly
delta_x      = 1./(ly-2)
Pr           = 1.
Ra           = 20000. # Rayleigh number
gr           = 0.001  # Gravity
buoyancy     = [0,gr]

Thot  = 1 # Heating on bottom wall
Tcold = 0 # Cooling on top wall
T0 = (Thot+Tcold)/2

delta_t = numpy.sqrt(gr*delta_x)
# nu: kinematic viscosity in lattice units
nu      = numpy.sqrt(Pr/Ra)*delta_t/(delta_x*delta_x)
# k: thermal diffusivity
k       = numpy.sqrt(1./(Pr*Ra))*delta_t/(delta_x*delta_x)
omegaNS = 1./(3*nu+0.5)  # Relaxation parameter for fluid
omegaT  = 1./(3.*k+0.5)  # Relaxation parameter for temperature

maxT   = 4000    # total number of iterations
tPlot  = 100      # iterations between successive graphical outputs
tStatistics = 10  # iterations between successive file accesses

# D2Q9 LATTICE CONSTANTS
tNS   = numpy.array([4./9, 1./9, 1./9, 1./9, 1./9, 1./36, 1./36, 1./36, 1./36])
cxNS  = numpy.array([   0,    1,    0,   -1,    0,     1,    -1,    -1,     1])
cyNS  = numpy.array([   0,    0,    1,    0,   -1,     1,     1,    -1,    -1])
oppNS = numpy.array([   0,    3,    4,    1,    2,     7,     8,     5,     6])

# D2Q5 LATTICE CONSTANTS
tT   =  numpy.array([1/3., 1/6., 1/6., 1/6., 1/6.])
cxT  =  numpy.array([   0,    1,    0,   -1,   0 ])
cyT  =  numpy.array([   0,    0,    1,    0,  -1 ])
oppT =  numpy.array([   0,    3,    4,    1,   2 ])

[y,x] = numpy.meshgrid(numpy.arange(ly),numpy.arange(lx))

# INITIAL CONDITION FOR FLUID: (rho=1, u=0) ==> fIn(i) = t(i)
fIn = tNS[:, numpy.newaxis, numpy.newaxis].\
		repeat(lx, axis = 1).\
		repeat(ly, axis = 2)

# INITIAL CONDITION FOR TEMPERATURE: (T=0) ==> TIn(i) = t(i)
tIn = Tcold * tT[:, numpy.newaxis, numpy.newaxis].\
		repeat(lx, axis = 1).\
		repeat(ly, axis = 2)

# Except for bottom wall, where T=1
tIn[:,:,ly-1] = Thot*tT[:, numpy.newaxis].repeat(lx,axis = 1)

# We need a small trigger, to break symmetry
tIn[:,lx/2,ly-2]=  tT * ( Thot + (Thot/10.) )

# Open file for statistics
fid = open('thermal_statistics.dat','w')
fid.write('Thermal Statistics: time-step --- uy[nx/2,ny/2] --- Nu\n\n\n')

# MAIN LOOP (TIME CYCLES)
for cycle in range(maxT):

    # MACROSCOPIC VARIABLES
    rho = fIn.sum(axis=0) # density
    T   = tIn.sum(axis=0) # temperature
    ux = (cxNS[:,numpy.newaxis,numpy.newaxis] * fIn).sum(axis = 0) / rho
    uy = (cyNS[:,numpy.newaxis,numpy.newaxis] * fIn).sum(axis = 0) / rho
    
    # MACROSCOPIC BOUNDARY CONDITIONS
    # NO-SLIP for fluid and CONSTANT at lower and upper
    # boundary...  periodicity wrt. left-right

    # COLLISION STEP FLUID
    fEq   = numpy.zeros( (9, lx, ly) )
    fOut  = numpy.zeros( (9, lx, ly) )
    force = numpy.zeros( (9, lx, ly) )
    for i in range(0,9):
        cuNS   = 3.0 * ( cxNS[i]*ux + cyNS[i]*uy )
        fEq[i] = rho * tNS[i] * ( 1.0 + cuNS + 0.5 * cuNS**2 - \
                                      1.5 * (ux**2 + uy**2) )
        force[i] = 3.0 * tNS[i] * rho * (T-T0) * \
            ( cxNS[i] * buoyancy[0] + cyNS[i] * buoyancy[1] ) / (Thot-Tcold)
        fOut[i]  = fIn[i] - omegaNS * ( fIn[i] - fEq[i] ) + force[i]
        

    # COLLISION STEP TEMPERATURE
    tEq   = numpy.zeros( (5, lx, ly) )
    tOut  = numpy.zeros( (5, lx, ly) )
    for i in range(0,5):
        cu       = 3 * ( cxT[i]*ux + cyT[i]*uy )
        tEq[i]   = T * tT[i] * ( 1 + cu )
        tOut[i]  = tIn[i] - omegaT * ( tIn[i] - tEq[i] )

    # MICROSCOPIC BOUNDARY CONDITIONS FOR FLUID
    for i in range(0,9):
        fOut[i,:,0]    = fIn[oppNS[i],:,0]
        fOut[i,:,ly-1] = fIn[oppNS[i],:,ly-1]

    # STREAMING STEP FLUID
    for i in range(0,9):
        fIn[i] = numpy.roll( \
            numpy.roll(fOut[i], cxNS[i], axis = 0), \
                cyNS[i], axis = 1)
        
    # STREAMING STEP FLUID
    for i in range(0,5):
        tIn[i] = numpy.roll( \
            numpy.roll(tOut[i], cxT[i], axis = 0), \
                cyT[i], axis = 1)

    # MICROSCOPIC BOUNDARY CONDITIONS FOR TEMEPERATURE
    tIn[4,:,ly-1] = Tcold - tIn[0,:,ly-1] - tIn[1,:,ly-1] - tIn[2,:,ly-1] - tIn[3,:,ly-1]
    tIn[2,:,0]    = Thot  - tIn[0,:,0]    - tIn[1,:,0]    - tIn[3,:,0]    - tIn[4,:,0]

    # VISUALIZATION
    if not cycle%tStatistics:
        u     = numpy.sqrt( ux**2 + uy**2 )
        uy_Nu = uy   # vertical velocity
        Nu    = 1.0 + (uy_Nu*T).sum() / (lx*k*(Thot-Tcold))
        fid.write( str(cycle) + ' ' + str(u[lx/2,ly/2]**2) + ' ' + str(Nu) +'\n' )
        if not cycle%tPlot:
            plt.subplot(311)
            plt.imshow(T.transpose())
            #plt.contour(T.transpose())
            plt.subplot(312)
            plt.imshow(ux.transpose())
            #plt.contour(ux.transpose())
            plt.subplot(313)
            plt.imshow(uy.transpose())
            #plt.contour(uy.transpose())
            plt.suptitle('Rayleigh-Benard instability\n Temperature (Nusselt number is '+str(Nu)+')\nux\nuy')
            #f.text(.5, .95, 'My Title', horizontalalignment='center')
            plt.draw()
            # subplot(2,1,1);
            # imagesc(u(:,ly:-1:1)');
            # title('Fluid velocity');
            # axis off; drawnow
            # subplot(2,1,2);
            # imagesc(T(:,ly:-1:1)')
            # title(['Temperature (Nusselt number is ' num2str(Nu) ')']);
            # axis off; drawnow

fid.close()
