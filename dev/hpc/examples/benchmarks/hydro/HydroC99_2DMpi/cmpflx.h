#ifndef CMPFLX_H_INCLUDED
#define CMPFLX_H_INCLUDED

#include "utils.h"

void cmpflx(const int narray,
            const int Hnxyt,
            const int Hnvar,
            const double Hgamma,
            const int slices, const int Hstep, double qgdnv[Hnvar][Hstep][Hnxyt], double flux[Hnvar][Hstep][Hnxyt]);

#endif // CMPFLX_H_INCLUDED
