#ifndef CONSTOPRIM_H_INCLUDED
#define CONSTOPRIM_H_INCLUDED

#include "utils.h"



void constoprim(const int n,
                const int Hnxyt,
                const int Hnvar,
                const double Hsmallr,
                const int slices, const int Hstep,
                double u[Hnvar][Hstep][Hnxyt], double q[Hnvar][Hstep][Hnxyt], double e[Hstep][Hnxyt]);

#endif // CONSTOPRIM_H_INCLUDED
