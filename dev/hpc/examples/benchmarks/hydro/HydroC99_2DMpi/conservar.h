#ifndef CONSERVAR_H_INCLUDED
#define CONSERVAR_H_INCLUDED



void gatherConservativeVars(const int idim,
                            const int rowcol,
                            const int Himin,
                            const int Himax,
                            const int Hjmin,
                            const int Hjmax,
                            const int Hnvar,
                            const int Hnxt,
                            const int Hnyt,
                            const int Hnxyt,
                            const int slices, const int Hstep,
                            double uold[Hnvar * Hnxt * Hnyt], double u[Hnvar][Hstep][Hnxyt]);

void updateConservativeVars(const int idim,
                            const int rowcol,
                            const double dtdx,
                            const int Himin,
                            const int Himax,
                            const int Hjmin,
                            const int Hjmax,
                            const int Hnvar,
                            const int Hnxt,
                            const int Hnyt,
                            const int Hnxyt,
                            const int slices, const int Hstep,
                            double uold[Hnvar * Hnxt * Hnyt],
                            double u[Hnvar][Hstep][Hnxyt], double flux[Hnvar][Hstep][Hnxyt]
  );

#endif // CONSERVAR_H_INCLUDED
