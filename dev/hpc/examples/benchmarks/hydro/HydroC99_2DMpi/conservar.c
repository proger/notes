/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#ifndef HMPP
#include "parametres.h"
#include "utils.h"
#include "conservar.h"

#define CFLOPS(c)               /* {flops+=c;} */

void
gatherConservativeVars(const int idim,
                       const int rowcol,
                       const int Himin,
                       const int Himax,
                       const int Hjmin,
                       const int Hjmax,
                       const int Hnvar,
                       const int Hnxt,
                       const int Hnyt,
                       const int Hnxyt,
                       const int slices, const int Hstep,
                       double uold[Hnvar * Hnxt * Hnyt], double u[Hnvar][Hstep][Hnxyt]
  ) {
  int i, j, ivar, s;

#define IHU(i, j, v)  ((i) + Hnxt * ((j) + Hnyt * (v)))

  WHERE("gatherConservativeVars");
  if (idim == 1) {
    // Gather conservative variables
    for (s = 0; s < slices; s++) {
      for (i = Himin; i < Himax; i++) {
        int idxuoID = IHU(i, rowcol + s, ID);
        u[ID][s][i] = uold[idxuoID];

        int idxuoIU = IHU(i, rowcol + s, IU);
        u[IU][s][i] = uold[idxuoIU];

        int idxuoIV = IHU(i, rowcol + s, IV);
        u[IV][s][i] = uold[idxuoIV];

        int idxuoIP = IHU(i, rowcol + s, IP);
        u[IP][s][i] = uold[idxuoIP];
      }
    }

    if (Hnvar > IP) {
      for (ivar = IP + 1; ivar < Hnvar; ivar++) {
        for (s = 0; s < slices; s++) {
          for (i = Himin; i < Himax; i++) {
            u[ivar][s][i] = uold[IHU(i, rowcol + s, ivar)];
          }
        }
      }
    }
    //
  } else {
    // Gather conservative variables
    for (s = 0; s < slices; s++) {
      for (j = Hjmin; j < Hjmax; j++) {
        u[ID][s][j] = uold[IHU(rowcol + s, j, ID)];
        u[IU][s][j] = uold[IHU(rowcol + s, j, IV)];
        u[IV][s][j] = uold[IHU(rowcol + s, j, IU)];
        u[IP][s][j] = uold[IHU(rowcol + s, j, IP)];
      }
    }
    if (Hnvar > IP) {
      for (ivar = IP + 1; ivar < Hnvar; ivar++) {
        for (s = 0; s < slices; s++) {
          for (j = Hjmin; j < Hjmax; j++) {
            u[ivar][s][j] = uold[IHU(rowcol + s, j, ivar)];
          }
        }
      }
    }
  }
}

#undef IHU

void
updateConservativeVars(const int idim,
                       const int rowcol,
                       const double dtdx,
                       const int Himin,
                       const int Himax,
                       const int Hjmin,
                       const int Hjmax,
                       const int Hnvar,
                       const int Hnxt,
                       const int Hnyt,
                       const int Hnxyt,
                       const int slices, const int Hstep,
                       double uold[Hnvar * Hnxt * Hnyt], double u[Hnvar][Hstep][Hnxyt], double flux[Hnvar][Hstep][Hnxyt]
  ) {
  int i, j, ivar, s;
  WHERE("updateConservativeVars");

#define IHU(i, j, v)  ((i) + Hnxt * ((j) + Hnyt * (v)))

  if (idim == 1) {

    // Update conservative variables
    for (ivar = 0; ivar <= IP; ivar++) {
      for (s = 0; s < slices; s++) {
        for (i = Himin + ExtraLayer; i < Himax - ExtraLayer; i++) {
          uold[IHU(i, rowcol + s, ivar)] = u[ivar][s][i] + (flux[ivar][s][i - 2] - flux[ivar][s][i - 1]) * dtdx;
          CFLOPS(3);
        }
      }
    }

    if (Hnvar > IP) {
      for (ivar = IP + 1; ivar < Hnvar; ivar++) {
        for (s = 0; s < slices; s++) {
          for (i = Himin + ExtraLayer; i < Himax - ExtraLayer; i++) {
            uold[IHU(i, rowcol + s, ivar)] = u[ivar][s][i] + (flux[ivar][s][i - 2] - flux[ivar][s][i - 1]) * dtdx;
            CFLOPS(3);
          }
        }
      }
    }
  } else {
    // Update conservative variables
    for (s = 0; s < slices; s++) {
      for (j = Hjmin + ExtraLayer; j < Hjmax - ExtraLayer; j++) {
        uold[IHU(rowcol + s, j, ID)] = u[ID][s][j] + (flux[ID][s][j - 2] - flux[ID][s][j - 1]) * dtdx;
        CFLOPS(3);

        uold[IHU(rowcol + s, j, IV)] = u[IU][s][j] + (flux[IU][s][j - 2] - flux[IU][s][j - 1]) * dtdx;
        CFLOPS(3);

        uold[IHU(rowcol + s, j, IU)] = u[IV][s][j] + (flux[IV][s][j - 2] - flux[IV][s][j - 1]) * dtdx;
        CFLOPS(3);

        uold[IHU(rowcol + s, j, IP)] = u[IP][s][j] + (flux[IP][s][j - 2] - flux[IP][s][j - 1]) * dtdx;
        CFLOPS(3);
      }
    }

    if (Hnvar > IP) {
      for (ivar = IP + 1; ivar < Hnvar; ivar++) {
        for (s = 0; s < slices; s++) {
          for (j = Hjmin + ExtraLayer; j < Hjmax - ExtraLayer; j++) {
            uold[IHU(rowcol + s, j, ivar)] = u[ivar][s][j] + (flux[ivar][s][j - 2] - flux[ivar][s][j - 1]) * dtdx;
            CFLOPS(3);
          }
        }
      }
    }
  }
}

#undef IHU
#endif
//EOF
