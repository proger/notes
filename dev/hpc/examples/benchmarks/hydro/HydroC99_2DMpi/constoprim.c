/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#ifndef HMPP
#include "parametres.h"
#include "constoprim.h"
#include "utils.h"

#define CFLOPS(c)               /* {flops+=c;} */

void
constoprim(const int n,
           const int Hnxyt,
           const int Hnvar,
           const double Hsmallr,
           const int slices, const int Hstep,
           double u[Hnvar][Hstep][Hnxyt], double q[Hnvar][Hstep][Hnxyt], double e[Hstep][Hnxyt]) {
  int ijmin, ijmax, IN, i, s;
  double eken;
  // const int nxyt = Hnxyt;
  WHERE("constoprim");
  ijmin = 0;
  ijmax = n;

  for (s = 0; s < slices; s++) {
    for (i = ijmin; i < ijmax; i++) {
      double qid = MAX(u[ID][s][i], Hsmallr);
      q[ID][s][i] = qid;

      double qiu = u[IU][s][i] / qid;
      double qiv = u[IV][s][i] / qid;
      q[IU][s][i] = qiu;
      q[IV][s][i] = qiv;

      eken = half * (Square(qiu) + Square(qiv));

      double qip = u[IP][s][i] / qid - eken;
      q[IP][s][i] = qip;
      e[s][i] = qip;

      CFLOPS(9);
    }
  }

  if (Hnvar > IP) {
    for (IN = IP + 1; IN < Hnvar; IN++) {
      for (s = 0; s < slices; s++) {
        for (i = ijmin; i < ijmax; i++) {
          q[IN][s][i] = u[IN][s][i] / q[IN][s][i];
          CFLOPS(1);
        }
      }
    }
  }
}                               // constoprim


#undef IHVW
#endif
//EOF
