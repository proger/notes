#ifndef TRACE_H_INCLUDED
#define TRACE_H_INCLUDED

#include "hmpp.h"



void trace(const double dtdx,
           const int n,
           const int Hscheme,
           const int Hnvar,
           const int Hnxyt,
           const int slices, const int Hstep,
           double q[Hnvar][Hstep][Hnxyt],
           double dq[Hnvar][Hstep][Hnxyt],
           double c[Hstep][Hnxyt], double qxm[Hnvar][Hstep][Hnxyt], double qxp[Hnvar][Hstep][Hnxyt]
  );

#endif // TRACE_H_INCLUDED
