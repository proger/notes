/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <math.h>
#include <malloc.h>
// #include <unistd.h>
// #include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifndef HMPP
#include "parametres.h"
#include "utils.h"
#include "cmpflx.h"

#define CFLOPS(c)               /* {flops+=c;} */

void
cmpflx(const int narray,
       const int Hnxyt,
       const int Hnvar,
       const double Hgamma,
       const int slices, const int Hstep, double qgdnv[Hnvar][Hstep][Hnxyt], double flux[Hnvar][Hstep][Hnxyt]) {
  int nface, i, IN;
  double entho, ekin, etot;
  WHERE("cmpflx");
  int s;

  nface = narray;
  entho = one / (Hgamma - one);

  // Compute fluxes
  for (s = 0; s < slices; s++) {
    for (i = 0; i < nface; i++) {
      double qgdnvID = qgdnv[ID][s][i];
      double qgdnvIU = qgdnv[IU][s][i];
      double qgdnvIP = qgdnv[IP][s][i];
      double qgdnvIV = qgdnv[IV][s][i];

      // Mass density
      double massDensity = qgdnvID * qgdnvIU;
      flux[ID][s][i] = massDensity;

      // Normal momentum
      flux[IU][s][i] = massDensity * qgdnvIU + qgdnvIP;
      // Transverse momentum 1
      flux[IV][s][i] = massDensity * qgdnvIV;

      // Total energy
      ekin = half * qgdnvID * (Square(qgdnvIU) + Square(qgdnvIV));
      etot = qgdnvIP * entho + ekin;

      flux[IP][s][i] = qgdnvIU * (etot + qgdnvIP);

      CFLOPS(15);
    }
  }

  // Other advected quantities
  if (Hnvar > IP) {
    for (s = 0; s < slices; s++) {
      for (IN = IP + 1; IN < Hnvar; IN++) {
        for (i = 0; i < nface; i++) {
          flux[IN][s][i] = flux[IN][s][i] * qgdnv[IN][s][i];
        }
      }
    }
  }
}                               // cmpflx

#endif

//EOF
