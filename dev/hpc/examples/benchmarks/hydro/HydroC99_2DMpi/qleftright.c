/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "qleftright.h"

#ifndef HMPP

void
// qleftright(const int idim, const hydroparam_t H, hydrovarwork_t * Hvw)
qleftright(const int idim,
           const int Hnx,
           const int Hny,
           const int Hnxyt,
           const int Hnvar,
           const int slices, const int Hstep,
           double qxm[Hnvar][Hstep][Hnxyt],
           double qxp[Hnvar][Hstep][Hnxyt], double qleft[Hnvar][Hstep][Hnxyt], double qright[Hnvar][Hstep][Hnxyt]) {
  // #define IHVW(i,v) ((i) + (v) * Hnxyt)
  int nvar, i, s;
  int bmax;
  WHERE("qleftright");
  if (idim == 1) {
    bmax = Hnx + 1;
  } else {
    bmax = Hny + 1;
  }
  for (nvar = 0; nvar < Hnvar; nvar++) {
    for (s = 0; s < slices; s++) {
      for (i = 0; i < bmax; i++) {
        qleft[nvar][s][i] = qxm[nvar][s][i + 1];
        qright[nvar][s][i] = qxp[nvar][s][i + 2];
      }
    }
  }
}

#undef IHVW

#endif /* HMPP */
// EOF
