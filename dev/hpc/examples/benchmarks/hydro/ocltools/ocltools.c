/*
 * (C) CEA/DAM Guillaume.Colin-de-Verdiere at Cea.Fr
 */
#include <CL/cl.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ocltools.h"

typedef struct _DeviceDesc {
  size_t mwid;                  // CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS
  size_t mwis[3];               // CL_DEVICE_MAX_WORK_ITEM_SIZES
  size_t mwgs;                  // CL_DEVICE_MAX_WORK_GROUP_SIZE
  size_t mmas;                  // CL_DEVICE_MAX_MEM_ALLOC_SIZE
  int vecchar;                  // CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR
  int vecshort;                 // CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT
  int vecint;                   // CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT
  int veclong;                  // CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG
  int vecfloat;                 // CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT
  int vecdouble;                // CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE
} DeviceDesc_t;

typedef struct _PlatformDesc {
  cl_device_id *devices;
  DeviceDesc_t *devdesc;
  cl_uint nbdevices;
} PlatformDesc_t;

static PlatformDesc_t *pdesc = NULL;
static cl_platform_id *platform = NULL;
static int _profiling = 0;


void
oclMkNDrange(const size_t nb, const size_t nbthreads, const MkNDrange_t form, size_t gws[3], size_t lws[3])
{
  size_t sizec;
  long leftover;
  gws[0] = 1;
  gws[1] = 1;
  gws[2] = 1;
  lws[0] = 1;
  lws[1] = 1;
  lws[2] = 1;

  if (form == NDR_1D) {
    sizec = nb;
    lws[0] = nbthreads;
    gws[0] = (sizec + lws[0] - 1) / lws[0];
    gws[0] *= lws[0];
  }

  if (form == NDR_2D) {
    sizec = (size_t) sqrt(nb);
    if ((sizec * sizec) < nb)
      sizec++;
    while ((lws[0] * lws[1]) < nbthreads) {
      if ((lws[0] * lws[1]) < nbthreads)
        lws[0] *= 2;
      if ((lws[0] * lws[1]) < nbthreads)
        lws[1] *= 2;
    }
    // pour ne pas depasser le nombre de threads demandes
    if ((lws[0] * lws[1]) > nbthreads)
      lws[1]--;
    // normalisation des dimensions pour faire plaisir a OpenCL
    gws[0] = (sizec + lws[0] - 1) / lws[0];
    gws[0] *= lws[0];
    gws[1] = (sizec + lws[1] - 1) / lws[1];
    gws[1] *= lws[1];
  }

  if (form == NDR_3D) {
    sizec = (size_t) cbrt(nb);
    if ((sizec * sizec * sizec) < nb)
      sizec++;
    while ((lws[0] * lws[1] * lws[2]) < nbthreads) {
      if ((lws[0] * lws[1] * lws[2]) < nbthreads)
        lws[0] *= 2;
      if ((lws[0] * lws[1] * lws[2]) < nbthreads)
        lws[1] *= 2;
      if ((lws[0] * lws[1] * lws[2]) < nbthreads)
        lws[0] *= 2;
      if ((lws[0] * lws[1] * lws[2]) < nbthreads)
        lws[1] *= 2;
      if ((lws[0] * lws[1] * lws[2]) < nbthreads)
        lws[2] *= 2;
    }
    // pour ne pas depasser le nombre de threads demandes
    if ((lws[0] * lws[1] * lws[2]) > nbthreads && (lws[2] > 1))
      lws[2]--;
    if ((lws[0] * lws[1] * lws[2]) > nbthreads && (lws[1] > 1))
      lws[1]--;
    if ((lws[0] * lws[1] * lws[2]) > nbthreads && (lws[0] > 1))
      lws[0]--;
    // normalisation des dimensions pour faire plaisir a OpenCL
    gws[0] = (sizec + lws[0] - 1) / lws[0];
    gws[0] *= lws[0];
    gws[1] = (sizec + lws[1] - 1) / lws[1];
    gws[1] *= lws[1];
    gws[2] = (sizec + lws[2] - 1) / lws[2];
    gws[2] *= lws[2];
  }

  if ((gws[0] * gws[1] * gws[2]) < nb)
    gws[0] += lws[0];

  leftover = nb - (gws[0] * gws[1] * gws[2]);
  if (leftover > 0) {
    fprintf(stderr, "nb %ld nbt %ld, gws %ld %ld %ld lws %ld %ld %ld (%ld)\n",
            nb, nbthreads, gws[0], gws[1], gws[2], lws[0], lws[1], lws[2], leftover);
    exit(1);
  }
  return;
}

double
oclChronoElaps(const cl_event event)
{
  cl_ulong Wstart, Wend;
  double start = 0, end = 0;
  cl_int err = 0;

  if (_profiling) {
    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(Wstart), &Wstart, NULL);
    oclCheckErr(err, "clGetEventProfilingInfo Wstart");
    err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(Wend), &Wend, NULL);
    oclCheckErr(err, "clGetEventProfilingInfo Wend");
    start = Wstart / 1.e9;
    end = Wend / 1.e9;
  }
  return (end - start);
}

void
oclPrintErr(cl_int rc, const char *msg, const char *file, const int line)
{
  char message[1000];
  if (rc != CL_SUCCESS) {
    strcpy(message, "");
    switch (rc) {
    case 0:
      strcat(message, "CL_SUCCESS");;
      break;
    case -1:
      strcat(message, "CL_DEVICE_NOT_FOUND");;
      break;
    case -2:
      strcat(message, "CL_DEVICE_NOT_AVAILABLE");;
      break;
    case -3:
      strcat(message, "CL_COMPILER_NOT_AVAILABLE");;
      break;
    case -4:
      strcat(message, "CL_MEM_OBJECT_ALLOCATION_FAILURE");;
      break;
    case -5:
      strcat(message, "CL_OUT_OF_RESOURCES");;
      break;
    case -6:
      strcat(message, "CL_OUT_OF_HOST_MEMORY");;
      break;
    case -7:
      strcat(message, "CL_PROFILING_INFO_NOT_AVAILABLE");;
      break;
    case -8:
      strcat(message, "CL_MEM_COPY_OVERLAP");;
      break;
    case -9:
      strcat(message, "CL_IMAGE_FORMAT_MISMATCH");;
      break;
    case -10:
      strcat(message, "CL_IMAGE_FORMAT_NOT_SUPPORTED");;
      break;
    case -11:
      strcat(message, "CL_BUILD_PROGRAM_FAILURE");;
      break;
    case -12:
      strcat(message, "CL_MAP_FAILURE");;
      break;
    case -30:
      strcat(message, "CL_INVALID_VALUE");;
      break;
    case -31:
      strcat(message, "CL_INVALID_DEVICE_TYPE");;
      break;
    case -32:
      strcat(message, "CL_INVALID_PLATFORM");;
      break;
    case -33:
      strcat(message, "CL_INVALID_DEVICE");;
      break;
    case -34:
      strcat(message, "CL_INVALID_CONTEXT");;
      break;
    case -35:
      strcat(message, "CL_INVALID_QUEUE_PROPERTIES");;
      break;
    case -36:
      strcat(message, "CL_INVALID_COMMAND_QUEUE");;
      break;
    case -37:
      strcat(message, "CL_INVALID_HOST_PTR");;
      break;
    case -38:
      strcat(message, "CL_INVALID_MEM_OBJECT");;
      break;
    case -39:
      strcat(message, "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR");;
      break;
    case -40:
      strcat(message, "CL_INVALID_IMAGE_SIZE");;
      break;
    case -41:
      strcat(message, "CL_INVALID_SAMPLER");;
      break;
    case -42:
      strcat(message, "CL_INVALID_BINARY");;
      break;
    case -43:
      strcat(message, "CL_INVALID_BUILD_OPTIONS");;
      break;
    case -44:
      strcat(message, "CL_INVALID_PROGRAM");;
      break;
    case -45:
      strcat(message, "CL_INVALID_PROGRAM_EXECUTABLE");;
      break;
    case -46:
      strcat(message, "CL_INVALID_KERNEL_NAME");;
      break;
    case -47:
      strcat(message, "CL_INVALID_KERNEL_DEFINITION");;
      break;
    case -48:
      strcat(message, "CL_INVALID_KERNEL");;
      break;
    case -49:
      strcat(message, "CL_INVALID_ARG_INDEX");;
      break;
    case -50:
      strcat(message, "CL_INVALID_ARG_VALUE");;
      break;
    case -51:
      strcat(message, "CL_INVALID_ARG_SIZE");;
      break;
    case -52:
      strcat(message, "CL_INVALID_KERNEL_ARGS");;
      break;
    case -53:
      strcat(message, "CL_INVALID_WORK_DIMENSION");;
      break;
    case -54:
      strcat(message, "CL_INVALID_WORK_GROUP_SIZE");;
      break;
    case -55:
      strcat(message, "CL_INVALID_WORK_ITEM_SIZE");;
      break;
    case -56:
      strcat(message, "CL_INVALID_GLOBAL_OFFSET");;
      break;
    case -57:
      strcat(message, "CL_INVALID_EVENT_WAIT_LIST");;
      break;
    case -58:
      strcat(message, "CL_INVALID_EVENT");;
      break;
    case -59:
      strcat(message, "CL_INVALID_OPERATION");;
      break;
    case -60:
      strcat(message, "CL_INVALID_GL_OBJECT");;
      break;
    case -61:
      strcat(message, "CL_INVALID_BUFFER_SIZE");;
      break;
    case -62:
      strcat(message, "CL_INVALID_MIP_LEVEL");;
      break;
    case -63:
      strcat(message, "CL_INVALID_GLOBAL_WORK_SIZE");;
      break;
    case -1001:
      strcat(message, "CL_PLATFORM_NOT_FOUND_KHR");;
      break;
    default:
      strcat(message, "unknown code");
    }
    fprintf(stderr, "Error %d <%s> (%s) [f=%s l=%d]\n", rc, message, msg, file, line);
  }
}

void
oclCheckErrF(cl_int rc, const char *msg, const char *file, const int line)
{
  if (rc != CL_SUCCESS) {
    oclPrintErr(rc, msg, file, line);
    abort();
  }
}

cl_uint
oclCreateProgramString(const char *fname, char ***pgmt, cl_uint * pgml)
{
  FILE *fd;
  int i = 0;
  int nbr = 500;
  char buffer[1024], *ptr;

  *pgmt = calloc(500, sizeof(char *));
  fd = fopen(fname, "r");

  if (fd) {
    while ((ptr = fgets(buffer, 1024, fd)) != NULL) {
      if (i >= (nbr - 2)) {
        nbr += 500;
        *pgmt = realloc(*pgmt, nbr * sizeof(char *));
      }
      (*pgmt)[i] = strdup(buffer);
      (*pgmt)[i + 1] = NULL;
      i++;
    }

    fclose(fd);
  } else {
    fprintf(stderr, "File %s not found; Aborting.\n", fname);
    abort();
  }
  *pgml = i;
  return CL_SUCCESS;
}

cl_program
oclCreatePgmFromCtx(const char *srcFile, const char *srcDir,
                    const cl_context ctx, const int theplatform, const int thedev, const int verbose)
{
  int i;
  cl_int err = 0;
  char **pgmt;
  cl_uint pgml;
  cl_program pgm;
  char *message = NULL;
  size_t msgl = 0;
  char options[1000];

  // creation d'un programme
  // printf("CreateProgramString\n");
  // -- on va lire le programme depuis le disque pour le mettre dans un tableau de strings
  err = oclCreateProgramString(srcFile, &pgmt, &pgml);
  // printf("CreateProgramString. (%d)\n", pgml);

  // Sortie du programme pour le voir ;-)
  if (verbose == 2) {
    for (i = 0; i < pgml; i++) {
      printf("%s", pgmt[i]);
    }
  }
  // creation du programme OpenCL a partir des strings
  // printf("clCreateProgramWithSource\n");
  pgm = clCreateProgramWithSource(ctx, pgml, (const char **) pgmt, NULL, &err);
  // printf("clCreateProgramWithSource.\n");
  oclCheckErr(err, "Creation du program");

  // compilation du programme
  // printf("clBuildProgram\n");
  // le 0, NULL specifie que le programme est construit pour tous les devices existants
  // err = 0;
  //     if (verbose) {
  //       printf("Compiling with option <%s>\n", "-cl-mad-enable");
  //     }
  // err = clBuildProgram(pgm, 0, NULL, "-cl-mad-enable", NULL, NULL);
  //err = clBuildProgram(pgm, 0, NULL, "", NULL, NULL);
  // ,-cl-nv-opt-level 3
  strcpy(options, "");
  strcat(options, "-O3 ");
  strcat(options, "-cl-mad-enable ");
#ifdef AMDATI
  strcat(options, "-DAMDATI ");
#endif
#ifdef NVIDIA
  strcat(options, "-DNVIDIA ");
#endif
  if (srcDir != NULL) {
    strcat(options, "-I");
    strcat(options, srcDir);
    strcat(options, " ");
  }

  err = clBuildProgram(pgm, 0, NULL, options, NULL, NULL);
  // printf("clBuildProgram.\n");

  // Seul moyen de recuperer les infos de compilation : les demander a l'objet programme
  // CheckErr(err, "clGetProgramBuildInfo");
  if (err != CL_SUCCESS) {
    fprintf(stderr, "Build OpenCL (opts=\"%s\") has error(s).\n", options);
    oclPrintErr(err, "clBuildProgram", __FILE__, __LINE__);
    assert(pdesc != NULL);
    assert(pdesc[theplatform].devices != NULL);

    // get the message length for the buffer allocation
    err = clGetProgramBuildInfo(pgm, pdesc[theplatform].devices[thedev], CL_PROGRAM_BUILD_LOG, 1, NULL, &msgl);
    // fprintf(stderr, "longueur du message d'erreur %d\n", msgl);
    message = (char *) calloc(msgl + 16, 1);
    assert(message != NULL);
    err = clGetProgramBuildInfo(pgm, pdesc[theplatform].devices[thedev], CL_PROGRAM_BUILD_LOG, msgl, message, &msgl);
    fprintf(stderr, "\n\n");
    fprintf(stderr, "------------------------------------\n");
    fprintf(stderr, "%s\n", message);
    fprintf(stderr, "------------------------------------\n");
    fprintf(stderr, "\n\n");
    oclCheckErr(err, "clGetProgramBuildInfo");
    abort();
  } else {
    fprintf(stderr, "Build OpenCL (opts=\"%s\") OK.\n", options);
  }
  // menage du texte du programme
  for (i = 0; i < pgml; i++) {
    if (pgmt[i])
      free(pgmt[i]);
  }
  free(pgmt);

  return pgm;
}

int
oclGetNbPlatforms(const int verbose)
{
  char message[1000];
  size_t msgl = 0;
  int i;
  cl_int err = 0;
  cl_uint nbplatforms = 0;

  // informations sur la platform
  err = 0;
  err = clGetPlatformIDs(0, NULL, &nbplatforms);
  oclCheckErr(err, "GetPlatformIDs -- 1");
  platform = (cl_platform_id *) calloc(nbplatforms, sizeof(cl_platform_id));
  err = clGetPlatformIDs(nbplatforms, platform, &nbplatforms);
  oclCheckErr(err, "GetPlatformIDs -- 2");
  printf("Nb platform : %d\n", nbplatforms);
  for (i = 0; i < nbplatforms; i++) {
    err = clGetPlatformInfo(platform[i], CL_PLATFORM_PROFILE, 1000, (void *) message, &msgl);
    oclCheckErr(err, "GetPFInof PROFILE");
    printf("[%d] Profile : %s\n", i, message);
    err = clGetPlatformInfo(platform[i], CL_PLATFORM_VERSION, 1000, (void *) message, &msgl);
    oclCheckErr(err, "GetPFInof VERSION");
    printf("[%d] VERSION : %s\n", i, message);
    err = clGetPlatformInfo(platform[i], CL_PLATFORM_NAME, 1000, (void *) message, &msgl);
    oclCheckErr(err, "GetPFInof NAME");
    printf("[%d] NAME : %s\n", i, message);
    err = clGetPlatformInfo(platform[i], CL_PLATFORM_VENDOR, 1000, message, &msgl);
    oclCheckErr(err, "GetPFInof VENDOR");
    printf("[%d] VENDOR : %s\n", i, message);
    err = clGetPlatformInfo(platform[i], CL_PLATFORM_EXTENSIONS, 1000, message, &msgl);
    oclCheckErr(err, "GetPFInof EXTENSIONS");
    printf("[%d] EXTENSIONS : %s\n", i, message);
  }
  pdesc = (PlatformDesc_t *) calloc(nbplatforms + 1, sizeof(PlatformDesc_t));
  assert(pdesc != NULL);

  return nbplatforms;
}

cl_context
oclCreateCtxForPlatform(const int theplatform, const int verbose)
{
  int j;
  cl_context ctx;
  cl_device_type devtype;
  cl_context_properties proplist[1000];
  char message[1000];
  size_t msgl = 0;
  cl_uint nbdevices = 0;
  cl_int err = 0;
  int maxwid = 0;
  size_t maxwis[4];
  size_t maxwiss = 0;
  size_t maxwgs;
  size_t maxwgss = 0;
  size_t maxclkmhz = 0;
  cl_ulong maxmemallocsz = 0;

  // Query des devices de la platforme
  err = clGetDeviceIDs(platform[theplatform], CL_DEVICE_TYPE_ALL, 0, NULL, &nbdevices);
  oclCheckErr(err, "GetDeviceInfo -- 1");
  pdesc[theplatform].devices = (cl_device_id *) calloc(nbdevices, sizeof(cl_device_id));
  assert(pdesc[theplatform].devices != NULL);
  pdesc[theplatform].devdesc = (DeviceDesc_t *) calloc(nbdevices, sizeof(DeviceDesc_t));
  assert(pdesc[theplatform].devdesc != NULL);

  err =
    clGetDeviceIDs(platform[theplatform], CL_DEVICE_TYPE_ALL, nbdevices, pdesc[theplatform].devices,
                   &pdesc[theplatform].nbdevices);
  oclCheckErr(err, "GetDeviceInfo -- 2");
  printf("[%d] : nbdevices = %d\n", theplatform, nbdevices);
  for (j = 0; j < nbdevices; j++) {
    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(maxwid), &maxwid, NULL);
    oclCheckErr(err, "deviceInfo maxwid");
    if (verbose)
      printf("(%d) :: device mxwkitdim %d", j, maxwid);
    pdesc[theplatform].devdesc[j].mwid = maxwid;

    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(maxwis[0]) * 3, &maxwis, &maxwiss);
    oclCheckErr(err, "deviceInfo maxwis");
    if (verbose)
      printf(" mxwkitsz %ld %ld %ld", maxwis[0], maxwis[1], maxwis[2]);
    memcpy(pdesc[theplatform].devdesc[j].mwis, maxwis, 3 * sizeof(size_t));

    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(maxwgs), &maxwgs, &maxwgss);
    oclCheckErr(err, "deviceInfo maxwgs");
    if (verbose)
      printf(" mxwkgsz %ld ", maxwgs);
    pdesc[theplatform].devdesc[j].mwgs = maxwgs;

    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(maxclkmhz), &maxclkmhz, NULL);
    oclCheckErr(err, "deviceInfo maxclkmhz");
    if (verbose)
      printf(" mxclockMhz %ld", maxclkmhz);

    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(maxmemallocsz), &maxmemallocsz,
                          NULL);
    oclCheckErr(err, "deviceInfo maxmemallocsz");
    if (verbose)
      printf(" mxmemallocsz %ld (Mo)", maxmemallocsz / (1024 * 1024));
    pdesc[theplatform].devdesc[j].mmas = maxmemallocsz;

    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_TYPE, sizeof(devtype), &devtype, NULL);
    oclCheckErr(err, "deviceInfo");
    if (verbose)
      printf(" type %ld", devtype);
    switch (devtype) {
    case CL_DEVICE_TYPE_CPU:
      strcpy(message, "CPU");
      break;
    case CL_DEVICE_TYPE_GPU:
      strcpy(message, "GPU");
      break;
    case CL_DEVICE_TYPE_ACCELERATOR:
      strcpy(message, "ACCELERATOR");
      break;
    }
    if (verbose)
      printf(" [%s]\n", message);

    // TODO recuperer la capacite FP64 si elle existe
    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_EXTENSIONS, 1000, message, &msgl);
    oclCheckErr(err, "deviceInfo");
    if (verbose) {
      printf("   extensions: %s\n", message);
    }
    err =
      clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, sizeof(int),
                      &pdesc[theplatform].devdesc[j].vecchar, NULL);
    err =
      clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, sizeof(int),
                      &pdesc[theplatform].devdesc[j].vecshort, NULL);
    err =
      clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, sizeof(int),
                      &pdesc[theplatform].devdesc[j].vecint, NULL);
    err =
      clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, sizeof(int),
                      &pdesc[theplatform].devdesc[j].veclong, NULL);
    err =
      clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, sizeof(int),
                      &pdesc[theplatform].devdesc[j].vecfloat, NULL);
    err =
      clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, sizeof(int),
                      &pdesc[theplatform].devdesc[j].vecdouble, NULL);
    if (verbose) {
      printf("   prefered vector size: c=%d s=%d i=%d l=%d f=%d d=%d\n",
             pdesc[theplatform].devdesc[j].vecchar,
             pdesc[theplatform].devdesc[j].vecshort,
             pdesc[theplatform].devdesc[j].vecint,
             pdesc[theplatform].devdesc[j].veclong,
             pdesc[theplatform].devdesc[j].vecfloat, pdesc[theplatform].devdesc[j].vecdouble);
    }


  }
  if (verbose)
    fflush(stdout);
  // Creation d'un contexte sur la platform[0] 
  // que veut dire la notion de multiplateforme ?

  proplist[0] = CL_CONTEXT_PLATFORM;
  proplist[1] = (cl_context_properties) platform[theplatform];
  proplist[2] = (cl_context_properties) NULL;
  ctx = clCreateContext(proplist, pdesc[theplatform].nbdevices, pdesc[theplatform].devices, NULL, NULL, &err);
  oclCheckErr(err, "Creation CTX");

  return ctx;
}

cl_command_queue
oclCreateCommandQueueForDev(const int theplatform, const int devselected, const cl_context ctx, const int profiling)
{
  cl_command_queue cqueue;
  cl_int err = 0;

  assert(pdesc != NULL);
  assert(pdesc[theplatform].devices != NULL);

  // creation de la command queue
  if (profiling) {
    cqueue = clCreateCommandQueue(ctx, pdesc[theplatform].devices[devselected], CL_QUEUE_PROFILING_ENABLE, &err);
    _profiling = 1;
  } else {
    cqueue = clCreateCommandQueue(ctx, pdesc[theplatform].devices[devselected], 0, &err);
  }
  // peut etre CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
  oclCheckErr(err, "Creation queue");
  return cqueue;
}

int
oclGetNumberOfDev(const int theplatform)
{
  assert(pdesc != NULL);

  return pdesc[theplatform].nbdevices;
}

int
oclGetNbOfGpu(const int theplatform)
{
  int j, nbgpu = 0;
  cl_int err = 0;
  cl_device_type devtype;

  assert(pdesc != NULL);
  assert(pdesc[theplatform].devices != NULL);

  for (j = 0; j < pdesc[theplatform].nbdevices; j++) {
    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_TYPE, sizeof(devtype), &devtype, NULL);
    oclCheckErr(err, "deviceInfo");
    switch (devtype) {
    case CL_DEVICE_TYPE_GPU:
      nbgpu++;
      break;
    }
  }
  return nbgpu;
}


int
oclGetNbOfCpu(const int theplatform)
{
  int j, nbcpu = 0;
  cl_int err = 0;
  cl_device_type devtype;

  assert(pdesc != NULL);
  assert(pdesc[theplatform].devices != NULL);

  for (j = 0; j < pdesc[theplatform].nbdevices; j++) {
    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_TYPE, sizeof(devtype), &devtype, NULL);
    oclCheckErr(err, "deviceInfo");
    switch (devtype) {
    case CL_DEVICE_TYPE_CPU:
      nbcpu++;
      break;
    }
  }
  return nbcpu;
}

int
oclGetGpuDev(const int theplatform, const int gpunum)
{
  int j, nbgpu = 0, numdev = -1;
  cl_int err = 0;
  cl_device_type devtype;

  assert(pdesc != NULL);
  assert(pdesc[theplatform].devices != NULL);

  for (j = 0; j < pdesc[theplatform].nbdevices; j++) {
    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_TYPE, sizeof(devtype), &devtype, NULL);
    oclCheckErr(err, "deviceInfo");
    switch (devtype) {
    case CL_DEVICE_TYPE_GPU:
      if (gpunum == nbgpu) {
	numdev = j;
      }
      nbgpu++;
      break;
    }
  }
  return numdev;
}

int
oclGetCpuDev(const int theplatform, const int cpunum)
{
  int j, nbcpu = 0, numdev = -1;
  cl_int err = 0;
  cl_device_type devtype;

  assert(pdesc != NULL);
  assert(pdesc[theplatform].devices != NULL);

  for (j = 0; j < pdesc[theplatform].nbdevices; j++) {
    err = clGetDeviceInfo(pdesc[theplatform].devices[j], CL_DEVICE_TYPE, sizeof(devtype), &devtype, NULL);
    oclCheckErr(err, "deviceInfo");
    switch (devtype) {
    case CL_DEVICE_TYPE_CPU:
      if (cpunum == nbcpu) {
	numdev = j;
      }
      nbcpu++;
      break;
    }
  }
  return numdev;
}

cl_device_id
oclGetDeviceOfCQueue(cl_command_queue q)
{
  cl_device_id res;
  cl_int err = 0;
  size_t lres = 0;
  err = clGetCommandQueueInfo(q, CL_QUEUE_DEVICE, sizeof(cl_device_id), &res, &lres);
  oclCheckErr(err, "clGetCommandQueueInfo qDev");
  return res;
}

cl_context
oclGetContextOfCQueue(cl_command_queue q)
{
  cl_context res;
  cl_int err = 0;
  size_t lres = 0;
  err = clGetCommandQueueInfo(q, CL_QUEUE_CONTEXT, sizeof(cl_context), &res, &lres);
  oclCheckErr(err, "clGetCommandQueueInfo qCtx");
  return res;
}

size_t
oclGetMaxWorkSize(cl_kernel k, cl_device_id d)
{
  cl_int err = 0;
  size_t lres = 0;
  size_t res;

  err = clGetKernelWorkGroupInfo(k, d, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &res, &lres);
  oclCheckErr(err, "clGetCommandQueueInfo qCtx");
  return res;
}


size_t
oclGetMaxMemAllocSize(int theplatform, int thedev)
{
  assert(pdesc != NULL);
  assert(pdesc[theplatform].devices != NULL);
  assert(pdesc[theplatform].devices[thedev] != NULL);
  return pdesc[theplatform].devdesc[thedev].mmas;
}

void
oclSetArg(cl_kernel k, cl_uint narg, size_t l, const void *arg)
{
  cl_int err = 0;
  err = clSetKernelArg(k, narg, l, arg);
  oclCheckErr(err, "clSetKernelArg");
}

void
oclNbBlocks(cl_kernel k, cl_command_queue q, size_t nbobj, int nbthread, long *maxth, long *nbblocks)
{
  dim3 gws, lws;
  int maxThreads = 0;
  maxThreads = oclGetMaxWorkSize(k, oclGetDeviceOfCQueue(q));
  maxThreads = MIN(maxThreads, nbthread);

  oclMkNDrange(nbobj, maxThreads, NDR_1D, gws, lws);
  *maxth = maxThreads;
  *nbblocks = (gws[0] * gws[1] * gws[2]) / maxThreads;
  return;
}

double
oclLaunchKernel(cl_kernel k, cl_command_queue q, size_t nbobj, int nbthread)
{
  cl_int err = 0;
  dim3 gws, lws;
  cl_event event;
  double elapsk;
  int maxThreads = 0;

  maxThreads = oclGetMaxWorkSize(k, oclGetDeviceOfCQueue(q));
  maxThreads = MIN(maxThreads, nbthread);

  oclMkNDrange(nbobj, maxThreads, NDR_1D, gws, lws);
  // printf("Launch: %ld G:%ld %ld %ld L:%ld %ld %ld\n", nbobj, gws[0], gws[1], gws[2], lws[0], lws[1], lws[2]);

  err = clEnqueueNDRangeKernel(q, k, NDR_1D, NULL, gws, lws, 0, NULL, &event);
  oclCheckErr(err, "clEnqueueNDRangeKernel");

  err = clWaitForEvents(1, &event);
  oclCheckErr(err, "clWaitForEvents");

  elapsk = oclChronoElaps(event);

  err = clReleaseEvent(event);
  oclCheckErr(err, "clReleaseEvent");

  return elapsk;
}

//EOF
