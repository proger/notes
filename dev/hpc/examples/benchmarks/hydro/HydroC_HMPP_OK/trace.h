#ifndef TRACE_H_INCLUDED
#define TRACE_H_INCLUDED

#include "hmpp.h"

#pragma hmpp <HGgodunov> Htrace codelet, &
#pragma hmpp <HGgodunov>       args[qxm;qxp].io=out, &
#pragma hmpp <HGgodunov>           args[0-4].size={(Hnxyt + 2) * Hnvar}, &

#pragma hmpp <HGgodunov>           args[6-9].const=true

void trace(double *RESTRICT q, double *RESTRICT dq,
           double *RESTRICT c, double *RESTRICT qxm,
           double *RESTRICT qxp, const double dtdx, int n, const int Hscheme,
           const int Hnvar, const int Hnxyt);

#endif // TRACE_H_INCLUDED
