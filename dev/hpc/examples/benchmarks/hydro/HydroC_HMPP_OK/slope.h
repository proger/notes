#ifndef SLOPE_H_INCLUDED
#define SLOPE_H_INCLUDED

#include "hmpp.h"

#pragma hmpp <HGgodunov> Hslope codelet, &
#pragma hmpp <HGgodunov>           args[0-1].size = {(Hnxyt + 2) * Hnvar}, &
#pragma hmpp <HGgodunov>            args[dq].io = inout, &
#pragma hmpp <HGgodunov>           args[2-5].const=true

void slope(double *RESTRICT q, double *RESTRICT dq, int n,
           const int Hnvar, const int Hnxyt, const double Hslope_type);

#endif // SLOPE_H_INCLUDED
