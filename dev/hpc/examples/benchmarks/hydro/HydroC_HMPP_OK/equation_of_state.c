/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

// #include <stdlib.h>
// #include <unistd.h>
#include <math.h>
#include <stdio.h>

#ifndef HMPP
#include "equation_of_state.h"
#include "parametres.h"
#include "utils.h"

#define CFLOPS(c) /* {flops+=c;} */

#define IHVW(i,v) ((i) + (v) * Hnxyt)

void
equation_of_state(double *RESTRICT eint,
                  double *RESTRICT q,
                  double *RESTRICT c,
                  int imin, int imax,
                  const int Hnxyt, const int Hnvar,
                  const double Hsmallc, const double Hgamma)
{
    int k;
    double smallp;

    WHERE("equation_of_state");
    smallp = Square(Hsmallc) / Hgamma;
    CFLOPS(1);

#pragma hmppcg parallel
    for (k = imin; k < imax; k++) {
      double rhok = q[IHVW(k, ID)];
      double base = (Hgamma - one) * rhok * eint[k];
      base = MAX(base, (double) (rhok * smallp));

      q[IHVW(k, IP)] = base;
      c[k] = sqrt(Hgamma * base / rhok);

      CFLOPS(7);
    }
}                              // equation_of_state


#endif
// EOF
