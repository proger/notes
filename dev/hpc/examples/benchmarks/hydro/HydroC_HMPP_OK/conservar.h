#ifndef CONSERVAR_H_INCLUDED
#define CONSERVAR_H_INCLUDED

#pragma hmpp <HGgodunov> HgatherConservativeVars codelet, &
#pragma hmpp <HGgodunov>               args[u].size={(Hnxyt + 2) * Hnvar}, &
#pragma hmpp <HGgodunov>            args[uold].size={Hnxt * Hnyt * Hnvar}, &

#pragma hmpp <HGgodunov>               args[u].io=out, &
#pragma hmpp <HGgodunov>            args[0;4-11].const=true

void gatherConservativeVars(const int idim, int rowcol,
                            double *RESTRICT uold,
                            double *RESTRICT u,
                            const int Himin,
                            const int Himax,
                            const int Hjmin,
                            const int Hjmax,
                            const int Hnvar,
                            const int Hnxt, const int Hnyt, const int Hnxyt);

#pragma hmpp <HGgodunov> HupdateConservativeVars codelet, &
#pragma hmpp <HGgodunov>         args[u; flux].size={(Hnxyt + 2) * Hnvar}, &
#pragma hmpp <HGgodunov>            args[uold].size={Hnxt * Hnyt * Hnvar}, &

#pragma hmpp <HGgodunov>            args[uold].io=inout, &
#pragma hmpp <HGgodunov>            args[0;6-13].const=true

void updateConservativeVars(const int idim, int rowcol, const double dtdx,
                            double *RESTRICT uold,
                            double *RESTRICT u,
                            double *RESTRICT flux,
                            const int Himin,
                            const int Himax,
                            const int Hjmin,
                            const int Hjmax,
                            const int Hnvar,
                            const int Hnxt, const int Hnyt, const int Hnxyt);

#endif // CONSERVAR_H_INCLUDED
