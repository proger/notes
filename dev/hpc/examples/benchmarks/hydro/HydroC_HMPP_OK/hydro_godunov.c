/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>

#include "hmpp.h"
#include "parametres.h"
#include "hydro_godunov.h"
#include "hydro_funcs.h"
#include "utils.h"
#include "make_boundary.h"

#include "riemann.h"
#include "qleftright.h"
#include "trace.h"
#include "slope.h"
#include "equation_of_state.h"
#include "constoprim.h"

#include "cmpflx.h"
#include "conservar.h"

// variables auxiliaires pour mettre en place le mode resident de HMPP
void
hydro_godunov(int idimStart, double dt, const hydroparam_t H, hydrovar_t * Hv,
              hydrowork_t * Hw, hydrovarwork_t * Hvw)
{

  // Local variables
  int i, j;
  double dtdx;

  double *dq;
  double *e;
  double *flux;
  double *q;
  double *qleft, *qright;
  double *qxm, *qxp;
  double *u;
  double *c;
  double *uold;
  double *sgnm;
  double *qgdnv;

  WHERE("hydro_godunov");

  int hmppGuard = 1;
  int idimIndex = 0;
  for (idimIndex = 0; idimIndex < 2; idimIndex++) {
    int idim = (idimStart-1 + idimIndex) % 2 + 1;
    // constant
    dtdx = dt / H.dx;

    // Update boundary conditions
    if (H.prt) {
      fprintf(stdout, "godunov %d\n", idim);
      PRINTUOLD(H, Hv);
    }
    make_boundary(idim, H, Hv);
    PRINTUOLD(H, Hv);

    // Allocate work space for 1D sweeps
    allocate_work_space(H.nxyt, H, Hw, Hvw);
    uold = Hv->uold;
    qgdnv = Hvw->qgdnv;
    flux = Hvw->flux;
    c = Hw->c;
    q = Hvw->q;
    e = Hw->e;
    u = Hvw->u;
    qxm = Hvw->qxm;
    qxp = Hvw->qxp;
    qleft = Hvw->qleft;
    qright = Hvw->qright;
    dq = Hvw->dq;
    sgnm = Hw->sgnm;


    int Hmin, Hmax;
    int Hdimsize;
    int Hndim_1;

    if (idim == 1) {
      Hmin = H.jmin + ExtraLayer;
      Hmax = H.jmax - ExtraLayer;
      Hdimsize = H.nxt;
      Hndim_1 = H.nx + 1;
    }
    else {
      Hmin = H.imin + ExtraLayer;
      Hmax = H.imax - ExtraLayer;
      Hdimsize = H.nyt;
      Hndim_1 = H.ny + 1;
    }

    if (!H.nstep && idim == 1) {		/* LM -- HERE a more secure implementation should be used: a new parameter ? */
#pragma hmpp <HGgodunov> allocate
#if 1
#pragma hmpp <HGgodunov> Hriemann	  advancedload, args[4-10]			/* Buffers and constant Values */
#pragma hmpp <HGgodunov> Hqleftright	  advancedload, args[Hnx;Hny;Hnxyt;Hnvar]	/* Constant Scalars */
#pragma hmpp <HGgodunov> Htrace		  advancedload, args[Hscheme;n;dtdx]
#pragma hmpp <HGgodunov> Hslope		  advancedload, args[Hslope_type;n]
#pragma hmpp <HGgodunov> HequationOfState advancedload, args[imin;imax;Hsmallc;Hgamma]
#pragma hmpp <HGgodunov> Hconstoprim	  advancedload, args[n;Hsmallr]
#pragma hmpp <HGgodunov> Hcmpflx	  advancedload, args[narray;Hgamma]
#pragma hmpp <HGgodunov> HgatherConservativeVars	 advancedload, args[4-11]
#pragma hmpp <HGgodunov> HupdateConservativeVars	 advancedload, args[6-13]
#endif

#pragma hmpp <HGgodunov> HgatherConservativeVars advancedload, args[uold]
    }

#pragma hmpp <HGgodunov> Hqleftright advancedload, args[idim]
#pragma hmpp <HGgodunov> Htrace advancedload, args[dtdx]

    for (j = Hmin; j < Hmax; j++) {
    
      // fprintf(stderr, "Godunov idim=%d, j=%d\n", idim, j);

#pragma hmpp <HGgodunov> HgatherConservativeVars callsite, args[4-11].noupdate=true, args[u;uold].noupdate=true, args[idim].advancedload=true
      gatherConservativeVars(idim, j, uold, u, H.imin, H.imax, H.jmin,
			     H.jmax, H.nvar, H.nxt, H.nyt, H.nxyt);
      PRINTARRAYV(u, Hdimsize, "u", H);

#pragma hmpp <HGgodunov> HDmemset callsite, args[0-2].noupdate=true
      Dmemset(dq, 0, (H.nxyt+2) * H.nvar);

      // Convert to primitive variables
#pragma hmpp <HGgodunov> Hconstoprim callsite, args[3-6].noupdate=true, args[e;q;u].noupdate=true
      constoprim(u, q, e, Hdimsize, H.nxyt, H.nvar, H.smallr);
      PRINTARRAY(e, Hdimsize, "e", H);
      PRINTARRAYV(q, Hdimsize, "q", H);

#pragma hmpp <HGgodunov> HequationOfState callsite, args[3-8].noupdate=true, args[eint;q;c].noupdate=true
      equation_of_state(e, q, c, 0, Hdimsize, H.nxyt, H.nvar, H.smallc, H.gamma);
      PRINTARRAY(c, Hdimsize, "c", H);

      // Characteristic tracing
      if (H.iorder != 1) {
#pragma hmpp <HGgodunov> Hslope callsite, args[2-5;q;dq].noupdate=true
	slope(q, dq, Hdimsize, H.nvar, H.nxyt, H.slope_type);
	PRINTARRAYV(dq, Hdimsize, "dq", H);
      }

#pragma hmpp <HGgodunov> Htrace callsite, args[6-9;q;c;dq;qxm;qxp].noupdate=true, args[dtdx].advancedload=true
      memset(qxm, 0, H.nvar * H.nxyt * sizeof(double));
      memset(qxp, 0, H.nvar * H.nxyt * sizeof(double));
      trace(q, dq, c, qxm, qxp, dtdx, Hdimsize, H.scheme, H.nvar, H.nxyt);
      PRINTARRAYV(qxm, Hdimsize, "qxm", H);
      PRINTARRAYV(qxp, Hdimsize, "qxp", H);

#pragma hmpp <HGgodunov> Hqleftright callsite, args[0-4].noupdate=true, args[qxm; qxp; qleft; qright].noupdate=true
      qleftright(idim, H.nx, H.ny, H.nxyt, H.nvar, qxm, qxp, qleft, qright);
      PRINTARRAYV(qleft, (idim==1)?H.nx + 1:H.ny + 1, "qleft", H);
      PRINTARRAYV(qright, (idim==1)?H.nx + 1:H.ny + 1, "qright", H);

#pragma hmpp <HGgodunov> Hriemann callsite, args[3-10].noupdate=true, args[qleft; qright; qgdnv].noupdate=true
      riemann(qleft, qright, qgdnv, sgnm,
	      Hndim_1, H.smallr, H.smallc, H.gamma, H.niter_riemann,
	      H.nvar, H.nxyt);

#pragma hmpp <HGgodunov> Hcmpflx callsite, args[2-5].noupdate=true, args[qgdnv; flux].noupdate=true
      cmpflx(qgdnv, flux, Hdimsize, H.nxyt, H.nvar, H.gamma);
      PRINTARRAYV(flux, (idim==1)?H.nx + 1:H.ny + 1, "flux", H);

#pragma hmpp <HGgodunov> HupdateConservativeVars callsite, args[6-13].noupdate=true, &
#pragma hmpp <HGgodunov>            args[flux;u;uold;rowcol].noupdate=true, args[dtdx;idim].advancedload=true
      updateConservativeVars(idim, j, dtdx, uold, u, flux, H.imin,
			     H.imax, H.jmin, H.jmax, H.nvar, H.nxt, H.nyt, H.nxyt);

    }                       // for j

    if (H.prt) {
      printf("After pass %d\n", idim);
      PRINTUOLD(H, Hv);
    }
  }
  // Deallocate work space
  deallocate_work_space(H, Hw, Hvw);

  

#pragma hmpp <HGgodunov> HupdateConservativeVars delegatedstore 	args[uold]
  if ((H.t+dt >= H.tend) || (H.nstep+1 >= H.nstepmax)) {		/* LM -- HERE a more secure implementation should be used: a new parameter ? */
#pragma hmpp <HGgodunov> release
  }

}                               // hydro_godunov


// EOF
