/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "slope.h"

#ifndef HMPP

#define DABS(x) (double) fabs((x))

void
slope(double *RESTRICT q, double *RESTRICT dq, int n,
      const int Hnvar, const int Hnxyt, const double Hslope_type)
{
    int nbv, i, ijmin, ijmax;
    double dlft, drgt, dcen, dsgn, slop, dlim;
    long ihvwin, ihvwimn, ihvwipn;
#define IHVW(i, v) ((i) + (v) * Hnxyt)

    WHERE("slope");
    ijmin = 0;
    ijmax = n;

    #pragma hmppcg parallel
    for (nbv = 0; nbv < Hnvar; nbv++) {
      #pragma hmppcg parallel
      for (i = ijmin + 1; i < ijmax - 1; i++) {
	ihvwin = IHVW(i, nbv);
	ihvwimn = IHVW(i-1, nbv);
	ihvwipn = IHVW(i+1, nbv);
	dlft = Hslope_type * (q[ihvwin] - q[ihvwimn]);
	drgt = Hslope_type * (q[ihvwipn] - q[ihvwin]);
	dcen = half * (dlft + drgt) / Hslope_type;
	dsgn = (dcen > 0) ? (double) 1.0 : (double) -1.0;     // sign(one, dcen);
	slop = (double) MIN(DABS(dlft), DABS(drgt));
	dlim = slop;
	if ((dlft * drgt) <= zero) {
	  dlim = zero;
	}
	dq[ihvwin] = dsgn * (double) MIN(dlim, DABS(dcen));
#ifdef FLOPS
	flops += 8;
#endif
      }
    }
}                               // slope

#undef IHVW

#endif /* HMPP */
//EOF
