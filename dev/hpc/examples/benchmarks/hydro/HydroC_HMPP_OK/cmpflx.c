/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <math.h>
#include <malloc.h>
// #include <unistd.h>
// #include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifndef HMPP
#include "parametres.h"
#include "utils.h"
#include "cmpflx.h"

#define CFLOPS(c) /* {flops+=c;} */

void
cmpflx(double *RESTRICT qgdnv, double *RESTRICT flux, const int narray,
       const int Hnxyt, const int Hnvar, const double Hgamma)
{
    int nface, i, IN;
    double entho, ekin, etot;
    const int nxyt = Hnxyt;
    WHERE("cmpflx");

#define IHVW(i,v) ((i) + (v) * nxyt)
    nface = narray;
    entho = one / (Hgamma - one);

    // Compute fluxes
#pragma hmppcg Parallel
    for (i = 0; i < nface; i++) {
      double qgdnvID = qgdnv[IHVW(i, ID)];
      double qgdnvIU = qgdnv[IHVW(i, IU)];
      double qgdnvIP = qgdnv[IHVW(i, IP)];
      double qgdnvIV = qgdnv[IHVW(i, IV)];

        // Mass density
      double massDensity = qgdnvID * qgdnvIU;
        flux[IHVW(i, ID)] = massDensity;

        // Normal momentum
        flux[IHVW(i, IU)] = massDensity * qgdnvIU + qgdnvIP;

        // Transverse momentum 1
        flux[IHVW(i, IV)] = massDensity * qgdnvIV;

        // Total energy
        ekin = half * qgdnvID * (Square(qgdnvIU) + Square(qgdnvIV));
        etot = qgdnvIP * entho + ekin;

        flux[IHVW(i, IP)] = qgdnvIU * (etot + qgdnvIP);

        CFLOPS(15);
    }

    // Other advected quantities
    if (Hnvar > IP) {

#pragma hmppcg Parallel
        for (IN = IP + 1; IN < Hnvar; IN++) {

#pragma hmppcg Parallel
            for (i = 0; i < nface; i++) {
                flux[IHVW(i, IN)] = flux[IHVW(i, IN)] * qgdnv[IHVW(i, IN)];
            }
        }
    }
}                               // cmpflx


#undef IHVW
#endif

//EOF
