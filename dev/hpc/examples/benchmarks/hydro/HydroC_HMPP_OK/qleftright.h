#ifndef QLEFTRIGHT_H_INCLUDED
#define QLEFTRIGHT_H_INCLUDED

#include "hmpp.h"

#pragma hmpp <HGgodunov> Hqleftright codelet, &
#pragma hmpp <HGgodunov>           args[0-4].const=true, &
#pragma hmpp <HGgodunov>           args[5-8].size={(Hnxyt+2)*Hnvar}, &
#pragma hmpp <HGgodunov> args[qleft, qright].io=out

void
qleftright(const int idim, const int Hnx, const int Hny, const int Hnxyt, const int Hnvar,
           double *RESTRICT qxm, double *RESTRICT qxp,
           double *RESTRICT qleft, double *RESTRICT qright);

#endif
