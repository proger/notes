/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdio.h>
// #include <stdlib.h>
#include <malloc.h>
// #include <unistd.h>
#include <math.h>

#ifdef HMPP
#undef HMPP
#endif

#include "parametres.h"
#include "compute_deltat.h"
#include "utils.h"
#include "equation_of_state.h"

#define DABS(x) (double) fabs((x))

void
ComputeQEforRow(const int j, double *uold, double *q, double *e,
                const double Hsmallr, const int Hnx, const int Hnxt,
                const int Hnyt, const int nxyt)
{
    int i;
    double eken;

#define IHV(i, j, v)  ((i) + Hnxt * ((j) + Hnyt * (v)))
#define IHVW(i, v) ((i) + (v) * nxyt)
    for (i = 0; i < Hnx; i++) {
        int idxuID = IHV(i + ExtraLayer, j, ID);
        int idxuIU = IHV(i + ExtraLayer, j, IU);
        int idxuIV = IHV(i + ExtraLayer, j, IV);
        int idxuIP = IHV(i + ExtraLayer, j, IP);
        q[IHVW(i, ID)] = MAX(uold[idxuID], Hsmallr);
        q[IHVW(i, IU)] = uold[idxuIU] / q[IHVW(i, ID)];
        q[IHVW(i, IV)] = uold[idxuIV] / q[IHVW(i, ID)];
        eken = half * (Square(q[IHVW(i, IU)]) + Square(q[IHVW(i, IV)]));
        q[IHVW(i, IP)] = uold[idxuIP] / q[IHVW(i, ID)] - eken;
        e[i] = q[IHVW(i, IP)];
    }
#undef IHV
#undef IHVW
} void
courantOnXY(double *cournox, double *cournoy, const int Hnx, const int nxyt,
            double *c, double *q)
{
    int i;
    double maxValC = zero;
    double tmp1, tmp2;

#define IHVW(i,v) ((i) + (v) * nxyt)
//     maxValC = c[0];
//     for (i = 0; i < Hnx; i++) {
//         maxValC = MAX(maxValC, c[i]);
//     }
//     for (i = 0; i < Hnx; i++) {
//         *cournox = MAX(*cournox, maxValC + DABS(q[IHVW(i, IU)]));
//         *cournoy = MAX(*cournoy, maxValC + DABS(q[IHVW(i, IV)]));
//     }
    for (i = 0; i < Hnx; i++) {
      tmp1 = c[i] + DABS(q[IHVW(i, IU)]);
      tmp2 = c[i] + DABS(q[IHVW(i, IV)]);
      *cournox = MAX(*cournox, tmp1);
      *cournoy = MAX(*cournoy, tmp2);
    }

#undef IHVW
}
void
compute_deltat(double *dt, const hydroparam_t H, hydrowork_t * Hw,
               hydrovar_t * Hv, hydrovarwork_t * Hvw)
{
    double cournox, cournoy;
    int j;
    WHERE("compute_deltat");

#define IHVW(i,v) ((i) + (v) * nxyt)

    //   compute time step on grid interior
    cournox = zero;
    cournoy = zero;
    Hvw->q = (double *) calloc(H.nvar * H.nxyt, sizeof(double));
    Hw->e = (double *) malloc(H.nx * sizeof(double));
    Hw->c = (double *) malloc(H.nx * sizeof(double));
    for (j = H.jmin + ExtraLayer; j < H.jmax - ExtraLayer; j++) {
        ComputeQEforRow(j, Hv->uold, Hvw->q, Hw->e, H.smallr, H.nx, H.nxt, H.nyt, H.nxyt);

        equation_of_state(Hw->e,
                          Hvw->q, Hw->c, 0, H.nx, H.nxyt, H.nvar, H.smallc, H.gamma);
        courantOnXY(&cournox, &cournoy, H.nx, H.nxyt, Hw->c, Hvw->q);

#ifdef FLOPS
        flops += 10;

#endif /*  */
    }
    Free(Hvw->q);
    Free(Hw->e);
    Free(Hw->c);
    *dt = H.courant_factor * H.dx / MAX(cournox, MAX(cournoy, H.smallc));

#ifdef FLOPS
    flops += 2;

#endif /*  */

    // fprintf(stdout, "%g %g %g %g\n", cournox, cournoy, H.smallc, H.courant_factor);
#undef IHVW
}                               // compute_deltat

//EOF
