#ifndef RIEMANN_H_INCLUDED
#define RIEMANN_H_INCLUDED

#include "hmpp.h"

#pragma hmpp <HGgodunov> Hriemann codelet, &
#pragma hmpp <HGgodunov>           args[0-2].size={(Hnxyt + 2) * Hnvar}, &
#pragma hmpp <HGgodunov>             args[3].size={narray}, &

#pragma hmpp <HGgodunov> args[qleft, qright].io=inout, args[qgdnv].io=out, &
#pragma hmpp <HGgodunov>             args[3].io=out, &

#pragma hmpp <HGgodunov>         args[3-10].const=true


void riemann(double *RESTRICT qleft, double *RESTRICT qright, double *RESTRICT qgdnv,	/* 0-2    */
             double *RESTRICT sgnm,							/* 3 */
             int narray,								/* 4-10 */
             const double Hsmallr,
             const double Hsmallc,
             const double Hgamma, const int Hniter_riemann, const int Hnvar,
             const int Hnxyt);


#pragma hmpp <HGgodunov> HDmemset codelet, &
#pragma hmpp <HGgodunov>          args[t].size={nbr}, &
#pragma hmpp <HGgodunov>          args[t].io=out, &
#pragma hmpp <HGgodunov> args[motif; nbr].const=true
void
Dmemset(double *t, double motif, size_t nbr);

#endif // RIEMANN_H_INCLUDED
