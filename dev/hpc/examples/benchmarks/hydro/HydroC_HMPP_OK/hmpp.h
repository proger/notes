#ifndef HMPP_H_INCLUDED
#define HMPP_H_INCLUDED

#pragma hmpp <HGgodunov> group, target=CUDA, cond="hmppGuard"
#pragma hmpp <HGgodunov> mapbyname, qleft, qright
#pragma hmpp <HGgodunov> mapbyname, qxm, qxp
#pragma hmpp <HGgodunov> mapbyname, q, dq
#pragma hmpp <HGgodunov> mapbyname, Hnxyt, Hnvar
#pragma hmpp <HGgodunov> mapbyname, c
#pragma hmpp <HGgodunov> mapbyname, qgdnv
#pragma hmpp <HGgodunov> mapbyname, flux, uold, u
#pragma hmpp <HGgodunov> mapbyname, dtdx, idim, rowcol
#pragma hmpp <HGgodunov> map, args[Hconstoprim::e; HequationOfState::eint]

#include "constoprim.h"
#include "equation_of_state.h"
#include "slope.h"
#include "trace.h"
#include "qleftright.h"
#include "riemann.h"
#include "cmpflx.h"
#include "conservar.h"

#endif // HMPP_H_INCLUDED
