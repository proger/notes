#ifndef CONSTOPRIM_H_INCLUDED
#define CONSTOPRIM_H_INCLUDED

#include "utils.h"

#pragma hmpp <HGgodunov> Hconstoprim codelet, &
#pragma hmpp <HGgodunov>             args[0-1].size={(Hnxyt + 2) * Hnvar}, &
#pragma hmpp <HGgodunov>               args[2].size={n}, &

#pragma hmpp <HGgodunov>            args[q; e].io=out, &
#pragma hmpp <HGgodunov>             args[3-6].const=true

void constoprim(double *RESTRICT u, double *RESTRICT q, double *RESTRICT e,
                const int n, const int Hnxyt, const int Hnvar, const double Hsmallr);

#endif // CONSTOPRIM_H_INCLUDED
