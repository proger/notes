#ifndef CMPFLX_H_INCLUDED
#define CMPFLX_H_INCLUDED

#include "utils.h"

#pragma hmpp <HGgodunov> Hcmpflx codelet, &
#pragma hmpp <HGgodunov>     args[qgdnv; flux].size={(Hnxyt + 2) * Hnvar}, &

#pragma hmpp <HGgodunov>            args[flux].io=out, &
#pragma hmpp <HGgodunov>             args[2-5].const=true

void cmpflx(double *RESTRICT qgdnv, double *RESTRICT flux,
            const int narray, const int Hnxyt, const int Hnvar, const double Hgamma);

#endif // CMPFLX_H_INCLUDED
