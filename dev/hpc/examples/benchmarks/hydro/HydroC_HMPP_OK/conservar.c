/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#ifndef HMPP
#include "parametres.h"
#include "utils.h"
#include "conservar.h"

#define CFLOPS(c) /* {flops+=c;} */

void
gatherConservativeVars(const int idim, int rowcol,
                       double *RESTRICT uold,
                       double *RESTRICT u,
                       const int Himin,
                       const int Himax,
                       const int Hjmin,
                       const int Hjmax,
                       const int Hnvar, const int Hnxt, const int Hnyt, const int Hnxyt)
{
    int i, j, ivar;

#define IHU(i, j, v)  ((i) + Hnxt * ((j) + Hnyt * (v)))
#define IHVW(i, v) ((i) + (v) * Hnxyt)

    WHERE("gatherConservativeVars");
    if (idim == 1) {
        // Gather conservative variables
#pragma hmppcg parallel
        for (i = Himin; i < Himax; i++) {
            int idxuoID = IHU(i, rowcol, ID);
            u[IHVW(i, ID)] = uold[idxuoID];

            int idxuoIU = IHU(i, rowcol, IU);
            u[IHVW(i, IU)] = uold[idxuoIU];

            int idxuoIV = IHU(i, rowcol, IV);
            u[IHVW(i, IV)] = uold[idxuoIV];

            int idxuoIP = IHU(i, rowcol, IP);
            u[IHVW(i, IP)] = uold[idxuoIP];
        }

        if (Hnvar > IP) {
#pragma hmppcg parallel
            for (ivar = IP + 1; ivar < Hnvar; ivar++) {
#pragma hmppcg parallel
                for (i = Himin; i < Himax; i++) {
                    u[IHVW(i, ivar)] = uold[IHU(i, rowcol, ivar)];
                }
            }
        }
    } else {
        // Gather conservative variables
#pragma hmppcg parallel
      for (j = Hjmin; j < Hjmax; j++) {
            u[IHVW(j, ID)] = uold[IHU(rowcol, j, ID)];
            u[IHVW(j, IU)] = uold[IHU(rowcol, j, IV)];
            u[IHVW(j, IV)] = uold[IHU(rowcol, j, IU)];
            u[IHVW(j, IP)] = uold[IHU(rowcol, j, IP)];
        }
        if (Hnvar > IP) {
#pragma hmppcg parallel
            for (ivar = IP + 1; ivar < Hnvar; ivar++) {
#pragma hmppcg parallel
                for (j = Hjmin; j < Hjmax; j++) {
                    u[IHVW(j, ivar)] = uold[IHU(rowcol, j, ivar)];
                }
            }
        }
    }
}

#undef IHVW
#undef IHU

void
updateConservativeVars(const int idim, int rowcol, const double dtdx,
                       double *RESTRICT uold,
                       double *RESTRICT u,
                       double *RESTRICT flux,
                       const int Himin,
                       const int Himax,
                       const int Hjmin,
                       const int Hjmax,
                       const int Hnvar, const int Hnxt, const int Hnyt, const int Hnxyt)
{
    int i, j, ivar;
    WHERE("updateConservativeVars");

#define IHU(i, j, v)  ((i) + Hnxt * ((j) + Hnyt * (v)))
#define IHVW(i, v) ((i) + (v) * Hnxyt)

    if (idim == 1) {

        // Update conservative variables
#pragma hmppcg parallel
      for(ivar = 0; ivar <= IP; ivar++) {
#pragma hmppcg parallel
        for (i = Himin + 2; i < Himax - 2; i++) {
          uold[IHU(i, rowcol, ivar)] = u[IHVW(i, ivar)] + (flux[IHVW(i - 2, ivar)] - flux[IHVW(i - 1, ivar)]) * dtdx;
          CFLOPS(3);
	}
      }

      if (Hnvar > IP) {
#pragma hmppcg parallel
        for (ivar = IP + 1; ivar < Hnvar; ivar++) {
#pragma hmppcg parallel
          for (i = Himin + ExtraLayer; i < Himax - ExtraLayer; i++) {
            uold[IHU(i, rowcol, ivar)] = u[IHVW(i, ivar)] + (flux[IHVW(i - 2, ivar)] - flux[IHVW(i - 1, ivar)]) * dtdx;
            CFLOPS(3);
          }
        }
      }
    }
    else
      {
        // Update conservative variables
#pragma hmppcg parallel
        for (j = Hjmin + ExtraLayer; j < Hjmax - ExtraLayer; j++) {
          uold[IHU(rowcol, j, ID)] = u[IHVW(j, ID)] + (flux[IHVW(j - 2, ID)] - flux[IHVW(j - 1, ID)]) * dtdx;
          CFLOPS(3);

          uold[IHU(rowcol, j, IV)] = u[IHVW(j, IU)] + (flux[IHVW(j - 2, IU)] - flux[IHVW(j - 1, IU)]) * dtdx;
          CFLOPS(3);

          uold[IHU(rowcol, j, IU)] = u[IHVW(j, IV)] + (flux[IHVW(j - 2, IV)] - flux[IHVW(j - 1, IV)]) * dtdx;
          CFLOPS(3);

          uold[IHU(rowcol, j, IP)] = u[IHVW(j, IP)] + (flux[IHVW(j - 2, IP)] - flux[IHVW(j - 1, IP)]) * dtdx;
          CFLOPS(3);
        }

        if (Hnvar > IP) {
#pragma hmppcg parallel
          for (ivar = IP + 1; ivar < Hnvar; ivar++) {
#pragma hmppcg parallel
            for (j = Hjmin + ExtraLayer; j < Hjmax - ExtraLayer; j++) {
              uold[IHU(rowcol, j, ivar)] =
                u[IHVW(j, ivar)] + (flux[IHVW(j - 2, ivar)] -
                                    flux[IHVW(j - 1, ivar)]) * dtdx;
              CFLOPS(3);
            }
          }
        }
      }
}

#undef IHVW
#undef IHU
#endif
//EOF
