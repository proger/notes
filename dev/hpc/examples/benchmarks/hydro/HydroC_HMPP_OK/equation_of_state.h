#ifndef EQUATION_OF_STATE_H_INCLUDED
#define EQUATION_OF_STATE_H_INCLUDED

#include "utils.h"
#include "parametres.h"

#ifdef HMPP
#pragma hmpp <HGgodunov> HequationOfState codelet, &
#pragma hmpp <HGgodunov>               args[q].size={(Hnxyt+2)*Hnvar}, &
#pragma hmpp <HGgodunov>          args[eint;c].size={imax - imin}, &

#pragma hmpp <HGgodunov>               args[q].io=inout, &
#pragma hmpp <HGgodunov>               args[c].io=out, &
#pragma hmpp <HGgodunov>             args[3-8].const=true
#endif

void equation_of_state(double *RESTRICT eint, double *RESTRICT q,	/* 0 ...  */
                       double *RESTRICT c,				/*   ... 2 */
                       int imin, int imax,				/* 3 ... */
                       const int Hnxyt, const int Hnvar,
                       const double Hsmallc, const double Hgamma);	/*   ... 8 */

#endif // EQUATION_OF_STATE_H_INCLUDED
