#ifndef CUHYDROGODUNOV_H
#define CUHYDROGODUNOV_H
#include <CL/cl.h>

#include "parametres.h"


#ifdef __cplusplus
extern "C" {
#endif
  void oclHydroGodunov(long idim, double dt, const hydroparam_t H, hydrovar_t * Hv, hydrowork_t * Hw, hydrovarwork_t * Hvw);
  void oclFreeOnDevice();
  void oclAllocOnDevice(const hydroparam_t H);
  void oclPutUoldOnDevice(const hydroparam_t H, hydrovar_t * Hv);
  void oclGetUoldFromDevice(const hydroparam_t H, hydrovar_t * Hv);
#ifdef __cplusplus
};
#endif

void oclGetUoldQECDevicePtr(cl_mem * uoldDEV, cl_mem * qDEV, cl_mem * eDEV, cl_mem * cDEV);
#endif
