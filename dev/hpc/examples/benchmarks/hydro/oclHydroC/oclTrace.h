#ifndef CUTRACE_H_INCLUDED
#define CUTRACE_H_INCLUDED

#include <CL/cl.h>

void oclTrace(cl_mem qDEV, cl_mem dqDEV, cl_mem cDEV, cl_mem qxmDEV, cl_mem qxpDEV,
              const double dtdx, const long n, const long Hscheme, const long Hnvar, const long Hnxyt);

#endif // TRACE_H_INCLUDED
