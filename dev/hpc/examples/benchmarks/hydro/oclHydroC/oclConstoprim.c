/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "oclConstoprim.h"
#include "oclInit.h"
#include "ocltools.h"

#define IHVW(i,v) ((i) + (v) * Hnxyt)

void
oclConstoprim(cl_mem u, cl_mem q, cl_mem e, const long n, const long Hnxyt, const long Hnvar, const double Hsmallr)
{

  WHERE("constoprim");
//   SetBlockDims(n, THREADSSZ, block, grid);
//   Loop1KcuConstoprim <<< grid, block >>> (n, u, q, e, Hnxyt, Hsmallr);
//   CheckErr("Loop1KcuConstoprim");
//   if (Hnvar > IP + 1) {
//     Loop2KcuConstoprim <<< grid, block >>> (n, u, q, Hnxyt, Hnvar);
//     CheckErr("Loop2KcuConstoprim");
//   }
//   cudaThreadSynchronize();
//   CheckErr("After synchronize cuConstoprim");

  OCLINITARG;
  OCLSETARG(ker[Loop1KcuConstoprim], n);
  OCLSETARG(ker[Loop1KcuConstoprim], u);
  OCLSETARG(ker[Loop1KcuConstoprim], q);
  OCLSETARG(ker[Loop1KcuConstoprim], e);
  OCLSETARG(ker[Loop1KcuConstoprim], Hnxyt);
  OCLSETARG(ker[Loop1KcuConstoprim], Hsmallr);
  oclLaunchKernel(ker[Loop1KcuConstoprim], cqueue, n, THREADSSZ);
  if (Hnvar > IP + 1) {
    OCLINITARG;
    OCLSETARG(ker[Loop2KcuConstoprim], n);
    OCLSETARG(ker[Loop2KcuConstoprim], u);
    OCLSETARG(ker[Loop2KcuConstoprim], q);
    OCLSETARG(ker[Loop2KcuConstoprim], Hnxyt);
    OCLSETARG(ker[Loop2KcuConstoprim], Hnvar);
    oclLaunchKernel(ker[Loop2KcuConstoprim], cqueue, n, THREADSSZ);
  }
}                               // constoprim


#undef IHVW
//EOF
