#include <CL/cl.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "oclInit.h"
#include "ocltools.h"

cl_command_queue cqueue = 0;
cl_context ctx = 0;
cl_program pgm = 0;
int devselected = 0;
int platformselected = 0;

cl_kernel *ker = NULL;

void
oclMemset(cl_mem a, cl_int v, size_t lbyte)
{
  int maxthr;
  size_t lgr;

  lgr = lbyte / sizeof(cl_double);
  OCLINITARG;
  OCLSETARG(ker[KernelMemset], a);
  OCLSETARG(ker[KernelMemset], v);
  OCLSETARG(ker[KernelMemset], lgr);    // en objets de type int
  maxthr = oclGetMaxWorkSize(ker[KernelMemset], oclGetDeviceOfCQueue(cqueue));
  if (lgr < maxthr)
    maxthr = lgr;
  oclLaunchKernel(ker[KernelMemset], cqueue, lgr, maxthr);
}

void
oclMemset4(cl_mem a, cl_int v, size_t lbyte)
{
  int maxthr;
  size_t lgr;

  // traitement vectoriel d'abord sous forme de int4
  lgr = lbyte / sizeof(cl_int) / 4;
  OCLINITARG;
  OCLSETARG(ker[KernelMemsetV4], a);
  OCLSETARG(ker[KernelMemsetV4], v);
  OCLSETARG(ker[KernelMemsetV4], lgr);  // en objets de type int4
  maxthr = oclGetMaxWorkSize(ker[KernelMemsetV4], oclGetDeviceOfCQueue(cqueue));
  if (lgr < maxthr)
    maxthr = lgr;
  oclLaunchKernel(ker[KernelMemsetV4], cqueue, lgr, maxthr);

  if ((lbyte - lgr * 4 * sizeof(cl_int)) > 0) {
    // traitement du reste
    lgr = lbyte - lgr * 4 * sizeof(cl_int);

    OCLINITARG;
    OCLSETARG(ker[KernelMemset], a);
    OCLSETARG(ker[KernelMemset], v);
    OCLSETARG(ker[KernelMemset], lgr);  // en byte
    assert((lgr % sizeof(cl_int)) == 0);
    maxthr = oclGetMaxWorkSize(ker[KernelMemset], oclGetDeviceOfCQueue(cqueue));
    if (lgr < maxthr)
      maxthr = lgr;
    oclLaunchKernel(ker[KernelMemset], cqueue, lgr, maxthr);
  }
}

void
oclMakeHydroKernels()
{
  // on cree tous les kernels d'un coup pour gagner du temps
  // en preprocesseur, #a transforma a en "a".
  assert(pgm != 0);

  ker = (cl_kernel *) calloc(LastEntryKernel, sizeof(cl_kernel));
  assert(ker != NULL);

  CREATEKER(ker[Loop1KcuCmpflx], Loop1KcuCmpflx);
  CREATEKER(ker[Loop2KcuCmpflx], Loop2KcuCmpflx);
  CREATEKER(ker[LoopKQEforRow], LoopKQEforRow);
  CREATEKER(ker[LoopKcourant], LoopKcourant);
  CREATEKER(ker[Loop1KcuGather], Loop1KcuGather);
  CREATEKER(ker[Loop2KcuGather], Loop2KcuGather);
  CREATEKER(ker[Loop3KcuGather], Loop3KcuGather);
  CREATEKER(ker[Loop4KcuGather], Loop4KcuGather);
  CREATEKER(ker[Loop1KcuUpdate], Loop1KcuUpdate);
  CREATEKER(ker[Loop2KcuUpdate], Loop2KcuUpdate);
  CREATEKER(ker[Loop3KcuUpdate], Loop3KcuUpdate);
  CREATEKER(ker[Loop4KcuUpdate], Loop4KcuUpdate);
  CREATEKER(ker[Loop1KcuConstoprim], Loop1KcuConstoprim);
  CREATEKER(ker[Loop2KcuConstoprim], Loop2KcuConstoprim);
  CREATEKER(ker[LoopEOS], LoopEOS);
  CREATEKER(ker[Loop1KcuMakeBoundary], Loop1KcuMakeBoundary);
  CREATEKER(ker[Loop2KcuMakeBoundary], Loop2KcuMakeBoundary);
  CREATEKER(ker[Loop1KcuQleftright], Loop1KcuQleftright);
  CREATEKER(ker[Init1KcuRiemann], Init1KcuRiemann);
  CREATEKER(ker[Init2KcuRiemann], Init2KcuRiemann);
  CREATEKER(ker[Init3KcuRiemann], Init3KcuRiemann);
  CREATEKER(ker[Init4KcuRiemann], Init4KcuRiemann);
  CREATEKER(ker[Loop1KcuRiemann], Loop1KcuRiemann);
  CREATEKER(ker[Loop10KcuRiemann], Loop10KcuRiemann);
  CREATEKER(ker[LoopKcuSlope], LoopKcuSlope);
  CREATEKER(ker[Loop1KcuTrace], Loop1KcuTrace);
  CREATEKER(ker[Loop2KcuTrace], Loop2KcuTrace);
  CREATEKER(ker[LoopKredMaxDble], LoopKredMaxDble);
  CREATEKER(ker[KernelMemset], KernelMemset);
  CREATEKER(ker[KernelMemsetV4], KernelMemsetV4);
}

void
oclInitCode()
{
  int verbose = 1;
  int nbplatf = 0;
  int nbgpu = 0;
  char srcdir[1024];

  nbplatf = oclGetNbPlatforms(verbose);
  if (nbplatf == 0) {
    fprintf(stderr, "No OpenCL platform available\n");
    abort();
  }
  platformselected = 0;
  ctx = oclCreateCtxForPlatform(platformselected, verbose);

#define CPUVERSION 0
#if CPUVERSION == 0
  printf("Building a GPU version\n");
  nbgpu = oclGetNbOfGpu(platformselected);
  if (nbgpu == 0) {
    fprintf(stderr, "No GPU available\n");
    abort();
  }
  devselected = oclGetGpuDev(platformselected, 0);
#else
  printf("Building a CPU version\n");
  nbcpu = oclGetNbOfCpu(platformselected);
  if (nbcpu == 0) {
    fprintf(stderr, "No CPU available\n");
    abort();
  }
  devselected = oclGetCpuDev(platformselected, 0);
#endif
  cqueue = oclCreateCommandQueueForDev(platformselected, devselected, ctx, 1);
  getcwd(srcdir, 1023);
  pgm = oclCreatePgmFromCtx("hydro_kernels.cl", srcdir, ctx, platformselected, devselected, verbose);
  // exit(2);
  oclMakeHydroKernels();
}

//EOF
