#ifndef CUMAKE_BOUNDARY_H_INCLUDED
#define CUMAKE_BOUNDARY_H_INCLUDED
#include <CL/cl.h>

void oclMakeBoundary(long idim, const hydroparam_t H, hydrovar_t * Hv, cl_mem uoldDEV);

#endif // MAKE_BOUNDARY_H_INCLUDED
