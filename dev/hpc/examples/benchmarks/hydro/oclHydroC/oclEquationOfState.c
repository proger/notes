/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

// #include <stdlib.h>
// #include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "oclEquationOfState.h"
#include "utils.h"
#include "oclInit.h"
#include "ocltools.h"

void
oclEquationOfState(cl_mem qDEV, cl_mem eintDEV, cl_mem cDEV,
                   long offsetIP, long offsetID, long imin, long imax, const double Hsmallc, const double Hgamma)
{
  WHERE("equation_of_state");
//   SetBlockDims((imax - imin), THREADSSZ, block, grid);
//   LoopEOS <<< grid, block >>> (rho, eint, p, c, imin, imax, Hsmallc, Hgamma);
//   CheckErr("LoopEOS");
//   cudaThreadSynchronize();
//   CheckErr("LoopEOS");
  OCLINITARG;
  OCLSETARG(ker[LoopEOS], qDEV);
  OCLSETARG(ker[LoopEOS], eintDEV);
  OCLSETARG(ker[LoopEOS], cDEV);
  OCLSETARG(ker[LoopEOS], offsetIP);
  OCLSETARG(ker[LoopEOS], offsetID);
  OCLSETARG(ker[LoopEOS], imin);
  OCLSETARG(ker[LoopEOS], imax);
  OCLSETARG(ker[LoopEOS], Hsmallc);
  OCLSETARG(ker[LoopEOS], Hgamma);
  oclLaunchKernel(ker[LoopEOS], cqueue, (imax - imin), THREADSSZ);
}                               // equation_of_state


// EOF
