/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "oclTrace.h"
#include "oclInit.h"
#include "ocltools.h"

#define IHVW(i, v) ((i) + (v) * Hnxyt)

void
oclTrace(cl_mem q, cl_mem dq, cl_mem c, cl_mem qxm, cl_mem qxp,
         const double dtdx, const long n, const long Hscheme, const long Hnvar, const long Hnxyt)
{
  long ijmin, ijmax;
  double zerol = 0.0, zeror = 0.0, project = 0.;

  WHERE("trace");
  ijmin = 0;
  ijmax = n;

  // if (strcmp(Hscheme, "muscl") == 0) {       // MUSCL-Hancock method
  if (Hscheme == HSCHEME_MUSCL) {       // MUSCL-Hancock method
    zerol = -hundred / dtdx;
    zeror = hundred / dtdx;
    project = one;
  }
  // if (strcmp(Hscheme, "plmde") == 0) {       // standard PLMDE
  if (Hscheme == HSCHEME_PLMDE) {       // standard PLMDE
    zerol = zero;
    zeror = zero;
    project = one;
  }
  // if (strcmp(Hscheme, "collela") == 0) {     // Collela's method
  if (Hscheme == HSCHEME_COLLELA) {     // Collela's method
    zerol = zero;
    zeror = zero;
    project = zero;
  }
//   SetBlockDims(((ijmax - 1) - (ijmin + 1)), THREADSSZs, block, grid);
//   Loop1KcuTrace <<< grid, block >>> (q, dq, c, qxm, qxp, dtdx, Hnxyt, ijmin, ijmax, zeror, zerol, project);
//   CheckErr("Loop1KcuTrace");
//   cudaThreadSynchronize();

//   if (Hnvar > IP + 1) {
//     Loop2KcuTrace <<< grid, block >>> (q, dq, qxm, qxp, dtdx, Hnvar, Hnxyt, ijmin, ijmax, zeror, zerol, project);
//     CheckErr("Loop2KcuTrace");
//     cudaThreadSynchronize();
//   }
  OCLINITARG;
  OCLSETARG(ker[Loop1KcuTrace], q);
  OCLSETARG(ker[Loop1KcuTrace], dq);
  OCLSETARG(ker[Loop1KcuTrace], c);
  OCLSETARG(ker[Loop1KcuTrace], qxm);
  OCLSETARG(ker[Loop1KcuTrace], qxp);
  OCLSETARG(ker[Loop1KcuTrace], dtdx);
  OCLSETARG(ker[Loop1KcuTrace], Hnxyt);
  OCLSETARG(ker[Loop1KcuTrace], ijmin);
  OCLSETARG(ker[Loop1KcuTrace], ijmax);
  OCLSETARG(ker[Loop1KcuTrace], zeror);
  OCLSETARG(ker[Loop1KcuTrace], zerol);
  OCLSETARG(ker[Loop1KcuTrace], project);
  oclLaunchKernel(ker[Loop1KcuTrace], cqueue, ((ijmax - 1) - (ijmin + 1)), THREADSSZ);
  if (Hnvar > IP + 1) {
    OCLINITARG;
    OCLSETARG(ker[Loop2KcuTrace], q);
    OCLSETARG(ker[Loop2KcuTrace], dq);
    OCLSETARG(ker[Loop2KcuTrace], qxm);
    OCLSETARG(ker[Loop2KcuTrace], qxp);
    OCLSETARG(ker[Loop2KcuTrace], dtdx);
    OCLSETARG(ker[Loop2KcuTrace], Hnvar);
    OCLSETARG(ker[Loop2KcuTrace], Hnxyt);
    OCLSETARG(ker[Loop2KcuTrace], ijmin);
    OCLSETARG(ker[Loop2KcuTrace], ijmax);
    OCLSETARG(ker[Loop2KcuTrace], zeror);
    OCLSETARG(ker[Loop2KcuTrace], zerol);
    OCLSETARG(ker[Loop2KcuTrace], project);
    oclLaunchKernel(ker[Loop2KcuTrace], cqueue, ((ijmax - 1) - (ijmin + 1)), THREADSSZ);
  }
}                               // trace

#undef IHVW

//EOF
