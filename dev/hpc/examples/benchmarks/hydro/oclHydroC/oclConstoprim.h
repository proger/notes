#ifndef CUCONSTOPRIM_H_INCLUDED
#define CUCONSTOPRIM_H_INCLUDED
#include <CL/cl.h>
#include "utils.h"

void oclConstoprim(cl_mem uDEV, cl_mem qDEV, cl_mem eDEV, const long n, const long Hnxyt, const long Hnvar, const double Hsmallr);

#endif // CUCONSTOPRIM_H_INCLUDED
