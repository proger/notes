/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "oclQleftright.h"
#include "oclInit.h"
#include "ocltools.h"

void
oclQleftright(const long idim, const long Hnx, const long Hny, const long Hnxyt,
              const long Hnvar, cl_mem qxm, cl_mem qxp, cl_mem qleft, cl_mem qright)
{
  long bmax;

  WHERE("qleftright");
  if (idim == 1) {
    bmax = Hnx + 1;
  } else {
    bmax = Hny + 1;
  }
//   SetBlockDims(bmax, THREADSSZ, block, grid);
//   Loop1KcuQleftright <<< grid, block >>> (bmax, Hnvar, Hnxyt, qxm, qxp, qleft, qright);
//   CheckErr("Loop1KcuQleftright");
//   cudaThreadSynchronize();
  OCLINITARG;
  OCLSETARG(ker[Loop1KcuQleftright], bmax);
  OCLSETARG(ker[Loop1KcuQleftright], Hnvar);
  OCLSETARG(ker[Loop1KcuQleftright], Hnxyt);
  OCLSETARG(ker[Loop1KcuQleftright], qxm);
  OCLSETARG(ker[Loop1KcuQleftright], qxp);
  OCLSETARG(ker[Loop1KcuQleftright], qleft);
  OCLSETARG(ker[Loop1KcuQleftright], qright);
  oclLaunchKernel(ker[Loop1KcuQleftright], cqueue, bmax, THREADSSZ);
}

// EOF
