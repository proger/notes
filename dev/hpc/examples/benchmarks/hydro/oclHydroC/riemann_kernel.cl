__kernel void
Loop1KcuRiemann(__global Args_t * K
		 , __global double * qleft
		 , __global double * qright
		 , __global double * sgnm
		 , __global double * qgdnv
		)
{
  double smallp, gamma6, ql, qr, usr, usl, wwl, wwr, smallpp;
  long iter;
  long tid = get_local_id(0);
  double ulS = 0.0;
  double plS = 0.0;
  double clS = 0.0;
  double urS = 0.0;
  double prS = 0.0;
  double crS = 0.0;
  double uoS = 0.0;
  double delpS = 0.0;
  double poldS = 0.0;
  double Kroi = 0.0;
  double Kuoi = 0.0;
  double Kpoi = 0.0;
  double Kwoi = 0.0;
  double Kdelpi = 0.0;
  long i = get_global_id(0);
  long Hnxyt = K->Hnxyt;
  if (i >= K->narray)
    return;

  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxID = IHVW(i, ID, Hnxyt);

  smallp = Square(K->Hsmallc) / K->Hgamma;

  double Krli = max(K->qleft[idxID], K->Hsmallr);
  double Kuli = K->qleft[idxIU];
  // opencl explose au dela de cette ligne si le code n'est pas en commentaire
  double Kpli = max(K->qleft[idxIP], (double) (Krli * smallp));
  double Krri = max(K->qright[idxID], K->Hsmallr);
  double Kuri = K->qright[idxIU];
  double Kpri = max(K->qright[idxIP], (double) (Krri * smallp));
  // Lagrangian sound speed
  double Kcli = K->Hgamma * Kpli * Krli;
  double Kcri = K->Hgamma * Kpri * Krri;
  // First guess
  double Kwli = sqrt(Kcli);
  double Kwri = sqrt(Kcri);
  double Kpstari =
    ((Kwri * Kpli + Kwli * Kpri) + Kwli * Kwri * (Kuli - Kuri)) / (Kwli + Kwri);
  Kpstari = max(Kpstari, 0.0);
  double Kpoldi = Kpstari;
  // ind est un masque de traitement pour le newton
  Kindi = 1;                // toutes les cellules sont a traiter

  ulS = Kuli;
  plS = Kpli;
  clS = Kcli;
  urS = Kuri;
  prS = Kpri;
  crS = Kcri;
  uoS = Kuoi;
  delpS = Kdelpi;
  poldS = Kpoldi;

  smallp = Square(K->Hsmallc) / K->Hgamma;
  smallpp = K->Hsmallr * smallp;
  gamma6 = (K->Hgamma + one) / (two * K->Hgamma);

  long indi = Kindi;

  for (iter = 0; iter < K->Hniter_riemann; iter++) {
    double precision = 1.e-6;
    wwl = sqrt(clS * (one + gamma6 * (poldS - plS) / plS));
    wwr = sqrt(crS * (one + gamma6 * (poldS - prS) / prS));
    ql = two * wwl * Square(wwl) / (Square(wwl) + clS);
    qr = two * wwr * Square(wwr) / (Square(wwr) + crS);
    usl = ulS - (poldS - plS) / wwl;
    usr = urS + (poldS - prS) / wwr;
    double t1 = qr * ql / (qr + ql) * (usl - usr);
    double t2 = -poldS;
    delpS = max(t1, t2);
    poldS = poldS + delpS;
    uoS = fabs(delpS / (poldS + smallpp));
    indi = uoS > precision;
    if (!indi)
      break;
  }
  // barrier(CLK_LOCAL_MEM_FENCE);
  Kuoi = uoS;
  Kpoldi = poldS;

  gamma6 = (K->Hgamma + one) / (two * K->Hgamma);

  Kpstari = Kpoldi;
  Kwli = sqrt(Kcli * (one + gamma6 * (Kpstari - Kpli) / Kpli));
  Kwri = sqrt(Kcri * (one + gamma6 * (Kpstari - Kpri) / Kpri));

  double Kustari = demi * (Kuli + (Kpli - Kpstari) / Kwli + Kuri - (Kpri - Kpstari) / Kwri);
  K->sgnm[i] = (Kustari > 0) ? 1 : -1;
  if (K->sgnm[i] == 1) {
    Kroi = Krli;
    Kuoi = Kuli;
    Kpoi = Kpli;
    Kwoi = Kwli;
  } else {
    Kroi = Krri;
    Kuoi = Kuri;
    Kpoi = Kpri;
    Kwoi = Kwri;
  }
  double Kcoi = max(K->Hsmallc, sqrt(fabs(K->Hgamma * Kpoi / Kroi)));
  double Krstari = Kroi / (one + Kroi * (Kpoi - Kpstari) / Square(Kwoi));
  Krstari = max(Krstari, K->Hsmallr);
  double Kcstari = max(K->Hsmallc, sqrt(fabs(K->Hgamma * Kpstari / Krstari)));
  double Kspouti = Kcoi - K->sgnm[i] * Kuoi;
  double Kspini = Kcstari - K->sgnm[i] * Kustari;
  double Kushocki = Kwoi / Kroi - K->sgnm[i] * Kuoi;
  if (Kpstari >= Kpoi) {
    Kspini = Kushocki;
    Kspouti = Kushocki;
  }
  double Kscri = max((double) (Kspouti - Kspini), (double) (K->Hsmallc + fabs(Kspouti + Kspini)));
  double Kfraci = (one + (Kspouti + Kspini) / Kscri) * demi;
  Kfraci = max(zero, (double) (min(one, Kfraci)));

  K->qgdnv[idxID] = Kfraci * Krstari + (one - Kfraci) * Kroi;
  K->qgdnv[idxIU] = Kfraci * Kustari + (one - Kfraci) * Kuoi;
  K->qgdnv[idxIP] = Kfraci * Kpstari + (one - Kfraci) * Kpoi;

  if (Kspouti < zero) {
    K->qgdnv[idxID] = Kroi;
    K->qgdnv[idxIU] = Kuoi;
    K->qgdnv[idxIP] = Kpoi;
  }
  if (Kspini > zero) {
    K->qgdnv[idxID] = Krstari;
    K->qgdnv[idxIU] = Kustari;
    K->qgdnv[idxIP] = Kpstari;
  }

  if (K->sgnm[i] == 1) {
    K->qgdnv[idxIV] = K->qleft[idxIV];
  } else {
    K->qgdnv[idxIV] = K->qright[idxIV];
  }
}

__kernel void
Loop10KcuRiemann(__global Args_t * K
		 , __global double * qleft
		 , __global double * qright
		 , __global double * sgnm
		 , __global double * qgdnv
		 )
{
  long invar;
  long i = get_global_id(0);
  long Hnxyt = K->Hnxyt;
  if (i >= K->narray)
    return;

  for (invar = IP + 1; invar < K->Hnvar; invar++) {
    size_t idxIN = IHVW(i, invar, Hnxyt);
    if (K->sgnm[i] == 1) {
      K->qgdnv[idxIN] = K->qleft[idxIN];
    }
    if (K->sgnm[i] != 1) {
      K->qgdnv[idxIN] = K->qright[idxIN];
    }
  }
}

