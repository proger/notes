; ModuleID = 'hydro_kernels.cl'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32"
target triple = "amdil-pc-amdopencl"

%0 = type { i8*, i8*, i8*, i8*, i32 }
%struct._Args = type <{ double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64 addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64 addrspace(1)*, i64 addrspace(1)*, i64, double, double, double, i64, i64, i64 }>

@sgv = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv1 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv2 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv3 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv4 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv5 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv6 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv7 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv8 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv9 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv10 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv11 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv12 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv13 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv14 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv15 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv16 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv17 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv18 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv19 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv20 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv21 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv22 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv23 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv24 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv25 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv26 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv27 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv28 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv29 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv30 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv31 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv32 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv33 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv34 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv35 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv36 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv37 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv38 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv39 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv40 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv41 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv42 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv43 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv44 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv45 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv46 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv47 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv48 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv49 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv50 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv51 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv52 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv53 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv54 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv55 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv56 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv57 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv58 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv59 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv60 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv61 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv62 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv63 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv64 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv65 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv66 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv67 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv68 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv69 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv70 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv71 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv72 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@LoopKredMaxDble_cllocal_sdata = internal addrspace(3) global [512 x double] zeroinitializer, align 8 ; <[512 x double] addrspace(3)*> [#uses=2]
@sgv73 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv74 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv75 = internal constant [1 x i8*] [i8* bitcast ([512 x double] addrspace(3)* @LoopKredMaxDble_cllocal_sdata to i8*)] ; <[1 x i8*]*> [#uses=1]
@sgv76 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv77 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv78 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv79 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv80 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv81 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv82 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv83 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv84 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@sgv85 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@fgv86 = internal constant [1 x i8] zeroinitializer ; <[1 x i8]*> [#uses=1]
@lvgv87 = internal constant [0 x i8*] zeroinitializer ; <[0 x i8*]*> [#uses=1]
@llvm.global.annotations = appending global [30 x %0] [%0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, i64, i64, double)* @__OpenCL_Loop1KcuCmpflx_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, i64, i64, i64)* @__OpenCL_Loop2KcuCmpflx_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv1, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv2, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv3 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double, i64, i64, i64, i64)* @__OpenCL_LoopKQEforRow_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv4, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv5, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv6 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, double, double addrspace(1)*, i64, i64)* @__OpenCL_LoopKcourant_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv7, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv8, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv9 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, i64, i64, i64, i64, i64, i64)* @__OpenCL_Loop1KcuGather_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv10, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv11, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv12 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, i64, i64, i64, i64, i64, i64)* @__OpenCL_Loop2KcuGather_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv13, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv14, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv15 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, i64, i64, i64, i64, i64, i64, i64)* @__OpenCL_Loop3KcuGather_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv16, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv17, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv18 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, i64, i64, i64, i64, i64, i64, i64)* @__OpenCL_Loop4KcuGather_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv19, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv20, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv21 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, double, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64, i64, i64, i64, i64)* @__OpenCL_Loop1KcuUpdate_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv22, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv23, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv24 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, double, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64, i64, i64, i64, i64, i64)* @__OpenCL_Loop2KcuUpdate_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv25, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv26, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv27 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, double, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64, i64, i64, i64, i64)* @__OpenCL_Loop3KcuUpdate_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv28, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv29, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv30 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, double, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64, i64, i64, i64, i64, i64)* @__OpenCL_Loop4KcuUpdate_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv31, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv32, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv33 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64, double)* @__OpenCL_Loop1KcuConstoprim_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv34, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv35, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv36 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, double addrspace(1)*, double addrspace(1)*, i64, i64)* @__OpenCL_Loop2KcuConstoprim_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv37, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv38, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv39 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64, i64, i64, i64, double, double)* @__OpenCL_LoopEOS_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv40, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv41, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv42 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, i64, double, i64, i64, i64, i64, i64, double addrspace(1)*)* @__OpenCL_Loop1KcuMakeBoundary_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv43, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv44, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv45 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, i64, double, i64, i64, i64, i64, i64, double addrspace(1)*)* @__OpenCL_Loop2KcuMakeBoundary_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv46, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv47, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv48 to i8*), i32 0 }, %0 { i8* bitcast (void (i64, i64, i64, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*)* @__OpenCL_Loop1KcuQleftright_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv49, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv50, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv51 to i8*), i32 0 }, %0 { i8* bitcast (void (%struct._Args addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*)* @__OpenCL_Init1KcuRiemann_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv52, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv53, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv54 to i8*), i32 0 }, %0 { i8* bitcast (void (%struct._Args addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*)* @__OpenCL_Init2KcuRiemann_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv55, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv56, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv57 to i8*), i32 0 }, %0 { i8* bitcast (void (%struct._Args addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64 addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*)* @__OpenCL_Init3KcuRiemann_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv58, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv59, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv60 to i8*), i32 0 }, %0 { i8* bitcast (void (%struct._Args addrspace(1)*, i64 addrspace(1)*, i64 addrspace(1)*)* @__OpenCL_Init4KcuRiemann_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv61, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv62, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv63 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, i64, i64, double, i64, i64)* @__OpenCL_LoopKcuSlope_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv64, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv65, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv66 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double, i64, i64, i64, double, double, double)* @__OpenCL_Loop1KcuTrace_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv67, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv68, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv69 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double, i64, i64, i64, i64, double, double, double)* @__OpenCL_Loop2KcuTrace_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv70, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv71, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv72 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, i64)* @__OpenCL_LoopKredMaxDble_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv73, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv74, i32 0, i32 0), i8* bitcast ([1 x i8*]* @lvgv75 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, i32, i64)* @__OpenCL_KernelMemset_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv76, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv77, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv78 to i8*), i32 0 }, %0 { i8* bitcast (void (<4 x i32> addrspace(1)*, i32, i64)* @__OpenCL_KernelMemsetV4_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv79, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv80, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv81 to i8*), i32 0 }, %0 { i8* bitcast (void (double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64, i64, double, double, double, i64)* @__OpenCL_Loop1KcuRiemann_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv82, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv83, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv84 to i8*), i32 0 }, %0 { i8* bitcast (void (%struct._Args addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, double addrspace(1)*, i64, i64, i64)* @__OpenCL_Loop10KcuRiemann_kernel to i8*), i8* getelementptr inbounds ([1 x i8]* @sgv85, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @fgv86, i32 0, i32 0), i8* bitcast ([0 x i8*]* @lvgv87 to i8*), i32 0 }], section "llvm.metadata" ; <[30 x %0]*> [#uses=0]

define void @idx2d(i64* %x, i64* %y, i64 %nx) nounwind {
entry:
  %x.addr = alloca i64*, align 4                  ; <i64**> [#uses=2]
  %y.addr = alloca i64*, align 4                  ; <i64**> [#uses=3]
  %nx.addr = alloca i64, align 8                  ; <i64*> [#uses=3]
  %i1d = alloca i64, align 8                      ; <i64*> [#uses=3]
  store i64* %x, i64** %x.addr
  store i64* %y, i64** %y.addr
  store i64 %nx, i64* %nx.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %call1 = call i32 @get_global_size(i32 1) nounwind ; <i32> [#uses=1]
  %tmp = sub i32 %call1, 1                        ; <i32> [#uses=1]
  %call2 = call i32 @get_global_id(i32 1) nounwind ; <i32> [#uses=1]
  %call3 = call i32 @get_global_size(i32 2) nounwind ; <i32> [#uses=1]
  %tmp4 = sub i32 %call3, 1                       ; <i32> [#uses=1]
  %call5 = call i32 @get_global_id(i32 2) nounwind ; <i32> [#uses=1]
  %tmp6 = mul i32 %tmp4, %call5                   ; <i32> [#uses=1]
  %tmp7 = add i32 %call2, %tmp6                   ; <i32> [#uses=1]
  %tmp8 = mul i32 %tmp, %tmp7                     ; <i32> [#uses=1]
  %tmp9 = add i32 %call, %tmp8                    ; <i32> [#uses=1]
  %conv = zext i32 %tmp9 to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i1d
  %tmp10 = load i64** %y.addr                     ; <i64*> [#uses=1]
  %tmp11 = load i64* %i1d                         ; <i64> [#uses=1]
  %tmp12 = load i64* %nx.addr                     ; <i64> [#uses=1]
  %tmp13 = sdiv i64 %tmp11, %tmp12                ; <i64> [#uses=1]
  store i64 %tmp13, i64* %tmp10
  %tmp14 = load i64** %x.addr                     ; <i64*> [#uses=1]
  %tmp15 = load i64* %i1d                         ; <i64> [#uses=1]
  %tmp16 = load i64** %y.addr                     ; <i64*> [#uses=1]
  %tmp17 = load i64* %tmp16                       ; <i64> [#uses=1]
  %tmp18 = load i64* %nx.addr                     ; <i64> [#uses=1]
  %tmp19 = mul i64 %tmp17, %tmp18                 ; <i64> [#uses=1]
  %tmp20 = sub i64 %tmp15, %tmp19                 ; <i64> [#uses=1]
  store i64 %tmp20, i64* %tmp14
  br label %return

return:                                           ; preds = %entry
  ret void
}

declare i32 @get_global_id(i32) nounwind

declare i32 @get_global_size(i32) nounwind

define double @Square(double %x) nounwind {
entry:
  %retval = alloca double, align 8                ; <double*> [#uses=2]
  %x.addr = alloca double, align 8                ; <double*> [#uses=3]
  store double %x, double* %x.addr
  %tmp = load double* %x.addr                     ; <double> [#uses=1]
  %tmp1 = load double* %x.addr                    ; <double> [#uses=1]
  %tmp2 = fmul double %tmp, %tmp1                 ; <double> [#uses=1]
  store double %tmp2, double* %retval
  br label %return

return:                                           ; preds = %entry
  %tmp3 = load double* %retval                    ; <double> [#uses=1]
  ret double %tmp3
}

define i32 @IHVW(i32 %i, i32 %v, i32 %Hnxyt) nounwind {
entry:
  %retval = alloca i32, align 4                   ; <i32*> [#uses=2]
  %i.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %v.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %Hnxyt.addr = alloca i32, align 4               ; <i32*> [#uses=2]
  store i32 %i, i32* %i.addr
  store i32 %v, i32* %v.addr
  store i32 %Hnxyt, i32* %Hnxyt.addr
  %tmp = load i32* %i.addr                        ; <i32> [#uses=1]
  %tmp1 = load i32* %v.addr                       ; <i32> [#uses=1]
  %tmp2 = load i32* %Hnxyt.addr                   ; <i32> [#uses=1]
  %tmp3 = mul i32 %tmp1, %tmp2                    ; <i32> [#uses=1]
  %tmp4 = add i32 %tmp, %tmp3                     ; <i32> [#uses=1]
  store i32 %tmp4, i32* %retval
  br label %return

return:                                           ; preds = %entry
  %tmp5 = load i32* %retval                       ; <i32> [#uses=1]
  ret i32 %tmp5
}

define i32 @IHU(i32 %i, i32 %j, i32 %v, i32 %Hnxt, i32 %Hnyt) nounwind {
entry:
  %retval = alloca i32, align 4                   ; <i32*> [#uses=2]
  %i.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %j.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %v.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %Hnxt.addr = alloca i32, align 4                ; <i32*> [#uses=2]
  %Hnyt.addr = alloca i32, align 4                ; <i32*> [#uses=2]
  store i32 %i, i32* %i.addr
  store i32 %j, i32* %j.addr
  store i32 %v, i32* %v.addr
  store i32 %Hnxt, i32* %Hnxt.addr
  store i32 %Hnyt, i32* %Hnyt.addr
  %tmp = load i32* %i.addr                        ; <i32> [#uses=1]
  %tmp1 = load i32* %Hnxt.addr                    ; <i32> [#uses=1]
  %tmp2 = load i32* %j.addr                       ; <i32> [#uses=1]
  %tmp3 = load i32* %Hnyt.addr                    ; <i32> [#uses=1]
  %tmp4 = load i32* %v.addr                       ; <i32> [#uses=1]
  %tmp5 = mul i32 %tmp3, %tmp4                    ; <i32> [#uses=1]
  %tmp6 = add i32 %tmp2, %tmp5                    ; <i32> [#uses=1]
  %tmp7 = mul i32 %tmp1, %tmp6                    ; <i32> [#uses=1]
  %tmp8 = add i32 %tmp, %tmp7                     ; <i32> [#uses=1]
  store i32 %tmp8, i32* %retval
  br label %return

return:                                           ; preds = %entry
  %tmp9 = load i32* %retval                       ; <i32> [#uses=1]
  ret i32 %tmp9
}

define i32 @IHV(i32 %i, i32 %j, i32 %v, i32 %Hnxt, i32 %Hnyt) nounwind {
entry:
  %retval = alloca i32, align 4                   ; <i32*> [#uses=2]
  %i.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %j.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %v.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %Hnxt.addr = alloca i32, align 4                ; <i32*> [#uses=2]
  %Hnyt.addr = alloca i32, align 4                ; <i32*> [#uses=2]
  store i32 %i, i32* %i.addr
  store i32 %j, i32* %j.addr
  store i32 %v, i32* %v.addr
  store i32 %Hnxt, i32* %Hnxt.addr
  store i32 %Hnyt, i32* %Hnyt.addr
  %tmp = load i32* %i.addr                        ; <i32> [#uses=1]
  %tmp1 = load i32* %Hnxt.addr                    ; <i32> [#uses=1]
  %tmp2 = load i32* %j.addr                       ; <i32> [#uses=1]
  %tmp3 = load i32* %Hnyt.addr                    ; <i32> [#uses=1]
  %tmp4 = load i32* %v.addr                       ; <i32> [#uses=1]
  %tmp5 = mul i32 %tmp3, %tmp4                    ; <i32> [#uses=1]
  %tmp6 = add i32 %tmp2, %tmp5                    ; <i32> [#uses=1]
  %tmp7 = mul i32 %tmp1, %tmp6                    ; <i32> [#uses=1]
  %tmp8 = add i32 %tmp, %tmp7                     ; <i32> [#uses=1]
  store i32 %tmp8, i32* %retval
  br label %return

return:                                           ; preds = %entry
  %tmp9 = load i32* %retval                       ; <i32> [#uses=1]
  ret i32 %tmp9
}

define void @__OpenCL_Loop1KcuCmpflx_kernel(double addrspace(1)* %qgdnv, double addrspace(1)* %flux, i64 %narray, i64 %Hnxyt, double %Hgamma) nounwind {
entry:
  %qgdnv.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=14]
  %flux.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=7]
  %narray.addr = alloca i64, align 8              ; <i64*> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=5]
  %Hgamma.addr = alloca double, align 8           ; <double*> [#uses=2]
  %entho = alloca double, align 8                 ; <double*> [#uses=3]
  %ekin = alloca double, align 8                  ; <double*> [#uses=3]
  %etot = alloca double, align 8                  ; <double*> [#uses=3]
  %i = alloca i64, align 8                        ; <i64*> [#uses=6]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=6]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=5]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=7]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=5]
  store double addrspace(1)* %qgdnv, double addrspace(1)** %qgdnv.addr
  store double addrspace(1)* %flux, double addrspace(1)** %flux.addr
  store i64 %narray, i64* %narray.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store double %Hgamma, double* %Hgamma.addr
  store double 0.000000e+00, double* %entho
  store double 0.000000e+00, double* %ekin
  store double 0.000000e+00, double* %etot
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %narray.addr                  ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %conv3 = trunc i64 %tmp2 to i32                 ; <i32> [#uses=1]
  %tmp4 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv5 = trunc i64 %tmp4 to i32                 ; <i32> [#uses=1]
  %tmp6 = mul i32 0, %conv5                       ; <i32> [#uses=1]
  %tmp7 = add i32 %conv3, %tmp6                   ; <i32> [#uses=1]
  store i32 %tmp7, i32* %idxID
  %tmp8 = load i64* %i                            ; <i64> [#uses=1]
  %conv9 = trunc i64 %tmp8 to i32                 ; <i32> [#uses=1]
  %tmp10 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv11 = trunc i64 %tmp10 to i32               ; <i32> [#uses=1]
  %tmp12 = mul i32 3, %conv11                     ; <i32> [#uses=1]
  %tmp13 = add i32 %conv9, %tmp12                 ; <i32> [#uses=1]
  store i32 %tmp13, i32* %idxIP
  %tmp14 = load i64* %i                           ; <i64> [#uses=1]
  %conv15 = trunc i64 %tmp14 to i32               ; <i32> [#uses=1]
  %tmp16 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv17 = trunc i64 %tmp16 to i32               ; <i32> [#uses=1]
  %tmp18 = mul i32 1, %conv17                     ; <i32> [#uses=1]
  %tmp19 = add i32 %conv15, %tmp18                ; <i32> [#uses=1]
  store i32 %tmp19, i32* %idxIU
  %tmp20 = load i64* %i                           ; <i64> [#uses=1]
  %conv21 = trunc i64 %tmp20 to i32               ; <i32> [#uses=1]
  %tmp22 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv23 = trunc i64 %tmp22 to i32               ; <i32> [#uses=1]
  %tmp24 = mul i32 2, %conv23                     ; <i32> [#uses=1]
  %tmp25 = add i32 %conv21, %tmp24                ; <i32> [#uses=1]
  store i32 %tmp25, i32* %idxIV
  %tmp26 = load double* %Hgamma.addr              ; <double> [#uses=1]
  %tmp27 = fsub double %tmp26, 1.000000e+00       ; <double> [#uses=1]
  %tmp28 = fdiv double 1.000000e+00, %tmp27       ; <double> [#uses=1]
  store double %tmp28, double* %entho
  %tmp29 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp30 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp29, i32 %tmp30 ; <double addrspace(1)*> [#uses=1]
  %tmp31 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp32 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx33 = getelementptr double addrspace(1)* %tmp31, i32 %tmp32 ; <double addrspace(1)*> [#uses=1]
  %tmp34 = load double addrspace(1)* %arrayidx33  ; <double> [#uses=1]
  %tmp35 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp36 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx37 = getelementptr double addrspace(1)* %tmp35, i32 %tmp36 ; <double addrspace(1)*> [#uses=1]
  %tmp38 = load double addrspace(1)* %arrayidx37  ; <double> [#uses=1]
  %tmp39 = fmul double %tmp34, %tmp38             ; <double> [#uses=1]
  store double %tmp39, double addrspace(1)* %arrayidx
  %tmp40 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp41 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx42 = getelementptr double addrspace(1)* %tmp40, i32 %tmp41 ; <double addrspace(1)*> [#uses=1]
  %tmp43 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp44 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx45 = getelementptr double addrspace(1)* %tmp43, i32 %tmp44 ; <double addrspace(1)*> [#uses=1]
  %tmp46 = load double addrspace(1)* %arrayidx45  ; <double> [#uses=1]
  %tmp47 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp48 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx49 = getelementptr double addrspace(1)* %tmp47, i32 %tmp48 ; <double addrspace(1)*> [#uses=1]
  %tmp50 = load double addrspace(1)* %arrayidx49  ; <double> [#uses=1]
  %tmp51 = fmul double %tmp46, %tmp50             ; <double> [#uses=1]
  %tmp52 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp53 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx54 = getelementptr double addrspace(1)* %tmp52, i32 %tmp53 ; <double addrspace(1)*> [#uses=1]
  %tmp55 = load double addrspace(1)* %arrayidx54  ; <double> [#uses=1]
  %tmp56 = fadd double %tmp51, %tmp55             ; <double> [#uses=1]
  store double %tmp56, double addrspace(1)* %arrayidx42
  %tmp57 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp58 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx59 = getelementptr double addrspace(1)* %tmp57, i32 %tmp58 ; <double addrspace(1)*> [#uses=1]
  %tmp60 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx62 = getelementptr double addrspace(1)* %tmp60, i32 %tmp61 ; <double addrspace(1)*> [#uses=1]
  %tmp63 = load double addrspace(1)* %arrayidx62  ; <double> [#uses=1]
  %tmp64 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp65 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx66 = getelementptr double addrspace(1)* %tmp64, i32 %tmp65 ; <double addrspace(1)*> [#uses=1]
  %tmp67 = load double addrspace(1)* %arrayidx66  ; <double> [#uses=1]
  %tmp68 = fmul double %tmp63, %tmp67             ; <double> [#uses=1]
  store double %tmp68, double addrspace(1)* %arrayidx59
  %tmp69 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp70 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx71 = getelementptr double addrspace(1)* %tmp69, i32 %tmp70 ; <double addrspace(1)*> [#uses=1]
  %tmp72 = load double addrspace(1)* %arrayidx71  ; <double> [#uses=1]
  %tmp73 = fmul double 5.000000e-01, %tmp72       ; <double> [#uses=1]
  %tmp74 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp75 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx76 = getelementptr double addrspace(1)* %tmp74, i32 %tmp75 ; <double addrspace(1)*> [#uses=1]
  %tmp77 = load double addrspace(1)* %arrayidx76  ; <double> [#uses=1]
  %tmp78 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp79 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx80 = getelementptr double addrspace(1)* %tmp78, i32 %tmp79 ; <double addrspace(1)*> [#uses=1]
  %tmp81 = load double addrspace(1)* %arrayidx80  ; <double> [#uses=1]
  %tmp82 = fmul double %tmp77, %tmp81             ; <double> [#uses=1]
  %tmp83 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp84 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx85 = getelementptr double addrspace(1)* %tmp83, i32 %tmp84 ; <double addrspace(1)*> [#uses=1]
  %tmp86 = load double addrspace(1)* %arrayidx85  ; <double> [#uses=1]
  %tmp87 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp88 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx89 = getelementptr double addrspace(1)* %tmp87, i32 %tmp88 ; <double addrspace(1)*> [#uses=1]
  %tmp90 = load double addrspace(1)* %arrayidx89  ; <double> [#uses=1]
  %tmp91 = fmul double %tmp86, %tmp90             ; <double> [#uses=1]
  %tmp92 = fadd double %tmp82, %tmp91             ; <double> [#uses=1]
  %tmp93 = fmul double %tmp73, %tmp92             ; <double> [#uses=1]
  store double %tmp93, double* %ekin
  %tmp94 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp95 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx96 = getelementptr double addrspace(1)* %tmp94, i32 %tmp95 ; <double addrspace(1)*> [#uses=1]
  %tmp97 = load double addrspace(1)* %arrayidx96  ; <double> [#uses=1]
  %tmp98 = load double* %entho                    ; <double> [#uses=1]
  %tmp99 = fmul double %tmp97, %tmp98             ; <double> [#uses=1]
  %tmp100 = load double* %ekin                    ; <double> [#uses=1]
  %tmp101 = fadd double %tmp99, %tmp100           ; <double> [#uses=1]
  store double %tmp101, double* %etot
  %tmp102 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp103 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx104 = getelementptr double addrspace(1)* %tmp102, i32 %tmp103 ; <double addrspace(1)*> [#uses=1]
  %tmp105 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp106 = load i32* %idxIU                      ; <i32> [#uses=1]
  %arrayidx107 = getelementptr double addrspace(1)* %tmp105, i32 %tmp106 ; <double addrspace(1)*> [#uses=1]
  %tmp108 = load double addrspace(1)* %arrayidx107 ; <double> [#uses=1]
  %tmp109 = load double* %etot                    ; <double> [#uses=1]
  %tmp110 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp111 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx112 = getelementptr double addrspace(1)* %tmp110, i32 %tmp111 ; <double addrspace(1)*> [#uses=1]
  %tmp113 = load double addrspace(1)* %arrayidx112 ; <double> [#uses=1]
  %tmp114 = fadd double %tmp109, %tmp113          ; <double> [#uses=1]
  %tmp115 = fmul double %tmp108, %tmp114          ; <double> [#uses=1]
  store double %tmp115, double addrspace(1)* %arrayidx104
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

define void @__OpenCL_Loop2KcuCmpflx_kernel(double addrspace(1)* %qgdnv, double addrspace(1)* %flux, i64 %narray, i64 %Hnxyt, i64 %Hnvar) nounwind {
entry:
  %qgdnv.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %flux.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=3]
  %narray.addr = alloca i64, align 8              ; <i64*> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %IN = alloca i64, align 8                       ; <i64*> [#uses=5]
  %i = alloca i64, align 8                        ; <i64*> [#uses=3]
  %idxIN = alloca i32, align 4                    ; <i32*> [#uses=4]
  store double addrspace(1)* %qgdnv, double addrspace(1)** %qgdnv.addr
  store double addrspace(1)* %flux, double addrspace(1)** %flux.addr
  store i64 %narray, i64* %narray.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %narray.addr                  ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %for.exit, %if.then
  ret void

if.end:                                           ; preds = %entry
  store i64 4, i64* %IN
  br label %for.cond

if.then:                                          ; preds = %entry
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end
  %tmp2 = load i64* %IN                           ; <i64> [#uses=1]
  %tmp3 = load i64* %Hnvar.addr                   ; <i64> [#uses=1]
  %cmp4 = icmp slt i64 %tmp2, %tmp3               ; <i1> [#uses=1]
  br i1 %cmp4, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %return

for.body:                                         ; preds = %for.cond
  %tmp5 = load i64* %i                            ; <i64> [#uses=1]
  %conv6 = trunc i64 %tmp5 to i32                 ; <i32> [#uses=1]
  %tmp7 = load i64* %IN                           ; <i64> [#uses=1]
  %conv8 = trunc i64 %tmp7 to i32                 ; <i32> [#uses=1]
  %tmp9 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv10 = trunc i64 %tmp9 to i32                ; <i32> [#uses=1]
  %tmp11 = mul i32 %conv8, %conv10                ; <i32> [#uses=1]
  %tmp12 = add i32 %conv6, %tmp11                 ; <i32> [#uses=1]
  store i32 %tmp12, i32* %idxIN
  %tmp13 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp14 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp13, i32 %tmp14 ; <double addrspace(1)*> [#uses=1]
  %tmp15 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp16 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx17 = getelementptr double addrspace(1)* %tmp15, i32 %tmp16 ; <double addrspace(1)*> [#uses=1]
  %tmp18 = load double addrspace(1)* %arrayidx17  ; <double> [#uses=1]
  %tmp19 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp20 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx21 = getelementptr double addrspace(1)* %tmp19, i32 %tmp20 ; <double addrspace(1)*> [#uses=1]
  %tmp22 = load double addrspace(1)* %arrayidx21  ; <double> [#uses=1]
  %tmp23 = fmul double %tmp18, %tmp22             ; <double> [#uses=1]
  store double %tmp23, double addrspace(1)* %arrayidx
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %tmp24 = load i64* %IN                          ; <i64> [#uses=1]
  %tmp25 = add i64 %tmp24, 1                      ; <i64> [#uses=1]
  store i64 %tmp25, i64* %IN
  br label %for.cond
}

define void @__OpenCL_LoopKQEforRow_kernel(i64 %j, double addrspace(1)* %uold, double addrspace(1)* %q, double addrspace(1)* %e, double %Hsmallr, i64 %Hnxt, i64 %Hnyt, i64 %Hnxyt, i64 %n) nounwind {
entry:
  %j.addr = alloca i64, align 8                   ; <i64*> [#uses=5]
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %q.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=11]
  %e.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %Hsmallr.addr = alloca double, align 8          ; <double*> [#uses=2]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=5]
  %n.addr = alloca i64, align 8                   ; <i64*> [#uses=2]
  %tmp = alloca double, align 8                   ; <double*> [#uses=3]
  %tmp1 = alloca double, align 8                  ; <double*> [#uses=3]
  %eken = alloca double, align 8                  ; <double*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=11]
  %idxuID = alloca i64, align 8                   ; <i64*> [#uses=2]
  %idxuIU = alloca i64, align 8                   ; <i64*> [#uses=2]
  %idxuIV = alloca i64, align 8                   ; <i64*> [#uses=2]
  %idxuIP = alloca i64, align 8                   ; <i64*> [#uses=2]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=5]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=3]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=3]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=3]
  store i64 %j, i64* %j.addr
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  store double addrspace(1)* %q, double addrspace(1)** %q.addr
  store double addrspace(1)* %e, double addrspace(1)** %e.addr
  store double %Hsmallr, double* %Hsmallr.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store i64 %n, i64* %n.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %tmp3 = load i64* %n.addr                       ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp2, %tmp3                ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp4 = load i64* %i                            ; <i64> [#uses=1]
  %tmp5 = add i64 %tmp4, 2                        ; <i64> [#uses=1]
  %conv6 = trunc i64 %tmp5 to i32                 ; <i32> [#uses=1]
  %tmp7 = load i64* %Hnxt.addr                    ; <i64> [#uses=1]
  %conv8 = trunc i64 %tmp7 to i32                 ; <i32> [#uses=1]
  %tmp9 = load i64* %j.addr                       ; <i64> [#uses=1]
  %conv10 = trunc i64 %tmp9 to i32                ; <i32> [#uses=1]
  %tmp11 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv12 = trunc i64 %tmp11 to i32               ; <i32> [#uses=1]
  %tmp13 = mul i32 %conv12, 0                     ; <i32> [#uses=1]
  %tmp14 = add i32 %conv10, %tmp13                ; <i32> [#uses=1]
  %tmp15 = mul i32 %conv8, %tmp14                 ; <i32> [#uses=1]
  %tmp16 = add i32 %conv6, %tmp15                 ; <i32> [#uses=1]
  %conv17 = zext i32 %tmp16 to i64                ; <i64> [#uses=1]
  store i64 %conv17, i64* %idxuID
  %tmp18 = load i64* %i                           ; <i64> [#uses=1]
  %tmp19 = add i64 %tmp18, 2                      ; <i64> [#uses=1]
  %conv20 = trunc i64 %tmp19 to i32               ; <i32> [#uses=1]
  %tmp21 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv22 = trunc i64 %tmp21 to i32               ; <i32> [#uses=1]
  %tmp23 = load i64* %j.addr                      ; <i64> [#uses=1]
  %conv24 = trunc i64 %tmp23 to i32               ; <i32> [#uses=1]
  %tmp25 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv26 = trunc i64 %tmp25 to i32               ; <i32> [#uses=1]
  %tmp27 = mul i32 %conv26, 1                     ; <i32> [#uses=1]
  %tmp28 = add i32 %conv24, %tmp27                ; <i32> [#uses=1]
  %tmp29 = mul i32 %conv22, %tmp28                ; <i32> [#uses=1]
  %tmp30 = add i32 %conv20, %tmp29                ; <i32> [#uses=1]
  %conv31 = zext i32 %tmp30 to i64                ; <i64> [#uses=1]
  store i64 %conv31, i64* %idxuIU
  %tmp32 = load i64* %i                           ; <i64> [#uses=1]
  %tmp33 = add i64 %tmp32, 2                      ; <i64> [#uses=1]
  %conv34 = trunc i64 %tmp33 to i32               ; <i32> [#uses=1]
  %tmp35 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv36 = trunc i64 %tmp35 to i32               ; <i32> [#uses=1]
  %tmp37 = load i64* %j.addr                      ; <i64> [#uses=1]
  %conv38 = trunc i64 %tmp37 to i32               ; <i32> [#uses=1]
  %tmp39 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv40 = trunc i64 %tmp39 to i32               ; <i32> [#uses=1]
  %tmp41 = mul i32 %conv40, 2                     ; <i32> [#uses=1]
  %tmp42 = add i32 %conv38, %tmp41                ; <i32> [#uses=1]
  %tmp43 = mul i32 %conv36, %tmp42                ; <i32> [#uses=1]
  %tmp44 = add i32 %conv34, %tmp43                ; <i32> [#uses=1]
  %conv45 = zext i32 %tmp44 to i64                ; <i64> [#uses=1]
  store i64 %conv45, i64* %idxuIV
  %tmp46 = load i64* %i                           ; <i64> [#uses=1]
  %tmp47 = add i64 %tmp46, 2                      ; <i64> [#uses=1]
  %conv48 = trunc i64 %tmp47 to i32               ; <i32> [#uses=1]
  %tmp49 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv50 = trunc i64 %tmp49 to i32               ; <i32> [#uses=1]
  %tmp51 = load i64* %j.addr                      ; <i64> [#uses=1]
  %conv52 = trunc i64 %tmp51 to i32               ; <i32> [#uses=1]
  %tmp53 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv54 = trunc i64 %tmp53 to i32               ; <i32> [#uses=1]
  %tmp55 = mul i32 %conv54, 3                     ; <i32> [#uses=1]
  %tmp56 = add i32 %conv52, %tmp55                ; <i32> [#uses=1]
  %tmp57 = mul i32 %conv50, %tmp56                ; <i32> [#uses=1]
  %tmp58 = add i32 %conv48, %tmp57                ; <i32> [#uses=1]
  %conv59 = zext i32 %tmp58 to i64                ; <i64> [#uses=1]
  store i64 %conv59, i64* %idxuIP
  %tmp60 = load i64* %i                           ; <i64> [#uses=1]
  %conv61 = trunc i64 %tmp60 to i32               ; <i32> [#uses=1]
  %tmp62 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv63 = trunc i64 %tmp62 to i32               ; <i32> [#uses=1]
  %tmp64 = mul i32 0, %conv63                     ; <i32> [#uses=1]
  %tmp65 = add i32 %conv61, %tmp64                ; <i32> [#uses=1]
  store i32 %tmp65, i32* %idxID
  %tmp66 = load i64* %i                           ; <i64> [#uses=1]
  %conv67 = trunc i64 %tmp66 to i32               ; <i32> [#uses=1]
  %tmp68 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv69 = trunc i64 %tmp68 to i32               ; <i32> [#uses=1]
  %tmp70 = mul i32 3, %conv69                     ; <i32> [#uses=1]
  %tmp71 = add i32 %conv67, %tmp70                ; <i32> [#uses=1]
  store i32 %tmp71, i32* %idxIP
  %tmp72 = load i64* %i                           ; <i64> [#uses=1]
  %conv73 = trunc i64 %tmp72 to i32               ; <i32> [#uses=1]
  %tmp74 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv75 = trunc i64 %tmp74 to i32               ; <i32> [#uses=1]
  %tmp76 = mul i32 1, %conv75                     ; <i32> [#uses=1]
  %tmp77 = add i32 %conv73, %tmp76                ; <i32> [#uses=1]
  store i32 %tmp77, i32* %idxIU
  %tmp78 = load i64* %i                           ; <i64> [#uses=1]
  %conv79 = trunc i64 %tmp78 to i32               ; <i32> [#uses=1]
  %tmp80 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv81 = trunc i64 %tmp80 to i32               ; <i32> [#uses=1]
  %tmp82 = mul i32 2, %conv81                     ; <i32> [#uses=1]
  %tmp83 = add i32 %conv79, %tmp82                ; <i32> [#uses=1]
  store i32 %tmp83, i32* %idxIV
  %tmp84 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp85 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp84, i32 %tmp85 ; <double addrspace(1)*> [#uses=1]
  %tmp86 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp87 = load i64* %idxuID                      ; <i64> [#uses=1]
  %conv88 = trunc i64 %tmp87 to i32               ; <i32> [#uses=1]
  %arrayidx89 = getelementptr double addrspace(1)* %tmp86, i32 %conv88 ; <double addrspace(1)*> [#uses=1]
  %tmp90 = load double addrspace(1)* %arrayidx89  ; <double> [#uses=1]
  %tmp91 = load double* %Hsmallr.addr             ; <double> [#uses=1]
  %call92 = call double @__max_f64(double %tmp90, double %tmp91) nounwind ; <double> [#uses=1]
  store double %call92, double addrspace(1)* %arrayidx
  %tmp93 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp94 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx95 = getelementptr double addrspace(1)* %tmp93, i32 %tmp94 ; <double addrspace(1)*> [#uses=1]
  %tmp96 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp97 = load i64* %idxuIU                      ; <i64> [#uses=1]
  %conv98 = trunc i64 %tmp97 to i32               ; <i32> [#uses=1]
  %arrayidx99 = getelementptr double addrspace(1)* %tmp96, i32 %conv98 ; <double addrspace(1)*> [#uses=1]
  %tmp100 = load double addrspace(1)* %arrayidx99 ; <double> [#uses=1]
  %tmp101 = load double addrspace(1)** %q.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp102 = load i32* %idxID                      ; <i32> [#uses=1]
  %arrayidx103 = getelementptr double addrspace(1)* %tmp101, i32 %tmp102 ; <double addrspace(1)*> [#uses=1]
  %tmp104 = load double addrspace(1)* %arrayidx103 ; <double> [#uses=1]
  %tmp105 = fdiv double %tmp100, %tmp104          ; <double> [#uses=1]
  store double %tmp105, double addrspace(1)* %arrayidx95
  %tmp106 = load double addrspace(1)** %q.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp107 = load i32* %idxIV                      ; <i32> [#uses=1]
  %arrayidx108 = getelementptr double addrspace(1)* %tmp106, i32 %tmp107 ; <double addrspace(1)*> [#uses=1]
  %tmp109 = load double addrspace(1)** %uold.addr ; <double addrspace(1)*> [#uses=1]
  %tmp110 = load i64* %idxuIV                     ; <i64> [#uses=1]
  %conv111 = trunc i64 %tmp110 to i32             ; <i32> [#uses=1]
  %arrayidx112 = getelementptr double addrspace(1)* %tmp109, i32 %conv111 ; <double addrspace(1)*> [#uses=1]
  %tmp113 = load double addrspace(1)* %arrayidx112 ; <double> [#uses=1]
  %tmp114 = load double addrspace(1)** %q.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp115 = load i32* %idxID                      ; <i32> [#uses=1]
  %arrayidx116 = getelementptr double addrspace(1)* %tmp114, i32 %tmp115 ; <double addrspace(1)*> [#uses=1]
  %tmp117 = load double addrspace(1)* %arrayidx116 ; <double> [#uses=1]
  %tmp118 = fdiv double %tmp113, %tmp117          ; <double> [#uses=1]
  store double %tmp118, double addrspace(1)* %arrayidx108
  %tmp119 = load double addrspace(1)** %q.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp120 = load i32* %idxIU                      ; <i32> [#uses=1]
  %arrayidx121 = getelementptr double addrspace(1)* %tmp119, i32 %tmp120 ; <double addrspace(1)*> [#uses=1]
  %tmp122 = load double addrspace(1)* %arrayidx121 ; <double> [#uses=1]
  store double %tmp122, double* %tmp
  %tmp123 = load double* %tmp                     ; <double> [#uses=1]
  %tmp124 = load double* %tmp                     ; <double> [#uses=1]
  %tmp125 = fmul double %tmp123, %tmp124          ; <double> [#uses=1]
  %tmp126 = load double addrspace(1)** %q.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp127 = load i32* %idxIV                      ; <i32> [#uses=1]
  %arrayidx128 = getelementptr double addrspace(1)* %tmp126, i32 %tmp127 ; <double addrspace(1)*> [#uses=1]
  %tmp129 = load double addrspace(1)* %arrayidx128 ; <double> [#uses=1]
  store double %tmp129, double* %tmp1
  %tmp130 = load double* %tmp1                    ; <double> [#uses=1]
  %tmp131 = load double* %tmp1                    ; <double> [#uses=1]
  %tmp132 = fmul double %tmp130, %tmp131          ; <double> [#uses=1]
  %tmp133 = fadd double %tmp125, %tmp132          ; <double> [#uses=1]
  %tmp134 = fmul double 5.000000e-01, %tmp133     ; <double> [#uses=1]
  store double %tmp134, double* %eken
  %tmp135 = load double addrspace(1)** %q.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp136 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx137 = getelementptr double addrspace(1)* %tmp135, i32 %tmp136 ; <double addrspace(1)*> [#uses=1]
  %tmp138 = load double addrspace(1)** %uold.addr ; <double addrspace(1)*> [#uses=1]
  %tmp139 = load i64* %idxuIP                     ; <i64> [#uses=1]
  %conv140 = trunc i64 %tmp139 to i32             ; <i32> [#uses=1]
  %arrayidx141 = getelementptr double addrspace(1)* %tmp138, i32 %conv140 ; <double addrspace(1)*> [#uses=1]
  %tmp142 = load double addrspace(1)* %arrayidx141 ; <double> [#uses=1]
  %tmp143 = load double addrspace(1)** %q.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp144 = load i32* %idxID                      ; <i32> [#uses=1]
  %arrayidx145 = getelementptr double addrspace(1)* %tmp143, i32 %tmp144 ; <double addrspace(1)*> [#uses=1]
  %tmp146 = load double addrspace(1)* %arrayidx145 ; <double> [#uses=1]
  %tmp147 = fdiv double %tmp142, %tmp146          ; <double> [#uses=1]
  %tmp148 = load double* %eken                    ; <double> [#uses=1]
  %tmp149 = fsub double %tmp147, %tmp148          ; <double> [#uses=1]
  store double %tmp149, double addrspace(1)* %arrayidx137
  %tmp150 = load double addrspace(1)** %e.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp151 = load i64* %i                          ; <i64> [#uses=1]
  %conv152 = trunc i64 %tmp151 to i32             ; <i32> [#uses=1]
  %arrayidx153 = getelementptr double addrspace(1)* %tmp150, i32 %conv152 ; <double addrspace(1)*> [#uses=1]
  %tmp154 = load double addrspace(1)** %q.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp155 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx156 = getelementptr double addrspace(1)* %tmp154, i32 %tmp155 ; <double addrspace(1)*> [#uses=1]
  %tmp157 = load double addrspace(1)* %arrayidx156 ; <double> [#uses=1]
  store double %tmp157, double addrspace(1)* %arrayidx153
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

declare double @__max_f64(double, double) nounwind

define void @__OpenCL_LoopKcourant_kernel(double addrspace(1)* %q, double addrspace(1)* %courant, double %Hsmallc, double addrspace(1)* %c, i64 %Hnxyt, i64 %n) nounwind {
entry:
  %q.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=3]
  %courant.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=3]
  %Hsmallc.addr = alloca double, align 8          ; <double*> [#uses=2]
  %c.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=3]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=5]
  %n.addr = alloca i64, align 8                   ; <i64*> [#uses=2]
  %cournox = alloca double, align 8               ; <double*> [#uses=3]
  %cournoy = alloca double, align 8               ; <double*> [#uses=3]
  %courantl = alloca double, align 8              ; <double*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=10]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=1]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=1]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=2]
  store double addrspace(1)* %q, double addrspace(1)** %q.addr
  store double addrspace(1)* %courant, double addrspace(1)** %courant.addr
  store double %Hsmallc, double* %Hsmallc.addr
  store double addrspace(1)* %c, double addrspace(1)** %c.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store i64 %n, i64* %n.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %n.addr                       ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %conv3 = trunc i64 %tmp2 to i32                 ; <i32> [#uses=1]
  %tmp4 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv5 = trunc i64 %tmp4 to i32                 ; <i32> [#uses=1]
  %tmp6 = mul i32 0, %conv5                       ; <i32> [#uses=1]
  %tmp7 = add i32 %conv3, %tmp6                   ; <i32> [#uses=1]
  store i32 %tmp7, i32* %idxID
  %tmp8 = load i64* %i                            ; <i64> [#uses=1]
  %conv9 = trunc i64 %tmp8 to i32                 ; <i32> [#uses=1]
  %tmp10 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv11 = trunc i64 %tmp10 to i32               ; <i32> [#uses=1]
  %tmp12 = mul i32 3, %conv11                     ; <i32> [#uses=1]
  %tmp13 = add i32 %conv9, %tmp12                 ; <i32> [#uses=1]
  store i32 %tmp13, i32* %idxIP
  %tmp14 = load i64* %i                           ; <i64> [#uses=1]
  %conv15 = trunc i64 %tmp14 to i32               ; <i32> [#uses=1]
  %tmp16 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv17 = trunc i64 %tmp16 to i32               ; <i32> [#uses=1]
  %tmp18 = mul i32 1, %conv17                     ; <i32> [#uses=1]
  %tmp19 = add i32 %conv15, %tmp18                ; <i32> [#uses=1]
  store i32 %tmp19, i32* %idxIU
  %tmp20 = load i64* %i                           ; <i64> [#uses=1]
  %conv21 = trunc i64 %tmp20 to i32               ; <i32> [#uses=1]
  %tmp22 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv23 = trunc i64 %tmp22 to i32               ; <i32> [#uses=1]
  %tmp24 = mul i32 2, %conv23                     ; <i32> [#uses=1]
  %tmp25 = add i32 %conv21, %tmp24                ; <i32> [#uses=1]
  store i32 %tmp25, i32* %idxIV
  store double 0.000000e+00, double* %cournoy
  store double 0.000000e+00, double* %cournox
  %tmp26 = load double addrspace(1)** %c.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp27 = load i64* %i                           ; <i64> [#uses=1]
  %conv28 = trunc i64 %tmp27 to i32               ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp26, i32 %conv28 ; <double addrspace(1)*> [#uses=1]
  %tmp29 = load double addrspace(1)* %arrayidx    ; <double> [#uses=1]
  %tmp30 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp31 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx32 = getelementptr double addrspace(1)* %tmp30, i32 %tmp31 ; <double addrspace(1)*> [#uses=1]
  %tmp33 = load double addrspace(1)* %arrayidx32  ; <double> [#uses=1]
  %call34 = call double @__fabs_f64(double %tmp33) nounwind ; <double> [#uses=1]
  %tmp35 = fadd double %tmp29, %call34            ; <double> [#uses=1]
  store double %tmp35, double* %cournox
  %tmp36 = load double addrspace(1)** %c.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp37 = load i64* %i                           ; <i64> [#uses=1]
  %conv38 = trunc i64 %tmp37 to i32               ; <i32> [#uses=1]
  %arrayidx39 = getelementptr double addrspace(1)* %tmp36, i32 %conv38 ; <double addrspace(1)*> [#uses=1]
  %tmp40 = load double addrspace(1)* %arrayidx39  ; <double> [#uses=1]
  %tmp41 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp42 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx43 = getelementptr double addrspace(1)* %tmp41, i32 %tmp42 ; <double addrspace(1)*> [#uses=1]
  %tmp44 = load double addrspace(1)* %arrayidx43  ; <double> [#uses=1]
  %call45 = call double @__fabs_f64(double %tmp44) nounwind ; <double> [#uses=1]
  %tmp46 = fadd double %tmp40, %call45            ; <double> [#uses=1]
  store double %tmp46, double* %cournoy
  %tmp47 = load double* %cournox                  ; <double> [#uses=1]
  %tmp48 = load double* %cournoy                  ; <double> [#uses=1]
  %tmp49 = load double* %Hsmallc.addr             ; <double> [#uses=1]
  %call50 = call double @__max_f64(double %tmp48, double %tmp49) nounwind ; <double> [#uses=1]
  %call51 = call double @__max_f64(double %tmp47, double %call50) nounwind ; <double> [#uses=1]
  store double %call51, double* %courantl
  %tmp52 = load double addrspace(1)** %courant.addr ; <double addrspace(1)*> [#uses=1]
  %tmp53 = load i64* %i                           ; <i64> [#uses=1]
  %conv54 = trunc i64 %tmp53 to i32               ; <i32> [#uses=1]
  %arrayidx55 = getelementptr double addrspace(1)* %tmp52, i32 %conv54 ; <double addrspace(1)*> [#uses=1]
  %tmp56 = load double addrspace(1)** %courant.addr ; <double addrspace(1)*> [#uses=1]
  %tmp57 = load i64* %i                           ; <i64> [#uses=1]
  %conv58 = trunc i64 %tmp57 to i32               ; <i32> [#uses=1]
  %arrayidx59 = getelementptr double addrspace(1)* %tmp56, i32 %conv58 ; <double addrspace(1)*> [#uses=1]
  %tmp60 = load double addrspace(1)* %arrayidx59  ; <double> [#uses=1]
  %tmp61 = load double* %courantl                 ; <double> [#uses=1]
  %call62 = call double @__max_f64(double %tmp60, double %tmp61) nounwind ; <double> [#uses=1]
  store double %call62, double addrspace(1)* %arrayidx55
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

declare double @__fabs_f64(double) nounwind

define void @__OpenCL_Loop1KcuGather_kernel(double addrspace(1)* %uold, double addrspace(1)* %u, i64 %rowcol, i64 %Hnxt, i64 %Himin, i64 %Himax, i64 %Hnyt, i64 %Hnxyt) nounwind {
entry:
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=5]
  %rowcol.addr = alloca i64, align 8              ; <i64*> [#uses=5]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Himin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Himax.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=5]
  %i = alloca i64, align 8                        ; <i64*> [#uses=11]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=2]
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store i64 %rowcol, i64* %rowcol.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Himin, i64* %Himin.addr
  store i64 %Himax, i64* %Himax.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %Himin.addr                   ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end5, %if.then6, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %tmp3 = load i64* %Himax.addr                   ; <i64> [#uses=1]
  %cmp4 = icmp sge i64 %tmp2, %tmp3               ; <i1> [#uses=1]
  br i1 %cmp4, label %if.then6, label %if.end5

if.then:                                          ; preds = %entry
  br label %return

if.end5:                                          ; preds = %if.end
  %tmp7 = load i64* %i                            ; <i64> [#uses=1]
  %conv8 = trunc i64 %tmp7 to i32                 ; <i32> [#uses=1]
  %tmp9 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv10 = trunc i64 %tmp9 to i32                ; <i32> [#uses=1]
  %tmp11 = mul i32 0, %conv10                     ; <i32> [#uses=1]
  %tmp12 = add i32 %conv8, %tmp11                 ; <i32> [#uses=1]
  store i32 %tmp12, i32* %idxID
  %tmp13 = load i64* %i                           ; <i64> [#uses=1]
  %conv14 = trunc i64 %tmp13 to i32               ; <i32> [#uses=1]
  %tmp15 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %tmp17 = mul i32 3, %conv16                     ; <i32> [#uses=1]
  %tmp18 = add i32 %conv14, %tmp17                ; <i32> [#uses=1]
  store i32 %tmp18, i32* %idxIP
  %tmp19 = load i64* %i                           ; <i64> [#uses=1]
  %conv20 = trunc i64 %tmp19 to i32               ; <i32> [#uses=1]
  %tmp21 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv22 = trunc i64 %tmp21 to i32               ; <i32> [#uses=1]
  %tmp23 = mul i32 1, %conv22                     ; <i32> [#uses=1]
  %tmp24 = add i32 %conv20, %tmp23                ; <i32> [#uses=1]
  store i32 %tmp24, i32* %idxIU
  %tmp25 = load i64* %i                           ; <i64> [#uses=1]
  %conv26 = trunc i64 %tmp25 to i32               ; <i32> [#uses=1]
  %tmp27 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv28 = trunc i64 %tmp27 to i32               ; <i32> [#uses=1]
  %tmp29 = mul i32 2, %conv28                     ; <i32> [#uses=1]
  %tmp30 = add i32 %conv26, %tmp29                ; <i32> [#uses=1]
  store i32 %tmp30, i32* %idxIV
  %tmp31 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp32 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp31, i32 %tmp32 ; <double addrspace(1)*> [#uses=1]
  %tmp33 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp34 = load i64* %i                           ; <i64> [#uses=1]
  %conv35 = trunc i64 %tmp34 to i32               ; <i32> [#uses=1]
  %tmp36 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv37 = trunc i64 %tmp36 to i32               ; <i32> [#uses=1]
  %tmp38 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv39 = trunc i64 %tmp38 to i32               ; <i32> [#uses=1]
  %tmp40 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv41 = trunc i64 %tmp40 to i32               ; <i32> [#uses=1]
  %tmp42 = mul i32 %conv41, 0                     ; <i32> [#uses=1]
  %tmp43 = add i32 %conv39, %tmp42                ; <i32> [#uses=1]
  %tmp44 = mul i32 %conv37, %tmp43                ; <i32> [#uses=1]
  %tmp45 = add i32 %conv35, %tmp44                ; <i32> [#uses=1]
  %arrayidx46 = getelementptr double addrspace(1)* %tmp33, i32 %tmp45 ; <double addrspace(1)*> [#uses=1]
  %tmp47 = load double addrspace(1)* %arrayidx46  ; <double> [#uses=1]
  store double %tmp47, double addrspace(1)* %arrayidx
  %tmp48 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp49 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx50 = getelementptr double addrspace(1)* %tmp48, i32 %tmp49 ; <double addrspace(1)*> [#uses=1]
  %tmp51 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp52 = load i64* %i                           ; <i64> [#uses=1]
  %conv53 = trunc i64 %tmp52 to i32               ; <i32> [#uses=1]
  %tmp54 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv55 = trunc i64 %tmp54 to i32               ; <i32> [#uses=1]
  %tmp56 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv57 = trunc i64 %tmp56 to i32               ; <i32> [#uses=1]
  %tmp58 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv59 = trunc i64 %tmp58 to i32               ; <i32> [#uses=1]
  %tmp60 = mul i32 %conv59, 1                     ; <i32> [#uses=1]
  %tmp61 = add i32 %conv57, %tmp60                ; <i32> [#uses=1]
  %tmp62 = mul i32 %conv55, %tmp61                ; <i32> [#uses=1]
  %tmp63 = add i32 %conv53, %tmp62                ; <i32> [#uses=1]
  %arrayidx64 = getelementptr double addrspace(1)* %tmp51, i32 %tmp63 ; <double addrspace(1)*> [#uses=1]
  %tmp65 = load double addrspace(1)* %arrayidx64  ; <double> [#uses=1]
  store double %tmp65, double addrspace(1)* %arrayidx50
  %tmp66 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp67 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx68 = getelementptr double addrspace(1)* %tmp66, i32 %tmp67 ; <double addrspace(1)*> [#uses=1]
  %tmp69 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp70 = load i64* %i                           ; <i64> [#uses=1]
  %conv71 = trunc i64 %tmp70 to i32               ; <i32> [#uses=1]
  %tmp72 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv73 = trunc i64 %tmp72 to i32               ; <i32> [#uses=1]
  %tmp74 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv75 = trunc i64 %tmp74 to i32               ; <i32> [#uses=1]
  %tmp76 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv77 = trunc i64 %tmp76 to i32               ; <i32> [#uses=1]
  %tmp78 = mul i32 %conv77, 2                     ; <i32> [#uses=1]
  %tmp79 = add i32 %conv75, %tmp78                ; <i32> [#uses=1]
  %tmp80 = mul i32 %conv73, %tmp79                ; <i32> [#uses=1]
  %tmp81 = add i32 %conv71, %tmp80                ; <i32> [#uses=1]
  %arrayidx82 = getelementptr double addrspace(1)* %tmp69, i32 %tmp81 ; <double addrspace(1)*> [#uses=1]
  %tmp83 = load double addrspace(1)* %arrayidx82  ; <double> [#uses=1]
  store double %tmp83, double addrspace(1)* %arrayidx68
  %tmp84 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp85 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx86 = getelementptr double addrspace(1)* %tmp84, i32 %tmp85 ; <double addrspace(1)*> [#uses=1]
  %tmp87 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp88 = load i64* %i                           ; <i64> [#uses=1]
  %conv89 = trunc i64 %tmp88 to i32               ; <i32> [#uses=1]
  %tmp90 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv91 = trunc i64 %tmp90 to i32               ; <i32> [#uses=1]
  %tmp92 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv93 = trunc i64 %tmp92 to i32               ; <i32> [#uses=1]
  %tmp94 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv95 = trunc i64 %tmp94 to i32               ; <i32> [#uses=1]
  %tmp96 = mul i32 %conv95, 3                     ; <i32> [#uses=1]
  %tmp97 = add i32 %conv93, %tmp96                ; <i32> [#uses=1]
  %tmp98 = mul i32 %conv91, %tmp97                ; <i32> [#uses=1]
  %tmp99 = add i32 %conv89, %tmp98                ; <i32> [#uses=1]
  %arrayidx100 = getelementptr double addrspace(1)* %tmp87, i32 %tmp99 ; <double addrspace(1)*> [#uses=1]
  %tmp101 = load double addrspace(1)* %arrayidx100 ; <double> [#uses=1]
  store double %tmp101, double addrspace(1)* %arrayidx86
  br label %return

if.then6:                                         ; preds = %if.end
  br label %return
}

define void @__OpenCL_Loop2KcuGather_kernel(double addrspace(1)* %uold, double addrspace(1)* %u, i64 %rowcol, i64 %Hnxt, i64 %Himin, i64 %Himax, i64 %Hnyt, i64 %Hnxyt) nounwind {
entry:
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=5]
  %rowcol.addr = alloca i64, align 8              ; <i64*> [#uses=5]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Himin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Himax.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=5]
  %i = alloca i64, align 8                        ; <i64*> [#uses=11]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=2]
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store i64 %rowcol, i64* %rowcol.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Himin, i64* %Himin.addr
  store i64 %Himax, i64* %Himax.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %Himin.addr                   ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end5, %if.then6, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %tmp3 = load i64* %Himax.addr                   ; <i64> [#uses=1]
  %cmp4 = icmp sge i64 %tmp2, %tmp3               ; <i1> [#uses=1]
  br i1 %cmp4, label %if.then6, label %if.end5

if.then:                                          ; preds = %entry
  br label %return

if.end5:                                          ; preds = %if.end
  %tmp7 = load i64* %i                            ; <i64> [#uses=1]
  %conv8 = trunc i64 %tmp7 to i32                 ; <i32> [#uses=1]
  %tmp9 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv10 = trunc i64 %tmp9 to i32                ; <i32> [#uses=1]
  %tmp11 = mul i32 0, %conv10                     ; <i32> [#uses=1]
  %tmp12 = add i32 %conv8, %tmp11                 ; <i32> [#uses=1]
  store i32 %tmp12, i32* %idxID
  %tmp13 = load i64* %i                           ; <i64> [#uses=1]
  %conv14 = trunc i64 %tmp13 to i32               ; <i32> [#uses=1]
  %tmp15 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %tmp17 = mul i32 3, %conv16                     ; <i32> [#uses=1]
  %tmp18 = add i32 %conv14, %tmp17                ; <i32> [#uses=1]
  store i32 %tmp18, i32* %idxIP
  %tmp19 = load i64* %i                           ; <i64> [#uses=1]
  %conv20 = trunc i64 %tmp19 to i32               ; <i32> [#uses=1]
  %tmp21 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv22 = trunc i64 %tmp21 to i32               ; <i32> [#uses=1]
  %tmp23 = mul i32 1, %conv22                     ; <i32> [#uses=1]
  %tmp24 = add i32 %conv20, %tmp23                ; <i32> [#uses=1]
  store i32 %tmp24, i32* %idxIU
  %tmp25 = load i64* %i                           ; <i64> [#uses=1]
  %conv26 = trunc i64 %tmp25 to i32               ; <i32> [#uses=1]
  %tmp27 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv28 = trunc i64 %tmp27 to i32               ; <i32> [#uses=1]
  %tmp29 = mul i32 2, %conv28                     ; <i32> [#uses=1]
  %tmp30 = add i32 %conv26, %tmp29                ; <i32> [#uses=1]
  store i32 %tmp30, i32* %idxIV
  %tmp31 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp32 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp31, i32 %tmp32 ; <double addrspace(1)*> [#uses=1]
  %tmp33 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp34 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv35 = trunc i64 %tmp34 to i32               ; <i32> [#uses=1]
  %tmp36 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv37 = trunc i64 %tmp36 to i32               ; <i32> [#uses=1]
  %tmp38 = load i64* %i                           ; <i64> [#uses=1]
  %conv39 = trunc i64 %tmp38 to i32               ; <i32> [#uses=1]
  %tmp40 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv41 = trunc i64 %tmp40 to i32               ; <i32> [#uses=1]
  %tmp42 = mul i32 %conv41, 0                     ; <i32> [#uses=1]
  %tmp43 = add i32 %conv39, %tmp42                ; <i32> [#uses=1]
  %tmp44 = mul i32 %conv37, %tmp43                ; <i32> [#uses=1]
  %tmp45 = add i32 %conv35, %tmp44                ; <i32> [#uses=1]
  %arrayidx46 = getelementptr double addrspace(1)* %tmp33, i32 %tmp45 ; <double addrspace(1)*> [#uses=1]
  %tmp47 = load double addrspace(1)* %arrayidx46  ; <double> [#uses=1]
  store double %tmp47, double addrspace(1)* %arrayidx
  %tmp48 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp49 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx50 = getelementptr double addrspace(1)* %tmp48, i32 %tmp49 ; <double addrspace(1)*> [#uses=1]
  %tmp51 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp52 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv53 = trunc i64 %tmp52 to i32               ; <i32> [#uses=1]
  %tmp54 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv55 = trunc i64 %tmp54 to i32               ; <i32> [#uses=1]
  %tmp56 = load i64* %i                           ; <i64> [#uses=1]
  %conv57 = trunc i64 %tmp56 to i32               ; <i32> [#uses=1]
  %tmp58 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv59 = trunc i64 %tmp58 to i32               ; <i32> [#uses=1]
  %tmp60 = mul i32 %conv59, 1                     ; <i32> [#uses=1]
  %tmp61 = add i32 %conv57, %tmp60                ; <i32> [#uses=1]
  %tmp62 = mul i32 %conv55, %tmp61                ; <i32> [#uses=1]
  %tmp63 = add i32 %conv53, %tmp62                ; <i32> [#uses=1]
  %arrayidx64 = getelementptr double addrspace(1)* %tmp51, i32 %tmp63 ; <double addrspace(1)*> [#uses=1]
  %tmp65 = load double addrspace(1)* %arrayidx64  ; <double> [#uses=1]
  store double %tmp65, double addrspace(1)* %arrayidx50
  %tmp66 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp67 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx68 = getelementptr double addrspace(1)* %tmp66, i32 %tmp67 ; <double addrspace(1)*> [#uses=1]
  %tmp69 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp70 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv71 = trunc i64 %tmp70 to i32               ; <i32> [#uses=1]
  %tmp72 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv73 = trunc i64 %tmp72 to i32               ; <i32> [#uses=1]
  %tmp74 = load i64* %i                           ; <i64> [#uses=1]
  %conv75 = trunc i64 %tmp74 to i32               ; <i32> [#uses=1]
  %tmp76 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv77 = trunc i64 %tmp76 to i32               ; <i32> [#uses=1]
  %tmp78 = mul i32 %conv77, 2                     ; <i32> [#uses=1]
  %tmp79 = add i32 %conv75, %tmp78                ; <i32> [#uses=1]
  %tmp80 = mul i32 %conv73, %tmp79                ; <i32> [#uses=1]
  %tmp81 = add i32 %conv71, %tmp80                ; <i32> [#uses=1]
  %arrayidx82 = getelementptr double addrspace(1)* %tmp69, i32 %tmp81 ; <double addrspace(1)*> [#uses=1]
  %tmp83 = load double addrspace(1)* %arrayidx82  ; <double> [#uses=1]
  store double %tmp83, double addrspace(1)* %arrayidx68
  %tmp84 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp85 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx86 = getelementptr double addrspace(1)* %tmp84, i32 %tmp85 ; <double addrspace(1)*> [#uses=1]
  %tmp87 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp88 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv89 = trunc i64 %tmp88 to i32               ; <i32> [#uses=1]
  %tmp90 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv91 = trunc i64 %tmp90 to i32               ; <i32> [#uses=1]
  %tmp92 = load i64* %i                           ; <i64> [#uses=1]
  %conv93 = trunc i64 %tmp92 to i32               ; <i32> [#uses=1]
  %tmp94 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv95 = trunc i64 %tmp94 to i32               ; <i32> [#uses=1]
  %tmp96 = mul i32 %conv95, 3                     ; <i32> [#uses=1]
  %tmp97 = add i32 %conv93, %tmp96                ; <i32> [#uses=1]
  %tmp98 = mul i32 %conv91, %tmp97                ; <i32> [#uses=1]
  %tmp99 = add i32 %conv89, %tmp98                ; <i32> [#uses=1]
  %arrayidx100 = getelementptr double addrspace(1)* %tmp87, i32 %tmp99 ; <double addrspace(1)*> [#uses=1]
  %tmp101 = load double addrspace(1)* %arrayidx100 ; <double> [#uses=1]
  store double %tmp101, double addrspace(1)* %arrayidx86
  br label %return

if.then6:                                         ; preds = %if.end
  br label %return
}

define void @__OpenCL_Loop3KcuGather_kernel(double addrspace(1)* %uold, double addrspace(1)* %u, i64 %rowcol, i64 %Hnxt, i64 %Himin, i64 %Himax, i64 %Hnyt, i64 %Hnxyt, i64 %Hnvar) nounwind {
entry:
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %rowcol.addr = alloca i64, align 8              ; <i64*> [#uses=2]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Himin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Himax.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=5]
  %ivar = alloca i64, align 8                     ; <i64*> [#uses=6]
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store i64 %rowcol, i64* %rowcol.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Himin, i64* %Himin.addr
  store i64 %Himax, i64* %Himax.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %Himin.addr                   ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %for.exit, %if.then6, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %tmp3 = load i64* %Himax.addr                   ; <i64> [#uses=1]
  %cmp4 = icmp sge i64 %tmp2, %tmp3               ; <i1> [#uses=1]
  br i1 %cmp4, label %if.then6, label %if.end5

if.then:                                          ; preds = %entry
  br label %return

if.end5:                                          ; preds = %if.end
  store i64 4, i64* %ivar
  br label %for.cond

if.then6:                                         ; preds = %if.end
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end5
  %tmp7 = load i64* %ivar                         ; <i64> [#uses=1]
  %tmp8 = load i64* %Hnvar.addr                   ; <i64> [#uses=1]
  %cmp9 = icmp slt i64 %tmp7, %tmp8               ; <i1> [#uses=1]
  br i1 %cmp9, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %return

for.body:                                         ; preds = %for.cond
  %tmp10 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp11 = load i64* %i                           ; <i64> [#uses=1]
  %conv12 = trunc i64 %tmp11 to i32               ; <i32> [#uses=1]
  %tmp13 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv14 = trunc i64 %tmp13 to i32               ; <i32> [#uses=1]
  %tmp15 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %tmp17 = mul i32 %conv14, %conv16               ; <i32> [#uses=1]
  %tmp18 = add i32 %conv12, %tmp17                ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp10, i32 %tmp18 ; <double addrspace(1)*> [#uses=1]
  %tmp19 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp20 = load i64* %i                           ; <i64> [#uses=1]
  %conv21 = trunc i64 %tmp20 to i32               ; <i32> [#uses=1]
  %tmp22 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv23 = trunc i64 %tmp22 to i32               ; <i32> [#uses=1]
  %tmp24 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv25 = trunc i64 %tmp24 to i32               ; <i32> [#uses=1]
  %tmp26 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv27 = trunc i64 %tmp26 to i32               ; <i32> [#uses=1]
  %tmp28 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv29 = trunc i64 %tmp28 to i32               ; <i32> [#uses=1]
  %tmp30 = mul i32 %conv27, %conv29               ; <i32> [#uses=1]
  %tmp31 = add i32 %conv25, %tmp30                ; <i32> [#uses=1]
  %tmp32 = mul i32 %conv23, %tmp31                ; <i32> [#uses=1]
  %tmp33 = add i32 %conv21, %tmp32                ; <i32> [#uses=1]
  %arrayidx34 = getelementptr double addrspace(1)* %tmp19, i32 %tmp33 ; <double addrspace(1)*> [#uses=1]
  %tmp35 = load double addrspace(1)* %arrayidx34  ; <double> [#uses=1]
  store double %tmp35, double addrspace(1)* %arrayidx
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %tmp36 = load i64* %ivar                        ; <i64> [#uses=1]
  %tmp37 = add i64 %tmp36, 1                      ; <i64> [#uses=1]
  store i64 %tmp37, i64* %ivar
  br label %for.cond
}

define void @__OpenCL_Loop4KcuGather_kernel(double addrspace(1)* %uold, double addrspace(1)* %u, i64 %rowcol, i64 %Hnxt, i64 %Himin, i64 %Himax, i64 %Hnyt, i64 %Hnxyt, i64 %Hnvar) nounwind {
entry:
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %rowcol.addr = alloca i64, align 8              ; <i64*> [#uses=2]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Himin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Himax.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=5]
  %ivar = alloca i64, align 8                     ; <i64*> [#uses=6]
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store i64 %rowcol, i64* %rowcol.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Himin, i64* %Himin.addr
  store i64 %Himax, i64* %Himax.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %Himin.addr                   ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %for.exit, %if.then6, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %tmp3 = load i64* %Himax.addr                   ; <i64> [#uses=1]
  %cmp4 = icmp sge i64 %tmp2, %tmp3               ; <i1> [#uses=1]
  br i1 %cmp4, label %if.then6, label %if.end5

if.then:                                          ; preds = %entry
  br label %return

if.end5:                                          ; preds = %if.end
  store i64 4, i64* %ivar
  br label %for.cond

if.then6:                                         ; preds = %if.end
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end5
  %tmp7 = load i64* %ivar                         ; <i64> [#uses=1]
  %tmp8 = load i64* %Hnvar.addr                   ; <i64> [#uses=1]
  %cmp9 = icmp slt i64 %tmp7, %tmp8               ; <i1> [#uses=1]
  br i1 %cmp9, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %return

for.body:                                         ; preds = %for.cond
  %tmp10 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp11 = load i64* %i                           ; <i64> [#uses=1]
  %conv12 = trunc i64 %tmp11 to i32               ; <i32> [#uses=1]
  %tmp13 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv14 = trunc i64 %tmp13 to i32               ; <i32> [#uses=1]
  %tmp15 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %tmp17 = mul i32 %conv14, %conv16               ; <i32> [#uses=1]
  %tmp18 = add i32 %conv12, %tmp17                ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp10, i32 %tmp18 ; <double addrspace(1)*> [#uses=1]
  %tmp19 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp20 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv21 = trunc i64 %tmp20 to i32               ; <i32> [#uses=1]
  %tmp22 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv23 = trunc i64 %tmp22 to i32               ; <i32> [#uses=1]
  %tmp24 = load i64* %i                           ; <i64> [#uses=1]
  %conv25 = trunc i64 %tmp24 to i32               ; <i32> [#uses=1]
  %tmp26 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv27 = trunc i64 %tmp26 to i32               ; <i32> [#uses=1]
  %tmp28 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv29 = trunc i64 %tmp28 to i32               ; <i32> [#uses=1]
  %tmp30 = mul i32 %conv27, %conv29               ; <i32> [#uses=1]
  %tmp31 = add i32 %conv25, %tmp30                ; <i32> [#uses=1]
  %tmp32 = mul i32 %conv23, %tmp31                ; <i32> [#uses=1]
  %tmp33 = add i32 %conv21, %tmp32                ; <i32> [#uses=1]
  %arrayidx34 = getelementptr double addrspace(1)* %tmp19, i32 %tmp33 ; <double addrspace(1)*> [#uses=1]
  %tmp35 = load double addrspace(1)* %arrayidx34  ; <double> [#uses=1]
  store double %tmp35, double addrspace(1)* %arrayidx
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %tmp36 = load i64* %ivar                        ; <i64> [#uses=1]
  %tmp37 = add i64 %tmp36, 1                      ; <i64> [#uses=1]
  store i64 %tmp37, i64* %ivar
  br label %for.cond
}

define void @__OpenCL_Loop1KcuUpdate_kernel(i64 %rowcol, double %dtdx, double addrspace(1)* %uold, double addrspace(1)* %u, double addrspace(1)* %flux, i64 %Himin, i64 %Himax, i64 %Hnxt, i64 %Hnyt, i64 %Hnxyt) nounwind {
entry:
  %rowcol.addr = alloca i64, align 8              ; <i64*> [#uses=5]
  %dtdx.addr = alloca double, align 8             ; <double*> [#uses=5]
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=5]
  %flux.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=9]
  %Himin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Himax.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=13]
  %i = alloca i64, align 8                        ; <i64*> [#uses=19]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=2]
  store i64 %rowcol, i64* %rowcol.addr
  store double %dtdx, double* %dtdx.addr
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store double addrspace(1)* %flux, double addrspace(1)** %flux.addr
  store i64 %Himin, i64* %Himin.addr
  store i64 %Himax, i64* %Himax.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %Himin.addr                   ; <i64> [#uses=1]
  %tmp2 = add i64 %tmp1, 2                        ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp2                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end7, %if.then8, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp3 = load i64* %i                            ; <i64> [#uses=1]
  %tmp4 = load i64* %Himax.addr                   ; <i64> [#uses=1]
  %tmp5 = sub i64 %tmp4, 2                        ; <i64> [#uses=1]
  %cmp6 = icmp sge i64 %tmp3, %tmp5               ; <i1> [#uses=1]
  br i1 %cmp6, label %if.then8, label %if.end7

if.then:                                          ; preds = %entry
  br label %return

if.end7:                                          ; preds = %if.end
  %tmp9 = load i64* %i                            ; <i64> [#uses=1]
  %conv10 = trunc i64 %tmp9 to i32                ; <i32> [#uses=1]
  %tmp11 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv12 = trunc i64 %tmp11 to i32               ; <i32> [#uses=1]
  %tmp13 = mul i32 0, %conv12                     ; <i32> [#uses=1]
  %tmp14 = add i32 %conv10, %tmp13                ; <i32> [#uses=1]
  store i32 %tmp14, i32* %idxID
  %tmp15 = load i64* %i                           ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %tmp17 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv18 = trunc i64 %tmp17 to i32               ; <i32> [#uses=1]
  %tmp19 = mul i32 3, %conv18                     ; <i32> [#uses=1]
  %tmp20 = add i32 %conv16, %tmp19                ; <i32> [#uses=1]
  store i32 %tmp20, i32* %idxIP
  %tmp21 = load i64* %i                           ; <i64> [#uses=1]
  %conv22 = trunc i64 %tmp21 to i32               ; <i32> [#uses=1]
  %tmp23 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv24 = trunc i64 %tmp23 to i32               ; <i32> [#uses=1]
  %tmp25 = mul i32 1, %conv24                     ; <i32> [#uses=1]
  %tmp26 = add i32 %conv22, %tmp25                ; <i32> [#uses=1]
  store i32 %tmp26, i32* %idxIU
  %tmp27 = load i64* %i                           ; <i64> [#uses=1]
  %conv28 = trunc i64 %tmp27 to i32               ; <i32> [#uses=1]
  %tmp29 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv30 = trunc i64 %tmp29 to i32               ; <i32> [#uses=1]
  %tmp31 = mul i32 2, %conv30                     ; <i32> [#uses=1]
  %tmp32 = add i32 %conv28, %tmp31                ; <i32> [#uses=1]
  store i32 %tmp32, i32* %idxIV
  %tmp33 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp34 = load i64* %i                           ; <i64> [#uses=1]
  %conv35 = trunc i64 %tmp34 to i32               ; <i32> [#uses=1]
  %tmp36 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv37 = trunc i64 %tmp36 to i32               ; <i32> [#uses=1]
  %tmp38 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv39 = trunc i64 %tmp38 to i32               ; <i32> [#uses=1]
  %tmp40 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv41 = trunc i64 %tmp40 to i32               ; <i32> [#uses=1]
  %tmp42 = mul i32 %conv41, 0                     ; <i32> [#uses=1]
  %tmp43 = add i32 %conv39, %tmp42                ; <i32> [#uses=1]
  %tmp44 = mul i32 %conv37, %tmp43                ; <i32> [#uses=1]
  %tmp45 = add i32 %conv35, %tmp44                ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp33, i32 %tmp45 ; <double addrspace(1)*> [#uses=1]
  %tmp46 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp47 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx48 = getelementptr double addrspace(1)* %tmp46, i32 %tmp47 ; <double addrspace(1)*> [#uses=1]
  %tmp49 = load double addrspace(1)* %arrayidx48  ; <double> [#uses=1]
  %tmp50 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp51 = load i64* %i                           ; <i64> [#uses=1]
  %tmp52 = sub i64 %tmp51, 2                      ; <i64> [#uses=1]
  %conv53 = trunc i64 %tmp52 to i32               ; <i32> [#uses=1]
  %tmp54 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv55 = trunc i64 %tmp54 to i32               ; <i32> [#uses=1]
  %tmp56 = mul i32 0, %conv55                     ; <i32> [#uses=1]
  %tmp57 = add i32 %conv53, %tmp56                ; <i32> [#uses=1]
  %arrayidx58 = getelementptr double addrspace(1)* %tmp50, i32 %tmp57 ; <double addrspace(1)*> [#uses=1]
  %tmp59 = load double addrspace(1)* %arrayidx58  ; <double> [#uses=1]
  %tmp60 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load i64* %i                           ; <i64> [#uses=1]
  %tmp62 = sub i64 %tmp61, 1                      ; <i64> [#uses=1]
  %conv63 = trunc i64 %tmp62 to i32               ; <i32> [#uses=1]
  %tmp64 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv65 = trunc i64 %tmp64 to i32               ; <i32> [#uses=1]
  %tmp66 = mul i32 0, %conv65                     ; <i32> [#uses=1]
  %tmp67 = add i32 %conv63, %tmp66                ; <i32> [#uses=1]
  %arrayidx68 = getelementptr double addrspace(1)* %tmp60, i32 %tmp67 ; <double addrspace(1)*> [#uses=1]
  %tmp69 = load double addrspace(1)* %arrayidx68  ; <double> [#uses=1]
  %tmp70 = fsub double %tmp59, %tmp69             ; <double> [#uses=1]
  %tmp71 = load double* %dtdx.addr                ; <double> [#uses=1]
  %tmp72 = fmul double %tmp70, %tmp71             ; <double> [#uses=1]
  %tmp73 = fadd double %tmp49, %tmp72             ; <double> [#uses=1]
  store double %tmp73, double addrspace(1)* %arrayidx
  %tmp74 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp75 = load i64* %i                           ; <i64> [#uses=1]
  %conv76 = trunc i64 %tmp75 to i32               ; <i32> [#uses=1]
  %tmp77 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv78 = trunc i64 %tmp77 to i32               ; <i32> [#uses=1]
  %tmp79 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv80 = trunc i64 %tmp79 to i32               ; <i32> [#uses=1]
  %tmp81 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv82 = trunc i64 %tmp81 to i32               ; <i32> [#uses=1]
  %tmp83 = mul i32 %conv82, 1                     ; <i32> [#uses=1]
  %tmp84 = add i32 %conv80, %tmp83                ; <i32> [#uses=1]
  %tmp85 = mul i32 %conv78, %tmp84                ; <i32> [#uses=1]
  %tmp86 = add i32 %conv76, %tmp85                ; <i32> [#uses=1]
  %arrayidx87 = getelementptr double addrspace(1)* %tmp74, i32 %tmp86 ; <double addrspace(1)*> [#uses=1]
  %tmp88 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp89 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx90 = getelementptr double addrspace(1)* %tmp88, i32 %tmp89 ; <double addrspace(1)*> [#uses=1]
  %tmp91 = load double addrspace(1)* %arrayidx90  ; <double> [#uses=1]
  %tmp92 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp93 = load i64* %i                           ; <i64> [#uses=1]
  %tmp94 = sub i64 %tmp93, 2                      ; <i64> [#uses=1]
  %conv95 = trunc i64 %tmp94 to i32               ; <i32> [#uses=1]
  %tmp96 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv97 = trunc i64 %tmp96 to i32               ; <i32> [#uses=1]
  %tmp98 = mul i32 1, %conv97                     ; <i32> [#uses=1]
  %tmp99 = add i32 %conv95, %tmp98                ; <i32> [#uses=1]
  %arrayidx100 = getelementptr double addrspace(1)* %tmp92, i32 %tmp99 ; <double addrspace(1)*> [#uses=1]
  %tmp101 = load double addrspace(1)* %arrayidx100 ; <double> [#uses=1]
  %tmp102 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp103 = load i64* %i                          ; <i64> [#uses=1]
  %tmp104 = sub i64 %tmp103, 1                    ; <i64> [#uses=1]
  %conv105 = trunc i64 %tmp104 to i32             ; <i32> [#uses=1]
  %tmp106 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv107 = trunc i64 %tmp106 to i32             ; <i32> [#uses=1]
  %tmp108 = mul i32 1, %conv107                   ; <i32> [#uses=1]
  %tmp109 = add i32 %conv105, %tmp108             ; <i32> [#uses=1]
  %arrayidx110 = getelementptr double addrspace(1)* %tmp102, i32 %tmp109 ; <double addrspace(1)*> [#uses=1]
  %tmp111 = load double addrspace(1)* %arrayidx110 ; <double> [#uses=1]
  %tmp112 = fsub double %tmp101, %tmp111          ; <double> [#uses=1]
  %tmp113 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp114 = fmul double %tmp112, %tmp113          ; <double> [#uses=1]
  %tmp115 = fadd double %tmp91, %tmp114           ; <double> [#uses=1]
  store double %tmp115, double addrspace(1)* %arrayidx87
  %tmp116 = load double addrspace(1)** %uold.addr ; <double addrspace(1)*> [#uses=1]
  %tmp117 = load i64* %i                          ; <i64> [#uses=1]
  %conv118 = trunc i64 %tmp117 to i32             ; <i32> [#uses=1]
  %tmp119 = load i64* %Hnxt.addr                  ; <i64> [#uses=1]
  %conv120 = trunc i64 %tmp119 to i32             ; <i32> [#uses=1]
  %tmp121 = load i64* %rowcol.addr                ; <i64> [#uses=1]
  %conv122 = trunc i64 %tmp121 to i32             ; <i32> [#uses=1]
  %tmp123 = load i64* %Hnyt.addr                  ; <i64> [#uses=1]
  %conv124 = trunc i64 %tmp123 to i32             ; <i32> [#uses=1]
  %tmp125 = mul i32 %conv124, 2                   ; <i32> [#uses=1]
  %tmp126 = add i32 %conv122, %tmp125             ; <i32> [#uses=1]
  %tmp127 = mul i32 %conv120, %tmp126             ; <i32> [#uses=1]
  %tmp128 = add i32 %conv118, %tmp127             ; <i32> [#uses=1]
  %arrayidx129 = getelementptr double addrspace(1)* %tmp116, i32 %tmp128 ; <double addrspace(1)*> [#uses=1]
  %tmp130 = load double addrspace(1)** %u.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp131 = load i32* %idxIV                      ; <i32> [#uses=1]
  %arrayidx132 = getelementptr double addrspace(1)* %tmp130, i32 %tmp131 ; <double addrspace(1)*> [#uses=1]
  %tmp133 = load double addrspace(1)* %arrayidx132 ; <double> [#uses=1]
  %tmp134 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp135 = load i64* %i                          ; <i64> [#uses=1]
  %tmp136 = sub i64 %tmp135, 2                    ; <i64> [#uses=1]
  %conv137 = trunc i64 %tmp136 to i32             ; <i32> [#uses=1]
  %tmp138 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv139 = trunc i64 %tmp138 to i32             ; <i32> [#uses=1]
  %tmp140 = mul i32 2, %conv139                   ; <i32> [#uses=1]
  %tmp141 = add i32 %conv137, %tmp140             ; <i32> [#uses=1]
  %arrayidx142 = getelementptr double addrspace(1)* %tmp134, i32 %tmp141 ; <double addrspace(1)*> [#uses=1]
  %tmp143 = load double addrspace(1)* %arrayidx142 ; <double> [#uses=1]
  %tmp144 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp145 = load i64* %i                          ; <i64> [#uses=1]
  %tmp146 = sub i64 %tmp145, 1                    ; <i64> [#uses=1]
  %conv147 = trunc i64 %tmp146 to i32             ; <i32> [#uses=1]
  %tmp148 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv149 = trunc i64 %tmp148 to i32             ; <i32> [#uses=1]
  %tmp150 = mul i32 2, %conv149                   ; <i32> [#uses=1]
  %tmp151 = add i32 %conv147, %tmp150             ; <i32> [#uses=1]
  %arrayidx152 = getelementptr double addrspace(1)* %tmp144, i32 %tmp151 ; <double addrspace(1)*> [#uses=1]
  %tmp153 = load double addrspace(1)* %arrayidx152 ; <double> [#uses=1]
  %tmp154 = fsub double %tmp143, %tmp153          ; <double> [#uses=1]
  %tmp155 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp156 = fmul double %tmp154, %tmp155          ; <double> [#uses=1]
  %tmp157 = fadd double %tmp133, %tmp156          ; <double> [#uses=1]
  store double %tmp157, double addrspace(1)* %arrayidx129
  %tmp158 = load double addrspace(1)** %uold.addr ; <double addrspace(1)*> [#uses=1]
  %tmp159 = load i64* %i                          ; <i64> [#uses=1]
  %conv160 = trunc i64 %tmp159 to i32             ; <i32> [#uses=1]
  %tmp161 = load i64* %Hnxt.addr                  ; <i64> [#uses=1]
  %conv162 = trunc i64 %tmp161 to i32             ; <i32> [#uses=1]
  %tmp163 = load i64* %rowcol.addr                ; <i64> [#uses=1]
  %conv164 = trunc i64 %tmp163 to i32             ; <i32> [#uses=1]
  %tmp165 = load i64* %Hnyt.addr                  ; <i64> [#uses=1]
  %conv166 = trunc i64 %tmp165 to i32             ; <i32> [#uses=1]
  %tmp167 = mul i32 %conv166, 3                   ; <i32> [#uses=1]
  %tmp168 = add i32 %conv164, %tmp167             ; <i32> [#uses=1]
  %tmp169 = mul i32 %conv162, %tmp168             ; <i32> [#uses=1]
  %tmp170 = add i32 %conv160, %tmp169             ; <i32> [#uses=1]
  %arrayidx171 = getelementptr double addrspace(1)* %tmp158, i32 %tmp170 ; <double addrspace(1)*> [#uses=1]
  %tmp172 = load double addrspace(1)** %u.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp173 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx174 = getelementptr double addrspace(1)* %tmp172, i32 %tmp173 ; <double addrspace(1)*> [#uses=1]
  %tmp175 = load double addrspace(1)* %arrayidx174 ; <double> [#uses=1]
  %tmp176 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp177 = load i64* %i                          ; <i64> [#uses=1]
  %tmp178 = sub i64 %tmp177, 2                    ; <i64> [#uses=1]
  %conv179 = trunc i64 %tmp178 to i32             ; <i32> [#uses=1]
  %tmp180 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv181 = trunc i64 %tmp180 to i32             ; <i32> [#uses=1]
  %tmp182 = mul i32 3, %conv181                   ; <i32> [#uses=1]
  %tmp183 = add i32 %conv179, %tmp182             ; <i32> [#uses=1]
  %arrayidx184 = getelementptr double addrspace(1)* %tmp176, i32 %tmp183 ; <double addrspace(1)*> [#uses=1]
  %tmp185 = load double addrspace(1)* %arrayidx184 ; <double> [#uses=1]
  %tmp186 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp187 = load i64* %i                          ; <i64> [#uses=1]
  %tmp188 = sub i64 %tmp187, 1                    ; <i64> [#uses=1]
  %conv189 = trunc i64 %tmp188 to i32             ; <i32> [#uses=1]
  %tmp190 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv191 = trunc i64 %tmp190 to i32             ; <i32> [#uses=1]
  %tmp192 = mul i32 3, %conv191                   ; <i32> [#uses=1]
  %tmp193 = add i32 %conv189, %tmp192             ; <i32> [#uses=1]
  %arrayidx194 = getelementptr double addrspace(1)* %tmp186, i32 %tmp193 ; <double addrspace(1)*> [#uses=1]
  %tmp195 = load double addrspace(1)* %arrayidx194 ; <double> [#uses=1]
  %tmp196 = fsub double %tmp185, %tmp195          ; <double> [#uses=1]
  %tmp197 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp198 = fmul double %tmp196, %tmp197          ; <double> [#uses=1]
  %tmp199 = fadd double %tmp175, %tmp198          ; <double> [#uses=1]
  store double %tmp199, double addrspace(1)* %arrayidx171
  br label %return

if.then8:                                         ; preds = %if.end
  br label %return
}

define void @__OpenCL_Loop2KcuUpdate_kernel(i64 %rowcol, double %dtdx, double addrspace(1)* %uold, double addrspace(1)* %u, double addrspace(1)* %flux, i64 %Himin, i64 %Himax, i64 %Hnvar, i64 %Hnxt, i64 %Hnyt, i64 %Hnxyt) nounwind {
entry:
  %rowcol.addr = alloca i64, align 8              ; <i64*> [#uses=2]
  %dtdx.addr = alloca double, align 8             ; <double*> [#uses=2]
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %flux.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=3]
  %Himin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Himax.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=4]
  %i = alloca i64, align 8                        ; <i64*> [#uses=7]
  %ivar = alloca i64, align 8                     ; <i64*> [#uses=8]
  store i64 %rowcol, i64* %rowcol.addr
  store double %dtdx, double* %dtdx.addr
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store double addrspace(1)* %flux, double addrspace(1)** %flux.addr
  store i64 %Himin, i64* %Himin.addr
  store i64 %Himax, i64* %Himax.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %Himin.addr                   ; <i64> [#uses=1]
  %tmp2 = add i64 %tmp1, 2                        ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp2                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %for.exit, %if.then8, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp3 = load i64* %i                            ; <i64> [#uses=1]
  %tmp4 = load i64* %Himax.addr                   ; <i64> [#uses=1]
  %tmp5 = sub i64 %tmp4, 2                        ; <i64> [#uses=1]
  %cmp6 = icmp sge i64 %tmp3, %tmp5               ; <i1> [#uses=1]
  br i1 %cmp6, label %if.then8, label %if.end7

if.then:                                          ; preds = %entry
  br label %return

if.end7:                                          ; preds = %if.end
  store i64 4, i64* %ivar
  br label %for.cond

if.then8:                                         ; preds = %if.end
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end7
  %tmp9 = load i64* %ivar                         ; <i64> [#uses=1]
  %tmp10 = load i64* %Hnvar.addr                  ; <i64> [#uses=1]
  %cmp11 = icmp slt i64 %tmp9, %tmp10             ; <i1> [#uses=1]
  br i1 %cmp11, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %return

for.body:                                         ; preds = %for.cond
  %tmp12 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp13 = load i64* %i                           ; <i64> [#uses=1]
  %conv14 = trunc i64 %tmp13 to i32               ; <i32> [#uses=1]
  %tmp15 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %tmp17 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv18 = trunc i64 %tmp17 to i32               ; <i32> [#uses=1]
  %tmp19 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv20 = trunc i64 %tmp19 to i32               ; <i32> [#uses=1]
  %tmp21 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv22 = trunc i64 %tmp21 to i32               ; <i32> [#uses=1]
  %tmp23 = mul i32 %conv20, %conv22               ; <i32> [#uses=1]
  %tmp24 = add i32 %conv18, %tmp23                ; <i32> [#uses=1]
  %tmp25 = mul i32 %conv16, %tmp24                ; <i32> [#uses=1]
  %tmp26 = add i32 %conv14, %tmp25                ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp12, i32 %tmp26 ; <double addrspace(1)*> [#uses=1]
  %tmp27 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp28 = load i64* %i                           ; <i64> [#uses=1]
  %conv29 = trunc i64 %tmp28 to i32               ; <i32> [#uses=1]
  %tmp30 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv31 = trunc i64 %tmp30 to i32               ; <i32> [#uses=1]
  %tmp32 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv33 = trunc i64 %tmp32 to i32               ; <i32> [#uses=1]
  %tmp34 = mul i32 %conv31, %conv33               ; <i32> [#uses=1]
  %tmp35 = add i32 %conv29, %tmp34                ; <i32> [#uses=1]
  %arrayidx36 = getelementptr double addrspace(1)* %tmp27, i32 %tmp35 ; <double addrspace(1)*> [#uses=1]
  %tmp37 = load double addrspace(1)* %arrayidx36  ; <double> [#uses=1]
  %tmp38 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp39 = load i64* %i                           ; <i64> [#uses=1]
  %tmp40 = sub i64 %tmp39, 2                      ; <i64> [#uses=1]
  %conv41 = trunc i64 %tmp40 to i32               ; <i32> [#uses=1]
  %tmp42 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv43 = trunc i64 %tmp42 to i32               ; <i32> [#uses=1]
  %tmp44 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv45 = trunc i64 %tmp44 to i32               ; <i32> [#uses=1]
  %tmp46 = mul i32 %conv43, %conv45               ; <i32> [#uses=1]
  %tmp47 = add i32 %conv41, %tmp46                ; <i32> [#uses=1]
  %arrayidx48 = getelementptr double addrspace(1)* %tmp38, i32 %tmp47 ; <double addrspace(1)*> [#uses=1]
  %tmp49 = load double addrspace(1)* %arrayidx48  ; <double> [#uses=1]
  %tmp50 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp51 = load i64* %i                           ; <i64> [#uses=1]
  %tmp52 = sub i64 %tmp51, 1                      ; <i64> [#uses=1]
  %conv53 = trunc i64 %tmp52 to i32               ; <i32> [#uses=1]
  %tmp54 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv55 = trunc i64 %tmp54 to i32               ; <i32> [#uses=1]
  %tmp56 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv57 = trunc i64 %tmp56 to i32               ; <i32> [#uses=1]
  %tmp58 = mul i32 %conv55, %conv57               ; <i32> [#uses=1]
  %tmp59 = add i32 %conv53, %tmp58                ; <i32> [#uses=1]
  %arrayidx60 = getelementptr double addrspace(1)* %tmp50, i32 %tmp59 ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load double addrspace(1)* %arrayidx60  ; <double> [#uses=1]
  %tmp62 = fsub double %tmp49, %tmp61             ; <double> [#uses=1]
  %tmp63 = load double* %dtdx.addr                ; <double> [#uses=1]
  %tmp64 = fmul double %tmp62, %tmp63             ; <double> [#uses=1]
  %tmp65 = fadd double %tmp37, %tmp64             ; <double> [#uses=1]
  store double %tmp65, double addrspace(1)* %arrayidx
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %tmp66 = load i64* %ivar                        ; <i64> [#uses=1]
  %tmp67 = add i64 %tmp66, 1                      ; <i64> [#uses=1]
  store i64 %tmp67, i64* %ivar
  br label %for.cond
}

define void @__OpenCL_Loop3KcuUpdate_kernel(i64 %rowcol, double %dtdx, double addrspace(1)* %uold, double addrspace(1)* %u, double addrspace(1)* %flux, i64 %Hjmin, i64 %Hjmax, i64 %Hnxt, i64 %Hnyt, i64 %Hnxyt) nounwind {
entry:
  %rowcol.addr = alloca i64, align 8              ; <i64*> [#uses=5]
  %dtdx.addr = alloca double, align 8             ; <double*> [#uses=5]
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=5]
  %flux.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=9]
  %Hjmin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hjmax.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=5]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=13]
  %j = alloca i64, align 8                        ; <i64*> [#uses=19]
  store i64 %rowcol, i64* %rowcol.addr
  store double %dtdx, double* %dtdx.addr
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store double addrspace(1)* %flux, double addrspace(1)** %flux.addr
  store i64 %Hjmin, i64* %Hjmin.addr
  store i64 %Hjmax, i64* %Hjmax.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %j
  %tmp = load i64* %j                             ; <i64> [#uses=1]
  %tmp1 = load i64* %Hjmin.addr                   ; <i64> [#uses=1]
  %tmp2 = add i64 %tmp1, 2                        ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp2                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end7, %if.then8, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp3 = load i64* %j                            ; <i64> [#uses=1]
  %tmp4 = load i64* %Hjmax.addr                   ; <i64> [#uses=1]
  %tmp5 = sub i64 %tmp4, 2                        ; <i64> [#uses=1]
  %cmp6 = icmp sge i64 %tmp3, %tmp5               ; <i1> [#uses=1]
  br i1 %cmp6, label %if.then8, label %if.end7

if.then:                                          ; preds = %entry
  br label %return

if.end7:                                          ; preds = %if.end
  %tmp9 = load double addrspace(1)** %uold.addr   ; <double addrspace(1)*> [#uses=1]
  %tmp10 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv11 = trunc i64 %tmp10 to i32               ; <i32> [#uses=1]
  %tmp12 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv13 = trunc i64 %tmp12 to i32               ; <i32> [#uses=1]
  %tmp14 = load i64* %j                           ; <i64> [#uses=1]
  %conv15 = trunc i64 %tmp14 to i32               ; <i32> [#uses=1]
  %tmp16 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv17 = trunc i64 %tmp16 to i32               ; <i32> [#uses=1]
  %tmp18 = mul i32 %conv17, 0                     ; <i32> [#uses=1]
  %tmp19 = add i32 %conv15, %tmp18                ; <i32> [#uses=1]
  %tmp20 = mul i32 %conv13, %tmp19                ; <i32> [#uses=1]
  %tmp21 = add i32 %conv11, %tmp20                ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp9, i32 %tmp21 ; <double addrspace(1)*> [#uses=1]
  %tmp22 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp23 = load i64* %j                           ; <i64> [#uses=1]
  %conv24 = trunc i64 %tmp23 to i32               ; <i32> [#uses=1]
  %tmp25 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv26 = trunc i64 %tmp25 to i32               ; <i32> [#uses=1]
  %tmp27 = mul i32 0, %conv26                     ; <i32> [#uses=1]
  %tmp28 = add i32 %conv24, %tmp27                ; <i32> [#uses=1]
  %arrayidx29 = getelementptr double addrspace(1)* %tmp22, i32 %tmp28 ; <double addrspace(1)*> [#uses=1]
  %tmp30 = load double addrspace(1)* %arrayidx29  ; <double> [#uses=1]
  %tmp31 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp32 = load i64* %j                           ; <i64> [#uses=1]
  %tmp33 = sub i64 %tmp32, 2                      ; <i64> [#uses=1]
  %conv34 = trunc i64 %tmp33 to i32               ; <i32> [#uses=1]
  %tmp35 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv36 = trunc i64 %tmp35 to i32               ; <i32> [#uses=1]
  %tmp37 = mul i32 0, %conv36                     ; <i32> [#uses=1]
  %tmp38 = add i32 %conv34, %tmp37                ; <i32> [#uses=1]
  %arrayidx39 = getelementptr double addrspace(1)* %tmp31, i32 %tmp38 ; <double addrspace(1)*> [#uses=1]
  %tmp40 = load double addrspace(1)* %arrayidx39  ; <double> [#uses=1]
  %tmp41 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp42 = load i64* %j                           ; <i64> [#uses=1]
  %tmp43 = sub i64 %tmp42, 1                      ; <i64> [#uses=1]
  %conv44 = trunc i64 %tmp43 to i32               ; <i32> [#uses=1]
  %tmp45 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv46 = trunc i64 %tmp45 to i32               ; <i32> [#uses=1]
  %tmp47 = mul i32 0, %conv46                     ; <i32> [#uses=1]
  %tmp48 = add i32 %conv44, %tmp47                ; <i32> [#uses=1]
  %arrayidx49 = getelementptr double addrspace(1)* %tmp41, i32 %tmp48 ; <double addrspace(1)*> [#uses=1]
  %tmp50 = load double addrspace(1)* %arrayidx49  ; <double> [#uses=1]
  %tmp51 = fsub double %tmp40, %tmp50             ; <double> [#uses=1]
  %tmp52 = load double* %dtdx.addr                ; <double> [#uses=1]
  %tmp53 = fmul double %tmp51, %tmp52             ; <double> [#uses=1]
  %tmp54 = fadd double %tmp30, %tmp53             ; <double> [#uses=1]
  store double %tmp54, double addrspace(1)* %arrayidx
  %tmp55 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp56 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv57 = trunc i64 %tmp56 to i32               ; <i32> [#uses=1]
  %tmp58 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv59 = trunc i64 %tmp58 to i32               ; <i32> [#uses=1]
  %tmp60 = load i64* %j                           ; <i64> [#uses=1]
  %conv61 = trunc i64 %tmp60 to i32               ; <i32> [#uses=1]
  %tmp62 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv63 = trunc i64 %tmp62 to i32               ; <i32> [#uses=1]
  %tmp64 = mul i32 %conv63, 3                     ; <i32> [#uses=1]
  %tmp65 = add i32 %conv61, %tmp64                ; <i32> [#uses=1]
  %tmp66 = mul i32 %conv59, %tmp65                ; <i32> [#uses=1]
  %tmp67 = add i32 %conv57, %tmp66                ; <i32> [#uses=1]
  %arrayidx68 = getelementptr double addrspace(1)* %tmp55, i32 %tmp67 ; <double addrspace(1)*> [#uses=1]
  %tmp69 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp70 = load i64* %j                           ; <i64> [#uses=1]
  %conv71 = trunc i64 %tmp70 to i32               ; <i32> [#uses=1]
  %tmp72 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv73 = trunc i64 %tmp72 to i32               ; <i32> [#uses=1]
  %tmp74 = mul i32 3, %conv73                     ; <i32> [#uses=1]
  %tmp75 = add i32 %conv71, %tmp74                ; <i32> [#uses=1]
  %arrayidx76 = getelementptr double addrspace(1)* %tmp69, i32 %tmp75 ; <double addrspace(1)*> [#uses=1]
  %tmp77 = load double addrspace(1)* %arrayidx76  ; <double> [#uses=1]
  %tmp78 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp79 = load i64* %j                           ; <i64> [#uses=1]
  %tmp80 = sub i64 %tmp79, 2                      ; <i64> [#uses=1]
  %conv81 = trunc i64 %tmp80 to i32               ; <i32> [#uses=1]
  %tmp82 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv83 = trunc i64 %tmp82 to i32               ; <i32> [#uses=1]
  %tmp84 = mul i32 3, %conv83                     ; <i32> [#uses=1]
  %tmp85 = add i32 %conv81, %tmp84                ; <i32> [#uses=1]
  %arrayidx86 = getelementptr double addrspace(1)* %tmp78, i32 %tmp85 ; <double addrspace(1)*> [#uses=1]
  %tmp87 = load double addrspace(1)* %arrayidx86  ; <double> [#uses=1]
  %tmp88 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp89 = load i64* %j                           ; <i64> [#uses=1]
  %tmp90 = sub i64 %tmp89, 1                      ; <i64> [#uses=1]
  %conv91 = trunc i64 %tmp90 to i32               ; <i32> [#uses=1]
  %tmp92 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv93 = trunc i64 %tmp92 to i32               ; <i32> [#uses=1]
  %tmp94 = mul i32 3, %conv93                     ; <i32> [#uses=1]
  %tmp95 = add i32 %conv91, %tmp94                ; <i32> [#uses=1]
  %arrayidx96 = getelementptr double addrspace(1)* %tmp88, i32 %tmp95 ; <double addrspace(1)*> [#uses=1]
  %tmp97 = load double addrspace(1)* %arrayidx96  ; <double> [#uses=1]
  %tmp98 = fsub double %tmp87, %tmp97             ; <double> [#uses=1]
  %tmp99 = load double* %dtdx.addr                ; <double> [#uses=1]
  %tmp100 = fmul double %tmp98, %tmp99            ; <double> [#uses=1]
  %tmp101 = fadd double %tmp77, %tmp100           ; <double> [#uses=1]
  store double %tmp101, double addrspace(1)* %arrayidx68
  %tmp102 = load double addrspace(1)** %uold.addr ; <double addrspace(1)*> [#uses=1]
  %tmp103 = load i64* %rowcol.addr                ; <i64> [#uses=1]
  %conv104 = trunc i64 %tmp103 to i32             ; <i32> [#uses=1]
  %tmp105 = load i64* %Hnxt.addr                  ; <i64> [#uses=1]
  %conv106 = trunc i64 %tmp105 to i32             ; <i32> [#uses=1]
  %tmp107 = load i64* %j                          ; <i64> [#uses=1]
  %conv108 = trunc i64 %tmp107 to i32             ; <i32> [#uses=1]
  %tmp109 = load i64* %Hnyt.addr                  ; <i64> [#uses=1]
  %conv110 = trunc i64 %tmp109 to i32             ; <i32> [#uses=1]
  %tmp111 = mul i32 %conv110, 2                   ; <i32> [#uses=1]
  %tmp112 = add i32 %conv108, %tmp111             ; <i32> [#uses=1]
  %tmp113 = mul i32 %conv106, %tmp112             ; <i32> [#uses=1]
  %tmp114 = add i32 %conv104, %tmp113             ; <i32> [#uses=1]
  %arrayidx115 = getelementptr double addrspace(1)* %tmp102, i32 %tmp114 ; <double addrspace(1)*> [#uses=1]
  %tmp116 = load double addrspace(1)** %u.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp117 = load i64* %j                          ; <i64> [#uses=1]
  %conv118 = trunc i64 %tmp117 to i32             ; <i32> [#uses=1]
  %tmp119 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv120 = trunc i64 %tmp119 to i32             ; <i32> [#uses=1]
  %tmp121 = mul i32 1, %conv120                   ; <i32> [#uses=1]
  %tmp122 = add i32 %conv118, %tmp121             ; <i32> [#uses=1]
  %arrayidx123 = getelementptr double addrspace(1)* %tmp116, i32 %tmp122 ; <double addrspace(1)*> [#uses=1]
  %tmp124 = load double addrspace(1)* %arrayidx123 ; <double> [#uses=1]
  %tmp125 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp126 = load i64* %j                          ; <i64> [#uses=1]
  %tmp127 = sub i64 %tmp126, 2                    ; <i64> [#uses=1]
  %conv128 = trunc i64 %tmp127 to i32             ; <i32> [#uses=1]
  %tmp129 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv130 = trunc i64 %tmp129 to i32             ; <i32> [#uses=1]
  %tmp131 = mul i32 1, %conv130                   ; <i32> [#uses=1]
  %tmp132 = add i32 %conv128, %tmp131             ; <i32> [#uses=1]
  %arrayidx133 = getelementptr double addrspace(1)* %tmp125, i32 %tmp132 ; <double addrspace(1)*> [#uses=1]
  %tmp134 = load double addrspace(1)* %arrayidx133 ; <double> [#uses=1]
  %tmp135 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp136 = load i64* %j                          ; <i64> [#uses=1]
  %tmp137 = sub i64 %tmp136, 1                    ; <i64> [#uses=1]
  %conv138 = trunc i64 %tmp137 to i32             ; <i32> [#uses=1]
  %tmp139 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv140 = trunc i64 %tmp139 to i32             ; <i32> [#uses=1]
  %tmp141 = mul i32 1, %conv140                   ; <i32> [#uses=1]
  %tmp142 = add i32 %conv138, %tmp141             ; <i32> [#uses=1]
  %arrayidx143 = getelementptr double addrspace(1)* %tmp135, i32 %tmp142 ; <double addrspace(1)*> [#uses=1]
  %tmp144 = load double addrspace(1)* %arrayidx143 ; <double> [#uses=1]
  %tmp145 = fsub double %tmp134, %tmp144          ; <double> [#uses=1]
  %tmp146 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp147 = fmul double %tmp145, %tmp146          ; <double> [#uses=1]
  %tmp148 = fadd double %tmp124, %tmp147          ; <double> [#uses=1]
  store double %tmp148, double addrspace(1)* %arrayidx115
  %tmp149 = load double addrspace(1)** %uold.addr ; <double addrspace(1)*> [#uses=1]
  %tmp150 = load i64* %rowcol.addr                ; <i64> [#uses=1]
  %conv151 = trunc i64 %tmp150 to i32             ; <i32> [#uses=1]
  %tmp152 = load i64* %Hnxt.addr                  ; <i64> [#uses=1]
  %conv153 = trunc i64 %tmp152 to i32             ; <i32> [#uses=1]
  %tmp154 = load i64* %j                          ; <i64> [#uses=1]
  %conv155 = trunc i64 %tmp154 to i32             ; <i32> [#uses=1]
  %tmp156 = load i64* %Hnyt.addr                  ; <i64> [#uses=1]
  %conv157 = trunc i64 %tmp156 to i32             ; <i32> [#uses=1]
  %tmp158 = mul i32 %conv157, 1                   ; <i32> [#uses=1]
  %tmp159 = add i32 %conv155, %tmp158             ; <i32> [#uses=1]
  %tmp160 = mul i32 %conv153, %tmp159             ; <i32> [#uses=1]
  %tmp161 = add i32 %conv151, %tmp160             ; <i32> [#uses=1]
  %arrayidx162 = getelementptr double addrspace(1)* %tmp149, i32 %tmp161 ; <double addrspace(1)*> [#uses=1]
  %tmp163 = load double addrspace(1)** %u.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp164 = load i64* %j                          ; <i64> [#uses=1]
  %conv165 = trunc i64 %tmp164 to i32             ; <i32> [#uses=1]
  %tmp166 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv167 = trunc i64 %tmp166 to i32             ; <i32> [#uses=1]
  %tmp168 = mul i32 2, %conv167                   ; <i32> [#uses=1]
  %tmp169 = add i32 %conv165, %tmp168             ; <i32> [#uses=1]
  %arrayidx170 = getelementptr double addrspace(1)* %tmp163, i32 %tmp169 ; <double addrspace(1)*> [#uses=1]
  %tmp171 = load double addrspace(1)* %arrayidx170 ; <double> [#uses=1]
  %tmp172 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp173 = load i64* %j                          ; <i64> [#uses=1]
  %tmp174 = sub i64 %tmp173, 2                    ; <i64> [#uses=1]
  %conv175 = trunc i64 %tmp174 to i32             ; <i32> [#uses=1]
  %tmp176 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv177 = trunc i64 %tmp176 to i32             ; <i32> [#uses=1]
  %tmp178 = mul i32 2, %conv177                   ; <i32> [#uses=1]
  %tmp179 = add i32 %conv175, %tmp178             ; <i32> [#uses=1]
  %arrayidx180 = getelementptr double addrspace(1)* %tmp172, i32 %tmp179 ; <double addrspace(1)*> [#uses=1]
  %tmp181 = load double addrspace(1)* %arrayidx180 ; <double> [#uses=1]
  %tmp182 = load double addrspace(1)** %flux.addr ; <double addrspace(1)*> [#uses=1]
  %tmp183 = load i64* %j                          ; <i64> [#uses=1]
  %tmp184 = sub i64 %tmp183, 1                    ; <i64> [#uses=1]
  %conv185 = trunc i64 %tmp184 to i32             ; <i32> [#uses=1]
  %tmp186 = load i64* %Hnxyt.addr                 ; <i64> [#uses=1]
  %conv187 = trunc i64 %tmp186 to i32             ; <i32> [#uses=1]
  %tmp188 = mul i32 2, %conv187                   ; <i32> [#uses=1]
  %tmp189 = add i32 %conv185, %tmp188             ; <i32> [#uses=1]
  %arrayidx190 = getelementptr double addrspace(1)* %tmp182, i32 %tmp189 ; <double addrspace(1)*> [#uses=1]
  %tmp191 = load double addrspace(1)* %arrayidx190 ; <double> [#uses=1]
  %tmp192 = fsub double %tmp181, %tmp191          ; <double> [#uses=1]
  %tmp193 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp194 = fmul double %tmp192, %tmp193          ; <double> [#uses=1]
  %tmp195 = fadd double %tmp171, %tmp194          ; <double> [#uses=1]
  store double %tmp195, double addrspace(1)* %arrayidx162
  br label %return

if.then8:                                         ; preds = %if.end
  br label %return
}

define void @__OpenCL_Loop4KcuUpdate_kernel(i64 %rowcol, double %dtdx, double addrspace(1)* %uold, double addrspace(1)* %u, double addrspace(1)* %flux, i64 %Hjmin, i64 %Hjmax, i64 %Hnvar, i64 %Hnxt, i64 %Hnyt, i64 %Hnxyt) nounwind {
entry:
  %rowcol.addr = alloca i64, align 8              ; <i64*> [#uses=2]
  %dtdx.addr = alloca double, align 8             ; <double*> [#uses=2]
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %flux.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=3]
  %Hjmin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hjmax.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=4]
  %j = alloca i64, align 8                        ; <i64*> [#uses=7]
  %ivar = alloca i64, align 8                     ; <i64*> [#uses=8]
  store i64 %rowcol, i64* %rowcol.addr
  store double %dtdx, double* %dtdx.addr
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store double addrspace(1)* %flux, double addrspace(1)** %flux.addr
  store i64 %Hjmin, i64* %Hjmin.addr
  store i64 %Hjmax, i64* %Hjmax.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %j
  %tmp = load i64* %j                             ; <i64> [#uses=1]
  %tmp1 = load i64* %Hjmin.addr                   ; <i64> [#uses=1]
  %tmp2 = add i64 %tmp1, 2                        ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp2                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %for.exit, %if.then8, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp3 = load i64* %j                            ; <i64> [#uses=1]
  %tmp4 = load i64* %Hjmax.addr                   ; <i64> [#uses=1]
  %tmp5 = sub i64 %tmp4, 2                        ; <i64> [#uses=1]
  %cmp6 = icmp sge i64 %tmp3, %tmp5               ; <i1> [#uses=1]
  br i1 %cmp6, label %if.then8, label %if.end7

if.then:                                          ; preds = %entry
  br label %return

if.end7:                                          ; preds = %if.end
  store i64 4, i64* %ivar
  br label %for.cond

if.then8:                                         ; preds = %if.end
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end7
  %tmp9 = load i64* %ivar                         ; <i64> [#uses=1]
  %tmp10 = load i64* %Hnvar.addr                  ; <i64> [#uses=1]
  %cmp11 = icmp slt i64 %tmp9, %tmp10             ; <i1> [#uses=1]
  br i1 %cmp11, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %return

for.body:                                         ; preds = %for.cond
  %tmp12 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp13 = load i64* %rowcol.addr                 ; <i64> [#uses=1]
  %conv14 = trunc i64 %tmp13 to i32               ; <i32> [#uses=1]
  %tmp15 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %tmp17 = load i64* %j                           ; <i64> [#uses=1]
  %conv18 = trunc i64 %tmp17 to i32               ; <i32> [#uses=1]
  %tmp19 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv20 = trunc i64 %tmp19 to i32               ; <i32> [#uses=1]
  %tmp21 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv22 = trunc i64 %tmp21 to i32               ; <i32> [#uses=1]
  %tmp23 = mul i32 %conv20, %conv22               ; <i32> [#uses=1]
  %tmp24 = add i32 %conv18, %tmp23                ; <i32> [#uses=1]
  %tmp25 = mul i32 %conv16, %tmp24                ; <i32> [#uses=1]
  %tmp26 = add i32 %conv14, %tmp25                ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp12, i32 %tmp26 ; <double addrspace(1)*> [#uses=1]
  %tmp27 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp28 = load i64* %j                           ; <i64> [#uses=1]
  %conv29 = trunc i64 %tmp28 to i32               ; <i32> [#uses=1]
  %tmp30 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv31 = trunc i64 %tmp30 to i32               ; <i32> [#uses=1]
  %tmp32 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv33 = trunc i64 %tmp32 to i32               ; <i32> [#uses=1]
  %tmp34 = mul i32 %conv31, %conv33               ; <i32> [#uses=1]
  %tmp35 = add i32 %conv29, %tmp34                ; <i32> [#uses=1]
  %arrayidx36 = getelementptr double addrspace(1)* %tmp27, i32 %tmp35 ; <double addrspace(1)*> [#uses=1]
  %tmp37 = load double addrspace(1)* %arrayidx36  ; <double> [#uses=1]
  %tmp38 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp39 = load i64* %j                           ; <i64> [#uses=1]
  %tmp40 = sub i64 %tmp39, 2                      ; <i64> [#uses=1]
  %conv41 = trunc i64 %tmp40 to i32               ; <i32> [#uses=1]
  %tmp42 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv43 = trunc i64 %tmp42 to i32               ; <i32> [#uses=1]
  %tmp44 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv45 = trunc i64 %tmp44 to i32               ; <i32> [#uses=1]
  %tmp46 = mul i32 %conv43, %conv45               ; <i32> [#uses=1]
  %tmp47 = add i32 %conv41, %tmp46                ; <i32> [#uses=1]
  %arrayidx48 = getelementptr double addrspace(1)* %tmp38, i32 %tmp47 ; <double addrspace(1)*> [#uses=1]
  %tmp49 = load double addrspace(1)* %arrayidx48  ; <double> [#uses=1]
  %tmp50 = load double addrspace(1)** %flux.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp51 = load i64* %j                           ; <i64> [#uses=1]
  %tmp52 = sub i64 %tmp51, 1                      ; <i64> [#uses=1]
  %conv53 = trunc i64 %tmp52 to i32               ; <i32> [#uses=1]
  %tmp54 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv55 = trunc i64 %tmp54 to i32               ; <i32> [#uses=1]
  %tmp56 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv57 = trunc i64 %tmp56 to i32               ; <i32> [#uses=1]
  %tmp58 = mul i32 %conv55, %conv57               ; <i32> [#uses=1]
  %tmp59 = add i32 %conv53, %tmp58                ; <i32> [#uses=1]
  %arrayidx60 = getelementptr double addrspace(1)* %tmp50, i32 %tmp59 ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load double addrspace(1)* %arrayidx60  ; <double> [#uses=1]
  %tmp62 = fsub double %tmp49, %tmp61             ; <double> [#uses=1]
  %tmp63 = load double* %dtdx.addr                ; <double> [#uses=1]
  %tmp64 = fmul double %tmp62, %tmp63             ; <double> [#uses=1]
  %tmp65 = fadd double %tmp37, %tmp64             ; <double> [#uses=1]
  store double %tmp65, double addrspace(1)* %arrayidx
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %tmp66 = load i64* %ivar                        ; <i64> [#uses=1]
  %tmp67 = add i64 %tmp66, 1                      ; <i64> [#uses=1]
  store i64 %tmp67, i64* %ivar
  br label %for.cond
}

define void @__OpenCL_Loop1KcuConstoprim_kernel(i64 %n, double addrspace(1)* %u, double addrspace(1)* %q, double addrspace(1)* %e, i64 %Hnxyt, double %Hsmallr) nounwind {
entry:
  %n.addr = alloca i64, align 8                   ; <i64*> [#uses=2]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=5]
  %q.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=11]
  %e.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=5]
  %Hsmallr.addr = alloca double, align 8          ; <double*> [#uses=2]
  %tmp = alloca double, align 8                   ; <double*> [#uses=3]
  %tmp1 = alloca double, align 8                  ; <double*> [#uses=3]
  %eken = alloca double, align 8                  ; <double*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=7]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=6]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=4]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=4]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=4]
  store i64 %n, i64* %n.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store double addrspace(1)* %q, double addrspace(1)** %q.addr
  store double addrspace(1)* %e, double addrspace(1)** %e.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store double %Hsmallr, double* %Hsmallr.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %tmp3 = load i64* %n.addr                       ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp2, %tmp3                ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp4 = load i64* %i                            ; <i64> [#uses=1]
  %conv5 = trunc i64 %tmp4 to i32                 ; <i32> [#uses=1]
  %tmp6 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv7 = trunc i64 %tmp6 to i32                 ; <i32> [#uses=1]
  %tmp8 = mul i32 0, %conv7                       ; <i32> [#uses=1]
  %tmp9 = add i32 %conv5, %tmp8                   ; <i32> [#uses=1]
  store i32 %tmp9, i32* %idxID
  %tmp10 = load i64* %i                           ; <i64> [#uses=1]
  %conv11 = trunc i64 %tmp10 to i32               ; <i32> [#uses=1]
  %tmp12 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv13 = trunc i64 %tmp12 to i32               ; <i32> [#uses=1]
  %tmp14 = mul i32 3, %conv13                     ; <i32> [#uses=1]
  %tmp15 = add i32 %conv11, %tmp14                ; <i32> [#uses=1]
  store i32 %tmp15, i32* %idxIP
  %tmp16 = load i64* %i                           ; <i64> [#uses=1]
  %conv17 = trunc i64 %tmp16 to i32               ; <i32> [#uses=1]
  %tmp18 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv19 = trunc i64 %tmp18 to i32               ; <i32> [#uses=1]
  %tmp20 = mul i32 1, %conv19                     ; <i32> [#uses=1]
  %tmp21 = add i32 %conv17, %tmp20                ; <i32> [#uses=1]
  store i32 %tmp21, i32* %idxIU
  %tmp22 = load i64* %i                           ; <i64> [#uses=1]
  %conv23 = trunc i64 %tmp22 to i32               ; <i32> [#uses=1]
  %tmp24 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv25 = trunc i64 %tmp24 to i32               ; <i32> [#uses=1]
  %tmp26 = mul i32 2, %conv25                     ; <i32> [#uses=1]
  %tmp27 = add i32 %conv23, %tmp26                ; <i32> [#uses=1]
  store i32 %tmp27, i32* %idxIV
  %tmp28 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp29 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp28, i32 %tmp29 ; <double addrspace(1)*> [#uses=1]
  %tmp30 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp31 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx32 = getelementptr double addrspace(1)* %tmp30, i32 %tmp31 ; <double addrspace(1)*> [#uses=1]
  %tmp33 = load double addrspace(1)* %arrayidx32  ; <double> [#uses=1]
  %tmp34 = load double* %Hsmallr.addr             ; <double> [#uses=1]
  %call35 = call double @__max_f64(double %tmp33, double %tmp34) nounwind ; <double> [#uses=1]
  store double %call35, double addrspace(1)* %arrayidx
  %tmp36 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp37 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx38 = getelementptr double addrspace(1)* %tmp36, i32 %tmp37 ; <double addrspace(1)*> [#uses=1]
  %tmp39 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp40 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx41 = getelementptr double addrspace(1)* %tmp39, i32 %tmp40 ; <double addrspace(1)*> [#uses=1]
  %tmp42 = load double addrspace(1)* %arrayidx41  ; <double> [#uses=1]
  %tmp43 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp44 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx45 = getelementptr double addrspace(1)* %tmp43, i32 %tmp44 ; <double addrspace(1)*> [#uses=1]
  %tmp46 = load double addrspace(1)* %arrayidx45  ; <double> [#uses=1]
  %tmp47 = fdiv double %tmp42, %tmp46             ; <double> [#uses=1]
  store double %tmp47, double addrspace(1)* %arrayidx38
  %tmp48 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp49 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx50 = getelementptr double addrspace(1)* %tmp48, i32 %tmp49 ; <double addrspace(1)*> [#uses=1]
  %tmp51 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp52 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx53 = getelementptr double addrspace(1)* %tmp51, i32 %tmp52 ; <double addrspace(1)*> [#uses=1]
  %tmp54 = load double addrspace(1)* %arrayidx53  ; <double> [#uses=1]
  %tmp55 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp56 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx57 = getelementptr double addrspace(1)* %tmp55, i32 %tmp56 ; <double addrspace(1)*> [#uses=1]
  %tmp58 = load double addrspace(1)* %arrayidx57  ; <double> [#uses=1]
  %tmp59 = fdiv double %tmp54, %tmp58             ; <double> [#uses=1]
  store double %tmp59, double addrspace(1)* %arrayidx50
  %tmp60 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx62 = getelementptr double addrspace(1)* %tmp60, i32 %tmp61 ; <double addrspace(1)*> [#uses=1]
  %tmp63 = load double addrspace(1)* %arrayidx62  ; <double> [#uses=1]
  store double %tmp63, double* %tmp
  %tmp64 = load double* %tmp                      ; <double> [#uses=1]
  %tmp65 = load double* %tmp                      ; <double> [#uses=1]
  %tmp66 = fmul double %tmp64, %tmp65             ; <double> [#uses=1]
  %tmp67 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp68 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx69 = getelementptr double addrspace(1)* %tmp67, i32 %tmp68 ; <double addrspace(1)*> [#uses=1]
  %tmp70 = load double addrspace(1)* %arrayidx69  ; <double> [#uses=1]
  store double %tmp70, double* %tmp1
  %tmp71 = load double* %tmp1                     ; <double> [#uses=1]
  %tmp72 = load double* %tmp1                     ; <double> [#uses=1]
  %tmp73 = fmul double %tmp71, %tmp72             ; <double> [#uses=1]
  %tmp74 = fadd double %tmp66, %tmp73             ; <double> [#uses=1]
  %tmp75 = fmul double 5.000000e-01, %tmp74       ; <double> [#uses=1]
  store double %tmp75, double* %eken
  %tmp76 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp77 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx78 = getelementptr double addrspace(1)* %tmp76, i32 %tmp77 ; <double addrspace(1)*> [#uses=1]
  %tmp79 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp80 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx81 = getelementptr double addrspace(1)* %tmp79, i32 %tmp80 ; <double addrspace(1)*> [#uses=1]
  %tmp82 = load double addrspace(1)* %arrayidx81  ; <double> [#uses=1]
  %tmp83 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp84 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx85 = getelementptr double addrspace(1)* %tmp83, i32 %tmp84 ; <double addrspace(1)*> [#uses=1]
  %tmp86 = load double addrspace(1)* %arrayidx85  ; <double> [#uses=1]
  %tmp87 = fdiv double %tmp82, %tmp86             ; <double> [#uses=1]
  %tmp88 = load double* %eken                     ; <double> [#uses=1]
  %tmp89 = fsub double %tmp87, %tmp88             ; <double> [#uses=1]
  store double %tmp89, double addrspace(1)* %arrayidx78
  %tmp90 = load double addrspace(1)** %e.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp91 = load i64* %i                           ; <i64> [#uses=1]
  %conv92 = trunc i64 %tmp91 to i32               ; <i32> [#uses=1]
  %arrayidx93 = getelementptr double addrspace(1)* %tmp90, i32 %conv92 ; <double addrspace(1)*> [#uses=1]
  %tmp94 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp95 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx96 = getelementptr double addrspace(1)* %tmp94, i32 %tmp95 ; <double addrspace(1)*> [#uses=1]
  %tmp97 = load double addrspace(1)* %arrayidx96  ; <double> [#uses=1]
  store double %tmp97, double addrspace(1)* %arrayidx93
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

define void @__OpenCL_Loop2KcuConstoprim_kernel(i64 %n, double addrspace(1)* %u, double addrspace(1)* %q, i64 %Hnxyt, i64 %Hnvar) nounwind {
entry:
  %n.addr = alloca i64, align 8                   ; <i64*> [#uses=2]
  %u.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %q.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=3]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %IN = alloca i64, align 8                       ; <i64*> [#uses=5]
  %i = alloca i64, align 8                        ; <i64*> [#uses=3]
  %idxIN = alloca i32, align 4                    ; <i32*> [#uses=4]
  store i64 %n, i64* %n.addr
  store double addrspace(1)* %u, double addrspace(1)** %u.addr
  store double addrspace(1)* %q, double addrspace(1)** %q.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %n.addr                       ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %for.exit, %if.then
  ret void

if.end:                                           ; preds = %entry
  store i64 4, i64* %IN
  br label %for.cond

if.then:                                          ; preds = %entry
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end
  %tmp2 = load i64* %IN                           ; <i64> [#uses=1]
  %tmp3 = load i64* %Hnvar.addr                   ; <i64> [#uses=1]
  %cmp4 = icmp slt i64 %tmp2, %tmp3               ; <i1> [#uses=1]
  br i1 %cmp4, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %return

for.body:                                         ; preds = %for.cond
  %tmp5 = load i64* %i                            ; <i64> [#uses=1]
  %conv6 = trunc i64 %tmp5 to i32                 ; <i32> [#uses=1]
  %tmp7 = load i64* %IN                           ; <i64> [#uses=1]
  %conv8 = trunc i64 %tmp7 to i32                 ; <i32> [#uses=1]
  %tmp9 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv10 = trunc i64 %tmp9 to i32                ; <i32> [#uses=1]
  %tmp11 = mul i32 %conv8, %conv10                ; <i32> [#uses=1]
  %tmp12 = add i32 %conv6, %tmp11                 ; <i32> [#uses=1]
  store i32 %tmp12, i32* %idxIN
  %tmp13 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp14 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp13, i32 %tmp14 ; <double addrspace(1)*> [#uses=1]
  %tmp15 = load double addrspace(1)** %u.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp16 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx17 = getelementptr double addrspace(1)* %tmp15, i32 %tmp16 ; <double addrspace(1)*> [#uses=1]
  %tmp18 = load double addrspace(1)* %arrayidx17  ; <double> [#uses=1]
  %tmp19 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp20 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx21 = getelementptr double addrspace(1)* %tmp19, i32 %tmp20 ; <double addrspace(1)*> [#uses=1]
  %tmp22 = load double addrspace(1)* %arrayidx21  ; <double> [#uses=1]
  %tmp23 = fdiv double %tmp18, %tmp22             ; <double> [#uses=1]
  store double %tmp23, double addrspace(1)* %arrayidx
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %tmp24 = load i64* %IN                          ; <i64> [#uses=1]
  %tmp25 = add i64 %tmp24, 1                      ; <i64> [#uses=1]
  store i64 %tmp25, i64* %IN
  br label %for.cond
}

define void @__OpenCL_LoopEOS_kernel(double addrspace(1)* %q, double addrspace(1)* %eint, double addrspace(1)* %c, i64 %offsetIP, i64 %offsetID, i64 %imin, i64 %imax, double %Hsmallc, double %Hgamma) nounwind {
entry:
  %q.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=3]
  %eint.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %c.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %offsetIP.addr = alloca i64, align 8            ; <i64*> [#uses=2]
  %offsetID.addr = alloca i64, align 8            ; <i64*> [#uses=2]
  %imin.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %imax.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Hsmallc.addr = alloca double, align 8          ; <double*> [#uses=3]
  %Hgamma.addr = alloca double, align 8           ; <double*> [#uses=4]
  %smallp = alloca double, align 8                ; <double*> [#uses=2]
  %p = alloca double addrspace(1)*, align 4       ; <double addrspace(1)**> [#uses=5]
  %rho = alloca double addrspace(1)*, align 4     ; <double addrspace(1)**> [#uses=4]
  %k = alloca i64, align 8                        ; <i64*> [#uses=12]
  store double addrspace(1)* %q, double addrspace(1)** %q.addr
  store double addrspace(1)* %eint, double addrspace(1)** %eint.addr
  store double addrspace(1)* %c, double addrspace(1)** %c.addr
  store i64 %offsetIP, i64* %offsetIP.addr
  store i64 %offsetID, i64* %offsetID.addr
  store i64 %imin, i64* %imin.addr
  store i64 %imax, i64* %imax.addr
  store double %Hsmallc, double* %Hsmallc.addr
  store double %Hgamma, double* %Hgamma.addr
  %tmp = load double addrspace(1)** %q.addr       ; <double addrspace(1)*> [#uses=1]
  %tmp1 = load i64* %offsetIP.addr                ; <i64> [#uses=1]
  %conv = trunc i64 %tmp1 to i32                  ; <i32> [#uses=1]
  %tmp2 = getelementptr double addrspace(1)* %tmp, i32 %conv ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp2, double addrspace(1)** %p
  %tmp3 = load double addrspace(1)** %q.addr      ; <double addrspace(1)*> [#uses=1]
  %tmp4 = load i64* %offsetID.addr                ; <i64> [#uses=1]
  %conv5 = trunc i64 %tmp4 to i32                 ; <i32> [#uses=1]
  %tmp6 = getelementptr double addrspace(1)* %tmp3, i32 %conv5 ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp6, double addrspace(1)** %rho
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv7 = zext i32 %call to i64                  ; <i64> [#uses=1]
  store i64 %conv7, i64* %k
  %tmp8 = load i64* %k                            ; <i64> [#uses=1]
  %tmp9 = load i64* %imin.addr                    ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp8, %tmp9                ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end13, %if.then14, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp10 = load i64* %k                           ; <i64> [#uses=1]
  %tmp11 = load i64* %imax.addr                   ; <i64> [#uses=1]
  %cmp12 = icmp sge i64 %tmp10, %tmp11            ; <i1> [#uses=1]
  br i1 %cmp12, label %if.then14, label %if.end13

if.then:                                          ; preds = %entry
  br label %return

if.end13:                                         ; preds = %if.end
  %tmp15 = load double* %Hsmallc.addr             ; <double> [#uses=1]
  %tmp16 = load double* %Hsmallc.addr             ; <double> [#uses=1]
  %tmp17 = fmul double %tmp15, %tmp16             ; <double> [#uses=1]
  %tmp18 = load double* %Hgamma.addr              ; <double> [#uses=1]
  %tmp19 = fdiv double %tmp17, %tmp18             ; <double> [#uses=1]
  store double %tmp19, double* %smallp
  %tmp20 = load double addrspace(1)** %p          ; <double addrspace(1)*> [#uses=1]
  %tmp21 = load i64* %k                           ; <i64> [#uses=1]
  %conv22 = trunc i64 %tmp21 to i32               ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp20, i32 %conv22 ; <double addrspace(1)*> [#uses=1]
  %tmp23 = load double* %Hgamma.addr              ; <double> [#uses=1]
  %tmp24 = fsub double %tmp23, 1.000000e+00       ; <double> [#uses=1]
  %tmp25 = load double addrspace(1)** %rho        ; <double addrspace(1)*> [#uses=1]
  %tmp26 = load i64* %k                           ; <i64> [#uses=1]
  %conv27 = trunc i64 %tmp26 to i32               ; <i32> [#uses=1]
  %arrayidx28 = getelementptr double addrspace(1)* %tmp25, i32 %conv27 ; <double addrspace(1)*> [#uses=1]
  %tmp29 = load double addrspace(1)* %arrayidx28  ; <double> [#uses=1]
  %tmp30 = fmul double %tmp24, %tmp29             ; <double> [#uses=1]
  %tmp31 = load double addrspace(1)** %eint.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp32 = load i64* %k                           ; <i64> [#uses=1]
  %conv33 = trunc i64 %tmp32 to i32               ; <i32> [#uses=1]
  %arrayidx34 = getelementptr double addrspace(1)* %tmp31, i32 %conv33 ; <double addrspace(1)*> [#uses=1]
  %tmp35 = load double addrspace(1)* %arrayidx34  ; <double> [#uses=1]
  %tmp36 = fmul double %tmp30, %tmp35             ; <double> [#uses=1]
  store double %tmp36, double addrspace(1)* %arrayidx
  %tmp37 = load double addrspace(1)** %p          ; <double addrspace(1)*> [#uses=1]
  %tmp38 = load i64* %k                           ; <i64> [#uses=1]
  %conv39 = trunc i64 %tmp38 to i32               ; <i32> [#uses=1]
  %arrayidx40 = getelementptr double addrspace(1)* %tmp37, i32 %conv39 ; <double addrspace(1)*> [#uses=1]
  %tmp41 = load double addrspace(1)** %p          ; <double addrspace(1)*> [#uses=1]
  %tmp42 = load i64* %k                           ; <i64> [#uses=1]
  %conv43 = trunc i64 %tmp42 to i32               ; <i32> [#uses=1]
  %arrayidx44 = getelementptr double addrspace(1)* %tmp41, i32 %conv43 ; <double addrspace(1)*> [#uses=1]
  %tmp45 = load double addrspace(1)* %arrayidx44  ; <double> [#uses=1]
  %tmp46 = load double addrspace(1)** %rho        ; <double addrspace(1)*> [#uses=1]
  %tmp47 = load i64* %k                           ; <i64> [#uses=1]
  %conv48 = trunc i64 %tmp47 to i32               ; <i32> [#uses=1]
  %arrayidx49 = getelementptr double addrspace(1)* %tmp46, i32 %conv48 ; <double addrspace(1)*> [#uses=1]
  %tmp50 = load double addrspace(1)* %arrayidx49  ; <double> [#uses=1]
  %tmp51 = load double* %smallp                   ; <double> [#uses=1]
  %tmp52 = fmul double %tmp50, %tmp51             ; <double> [#uses=1]
  %call53 = call double @__max_f64(double %tmp45, double %tmp52) nounwind ; <double> [#uses=1]
  store double %call53, double addrspace(1)* %arrayidx40
  %tmp54 = load double addrspace(1)** %c.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp55 = load i64* %k                           ; <i64> [#uses=1]
  %conv56 = trunc i64 %tmp55 to i32               ; <i32> [#uses=1]
  %arrayidx57 = getelementptr double addrspace(1)* %tmp54, i32 %conv56 ; <double addrspace(1)*> [#uses=1]
  %tmp58 = load double* %Hgamma.addr              ; <double> [#uses=1]
  %tmp59 = load double addrspace(1)** %p          ; <double addrspace(1)*> [#uses=1]
  %tmp60 = load i64* %k                           ; <i64> [#uses=1]
  %conv61 = trunc i64 %tmp60 to i32               ; <i32> [#uses=1]
  %arrayidx62 = getelementptr double addrspace(1)* %tmp59, i32 %conv61 ; <double addrspace(1)*> [#uses=1]
  %tmp63 = load double addrspace(1)* %arrayidx62  ; <double> [#uses=1]
  %tmp64 = fmul double %tmp58, %tmp63             ; <double> [#uses=1]
  %tmp65 = load double addrspace(1)** %rho        ; <double addrspace(1)*> [#uses=1]
  %tmp66 = load i64* %k                           ; <i64> [#uses=1]
  %conv67 = trunc i64 %tmp66 to i32               ; <i32> [#uses=1]
  %arrayidx68 = getelementptr double addrspace(1)* %tmp65, i32 %conv67 ; <double addrspace(1)*> [#uses=1]
  %tmp69 = load double addrspace(1)* %arrayidx68  ; <double> [#uses=1]
  %tmp70 = fdiv double %tmp64, %tmp69             ; <double> [#uses=1]
  %call71 = call double @__sqrt_f64(double %tmp70) nounwind ; <double> [#uses=1]
  store double %call71, double addrspace(1)* %arrayidx57
  br label %return

if.then14:                                        ; preds = %if.end
  br label %return
}

declare double @__sqrt_f64(double) nounwind

define void @__OpenCL_Loop1KcuMakeBoundary_kernel(i64 %i, i64 %i0, double %sign, i64 %Hjmin, i64 %n, i64 %Hnxt, i64 %Hnyt, i64 %Hnvar, double addrspace(1)* %uold) nounwind {
entry:
  %i.addr = alloca i64, align 8                   ; <i64*> [#uses=2]
  %i0.addr = alloca i64, align 8                  ; <i64*> [#uses=2]
  %sign.addr = alloca double, align 8             ; <double*> [#uses=2]
  %Hjmin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %n.addr = alloca i64, align 8                   ; <i64*> [#uses=3]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=3]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=3]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=3]
  %tmp = alloca i64, align 8                      ; <i64*> [#uses=3]
  %j = alloca i64, align 8                        ; <i64*> [#uses=5]
  %ivar = alloca i64, align 8                     ; <i64*> [#uses=6]
  %vsign = alloca double, align 8                 ; <double*> [#uses=3]
  store i64 %i, i64* %i.addr
  store i64 %i0, i64* %i0.addr
  store double %sign, double* %sign.addr
  store i64 %Hjmin, i64* %Hjmin.addr
  store i64 %n, i64* %n.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  %tmp1 = load double* %sign.addr                 ; <double> [#uses=1]
  store double %tmp1, double* %vsign
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %call2 = call i32 @get_global_size(i32 1) nounwind ; <i32> [#uses=1]
  %tmp3 = sub i32 %call2, 1                       ; <i32> [#uses=1]
  %call4 = call i32 @get_global_id(i32 1) nounwind ; <i32> [#uses=1]
  %call5 = call i32 @get_global_size(i32 2) nounwind ; <i32> [#uses=1]
  %tmp6 = sub i32 %call5, 1                       ; <i32> [#uses=1]
  %call7 = call i32 @get_global_id(i32 2) nounwind ; <i32> [#uses=1]
  %tmp8 = mul i32 %tmp6, %call7                   ; <i32> [#uses=1]
  %tmp9 = add i32 %call4, %tmp8                   ; <i32> [#uses=1]
  %tmp10 = mul i32 %tmp3, %tmp9                   ; <i32> [#uses=1]
  %tmp11 = add i32 %call, %tmp10                  ; <i32> [#uses=1]
  %conv = zext i32 %tmp11 to i64                  ; <i64> [#uses=1]
  store i64 %conv, i64* %tmp
  %tmp12 = load i64* %tmp                         ; <i64> [#uses=1]
  %tmp13 = load i64* %n.addr                      ; <i64> [#uses=1]
  %tmp14 = sdiv i64 %tmp12, %tmp13                ; <i64> [#uses=1]
  store i64 %tmp14, i64* %ivar
  %tmp15 = load i64* %tmp                         ; <i64> [#uses=1]
  %tmp16 = load i64* %ivar                        ; <i64> [#uses=1]
  %tmp17 = load i64* %n.addr                      ; <i64> [#uses=1]
  %tmp18 = mul i64 %tmp16, %tmp17                 ; <i64> [#uses=1]
  %tmp19 = sub i64 %tmp15, %tmp18                 ; <i64> [#uses=1]
  store i64 %tmp19, i64* %j
  %tmp20 = load i64* %ivar                        ; <i64> [#uses=1]
  %tmp21 = load i64* %Hnvar.addr                  ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp20, %tmp21              ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end24, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp22 = load i64* %ivar                        ; <i64> [#uses=1]
  %cmp23 = icmp eq i64 %tmp22, 1                  ; <i1> [#uses=1]
  br i1 %cmp23, label %if.then25, label %if.end24

if.then:                                          ; preds = %entry
  br label %return

if.end24:                                         ; preds = %if.then25, %if.end
  %tmp26 = load i64* %j                           ; <i64> [#uses=1]
  %tmp27 = load i64* %Hjmin.addr                  ; <i64> [#uses=1]
  %tmp28 = add i64 %tmp27, 2                      ; <i64> [#uses=1]
  %tmp29 = add i64 %tmp26, %tmp28                 ; <i64> [#uses=1]
  store i64 %tmp29, i64* %j
  %tmp30 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp31 = load i64* %i.addr                      ; <i64> [#uses=1]
  %conv32 = trunc i64 %tmp31 to i32               ; <i32> [#uses=1]
  %tmp33 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv34 = trunc i64 %tmp33 to i32               ; <i32> [#uses=1]
  %tmp35 = load i64* %j                           ; <i64> [#uses=1]
  %conv36 = trunc i64 %tmp35 to i32               ; <i32> [#uses=1]
  %tmp37 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv38 = trunc i64 %tmp37 to i32               ; <i32> [#uses=1]
  %tmp39 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv40 = trunc i64 %tmp39 to i32               ; <i32> [#uses=1]
  %tmp41 = mul i32 %conv38, %conv40               ; <i32> [#uses=1]
  %tmp42 = add i32 %conv36, %tmp41                ; <i32> [#uses=1]
  %tmp43 = mul i32 %conv34, %tmp42                ; <i32> [#uses=1]
  %tmp44 = add i32 %conv32, %tmp43                ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp30, i32 %tmp44 ; <double addrspace(1)*> [#uses=1]
  %tmp45 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp46 = load i64* %i0.addr                     ; <i64> [#uses=1]
  %conv47 = trunc i64 %tmp46 to i32               ; <i32> [#uses=1]
  %tmp48 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv49 = trunc i64 %tmp48 to i32               ; <i32> [#uses=1]
  %tmp50 = load i64* %j                           ; <i64> [#uses=1]
  %conv51 = trunc i64 %tmp50 to i32               ; <i32> [#uses=1]
  %tmp52 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv53 = trunc i64 %tmp52 to i32               ; <i32> [#uses=1]
  %tmp54 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv55 = trunc i64 %tmp54 to i32               ; <i32> [#uses=1]
  %tmp56 = mul i32 %conv53, %conv55               ; <i32> [#uses=1]
  %tmp57 = add i32 %conv51, %tmp56                ; <i32> [#uses=1]
  %tmp58 = mul i32 %conv49, %tmp57                ; <i32> [#uses=1]
  %tmp59 = add i32 %conv47, %tmp58                ; <i32> [#uses=1]
  %arrayidx60 = getelementptr double addrspace(1)* %tmp45, i32 %tmp59 ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load double addrspace(1)* %arrayidx60  ; <double> [#uses=1]
  %tmp62 = load double* %vsign                    ; <double> [#uses=1]
  %tmp63 = fmul double %tmp61, %tmp62             ; <double> [#uses=1]
  store double %tmp63, double addrspace(1)* %arrayidx
  br label %return

if.then25:                                        ; preds = %if.end
  store double -1.000000e+00, double* %vsign
  br label %if.end24
}

define void @__OpenCL_Loop2KcuMakeBoundary_kernel(i64 %j, i64 %j0, double %sign, i64 %Himin, i64 %n, i64 %Hnxt, i64 %Hnyt, i64 %Hnvar, double addrspace(1)* %uold) nounwind {
entry:
  %j.addr = alloca i64, align 8                   ; <i64*> [#uses=2]
  %j0.addr = alloca i64, align 8                  ; <i64*> [#uses=2]
  %sign.addr = alloca double, align 8             ; <double*> [#uses=2]
  %Himin.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %n.addr = alloca i64, align 8                   ; <i64*> [#uses=3]
  %Hnxt.addr = alloca i64, align 8                ; <i64*> [#uses=3]
  %Hnyt.addr = alloca i64, align 8                ; <i64*> [#uses=3]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %uold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=3]
  %tmp = alloca i64, align 8                      ; <i64*> [#uses=3]
  %i = alloca i64, align 8                        ; <i64*> [#uses=5]
  %ivar = alloca i64, align 8                     ; <i64*> [#uses=6]
  %vsign = alloca double, align 8                 ; <double*> [#uses=3]
  store i64 %j, i64* %j.addr
  store i64 %j0, i64* %j0.addr
  store double %sign, double* %sign.addr
  store i64 %Himin, i64* %Himin.addr
  store i64 %n, i64* %n.addr
  store i64 %Hnxt, i64* %Hnxt.addr
  store i64 %Hnyt, i64* %Hnyt.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  store double addrspace(1)* %uold, double addrspace(1)** %uold.addr
  %tmp1 = load double* %sign.addr                 ; <double> [#uses=1]
  store double %tmp1, double* %vsign
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %call2 = call i32 @get_global_size(i32 1) nounwind ; <i32> [#uses=1]
  %tmp3 = sub i32 %call2, 1                       ; <i32> [#uses=1]
  %call4 = call i32 @get_global_id(i32 1) nounwind ; <i32> [#uses=1]
  %call5 = call i32 @get_global_size(i32 2) nounwind ; <i32> [#uses=1]
  %tmp6 = sub i32 %call5, 1                       ; <i32> [#uses=1]
  %call7 = call i32 @get_global_id(i32 2) nounwind ; <i32> [#uses=1]
  %tmp8 = mul i32 %tmp6, %call7                   ; <i32> [#uses=1]
  %tmp9 = add i32 %call4, %tmp8                   ; <i32> [#uses=1]
  %tmp10 = mul i32 %tmp3, %tmp9                   ; <i32> [#uses=1]
  %tmp11 = add i32 %call, %tmp10                  ; <i32> [#uses=1]
  %conv = zext i32 %tmp11 to i64                  ; <i64> [#uses=1]
  store i64 %conv, i64* %tmp
  %tmp12 = load i64* %tmp                         ; <i64> [#uses=1]
  %tmp13 = load i64* %n.addr                      ; <i64> [#uses=1]
  %tmp14 = sdiv i64 %tmp12, %tmp13                ; <i64> [#uses=1]
  store i64 %tmp14, i64* %ivar
  %tmp15 = load i64* %tmp                         ; <i64> [#uses=1]
  %tmp16 = load i64* %ivar                        ; <i64> [#uses=1]
  %tmp17 = load i64* %n.addr                      ; <i64> [#uses=1]
  %tmp18 = mul i64 %tmp16, %tmp17                 ; <i64> [#uses=1]
  %tmp19 = sub i64 %tmp15, %tmp18                 ; <i64> [#uses=1]
  store i64 %tmp19, i64* %i
  %tmp20 = load i64* %ivar                        ; <i64> [#uses=1]
  %tmp21 = load i64* %Hnvar.addr                  ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp20, %tmp21              ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end24, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp22 = load i64* %ivar                        ; <i64> [#uses=1]
  %cmp23 = icmp eq i64 %tmp22, 2                  ; <i1> [#uses=1]
  br i1 %cmp23, label %if.then25, label %if.end24

if.then:                                          ; preds = %entry
  br label %return

if.end24:                                         ; preds = %if.then25, %if.end
  %tmp26 = load i64* %i                           ; <i64> [#uses=1]
  %tmp27 = load i64* %Himin.addr                  ; <i64> [#uses=1]
  %tmp28 = add i64 %tmp27, 2                      ; <i64> [#uses=1]
  %tmp29 = add i64 %tmp26, %tmp28                 ; <i64> [#uses=1]
  store i64 %tmp29, i64* %i
  %tmp30 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp31 = load i64* %i                           ; <i64> [#uses=1]
  %conv32 = trunc i64 %tmp31 to i32               ; <i32> [#uses=1]
  %tmp33 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv34 = trunc i64 %tmp33 to i32               ; <i32> [#uses=1]
  %tmp35 = load i64* %j.addr                      ; <i64> [#uses=1]
  %conv36 = trunc i64 %tmp35 to i32               ; <i32> [#uses=1]
  %tmp37 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv38 = trunc i64 %tmp37 to i32               ; <i32> [#uses=1]
  %tmp39 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv40 = trunc i64 %tmp39 to i32               ; <i32> [#uses=1]
  %tmp41 = mul i32 %conv38, %conv40               ; <i32> [#uses=1]
  %tmp42 = add i32 %conv36, %tmp41                ; <i32> [#uses=1]
  %tmp43 = mul i32 %conv34, %tmp42                ; <i32> [#uses=1]
  %tmp44 = add i32 %conv32, %tmp43                ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp30, i32 %tmp44 ; <double addrspace(1)*> [#uses=1]
  %tmp45 = load double addrspace(1)** %uold.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp46 = load i64* %i                           ; <i64> [#uses=1]
  %conv47 = trunc i64 %tmp46 to i32               ; <i32> [#uses=1]
  %tmp48 = load i64* %Hnxt.addr                   ; <i64> [#uses=1]
  %conv49 = trunc i64 %tmp48 to i32               ; <i32> [#uses=1]
  %tmp50 = load i64* %j0.addr                     ; <i64> [#uses=1]
  %conv51 = trunc i64 %tmp50 to i32               ; <i32> [#uses=1]
  %tmp52 = load i64* %Hnyt.addr                   ; <i64> [#uses=1]
  %conv53 = trunc i64 %tmp52 to i32               ; <i32> [#uses=1]
  %tmp54 = load i64* %ivar                        ; <i64> [#uses=1]
  %conv55 = trunc i64 %tmp54 to i32               ; <i32> [#uses=1]
  %tmp56 = mul i32 %conv53, %conv55               ; <i32> [#uses=1]
  %tmp57 = add i32 %conv51, %tmp56                ; <i32> [#uses=1]
  %tmp58 = mul i32 %conv49, %tmp57                ; <i32> [#uses=1]
  %tmp59 = add i32 %conv47, %tmp58                ; <i32> [#uses=1]
  %arrayidx60 = getelementptr double addrspace(1)* %tmp45, i32 %tmp59 ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load double addrspace(1)* %arrayidx60  ; <double> [#uses=1]
  %tmp62 = load double* %vsign                    ; <double> [#uses=1]
  %tmp63 = fmul double %tmp61, %tmp62             ; <double> [#uses=1]
  store double %tmp63, double addrspace(1)* %arrayidx
  br label %return

if.then25:                                        ; preds = %if.end
  store double -1.000000e+00, double* %vsign
  br label %if.end24
}

define void @__OpenCL_Loop1KcuQleftright_kernel(i64 %bmax, i64 %Hnvar, i64 %Hnxyt, double addrspace(1)* %qxm, double addrspace(1)* %qxp, double addrspace(1)* %qleft, double addrspace(1)* %qright) nounwind {
entry:
  %bmax.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=5]
  %qxm.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %qxp.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %qleft.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %qright.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %nvar = alloca i64, align 8                     ; <i64*> [#uses=8]
  %i = alloca i64, align 8                        ; <i64*> [#uses=6]
  store i64 %bmax, i64* %bmax.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store double addrspace(1)* %qxm, double addrspace(1)** %qxm.addr
  store double addrspace(1)* %qxp, double addrspace(1)** %qxp.addr
  store double addrspace(1)* %qleft, double addrspace(1)** %qleft.addr
  store double addrspace(1)* %qright, double addrspace(1)** %qright.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %bmax.addr                    ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %for.exit, %if.then
  ret void

if.end:                                           ; preds = %entry
  store i64 0, i64* %nvar
  br label %for.cond

if.then:                                          ; preds = %entry
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end
  %tmp2 = load i64* %nvar                         ; <i64> [#uses=1]
  %tmp3 = load i64* %Hnvar.addr                   ; <i64> [#uses=1]
  %cmp4 = icmp slt i64 %tmp2, %tmp3               ; <i1> [#uses=1]
  br i1 %cmp4, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %return

for.body:                                         ; preds = %for.cond
  %tmp5 = load double addrspace(1)** %qleft.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp6 = load i64* %i                            ; <i64> [#uses=1]
  %conv7 = trunc i64 %tmp6 to i32                 ; <i32> [#uses=1]
  %tmp8 = load i64* %nvar                         ; <i64> [#uses=1]
  %conv9 = trunc i64 %tmp8 to i32                 ; <i32> [#uses=1]
  %tmp10 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv11 = trunc i64 %tmp10 to i32               ; <i32> [#uses=1]
  %tmp12 = mul i32 %conv9, %conv11                ; <i32> [#uses=1]
  %tmp13 = add i32 %conv7, %tmp12                 ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp5, i32 %tmp13 ; <double addrspace(1)*> [#uses=1]
  %tmp14 = load double addrspace(1)** %qxm.addr   ; <double addrspace(1)*> [#uses=1]
  %tmp15 = load i64* %i                           ; <i64> [#uses=1]
  %tmp16 = add i64 %tmp15, 1                      ; <i64> [#uses=1]
  %conv17 = trunc i64 %tmp16 to i32               ; <i32> [#uses=1]
  %tmp18 = load i64* %nvar                        ; <i64> [#uses=1]
  %conv19 = trunc i64 %tmp18 to i32               ; <i32> [#uses=1]
  %tmp20 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv21 = trunc i64 %tmp20 to i32               ; <i32> [#uses=1]
  %tmp22 = mul i32 %conv19, %conv21               ; <i32> [#uses=1]
  %tmp23 = add i32 %conv17, %tmp22                ; <i32> [#uses=1]
  %arrayidx24 = getelementptr double addrspace(1)* %tmp14, i32 %tmp23 ; <double addrspace(1)*> [#uses=1]
  %tmp25 = load double addrspace(1)* %arrayidx24  ; <double> [#uses=1]
  store double %tmp25, double addrspace(1)* %arrayidx
  %tmp26 = load double addrspace(1)** %qright.addr ; <double addrspace(1)*> [#uses=1]
  %tmp27 = load i64* %i                           ; <i64> [#uses=1]
  %conv28 = trunc i64 %tmp27 to i32               ; <i32> [#uses=1]
  %tmp29 = load i64* %nvar                        ; <i64> [#uses=1]
  %conv30 = trunc i64 %tmp29 to i32               ; <i32> [#uses=1]
  %tmp31 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv32 = trunc i64 %tmp31 to i32               ; <i32> [#uses=1]
  %tmp33 = mul i32 %conv30, %conv32               ; <i32> [#uses=1]
  %tmp34 = add i32 %conv28, %tmp33                ; <i32> [#uses=1]
  %arrayidx35 = getelementptr double addrspace(1)* %tmp26, i32 %tmp34 ; <double addrspace(1)*> [#uses=1]
  %tmp36 = load double addrspace(1)** %qxp.addr   ; <double addrspace(1)*> [#uses=1]
  %tmp37 = load i64* %i                           ; <i64> [#uses=1]
  %tmp38 = add i64 %tmp37, 2                      ; <i64> [#uses=1]
  %conv39 = trunc i64 %tmp38 to i32               ; <i32> [#uses=1]
  %tmp40 = load i64* %nvar                        ; <i64> [#uses=1]
  %conv41 = trunc i64 %tmp40 to i32               ; <i32> [#uses=1]
  %tmp42 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv43 = trunc i64 %tmp42 to i32               ; <i32> [#uses=1]
  %tmp44 = mul i32 %conv41, %conv43               ; <i32> [#uses=1]
  %tmp45 = add i32 %conv39, %tmp44                ; <i32> [#uses=1]
  %arrayidx46 = getelementptr double addrspace(1)* %tmp36, i32 %tmp45 ; <double addrspace(1)*> [#uses=1]
  %tmp47 = load double addrspace(1)* %arrayidx46  ; <double> [#uses=1]
  store double %tmp47, double addrspace(1)* %arrayidx35
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %tmp48 = load i64* %nvar                        ; <i64> [#uses=1]
  %tmp49 = add i64 %tmp48, 1                      ; <i64> [#uses=1]
  store i64 %tmp49, i64* %nvar
  br label %for.cond
}

define void @__OpenCL_Init1KcuRiemann_kernel(%struct._Args addrspace(1)* %K, double addrspace(1)* %qleft, double addrspace(1)* %qright, double addrspace(1)* %qgdnv, double addrspace(1)* %rl, double addrspace(1)* %ul, double addrspace(1)* %pl, double addrspace(1)* %cl, double addrspace(1)* %wl, double addrspace(1)* %rr, double addrspace(1)* %ur) nounwind {
entry:
  %K.addr = alloca %struct._Args addrspace(1)*, align 4 ; <%struct._Args addrspace(1)**> [#uses=11]
  %qleft.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %qright.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %qgdnv.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %rl.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %ul.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %pl.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %cl.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %wl.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %rr.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %ur.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %tid = alloca i64, align 8                      ; <i64*> [#uses=2]
  store %struct._Args addrspace(1)* %K, %struct._Args addrspace(1)** %K.addr
  store double addrspace(1)* %qleft, double addrspace(1)** %qleft.addr
  store double addrspace(1)* %qright, double addrspace(1)** %qright.addr
  store double addrspace(1)* %qgdnv, double addrspace(1)** %qgdnv.addr
  store double addrspace(1)* %rl, double addrspace(1)** %rl.addr
  store double addrspace(1)* %ul, double addrspace(1)** %ul.addr
  store double addrspace(1)* %pl, double addrspace(1)** %pl.addr
  store double addrspace(1)* %cl, double addrspace(1)** %cl.addr
  store double addrspace(1)* %wl, double addrspace(1)** %wl.addr
  store double addrspace(1)* %rr, double addrspace(1)** %rr.addr
  store double addrspace(1)* %ur, double addrspace(1)** %ur.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %tid
  %tmp = load i64* %tid                           ; <i64> [#uses=1]
  %cmp = icmp ne i64 %tmp, 0                      ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp1 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele = getelementptr inbounds %struct._Args addrspace(1)* %tmp1, i32 0, i32 0 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp2 = load double addrspace(1)** %qleft.addr  ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp2, double addrspace(1)* addrspace(1)* %structele
  %tmp3 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele4 = getelementptr inbounds %struct._Args addrspace(1)* %tmp3, i32 0, i32 1 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp5 = load double addrspace(1)** %qright.addr ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp5, double addrspace(1)* addrspace(1)* %structele4
  %tmp6 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele7 = getelementptr inbounds %struct._Args addrspace(1)* %tmp6, i32 0, i32 2 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp8 = load double addrspace(1)** %qgdnv.addr  ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp8, double addrspace(1)* addrspace(1)* %structele7
  %tmp9 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele10 = getelementptr inbounds %struct._Args addrspace(1)* %tmp9, i32 0, i32 3 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp11 = load double addrspace(1)** %rl.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp11, double addrspace(1)* addrspace(1)* %structele10
  %tmp12 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele13 = getelementptr inbounds %struct._Args addrspace(1)* %tmp12, i32 0, i32 4 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp14 = load double addrspace(1)** %ul.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp14, double addrspace(1)* addrspace(1)* %structele13
  %tmp15 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele16 = getelementptr inbounds %struct._Args addrspace(1)* %tmp15, i32 0, i32 5 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp17 = load double addrspace(1)** %pl.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp17, double addrspace(1)* addrspace(1)* %structele16
  %tmp18 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele19 = getelementptr inbounds %struct._Args addrspace(1)* %tmp18, i32 0, i32 6 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp20 = load double addrspace(1)** %cl.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp20, double addrspace(1)* addrspace(1)* %structele19
  %tmp21 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele22 = getelementptr inbounds %struct._Args addrspace(1)* %tmp21, i32 0, i32 7 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp23 = load double addrspace(1)** %wl.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp23, double addrspace(1)* addrspace(1)* %structele22
  %tmp24 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele25 = getelementptr inbounds %struct._Args addrspace(1)* %tmp24, i32 0, i32 8 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp26 = load double addrspace(1)** %rr.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp26, double addrspace(1)* addrspace(1)* %structele25
  %tmp27 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele28 = getelementptr inbounds %struct._Args addrspace(1)* %tmp27, i32 0, i32 9 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp29 = load double addrspace(1)** %ur.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp29, double addrspace(1)* addrspace(1)* %structele28
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

define void @__OpenCL_Init2KcuRiemann_kernel(%struct._Args addrspace(1)* %K, double addrspace(1)* %pr, double addrspace(1)* %cr, double addrspace(1)* %wr, double addrspace(1)* %ro, double addrspace(1)* %uo, double addrspace(1)* %po, double addrspace(1)* %co, double addrspace(1)* %wo, double addrspace(1)* %rstar, double addrspace(1)* %ustar) nounwind {
entry:
  %K.addr = alloca %struct._Args addrspace(1)*, align 4 ; <%struct._Args addrspace(1)**> [#uses=11]
  %pr.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %cr.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %wr.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %ro.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %uo.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %po.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %co.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %wo.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %rstar.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %ustar.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %tid = alloca i64, align 8                      ; <i64*> [#uses=2]
  store %struct._Args addrspace(1)* %K, %struct._Args addrspace(1)** %K.addr
  store double addrspace(1)* %pr, double addrspace(1)** %pr.addr
  store double addrspace(1)* %cr, double addrspace(1)** %cr.addr
  store double addrspace(1)* %wr, double addrspace(1)** %wr.addr
  store double addrspace(1)* %ro, double addrspace(1)** %ro.addr
  store double addrspace(1)* %uo, double addrspace(1)** %uo.addr
  store double addrspace(1)* %po, double addrspace(1)** %po.addr
  store double addrspace(1)* %co, double addrspace(1)** %co.addr
  store double addrspace(1)* %wo, double addrspace(1)** %wo.addr
  store double addrspace(1)* %rstar, double addrspace(1)** %rstar.addr
  store double addrspace(1)* %ustar, double addrspace(1)** %ustar.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %tid
  %tmp = load i64* %tid                           ; <i64> [#uses=1]
  %cmp = icmp ne i64 %tmp, 0                      ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp1 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele = getelementptr inbounds %struct._Args addrspace(1)* %tmp1, i32 0, i32 10 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp2 = load double addrspace(1)** %pr.addr     ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp2, double addrspace(1)* addrspace(1)* %structele
  %tmp3 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele4 = getelementptr inbounds %struct._Args addrspace(1)* %tmp3, i32 0, i32 11 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp5 = load double addrspace(1)** %cr.addr     ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp5, double addrspace(1)* addrspace(1)* %structele4
  %tmp6 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele7 = getelementptr inbounds %struct._Args addrspace(1)* %tmp6, i32 0, i32 12 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp8 = load double addrspace(1)** %wr.addr     ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp8, double addrspace(1)* addrspace(1)* %structele7
  %tmp9 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele10 = getelementptr inbounds %struct._Args addrspace(1)* %tmp9, i32 0, i32 13 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp11 = load double addrspace(1)** %ro.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp11, double addrspace(1)* addrspace(1)* %structele10
  %tmp12 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele13 = getelementptr inbounds %struct._Args addrspace(1)* %tmp12, i32 0, i32 14 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp14 = load double addrspace(1)** %uo.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp14, double addrspace(1)* addrspace(1)* %structele13
  %tmp15 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele16 = getelementptr inbounds %struct._Args addrspace(1)* %tmp15, i32 0, i32 15 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp17 = load double addrspace(1)** %po.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp17, double addrspace(1)* addrspace(1)* %structele16
  %tmp18 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele19 = getelementptr inbounds %struct._Args addrspace(1)* %tmp18, i32 0, i32 16 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp20 = load double addrspace(1)** %co.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp20, double addrspace(1)* addrspace(1)* %structele19
  %tmp21 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele22 = getelementptr inbounds %struct._Args addrspace(1)* %tmp21, i32 0, i32 17 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp23 = load double addrspace(1)** %wo.addr    ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp23, double addrspace(1)* addrspace(1)* %structele22
  %tmp24 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele25 = getelementptr inbounds %struct._Args addrspace(1)* %tmp24, i32 0, i32 18 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp26 = load double addrspace(1)** %rstar.addr ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp26, double addrspace(1)* addrspace(1)* %structele25
  %tmp27 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele28 = getelementptr inbounds %struct._Args addrspace(1)* %tmp27, i32 0, i32 19 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp29 = load double addrspace(1)** %ustar.addr ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp29, double addrspace(1)* addrspace(1)* %structele28
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

define void @__OpenCL_Init3KcuRiemann_kernel(%struct._Args addrspace(1)* %K, double addrspace(1)* %pstar, double addrspace(1)* %cstar, i64 addrspace(1)* %sgnm, double addrspace(1)* %spin, double addrspace(1)* %spout, double addrspace(1)* %ushock, double addrspace(1)* %frac, double addrspace(1)* %scr, double addrspace(1)* %delp, double addrspace(1)* %pold) nounwind {
entry:
  %K.addr = alloca %struct._Args addrspace(1)*, align 4 ; <%struct._Args addrspace(1)**> [#uses=11]
  %pstar.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %cstar.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %sgnm.addr = alloca i64 addrspace(1)*, align 4  ; <i64 addrspace(1)**> [#uses=2]
  %spin.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %spout.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %ushock.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %frac.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %scr.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %delp.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %pold.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %tid = alloca i64, align 8                      ; <i64*> [#uses=2]
  store %struct._Args addrspace(1)* %K, %struct._Args addrspace(1)** %K.addr
  store double addrspace(1)* %pstar, double addrspace(1)** %pstar.addr
  store double addrspace(1)* %cstar, double addrspace(1)** %cstar.addr
  store i64 addrspace(1)* %sgnm, i64 addrspace(1)** %sgnm.addr
  store double addrspace(1)* %spin, double addrspace(1)** %spin.addr
  store double addrspace(1)* %spout, double addrspace(1)** %spout.addr
  store double addrspace(1)* %ushock, double addrspace(1)** %ushock.addr
  store double addrspace(1)* %frac, double addrspace(1)** %frac.addr
  store double addrspace(1)* %scr, double addrspace(1)** %scr.addr
  store double addrspace(1)* %delp, double addrspace(1)** %delp.addr
  store double addrspace(1)* %pold, double addrspace(1)** %pold.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %tid
  %tmp = load i64* %tid                           ; <i64> [#uses=1]
  %cmp = icmp ne i64 %tmp, 0                      ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp1 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele = getelementptr inbounds %struct._Args addrspace(1)* %tmp1, i32 0, i32 20 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp2 = load double addrspace(1)** %pstar.addr  ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp2, double addrspace(1)* addrspace(1)* %structele
  %tmp3 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele4 = getelementptr inbounds %struct._Args addrspace(1)* %tmp3, i32 0, i32 21 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp5 = load double addrspace(1)** %cstar.addr  ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp5, double addrspace(1)* addrspace(1)* %structele4
  %tmp6 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele7 = getelementptr inbounds %struct._Args addrspace(1)* %tmp6, i32 0, i32 23 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp8 = load double addrspace(1)** %spin.addr   ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp8, double addrspace(1)* addrspace(1)* %structele7
  %tmp9 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele10 = getelementptr inbounds %struct._Args addrspace(1)* %tmp9, i32 0, i32 24 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp11 = load double addrspace(1)** %spout.addr ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp11, double addrspace(1)* addrspace(1)* %structele10
  %tmp12 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele13 = getelementptr inbounds %struct._Args addrspace(1)* %tmp12, i32 0, i32 25 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp14 = load double addrspace(1)** %ushock.addr ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp14, double addrspace(1)* addrspace(1)* %structele13
  %tmp15 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele16 = getelementptr inbounds %struct._Args addrspace(1)* %tmp15, i32 0, i32 26 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp17 = load double addrspace(1)** %frac.addr  ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp17, double addrspace(1)* addrspace(1)* %structele16
  %tmp18 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele19 = getelementptr inbounds %struct._Args addrspace(1)* %tmp18, i32 0, i32 27 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp20 = load double addrspace(1)** %scr.addr   ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp20, double addrspace(1)* addrspace(1)* %structele19
  %tmp21 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele22 = getelementptr inbounds %struct._Args addrspace(1)* %tmp21, i32 0, i32 28 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp23 = load double addrspace(1)** %delp.addr  ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp23, double addrspace(1)* addrspace(1)* %structele22
  %tmp24 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele25 = getelementptr inbounds %struct._Args addrspace(1)* %tmp24, i32 0, i32 29 ; <double addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp26 = load double addrspace(1)** %pold.addr  ; <double addrspace(1)*> [#uses=1]
  store double addrspace(1)* %tmp26, double addrspace(1)* addrspace(1)* %structele25
  %tmp27 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele28 = getelementptr inbounds %struct._Args addrspace(1)* %tmp27, i32 0, i32 22 ; <i64 addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp29 = load i64 addrspace(1)** %sgnm.addr     ; <i64 addrspace(1)*> [#uses=1]
  store i64 addrspace(1)* %tmp29, i64 addrspace(1)* addrspace(1)* %structele28
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

define void @__OpenCL_Init4KcuRiemann_kernel(%struct._Args addrspace(1)* %K, i64 addrspace(1)* %ind, i64 addrspace(1)* %ind2) nounwind {
entry:
  %K.addr = alloca %struct._Args addrspace(1)*, align 4 ; <%struct._Args addrspace(1)**> [#uses=3]
  %ind.addr = alloca i64 addrspace(1)*, align 4   ; <i64 addrspace(1)**> [#uses=2]
  %ind2.addr = alloca i64 addrspace(1)*, align 4  ; <i64 addrspace(1)**> [#uses=2]
  %tid = alloca i64, align 8                      ; <i64*> [#uses=2]
  store %struct._Args addrspace(1)* %K, %struct._Args addrspace(1)** %K.addr
  store i64 addrspace(1)* %ind, i64 addrspace(1)** %ind.addr
  store i64 addrspace(1)* %ind2, i64 addrspace(1)** %ind2.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %tid
  %tmp = load i64* %tid                           ; <i64> [#uses=1]
  %cmp = icmp ne i64 %tmp, 0                      ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp1 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele = getelementptr inbounds %struct._Args addrspace(1)* %tmp1, i32 0, i32 30 ; <i64 addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp2 = load i64 addrspace(1)** %ind.addr       ; <i64 addrspace(1)*> [#uses=1]
  store i64 addrspace(1)* %tmp2, i64 addrspace(1)* addrspace(1)* %structele
  %tmp3 = load %struct._Args addrspace(1)** %K.addr ; <%struct._Args addrspace(1)*> [#uses=1]
  %structele4 = getelementptr inbounds %struct._Args addrspace(1)* %tmp3, i32 0, i32 31 ; <i64 addrspace(1)* addrspace(1)*> [#uses=1]
  %tmp5 = load i64 addrspace(1)** %ind2.addr      ; <i64 addrspace(1)*> [#uses=1]
  store i64 addrspace(1)* %tmp5, i64 addrspace(1)* addrspace(1)* %structele4
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

define void @__OpenCL_LoopKcuSlope_kernel(double addrspace(1)* %q, double addrspace(1)* %dq, i64 %Hnvar, i64 %Hnxyt, double %slope_type, i64 %ijmin, i64 %ijmax) nounwind {
entry:
  %q.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=5]
  %dq.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=4]
  %slope_type.addr = alloca double, align 8       ; <double*> [#uses=4]
  %ijmin.addr = alloca i64, align 8               ; <i64*> [#uses=3]
  %ijmax.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %tmp = alloca i64, align 8                      ; <i64*> [#uses=3]
  %tmp1 = alloca i64, align 8                     ; <i64*> [#uses=3]
  %n = alloca i64, align 8                        ; <i64*> [#uses=6]
  %dlft = alloca double, align 8                  ; <double*> [#uses=4]
  %drgt = alloca double, align 8                  ; <double*> [#uses=4]
  %dcen = alloca double, align 8                  ; <double*> [#uses=3]
  %dsgn = alloca double, align 8                  ; <double*> [#uses=2]
  %slop = alloca double, align 8                  ; <double*> [#uses=2]
  %dlim = alloca double, align 8                  ; <double*> [#uses=2]
  %ihvwin = alloca i64, align 8                   ; <i64*> [#uses=4]
  %ihvwimn = alloca i64, align 8                  ; <i64*> [#uses=2]
  %ihvwipn = alloca i64, align 8                  ; <i64*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=6]
  store double addrspace(1)* %q, double addrspace(1)** %q.addr
  store double addrspace(1)* %dq, double addrspace(1)** %dq.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store double %slope_type, double* %slope_type.addr
  store i64 %ijmin, i64* %ijmin.addr
  store i64 %ijmax, i64* %ijmax.addr
  %tmp2 = load i64* %ijmax.addr                   ; <i64> [#uses=1]
  %tmp3 = load i64* %ijmin.addr                   ; <i64> [#uses=1]
  %tmp4 = sub i64 %tmp2, %tmp3                    ; <i64> [#uses=1]
  store i64 %tmp4, i64* %tmp
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %call5 = call i32 @get_global_size(i32 1) nounwind ; <i32> [#uses=1]
  %tmp6 = sub i32 %call5, 1                       ; <i32> [#uses=1]
  %call7 = call i32 @get_global_id(i32 1) nounwind ; <i32> [#uses=1]
  %call8 = call i32 @get_global_size(i32 2) nounwind ; <i32> [#uses=1]
  %tmp9 = sub i32 %call8, 1                       ; <i32> [#uses=1]
  %call10 = call i32 @get_global_id(i32 2) nounwind ; <i32> [#uses=1]
  %tmp11 = mul i32 %tmp9, %call10                 ; <i32> [#uses=1]
  %tmp12 = add i32 %call7, %tmp11                 ; <i32> [#uses=1]
  %tmp13 = mul i32 %tmp6, %tmp12                  ; <i32> [#uses=1]
  %tmp14 = add i32 %call, %tmp13                  ; <i32> [#uses=1]
  %conv = zext i32 %tmp14 to i64                  ; <i64> [#uses=1]
  store i64 %conv, i64* %tmp1
  %tmp15 = load i64* %tmp1                        ; <i64> [#uses=1]
  %tmp16 = load i64* %tmp                         ; <i64> [#uses=1]
  %tmp17 = sdiv i64 %tmp15, %tmp16                ; <i64> [#uses=1]
  store i64 %tmp17, i64* %n
  %tmp18 = load i64* %tmp1                        ; <i64> [#uses=1]
  %tmp19 = load i64* %n                           ; <i64> [#uses=1]
  %tmp20 = load i64* %tmp                         ; <i64> [#uses=1]
  %tmp21 = mul i64 %tmp19, %tmp20                 ; <i64> [#uses=1]
  %tmp22 = sub i64 %tmp18, %tmp21                 ; <i64> [#uses=1]
  store i64 %tmp22, i64* %i
  %tmp23 = load i64* %n                           ; <i64> [#uses=1]
  %tmp24 = load i64* %Hnvar.addr                  ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp23, %tmp24              ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %cond.end101, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp25 = load i64* %i                           ; <i64> [#uses=1]
  %tmp26 = load i64* %ijmin.addr                  ; <i64> [#uses=1]
  %tmp27 = add i64 %tmp25, %tmp26                 ; <i64> [#uses=1]
  store i64 %tmp27, i64* %i
  %tmp28 = load i64* %i                           ; <i64> [#uses=1]
  %conv29 = trunc i64 %tmp28 to i32               ; <i32> [#uses=1]
  %tmp30 = load i64* %n                           ; <i64> [#uses=1]
  %conv31 = trunc i64 %tmp30 to i32               ; <i32> [#uses=1]
  %tmp32 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv33 = trunc i64 %tmp32 to i32               ; <i32> [#uses=1]
  %tmp34 = mul i32 %conv31, %conv33               ; <i32> [#uses=1]
  %tmp35 = add i32 %conv29, %tmp34                ; <i32> [#uses=1]
  %conv36 = zext i32 %tmp35 to i64                ; <i64> [#uses=1]
  store i64 %conv36, i64* %ihvwin
  %tmp37 = load i64* %i                           ; <i64> [#uses=1]
  %tmp38 = sub i64 %tmp37, 1                      ; <i64> [#uses=1]
  %conv39 = trunc i64 %tmp38 to i32               ; <i32> [#uses=1]
  %tmp40 = load i64* %n                           ; <i64> [#uses=1]
  %conv41 = trunc i64 %tmp40 to i32               ; <i32> [#uses=1]
  %tmp42 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv43 = trunc i64 %tmp42 to i32               ; <i32> [#uses=1]
  %tmp44 = mul i32 %conv41, %conv43               ; <i32> [#uses=1]
  %tmp45 = add i32 %conv39, %tmp44                ; <i32> [#uses=1]
  %conv46 = zext i32 %tmp45 to i64                ; <i64> [#uses=1]
  store i64 %conv46, i64* %ihvwimn
  %tmp47 = load i64* %i                           ; <i64> [#uses=1]
  %tmp48 = add i64 %tmp47, 1                      ; <i64> [#uses=1]
  %conv49 = trunc i64 %tmp48 to i32               ; <i32> [#uses=1]
  %tmp50 = load i64* %n                           ; <i64> [#uses=1]
  %conv51 = trunc i64 %tmp50 to i32               ; <i32> [#uses=1]
  %tmp52 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv53 = trunc i64 %tmp52 to i32               ; <i32> [#uses=1]
  %tmp54 = mul i32 %conv51, %conv53               ; <i32> [#uses=1]
  %tmp55 = add i32 %conv49, %tmp54                ; <i32> [#uses=1]
  %conv56 = zext i32 %tmp55 to i64                ; <i64> [#uses=1]
  store i64 %conv56, i64* %ihvwipn
  %tmp57 = load double* %slope_type.addr          ; <double> [#uses=1]
  %tmp58 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp59 = load i64* %ihvwin                      ; <i64> [#uses=1]
  %conv60 = trunc i64 %tmp59 to i32               ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp58, i32 %conv60 ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load double addrspace(1)* %arrayidx    ; <double> [#uses=1]
  %tmp62 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp63 = load i64* %ihvwimn                     ; <i64> [#uses=1]
  %conv64 = trunc i64 %tmp63 to i32               ; <i32> [#uses=1]
  %arrayidx65 = getelementptr double addrspace(1)* %tmp62, i32 %conv64 ; <double addrspace(1)*> [#uses=1]
  %tmp66 = load double addrspace(1)* %arrayidx65  ; <double> [#uses=1]
  %tmp67 = fsub double %tmp61, %tmp66             ; <double> [#uses=1]
  %tmp68 = fmul double %tmp57, %tmp67             ; <double> [#uses=1]
  store double %tmp68, double* %dlft
  %tmp69 = load double* %slope_type.addr          ; <double> [#uses=1]
  %tmp70 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp71 = load i64* %ihvwipn                     ; <i64> [#uses=1]
  %conv72 = trunc i64 %tmp71 to i32               ; <i32> [#uses=1]
  %arrayidx73 = getelementptr double addrspace(1)* %tmp70, i32 %conv72 ; <double addrspace(1)*> [#uses=1]
  %tmp74 = load double addrspace(1)* %arrayidx73  ; <double> [#uses=1]
  %tmp75 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp76 = load i64* %ihvwin                      ; <i64> [#uses=1]
  %conv77 = trunc i64 %tmp76 to i32               ; <i32> [#uses=1]
  %arrayidx78 = getelementptr double addrspace(1)* %tmp75, i32 %conv77 ; <double addrspace(1)*> [#uses=1]
  %tmp79 = load double addrspace(1)* %arrayidx78  ; <double> [#uses=1]
  %tmp80 = fsub double %tmp74, %tmp79             ; <double> [#uses=1]
  %tmp81 = fmul double %tmp69, %tmp80             ; <double> [#uses=1]
  store double %tmp81, double* %drgt
  %tmp82 = load double* %dlft                     ; <double> [#uses=1]
  %tmp83 = load double* %drgt                     ; <double> [#uses=1]
  %tmp84 = fadd double %tmp82, %tmp83             ; <double> [#uses=1]
  %tmp85 = fmul double 5.000000e-01, %tmp84       ; <double> [#uses=1]
  %tmp86 = load double* %slope_type.addr          ; <double> [#uses=1]
  %tmp87 = fdiv double %tmp85, %tmp86             ; <double> [#uses=1]
  store double %tmp87, double* %dcen
  %tmp88 = load double* %dcen                     ; <double> [#uses=1]
  %cmp89 = fcmp ogt double %tmp88, 0.000000e+00   ; <i1> [#uses=1]
  br i1 %cmp89, label %cond.then, label %cond.else

if.then:                                          ; preds = %entry
  br label %return

cond.then:                                        ; preds = %if.end
  br label %cond.end

cond.else:                                        ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.else, %cond.then
  %cond = phi double [ 1.000000e+00, %cond.then ], [ -1.000000e+00, %cond.else ] ; <double> [#uses=1]
  store double %cond, double* %dsgn
  %tmp90 = load double* %dlft                     ; <double> [#uses=1]
  %call91 = call double @__fabs_f64(double %tmp90) nounwind ; <double> [#uses=1]
  %tmp92 = load double* %drgt                     ; <double> [#uses=1]
  %call93 = call double @__fabs_f64(double %tmp92) nounwind ; <double> [#uses=1]
  %call94 = call double @__min_f64(double %call91, double %call93) nounwind ; <double> [#uses=1]
  store double %call94, double* %slop
  %tmp95 = load double* %dlft                     ; <double> [#uses=1]
  %tmp96 = load double* %drgt                     ; <double> [#uses=1]
  %tmp97 = fmul double %tmp95, %tmp96             ; <double> [#uses=1]
  %cmp98 = fcmp ole double %tmp97, 0.000000e+00   ; <i1> [#uses=1]
  br i1 %cmp98, label %cond.then99, label %cond.else100

cond.then99:                                      ; preds = %cond.end
  br label %cond.end101

cond.else100:                                     ; preds = %cond.end
  %tmp102 = load double* %slop                    ; <double> [#uses=1]
  br label %cond.end101

cond.end101:                                      ; preds = %cond.else100, %cond.then99
  %cond103 = phi double [ 0.000000e+00, %cond.then99 ], [ %tmp102, %cond.else100 ] ; <double> [#uses=1]
  store double %cond103, double* %dlim
  %tmp104 = load double addrspace(1)** %dq.addr   ; <double addrspace(1)*> [#uses=1]
  %tmp105 = load i64* %ihvwin                     ; <i64> [#uses=1]
  %conv106 = trunc i64 %tmp105 to i32             ; <i32> [#uses=1]
  %arrayidx107 = getelementptr double addrspace(1)* %tmp104, i32 %conv106 ; <double addrspace(1)*> [#uses=1]
  %tmp108 = load double* %dsgn                    ; <double> [#uses=1]
  %tmp109 = load double* %dlim                    ; <double> [#uses=1]
  %tmp110 = load double* %dcen                    ; <double> [#uses=1]
  %call111 = call double @__fabs_f64(double %tmp110) nounwind ; <double> [#uses=1]
  %call112 = call double @__min_f64(double %tmp109, double %call111) nounwind ; <double> [#uses=1]
  %tmp113 = fmul double %tmp108, %call112         ; <double> [#uses=1]
  store double %tmp113, double addrspace(1)* %arrayidx107
  br label %return
}

declare double @__min_f64(double, double) nounwind

define void @__OpenCL_Loop1KcuTrace_kernel(double addrspace(1)* %q, double addrspace(1)* %dq, double addrspace(1)* %c, double addrspace(1)* %qxm, double addrspace(1)* %qxp, double %dtdx, i64 %Hnxyt, i64 %imin, i64 %imax, double %zeror, double %zerol, double %project) nounwind {
entry:
  %q.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=5]
  %dq.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %c.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %qxm.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %qxp.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %dtdx.addr = alloca double, align 8             ; <double*> [#uses=7]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=5]
  %imin.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %imax.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %zeror.addr = alloca double, align 8            ; <double*> [#uses=4]
  %zerol.addr = alloca double, align 8            ; <double*> [#uses=4]
  %project.addr = alloca double, align 8          ; <double*> [#uses=7]
  %cc = alloca double, align 8                    ; <double*> [#uses=17]
  %csq = alloca double, align 8                   ; <double*> [#uses=4]
  %r = alloca double, align 8                     ; <double*> [#uses=9]
  %u = alloca double, align 8                     ; <double*> [#uses=15]
  %v = alloca double, align 8                     ; <double*> [#uses=3]
  %p = alloca double, align 8                     ; <double*> [#uses=3]
  %dr = alloca double, align 8                    ; <double*> [#uses=2]
  %du = alloca double, align 8                    ; <double*> [#uses=3]
  %dv = alloca double, align 8                    ; <double*> [#uses=2]
  %dp = alloca double, align 8                    ; <double*> [#uses=4]
  %alpham = alloca double, align 8                ; <double*> [#uses=3]
  %alphap = alloca double, align 8                ; <double*> [#uses=3]
  %alpha0r = alloca double, align 8               ; <double*> [#uses=3]
  %alpha0v = alloca double, align 8               ; <double*> [#uses=3]
  %spminus = alloca double, align 8               ; <double*> [#uses=6]
  %spplus = alloca double, align 8                ; <double*> [#uses=6]
  %spzero = alloca double, align 8                ; <double*> [#uses=8]
  %apright = alloca double, align 8               ; <double*> [#uses=4]
  %amright = alloca double, align 8               ; <double*> [#uses=4]
  %azrright = alloca double, align 8              ; <double*> [#uses=2]
  %azv1right = alloca double, align 8             ; <double*> [#uses=2]
  %apleft = alloca double, align 8                ; <double*> [#uses=4]
  %amleft = alloca double, align 8                ; <double*> [#uses=4]
  %azrleft = alloca double, align 8               ; <double*> [#uses=2]
  %azv1left = alloca double, align 8              ; <double*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=8]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=5]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=5]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=5]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=5]
  store double addrspace(1)* %q, double addrspace(1)** %q.addr
  store double addrspace(1)* %dq, double addrspace(1)** %dq.addr
  store double addrspace(1)* %c, double addrspace(1)** %c.addr
  store double addrspace(1)* %qxm, double addrspace(1)** %qxm.addr
  store double addrspace(1)* %qxp, double addrspace(1)** %qxp.addr
  store double %dtdx, double* %dtdx.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store i64 %imin, i64* %imin.addr
  store i64 %imax, i64* %imax.addr
  store double %zeror, double* %zeror.addr
  store double %zerol, double* %zerol.addr
  store double %project, double* %project.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %imin.addr                    ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end229, %if.then6, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %tmp3 = load i64* %imax.addr                    ; <i64> [#uses=1]
  %cmp4 = icmp sge i64 %tmp2, %tmp3               ; <i1> [#uses=1]
  br i1 %cmp4, label %if.then6, label %if.end5

if.then:                                          ; preds = %entry
  br label %return

if.end5:                                          ; preds = %if.end
  %tmp7 = load i64* %i                            ; <i64> [#uses=1]
  %conv8 = trunc i64 %tmp7 to i32                 ; <i32> [#uses=1]
  %tmp9 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv10 = trunc i64 %tmp9 to i32                ; <i32> [#uses=1]
  %tmp11 = mul i32 1, %conv10                     ; <i32> [#uses=1]
  %tmp12 = add i32 %conv8, %tmp11                 ; <i32> [#uses=1]
  store i32 %tmp12, i32* %idxIU
  %tmp13 = load i64* %i                           ; <i64> [#uses=1]
  %conv14 = trunc i64 %tmp13 to i32               ; <i32> [#uses=1]
  %tmp15 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %tmp17 = mul i32 2, %conv16                     ; <i32> [#uses=1]
  %tmp18 = add i32 %conv14, %tmp17                ; <i32> [#uses=1]
  store i32 %tmp18, i32* %idxIV
  %tmp19 = load i64* %i                           ; <i64> [#uses=1]
  %conv20 = trunc i64 %tmp19 to i32               ; <i32> [#uses=1]
  %tmp21 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv22 = trunc i64 %tmp21 to i32               ; <i32> [#uses=1]
  %tmp23 = mul i32 3, %conv22                     ; <i32> [#uses=1]
  %tmp24 = add i32 %conv20, %tmp23                ; <i32> [#uses=1]
  store i32 %tmp24, i32* %idxIP
  %tmp25 = load i64* %i                           ; <i64> [#uses=1]
  %conv26 = trunc i64 %tmp25 to i32               ; <i32> [#uses=1]
  %tmp27 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv28 = trunc i64 %tmp27 to i32               ; <i32> [#uses=1]
  %tmp29 = mul i32 0, %conv28                     ; <i32> [#uses=1]
  %tmp30 = add i32 %conv26, %tmp29                ; <i32> [#uses=1]
  store i32 %tmp30, i32* %idxID
  %tmp31 = load double addrspace(1)** %c.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp32 = load i64* %i                           ; <i64> [#uses=1]
  %conv33 = trunc i64 %tmp32 to i32               ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp31, i32 %conv33 ; <double addrspace(1)*> [#uses=1]
  %tmp34 = load double addrspace(1)* %arrayidx    ; <double> [#uses=1]
  store double %tmp34, double* %cc
  %tmp35 = load double* %cc                       ; <double> [#uses=1]
  %tmp36 = load double* %cc                       ; <double> [#uses=1]
  %tmp37 = fmul double %tmp35, %tmp36             ; <double> [#uses=1]
  store double %tmp37, double* %csq
  %tmp38 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp39 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx40 = getelementptr double addrspace(1)* %tmp38, i32 %tmp39 ; <double addrspace(1)*> [#uses=1]
  %tmp41 = load double addrspace(1)* %arrayidx40  ; <double> [#uses=1]
  store double %tmp41, double* %r
  %tmp42 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp43 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx44 = getelementptr double addrspace(1)* %tmp42, i32 %tmp43 ; <double addrspace(1)*> [#uses=1]
  %tmp45 = load double addrspace(1)* %arrayidx44  ; <double> [#uses=1]
  store double %tmp45, double* %u
  %tmp46 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp47 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx48 = getelementptr double addrspace(1)* %tmp46, i32 %tmp47 ; <double addrspace(1)*> [#uses=1]
  %tmp49 = load double addrspace(1)* %arrayidx48  ; <double> [#uses=1]
  store double %tmp49, double* %v
  %tmp50 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp51 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx52 = getelementptr double addrspace(1)* %tmp50, i32 %tmp51 ; <double addrspace(1)*> [#uses=1]
  %tmp53 = load double addrspace(1)* %arrayidx52  ; <double> [#uses=1]
  store double %tmp53, double* %p
  %tmp54 = load double addrspace(1)** %dq.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp55 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx56 = getelementptr double addrspace(1)* %tmp54, i32 %tmp55 ; <double addrspace(1)*> [#uses=1]
  %tmp57 = load double addrspace(1)* %arrayidx56  ; <double> [#uses=1]
  store double %tmp57, double* %dr
  %tmp58 = load double addrspace(1)** %dq.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp59 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx60 = getelementptr double addrspace(1)* %tmp58, i32 %tmp59 ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load double addrspace(1)* %arrayidx60  ; <double> [#uses=1]
  store double %tmp61, double* %du
  %tmp62 = load double addrspace(1)** %dq.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp63 = load i32* %idxIV                       ; <i32> [#uses=1]
  %arrayidx64 = getelementptr double addrspace(1)* %tmp62, i32 %tmp63 ; <double addrspace(1)*> [#uses=1]
  %tmp65 = load double addrspace(1)* %arrayidx64  ; <double> [#uses=1]
  store double %tmp65, double* %dv
  %tmp66 = load double addrspace(1)** %dq.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp67 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx68 = getelementptr double addrspace(1)* %tmp66, i32 %tmp67 ; <double addrspace(1)*> [#uses=1]
  %tmp69 = load double addrspace(1)* %arrayidx68  ; <double> [#uses=1]
  store double %tmp69, double* %dp
  %tmp70 = load double* %dp                       ; <double> [#uses=1]
  %tmp71 = load double* %r                        ; <double> [#uses=1]
  %tmp72 = load double* %cc                       ; <double> [#uses=1]
  %tmp73 = fmul double %tmp71, %tmp72             ; <double> [#uses=1]
  %tmp74 = fdiv double %tmp70, %tmp73             ; <double> [#uses=1]
  %tmp75 = load double* %du                       ; <double> [#uses=1]
  %tmp76 = fsub double %tmp74, %tmp75             ; <double> [#uses=1]
  %tmp77 = fmul double 5.000000e-01, %tmp76       ; <double> [#uses=1]
  %tmp78 = load double* %r                        ; <double> [#uses=1]
  %tmp79 = fmul double %tmp77, %tmp78             ; <double> [#uses=1]
  %tmp80 = load double* %cc                       ; <double> [#uses=1]
  %tmp81 = fdiv double %tmp79, %tmp80             ; <double> [#uses=1]
  store double %tmp81, double* %alpham
  %tmp82 = load double* %dp                       ; <double> [#uses=1]
  %tmp83 = load double* %r                        ; <double> [#uses=1]
  %tmp84 = load double* %cc                       ; <double> [#uses=1]
  %tmp85 = fmul double %tmp83, %tmp84             ; <double> [#uses=1]
  %tmp86 = fdiv double %tmp82, %tmp85             ; <double> [#uses=1]
  %tmp87 = load double* %du                       ; <double> [#uses=1]
  %tmp88 = fadd double %tmp86, %tmp87             ; <double> [#uses=1]
  %tmp89 = fmul double 5.000000e-01, %tmp88       ; <double> [#uses=1]
  %tmp90 = load double* %r                        ; <double> [#uses=1]
  %tmp91 = fmul double %tmp89, %tmp90             ; <double> [#uses=1]
  %tmp92 = load double* %cc                       ; <double> [#uses=1]
  %tmp93 = fdiv double %tmp91, %tmp92             ; <double> [#uses=1]
  store double %tmp93, double* %alphap
  %tmp94 = load double* %dr                       ; <double> [#uses=1]
  %tmp95 = load double* %dp                       ; <double> [#uses=1]
  %tmp96 = load double* %csq                      ; <double> [#uses=1]
  %tmp97 = fdiv double %tmp95, %tmp96             ; <double> [#uses=1]
  %tmp98 = fsub double %tmp94, %tmp97             ; <double> [#uses=1]
  store double %tmp98, double* %alpha0r
  %tmp99 = load double* %dv                       ; <double> [#uses=1]
  store double %tmp99, double* %alpha0v
  %tmp100 = load double* %u                       ; <double> [#uses=1]
  %tmp101 = load double* %cc                      ; <double> [#uses=1]
  %tmp102 = fsub double %tmp100, %tmp101          ; <double> [#uses=1]
  %tmp103 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp104 = fmul double %tmp102, %tmp103          ; <double> [#uses=1]
  %tmp105 = fadd double %tmp104, 1.000000e+00     ; <double> [#uses=1]
  store double %tmp105, double* %spminus
  %tmp106 = load double* %u                       ; <double> [#uses=1]
  %tmp107 = load double* %cc                      ; <double> [#uses=1]
  %tmp108 = fadd double %tmp106, %tmp107          ; <double> [#uses=1]
  %tmp109 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp110 = fmul double %tmp108, %tmp109          ; <double> [#uses=1]
  %tmp111 = fadd double %tmp110, 1.000000e+00     ; <double> [#uses=1]
  store double %tmp111, double* %spplus
  %tmp112 = load double* %u                       ; <double> [#uses=1]
  %tmp113 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp114 = fmul double %tmp112, %tmp113          ; <double> [#uses=1]
  %tmp115 = fadd double %tmp114, 1.000000e+00     ; <double> [#uses=1]
  store double %tmp115, double* %spzero
  %tmp116 = load double* %u                       ; <double> [#uses=1]
  %tmp117 = load double* %cc                      ; <double> [#uses=1]
  %tmp118 = fsub double %tmp116, %tmp117          ; <double> [#uses=1]
  %tmp119 = load double* %zeror.addr              ; <double> [#uses=1]
  %cmp120 = fcmp oge double %tmp118, %tmp119      ; <i1> [#uses=1]
  br i1 %cmp120, label %if.then122, label %if.end121

if.then6:                                         ; preds = %if.end
  br label %return

if.end121:                                        ; preds = %if.then122, %if.end5
  %tmp124 = load double* %u                       ; <double> [#uses=1]
  %tmp125 = load double* %cc                      ; <double> [#uses=1]
  %tmp126 = fadd double %tmp124, %tmp125          ; <double> [#uses=1]
  %tmp127 = load double* %zeror.addr              ; <double> [#uses=1]
  %cmp128 = fcmp oge double %tmp126, %tmp127      ; <i1> [#uses=1]
  br i1 %cmp128, label %if.then130, label %if.end129

if.then122:                                       ; preds = %if.end5
  %tmp123 = load double* %project.addr            ; <double> [#uses=1]
  store double %tmp123, double* %spminus
  br label %if.end121

if.end129:                                        ; preds = %if.then130, %if.end121
  %tmp132 = load double* %u                       ; <double> [#uses=1]
  %tmp133 = load double* %zeror.addr              ; <double> [#uses=1]
  %cmp134 = fcmp oge double %tmp132, %tmp133      ; <i1> [#uses=1]
  br i1 %cmp134, label %if.then136, label %if.end135

if.then130:                                       ; preds = %if.end121
  %tmp131 = load double* %project.addr            ; <double> [#uses=1]
  store double %tmp131, double* %spplus
  br label %if.end129

if.end135:                                        ; preds = %if.then136, %if.end129
  %tmp138 = load double* %spplus                  ; <double> [#uses=1]
  %tmp139 = fmul double -5.000000e-01, %tmp138    ; <double> [#uses=1]
  %tmp140 = load double* %alphap                  ; <double> [#uses=1]
  %tmp141 = fmul double %tmp139, %tmp140          ; <double> [#uses=1]
  store double %tmp141, double* %apright
  %tmp142 = load double* %spminus                 ; <double> [#uses=1]
  %tmp143 = fmul double -5.000000e-01, %tmp142    ; <double> [#uses=1]
  %tmp144 = load double* %alpham                  ; <double> [#uses=1]
  %tmp145 = fmul double %tmp143, %tmp144          ; <double> [#uses=1]
  store double %tmp145, double* %amright
  %tmp146 = load double* %spzero                  ; <double> [#uses=1]
  %tmp147 = fmul double -5.000000e-01, %tmp146    ; <double> [#uses=1]
  %tmp148 = load double* %alpha0r                 ; <double> [#uses=1]
  %tmp149 = fmul double %tmp147, %tmp148          ; <double> [#uses=1]
  store double %tmp149, double* %azrright
  %tmp150 = load double* %spzero                  ; <double> [#uses=1]
  %tmp151 = fmul double -5.000000e-01, %tmp150    ; <double> [#uses=1]
  %tmp152 = load double* %alpha0v                 ; <double> [#uses=1]
  %tmp153 = fmul double %tmp151, %tmp152          ; <double> [#uses=1]
  store double %tmp153, double* %azv1right
  %tmp154 = load double addrspace(1)** %qxp.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp155 = load i32* %idxID                      ; <i32> [#uses=1]
  %arrayidx156 = getelementptr double addrspace(1)* %tmp154, i32 %tmp155 ; <double addrspace(1)*> [#uses=1]
  %tmp157 = load double* %r                       ; <double> [#uses=1]
  %tmp158 = load double* %apright                 ; <double> [#uses=1]
  %tmp159 = load double* %amright                 ; <double> [#uses=1]
  %tmp160 = fadd double %tmp158, %tmp159          ; <double> [#uses=1]
  %tmp161 = load double* %azrright                ; <double> [#uses=1]
  %tmp162 = fadd double %tmp160, %tmp161          ; <double> [#uses=1]
  %tmp163 = fadd double %tmp157, %tmp162          ; <double> [#uses=1]
  store double %tmp163, double addrspace(1)* %arrayidx156
  %tmp164 = load double addrspace(1)** %qxp.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp165 = load i32* %idxIU                      ; <i32> [#uses=1]
  %arrayidx166 = getelementptr double addrspace(1)* %tmp164, i32 %tmp165 ; <double addrspace(1)*> [#uses=1]
  %tmp167 = load double* %u                       ; <double> [#uses=1]
  %tmp168 = load double* %apright                 ; <double> [#uses=1]
  %tmp169 = load double* %amright                 ; <double> [#uses=1]
  %tmp170 = fsub double %tmp168, %tmp169          ; <double> [#uses=1]
  %tmp171 = load double* %cc                      ; <double> [#uses=1]
  %tmp172 = fmul double %tmp170, %tmp171          ; <double> [#uses=1]
  %tmp173 = load double* %r                       ; <double> [#uses=1]
  %tmp174 = fdiv double %tmp172, %tmp173          ; <double> [#uses=1]
  %tmp175 = fadd double %tmp167, %tmp174          ; <double> [#uses=1]
  store double %tmp175, double addrspace(1)* %arrayidx166
  %tmp176 = load double addrspace(1)** %qxp.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp177 = load i32* %idxIV                      ; <i32> [#uses=1]
  %arrayidx178 = getelementptr double addrspace(1)* %tmp176, i32 %tmp177 ; <double addrspace(1)*> [#uses=1]
  %tmp179 = load double* %v                       ; <double> [#uses=1]
  %tmp180 = load double* %azv1right               ; <double> [#uses=1]
  %tmp181 = fadd double %tmp179, %tmp180          ; <double> [#uses=1]
  store double %tmp181, double addrspace(1)* %arrayidx178
  %tmp182 = load double addrspace(1)** %qxp.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp183 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx184 = getelementptr double addrspace(1)* %tmp182, i32 %tmp183 ; <double addrspace(1)*> [#uses=1]
  %tmp185 = load double* %p                       ; <double> [#uses=1]
  %tmp186 = load double* %apright                 ; <double> [#uses=1]
  %tmp187 = load double* %amright                 ; <double> [#uses=1]
  %tmp188 = fadd double %tmp186, %tmp187          ; <double> [#uses=1]
  %tmp189 = load double* %csq                     ; <double> [#uses=1]
  %tmp190 = fmul double %tmp188, %tmp189          ; <double> [#uses=1]
  %tmp191 = fadd double %tmp185, %tmp190          ; <double> [#uses=1]
  store double %tmp191, double addrspace(1)* %arrayidx184
  %tmp192 = load double* %u                       ; <double> [#uses=1]
  %tmp193 = load double* %cc                      ; <double> [#uses=1]
  %tmp194 = fsub double %tmp192, %tmp193          ; <double> [#uses=1]
  %tmp195 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp196 = fmul double %tmp194, %tmp195          ; <double> [#uses=1]
  %tmp197 = fsub double %tmp196, 1.000000e+00     ; <double> [#uses=1]
  store double %tmp197, double* %spminus
  %tmp198 = load double* %u                       ; <double> [#uses=1]
  %tmp199 = load double* %cc                      ; <double> [#uses=1]
  %tmp200 = fadd double %tmp198, %tmp199          ; <double> [#uses=1]
  %tmp201 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp202 = fmul double %tmp200, %tmp201          ; <double> [#uses=1]
  %tmp203 = fsub double %tmp202, 1.000000e+00     ; <double> [#uses=1]
  store double %tmp203, double* %spplus
  %tmp204 = load double* %u                       ; <double> [#uses=1]
  %tmp205 = load double* %dtdx.addr               ; <double> [#uses=1]
  %tmp206 = fmul double %tmp204, %tmp205          ; <double> [#uses=1]
  %tmp207 = fsub double %tmp206, 1.000000e+00     ; <double> [#uses=1]
  store double %tmp207, double* %spzero
  %tmp208 = load double* %u                       ; <double> [#uses=1]
  %tmp209 = load double* %cc                      ; <double> [#uses=1]
  %tmp210 = fsub double %tmp208, %tmp209          ; <double> [#uses=1]
  %tmp211 = load double* %zerol.addr              ; <double> [#uses=1]
  %cmp212 = fcmp ole double %tmp210, %tmp211      ; <i1> [#uses=1]
  br i1 %cmp212, label %if.then214, label %if.end213

if.then136:                                       ; preds = %if.end129
  %tmp137 = load double* %project.addr            ; <double> [#uses=1]
  store double %tmp137, double* %spzero
  br label %if.end135

if.end213:                                        ; preds = %if.then214, %if.end135
  %tmp217 = load double* %u                       ; <double> [#uses=1]
  %tmp218 = load double* %cc                      ; <double> [#uses=1]
  %tmp219 = fadd double %tmp217, %tmp218          ; <double> [#uses=1]
  %tmp220 = load double* %zerol.addr              ; <double> [#uses=1]
  %cmp221 = fcmp ole double %tmp219, %tmp220      ; <i1> [#uses=1]
  br i1 %cmp221, label %if.then223, label %if.end222

if.then214:                                       ; preds = %if.end135
  %tmp215 = load double* %project.addr            ; <double> [#uses=1]
  %tmp216 = fsub double -0.000000e+00, %tmp215    ; <double> [#uses=1]
  store double %tmp216, double* %spminus
  br label %if.end213

if.end222:                                        ; preds = %if.then223, %if.end213
  %tmp226 = load double* %u                       ; <double> [#uses=1]
  %tmp227 = load double* %zerol.addr              ; <double> [#uses=1]
  %cmp228 = fcmp ole double %tmp226, %tmp227      ; <i1> [#uses=1]
  br i1 %cmp228, label %if.then230, label %if.end229

if.then223:                                       ; preds = %if.end213
  %tmp224 = load double* %project.addr            ; <double> [#uses=1]
  %tmp225 = fsub double -0.000000e+00, %tmp224    ; <double> [#uses=1]
  store double %tmp225, double* %spplus
  br label %if.end222

if.end229:                                        ; preds = %if.then230, %if.end222
  %tmp233 = load double* %spplus                  ; <double> [#uses=1]
  %tmp234 = fmul double -5.000000e-01, %tmp233    ; <double> [#uses=1]
  %tmp235 = load double* %alphap                  ; <double> [#uses=1]
  %tmp236 = fmul double %tmp234, %tmp235          ; <double> [#uses=1]
  store double %tmp236, double* %apleft
  %tmp237 = load double* %spminus                 ; <double> [#uses=1]
  %tmp238 = fmul double -5.000000e-01, %tmp237    ; <double> [#uses=1]
  %tmp239 = load double* %alpham                  ; <double> [#uses=1]
  %tmp240 = fmul double %tmp238, %tmp239          ; <double> [#uses=1]
  store double %tmp240, double* %amleft
  %tmp241 = load double* %spzero                  ; <double> [#uses=1]
  %tmp242 = fmul double -5.000000e-01, %tmp241    ; <double> [#uses=1]
  %tmp243 = load double* %alpha0r                 ; <double> [#uses=1]
  %tmp244 = fmul double %tmp242, %tmp243          ; <double> [#uses=1]
  store double %tmp244, double* %azrleft
  %tmp245 = load double* %spzero                  ; <double> [#uses=1]
  %tmp246 = fmul double -5.000000e-01, %tmp245    ; <double> [#uses=1]
  %tmp247 = load double* %alpha0v                 ; <double> [#uses=1]
  %tmp248 = fmul double %tmp246, %tmp247          ; <double> [#uses=1]
  store double %tmp248, double* %azv1left
  %tmp249 = load double addrspace(1)** %qxm.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp250 = load i32* %idxID                      ; <i32> [#uses=1]
  %arrayidx251 = getelementptr double addrspace(1)* %tmp249, i32 %tmp250 ; <double addrspace(1)*> [#uses=1]
  %tmp252 = load double* %r                       ; <double> [#uses=1]
  %tmp253 = load double* %apleft                  ; <double> [#uses=1]
  %tmp254 = load double* %amleft                  ; <double> [#uses=1]
  %tmp255 = fadd double %tmp253, %tmp254          ; <double> [#uses=1]
  %tmp256 = load double* %azrleft                 ; <double> [#uses=1]
  %tmp257 = fadd double %tmp255, %tmp256          ; <double> [#uses=1]
  %tmp258 = fadd double %tmp252, %tmp257          ; <double> [#uses=1]
  store double %tmp258, double addrspace(1)* %arrayidx251
  %tmp259 = load double addrspace(1)** %qxm.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp260 = load i32* %idxIU                      ; <i32> [#uses=1]
  %arrayidx261 = getelementptr double addrspace(1)* %tmp259, i32 %tmp260 ; <double addrspace(1)*> [#uses=1]
  %tmp262 = load double* %u                       ; <double> [#uses=1]
  %tmp263 = load double* %apleft                  ; <double> [#uses=1]
  %tmp264 = load double* %amleft                  ; <double> [#uses=1]
  %tmp265 = fsub double %tmp263, %tmp264          ; <double> [#uses=1]
  %tmp266 = load double* %cc                      ; <double> [#uses=1]
  %tmp267 = fmul double %tmp265, %tmp266          ; <double> [#uses=1]
  %tmp268 = load double* %r                       ; <double> [#uses=1]
  %tmp269 = fdiv double %tmp267, %tmp268          ; <double> [#uses=1]
  %tmp270 = fadd double %tmp262, %tmp269          ; <double> [#uses=1]
  store double %tmp270, double addrspace(1)* %arrayidx261
  %tmp271 = load double addrspace(1)** %qxm.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp272 = load i32* %idxIV                      ; <i32> [#uses=1]
  %arrayidx273 = getelementptr double addrspace(1)* %tmp271, i32 %tmp272 ; <double addrspace(1)*> [#uses=1]
  %tmp274 = load double* %v                       ; <double> [#uses=1]
  %tmp275 = load double* %azv1left                ; <double> [#uses=1]
  %tmp276 = fadd double %tmp274, %tmp275          ; <double> [#uses=1]
  store double %tmp276, double addrspace(1)* %arrayidx273
  %tmp277 = load double addrspace(1)** %qxm.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp278 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx279 = getelementptr double addrspace(1)* %tmp277, i32 %tmp278 ; <double addrspace(1)*> [#uses=1]
  %tmp280 = load double* %p                       ; <double> [#uses=1]
  %tmp281 = load double* %apleft                  ; <double> [#uses=1]
  %tmp282 = load double* %amleft                  ; <double> [#uses=1]
  %tmp283 = fadd double %tmp281, %tmp282          ; <double> [#uses=1]
  %tmp284 = load double* %csq                     ; <double> [#uses=1]
  %tmp285 = fmul double %tmp283, %tmp284          ; <double> [#uses=1]
  %tmp286 = fadd double %tmp280, %tmp285          ; <double> [#uses=1]
  store double %tmp286, double addrspace(1)* %arrayidx279
  br label %return

if.then230:                                       ; preds = %if.end222
  %tmp231 = load double* %project.addr            ; <double> [#uses=1]
  %tmp232 = fsub double -0.000000e+00, %tmp231    ; <double> [#uses=1]
  store double %tmp232, double* %spzero
  br label %if.end229
}

define void @__OpenCL_Loop2KcuTrace_kernel(double addrspace(1)* %q, double addrspace(1)* %dq, double addrspace(1)* %qxm, double addrspace(1)* %qxp, double %dtdx, i64 %Hnvar, i64 %Hnxyt, i64 %imin, i64 %imax, double %zeror, double %zerol, double %project) nounwind {
entry:
  %q.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=3]
  %dq.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %qxm.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %qxp.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %dtdx.addr = alloca double, align 8             ; <double*> [#uses=3]
  %Hnvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=6]
  %imin.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %imax.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %zeror.addr = alloca double, align 8            ; <double*> [#uses=2]
  %zerol.addr = alloca double, align 8            ; <double*> [#uses=2]
  %project.addr = alloca double, align 8          ; <double*> [#uses=3]
  %IN = alloca i64, align 8                       ; <i64*> [#uses=5]
  %u = alloca double, align 8                     ; <double*> [#uses=5]
  %a = alloca double, align 8                     ; <double*> [#uses=3]
  %da = alloca double, align 8                    ; <double*> [#uses=3]
  %spzero = alloca double, align 8                ; <double*> [#uses=6]
  %acmpright = alloca double, align 8             ; <double*> [#uses=2]
  %acmpleft = alloca double, align 8              ; <double*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=8]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=2]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=1]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=1]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=1]
  %idxIN = alloca i32, align 4                    ; <i32*> [#uses=5]
  store double addrspace(1)* %q, double addrspace(1)** %q.addr
  store double addrspace(1)* %dq, double addrspace(1)** %dq.addr
  store double addrspace(1)* %qxm, double addrspace(1)** %qxm.addr
  store double addrspace(1)* %qxp, double addrspace(1)** %qxp.addr
  store double %dtdx, double* %dtdx.addr
  store i64 %Hnvar, i64* %Hnvar.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store i64 %imin, i64* %imin.addr
  store i64 %imax, i64* %imax.addr
  store double %zeror, double* %zeror.addr
  store double %zerol, double* %zerol.addr
  store double %project, double* %project.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp1 = load i64* %imin.addr                    ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp, %tmp1                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %for.exit, %if.then6, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp2 = load i64* %i                            ; <i64> [#uses=1]
  %tmp3 = load i64* %imax.addr                    ; <i64> [#uses=1]
  %cmp4 = icmp sge i64 %tmp2, %tmp3               ; <i1> [#uses=1]
  br i1 %cmp4, label %if.then6, label %if.end5

if.then:                                          ; preds = %entry
  br label %return

if.end5:                                          ; preds = %if.end
  %tmp7 = load i64* %i                            ; <i64> [#uses=1]
  %conv8 = trunc i64 %tmp7 to i32                 ; <i32> [#uses=1]
  %tmp9 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv10 = trunc i64 %tmp9 to i32                ; <i32> [#uses=1]
  %tmp11 = mul i32 1, %conv10                     ; <i32> [#uses=1]
  %tmp12 = add i32 %conv8, %tmp11                 ; <i32> [#uses=1]
  store i32 %tmp12, i32* %idxIU
  %tmp13 = load i64* %i                           ; <i64> [#uses=1]
  %conv14 = trunc i64 %tmp13 to i32               ; <i32> [#uses=1]
  %tmp15 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %tmp17 = mul i32 2, %conv16                     ; <i32> [#uses=1]
  %tmp18 = add i32 %conv14, %tmp17                ; <i32> [#uses=1]
  store i32 %tmp18, i32* %idxIV
  %tmp19 = load i64* %i                           ; <i64> [#uses=1]
  %conv20 = trunc i64 %tmp19 to i32               ; <i32> [#uses=1]
  %tmp21 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv22 = trunc i64 %tmp21 to i32               ; <i32> [#uses=1]
  %tmp23 = mul i32 3, %conv22                     ; <i32> [#uses=1]
  %tmp24 = add i32 %conv20, %tmp23                ; <i32> [#uses=1]
  store i32 %tmp24, i32* %idxIP
  %tmp25 = load i64* %i                           ; <i64> [#uses=1]
  %conv26 = trunc i64 %tmp25 to i32               ; <i32> [#uses=1]
  %tmp27 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv28 = trunc i64 %tmp27 to i32               ; <i32> [#uses=1]
  %tmp29 = mul i32 0, %conv28                     ; <i32> [#uses=1]
  %tmp30 = add i32 %conv26, %tmp29                ; <i32> [#uses=1]
  store i32 %tmp30, i32* %idxID
  store i64 4, i64* %IN
  br label %for.cond

if.then6:                                         ; preds = %if.end
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end5
  %tmp31 = load i64* %IN                          ; <i64> [#uses=1]
  %tmp32 = load i64* %Hnvar.addr                  ; <i64> [#uses=1]
  %cmp33 = icmp slt i64 %tmp31, %tmp32            ; <i1> [#uses=1]
  br i1 %cmp33, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %return

for.body:                                         ; preds = %for.cond
  %tmp34 = load i64* %i                           ; <i64> [#uses=1]
  %conv35 = trunc i64 %tmp34 to i32               ; <i32> [#uses=1]
  %tmp36 = load i64* %IN                          ; <i64> [#uses=1]
  %conv37 = trunc i64 %tmp36 to i32               ; <i32> [#uses=1]
  %tmp38 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv39 = trunc i64 %tmp38 to i32               ; <i32> [#uses=1]
  %tmp40 = mul i32 %conv37, %conv39               ; <i32> [#uses=1]
  %tmp41 = add i32 %conv35, %tmp40                ; <i32> [#uses=1]
  store i32 %tmp41, i32* %idxIN
  %tmp42 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp43 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp42, i32 %tmp43 ; <double addrspace(1)*> [#uses=1]
  %tmp44 = load double addrspace(1)* %arrayidx    ; <double> [#uses=1]
  store double %tmp44, double* %u
  %tmp45 = load double addrspace(1)** %q.addr     ; <double addrspace(1)*> [#uses=1]
  %tmp46 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx47 = getelementptr double addrspace(1)* %tmp45, i32 %tmp46 ; <double addrspace(1)*> [#uses=1]
  %tmp48 = load double addrspace(1)* %arrayidx47  ; <double> [#uses=1]
  store double %tmp48, double* %a
  %tmp49 = load double addrspace(1)** %dq.addr    ; <double addrspace(1)*> [#uses=1]
  %tmp50 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx51 = getelementptr double addrspace(1)* %tmp49, i32 %tmp50 ; <double addrspace(1)*> [#uses=1]
  %tmp52 = load double addrspace(1)* %arrayidx51  ; <double> [#uses=1]
  store double %tmp52, double* %da
  %tmp53 = load double* %u                        ; <double> [#uses=1]
  %tmp54 = load double* %dtdx.addr                ; <double> [#uses=1]
  %tmp55 = fmul double %tmp53, %tmp54             ; <double> [#uses=1]
  %tmp56 = fadd double %tmp55, 1.000000e+00       ; <double> [#uses=1]
  store double %tmp56, double* %spzero
  %tmp57 = load double* %u                        ; <double> [#uses=1]
  %tmp58 = load double* %zeror.addr               ; <double> [#uses=1]
  %cmp59 = fcmp oge double %tmp57, %tmp58         ; <i1> [#uses=1]
  br i1 %cmp59, label %if.then61, label %if.end60

for.inc:                                          ; preds = %if.end80
  %tmp94 = load i64* %IN                          ; <i64> [#uses=1]
  %tmp95 = add i64 %tmp94, 1                      ; <i64> [#uses=1]
  store i64 %tmp95, i64* %IN
  br label %for.cond

if.end60:                                         ; preds = %if.then61, %for.body
  %tmp63 = load double* %spzero                   ; <double> [#uses=1]
  %tmp64 = fmul double -5.000000e-01, %tmp63      ; <double> [#uses=1]
  %tmp65 = load double* %da                       ; <double> [#uses=1]
  %tmp66 = fmul double %tmp64, %tmp65             ; <double> [#uses=1]
  store double %tmp66, double* %acmpright
  %tmp67 = load double addrspace(1)** %qxp.addr   ; <double addrspace(1)*> [#uses=1]
  %tmp68 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx69 = getelementptr double addrspace(1)* %tmp67, i32 %tmp68 ; <double addrspace(1)*> [#uses=1]
  %tmp70 = load double* %a                        ; <double> [#uses=1]
  %tmp71 = load double* %acmpright                ; <double> [#uses=1]
  %tmp72 = fadd double %tmp70, %tmp71             ; <double> [#uses=1]
  store double %tmp72, double addrspace(1)* %arrayidx69
  %tmp73 = load double* %u                        ; <double> [#uses=1]
  %tmp74 = load double* %dtdx.addr                ; <double> [#uses=1]
  %tmp75 = fmul double %tmp73, %tmp74             ; <double> [#uses=1]
  %tmp76 = fsub double %tmp75, 1.000000e+00       ; <double> [#uses=1]
  store double %tmp76, double* %spzero
  %tmp77 = load double* %u                        ; <double> [#uses=1]
  %tmp78 = load double* %zerol.addr               ; <double> [#uses=1]
  %cmp79 = fcmp ole double %tmp77, %tmp78         ; <i1> [#uses=1]
  br i1 %cmp79, label %if.then81, label %if.end80

if.then61:                                        ; preds = %for.body
  %tmp62 = load double* %project.addr             ; <double> [#uses=1]
  store double %tmp62, double* %spzero
  br label %if.end60

if.end80:                                         ; preds = %if.then81, %if.end60
  %tmp84 = load double* %spzero                   ; <double> [#uses=1]
  %tmp85 = fmul double -5.000000e-01, %tmp84      ; <double> [#uses=1]
  %tmp86 = load double* %da                       ; <double> [#uses=1]
  %tmp87 = fmul double %tmp85, %tmp86             ; <double> [#uses=1]
  store double %tmp87, double* %acmpleft
  %tmp88 = load double addrspace(1)** %qxm.addr   ; <double addrspace(1)*> [#uses=1]
  %tmp89 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx90 = getelementptr double addrspace(1)* %tmp88, i32 %tmp89 ; <double addrspace(1)*> [#uses=1]
  %tmp91 = load double* %a                        ; <double> [#uses=1]
  %tmp92 = load double* %acmpleft                 ; <double> [#uses=1]
  %tmp93 = fadd double %tmp91, %tmp92             ; <double> [#uses=1]
  store double %tmp93, double addrspace(1)* %arrayidx90
  br label %for.inc

if.then81:                                        ; preds = %if.end60
  %tmp82 = load double* %project.addr             ; <double> [#uses=1]
  %tmp83 = fsub double -0.000000e+00, %tmp82      ; <double> [#uses=1]
  store double %tmp83, double* %spzero
  br label %if.end80
}

define void @__OpenCL_LoopKredMaxDble_kernel(double addrspace(1)* %src, double addrspace(1)* %res, i64 %nb) nounwind {
entry:
  %src.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=3]
  %res.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %nb.addr = alloca i64, align 8                  ; <i64*> [#uses=2]
  %sdata = alloca double addrspace(3)*, align 4   ; <double addrspace(3)**> [#uses=31]
  %blockSize = alloca i64, align 8                ; <i64*> [#uses=10]
  %tidL = alloca i64, align 8                     ; <i64*> [#uses=35]
  %myblock = alloca i64, align 8                  ; <i64*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=3]
  store double addrspace(1)* %src, double addrspace(1)** %src.addr
  store double addrspace(1)* %res, double addrspace(1)** %res.addr
  store i64 %nb, i64* %nb.addr
  store double addrspace(3)* getelementptr inbounds ([512 x double] addrspace(3)* @LoopKredMaxDble_cllocal_sdata, i32 0, i32 0), double addrspace(3)** %sdata
  %call = call i32 @get_local_size(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %blockSize
  %call1 = call i32 @get_local_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv2 = zext i32 %call1 to i64                 ; <i64> [#uses=1]
  store i64 %conv2, i64* %tidL
  %call3 = call i32 @get_group_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv4 = zext i32 %call3 to i64                 ; <i64> [#uses=1]
  store i64 %conv4, i64* %myblock
  %call5 = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv6 = zext i32 %call5 to i64                 ; <i64> [#uses=1]
  store i64 %conv6, i64* %i
  %tmp = load double addrspace(3)** %sdata        ; <double addrspace(3)*> [#uses=1]
  %tmp7 = load i64* %tidL                         ; <i64> [#uses=1]
  %conv8 = trunc i64 %tmp7 to i32                 ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(3)* %tmp, i32 %conv8 ; <double addrspace(3)*> [#uses=1]
  %tmp9 = load double addrspace(1)** %src.addr    ; <double addrspace(1)*> [#uses=1]
  %arrayidx10 = getelementptr double addrspace(1)* %tmp9, i32 0 ; <double addrspace(1)*> [#uses=1]
  %tmp11 = load double addrspace(1)* %arrayidx10  ; <double> [#uses=1]
  store double %tmp11, double addrspace(3)* %arrayidx
  call void @barrier(i32 0, i32 1) nounwind
  %tmp12 = load i64* %i                           ; <i64> [#uses=1]
  %tmp13 = load i64* %nb.addr                     ; <i64> [#uses=1]
  %cmp = icmp slt i64 %tmp12, %tmp13              ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end221
  ret void

if.end:                                           ; preds = %if.then, %entry
  call void @barrier(i32 1, i32 1) nounwind
  %tmp23 = load i64* %blockSize                   ; <i64> [#uses=1]
  %cmp24 = icmp sge i64 %tmp23, 512               ; <i1> [#uses=1]
  br i1 %cmp24, label %if.then26, label %if.end25

if.then:                                          ; preds = %entry
  %tmp14 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp15 = load i64* %tidL                        ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %arrayidx17 = getelementptr double addrspace(3)* %tmp14, i32 %conv16 ; <double addrspace(3)*> [#uses=1]
  %tmp18 = load double addrspace(1)** %src.addr   ; <double addrspace(1)*> [#uses=1]
  %tmp19 = load i64* %i                           ; <i64> [#uses=1]
  %conv20 = trunc i64 %tmp19 to i32               ; <i32> [#uses=1]
  %arrayidx21 = getelementptr double addrspace(1)* %tmp18, i32 %conv20 ; <double addrspace(1)*> [#uses=1]
  %tmp22 = load double addrspace(1)* %arrayidx21  ; <double> [#uses=1]
  store double %tmp22, double addrspace(3)* %arrayidx17
  br label %if.end

if.end25:                                         ; preds = %if.end29, %if.end
  %tmp47 = load i64* %blockSize                   ; <i64> [#uses=1]
  %cmp48 = icmp sge i64 %tmp47, 256               ; <i1> [#uses=1]
  br i1 %cmp48, label %if.then50, label %if.end49

if.then26:                                        ; preds = %if.end
  %tmp27 = load i64* %tidL                        ; <i64> [#uses=1]
  %cmp28 = icmp slt i64 %tmp27, 256               ; <i1> [#uses=1]
  br i1 %cmp28, label %if.then30, label %if.end29

if.end29:                                         ; preds = %if.then30, %if.then26
  call void @barrier(i32 2, i32 1) nounwind
  br label %if.end25

if.then30:                                        ; preds = %if.then26
  %tmp31 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp32 = load i64* %tidL                        ; <i64> [#uses=1]
  %conv33 = trunc i64 %tmp32 to i32               ; <i32> [#uses=1]
  %arrayidx34 = getelementptr double addrspace(3)* %tmp31, i32 %conv33 ; <double addrspace(3)*> [#uses=1]
  %tmp35 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp36 = load i64* %tidL                        ; <i64> [#uses=1]
  %conv37 = trunc i64 %tmp36 to i32               ; <i32> [#uses=1]
  %arrayidx38 = getelementptr double addrspace(3)* %tmp35, i32 %conv37 ; <double addrspace(3)*> [#uses=1]
  %tmp39 = load double addrspace(3)* %arrayidx38  ; <double> [#uses=1]
  %tmp40 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp41 = load i64* %tidL                        ; <i64> [#uses=1]
  %tmp42 = add i64 %tmp41, 256                    ; <i64> [#uses=1]
  %conv43 = trunc i64 %tmp42 to i32               ; <i32> [#uses=1]
  %arrayidx44 = getelementptr double addrspace(3)* %tmp40, i32 %conv43 ; <double addrspace(3)*> [#uses=1]
  %tmp45 = load double addrspace(3)* %arrayidx44  ; <double> [#uses=1]
  %call46 = call double @__max_f64(double %tmp39, double %tmp45) nounwind ; <double> [#uses=1]
  store double %call46, double addrspace(3)* %arrayidx34
  br label %if.end29

if.end49:                                         ; preds = %if.end53, %if.end25
  %tmp71 = load i64* %blockSize                   ; <i64> [#uses=1]
  %cmp72 = icmp sge i64 %tmp71, 128               ; <i1> [#uses=1]
  br i1 %cmp72, label %if.then74, label %if.end73

if.then50:                                        ; preds = %if.end25
  %tmp51 = load i64* %tidL                        ; <i64> [#uses=1]
  %cmp52 = icmp slt i64 %tmp51, 128               ; <i1> [#uses=1]
  br i1 %cmp52, label %if.then54, label %if.end53

if.end53:                                         ; preds = %if.then54, %if.then50
  call void @barrier(i32 3, i32 1) nounwind
  br label %if.end49

if.then54:                                        ; preds = %if.then50
  %tmp55 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp56 = load i64* %tidL                        ; <i64> [#uses=1]
  %conv57 = trunc i64 %tmp56 to i32               ; <i32> [#uses=1]
  %arrayidx58 = getelementptr double addrspace(3)* %tmp55, i32 %conv57 ; <double addrspace(3)*> [#uses=1]
  %tmp59 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp60 = load i64* %tidL                        ; <i64> [#uses=1]
  %conv61 = trunc i64 %tmp60 to i32               ; <i32> [#uses=1]
  %arrayidx62 = getelementptr double addrspace(3)* %tmp59, i32 %conv61 ; <double addrspace(3)*> [#uses=1]
  %tmp63 = load double addrspace(3)* %arrayidx62  ; <double> [#uses=1]
  %tmp64 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp65 = load i64* %tidL                        ; <i64> [#uses=1]
  %tmp66 = add i64 %tmp65, 128                    ; <i64> [#uses=1]
  %conv67 = trunc i64 %tmp66 to i32               ; <i32> [#uses=1]
  %arrayidx68 = getelementptr double addrspace(3)* %tmp64, i32 %conv67 ; <double addrspace(3)*> [#uses=1]
  %tmp69 = load double addrspace(3)* %arrayidx68  ; <double> [#uses=1]
  %call70 = call double @__max_f64(double %tmp63, double %tmp69) nounwind ; <double> [#uses=1]
  store double %call70, double addrspace(3)* %arrayidx58
  br label %if.end53

if.end73:                                         ; preds = %if.end77, %if.end49
  %tmp95 = load i64* %tidL                        ; <i64> [#uses=1]
  %cmp96 = icmp slt i64 %tmp95, 32                ; <i1> [#uses=1]
  br i1 %cmp96, label %if.then98, label %if.end97

if.then74:                                        ; preds = %if.end49
  %tmp75 = load i64* %tidL                        ; <i64> [#uses=1]
  %cmp76 = icmp slt i64 %tmp75, 64                ; <i1> [#uses=1]
  br i1 %cmp76, label %if.then78, label %if.end77

if.end77:                                         ; preds = %if.then78, %if.then74
  call void @barrier(i32 4, i32 1) nounwind
  br label %if.end73

if.then78:                                        ; preds = %if.then74
  %tmp79 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp80 = load i64* %tidL                        ; <i64> [#uses=1]
  %conv81 = trunc i64 %tmp80 to i32               ; <i32> [#uses=1]
  %arrayidx82 = getelementptr double addrspace(3)* %tmp79, i32 %conv81 ; <double addrspace(3)*> [#uses=1]
  %tmp83 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp84 = load i64* %tidL                        ; <i64> [#uses=1]
  %conv85 = trunc i64 %tmp84 to i32               ; <i32> [#uses=1]
  %arrayidx86 = getelementptr double addrspace(3)* %tmp83, i32 %conv85 ; <double addrspace(3)*> [#uses=1]
  %tmp87 = load double addrspace(3)* %arrayidx86  ; <double> [#uses=1]
  %tmp88 = load double addrspace(3)** %sdata      ; <double addrspace(3)*> [#uses=1]
  %tmp89 = load i64* %tidL                        ; <i64> [#uses=1]
  %tmp90 = add i64 %tmp89, 64                     ; <i64> [#uses=1]
  %conv91 = trunc i64 %tmp90 to i32               ; <i32> [#uses=1]
  %arrayidx92 = getelementptr double addrspace(3)* %tmp88, i32 %conv91 ; <double addrspace(3)*> [#uses=1]
  %tmp93 = load double addrspace(3)* %arrayidx92  ; <double> [#uses=1]
  %call94 = call double @__max_f64(double %tmp87, double %tmp93) nounwind ; <double> [#uses=1]
  store double %call94, double addrspace(3)* %arrayidx82
  br label %if.end77

if.end97:                                         ; preds = %if.end201, %if.end73
  %tmp219 = load i64* %tidL                       ; <i64> [#uses=1]
  %cmp220 = icmp eq i64 %tmp219, 0                ; <i1> [#uses=1]
  br i1 %cmp220, label %if.then222, label %if.end221

if.then98:                                        ; preds = %if.end73
  %tmp99 = load i64* %blockSize                   ; <i64> [#uses=1]
  %cmp100 = icmp sge i64 %tmp99, 64               ; <i1> [#uses=1]
  br i1 %cmp100, label %if.then102, label %if.end101

if.end101:                                        ; preds = %if.then102, %if.then98
  %tmp119 = load i64* %blockSize                  ; <i64> [#uses=1]
  %cmp120 = icmp sge i64 %tmp119, 32              ; <i1> [#uses=1]
  br i1 %cmp120, label %if.then122, label %if.end121

if.then102:                                       ; preds = %if.then98
  %tmp103 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp104 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv105 = trunc i64 %tmp104 to i32             ; <i32> [#uses=1]
  %arrayidx106 = getelementptr double addrspace(3)* %tmp103, i32 %conv105 ; <double addrspace(3)*> [#uses=1]
  %tmp107 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp108 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv109 = trunc i64 %tmp108 to i32             ; <i32> [#uses=1]
  %arrayidx110 = getelementptr double addrspace(3)* %tmp107, i32 %conv109 ; <double addrspace(3)*> [#uses=1]
  %tmp111 = load double addrspace(3)* %arrayidx110 ; <double> [#uses=1]
  %tmp112 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp113 = load i64* %tidL                       ; <i64> [#uses=1]
  %tmp114 = add i64 %tmp113, 32                   ; <i64> [#uses=1]
  %conv115 = trunc i64 %tmp114 to i32             ; <i32> [#uses=1]
  %arrayidx116 = getelementptr double addrspace(3)* %tmp112, i32 %conv115 ; <double addrspace(3)*> [#uses=1]
  %tmp117 = load double addrspace(3)* %arrayidx116 ; <double> [#uses=1]
  %call118 = call double @__max_f64(double %tmp111, double %tmp117) nounwind ; <double> [#uses=1]
  store double %call118, double addrspace(3)* %arrayidx106
  call void @barrier(i32 5, i32 1) nounwind
  br label %if.end101

if.end121:                                        ; preds = %if.then122, %if.end101
  %tmp139 = load i64* %blockSize                  ; <i64> [#uses=1]
  %cmp140 = icmp sge i64 %tmp139, 16              ; <i1> [#uses=1]
  br i1 %cmp140, label %if.then142, label %if.end141

if.then122:                                       ; preds = %if.end101
  %tmp123 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp124 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv125 = trunc i64 %tmp124 to i32             ; <i32> [#uses=1]
  %arrayidx126 = getelementptr double addrspace(3)* %tmp123, i32 %conv125 ; <double addrspace(3)*> [#uses=1]
  %tmp127 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp128 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv129 = trunc i64 %tmp128 to i32             ; <i32> [#uses=1]
  %arrayidx130 = getelementptr double addrspace(3)* %tmp127, i32 %conv129 ; <double addrspace(3)*> [#uses=1]
  %tmp131 = load double addrspace(3)* %arrayidx130 ; <double> [#uses=1]
  %tmp132 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp133 = load i64* %tidL                       ; <i64> [#uses=1]
  %tmp134 = add i64 %tmp133, 16                   ; <i64> [#uses=1]
  %conv135 = trunc i64 %tmp134 to i32             ; <i32> [#uses=1]
  %arrayidx136 = getelementptr double addrspace(3)* %tmp132, i32 %conv135 ; <double addrspace(3)*> [#uses=1]
  %tmp137 = load double addrspace(3)* %arrayidx136 ; <double> [#uses=1]
  %call138 = call double @__max_f64(double %tmp131, double %tmp137) nounwind ; <double> [#uses=1]
  store double %call138, double addrspace(3)* %arrayidx126
  call void @barrier(i32 6, i32 1) nounwind
  br label %if.end121

if.end141:                                        ; preds = %if.then142, %if.end121
  %tmp159 = load i64* %blockSize                  ; <i64> [#uses=1]
  %cmp160 = icmp sge i64 %tmp159, 8               ; <i1> [#uses=1]
  br i1 %cmp160, label %if.then162, label %if.end161

if.then142:                                       ; preds = %if.end121
  %tmp143 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp144 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv145 = trunc i64 %tmp144 to i32             ; <i32> [#uses=1]
  %arrayidx146 = getelementptr double addrspace(3)* %tmp143, i32 %conv145 ; <double addrspace(3)*> [#uses=1]
  %tmp147 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp148 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv149 = trunc i64 %tmp148 to i32             ; <i32> [#uses=1]
  %arrayidx150 = getelementptr double addrspace(3)* %tmp147, i32 %conv149 ; <double addrspace(3)*> [#uses=1]
  %tmp151 = load double addrspace(3)* %arrayidx150 ; <double> [#uses=1]
  %tmp152 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp153 = load i64* %tidL                       ; <i64> [#uses=1]
  %tmp154 = add i64 %tmp153, 8                    ; <i64> [#uses=1]
  %conv155 = trunc i64 %tmp154 to i32             ; <i32> [#uses=1]
  %arrayidx156 = getelementptr double addrspace(3)* %tmp152, i32 %conv155 ; <double addrspace(3)*> [#uses=1]
  %tmp157 = load double addrspace(3)* %arrayidx156 ; <double> [#uses=1]
  %call158 = call double @__max_f64(double %tmp151, double %tmp157) nounwind ; <double> [#uses=1]
  store double %call158, double addrspace(3)* %arrayidx146
  call void @barrier(i32 7, i32 1) nounwind
  br label %if.end141

if.end161:                                        ; preds = %if.then162, %if.end141
  %tmp179 = load i64* %blockSize                  ; <i64> [#uses=1]
  %cmp180 = icmp sge i64 %tmp179, 4               ; <i1> [#uses=1]
  br i1 %cmp180, label %if.then182, label %if.end181

if.then162:                                       ; preds = %if.end141
  %tmp163 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp164 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv165 = trunc i64 %tmp164 to i32             ; <i32> [#uses=1]
  %arrayidx166 = getelementptr double addrspace(3)* %tmp163, i32 %conv165 ; <double addrspace(3)*> [#uses=1]
  %tmp167 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp168 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv169 = trunc i64 %tmp168 to i32             ; <i32> [#uses=1]
  %arrayidx170 = getelementptr double addrspace(3)* %tmp167, i32 %conv169 ; <double addrspace(3)*> [#uses=1]
  %tmp171 = load double addrspace(3)* %arrayidx170 ; <double> [#uses=1]
  %tmp172 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp173 = load i64* %tidL                       ; <i64> [#uses=1]
  %tmp174 = add i64 %tmp173, 4                    ; <i64> [#uses=1]
  %conv175 = trunc i64 %tmp174 to i32             ; <i32> [#uses=1]
  %arrayidx176 = getelementptr double addrspace(3)* %tmp172, i32 %conv175 ; <double addrspace(3)*> [#uses=1]
  %tmp177 = load double addrspace(3)* %arrayidx176 ; <double> [#uses=1]
  %call178 = call double @__max_f64(double %tmp171, double %tmp177) nounwind ; <double> [#uses=1]
  store double %call178, double addrspace(3)* %arrayidx166
  call void @barrier(i32 8, i32 1) nounwind
  br label %if.end161

if.end181:                                        ; preds = %if.then182, %if.end161
  %tmp199 = load i64* %blockSize                  ; <i64> [#uses=1]
  %cmp200 = icmp sge i64 %tmp199, 2               ; <i1> [#uses=1]
  br i1 %cmp200, label %if.then202, label %if.end201

if.then182:                                       ; preds = %if.end161
  %tmp183 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp184 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv185 = trunc i64 %tmp184 to i32             ; <i32> [#uses=1]
  %arrayidx186 = getelementptr double addrspace(3)* %tmp183, i32 %conv185 ; <double addrspace(3)*> [#uses=1]
  %tmp187 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp188 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv189 = trunc i64 %tmp188 to i32             ; <i32> [#uses=1]
  %arrayidx190 = getelementptr double addrspace(3)* %tmp187, i32 %conv189 ; <double addrspace(3)*> [#uses=1]
  %tmp191 = load double addrspace(3)* %arrayidx190 ; <double> [#uses=1]
  %tmp192 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp193 = load i64* %tidL                       ; <i64> [#uses=1]
  %tmp194 = add i64 %tmp193, 2                    ; <i64> [#uses=1]
  %conv195 = trunc i64 %tmp194 to i32             ; <i32> [#uses=1]
  %arrayidx196 = getelementptr double addrspace(3)* %tmp192, i32 %conv195 ; <double addrspace(3)*> [#uses=1]
  %tmp197 = load double addrspace(3)* %arrayidx196 ; <double> [#uses=1]
  %call198 = call double @__max_f64(double %tmp191, double %tmp197) nounwind ; <double> [#uses=1]
  store double %call198, double addrspace(3)* %arrayidx186
  call void @barrier(i32 9, i32 1) nounwind
  br label %if.end181

if.end201:                                        ; preds = %if.then202, %if.end181
  br label %if.end97

if.then202:                                       ; preds = %if.end181
  %tmp203 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp204 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv205 = trunc i64 %tmp204 to i32             ; <i32> [#uses=1]
  %arrayidx206 = getelementptr double addrspace(3)* %tmp203, i32 %conv205 ; <double addrspace(3)*> [#uses=1]
  %tmp207 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp208 = load i64* %tidL                       ; <i64> [#uses=1]
  %conv209 = trunc i64 %tmp208 to i32             ; <i32> [#uses=1]
  %arrayidx210 = getelementptr double addrspace(3)* %tmp207, i32 %conv209 ; <double addrspace(3)*> [#uses=1]
  %tmp211 = load double addrspace(3)* %arrayidx210 ; <double> [#uses=1]
  %tmp212 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %tmp213 = load i64* %tidL                       ; <i64> [#uses=1]
  %tmp214 = add i64 %tmp213, 1                    ; <i64> [#uses=1]
  %conv215 = trunc i64 %tmp214 to i32             ; <i32> [#uses=1]
  %arrayidx216 = getelementptr double addrspace(3)* %tmp212, i32 %conv215 ; <double addrspace(3)*> [#uses=1]
  %tmp217 = load double addrspace(3)* %arrayidx216 ; <double> [#uses=1]
  %call218 = call double @__max_f64(double %tmp211, double %tmp217) nounwind ; <double> [#uses=1]
  store double %call218, double addrspace(3)* %arrayidx206
  call void @barrier(i32 10, i32 1) nounwind
  br label %if.end201

if.end221:                                        ; preds = %if.then222, %if.end97
  br label %return

if.then222:                                       ; preds = %if.end97
  %tmp223 = load double addrspace(1)** %res.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp224 = load i64* %myblock                    ; <i64> [#uses=1]
  %conv225 = trunc i64 %tmp224 to i32             ; <i32> [#uses=1]
  %arrayidx226 = getelementptr double addrspace(1)* %tmp223, i32 %conv225 ; <double addrspace(1)*> [#uses=1]
  %tmp227 = load double addrspace(3)** %sdata     ; <double addrspace(3)*> [#uses=1]
  %arrayidx228 = getelementptr double addrspace(3)* %tmp227, i32 0 ; <double addrspace(3)*> [#uses=1]
  %tmp229 = load double addrspace(3)* %arrayidx228 ; <double> [#uses=1]
  store double %tmp229, double addrspace(1)* %arrayidx226
  br label %if.end221
}

declare i32 @get_local_size(i32) nounwind

declare i32 @get_local_id(i32) nounwind

declare i32 @get_group_id(i32) nounwind

declare void @barrier(i32, i32) nounwind

define void @__OpenCL_KernelMemset_kernel(double addrspace(1)* %a, i32 %v, i64 %lobj) nounwind {
entry:
  %a.addr = alloca double addrspace(1)*, align 4  ; <double addrspace(1)**> [#uses=2]
  %v.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %lobj.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %gid = alloca i32, align 4                      ; <i32*> [#uses=3]
  %dv = alloca double, align 8                    ; <double*> [#uses=2]
  store double addrspace(1)* %a, double addrspace(1)** %a.addr
  store i32 %v, i32* %v.addr
  store i64 %lobj, i64* %lobj.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  store i32 %call, i32* %gid
  %tmp = load i32* %gid                           ; <i32> [#uses=1]
  %conv = zext i32 %tmp to i64                    ; <i64> [#uses=1]
  %tmp1 = load i64* %lobj.addr                    ; <i64> [#uses=1]
  %cmp = icmp sge i64 %conv, %tmp1                ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp2 = load i32* %v.addr                       ; <i32> [#uses=1]
  %conv3 = sitofp i32 %tmp2 to double             ; <double> [#uses=1]
  %tmp4 = fadd double 0.000000e+00, %conv3        ; <double> [#uses=1]
  store double %tmp4, double* %dv
  %tmp5 = load double addrspace(1)** %a.addr      ; <double addrspace(1)*> [#uses=1]
  %tmp6 = load i32* %gid                          ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp5, i32 %tmp6 ; <double addrspace(1)*> [#uses=1]
  %tmp7 = load double* %dv                        ; <double> [#uses=1]
  store double %tmp7, double addrspace(1)* %arrayidx
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

define void @__OpenCL_KernelMemsetV4_kernel(<4 x i32> addrspace(1)* %a, i32 %v, i64 %lobj) nounwind {
entry:
  %a.addr = alloca <4 x i32> addrspace(1)*, align 4 ; <<4 x i32> addrspace(1)**> [#uses=2]
  %v.addr = alloca i32, align 4                   ; <i32*> [#uses=2]
  %lobj.addr = alloca i64, align 8                ; <i64*> [#uses=2]
  %gid = alloca i32, align 4                      ; <i32*> [#uses=3]
  store <4 x i32> addrspace(1)* %a, <4 x i32> addrspace(1)** %a.addr
  store i32 %v, i32* %v.addr
  store i64 %lobj, i64* %lobj.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %tmp = mul i32 %call, 4                         ; <i32> [#uses=1]
  store i32 %tmp, i32* %gid
  %tmp1 = load i32* %gid                          ; <i32> [#uses=1]
  %conv = zext i32 %tmp1 to i64                   ; <i64> [#uses=1]
  %tmp2 = load i64* %lobj.addr                    ; <i64> [#uses=1]
  %cmp = icmp sge i64 %conv, %tmp2                ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp3 = load <4 x i32> addrspace(1)** %a.addr   ; <<4 x i32> addrspace(1)*> [#uses=1]
  %tmp4 = load i32* %gid                          ; <i32> [#uses=1]
  %arrayidx = getelementptr <4 x i32> addrspace(1)* %tmp3, i32 %tmp4 ; <<4 x i32> addrspace(1)*> [#uses=1]
  %tmp5 = load i32* %v.addr                       ; <i32> [#uses=4]
  %conv6 = insertelement <4 x i32> undef, i32 %tmp5, i32 0 ; <<4 x i32>> [#uses=1]
  %conv7 = insertelement <4 x i32> %conv6, i32 %tmp5, i32 1 ; <<4 x i32>> [#uses=1]
  %conv8 = insertelement <4 x i32> %conv7, i32 %tmp5, i32 2 ; <<4 x i32>> [#uses=1]
  %conv9 = insertelement <4 x i32> %conv8, i32 %tmp5, i32 3 ; <<4 x i32>> [#uses=1]
  store <4 x i32> %conv9, <4 x i32> addrspace(1)* %arrayidx
  br label %return

if.then:                                          ; preds = %entry
  br label %return
}

define void @__OpenCL_Loop1KcuRiemann_kernel(double addrspace(1)* %qleft, double addrspace(1)* %qright, double addrspace(1)* %sgnm, double addrspace(1)* %qgdnv, i64 %Hnxyt, i64 %Knarray, double %Hsmallc, double %Hgamma, double %Hsmallr, i64 %Hniter_riemann) nounwind {
entry:
  %qleft.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %qright.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=5]
  %sgnm.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=7]
  %qgdnv.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=12]
  %Hnxyt.addr = alloca i64, align 8               ; <i64*> [#uses=5]
  %Knarray.addr = alloca i64, align 8             ; <i64*> [#uses=2]
  %Hsmallc.addr = alloca double, align 8          ; <double*> [#uses=8]
  %Hgamma.addr = alloca double, align 8           ; <double*> [#uses=11]
  %Hsmallr.addr = alloca double, align 8          ; <double*> [#uses=5]
  %Hniter_riemann.addr = alloca i64, align 8      ; <i64*> [#uses=2]
  %smallp = alloca double, align 8                ; <double*> [#uses=5]
  %gamma6 = alloca double, align 8                ; <double*> [#uses=6]
  %ql = alloca double, align 8                    ; <double*> [#uses=3]
  %qr = alloca double, align 8                    ; <double*> [#uses=3]
  %usr = alloca double, align 8                   ; <double*> [#uses=2]
  %usl = alloca double, align 8                   ; <double*> [#uses=2]
  %wwl = alloca double, align 8                   ; <double*> [#uses=7]
  %wwr = alloca double, align 8                   ; <double*> [#uses=7]
  %smallpp = alloca double, align 8               ; <double*> [#uses=2]
  %iter = alloca i64, align 8                     ; <i64*> [#uses=4]
  %tid = alloca i64, align 8                      ; <i64*> [#uses=1]
  %ulS = alloca double, align 8                   ; <double*> [#uses=3]
  %plS = alloca double, align 8                   ; <double*> [#uses=5]
  %clS = alloca double, align 8                   ; <double*> [#uses=4]
  %urS = alloca double, align 8                   ; <double*> [#uses=3]
  %prS = alloca double, align 8                   ; <double*> [#uses=5]
  %crS = alloca double, align 8                   ; <double*> [#uses=4]
  %uoS = alloca double, align 8                   ; <double*> [#uses=5]
  %delpS = alloca double, align 8                 ; <double*> [#uses=5]
  %poldS = alloca double, align 8                 ; <double*> [#uses=11]
  %Kroi = alloca double, align 8                  ; <double*> [#uses=9]
  %Kuoi = alloca double, align 8                  ; <double*> [#uses=9]
  %Kpoi = alloca double, align 8                  ; <double*> [#uses=8]
  %Kwoi = alloca double, align 8                  ; <double*> [#uses=6]
  %Kdelpi = alloca double, align 8                ; <double*> [#uses=2]
  %i = alloca i64, align 8                        ; <i64*> [#uses=12]
  %idxIU = alloca i32, align 4                    ; <i32*> [#uses=6]
  %idxIV = alloca i32, align 4                    ; <i32*> [#uses=5]
  %idxIP = alloca i32, align 4                    ; <i32*> [#uses=6]
  %idxID = alloca i32, align 4                    ; <i32*> [#uses=6]
  %Krli = alloca double, align 8                  ; <double*> [#uses=4]
  %Kuli = alloca double, align 8                  ; <double*> [#uses=5]
  %Kpli = alloca double, align 8                  ; <double*> [#uses=8]
  %Krri = alloca double, align 8                  ; <double*> [#uses=4]
  %Kuri = alloca double, align 8                  ; <double*> [#uses=5]
  %Kpri = alloca double, align 8                  ; <double*> [#uses=8]
  %Kcli = alloca double, align 8                  ; <double*> [#uses=4]
  %Kcri = alloca double, align 8                  ; <double*> [#uses=4]
  %Kwli = alloca double, align 8                  ; <double*> [#uses=7]
  %Kwri = alloca double, align 8                  ; <double*> [#uses=7]
  %Kpstari = alloca double, align 8               ; <double*> [#uses=14]
  %Kpoldi = alloca double, align 8                ; <double*> [#uses=4]
  %Kindi = alloca i64, align 8                    ; <i64*> [#uses=2]
  %indi = alloca i64, align 8                     ; <i64*> [#uses=3]
  %Kustari = alloca double, align 8               ; <double*> [#uses=5]
  %Kcoi = alloca double, align 8                  ; <double*> [#uses=2]
  %Krstari = alloca double, align 8               ; <double*> [#uses=6]
  %Kcstari = alloca double, align 8               ; <double*> [#uses=2]
  %Kspouti = alloca double, align 8               ; <double*> [#uses=6]
  %Kspini = alloca double, align 8                ; <double*> [#uses=6]
  %Kushocki = alloca double, align 8              ; <double*> [#uses=3]
  %Kscri = alloca double, align 8                 ; <double*> [#uses=2]
  %Kfraci = alloca double, align 8                ; <double*> [#uses=9]
  %precision = alloca double, align 8             ; <double*> [#uses=2]
  %t1 = alloca double, align 8                    ; <double*> [#uses=2]
  %t2 = alloca double, align 8                    ; <double*> [#uses=2]
  store double addrspace(1)* %qleft, double addrspace(1)** %qleft.addr
  store double addrspace(1)* %qright, double addrspace(1)** %qright.addr
  store double addrspace(1)* %sgnm, double addrspace(1)** %sgnm.addr
  store double addrspace(1)* %qgdnv, double addrspace(1)** %qgdnv.addr
  store i64 %Hnxyt, i64* %Hnxyt.addr
  store i64 %Knarray, i64* %Knarray.addr
  store double %Hsmallc, double* %Hsmallc.addr
  store double %Hgamma, double* %Hgamma.addr
  store double %Hsmallr, double* %Hsmallr.addr
  store i64 %Hniter_riemann, i64* %Hniter_riemann.addr
  store double 0.000000e+00, double* %ulS
  store double 0.000000e+00, double* %plS
  store double 0.000000e+00, double* %clS
  store double 0.000000e+00, double* %urS
  store double 0.000000e+00, double* %prS
  store double 0.000000e+00, double* %crS
  store double 0.000000e+00, double* %uoS
  store double 0.000000e+00, double* %delpS
  store double 0.000000e+00, double* %poldS
  store double 0.000000e+00, double* %Kroi
  store double 0.000000e+00, double* %Kuoi
  store double 0.000000e+00, double* %Kpoi
  store double 0.000000e+00, double* %Kwoi
  store double 0.000000e+00, double* %Kdelpi
  %call = call i32 @get_local_id(i32 0) nounwind  ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %tid
  %call1 = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv2 = zext i32 %call1 to i64                 ; <i64> [#uses=1]
  store i64 %conv2, i64* %i
  %tmp = load i64* %i                             ; <i64> [#uses=1]
  %tmp3 = load i64* %Knarray.addr                 ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp, %tmp3                 ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %if.end459, %if.then
  ret void

if.end:                                           ; preds = %entry
  %tmp4 = load i64* %i                            ; <i64> [#uses=1]
  %conv5 = trunc i64 %tmp4 to i32                 ; <i32> [#uses=1]
  %tmp6 = load i64* %Hnxyt.addr                   ; <i64> [#uses=1]
  %conv7 = trunc i64 %tmp6 to i32                 ; <i32> [#uses=1]
  %tmp8 = mul i32 1, %conv7                       ; <i32> [#uses=1]
  %tmp9 = add i32 %conv5, %tmp8                   ; <i32> [#uses=1]
  store i32 %tmp9, i32* %idxIU
  %tmp10 = load i64* %i                           ; <i64> [#uses=1]
  %conv11 = trunc i64 %tmp10 to i32               ; <i32> [#uses=1]
  %tmp12 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv13 = trunc i64 %tmp12 to i32               ; <i32> [#uses=1]
  %tmp14 = mul i32 2, %conv13                     ; <i32> [#uses=1]
  %tmp15 = add i32 %conv11, %tmp14                ; <i32> [#uses=1]
  store i32 %tmp15, i32* %idxIV
  %tmp16 = load i64* %i                           ; <i64> [#uses=1]
  %conv17 = trunc i64 %tmp16 to i32               ; <i32> [#uses=1]
  %tmp18 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv19 = trunc i64 %tmp18 to i32               ; <i32> [#uses=1]
  %tmp20 = mul i32 3, %conv19                     ; <i32> [#uses=1]
  %tmp21 = add i32 %conv17, %tmp20                ; <i32> [#uses=1]
  store i32 %tmp21, i32* %idxIP
  %tmp22 = load i64* %i                           ; <i64> [#uses=1]
  %conv23 = trunc i64 %tmp22 to i32               ; <i32> [#uses=1]
  %tmp24 = load i64* %Hnxyt.addr                  ; <i64> [#uses=1]
  %conv25 = trunc i64 %tmp24 to i32               ; <i32> [#uses=1]
  %tmp26 = mul i32 0, %conv25                     ; <i32> [#uses=1]
  %tmp27 = add i32 %conv23, %tmp26                ; <i32> [#uses=1]
  store i32 %tmp27, i32* %idxID
  %tmp28 = load double* %Hsmallc.addr             ; <double> [#uses=1]
  %tmp29 = load double* %Hsmallc.addr             ; <double> [#uses=1]
  %tmp30 = fmul double %tmp28, %tmp29             ; <double> [#uses=1]
  %tmp31 = load double* %Hgamma.addr              ; <double> [#uses=1]
  %tmp32 = fdiv double %tmp30, %tmp31             ; <double> [#uses=1]
  store double %tmp32, double* %smallp
  %tmp33 = load double addrspace(1)** %qleft.addr ; <double addrspace(1)*> [#uses=1]
  %tmp34 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp33, i32 %tmp34 ; <double addrspace(1)*> [#uses=1]
  %tmp35 = load double addrspace(1)* %arrayidx    ; <double> [#uses=1]
  %tmp36 = load double* %Hsmallr.addr             ; <double> [#uses=1]
  %call37 = call double @__max_f64(double %tmp35, double %tmp36) nounwind ; <double> [#uses=1]
  store double %call37, double* %Krli
  %tmp38 = load double addrspace(1)** %qleft.addr ; <double addrspace(1)*> [#uses=1]
  %tmp39 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx40 = getelementptr double addrspace(1)* %tmp38, i32 %tmp39 ; <double addrspace(1)*> [#uses=1]
  %tmp41 = load double addrspace(1)* %arrayidx40  ; <double> [#uses=1]
  store double %tmp41, double* %Kuli
  %tmp42 = load double addrspace(1)** %qleft.addr ; <double addrspace(1)*> [#uses=1]
  %tmp43 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx44 = getelementptr double addrspace(1)* %tmp42, i32 %tmp43 ; <double addrspace(1)*> [#uses=1]
  %tmp45 = load double addrspace(1)* %arrayidx44  ; <double> [#uses=1]
  %tmp46 = load double* %Krli                     ; <double> [#uses=1]
  %tmp47 = load double* %smallp                   ; <double> [#uses=1]
  %tmp48 = fmul double %tmp46, %tmp47             ; <double> [#uses=1]
  %call49 = call double @__max_f64(double %tmp45, double %tmp48) nounwind ; <double> [#uses=1]
  store double %call49, double* %Kpli
  %tmp50 = load double addrspace(1)** %qright.addr ; <double addrspace(1)*> [#uses=1]
  %tmp51 = load i32* %idxID                       ; <i32> [#uses=1]
  %arrayidx52 = getelementptr double addrspace(1)* %tmp50, i32 %tmp51 ; <double addrspace(1)*> [#uses=1]
  %tmp53 = load double addrspace(1)* %arrayidx52  ; <double> [#uses=1]
  %tmp54 = load double* %Hsmallr.addr             ; <double> [#uses=1]
  %call55 = call double @__max_f64(double %tmp53, double %tmp54) nounwind ; <double> [#uses=1]
  store double %call55, double* %Krri
  %tmp56 = load double addrspace(1)** %qright.addr ; <double addrspace(1)*> [#uses=1]
  %tmp57 = load i32* %idxIU                       ; <i32> [#uses=1]
  %arrayidx58 = getelementptr double addrspace(1)* %tmp56, i32 %tmp57 ; <double addrspace(1)*> [#uses=1]
  %tmp59 = load double addrspace(1)* %arrayidx58  ; <double> [#uses=1]
  store double %tmp59, double* %Kuri
  %tmp60 = load double addrspace(1)** %qright.addr ; <double addrspace(1)*> [#uses=1]
  %tmp61 = load i32* %idxIP                       ; <i32> [#uses=1]
  %arrayidx62 = getelementptr double addrspace(1)* %tmp60, i32 %tmp61 ; <double addrspace(1)*> [#uses=1]
  %tmp63 = load double addrspace(1)* %arrayidx62  ; <double> [#uses=1]
  %tmp64 = load double* %Krri                     ; <double> [#uses=1]
  %tmp65 = load double* %smallp                   ; <double> [#uses=1]
  %tmp66 = fmul double %tmp64, %tmp65             ; <double> [#uses=1]
  %call67 = call double @__max_f64(double %tmp63, double %tmp66) nounwind ; <double> [#uses=1]
  store double %call67, double* %Kpri
  %tmp68 = load double* %Hgamma.addr              ; <double> [#uses=1]
  %tmp69 = load double* %Kpli                     ; <double> [#uses=1]
  %tmp70 = fmul double %tmp68, %tmp69             ; <double> [#uses=1]
  %tmp71 = load double* %Krli                     ; <double> [#uses=1]
  %tmp72 = fmul double %tmp70, %tmp71             ; <double> [#uses=1]
  store double %tmp72, double* %Kcli
  %tmp73 = load double* %Hgamma.addr              ; <double> [#uses=1]
  %tmp74 = load double* %Kpri                     ; <double> [#uses=1]
  %tmp75 = fmul double %tmp73, %tmp74             ; <double> [#uses=1]
  %tmp76 = load double* %Krri                     ; <double> [#uses=1]
  %tmp77 = fmul double %tmp75, %tmp76             ; <double> [#uses=1]
  store double %tmp77, double* %Kcri
  %tmp78 = load double* %Kcli                     ; <double> [#uses=1]
  %call79 = call double @__sqrt_f64(double %tmp78) nounwind ; <double> [#uses=1]
  store double %call79, double* %Kwli
  %tmp80 = load double* %Kcri                     ; <double> [#uses=1]
  %call81 = call double @__sqrt_f64(double %tmp80) nounwind ; <double> [#uses=1]
  store double %call81, double* %Kwri
  %tmp82 = load double* %Kwri                     ; <double> [#uses=1]
  %tmp83 = load double* %Kpli                     ; <double> [#uses=1]
  %tmp84 = fmul double %tmp82, %tmp83             ; <double> [#uses=1]
  %tmp85 = load double* %Kwli                     ; <double> [#uses=1]
  %tmp86 = load double* %Kpri                     ; <double> [#uses=1]
  %tmp87 = fmul double %tmp85, %tmp86             ; <double> [#uses=1]
  %tmp88 = fadd double %tmp84, %tmp87             ; <double> [#uses=1]
  %tmp89 = load double* %Kwli                     ; <double> [#uses=1]
  %tmp90 = load double* %Kwri                     ; <double> [#uses=1]
  %tmp91 = fmul double %tmp89, %tmp90             ; <double> [#uses=1]
  %tmp92 = load double* %Kuli                     ; <double> [#uses=1]
  %tmp93 = load double* %Kuri                     ; <double> [#uses=1]
  %tmp94 = fsub double %tmp92, %tmp93             ; <double> [#uses=1]
  %tmp95 = fmul double %tmp91, %tmp94             ; <double> [#uses=1]
  %tmp96 = fadd double %tmp88, %tmp95             ; <double> [#uses=1]
  %tmp97 = load double* %Kwli                     ; <double> [#uses=1]
  %tmp98 = load double* %Kwri                     ; <double> [#uses=1]
  %tmp99 = fadd double %tmp97, %tmp98             ; <double> [#uses=1]
  %tmp100 = fdiv double %tmp96, %tmp99            ; <double> [#uses=1]
  store double %tmp100, double* %Kpstari
  %tmp101 = load double* %Kpstari                 ; <double> [#uses=1]
  %call102 = call double @__max_f64(double %tmp101, double 0.000000e+00) nounwind ; <double> [#uses=1]
  store double %call102, double* %Kpstari
  %tmp103 = load double* %Kpstari                 ; <double> [#uses=1]
  store double %tmp103, double* %Kpoldi
  store i64 1, i64* %Kindi
  %tmp104 = load double* %Kuli                    ; <double> [#uses=1]
  store double %tmp104, double* %ulS
  %tmp105 = load double* %Kpli                    ; <double> [#uses=1]
  store double %tmp105, double* %plS
  %tmp106 = load double* %Kcli                    ; <double> [#uses=1]
  store double %tmp106, double* %clS
  %tmp107 = load double* %Kuri                    ; <double> [#uses=1]
  store double %tmp107, double* %urS
  %tmp108 = load double* %Kpri                    ; <double> [#uses=1]
  store double %tmp108, double* %prS
  %tmp109 = load double* %Kcri                    ; <double> [#uses=1]
  store double %tmp109, double* %crS
  %tmp110 = load double* %Kuoi                    ; <double> [#uses=1]
  store double %tmp110, double* %uoS
  %tmp111 = load double* %Kdelpi                  ; <double> [#uses=1]
  store double %tmp111, double* %delpS
  %tmp112 = load double* %Kpoldi                  ; <double> [#uses=1]
  store double %tmp112, double* %poldS
  %tmp113 = load double* %Hsmallc.addr            ; <double> [#uses=1]
  %tmp114 = load double* %Hsmallc.addr            ; <double> [#uses=1]
  %tmp115 = fmul double %tmp113, %tmp114          ; <double> [#uses=1]
  %tmp116 = load double* %Hgamma.addr             ; <double> [#uses=1]
  %tmp117 = fdiv double %tmp115, %tmp116          ; <double> [#uses=1]
  store double %tmp117, double* %smallp
  %tmp118 = load double* %Hsmallr.addr            ; <double> [#uses=1]
  %tmp119 = load double* %smallp                  ; <double> [#uses=1]
  %tmp120 = fmul double %tmp118, %tmp119          ; <double> [#uses=1]
  store double %tmp120, double* %smallpp
  %tmp121 = load double* %Hgamma.addr             ; <double> [#uses=1]
  %tmp122 = fadd double %tmp121, 1.000000e+00     ; <double> [#uses=1]
  %tmp123 = load double* %Hgamma.addr             ; <double> [#uses=1]
  %tmp124 = fmul double 2.000000e+00, %tmp123     ; <double> [#uses=1]
  %tmp125 = fdiv double %tmp122, %tmp124          ; <double> [#uses=1]
  store double %tmp125, double* %gamma6
  %tmp126 = load i64* %Kindi                      ; <i64> [#uses=1]
  store i64 %tmp126, i64* %indi
  store i64 0, i64* %iter
  br label %for.cond

if.then:                                          ; preds = %entry
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end
  %tmp127 = load i64* %iter                       ; <i64> [#uses=1]
  %tmp128 = load i64* %Hniter_riemann.addr        ; <i64> [#uses=1]
  %cmp129 = icmp slt i64 %tmp127, %tmp128         ; <i1> [#uses=1]
  br i1 %cmp129, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %__T341139552

for.body:                                         ; preds = %for.cond
  store double 1.000000e-06, double* %precision
  %tmp130 = load double* %clS                     ; <double> [#uses=1]
  %tmp131 = load double* %gamma6                  ; <double> [#uses=1]
  %tmp132 = load double* %poldS                   ; <double> [#uses=1]
  %tmp133 = load double* %plS                     ; <double> [#uses=1]
  %tmp134 = fsub double %tmp132, %tmp133          ; <double> [#uses=1]
  %tmp135 = fmul double %tmp131, %tmp134          ; <double> [#uses=1]
  %tmp136 = load double* %plS                     ; <double> [#uses=1]
  %tmp137 = fdiv double %tmp135, %tmp136          ; <double> [#uses=1]
  %tmp138 = fadd double 1.000000e+00, %tmp137     ; <double> [#uses=1]
  %tmp139 = fmul double %tmp130, %tmp138          ; <double> [#uses=1]
  %call140 = call double @__sqrt_f64(double %tmp139) nounwind ; <double> [#uses=1]
  store double %call140, double* %wwl
  %tmp141 = load double* %crS                     ; <double> [#uses=1]
  %tmp142 = load double* %gamma6                  ; <double> [#uses=1]
  %tmp143 = load double* %poldS                   ; <double> [#uses=1]
  %tmp144 = load double* %prS                     ; <double> [#uses=1]
  %tmp145 = fsub double %tmp143, %tmp144          ; <double> [#uses=1]
  %tmp146 = fmul double %tmp142, %tmp145          ; <double> [#uses=1]
  %tmp147 = load double* %prS                     ; <double> [#uses=1]
  %tmp148 = fdiv double %tmp146, %tmp147          ; <double> [#uses=1]
  %tmp149 = fadd double 1.000000e+00, %tmp148     ; <double> [#uses=1]
  %tmp150 = fmul double %tmp141, %tmp149          ; <double> [#uses=1]
  %call151 = call double @__sqrt_f64(double %tmp150) nounwind ; <double> [#uses=1]
  store double %call151, double* %wwr
  %tmp152 = load double* %wwl                     ; <double> [#uses=1]
  %tmp153 = fmul double 2.000000e+00, %tmp152     ; <double> [#uses=1]
  %tmp154 = load double* %wwl                     ; <double> [#uses=1]
  %tmp155 = load double* %wwl                     ; <double> [#uses=1]
  %tmp156 = fmul double %tmp154, %tmp155          ; <double> [#uses=1]
  %tmp157 = fmul double %tmp153, %tmp156          ; <double> [#uses=1]
  %tmp158 = load double* %wwl                     ; <double> [#uses=1]
  %tmp159 = load double* %wwl                     ; <double> [#uses=1]
  %tmp160 = fmul double %tmp158, %tmp159          ; <double> [#uses=1]
  %tmp161 = load double* %clS                     ; <double> [#uses=1]
  %tmp162 = fadd double %tmp160, %tmp161          ; <double> [#uses=1]
  %tmp163 = fdiv double %tmp157, %tmp162          ; <double> [#uses=1]
  store double %tmp163, double* %ql
  %tmp164 = load double* %wwr                     ; <double> [#uses=1]
  %tmp165 = fmul double 2.000000e+00, %tmp164     ; <double> [#uses=1]
  %tmp166 = load double* %wwr                     ; <double> [#uses=1]
  %tmp167 = load double* %wwr                     ; <double> [#uses=1]
  %tmp168 = fmul double %tmp166, %tmp167          ; <double> [#uses=1]
  %tmp169 = fmul double %tmp165, %tmp168          ; <double> [#uses=1]
  %tmp170 = load double* %wwr                     ; <double> [#uses=1]
  %tmp171 = load double* %wwr                     ; <double> [#uses=1]
  %tmp172 = fmul double %tmp170, %tmp171          ; <double> [#uses=1]
  %tmp173 = load double* %crS                     ; <double> [#uses=1]
  %tmp174 = fadd double %tmp172, %tmp173          ; <double> [#uses=1]
  %tmp175 = fdiv double %tmp169, %tmp174          ; <double> [#uses=1]
  store double %tmp175, double* %qr
  %tmp176 = load double* %ulS                     ; <double> [#uses=1]
  %tmp177 = load double* %poldS                   ; <double> [#uses=1]
  %tmp178 = load double* %plS                     ; <double> [#uses=1]
  %tmp179 = fsub double %tmp177, %tmp178          ; <double> [#uses=1]
  %tmp180 = load double* %wwl                     ; <double> [#uses=1]
  %tmp181 = fdiv double %tmp179, %tmp180          ; <double> [#uses=1]
  %tmp182 = fsub double %tmp176, %tmp181          ; <double> [#uses=1]
  store double %tmp182, double* %usl
  %tmp183 = load double* %urS                     ; <double> [#uses=1]
  %tmp184 = load double* %poldS                   ; <double> [#uses=1]
  %tmp185 = load double* %prS                     ; <double> [#uses=1]
  %tmp186 = fsub double %tmp184, %tmp185          ; <double> [#uses=1]
  %tmp187 = load double* %wwr                     ; <double> [#uses=1]
  %tmp188 = fdiv double %tmp186, %tmp187          ; <double> [#uses=1]
  %tmp189 = fadd double %tmp183, %tmp188          ; <double> [#uses=1]
  store double %tmp189, double* %usr
  %tmp190 = load double* %qr                      ; <double> [#uses=1]
  %tmp191 = load double* %ql                      ; <double> [#uses=1]
  %tmp192 = fmul double %tmp190, %tmp191          ; <double> [#uses=1]
  %tmp193 = load double* %qr                      ; <double> [#uses=1]
  %tmp194 = load double* %ql                      ; <double> [#uses=1]
  %tmp195 = fadd double %tmp193, %tmp194          ; <double> [#uses=1]
  %tmp196 = fdiv double %tmp192, %tmp195          ; <double> [#uses=1]
  %tmp197 = load double* %usl                     ; <double> [#uses=1]
  %tmp198 = load double* %usr                     ; <double> [#uses=1]
  %tmp199 = fsub double %tmp197, %tmp198          ; <double> [#uses=1]
  %tmp200 = fmul double %tmp196, %tmp199          ; <double> [#uses=1]
  store double %tmp200, double* %t1
  %tmp201 = load double* %poldS                   ; <double> [#uses=1]
  %tmp202 = fsub double -0.000000e+00, %tmp201    ; <double> [#uses=1]
  store double %tmp202, double* %t2
  %tmp203 = load double* %t1                      ; <double> [#uses=1]
  %tmp204 = load double* %t2                      ; <double> [#uses=1]
  %call205 = call double @__max_f64(double %tmp203, double %tmp204) nounwind ; <double> [#uses=1]
  store double %call205, double* %delpS
  %tmp206 = load double* %poldS                   ; <double> [#uses=1]
  %tmp207 = load double* %delpS                   ; <double> [#uses=1]
  %tmp208 = fadd double %tmp206, %tmp207          ; <double> [#uses=1]
  store double %tmp208, double* %poldS
  %tmp209 = load double* %delpS                   ; <double> [#uses=1]
  %tmp210 = load double* %poldS                   ; <double> [#uses=1]
  %tmp211 = load double* %smallpp                 ; <double> [#uses=1]
  %tmp212 = fadd double %tmp210, %tmp211          ; <double> [#uses=1]
  %tmp213 = fdiv double %tmp209, %tmp212          ; <double> [#uses=1]
  %call214 = call double @__fabs_f64(double %tmp213) nounwind ; <double> [#uses=1]
  store double %call214, double* %uoS
  %tmp215 = load double* %uoS                     ; <double> [#uses=1]
  %tmp216 = load double* %precision               ; <double> [#uses=1]
  %cmp217 = fcmp ogt double %tmp215, %tmp216      ; <i1> [#uses=1]
  %cmp.ext = zext i1 %cmp217 to i32               ; <i32> [#uses=1]
  %conv218 = sext i32 %cmp.ext to i64             ; <i64> [#uses=1]
  store i64 %conv218, i64* %indi
  %tmp219 = load i64* %indi                       ; <i64> [#uses=1]
  %tobool = icmp ne i64 %tmp219, 0                ; <i1> [#uses=1]
  %lnot = xor i1 %tobool, true                    ; <i1> [#uses=1]
  br i1 %lnot, label %if.then221, label %if.end220

for.inc:                                          ; preds = %if.end220
  %tmp222 = load i64* %iter                       ; <i64> [#uses=1]
  %tmp223 = add i64 %tmp222, 1                    ; <i64> [#uses=1]
  store i64 %tmp223, i64* %iter
  br label %for.cond

if.end220:                                        ; preds = %for.body
  br label %for.inc

if.then221:                                       ; preds = %for.body
  br label %__T341139552

__T341139552:                                     ; preds = %for.exit, %if.then221
  %tmp224 = load double* %uoS                     ; <double> [#uses=1]
  store double %tmp224, double* %Kuoi
  %tmp225 = load double* %poldS                   ; <double> [#uses=1]
  store double %tmp225, double* %Kpoldi
  %tmp226 = load double* %Hgamma.addr             ; <double> [#uses=1]
  %tmp227 = fadd double %tmp226, 1.000000e+00     ; <double> [#uses=1]
  %tmp228 = load double* %Hgamma.addr             ; <double> [#uses=1]
  %tmp229 = fmul double 2.000000e+00, %tmp228     ; <double> [#uses=1]
  %tmp230 = fdiv double %tmp227, %tmp229          ; <double> [#uses=1]
  store double %tmp230, double* %gamma6
  %tmp231 = load double* %Kpoldi                  ; <double> [#uses=1]
  store double %tmp231, double* %Kpstari
  %tmp232 = load double* %Kcli                    ; <double> [#uses=1]
  %tmp233 = load double* %gamma6                  ; <double> [#uses=1]
  %tmp234 = load double* %Kpstari                 ; <double> [#uses=1]
  %tmp235 = load double* %Kpli                    ; <double> [#uses=1]
  %tmp236 = fsub double %tmp234, %tmp235          ; <double> [#uses=1]
  %tmp237 = fmul double %tmp233, %tmp236          ; <double> [#uses=1]
  %tmp238 = load double* %Kpli                    ; <double> [#uses=1]
  %tmp239 = fdiv double %tmp237, %tmp238          ; <double> [#uses=1]
  %tmp240 = fadd double 1.000000e+00, %tmp239     ; <double> [#uses=1]
  %tmp241 = fmul double %tmp232, %tmp240          ; <double> [#uses=1]
  %call242 = call double @__sqrt_f64(double %tmp241) nounwind ; <double> [#uses=1]
  store double %call242, double* %Kwli
  %tmp243 = load double* %Kcri                    ; <double> [#uses=1]
  %tmp244 = load double* %gamma6                  ; <double> [#uses=1]
  %tmp245 = load double* %Kpstari                 ; <double> [#uses=1]
  %tmp246 = load double* %Kpri                    ; <double> [#uses=1]
  %tmp247 = fsub double %tmp245, %tmp246          ; <double> [#uses=1]
  %tmp248 = fmul double %tmp244, %tmp247          ; <double> [#uses=1]
  %tmp249 = load double* %Kpri                    ; <double> [#uses=1]
  %tmp250 = fdiv double %tmp248, %tmp249          ; <double> [#uses=1]
  %tmp251 = fadd double 1.000000e+00, %tmp250     ; <double> [#uses=1]
  %tmp252 = fmul double %tmp243, %tmp251          ; <double> [#uses=1]
  %call253 = call double @__sqrt_f64(double %tmp252) nounwind ; <double> [#uses=1]
  store double %call253, double* %Kwri
  %tmp254 = load double* %Kuli                    ; <double> [#uses=1]
  %tmp255 = load double* %Kpli                    ; <double> [#uses=1]
  %tmp256 = load double* %Kpstari                 ; <double> [#uses=1]
  %tmp257 = fsub double %tmp255, %tmp256          ; <double> [#uses=1]
  %tmp258 = load double* %Kwli                    ; <double> [#uses=1]
  %tmp259 = fdiv double %tmp257, %tmp258          ; <double> [#uses=1]
  %tmp260 = fadd double %tmp254, %tmp259          ; <double> [#uses=1]
  %tmp261 = load double* %Kuri                    ; <double> [#uses=1]
  %tmp262 = fadd double %tmp260, %tmp261          ; <double> [#uses=1]
  %tmp263 = load double* %Kpri                    ; <double> [#uses=1]
  %tmp264 = load double* %Kpstari                 ; <double> [#uses=1]
  %tmp265 = fsub double %tmp263, %tmp264          ; <double> [#uses=1]
  %tmp266 = load double* %Kwri                    ; <double> [#uses=1]
  %tmp267 = fdiv double %tmp265, %tmp266          ; <double> [#uses=1]
  %tmp268 = fsub double %tmp262, %tmp267          ; <double> [#uses=1]
  %tmp269 = fmul double 5.000000e-01, %tmp268     ; <double> [#uses=1]
  store double %tmp269, double* %Kustari
  %tmp270 = load double addrspace(1)** %sgnm.addr ; <double addrspace(1)*> [#uses=1]
  %tmp271 = load i64* %i                          ; <i64> [#uses=1]
  %conv272 = trunc i64 %tmp271 to i32             ; <i32> [#uses=1]
  %arrayidx273 = getelementptr double addrspace(1)* %tmp270, i32 %conv272 ; <double addrspace(1)*> [#uses=1]
  %tmp274 = load double* %Kustari                 ; <double> [#uses=1]
  %cmp275 = fcmp ogt double %tmp274, 0.000000e+00 ; <i1> [#uses=1]
  br i1 %cmp275, label %cond.then, label %cond.else

cond.then:                                        ; preds = %__T341139552
  br label %cond.end

cond.else:                                        ; preds = %__T341139552
  br label %cond.end

cond.end:                                         ; preds = %cond.else, %cond.then
  %cond = phi i32 [ 1, %cond.then ], [ -1, %cond.else ] ; <i32> [#uses=1]
  %conv277 = sitofp i32 %cond to double           ; <double> [#uses=1]
  store double %conv277, double addrspace(1)* %arrayidx273
  %tmp278 = load double addrspace(1)** %sgnm.addr ; <double addrspace(1)*> [#uses=1]
  %tmp279 = load i64* %i                          ; <i64> [#uses=1]
  %conv280 = trunc i64 %tmp279 to i32             ; <i32> [#uses=1]
  %arrayidx281 = getelementptr double addrspace(1)* %tmp278, i32 %conv280 ; <double addrspace(1)*> [#uses=1]
  %tmp282 = load double addrspace(1)* %arrayidx281 ; <double> [#uses=1]
  %cmp283 = fcmp oeq double %tmp282, 1.000000e+00 ; <i1> [#uses=1]
  br i1 %cmp283, label %if.then286, label %if.else

if.end285:                                        ; preds = %if.else, %if.then286
  %tmp295 = load double* %Hsmallc.addr            ; <double> [#uses=1]
  %tmp296 = load double* %Hgamma.addr             ; <double> [#uses=1]
  %tmp297 = load double* %Kpoi                    ; <double> [#uses=1]
  %tmp298 = fmul double %tmp296, %tmp297          ; <double> [#uses=1]
  %tmp299 = load double* %Kroi                    ; <double> [#uses=1]
  %tmp300 = fdiv double %tmp298, %tmp299          ; <double> [#uses=1]
  %call301 = call double @__fabs_f64(double %tmp300) nounwind ; <double> [#uses=1]
  %call302 = call double @__sqrt_f64(double %call301) nounwind ; <double> [#uses=1]
  %call303 = call double @__max_f64(double %tmp295, double %call302) nounwind ; <double> [#uses=1]
  store double %call303, double* %Kcoi
  %tmp304 = load double* %Kroi                    ; <double> [#uses=1]
  %tmp305 = load double* %Kroi                    ; <double> [#uses=1]
  %tmp306 = load double* %Kpoi                    ; <double> [#uses=1]
  %tmp307 = load double* %Kpstari                 ; <double> [#uses=1]
  %tmp308 = fsub double %tmp306, %tmp307          ; <double> [#uses=1]
  %tmp309 = fmul double %tmp305, %tmp308          ; <double> [#uses=1]
  %tmp310 = load double* %Kwoi                    ; <double> [#uses=1]
  %tmp311 = load double* %Kwoi                    ; <double> [#uses=1]
  %tmp312 = fmul double %tmp310, %tmp311          ; <double> [#uses=1]
  %tmp313 = fdiv double %tmp309, %tmp312          ; <double> [#uses=1]
  %tmp314 = fadd double 1.000000e+00, %tmp313     ; <double> [#uses=1]
  %tmp315 = fdiv double %tmp304, %tmp314          ; <double> [#uses=1]
  store double %tmp315, double* %Krstari
  %tmp316 = load double* %Krstari                 ; <double> [#uses=1]
  %tmp317 = load double* %Hsmallr.addr            ; <double> [#uses=1]
  %call318 = call double @__max_f64(double %tmp316, double %tmp317) nounwind ; <double> [#uses=1]
  store double %call318, double* %Krstari
  %tmp319 = load double* %Hsmallc.addr            ; <double> [#uses=1]
  %tmp320 = load double* %Hgamma.addr             ; <double> [#uses=1]
  %tmp321 = load double* %Kpstari                 ; <double> [#uses=1]
  %tmp322 = fmul double %tmp320, %tmp321          ; <double> [#uses=1]
  %tmp323 = load double* %Krstari                 ; <double> [#uses=1]
  %tmp324 = fdiv double %tmp322, %tmp323          ; <double> [#uses=1]
  %call325 = call double @__fabs_f64(double %tmp324) nounwind ; <double> [#uses=1]
  %call326 = call double @__sqrt_f64(double %call325) nounwind ; <double> [#uses=1]
  %call327 = call double @__max_f64(double %tmp319, double %call326) nounwind ; <double> [#uses=1]
  store double %call327, double* %Kcstari
  %tmp328 = load double* %Kcoi                    ; <double> [#uses=1]
  %tmp329 = load double addrspace(1)** %sgnm.addr ; <double addrspace(1)*> [#uses=1]
  %tmp330 = load i64* %i                          ; <i64> [#uses=1]
  %conv331 = trunc i64 %tmp330 to i32             ; <i32> [#uses=1]
  %arrayidx332 = getelementptr double addrspace(1)* %tmp329, i32 %conv331 ; <double addrspace(1)*> [#uses=1]
  %tmp333 = load double addrspace(1)* %arrayidx332 ; <double> [#uses=1]
  %tmp334 = load double* %Kuoi                    ; <double> [#uses=1]
  %tmp335 = fmul double %tmp333, %tmp334          ; <double> [#uses=1]
  %tmp336 = fsub double %tmp328, %tmp335          ; <double> [#uses=1]
  store double %tmp336, double* %Kspouti
  %tmp337 = load double* %Kcstari                 ; <double> [#uses=1]
  %tmp338 = load double addrspace(1)** %sgnm.addr ; <double addrspace(1)*> [#uses=1]
  %tmp339 = load i64* %i                          ; <i64> [#uses=1]
  %conv340 = trunc i64 %tmp339 to i32             ; <i32> [#uses=1]
  %arrayidx341 = getelementptr double addrspace(1)* %tmp338, i32 %conv340 ; <double addrspace(1)*> [#uses=1]
  %tmp342 = load double addrspace(1)* %arrayidx341 ; <double> [#uses=1]
  %tmp343 = load double* %Kustari                 ; <double> [#uses=1]
  %tmp344 = fmul double %tmp342, %tmp343          ; <double> [#uses=1]
  %tmp345 = fsub double %tmp337, %tmp344          ; <double> [#uses=1]
  store double %tmp345, double* %Kspini
  %tmp346 = load double* %Kwoi                    ; <double> [#uses=1]
  %tmp347 = load double* %Kroi                    ; <double> [#uses=1]
  %tmp348 = fdiv double %tmp346, %tmp347          ; <double> [#uses=1]
  %tmp349 = load double addrspace(1)** %sgnm.addr ; <double addrspace(1)*> [#uses=1]
  %tmp350 = load i64* %i                          ; <i64> [#uses=1]
  %conv351 = trunc i64 %tmp350 to i32             ; <i32> [#uses=1]
  %arrayidx352 = getelementptr double addrspace(1)* %tmp349, i32 %conv351 ; <double addrspace(1)*> [#uses=1]
  %tmp353 = load double addrspace(1)* %arrayidx352 ; <double> [#uses=1]
  %tmp354 = load double* %Kuoi                    ; <double> [#uses=1]
  %tmp355 = fmul double %tmp353, %tmp354          ; <double> [#uses=1]
  %tmp356 = fsub double %tmp348, %tmp355          ; <double> [#uses=1]
  store double %tmp356, double* %Kushocki
  %tmp357 = load double* %Kpstari                 ; <double> [#uses=1]
  %tmp358 = load double* %Kpoi                    ; <double> [#uses=1]
  %cmp359 = fcmp oge double %tmp357, %tmp358      ; <i1> [#uses=1]
  br i1 %cmp359, label %if.then362, label %if.end361

if.then286:                                       ; preds = %cond.end
  %tmp287 = load double* %Krli                    ; <double> [#uses=1]
  store double %tmp287, double* %Kroi
  %tmp288 = load double* %Kuli                    ; <double> [#uses=1]
  store double %tmp288, double* %Kuoi
  %tmp289 = load double* %Kpli                    ; <double> [#uses=1]
  store double %tmp289, double* %Kpoi
  %tmp290 = load double* %Kwli                    ; <double> [#uses=1]
  store double %tmp290, double* %Kwoi
  br label %if.end285

if.else:                                          ; preds = %cond.end
  %tmp291 = load double* %Krri                    ; <double> [#uses=1]
  store double %tmp291, double* %Kroi
  %tmp292 = load double* %Kuri                    ; <double> [#uses=1]
  store double %tmp292, double* %Kuoi
  %tmp293 = load double* %Kpri                    ; <double> [#uses=1]
  store double %tmp293, double* %Kpoi
  %tmp294 = load double* %Kwri                    ; <double> [#uses=1]
  store double %tmp294, double* %Kwoi
  br label %if.end285

if.end361:                                        ; preds = %if.then362, %if.end285
  %tmp365 = load double* %Kspouti                 ; <double> [#uses=1]
  %tmp366 = load double* %Kspini                  ; <double> [#uses=1]
  %tmp367 = fsub double %tmp365, %tmp366          ; <double> [#uses=1]
  %tmp368 = load double* %Hsmallc.addr            ; <double> [#uses=1]
  %tmp369 = load double* %Kspouti                 ; <double> [#uses=1]
  %tmp370 = load double* %Kspini                  ; <double> [#uses=1]
  %tmp371 = fadd double %tmp369, %tmp370          ; <double> [#uses=1]
  %call372 = call double @__fabs_f64(double %tmp371) nounwind ; <double> [#uses=1]
  %tmp373 = fadd double %tmp368, %call372         ; <double> [#uses=1]
  %call374 = call double @__max_f64(double %tmp367, double %tmp373) nounwind ; <double> [#uses=1]
  store double %call374, double* %Kscri
  %tmp375 = load double* %Kspouti                 ; <double> [#uses=1]
  %tmp376 = load double* %Kspini                  ; <double> [#uses=1]
  %tmp377 = fadd double %tmp375, %tmp376          ; <double> [#uses=1]
  %tmp378 = load double* %Kscri                   ; <double> [#uses=1]
  %tmp379 = fdiv double %tmp377, %tmp378          ; <double> [#uses=1]
  %tmp380 = fadd double 1.000000e+00, %tmp379     ; <double> [#uses=1]
  %tmp381 = fmul double %tmp380, 5.000000e-01     ; <double> [#uses=1]
  store double %tmp381, double* %Kfraci
  %tmp382 = load double* %Kfraci                  ; <double> [#uses=1]
  %call383 = call double @__min_f64(double 1.000000e+00, double %tmp382) nounwind ; <double> [#uses=1]
  %call384 = call double @__max_f64(double 0.000000e+00, double %call383) nounwind ; <double> [#uses=1]
  store double %call384, double* %Kfraci
  %tmp385 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp386 = load i32* %idxID                      ; <i32> [#uses=1]
  %arrayidx387 = getelementptr double addrspace(1)* %tmp385, i32 %tmp386 ; <double addrspace(1)*> [#uses=1]
  %tmp388 = load double* %Kfraci                  ; <double> [#uses=1]
  %tmp389 = load double* %Krstari                 ; <double> [#uses=1]
  %tmp390 = fmul double %tmp388, %tmp389          ; <double> [#uses=1]
  %tmp391 = load double* %Kfraci                  ; <double> [#uses=1]
  %tmp392 = fsub double 1.000000e+00, %tmp391     ; <double> [#uses=1]
  %tmp393 = load double* %Kroi                    ; <double> [#uses=1]
  %tmp394 = fmul double %tmp392, %tmp393          ; <double> [#uses=1]
  %tmp395 = fadd double %tmp390, %tmp394          ; <double> [#uses=1]
  store double %tmp395, double addrspace(1)* %arrayidx387
  %tmp396 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp397 = load i32* %idxIU                      ; <i32> [#uses=1]
  %arrayidx398 = getelementptr double addrspace(1)* %tmp396, i32 %tmp397 ; <double addrspace(1)*> [#uses=1]
  %tmp399 = load double* %Kfraci                  ; <double> [#uses=1]
  %tmp400 = load double* %Kustari                 ; <double> [#uses=1]
  %tmp401 = fmul double %tmp399, %tmp400          ; <double> [#uses=1]
  %tmp402 = load double* %Kfraci                  ; <double> [#uses=1]
  %tmp403 = fsub double 1.000000e+00, %tmp402     ; <double> [#uses=1]
  %tmp404 = load double* %Kuoi                    ; <double> [#uses=1]
  %tmp405 = fmul double %tmp403, %tmp404          ; <double> [#uses=1]
  %tmp406 = fadd double %tmp401, %tmp405          ; <double> [#uses=1]
  store double %tmp406, double addrspace(1)* %arrayidx398
  %tmp407 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp408 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx409 = getelementptr double addrspace(1)* %tmp407, i32 %tmp408 ; <double addrspace(1)*> [#uses=1]
  %tmp410 = load double* %Kfraci                  ; <double> [#uses=1]
  %tmp411 = load double* %Kpstari                 ; <double> [#uses=1]
  %tmp412 = fmul double %tmp410, %tmp411          ; <double> [#uses=1]
  %tmp413 = load double* %Kfraci                  ; <double> [#uses=1]
  %tmp414 = fsub double 1.000000e+00, %tmp413     ; <double> [#uses=1]
  %tmp415 = load double* %Kpoi                    ; <double> [#uses=1]
  %tmp416 = fmul double %tmp414, %tmp415          ; <double> [#uses=1]
  %tmp417 = fadd double %tmp412, %tmp416          ; <double> [#uses=1]
  store double %tmp417, double addrspace(1)* %arrayidx409
  %tmp418 = load double* %Kspouti                 ; <double> [#uses=1]
  %cmp419 = fcmp olt double %tmp418, 0.000000e+00 ; <i1> [#uses=1]
  br i1 %cmp419, label %if.then422, label %if.end421

if.then362:                                       ; preds = %if.end285
  %tmp363 = load double* %Kushocki                ; <double> [#uses=1]
  store double %tmp363, double* %Kspini
  %tmp364 = load double* %Kushocki                ; <double> [#uses=1]
  store double %tmp364, double* %Kspouti
  br label %if.end361

if.end421:                                        ; preds = %if.then422, %if.end361
  %tmp435 = load double* %Kspini                  ; <double> [#uses=1]
  %cmp436 = fcmp ogt double %tmp435, 0.000000e+00 ; <i1> [#uses=1]
  br i1 %cmp436, label %if.then439, label %if.end438

if.then422:                                       ; preds = %if.end361
  %tmp423 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp424 = load i32* %idxID                      ; <i32> [#uses=1]
  %arrayidx425 = getelementptr double addrspace(1)* %tmp423, i32 %tmp424 ; <double addrspace(1)*> [#uses=1]
  %tmp426 = load double* %Kroi                    ; <double> [#uses=1]
  store double %tmp426, double addrspace(1)* %arrayidx425
  %tmp427 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp428 = load i32* %idxIU                      ; <i32> [#uses=1]
  %arrayidx429 = getelementptr double addrspace(1)* %tmp427, i32 %tmp428 ; <double addrspace(1)*> [#uses=1]
  %tmp430 = load double* %Kuoi                    ; <double> [#uses=1]
  store double %tmp430, double addrspace(1)* %arrayidx429
  %tmp431 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp432 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx433 = getelementptr double addrspace(1)* %tmp431, i32 %tmp432 ; <double addrspace(1)*> [#uses=1]
  %tmp434 = load double* %Kpoi                    ; <double> [#uses=1]
  store double %tmp434, double addrspace(1)* %arrayidx433
  br label %if.end421

if.end438:                                        ; preds = %if.then439, %if.end421
  %tmp452 = load double addrspace(1)** %sgnm.addr ; <double addrspace(1)*> [#uses=1]
  %tmp453 = load i64* %i                          ; <i64> [#uses=1]
  %conv454 = trunc i64 %tmp453 to i32             ; <i32> [#uses=1]
  %arrayidx455 = getelementptr double addrspace(1)* %tmp452, i32 %conv454 ; <double addrspace(1)*> [#uses=1]
  %tmp456 = load double addrspace(1)* %arrayidx455 ; <double> [#uses=1]
  %cmp457 = fcmp oeq double %tmp456, 1.000000e+00 ; <i1> [#uses=1]
  br i1 %cmp457, label %if.then460, label %if.else461

if.then439:                                       ; preds = %if.end421
  %tmp440 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp441 = load i32* %idxID                      ; <i32> [#uses=1]
  %arrayidx442 = getelementptr double addrspace(1)* %tmp440, i32 %tmp441 ; <double addrspace(1)*> [#uses=1]
  %tmp443 = load double* %Krstari                 ; <double> [#uses=1]
  store double %tmp443, double addrspace(1)* %arrayidx442
  %tmp444 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp445 = load i32* %idxIU                      ; <i32> [#uses=1]
  %arrayidx446 = getelementptr double addrspace(1)* %tmp444, i32 %tmp445 ; <double addrspace(1)*> [#uses=1]
  %tmp447 = load double* %Kustari                 ; <double> [#uses=1]
  store double %tmp447, double addrspace(1)* %arrayidx446
  %tmp448 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp449 = load i32* %idxIP                      ; <i32> [#uses=1]
  %arrayidx450 = getelementptr double addrspace(1)* %tmp448, i32 %tmp449 ; <double addrspace(1)*> [#uses=1]
  %tmp451 = load double* %Kpstari                 ; <double> [#uses=1]
  store double %tmp451, double addrspace(1)* %arrayidx450
  br label %if.end438

if.end459:                                        ; preds = %if.else461, %if.then460
  br label %return

if.then460:                                       ; preds = %if.end438
  %tmp462 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp463 = load i32* %idxIV                      ; <i32> [#uses=1]
  %arrayidx464 = getelementptr double addrspace(1)* %tmp462, i32 %tmp463 ; <double addrspace(1)*> [#uses=1]
  %tmp465 = load double addrspace(1)** %qleft.addr ; <double addrspace(1)*> [#uses=1]
  %tmp466 = load i32* %idxIV                      ; <i32> [#uses=1]
  %arrayidx467 = getelementptr double addrspace(1)* %tmp465, i32 %tmp466 ; <double addrspace(1)*> [#uses=1]
  %tmp468 = load double addrspace(1)* %arrayidx467 ; <double> [#uses=1]
  store double %tmp468, double addrspace(1)* %arrayidx464
  br label %if.end459

if.else461:                                       ; preds = %if.end438
  %tmp469 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp470 = load i32* %idxIV                      ; <i32> [#uses=1]
  %arrayidx471 = getelementptr double addrspace(1)* %tmp469, i32 %tmp470 ; <double addrspace(1)*> [#uses=1]
  %tmp472 = load double addrspace(1)** %qright.addr ; <double addrspace(1)*> [#uses=1]
  %tmp473 = load i32* %idxIV                      ; <i32> [#uses=1]
  %arrayidx474 = getelementptr double addrspace(1)* %tmp472, i32 %tmp473 ; <double addrspace(1)*> [#uses=1]
  %tmp475 = load double addrspace(1)* %arrayidx474 ; <double> [#uses=1]
  store double %tmp475, double addrspace(1)* %arrayidx471
  br label %if.end459
}

define void @__OpenCL_Loop10KcuRiemann_kernel(%struct._Args addrspace(1)* %K, double addrspace(1)* %qleft, double addrspace(1)* %qright, double addrspace(1)* %sgnm, double addrspace(1)* %qgdnv, i64 %Knarray, i64 %Knvar, i64 %KHnxyt) nounwind {
entry:
  %K.addr = alloca %struct._Args addrspace(1)*, align 4 ; <%struct._Args addrspace(1)**> [#uses=1]
  %qleft.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %qright.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=2]
  %sgnm.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=3]
  %qgdnv.addr = alloca double addrspace(1)*, align 4 ; <double addrspace(1)**> [#uses=3]
  %Knarray.addr = alloca i64, align 8             ; <i64*> [#uses=2]
  %Knvar.addr = alloca i64, align 8               ; <i64*> [#uses=2]
  %KHnxyt.addr = alloca i64, align 8              ; <i64*> [#uses=2]
  %invar = alloca i64, align 8                    ; <i64*> [#uses=5]
  %i = alloca i64, align 8                        ; <i64*> [#uses=5]
  %Hnxyt = alloca i64, align 8                    ; <i64*> [#uses=2]
  %idxIN = alloca i32, align 4                    ; <i32*> [#uses=5]
  store %struct._Args addrspace(1)* %K, %struct._Args addrspace(1)** %K.addr
  store double addrspace(1)* %qleft, double addrspace(1)** %qleft.addr
  store double addrspace(1)* %qright, double addrspace(1)** %qright.addr
  store double addrspace(1)* %sgnm, double addrspace(1)** %sgnm.addr
  store double addrspace(1)* %qgdnv, double addrspace(1)** %qgdnv.addr
  store i64 %Knarray, i64* %Knarray.addr
  store i64 %Knvar, i64* %Knvar.addr
  store i64 %KHnxyt, i64* %KHnxyt.addr
  %call = call i32 @get_global_id(i32 0) nounwind ; <i32> [#uses=1]
  %conv = zext i32 %call to i64                   ; <i64> [#uses=1]
  store i64 %conv, i64* %i
  %tmp = load i64* %KHnxyt.addr                   ; <i64> [#uses=1]
  store i64 %tmp, i64* %Hnxyt
  %tmp1 = load i64* %i                            ; <i64> [#uses=1]
  %tmp2 = load i64* %Knarray.addr                 ; <i64> [#uses=1]
  %cmp = icmp sge i64 %tmp1, %tmp2                ; <i1> [#uses=1]
  br i1 %cmp, label %if.then, label %if.end

return:                                           ; preds = %for.exit, %if.then
  ret void

if.end:                                           ; preds = %entry
  store i64 4, i64* %invar
  br label %for.cond

if.then:                                          ; preds = %entry
  br label %return

for.cond:                                         ; preds = %for.inc, %if.end
  %tmp3 = load i64* %invar                        ; <i64> [#uses=1]
  %tmp4 = load i64* %Knvar.addr                   ; <i64> [#uses=1]
  %cmp5 = icmp slt i64 %tmp3, %tmp4               ; <i1> [#uses=1]
  br i1 %cmp5, label %for.body, label %for.exit

for.exit:                                         ; preds = %for.cond
  br label %return

for.body:                                         ; preds = %for.cond
  %tmp6 = load i64* %i                            ; <i64> [#uses=1]
  %conv7 = trunc i64 %tmp6 to i32                 ; <i32> [#uses=1]
  %tmp8 = load i64* %invar                        ; <i64> [#uses=1]
  %conv9 = trunc i64 %tmp8 to i32                 ; <i32> [#uses=1]
  %tmp10 = load i64* %Hnxyt                       ; <i64> [#uses=1]
  %conv11 = trunc i64 %tmp10 to i32               ; <i32> [#uses=1]
  %tmp12 = mul i32 %conv9, %conv11                ; <i32> [#uses=1]
  %tmp13 = add i32 %conv7, %tmp12                 ; <i32> [#uses=1]
  store i32 %tmp13, i32* %idxIN
  %tmp14 = load double addrspace(1)** %sgnm.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp15 = load i64* %i                           ; <i64> [#uses=1]
  %conv16 = trunc i64 %tmp15 to i32               ; <i32> [#uses=1]
  %arrayidx = getelementptr double addrspace(1)* %tmp14, i32 %conv16 ; <double addrspace(1)*> [#uses=1]
  %tmp17 = load double addrspace(1)* %arrayidx    ; <double> [#uses=1]
  %cmp18 = fcmp oeq double %tmp17, 1.000000e+00   ; <i1> [#uses=1]
  br i1 %cmp18, label %if.then20, label %if.end19

for.inc:                                          ; preds = %if.end34
  %tmp43 = load i64* %invar                       ; <i64> [#uses=1]
  %tmp44 = add i64 %tmp43, 1                      ; <i64> [#uses=1]
  store i64 %tmp44, i64* %invar
  br label %for.cond

if.end19:                                         ; preds = %if.then20, %for.body
  %tmp28 = load double addrspace(1)** %sgnm.addr  ; <double addrspace(1)*> [#uses=1]
  %tmp29 = load i64* %i                           ; <i64> [#uses=1]
  %conv30 = trunc i64 %tmp29 to i32               ; <i32> [#uses=1]
  %arrayidx31 = getelementptr double addrspace(1)* %tmp28, i32 %conv30 ; <double addrspace(1)*> [#uses=1]
  %tmp32 = load double addrspace(1)* %arrayidx31  ; <double> [#uses=1]
  %cmp33 = fcmp une double %tmp32, 1.000000e+00   ; <i1> [#uses=1]
  br i1 %cmp33, label %if.then35, label %if.end34

if.then20:                                        ; preds = %for.body
  %tmp21 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp22 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx23 = getelementptr double addrspace(1)* %tmp21, i32 %tmp22 ; <double addrspace(1)*> [#uses=1]
  %tmp24 = load double addrspace(1)** %qleft.addr ; <double addrspace(1)*> [#uses=1]
  %tmp25 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx26 = getelementptr double addrspace(1)* %tmp24, i32 %tmp25 ; <double addrspace(1)*> [#uses=1]
  %tmp27 = load double addrspace(1)* %arrayidx26  ; <double> [#uses=1]
  store double %tmp27, double addrspace(1)* %arrayidx23
  br label %if.end19

if.end34:                                         ; preds = %if.then35, %if.end19
  br label %for.inc

if.then35:                                        ; preds = %if.end19
  %tmp36 = load double addrspace(1)** %qgdnv.addr ; <double addrspace(1)*> [#uses=1]
  %tmp37 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx38 = getelementptr double addrspace(1)* %tmp36, i32 %tmp37 ; <double addrspace(1)*> [#uses=1]
  %tmp39 = load double addrspace(1)** %qright.addr ; <double addrspace(1)*> [#uses=1]
  %tmp40 = load i32* %idxIN                       ; <i32> [#uses=1]
  %arrayidx41 = getelementptr double addrspace(1)* %tmp39, i32 %tmp40 ; <double addrspace(1)*> [#uses=1]
  %tmp42 = load double addrspace(1)* %arrayidx41  ; <double> [#uses=1]
  store double %tmp42, double addrspace(1)* %arrayidx38
  br label %if.end34
}
