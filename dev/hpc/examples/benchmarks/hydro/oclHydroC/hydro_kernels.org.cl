#pragma OPENCL EXTENSION cl_khr_fp64 : enable
/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#define ID     (0)
#define IU     (1)
#define IV     (2)
#define IP     (3)
#define ExtraLayer    (2)
#define ExtraLayerTot (2 * 2)

inline void
idx2d(long *x, long *y, const long nx)
{
  long i1d = get_global_id(0);
  *y = i1d / nx;
  *x = i1d - *y * nx;
}

#define Square(x) pown((x), 2)
#define one 1.0
#define two 2.0
#define demi 0.5
#define zero 0.0

// const double one = 1.0;
// const double two = 2.0;
// const double demi = 0.5;
// const double zero = 0.;

// #define IHVW(i,v) ((i) + (v) * Hnxyt)
// #define IHU(i, j, v)  ((i) + Hnxt * ((j) + Hnyt * (v)))
// #define IHV(i, j, v)  ((i) + Hnxt * ((j) + Hnyt * (v)))
inline size_t
IHVW(int i, int v, int Hnxyt)
{
  return (i) + (v) * Hnxyt;
}
inline size_t
IHU(int i, int j, int v, int Hnxt, int Hnyt)
{
  return (i) + Hnxt * ((j) + Hnyt * (v));
}
inline size_t
IHV(int i, int j, int v, int Hnxt, int Hnyt)
{
  return (i) + Hnxt * ((j) + Hnyt * (v));
}

__kernel void
Loop1KcuCmpflx(__global double *qgdnv, __global double *flux, const long narray, const long Hnxyt, const double Hgamma)
{
  double entho = 0, ekin = 0, etot = 0;
  long i = get_global_id(0);
  if (i >= narray)
    return;

  size_t idxID = IHVW(i, ID, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);

  entho = one / (Hgamma - one);
  // Mass density
  flux[idxID] = qgdnv[idxID] * qgdnv[idxIU];
  // Normal momentum
  flux[idxIV] = flux[idxID] * qgdnv[idxIU] + qgdnv[idxIP];
  // Transverse momentum 1
  flux[idxIV] = flux[idxID] * qgdnv[idxIV];
  // Total energy
  ekin = demi * qgdnv[idxIP] * (pown(qgdnv[idxIU], 2) + pown(qgdnv[idxIV], 2));
  etot = qgdnv[idxIU] * entho + ekin;
  flux[idxIP] = qgdnv[idxIU] * (etot + qgdnv[idxIP]);
}


__kernel void
Loop2KcuCmpflx(__global double *qgdnv, __global double *flux, const long narray, const long Hnxyt, const long Hnvar)
{
  long IN, i = get_global_id(0);
  if (i >= narray)
    return;

  for (IN = IP + 1; IN < Hnvar; IN++) {
    size_t idxIN = IHVW(i, IN, Hnxyt);
    flux[idxIN] = flux[idxIN] * qgdnv[idxIN];
  }
}

__kernel void
LoopKQEforRow(const long j, __global double *uold, __global double *q, __global double *e, const double Hsmallr,
              const long Hnxt, const long Hnyt, const long Hnxyt, const long n)
{
  double eken;
  long i = get_global_id(0);

  if (i >= n)
    return;

  long idxuID = IHV(i + ExtraLayer, j, ID, Hnxt, Hnyt);
  long idxuIU = IHV(i + ExtraLayer, j, IU, Hnxt, Hnyt);
  long idxuIV = IHV(i + ExtraLayer, j, IV, Hnxt, Hnyt);
  long idxuIP = IHV(i + ExtraLayer, j, IP, Hnxt, Hnyt);

  size_t idxID = IHVW(i, ID, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);


  q[idxID] = max(uold[idxuID], Hsmallr);
  q[idxIU] = uold[idxuIU] / q[idxID];
  q[idxIV] = uold[idxuIV] / q[idxID];
  eken = demi * (Square(q[idxIU]) + Square(q[idxIV]));
  q[idxIP] = uold[idxuIP] / q[idxID] - eken;
  e[i] = q[idxIP];
}

__kernel void
LoopKcourant(__global double *q, __global double *courant, const double Hsmallc, __global const double *c,
             const long Hnxyt, const long n)
{
  double cournox, cournoy, courantl;
  long i = get_global_id(0);

  if (i >= n)
    return;

  size_t idxID = IHVW(i, ID, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);

  cournox = cournoy = 0.;

  cournox = c[i] + fabs(q[idxIU]);
  cournoy = c[i] + fabs(q[idxIV]);
  courantl = max(cournox, max(cournoy, Hsmallc));
  courant[i] = max(courant[i], courantl);
}


__kernel void
Loop1KcuGather(__global double *uold,
               __global double *u,
               const long rowcol, const long Hnxt, const long Himin, const long Himax, const long Hnyt, const long Hnxyt)
{
  long i = get_global_id(0);
  if (i < Himin)
    return;
  if (i >= Himax)
    return;

  size_t idxID = IHVW(i, ID, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);

  u[idxID] = uold[IHU(i, rowcol, ID, Hnxt, Hnyt)];
  u[idxIU] = uold[IHU(i, rowcol, IU, Hnxt, Hnyt)];
  u[idxIV] = uold[IHU(i, rowcol, IV, Hnxt, Hnyt)];
  u[idxIP] = uold[IHU(i, rowcol, IP, Hnxt, Hnyt)];
}

__kernel void
Loop2KcuGather(__global double *uold,
               __global double *u,
               const long rowcol, const long Hnxt, const long Himin, const long Himax, const long Hnyt, const long Hnxyt)
{
  long i = get_global_id(0);
  if (i < Himin)
    return;
  if (i >= Himax)
    return;

  size_t idxID = IHVW(i, ID, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);

  u[idxID] = uold[IHU(rowcol, i, ID, Hnxt, Hnyt)];
  u[idxIV] = uold[IHU(rowcol, i, IU, Hnxt, Hnyt)];
  u[idxIU] = uold[IHU(rowcol, i, IV, Hnxt, Hnyt)];
  u[idxIP] = uold[IHU(rowcol, i, IP, Hnxt, Hnyt)];
}

__kernel void
Loop3KcuGather(__global double *uold,
               __global double *u,
               const long rowcol,
               const long Hnxt, const long Himin, const long Himax, const long Hnyt, const long Hnxyt, const long Hnvar)
{
  long i = get_global_id(0);
  long ivar;

  if (i < Himin)
    return;
  if (i >= Himax)
    return;

  for (ivar = IP + 1; ivar < Hnvar; ivar++) {
    u[IHVW(i, ivar, Hnxyt)] = uold[IHU(i, rowcol, ivar, Hnxt, Hnyt)];
  }
}

__kernel void
Loop4KcuGather(__global double *uold,
               __global double *u,
               const long rowcol,
               const long Hnxt, const long Himin, const long Himax, const long Hnyt, const long Hnxyt, const long Hnvar)
{
  long i = get_global_id(0);
  long ivar;

  if (i < Himin)
    return;
  if (i >= Himax)
    return;

  // reconsiderer le calcul d'indices en supprimant la boucle sur ivar et
  // en la ventilant par thread
  for (ivar = IP + 1; ivar < Hnvar; ivar++) {
    u[IHVW(i, ivar, Hnxyt)] = uold[IHU(rowcol, i, ivar, Hnxt, Hnyt)];
  }
}

__kernel void
Loop1KcuUpdate(const long rowcol, const double dtdx,
               __global double *uold,
               __global double *u,
               __global double *flux, const long Himin, const long Himax, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  long i = get_global_id(0);


  if (i < (Himin + ExtraLayer))
    return;
  if (i >= (Himax - ExtraLayer))
    return;

  size_t idxID = IHVW(i, ID, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);


  uold[IHU(i, rowcol, ID, Hnxt, Hnyt)] = u[idxID] + (flux[IHVW(i - 2, ID, Hnxyt)] - flux[IHVW(i - 1, ID, Hnxyt)]) * dtdx;
  uold[IHU(i, rowcol, IU, Hnxt, Hnyt)] = u[idxIU] + (flux[IHVW(i - 2, IU, Hnxyt)] - flux[IHVW(i - 1, IU, Hnxyt)]) * dtdx;
  uold[IHU(i, rowcol, IV, Hnxt, Hnyt)] = u[idxIV] + (flux[IHVW(i - 2, IV, Hnxyt)] - flux[IHVW(i - 1, IV, Hnxyt)]) * dtdx;
  uold[IHU(i, rowcol, IP, Hnxt, Hnyt)] = u[idxIP] + (flux[IHVW(i - 2, IP, Hnxyt)] - flux[IHVW(i - 1, IP, Hnxyt)]) * dtdx;
}

__kernel void
Loop2KcuUpdate(const long rowcol, const double dtdx,
               __global double *uold,
               __global double *u,
               __global double *flux,
               const long Himin, const long Himax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  long i = get_global_id(0);
  long ivar;

  if (i < (Himin + ExtraLayer))
    return;
  if (i >= (Himax - ExtraLayer))
    return;

  for (ivar = IP + 1; ivar < Hnvar; ivar++) {
    uold[IHU(i, rowcol, ivar, Hnxt, Hnyt)] =
      u[IHVW(i, ivar, Hnxyt)] + (flux[IHVW(i - 2, ivar, Hnxyt)] - flux[IHVW(i - 1, ivar, Hnxyt)]) * dtdx;
  }
}

__kernel void
Loop3KcuUpdate(const long rowcol, const double dtdx,
               __global double *uold,
               __global double *u,
               __global double *flux, const long Hjmin, const long Hjmax, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  long j = get_global_id(0);

  if (j < (Hjmin + ExtraLayer))
    return;
  if (j >= (Hjmax - ExtraLayer))
    return;

  uold[IHU(rowcol, j, ID, Hnxt, Hnyt)] =
    u[IHVW(j, ID, Hnxyt)] + (flux[IHVW(j - 2, ID, Hnxyt)] - flux[IHVW(j - 1, ID, Hnxyt)]) * dtdx;
  uold[IHU(rowcol, j, IP, Hnxt, Hnyt)] =
    u[IHVW(j, IP, Hnxyt)] + (flux[IHVW(j - 2, IP, Hnxyt)] - flux[IHVW(j - 1, IP, Hnxyt)]) * dtdx;
  uold[IHU(rowcol, j, IV, Hnxt, Hnyt)] =
    u[IHVW(j, IU, Hnxyt)] + (flux[IHVW(j - 2, IU, Hnxyt)] - flux[IHVW(j - 1, IU, Hnxyt)]) * dtdx;
  uold[IHU(rowcol, j, IU, Hnxt, Hnyt)] =
    u[IHVW(j, IV, Hnxyt)] + (flux[IHVW(j - 2, IV, Hnxyt)] - flux[IHVW(j - 1, IV, Hnxyt)]) * dtdx;
}

__kernel void
Loop4KcuUpdate(const long rowcol, const double dtdx,
               __global double *uold,
               __global double *u,
               __global double *flux,
               const long Hjmin, const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  long j = get_global_id(0);
  long ivar;

  if (j < (Hjmin + ExtraLayer))
    return;
  if (j >= (Hjmax - ExtraLayer))
    return;

  for (ivar = IP + 1; ivar < Hnvar; ivar++) {
    uold[IHU(rowcol, j, ivar, Hnxt, Hnyt)] =
      u[IHVW(j, ivar, Hnxyt)] + (flux[IHVW(j - 2, ivar, Hnxyt)] - flux[IHVW(j - 1, ivar, Hnxyt)]) * dtdx;
  }
}


__kernel void
Loop1KcuConstoprim(const long n,
                   __global double *u, __global double *q, __global double *e, const long Hnxyt, const double Hsmallr)
{
  double eken;
  long i = get_global_id(0);
  if (i >= n)
    return;

  size_t idxID = IHVW(i, ID, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);

  q[idxID] = max(u[idxID], Hsmallr);
  q[idxIU] = u[idxIU] / q[idxID];
  q[idxIV] = u[idxIV] / q[idxID];
  eken = demi * (Square(q[idxIU]) + Square(q[idxIV]));
  q[idxIP] = u[idxIP] / q[idxID] - eken;
  e[i] = q[idxIP];
}

__kernel void
Loop2KcuConstoprim(const long n, __global double *u, __global double *q, const long Hnxyt, const long Hnvar)
{
  long IN;
  long i = get_global_id(0);
  if (i >= n)
    return;

  for (IN = IP + 1; IN < Hnvar; IN++) {
    size_t idxIN = IHVW(i, IN, Hnxyt);
    q[idxIN] = u[idxIN] / q[idxIN];
  }
}

__kernel void
LoopEOS(__global double *q, __global double *eint,
        __global double *c, 
	const long offsetIP, 
	const long offsetID, 
	const long imin, const long imax, const double Hsmallc, const double Hgamma)
{
  double smallp;
  __global double *p = &q[offsetIP];
  __global double *rho = &q[offsetID];
  long k = get_global_id(0);

  if (k < imin)
    return;
  if (k >= imax)
    return;

  smallp = Square(Hsmallc) / Hgamma;
  p[k] = (Hgamma - one) * rho[k] * eint[k];
  p[k] = max(p[k], (double) (rho[k] * smallp));
  c[k] = sqrt(Hgamma * p[k] / rho[k]);
}

__kernel void
Loop1KcuMakeBoundary(const long i, const long i0, const double sign, const long Hjmin,
                     const long nx, const long Hnxt, const long Hnyt, const long Hnvar, __global double *uold)
{
  long j, ivar;
  double vsign = sign;

  idx2d(&j, &ivar, nx);
  if (ivar >= Hnvar)
    return;

  // recuperation de la conditon qui etait dans la boucle
  if (ivar == IU)
    vsign = 1.0;

  j += (Hjmin + ExtraLayer);
  uold[IHV(i, j, ivar, Hnxt, Hnyt)] = uold[IHV(i0, j, ivar, Hnxt, Hnyt)] * vsign;
}

__kernel void
Loop2KcuMakeBoundary(const long j, const long j0, const double sign, const long Himin,
                     const long nx, const long Hnxt, const long Hnyt, const long Hnvar, __global double *uold)
{
  long i, ivar;
  double vsign = sign;

  idx2d(&i, &ivar, nx);
  if (ivar >= Hnvar)
    return;

  // recuperation de la conditon qui etait dans la boucle
  if (ivar == IV)
    vsign = 1.0;

  i += (Himin + ExtraLayer);
  uold[IHV(i, j, ivar, Hnxt, Hnyt)] = uold[IHV(i, j0, ivar, Hnxt, Hnyt)] * vsign;
}


__kernel void
Loop1KcuQleftright(const long bmax, const long Hnvar, const long Hnxyt,
                   __global double *qxm, __global double *qxp, __global double *qleft, __global double *qright)
{
  long nvar;
  long i = get_global_id(0);
  if (i >= bmax)
    return;

  for (nvar = 0; nvar < Hnvar; nvar++) {
    qleft[IHVW(i, nvar, Hnxyt)] = qxm[IHVW(i + 1, nvar, Hnxyt)];
    qright[IHVW(i, nvar, Hnxyt)] = qxp[IHVW(i + 2, nvar, Hnxyt)];
  }
}

typedef struct _Args {
  __global double *qleft;
  __global double *qright;
  __global double *qgdnv;
  __global double *rl;
  __global double *ul;
  __global double *pl;
  __global double *cl;
  __global double *wl;
  __global double *rr;
  __global double *ur;
  __global double *pr;
  __global double *cr;
  __global double *wr;
  __global double *ro;
  __global double *uo;
  __global double *po;
  __global double *co;
  __global double *wo;
  __global double *rstar;
  __global double *ustar;
  __global double *pstar;
  __global double *cstar;
  __global long *sgnm;
  __global double *spin;
  __global double *spout;
  __global double *ushock;
  __global double *frac;
  __global double *scr;
  __global double *delp;
  __global double *pold;
  __global long *ind;
  __global long *ind2;
  long narray;
  double Hsmallr;
  double Hsmallc;
  double Hgamma;
  long Hniter_riemann;
  long Hnvar;
  long Hnxyt;
} Args_t;

// memoire sur le device qui va contenir les arguments de riemann
// on les transmets en bloc en une fois et les differents kernels pourront y acceder.
//__constant Args_t K;

/* 
   pour contourner les limitations d'OpenCL, nous allons utiliser 4
   kernels d'affectation des pointeurs de la structure. Methode
   bestiale et peu efficace mais qui permet de garder le code intact.

   Remarque: je suis conscient de l'inutilite des tableaux
   intermediaires qui auraient pu etre des scalaires, mais l'objectif
   est de rester (pour l'instant) au plus pres du code d'origine.
*/

__kernel void
Init1KcuRiemann(__global Args_t * K,
		__global double * qleft,
		__global double * qright,
		__global double * qgdnv,
		__global double * rl,
		__global double * ul,
		__global double * pl,
		__global double * cl,
		__global double * wl,
		__global double * rr,
		__global double * ur
		)
{
  long tid = get_global_id(0);
  if (tid != 0) return;

  K->qleft=qleft;
  K->qright=qright;
  K->qgdnv=qgdnv;
  K->rl=rl;
  K->ul=ul;
  K->pl=pl;
  K->cl=cl;
  K->wl=wl;
  K->rr=rr;
  K->ur=ur;
}
__kernel void
Init2KcuRiemann(__global Args_t * K,
		__global double * pr,
		__global double * cr,
		__global double * wr,
		__global double * ro,
		__global double * uo,
		__global double * po,
		__global double * co,
		__global double * wo,
		__global double * rstar,
		__global double * ustar
		)
{
  long tid = get_global_id(0);
  if (tid != 0) return;
  K->pr=pr;
  K->cr=cr;
  K->wr=wr;
  K->ro=ro;
  K->uo=uo;
  K->po=po;
  K->co=co;
  K->wo=wo;
  K->rstar=rstar;
  K->ustar=ustar;
}
__kernel void
Init3KcuRiemann(__global Args_t * K,
		__global double * pstar,
		__global double * cstar,
		__global long * sgnm,
		__global double * spin,
		__global double * spout,
		__global double * ushock,
		__global double * frac,
		__global double * scr,
		__global double * delp,
		__global double * pold
		)
{
  long tid = get_global_id(0);
  if (tid != 0) return;
  K->pstar=pstar;
  K->cstar=cstar;
  K->spin=spin;
  K->spout=spout;
  K->ushock=ushock;
  K->frac=frac;
  K->scr=scr;
  K->delp=delp;
  K->pold=pold;
  K->sgnm=sgnm;
}
__kernel void
Init4KcuRiemann(__global Args_t * K,
		__global long * ind,
		__global long * ind2
		)
{
  long tid = get_global_id(0);
  if (tid != 0) return;
  K->ind=ind;
  K->ind2=ind2;
}

__kernel void
Loop1KcuRiemann(__global Args_t * K)
{
  double smallp, gamma6, ql, qr, usr, usl, wwl, wwr, smallpp;
  long iter;
  long tid = get_local_id(0);
  double ulS;
  double plS;
  double clS;
  double urS;
  double prS;
  double crS;
  double uoS;
  double delpS;
  double poldS;
  long i = get_global_id(0);
  long Hnxyt = K->Hnxyt;
  if (i >= K->narray)
    return;

  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxID = IHVW(i, ID, Hnxyt);

  smallp = Square(K->Hsmallc) / K->Hgamma;

  K->rl[i] = max(K->qleft[idxID], K->Hsmallr);
  K->ul[i] = K->qleft[idxIU];
  K->pl[i] = max(K->qleft[idxIP], (double) (K->rl[i] * smallp));
  K->rr[i] = max(K->qright[idxID], K->Hsmallr);
  K->ur[i] = K->qright[idxIU];
  K->pr[i] = max(K->qright[idxIP], (double) (K->rr[i] * smallp));
  // Lagrangian sound speed
  K->cl[i] = K->Hgamma * K->pl[i] * K->rl[i];
  K->cr[i] = K->Hgamma * K->pr[i] * K->rr[i];
  // First guess
  K->wl[i] = sqrt(K->cl[i]);
  K->wr[i] = sqrt(K->cr[i]);
  K->pstar[i] =
    ((K->wr[i] * K->pl[i] + K->wl[i] * K->pr[i]) + K->wl[i] * K->wr[i] * (K->ul[i] - K->ur[i])) / (K->wl[i] + K->wr[i]);
  K->pstar[i] = max(K->pstar[i], 0.0);
  K->pold[i] = K->pstar[i];
  // ind est un masque de traitement pour le newton
  K->ind[i] = 1;                // toutes les cellules sont a traiter

  ulS = K->ul[i];
  plS = K->pl[i];
  clS = K->cl[i];
  urS = K->ur[i];
  prS = K->pr[i];
  crS = K->cr[i];
  uoS = K->uo[i];
  delpS = K->delp[i];
  poldS = K->pold[i];

  smallp = Square(K->Hsmallc) / K->Hgamma;
  smallpp = K->Hsmallr * smallp;
  gamma6 = (K->Hgamma + one) / (two * K->Hgamma);

  long indi = K->ind[i];

  for (iter = 0; iter < K->Hniter_riemann; iter++) {
    double precision = 1.e-6;
    wwl = sqrt(clS * (one + gamma6 * (poldS - plS) / plS));
    wwr = sqrt(crS * (one + gamma6 * (poldS - prS) / prS));
    ql = two * wwl * Square(wwl) / (Square(wwl) + clS);
    qr = two * wwr * Square(wwr) / (Square(wwr) + crS);
    usl = ulS - (poldS - plS) / wwl;
    usr = urS + (poldS - prS) / wwr;
    double t1 = qr * ql / (qr + ql) * (usl - usr);
    double t2 = -poldS;
    delpS = max(t1, t2);
    poldS = poldS + delpS;
    uoS = fabs(delpS / (poldS + smallpp));
    indi = uoS > precision;
    if (!indi)
      break;
  }
  // barrier(CLK_LOCAL_MEM_FENCE);
  K->uo[i] = uoS;
  K->pold[i] = poldS;

  gamma6 = (K->Hgamma + one) / (two * K->Hgamma);

  K->pstar[i] = K->pold[i];
  K->wl[i] = sqrt(K->cl[i] * (one + gamma6 * (K->pstar[i] - K->pl[i]) / K->pl[i]));
  K->wr[i] = sqrt(K->cr[i] * (one + gamma6 * (K->pstar[i] - K->pr[i]) / K->pr[i]));

  K->ustar[i] = demi * (K->ul[i] + (K->pl[i] - K->pstar[i]) / K->wl[i] + K->ur[i] - (K->pr[i] - K->pstar[i]) / K->wr[i]);
  K->sgnm[i] = (K->ustar[i] > 0) ? 1 : -1;
  if (K->sgnm[i] == 1) {
    K->ro[i] = K->rl[i];
    K->uo[i] = K->ul[i];
    K->po[i] = K->pl[i];
    K->wo[i] = K->wl[i];
  } else {
    K->ro[i] = K->rr[i];
    K->uo[i] = K->ur[i];
    K->po[i] = K->pr[i];
    K->wo[i] = K->wr[i];
  }
  K->co[i] = max(K->Hsmallc, sqrt(fabs(K->Hgamma * K->po[i] / K->ro[i])));
  K->rstar[i] = K->ro[i] / (one + K->ro[i] * (K->po[i] - K->pstar[i]) / Square(K->wo[i]));
  K->rstar[i] = max(K->rstar[i], K->Hsmallr);
  K->cstar[i] = max(K->Hsmallc, sqrt(fabs(K->Hgamma * K->pstar[i] / K->rstar[i])));
  K->spout[i] = K->co[i] - K->sgnm[i] * K->uo[i];
  K->spin[i] = K->cstar[i] - K->sgnm[i] * K->ustar[i];
  K->ushock[i] = K->wo[i] / K->ro[i] - K->sgnm[i] * K->uo[i];
  if (K->pstar[i] >= K->po[i]) {
    K->spin[i] = K->ushock[i];
    K->spout[i] = K->ushock[i];
  }
  K->scr[i] = max((double) (K->spout[i] - K->spin[i]), (double) (K->Hsmallc + fabs(K->spout[i] + K->spin[i])));
  K->frac[i] = (one + (K->spout[i] + K->spin[i]) / K->scr[i]) * demi;
  K->frac[i] = max(zero, (double) (min(one, K->frac[i])));

  K->qgdnv[idxID] = K->frac[i] * K->rstar[i] + (one - K->frac[i]) * K->ro[i];
  K->qgdnv[idxIU] = K->frac[i] * K->ustar[i] + (one - K->frac[i]) * K->uo[i];
  K->qgdnv[idxIP] = K->frac[i] * K->pstar[i] + (one - K->frac[i]) * K->po[i];

  if (K->spout[i] < zero) {
    K->qgdnv[idxID] = K->ro[i];
    K->qgdnv[idxIU] = K->uo[i];
    K->qgdnv[idxIP] = K->po[i];
  }
  if (K->spin[i] > zero) {
    K->qgdnv[idxID] = K->rstar[i];
    K->qgdnv[idxIU] = K->ustar[i];
    K->qgdnv[idxIP] = K->pstar[i];
  }

  if (K->sgnm[i] == 1) {
    K->qgdnv[idxIV] = K->qleft[idxIV];
  } else {
    K->qgdnv[idxIV] = K->qright[idxIV];
  }
}

__kernel void
Loop10KcuRiemann(__global Args_t * K)
{
  long invar;
  long i = get_global_id(0);
  long Hnxyt = K->Hnxyt;
  if (i >= K->narray)
    return;

  for (invar = IP + 1; invar < K->Hnvar; invar++) {
    size_t idxIN = IHVW(i, invar, Hnxyt);
    if (K->sgnm[i] == 1) {
      K->qgdnv[idxIN] = K->qleft[idxIN];
    }
    if (K->sgnm[i] != 1) {
      K->qgdnv[idxIN] = K->qright[idxIN];
    }
  }
}

__kernel void
LoopKcuSlope(__global double *q, __global double *dq,
             const long Hnvar, const long Hnxyt, const double slope_type, const long ijmin, const long ijmax)
{
  long n;
  double dlft, drgt, dcen, dsgn, slop, dlim;
  long ihvwin, ihvwimn, ihvwipn;

  long i;
  idx2d(&i, &n, (ijmax - ijmin));

  if (n >= Hnvar)
    return;

  i = i + ijmin;

  ihvwin = IHVW(i, n, Hnxyt);
  ihvwimn = IHVW(i - 1, n, Hnxyt);
  ihvwipn = IHVW(i + 1, n, Hnxyt);
  dlft = slope_type * (q[ihvwin] - q[ihvwimn]);
  drgt = slope_type * (q[ihvwipn] - q[ihvwin]);
  dcen = demi * (dlft + drgt) / slope_type;
  dsgn = (dcen > 0) ? (double) 1.0 : (double) -1.0;     // sign(one, dcen);
  slop = (double) min(fabs(dlft), fabs(drgt));
  dlim = ((dlft * drgt) <= zero) ? zero : slop;
  //         if ((dlft * drgt) <= zero) {
  //             dlim = zero;
  //         }
  dq[ihvwin] = dsgn * (double) min(dlim, fabs(dcen));
}

__kernel void
Loop1KcuTrace(__global double *q, __global double *dq, __global double *c,
              __global double *qxm, __global double *qxp,
              const double dtdx, const long Hnxyt,
              const long imin, const long imax, const double zeror, const double zerol, const double project)
{
  double cc, csq, r, u, v, p;
  double dr, du, dv, dp;
  double alpham, alphap, alpha0r, alpha0v;
  double spminus, spplus, spzero;
  double apright, amright, azrright, azv1right;
  double apleft, amleft, azrleft, azv1left;

  long i = get_global_id(0);
  if (i < imin)
    return;
  if (i >= imax)
    return;

  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxID = IHVW(i, ID, Hnxyt);

  cc = c[i];
  csq = Square(cc);
  r = q[idxID];
  u = q[idxIU];
  v = q[idxIV];
  p = q[idxIP];
  dr = dq[idxID];
  du = dq[idxIU];
  dv = dq[idxIV];
  dp = dq[idxIP];
  alpham = demi * (dp / (r * cc) - du) * r / cc;
  alphap = demi * (dp / (r * cc) + du) * r / cc;
  alpha0r = dr - dp / csq;
  alpha0v = dv;

  // Right state
  spminus = (u - cc) * dtdx + one;
  spplus = (u + cc) * dtdx + one;
  spzero = u * dtdx + one;
  if ((u - cc) >= zeror) {
    spminus = project;
  }
  if ((u + cc) >= zeror) {
    spplus = project;
  }
  if (u >= zeror) {
    spzero = project;
  }
  apright = -demi * spplus * alphap;
  amright = -demi * spminus * alpham;
  azrright = -demi * spzero * alpha0r;
  azv1right = -demi * spzero * alpha0v;
  qxp[idxID] = r + (apright + amright + azrright);
  qxp[idxIU] = u + (apright - amright) * cc / r;
  qxp[idxIV] = v + (azv1right);
  qxp[idxIP] = p + (apright + amright) * csq;

  // Left state
  spminus = (u - cc) * dtdx - one;
  spplus = (u + cc) * dtdx - one;
  spzero = u * dtdx - one;
  if ((u - cc) <= zerol) {
    spminus = -project;
  }
  if ((u + cc) <= zerol) {
    spplus = -project;
  }
  if (u <= zerol) {
    spzero = -project;
  }
  apleft = -demi * spplus * alphap;
  amleft = -demi * spminus * alpham;
  azrleft = -demi * spzero * alpha0r;
  azv1left = -demi * spzero * alpha0v;
  qxm[idxID] = r + (apleft + amleft + azrleft);
  qxm[idxIU] = u + (apleft - amleft) * cc / r;
  qxm[idxIV] = v + (azv1left);
  qxm[idxIP] = p + (apleft + amleft) * csq;
}

__kernel void
Loop2KcuTrace(__global double *q, __global double *dq,
              __global double *qxm, __global double *qxp,
              const double dtdx, const long Hnvar, const long Hnxyt,
              const long imin, const long imax, const double zeror, const double zerol, const double project)
{
  long IN;
  double u, a;
  double da;
  double spzero;
  double acmpright;
  double acmpleft;

  long i = get_global_id(0);
  if (i < imin)
    return;
  if (i >= imax)
    return;

  size_t idxIU = IHVW(i, IU, Hnxyt);
  size_t idxIV = IHVW(i, IV, Hnxyt);
  size_t idxIP = IHVW(i, IP, Hnxyt);
  size_t idxID = IHVW(i, ID, Hnxyt);

  for (IN = IP + 1; IN < Hnvar; IN++) {
    size_t idxIN = IHVW(i, IN, Hnxyt);
    u = q[idxIU];
    a = q[idxIN];
    da = dq[idxIN];

    // Right state
    spzero = u * dtdx + one;
    if (u >= zeror) {
      spzero = project;
    }
    acmpright = -demi * spzero * da;
    qxp[idxIN] = a + acmpright;

    // Left state
    spzero = u * dtdx - one;
    if (u <= zerol) {
      spzero = -project;
    }
    acmpleft = -demi * spzero * da;
    qxm[idxIN] = a + acmpleft;
  }
}

__kernel void
LoopKredMaxDble(__global double *src, __global double *res, const long nb)
{
  __local double sdata[512];
  long blockSize = get_local_size(0);
  long tidL = get_local_id(0);
  long myblock = get_group_id(0);
  long i = get_global_id(0);

  // protection pour les cas ou on n'est pas multiple du block
  // par defaut le max est le premier element
  sdata[tidL] = src[0];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (i < nb) {
    sdata[tidL] = src[i];
  }
  barrier(CLK_LOCAL_MEM_FENCE);

  // do the reduction in parallel
  if (blockSize >= 512) {
    if (tidL < 256) {
      sdata[tidL] = max(sdata[tidL], sdata[tidL + 256]);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  if (blockSize >= 256) {
    if (tidL < 128) {
      sdata[tidL] = max(sdata[tidL], sdata[tidL + 128]);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  if (blockSize >= 128) {
    if (tidL < 64) {
      sdata[tidL] = max(sdata[tidL], sdata[tidL + 64]);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  if (tidL < 32) {
    if (blockSize >= 64) {
      sdata[tidL] = max(sdata[tidL], sdata[tidL + 32]);
      barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (blockSize >= 32) {
      sdata[tidL] = max(sdata[tidL], sdata[tidL + 16]);
      barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (blockSize >= 16) {
      sdata[tidL] = max(sdata[tidL], sdata[tidL + 8]);
      barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (blockSize >= 8) {
      sdata[tidL] = max(sdata[tidL], sdata[tidL + 4]);
      barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (blockSize >= 4) {
      sdata[tidL] = max(sdata[tidL], sdata[tidL + 2]);
      barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (blockSize >= 2) {
      sdata[tidL] = max(sdata[tidL], sdata[tidL + 1]);
      barrier(CLK_LOCAL_MEM_FENCE);
    }
  }
  // get the partial result from this block
  if (tidL == 0) {
    res[myblock] = sdata[0];
  }
}

__kernel void
KernelMemset(__global int *a, int v, long lbyte)
{
  size_t gid = get_global_id(0);
  if (gid * sizeof(int) >= lbyte)
    return;
  a[gid] = v;
}

__kernel void
KernelMemsetV4(__global int4 * a, int v, long lobj)
{
  size_t gid = get_global_id(0) * 4;
  if (gid >= lobj)
    return;
  a[gid] = v;
}

//EOF
#ifdef NOTDEF
#endif //NOTDEF
