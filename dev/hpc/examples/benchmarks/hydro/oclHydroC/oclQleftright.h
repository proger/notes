#ifndef CUQLEFTRIGHT_H_INCLUDED
#define CUQLEFTRIGHT_H_INCLUDED
#include <CL/cl.h>

void
  oclQleftright(const long idim, const long Hnx, const long Hny, const long Hnxyt,
                const long Hnvar, cl_mem qxmDEV, cl_mem qxpDEV, cl_mem qleftDEV, cl_mem qrightDEV);

#endif // QLEFTRIGHT_H_INCLUDED
