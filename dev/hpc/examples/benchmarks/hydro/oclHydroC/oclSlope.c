/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "oclSlope.h"
#include "oclInit.h"
#include "ocltools.h"

void
oclSlope(cl_mem q, cl_mem dq, const long narray, const long Hnvar, const long Hnxyt, const double slope_type)
{
  long ijmin, ijmax;

  WHERE("slope");
  ijmin = 0;
  ijmax = narray;
  //   SetBlockDims(((ijmax - 1) - (ijmin + 1)) * Hnvar, THREADSSZ, block, grid);
  //   LoopKcuSlope <<< grid, block >>> (q, dq, Hnvar, Hnxyt, slope_type, ijmin, ijmax);
  //   CheckErr("LoopKcuSlope");
  //   cudaThreadSynchronize();
  OCLINITARG;
  OCLSETARG(ker[LoopKcuSlope], q);
  OCLSETARG(ker[LoopKcuSlope], dq);
  OCLSETARG(ker[LoopKcuSlope], Hnvar);
  OCLSETARG(ker[LoopKcuSlope], Hnxyt);
  OCLSETARG(ker[LoopKcuSlope], slope_type);
  OCLSETARG(ker[LoopKcuSlope], ijmin);
  OCLSETARG(ker[LoopKcuSlope], ijmax);
  oclLaunchKernel(ker[LoopKcuSlope], cqueue, ((ijmax - 1) - (ijmin + 1)) * Hnvar, THREADSSZ);
}                               // slope

#undef IHVW
//EOF
