/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <math.h>
#include <malloc.h>
// #include <unistd.h>
// #include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "oclCmpflx.h"

#include "oclInit.h"
#include "ocltools.h"

#define IHVW(i,v) ((i) + (v) * Hnxyt)

void
oclCmpflx(cl_mem qgdnv, cl_mem flux, const long narray, const long Hnxyt, const long Hnvar, const double Hgamma)
{
  cl_int err = 0;
  dim3 gws, lws;
  cl_event event;
  double elapsk;

  WHERE("cmpflx");

  // SetBlockDims(narray, THREADSSZ, block, grid);
  oclMkNDrange(narray, THREADSSZ, NDR_1D, gws, lws);

  // Compute fluxes
  // Loop1KcuCmpflx <<< grid, block >>> (qgdnv, flux, narray, Hnxyt, Hgamma);
  oclSetArg(ker[Loop1KcuCmpflx], 0, sizeof(cl_mem), &qgdnv);
  oclSetArg(ker[Loop1KcuCmpflx], 1, sizeof(cl_mem), &flux);
  oclSetArg(ker[Loop1KcuCmpflx], 2, sizeof(narray), &narray);
  oclSetArg(ker[Loop1KcuCmpflx], 3, sizeof(Hnxyt), &Hnxyt);
  oclSetArg(ker[Loop1KcuCmpflx], 4, sizeof(Hgamma), &Hgamma);

  err = clEnqueueNDRangeKernel(cqueue, ker[Loop1KcuCmpflx], 1, NULL, gws, lws, 0, NULL, &event);
  oclCheckErr(err, "clEnqueueNDRangeKernel Loop1KcuCmpflx");
  err = clWaitForEvents(1, &event);
  oclCheckErr(err, "clWaitForEvents");
  elapsk = oclChronoElaps(event);
  err = clReleaseEvent(event);
  oclCheckErr(err, "clReleaseEvent");

  // Other advected quantities
  if (Hnvar > IP + 1) {
    // Loop2KcuCmpflx <<< grid, block >>> (qgdnv, flux, narray, Hnxyt, Hnvar);
    oclSetArg(ker[Loop2KcuCmpflx], 0, sizeof(cl_mem), &qgdnv);
    oclSetArg(ker[Loop2KcuCmpflx], 1, sizeof(cl_mem), &flux);
    oclSetArg(ker[Loop2KcuCmpflx], 2, sizeof(narray), &narray);
    oclSetArg(ker[Loop2KcuCmpflx], 3, sizeof(Hnxyt), &Hnxyt);
    oclSetArg(ker[Loop2KcuCmpflx], 4, sizeof(Hnvar), &Hnvar);
    err = clEnqueueNDRangeKernel(cqueue, ker[Loop2KcuCmpflx], 1, NULL, gws, lws, 0, NULL, &event);
    oclCheckErr(err, "clEnqueueNDRangeKernel Loop1KcuCmpflx");
    err = clWaitForEvents(1, &event);
    oclCheckErr(err, "clWaitForEvents");
    elapsk = oclChronoElaps(event);
    err = clReleaseEvent(event);
    oclCheckErr(err, "clReleaseEvent");
  }
}                               // cmpflx


#undef IHVW

//EOF
