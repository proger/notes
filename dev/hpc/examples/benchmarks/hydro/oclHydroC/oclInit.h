#ifndef OCLINIT_H
#define OCLINIT_H
#include <CL/cl.h>

#define THREADSSZ 32

typedef enum {
  Loop1KcuCmpflx = 1,
  Loop2KcuCmpflx,
  LoopKQEforRow,
  LoopKcourant,
  Loop1KcuGather,
  Loop2KcuGather,
  Loop3KcuGather,
  Loop4KcuGather,
  Loop1KcuUpdate,
  Loop2KcuUpdate,
  Loop3KcuUpdate,
  Loop4KcuUpdate,
  Loop1KcuConstoprim,
  Loop2KcuConstoprim,
  LoopEOS,
  Loop1KcuMakeBoundary,
  Loop2KcuMakeBoundary,
  Loop1KcuQleftright,
  Init1KcuRiemann,
  Init2KcuRiemann,
  Init3KcuRiemann,
  Init4KcuRiemann,
  Loop1KcuRiemann,
  Loop10KcuRiemann,
  LoopKcuSlope,
  Loop1KcuTrace,
  Loop2KcuTrace,
  LoopKredMaxDble,
  KernelMemset,
  KernelMemsetV4,
  LastEntryKernel
} myKernel_t;

extern cl_command_queue cqueue;
extern cl_context ctx;
extern cl_program pgm;
extern int devselected;
extern int platformselected;

extern cl_kernel *ker;
void oclMemset(cl_mem a, cl_int v, size_t lbyte);
void oclMakeHydroKernels();
void oclInitCode();

#endif // OCLINIT_H
//EOF
