#ifndef CUCMPFLX_H_INCLUDED
#define CUCMPFLX_H_INCLUDED

#include <CL/cl.h>

#include "utils.h"

void oclCmpflx(cl_mem qgdnv, cl_mem flux, const long narray, const long Hnxyt, const long Hnvar, const double Hgamma);

#endif // CMPFLX_H_INCLUDED
