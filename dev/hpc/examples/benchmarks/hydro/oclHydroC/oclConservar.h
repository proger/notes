#ifndef CUCONSERVAR_H
#define CUCONSERVAR_H

#include <CL/cl.h>

void
  oclGatherConservativeVars(const long idim,
                            const long rowcol,
                            cl_mem uoldDEV,
                            cl_mem uDEV,
                            const long Himin,
                            const long Himax,
                            const long Hjmin,
                            const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt);

void
  oclUpdateConservativeVars(const long idim,
                            const long rowcol,
                            const double dtdx,
                            cl_mem uoldDEV,
                            cl_mem uDEV,
                            cl_mem fluxDEV,
                            const long Himin,
                            const long Himax,
                            const long Hjmin,
                            const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt);
#endif
