#ifndef CUEQUATION_OF_STATE_H_INCLUDED
#define CUEQUATION_OF_STATE_H_INCLUDED

#include "utils.h"
#include "parametres.h"
#include <CL/cl.h>

void oclEquationOfState(cl_mem qDEV, cl_mem eintDEV, cl_mem cDEV,
                        long offsetIP, long offsetID, long imin, long imax, const double Hsmallc, const double Hgamma);

#endif // EQUATION_OF_STATE_H_INCLUDED
