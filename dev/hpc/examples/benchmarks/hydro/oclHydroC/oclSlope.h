#ifndef CUSLOPE_H_INCLUDED
#define CUSLOPE_H_INCLUDED

#include <CL/cl.h>

void oclSlope(cl_mem qDEV, cl_mem dqDEV, const long narray, const long Hnvar, const long Hnxyt, const double slope_type);

#endif // SLOPE_H_INCLUDED
