#include <CL/cl.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <CL/cl.h>

#include "oclReduce.h"
#include "oclInit.h"
#include "ocltools.h"

double
oclReduceMax(cl_mem array, long nb)
{
  cl_event event;
  cl_int err;
  long bs = 64;
  long nbb = nb / bs;
  double resultat = 0;
  cl_mem tmp, temp1, temp2;
  long oldnbb = nb;
  long oldbs = bs;

  nbb = (nb + bs - 1) / bs;

//   status = cudaMalloc((void **) &temp1, nbb * sizeof(double));
//   VERIF(status, "cudaMalloc temp1");
//   status = cudaMalloc((void **) &temp2, nbb * sizeof(double));
//   VERIF(status, "cudaMalloc temp2");
  temp1 = clCreateBuffer(ctx, CL_MEM_READ_WRITE, nbb * sizeof(double), NULL, &err);
  oclCheckErr(err, "");
  temp2 = clCreateBuffer(ctx, CL_MEM_READ_WRITE, nbb * sizeof(double), NULL, &err);
  oclCheckErr(err, "");

  // on traite d'abord le tableau d'origine

  //   SetBlockDims(nb, bs, block, grid);
  //   LoopKredMaxDble <<< grid, block >>> (array, temp1, nb);
  //   CheckErr("KredMaxDble");
  //   cudaThreadSynchronize();
  //   CheckErr("reducMax");
  OCLINITARG;
  OCLSETARG(ker[LoopKredMaxDble], array);
  OCLSETARG(ker[LoopKredMaxDble], temp1);
  OCLSETARG(ker[LoopKredMaxDble], nb);
  // recuperation du nombre de blocs effectif et du block size utilise
  oclNbBlocks(ker[LoopKredMaxDble], cqueue, oldnbb, oldbs, &bs, &nbb);
  oclLaunchKernel(ker[LoopKredMaxDble], cqueue, nb, bs);

  // ici on a nbb maxima locaux
  oldnbb = nbb;
  oldbs = bs;

  while (oldnbb > 1) {
    //     SetBlockDims(nbb, bs, block, grid);
    //     LoopKredMaxDble <<< grid, block >>> (temp1, temp2, nbb);
    //     CheckErr("KredMaxDble 2");
    //     cudaThreadSynchronize();
    //     CheckErr("reducMax 2");
    OCLINITARG;
    OCLSETARG(ker[LoopKredMaxDble], temp1);
    OCLSETARG(ker[LoopKredMaxDble], temp2);
    OCLSETARG(ker[LoopKredMaxDble], nbb);
    oclLaunchKernel(ker[LoopKredMaxDble], cqueue, nbb, bs);

    // on permute les tableaux pour une eventuelle iteration suivante,
    tmp = temp1;
    temp1 = temp2;
    temp2 = tmp;
    // on rediminue la taille du probleme
    oldnbb = nbb;
    oldbs = bs;
    // recuperation du nb de blocks utilises effectivement
    oclNbBlocks(ker[LoopKredMaxDble], cqueue, oldnbb, oldbs, &bs, &nbb);
    // fprintf(stderr, "o=%ld n=%ld b=%ld\n", oldnbb, nbb, bs);
  }

  // recuperation du resultat final.
  //   cudaMemcpy(&resultat, temp1, sizeof(double), cudaMemcpyDeviceToHost);
  //   cudaFree(temp1);
  //   cudaFree(temp2);

  err = clEnqueueReadBuffer(cqueue, temp1, CL_TRUE, 0, sizeof(double), &resultat, 0, NULL, &event);
  oclCheckErr(err, "clEnqueueReadBuffer");
  err = clWaitForEvents(1, &event);
  oclCheckErr(err, "clWaitForEvents");
  err = clReleaseEvent(event);
  oclCheckErr(err, "clReleaseEvent");

  err = clReleaseMemObject(temp1);
  err = clReleaseMemObject(temp2);
  return resultat;
}

//EOF
