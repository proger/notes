#ifndef CUEQUATION_OF_STATE_H_INCLUDED
#define CUEQUATION_OF_STATE_H_INCLUDED

#include "utils.h"
#include "parametres.h"

void cuEquationOfState(const long imin, const long imax, //
		       const double Hsmallc, const double Hgamma, //
		       const long Hnxyt, const long slices,    //
                       double *RESTRICT rhoDEV,         // [Hnxystep][Hnxyt]
                       double *RESTRICT eintDEV,        // [Hnxystep][Hnxyt]
                       double *RESTRICT pDEV,           // [Hnxystep][Hnxyt]
                       double *RESTRICT cDEV            // [Hnxystep][Hnxyt]
  );

#endif // EQUATION_OF_STATE_H_INCLUDED
