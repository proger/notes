#ifndef CURIEMANN_H_INCLUDED
#define CURIEMANN_H_INCLUDED


void cuRiemann(const long narray, const double Hsmallr, const double Hsmallc, const double Hgamma, //
	       const long Hniter_riemann, const long Hnvar, const long Hnxyt, const int slices, const int Hnxystep,      //
               double *RESTRICT qleftDEV,       // [Hnvar][Hnxystep][Hnxyt]
               double *RESTRICT qrightDEV,      // [Hnvar][Hnxystep][Hnxyt]
               double *RESTRICT qgdnvDEV,       // [Hnvar][Hnxystep][Hnxyt]
               long *RESTRICT sgnmDEV  // [Hnxystep][narray]
               // temporaries
//                , double *RESTRICT rlDEV,  // [Hnxystep][narray]
//                double *RESTRICT ulDEV,  // [Hnxystep][narray] 
//                double *RESTRICT plDEV,  // [Hnxystep][narray]
//                double *RESTRICT clDEV,  // [Hnxystep][narray] 
//                double *RESTRICT wlDEV,  // [Hnxystep][narray]
//                double *RESTRICT rrDEV,  // [Hnxystep][narray] 
//                double *RESTRICT urDEV,  // [Hnxystep][narray]
//                double *RESTRICT prDEV,  // [Hnxystep][narray] 
//                double *RESTRICT crDEV,  // [Hnxystep][narray]
//                double *RESTRICT wrDEV,  // [Hnxystep][narray] 
//                double *RESTRICT roDEV,  // [Hnxystep][narray]
//                double *RESTRICT uoDEV,  // [Hnxystep][narray] 
//                double *RESTRICT poDEV,  // [Hnxystep][narray]
//                double *RESTRICT coDEV,  // [Hnxystep][narray] 
//                double *RESTRICT woDEV,  // [Hnxystep][narray]
//                double *RESTRICT rstarDEV,       // [Hnxystep][narray] 
//                double *RESTRICT ustarDEV,       // [Hnxystep][narray]
//                double *RESTRICT pstarDEV,       // [Hnxystep][narray] 
//                double *RESTRICT cstarDEV,       // [Hnxystep][narray]
//                double *RESTRICT spinDEV,        // [Hnxystep][narray]
//                double *RESTRICT spoutDEV,       // [Hnxystep][narray] 
//                double *RESTRICT ushockDEV,      // [Hnxystep][narray]
//                double *RESTRICT fracDEV,        // [Hnxystep][narray] 
//                double *RESTRICT scrDEV, // [Hnxystep][narray]
//                double *RESTRICT delpDEV,        // [Hnxystep][narray] 
//                double *RESTRICT poldDEV,        // [Hnxystep][narray]
//                long *RESTRICT indDEV,   // [Hnxystep][narray] 
//                long *RESTRICT ind2DEV   // [Hnxystep][narray]
  );

#endif // CURIEMANN_H_INCLUDED
