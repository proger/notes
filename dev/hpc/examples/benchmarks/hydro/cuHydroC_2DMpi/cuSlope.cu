/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include "parametres.h"
#include "utils.h"
#include "gridfuncs.h"
#include "cuSlope.h"
#define DABS(x) (double) fabs((x))
//
__global__ void
LoopKcuSlope(const long Hnvar, const long Hnxyt, const double slope_type, const long ijmin, const long ijmax, const int slices, const int Hnxystep,  //
             double *RESTRICT q, double *RESTRICT dq) {
  long n;
  double dlft, drgt, dcen, dsgn, slop, dlim;
  long i, j;
  idx2d(i, j, Hnxyt);
  if (j >= slices)
    return;

  // printf("LoopKcuSlope: %ld %ld \n", i, j);

  //  idx3d(i, j, n, Hnxyt, Hstep);
  //   if (n >= Hnvar)
  //  return;
  if (i < 1) return;
  if (i >= ijmax -1) return;


  for (n = 0; n < Hnvar; n++) {
    dlft = slope_type * (q[IHVWS(i,j,n)] - q[IHVWS(i - 1,j,n)]);
    drgt = slope_type * (q[IHVWS(i + 1,j,n)] - q[IHVWS(i,j,n)]);
    dcen = half * (dlft + drgt) / slope_type;
    dsgn = (dcen > 0) ? (double) 1.0 : (double) -1.0;     // sign(one, dcen);
    slop = (double) MIN(DABS(dlft), DABS(drgt));
    dlim = ((dlft * drgt) <= zero) ? zero : slop;
    //         if ((dlft * drgt) <= zero) {
    //             dlim = zero;
    //         }
    dq[IHVWS(i,j,n)] = dsgn * (double) MIN(dlim, DABS(dcen));
  }
}

void
cuSlope(const long narray, const long Hnvar, const long Hnxyt, const double slope_type, const int slices, const int Hnxystep,        // 
        double *RESTRICT qDEV,  // [Hnvar][Hnxystep][Hnxyt]
        double *RESTRICT dqDEV  // [Hnvar][Hnxystep][Hnxyt]
	) {
  long ijmin, ijmax;
  dim3 grid, block;
  WHERE("slope");
  ijmin = 1;
  ijmax = narray;
  SetBlockDims(Hnxyt * slices, THREADSSZ, block, grid); // Hnvar * 
  LoopKcuSlope <<< grid, block >>> (Hnvar, Hnxyt, slope_type, ijmin, ijmax, slices, Hnxystep, qDEV, dqDEV);
  CheckErr("LoopKcuSlope");
  cudaThreadSynchronize();
}                               // slope

//EOF
