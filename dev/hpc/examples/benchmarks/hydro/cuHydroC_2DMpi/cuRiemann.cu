/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <cuda.h>

#include "parametres.h"
#include "utils.h"
#include "cuRiemann.h"
#include "gridfuncs.h"

#define DABS(x) (double) fabs((x))

typedef struct _Args {
  double *qleft;
  double *qright;
  double *qgdnv;
  long *sgnm;
//   double *rl;
//   double *ul;
//   double *pl;
//   double *cl;
//   double *wl;
//   double *rr;
//   double *ur;
//   double *pr;
//   double *cr;
//   double *wr;
//   double *ro;
//   double *uo;
//   double *po;
//   double *co;
//   double *wo;
//   double *rstar;
//   double *ustar;
//   double *pstar;
//   double *cstar;
//   double *spin;
//   double *spout;
//   double *ushock;
//   double *frac;
//   double *scr;
//   double *delp;
//   double *pold;
//   long *ind;
//   long *ind2;
  long narray;
  double Hsmallr;
  double Hsmallc;
  double Hgamma;
  long Hniter_riemann;
  long Hnvar;
  long Hnxyt;
  long Hnxystep;
  int slices;
} Args_t;

// memoire de constante sur le device qui va contenir les arguments de riemann
// on les transmets en bloc en une fois et les differents kernels pourront y acceder.
__constant__ Args_t K;

__global__ void
Loop1KcuRiemann() {
  double smallp, gamma6, ql, qr, usr, usl, wwl, wwr, smallpp;
  long iter;
  double ulS, rlS;
  double plS, wlS;
  double clS;
  double urS, rrS;
  double prS, wrS;
  double crS;
  double uoS, roS, poS, coS, woS;
  double delpS;
  double poldS;
  double pstarS, ustarS, rstarS, cstar;
  double spoutS, spinS, fracS, ushockS, scrS, sgnmS;
  int indS;
  long Hnxyt = K.Hnxyt;
  long Hnxystep = K.Hnxystep;
  int slices = K.slices;

  long i, j, idx;
  idx = idx1d();
  j = idx / Hnxyt;
  i = idx % Hnxyt;

  if (j >= slices)
    return;
  if (i >= K.narray)
    return;

  smallp = Square(K.Hsmallc) / K.Hgamma;

  rlS = MAX(K.qleft[IHVWS(i, j, ID)], K.Hsmallr);
  ulS = K.qleft[IHVWS(i, j, IU)];
  plS = MAX(K.qleft[IHVWS(i, j, IP)], (double) (rlS * smallp));
  rrS = MAX(K.qright[IHVWS(i, j, ID)], K.Hsmallr);
  urS = K.qright[IHVWS(i, j, IU)];
  prS = MAX(K.qright[IHVWS(i, j, IP)], (double) (rrS * smallp));
  // Lagrangian sound speed
  clS = K.Hgamma * plS * rlS;
  crS = K.Hgamma * prS * rrS;
  // First guess
  wlS = sqrt(clS);
  wrS = sqrt(crS);
  pstarS = ((wrS * plS + wlS * prS) + wlS * wrS * (ulS - urS)) / (wlS + wrS);
  pstarS = MAX(pstarS, 0.0);
  poldS = pstarS;
  // indS est un masque de traitement pour le newton
  indS = 1;                     // toutes les cellules sont a traiter

  smallp = Square(K.Hsmallc) / K.Hgamma;
  smallpp = K.Hsmallr * smallp;
  gamma6 = (K.Hgamma + one) / (two * K.Hgamma);

  long indi = indS;

  for (iter = 0; iter < K.Hniter_riemann; iter++) {
    double precision = 1.e-6;
    wwl = sqrt(clS * (one + gamma6 * (poldS - plS) / plS));
    wwr = sqrt(crS * (one + gamma6 * (poldS - prS) / prS));
    ql = two * wwl * Square(wwl) / (Square(wwl) + clS);
    qr = two * wwr * Square(wwr) / (Square(wwr) + crS);
    usl = ulS - (poldS - plS) / wwl;
    usr = urS + (poldS - prS) / wwr;
    double t1 = qr * ql / (qr + ql) * (usl - usr);
    double t2 = -poldS;
    delpS = MAX(t1, t2);
    poldS = poldS + delpS;
    uoS = DABS(delpS / (poldS + smallpp));
    indi = uoS > precision;
    if (!indi)
      break;
  }

  gamma6 = (K.Hgamma + one) / (two * K.Hgamma);

  pstarS = poldS;
  wlS = sqrt(clS * (one + gamma6 * (pstarS - plS) / plS));
  wrS = sqrt(crS * (one + gamma6 * (pstarS - prS) / prS));

  ustarS = half * (ulS + (plS - pstarS) / wlS + urS - (prS - pstarS) / wrS);
  sgnmS = (ustarS > 0) ? 1 : -1;
  if (sgnmS == 1) {
    roS = rlS;
    uoS = ulS;
    poS = plS;
    woS = wlS;
  } else {
    roS = rrS;
    uoS = urS;
    poS = prS;
    woS = wrS;
  }
  coS = MAX(K.Hsmallc, sqrt(DABS(K.Hgamma * poS / roS)));
  rstarS = roS / (one + roS * (poS - pstarS) / Square(woS));
  rstarS = MAX(rstarS, K.Hsmallr);
  cstar = MAX(K.Hsmallc, sqrt(DABS(K.Hgamma * pstarS / rstarS)));
  spoutS = coS - sgnmS * uoS;
  spinS = cstar - sgnmS * ustarS;
  ushockS = woS / roS - sgnmS * uoS;
  if (pstarS >= poS) {
    spinS = ushockS;
    spoutS = ushockS;
  }
  scrS = MAX((double) (spoutS - spinS), (double) (K.Hsmallc + DABS(spoutS + spinS)));
  fracS = (one + (spoutS + spinS) / scrS) * half;
  fracS = MAX(zero, (double) (MIN(one, fracS)));

  K.qgdnv[IHVWS(i, j, ID)] = fracS * rstarS + (one - fracS) * roS;
  K.qgdnv[IHVWS(i, j, IU)] = fracS * ustarS + (one - fracS) * uoS;
  K.qgdnv[IHVWS(i, j, IP)] = fracS * pstarS + (one - fracS) * poS;

  if (spoutS < zero) {
    K.qgdnv[IHVWS(i, j, ID)] = roS;
    K.qgdnv[IHVWS(i, j, IU)] = uoS;
    K.qgdnv[IHVWS(i, j, IP)] = poS;
  }
  if (spinS > zero) {
    K.qgdnv[IHVWS(i, j, ID)] = rstarS;
    K.qgdnv[IHVWS(i, j, IU)] = ustarS;
    K.qgdnv[IHVWS(i, j, IP)] = pstarS;
  }

  if (sgnmS == 1) {
    K.qgdnv[IHVWS(i, j, IV)] = K.qleft[IHVWS(i, j, IV)];
  } else {
    K.qgdnv[IHVWS(i, j, IV)] = K.qright[IHVWS(i, j, IV)];
  }
  K.sgnm[IHS(i, j)] = sgnmS;
}

__global__ void
Loop10KcuRiemann() {
  long invar;
  long i;
  long Hnxyt = K.Hnxyt;
  int slices = K.slices;
  int Hnxystep = K.Hnxystep;
  long j;

  idx2d(i, j, Hnxyt);
  if (j >= slices)
    return;

  for (invar = IP + 1; invar < K.Hnvar; invar++) {
    if (K.sgnm[IHS(i, j)] == 1) {
      K.qgdnv[IHVWS(i, j, invar)] = K.qleft[IHVWS(i, j, invar)];
    }
    if (K.sgnm[IHS(i, j)] != 1) {
      K.qgdnv[IHVWS(i, j, invar)] = K.qright[IHVWS(i, j, invar)];
    }
  }
}

void
cuRiemann(const long narray, const double Hsmallr, const double Hsmallc, const double Hgamma,   //
          const long Hniter_riemann, const long Hnvar, const long Hnxyt, const int slices, const int Hnxystep,  //
          double *RESTRICT qleftDEV,    // [Hnvar][Hnxystep][Hnxyt]
          double *RESTRICT qrightDEV,   // [Hnvar][Hnxystep][Hnxyt]
          double *RESTRICT qgdnvDEV,    // [Hnvar][Hnxystep][Hnxyt]
          long *RESTRICT sgnmDEV       // [Hnxystep][narray]
          // temporaries
//           , double *RESTRICT rlDEV,       // [Hnxystep][narray]
//           double *RESTRICT ulDEV,       // [Hnxystep][narray] 
//           double *RESTRICT plDEV,       // [Hnxystep][narray]
//           double *RESTRICT clDEV,       // [Hnxystep][narray] 
//           double *RESTRICT wlDEV,       // [Hnxystep][narray]
//           double *RESTRICT rrDEV,       // [Hnxystep][narray] 
//           double *RESTRICT urDEV,       // [Hnxystep][narray]
//           double *RESTRICT prDEV,       // [Hnxystep][narray] 
//           double *RESTRICT crDEV,       // [Hnxystep][narray]
//           double *RESTRICT wrDEV,       // [Hnxystep][narray] 
//           double *RESTRICT roDEV,       // [Hnxystep][narray]
//           double *RESTRICT uoDEV,       // [Hnxystep][narray] 
//           double *RESTRICT poDEV,       // [Hnxystep][narray]
//           double *RESTRICT coDEV,       // [Hnxystep][narray] 
//           double *RESTRICT woDEV,       // [Hnxystep][narray]
//           double *RESTRICT rstarDEV,    // [Hnxystep][narray] 
//           double *RESTRICT ustarDEV,    // [Hnxystep][narray]
//           double *RESTRICT pstarDEV,    // [Hnxystep][narray] 
//           double *RESTRICT cstarDEV,    // [Hnxystep][narray]
//           double *RESTRICT spinDEV,     // [Hnxystep][narray]
//           double *RESTRICT spoutDEV,    // [Hnxystep][narray] 
//           double *RESTRICT ushockDEV,   // [Hnxystep][narray]
//           double *RESTRICT fracDEV,     // [Hnxystep][narray] 
//           double *RESTRICT scrDEV,      // [Hnxystep][narray]
//           double *RESTRICT delpDEV,     // [Hnxystep][narray] 
//           double *RESTRICT poldDEV,     // [Hnxystep][narray]
//           long *RESTRICT indDEV,        // [Hnxystep][narray] 
//           long *RESTRICT ind2DEV        // [Hnxystep][narray]
  ) {
  // Local variables
  dim3 block, grid;
  Args_t k;

  WHERE("riemann");
  k.qleft = qleftDEV;
  k.qright = qrightDEV;
  k.qgdnv = qgdnvDEV;
  k.sgnm = sgnmDEV;
  //
//   k.rl = rlDEV;
//   k.ul = ulDEV;
//   k.pl = plDEV;
//   k.cl = clDEV;
//   k.wl = wlDEV;
//   k.rr = rrDEV;
//   k.ur = urDEV;
//   k.pr = prDEV;
//   k.cr = crDEV;
//   k.wr = wrDEV;
//   k.ro = roDEV;
//   k.uo = uoDEV;
//   k.po = poDEV;
//   k.co = coDEV;
//   k.wo = woDEV;
//   k.rstar = rstarDEV;
//   k.ustar = ustarDEV;
//   k.pstar = pstarDEV;
//   k.cstar = cstarDEV;
//   k.spin = spinDEV;
//   k.spout = spoutDEV;
//   k.ushock = ushockDEV;
//   k.frac = fracDEV;
//   k.scr = scrDEV;
//   k.delp = delpDEV;
//   k.pold = poldDEV;
//   k.ind = indDEV;
//   k.ind2 = ind2DEV;
  //
  k.narray = narray;
  k.Hsmallr = Hsmallr;
  k.Hsmallc = Hsmallc;
  k.Hgamma = Hgamma;
  k.Hniter_riemann = Hniter_riemann;
  k.Hnvar = Hnvar;
  k.Hnxyt = Hnxyt;
  k.Hnxystep = Hnxystep;
  k.slices = slices;

  cudaMemcpyToSymbol(K, &k, sizeof(Args_t), 0, cudaMemcpyHostToDevice);
  CheckErr("cudaMemcpyToSymbol");

  // 64 threads donnent le meilleur rendement compte-tenu de la complexite du kernel
  SetBlockDims(Hnxyt * slices, 192, block, grid);

#if CUDA_VERSION > 2000
  // cudaFuncSetCacheConfig(Loop1KcuRiemann, cudaFuncCachePreferShared);
  cudaFuncSetCacheConfig(Loop1KcuRiemann, cudaFuncCachePreferL1);
  // cudaFuncSetCacheConfig(Loop1KcuRiemann, cudaFuncCachePreferNone);
#endif

  // Pressure, density and velocity
  Loop1KcuRiemann <<< grid, block >>> ();
  CheckErr("Avant synchronize Loop1KcuRiemann");
  cudaThreadSynchronize();
  CheckErr("After synchronize Loop1KcuRiemann");

  // other passive variables
  if (Hnvar > IP + 1) {
    Loop10KcuRiemann <<< grid, block >>> ();
    cudaThreadSynchronize();
    CheckErr("After synchronize Loop10KcuRiemann");
  }
}                               // riemann


//EOF
