/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

// #include <stdlib.h>
// #include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "gridfuncs.h"
#include "cuEquationOfState.h"
#include "utils.h"

__global__ void
LoopEOS(const long imin, const long imax, const double Hsmallc, const double Hgamma, const int Hnxyt, const int slices,   //
        double *RESTRICT rho,   // [Hnxystep][Hnxyt]
        double *RESTRICT eint,  // [Hnxystep][Hnxyt]
        double *RESTRICT p,     // [Hnxystep][Hnxyt]
        double *RESTRICT c      // [Hnxystep][Hnxyt]
  ) {
  double smallp;
  long i, j;
  idx2d(i, j, Hnxyt);
  if (j >= slices)
    return;
  if (i < imin) return;
  if (i >= imax) return;

  smallp = Square(Hsmallc) / Hgamma;
  p[IHS(i,j)] = (Hgamma - one) * rho[IHS(i,j)] * eint[IHS(i,j)];
  p[IHS(i,j)] = MAX(p[IHS(i,j)], (double) (rho[IHS(i,j)] * smallp));
  c[IHS(i,j)] = sqrt(Hgamma * p[IHS(i,j)] / rho[IHS(i,j)]);
}

void
cuEquationOfState(const long imin, const long imax, //
		  const double Hsmallc, const double Hgamma, //
		  const long Hnxyt, const long slices, //
                  double *RESTRICT rhoDEV,      // [Hnxystep][Hnxyt]
                  double *RESTRICT eintDEV,     // [Hnxystep][Hnxyt]
                  double *RESTRICT pDEV,        // [Hnxystep][Hnxyt]
                  double *RESTRICT cDEV         // [Hnxystep][Hnxyt]
  ) {
  dim3 grid, block;
  WHERE("equation_of_state");
  SetBlockDims(Hnxyt * slices, THREADSSZ, block, grid);
  LoopEOS <<< grid, block >>> (imin, imax, Hsmallc, Hgamma, Hnxyt, slices, rhoDEV, eintDEV, pDEV, cDEV);
  CheckErr("LoopEOS");
  cudaThreadSynchronize();
  CheckErr("LoopEOS");
}                               // equation_of_state


// EOF
