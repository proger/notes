/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "gridfuncs.h"
#include "cuQleftright.h"

__global__ void
Loop1KcuQleftright(const long bmax, const long Hnvar, const long Hnxyt, const int slices,  const int Hnxystep,      //
                   double *RESTRICT qxm, double *RESTRICT qxp, double *RESTRICT qleft, double *RESTRICT qright) {
  int nvar;
  long i, j;
  idx2d(i, j, Hnxyt);
  if (j >= slices)
    return;
  if (i >= bmax)
    return;

  for (nvar = 0; nvar < Hnvar; nvar++) {
    qleft[IHVWS(i, j, nvar)] = qxm[IHVWS(i + 1, j, nvar)];
    qright[IHVWS(i, j, nvar)] = qxp[IHVWS(i + 2, j, nvar)];
  }
}

void
cuQleftright(const long idim, const long Hnx, const long Hny, const long Hnxyt, const long Hnvar, const int slices,  const int Hnxystep,      // 
             double *RESTRICT qxmDEV,   // [Hnvar][Hnxystep][Hnxyt]
             double *RESTRICT qxpDEV,   // [Hnvar][Hnxystep][Hnxyt]
             double *RESTRICT qleftDEV, // [Hnvar][Hnxystep][Hnxyt]
             double *RESTRICT qrightDEV // [Hnvar][Hnxystep][Hnxyt]
  ) {
  long bmax;
  dim3 block, grid;
  WHERE("qleftright");
  if (idim == 1) {
    bmax = Hnx + 1;
  } else {
    bmax = Hny + 1;
  }
  SetBlockDims(Hnxyt * slices, THREADSSZ, block, grid);
  Loop1KcuQleftright <<< grid, block >>> (bmax, Hnvar, Hnxyt, slices, Hnxystep, qxmDEV, qxpDEV, qleftDEV, qrightDEV);
  CheckErr("Loop1KcuQleftright");
  cudaThreadSynchronize();
}

#undef IHVW

// EOF
