/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "utils.h"
#include "hydro_utils.h"
#include "hydro_funcs.h"
void
hydro_init(hydroparam_t * H, hydrovar_t * Hv)
{
  long i, j;
  long x, y;

  // *WARNING* : we will use 0 based arrays everywhere since it is C code!
  H->imin = H->jmin = 0;

  // We add two extra layers left/right/top/bottom
  H->imax = H->nx + ExtraLayerTot;
  H->jmax = H->ny + ExtraLayerTot;
  H->nxt = H->imax - H->imin;   // column size in the array
  H->nyt = H->jmax - H->jmin;   // row size in the array
  // maximum direction size
  H->nxyt = (H->nxt > H->nyt) ? H->nxt : H->nyt;

  H->arSz = (H->nxyt + 2);
  H->arVarSz = (H->nxyt + 2) * H->nvar;
  H->arUoldSz = H->nvar * H->nxt * H->nyt;
  // allocate uold for each conservative variable
#warning "Use a CUDAMALLOCHOST here"
  Hv->uold = (double *) calloc(H->arUoldSz, sizeof(double));

  // wind tunnel with point explosion
  for (j = H->jmin + ExtraLayer; j < H->jmax - ExtraLayer; j++) {
    for (i = H->imin + ExtraLayer; i < H->imax - ExtraLayer; i++) {
      Hv->uold[IHvP(i, j, ID)] = one;
      Hv->uold[IHvP(i, j, IU)] = zero;
      Hv->uold[IHvP(i, j, IV)] = zero;
      Hv->uold[IHvP(i, j, IP)] = 1e-5;
    }
  }
  // Initial shock
  if (H->nproc == 1) {
    x = (H->imax - H->imin) / 2 + ExtraLayer * 0;
    y = (H->jmax - H->jmin) / 2 + ExtraLayer * 0;
    Hv->uold[IHvP(x, y, IP)] = one / H->dx / H->dx;
    printf("%d %d\n", x, y);
  } else {
    x = ((H->globnx + 2*ExtraLayer) / 2);
    y = ((H->globny + 2*ExtraLayer) / 2);
    if ((x >= H->box[XMIN_BOX]) && (x < H->box[XMAX_BOX]) && (y >= H->box[YMIN_BOX]) && (y < H->box[YMAX_BOX])) {
      x = (H->globnx / 2) - H->box[XMIN_BOX] + ExtraLayer;
      y = (H->globny / 2) - H->box[YMIN_BOX] + ExtraLayer;
      Hv->uold[IHvP(x, y, IP)] = one / H->dx / H->dx;
      printf("[%d] %d %d\n", H->mype, x, y);
    }
  }
//   // Perturbation of the computation
//   for (i = 0; i < 10; i++) {
//     x = ((H->globnx + 2*ExtraLayer) / 5) + i;
//     y = ((H->globny + 2*ExtraLayer) / 4);
//     if (H->nproc == 1) {
//       Hv->uold[IHvP(x, y, ID)] = 1e5;
//       printf("%d %d\n", x, y);
//     } else {
//       if ((x >= H->box[XMIN_BOX]) && (x < H->box[XMAX_BOX]) && (y >= H->box[YMIN_BOX]) && (y < H->box[YMAX_BOX])) {
// 	x = x - H->box[XMIN_BOX] + ExtraLayer;
// 	y = y - H->box[YMIN_BOX] + ExtraLayer;
// 	Hv->uold[IHvP(x, y, ID)] = 1e5;;
// 	printf("[%d] %d %d\n", H->mype, x, y);
//       }
//     }
//   }
}                               // hydro_init

void
hydro_finish(const hydroparam_t H, hydrovar_t * Hv)
{
  Free(Hv->uold);
}                               // hydro_finish

void
allocate_work_space(const hydroparam_t H, hydrowork_t * Hw, hydrovarwork_t * Hvw)
{
  WHERE("allocate_work_space");
  Hvw->u = DMalloc(H.nxystep * H.arVarSz);
  Hvw->q = DMalloc(H.nxystep * H.arVarSz);
  Hvw->dq = DMalloc(H.nxystep * H.arVarSz);
  Hvw->qxm = DMalloc(H.nxystep * H.arVarSz);
  Hvw->qxp = DMalloc(H.nxystep * H.arVarSz);
  Hvw->qleft = DMalloc(H.nxystep * H.arVarSz);
  Hvw->qright = DMalloc(H.nxystep * H.arVarSz);
  Hvw->qgdnv = DMalloc(H.nxystep * H.arVarSz);
  Hvw->flux = DMalloc(H.nxystep * H.arVarSz);
  Hw->e = DMalloc(H.nxystep * H.arSz);
  Hw->c = DMalloc(H.nxystep * H.arSz);
  Hw->sgnm = IMalloc(H.nxystep * H.arSz);
  //
  Hw->rl = DMalloc(H.nxystep * H.arSz);
  Hw->ul = DMalloc(H.nxystep * H.arSz);
  Hw->pl = DMalloc(H.nxystep * H.arSz);
  Hw->cl = DMalloc(H.nxystep * H.arSz);
  Hw->rr = DMalloc(H.nxystep * H.arSz);
  Hw->ur = DMalloc(H.nxystep * H.arSz);
  Hw->pr = DMalloc(H.nxystep * H.arSz);
  Hw->cr = DMalloc(H.nxystep * H.arSz);
  Hw->ro = DMalloc(H.nxystep * H.arSz);
  Hw->uo = DMalloc(H.nxystep * H.arSz);
  Hw->po = DMalloc(H.nxystep * H.arSz);
  Hw->co = DMalloc(H.nxystep * H.arSz);
  Hw->rstar = DMalloc(H.nxystep * H.arSz);
  Hw->ustar = DMalloc(H.nxystep * H.arSz);
  Hw->pstar = DMalloc(H.nxystep * H.arSz);
  Hw->cstar = DMalloc(H.nxystep * H.arSz);
  Hw->wl = DMalloc(H.nxystep * H.arSz);
  Hw->wr = DMalloc(H.nxystep * H.arSz);
  Hw->wo = DMalloc((H.nxystep * H.arSz));
  Hw->spin = DMalloc(H.nxystep * H.arSz);
  Hw->spout = DMalloc(H.nxystep * H.arSz);
  Hw->ushock = DMalloc(H.nxystep * H.arSz);
  Hw->frac = DMalloc(H.nxystep * H.arSz);
  Hw->scr = DMalloc(H.nxystep * H.arSz);
  Hw->delp = DMalloc(H.nxystep * H.arSz);
  Hw->pold = DMalloc(H.nxystep * H.arSz);
  Hw->ind = IMalloc(H.nxystep * H.arSz);
  Hw->ind2 = IMalloc(H.nxystep * H.arSz);
}                               // allocate_work_space


/*
static void
VFree(double **v, const hydroparam_t H)
{
    long i;
    for (i = 0; i < H.nvar; i++) {
        Free(v[i]);
    }
    Free(v);
} // VFree
*/
void
deallocate_work_space(const hydroparam_t H, hydrowork_t * Hw, hydrovarwork_t * Hvw)
{
  WHERE("deallocate_work_space");

  //
  Free(Hw->e);
  //
  Free(Hvw->u);
  Free(Hvw->q);
  Free(Hvw->dq);
  Free(Hvw->qxm);
  Free(Hvw->qxp);
  Free(Hvw->qleft);
  Free(Hvw->qright);
  Free(Hvw->qgdnv);
  Free(Hvw->flux);
  Free(Hw->sgnm);

  //
  Free(Hw->c);
  Free(Hw->rl);
  Free(Hw->ul);
  Free(Hw->pl);
  Free(Hw->cl);
  Free(Hw->rr);
  Free(Hw->ur);
  Free(Hw->pr);
  Free(Hw->cr);
  Free(Hw->ro);
  Free(Hw->uo);
  Free(Hw->po);
  Free(Hw->co);
  Free(Hw->rstar);
  Free(Hw->ustar);
  Free(Hw->pstar);
  Free(Hw->cstar);
  Free(Hw->wl);
  Free(Hw->wr);
  Free(Hw->wo);
  Free(Hw->spin);
  Free(Hw->spout);
  Free(Hw->ushock);
  Free(Hw->frac);
  Free(Hw->scr);
  Free(Hw->delp);
  Free(Hw->pold);
  Free(Hw->ind);
  Free(Hw->ind2);
}                               // deallocate_work_space


// EOF
