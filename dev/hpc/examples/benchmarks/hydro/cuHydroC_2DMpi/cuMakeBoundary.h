#ifndef CUMAKE_BOUNDARY_H_INCLUDED
#define CUMAKE_BOUNDARY_H_INCLUDED

void cuMakeBoundary(long idim, const hydroparam_t H, hydrovar_t * Hv, double *uoldDEV);

#endif // MAKE_BOUNDARY_H_INCLUDED
