#ifndef CUTRACE_H_INCLUDED
#define CUTRACE_H_INCLUDED

void cuTrace(const double dtdx, const long n, const long Hscheme, const long Hnvar, const long Hnxyt, const int slices, const int Hnxystep,  //
             double * RESTRICT qDEV,       // [Hnvar][Hnxystep][Hnxyt]
             double * RESTRICT dqDEV,      // [Hnvar][Hnxystep][Hnxyt]
             double * RESTRICT cDEV,       //        [Hnxystep][Hnxyt]
             double * RESTRICT qxmDEV,     // [Hnvar][Hnxystep][Hnxyt] 
             double * RESTRICT qxpDEV      // [Hnvar][Hnxystep][Hnxyt]
  );

#endif // TRACE_H_INCLUDED
