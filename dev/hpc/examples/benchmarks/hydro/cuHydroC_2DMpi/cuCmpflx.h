#ifndef CUCMPFLX_H_INCLUDED
#define CUCMPFLX_H_INCLUDED

#include "utils.h"
void cuCmpflx(int narray, int Hnxyt, int Hnvar, double Hgamma, int slices, const int Hnxstep,       //
              double *RESTRICT qgdnv,   //[Hnvar][Hnxstep][Hnxyt]
              double *RESTRICT flux     //[Hnvar][Hnxstep][Hnxyt]
  );

#endif // CMPFLX_H_INCLUDED
