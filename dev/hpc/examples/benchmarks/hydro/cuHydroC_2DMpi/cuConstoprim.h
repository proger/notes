#ifndef CUCONSTOPRIM_H_INCLUDED
#define CUCONSTOPRIM_H_INCLUDED

#include "utils.h"

void cuConstoprim(const long n, const long Hnxyt, const long Hnvar, const double Hsmallr, const int slices, const int Hnxystep,      //
                  double *RESTRICT uDEV,  // [Hnvar][Hnxystep][Hnxyt]
                  double *RESTRICT qDEV,  // [Hnvar][Hnxystep][Hnxyt]
                  double *RESTRICT eDEV   //        [Hnxystep][Hnxyt]
  );

#endif // CUCONSTOPRIM_H_INCLUDED
