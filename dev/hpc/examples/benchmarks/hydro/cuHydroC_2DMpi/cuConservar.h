#ifndef CUCONSERVAR_H
#define CUCONSERVAR_H

void
  cuGatherConservativeVars(const long idim, const long rowcol, const long Himin, const long Himax, const long Hjmin,    //
                           const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt, const int slices,   const int Hnxstep,  //
                           double *RESTRICT uoldDEV,    // [Hnvar * Hnxt * Hnyt]
                           double *RESTRICT uDEV        // [Hnvar][Hnxstep][Hnxyt]
  );

void
  cuUpdateConservativeVars(const long idim, const long rowcol, const double dtdx, const long Himin, const long Himax,   // 
                           const long Hjmin, const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt, const int slices, const int Hnxstep,  //
                           double *RESTRICT uoldDEV,    // [Hnvar * Hnxt * Hnyt]
                           double *RESTRICT uDEV,       // [Hnvar][Hnxstep][Hnxyt]
                           double *RESTRICT fluxDEV     // [Hnvar][Hnxstep][Hnxyt]
  );
#endif
