/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <math.h>
#include <malloc.h>
// #include <unistd.h>
// #include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "cuCmpflx.h"
#include "gridfuncs.h"

__global__ void
Loop1KcuCmpflx(long narray, long Hnxyt, double Hgamma, const int slices, const int Hnxystep, //
               double *RESTRICT qgdnv,  // [Hnvar][Hnxystep][Hnxyt]
               double *RESTRICT flux    // [Hnvar][Hnxystep][Hnxyt]
  ) {
  double entho, ekin, etot;
  long i, j;
  idx2d(i, j, Hnxyt);
  if (j >= slices)
    return;

  if (i >= narray) return;

  entho = one / (Hgamma - one);
  // Mass density
  flux[IHVWS(i, j, ID)] = qgdnv[IHVWS(i, j, ID)] * qgdnv[IHVWS(i, j, IU)];
  // Normal momentum
  flux[IHVWS(i, j, IU)] = flux[IHVWS(i, j, ID)] * qgdnv[IHVWS(i, j, IU)] + qgdnv[IHVWS(i, j, IP)];
  // Transverse momentum 1
  flux[IHVWS(i, j, IV)] = flux[IHVWS(i, j, ID)] * qgdnv[IHVWS(i, j, IV)];
  // Total energy
  ekin = half * qgdnv[IHVWS(i, j, ID)] * (Square(qgdnv[IHVWS(i, j, IU)]) + Square(qgdnv[IHVWS(i, j, IV)]));
  etot = qgdnv[IHVWS(i, j, IP)] * entho + ekin;
  flux[IHVWS(i, j, IP)] = qgdnv[IHVWS(i, j, IU)] * (etot + qgdnv[IHVWS(i, j, IP)]);
}

__global__ void
Loop2KcuCmpflx(const long narray, const long Hnxyt, const long Hnvar, const int slices, const int Hnxystep,  //
               double *RESTRICT qgdnv,  // [Hnvar][Hnxystep][Hnxyt]
               double *RESTRICT flux    // [Hnvar][Hnxystep][Hnxyt]
  ) {
  long IN;
  long i, j;
  idx2d(i, j, Hnxyt);
  if (j >= slices)
    return;

  for (IN = IP + 1; IN < Hnvar; IN++) {
    flux[IHVWS(i, j, IN)] = flux[IHVWS(i, j, IN)] * qgdnv[IHVWS(i, j, IN)];
  }
}

void
cuCmpflx(int narray, int Hnxyt, int Hnvar, double Hgamma, int slices, const int Hnxstep,   //
         double *RESTRICT qgdnv,        //[Hnvar][Hnxstep][Hnxyt]
         double *RESTRICT flux          //[Hnvar][Hnxstep][Hnxyt]
  ) {
  dim3 grid, block;

  WHERE("cmpflx");

  SetBlockDims(Hnxyt * slices, THREADSSZ, block, grid);

  // Compute fluxes
  Loop1KcuCmpflx <<< grid, block >>> (narray, Hnxyt, Hgamma, slices, Hnxstep, qgdnv, flux);
  CheckErr("Loop1KcuCmpflx");
  // Other advected quantities
  if (Hnvar > IP + 1) {
    Loop2KcuCmpflx <<< grid, block >>> (narray, Hnxyt, Hnvar, slices, Hnxstep, qgdnv, flux);
    CheckErr("Loop2KcuCmpflx");
  }
  cudaThreadSynchronize();
  CheckErr("After synchronize cuCmpflx");
}                               // cmpflx

//EOF
