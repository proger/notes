#ifndef CUHYDROGODUNOV_H
#define CUHYDROGODUNOV_H

#include "parametres.h"

#ifdef __cplusplus
extern "C" {
#endif
  void cuHydroGodunov(long idim, double dt, const hydroparam_t H, hydrovar_t * Hv, hydrowork_t * Hw,
                      hydrovarwork_t * Hvw);
  void cuFreeOnDevice();
  void cuAllocOnDevice(const hydroparam_t H);
  void cuPutUoldOnDevice(const hydroparam_t H, hydrovar_t * Hv);
  void cuGetUoldFromDevice(const hydroparam_t H, hydrovar_t * Hv);
#ifdef __cplusplus
};
#endif

void cuGetUoldQECDevicePtr(double **uoldDEV, double **qDEV, double **eDEV, double **cDEV);
#endif
