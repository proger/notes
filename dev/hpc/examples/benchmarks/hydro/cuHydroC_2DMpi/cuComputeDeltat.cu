/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdio.h>
// #include <stdlib.h>
#include <malloc.h>
// #include <unistd.h>
#include <math.h>
#include <mpi.h>
#include <cuda.h>

#include "parametres.h"
#include "cuComputeDeltat.h"
#include "cuHydroGodunov.h"
#include "gridfuncs.h"
#include "utils.h"
#include "cuEquationOfState.h"

#define DABS(x) (double) fabs((x))
#define VERIF(x, ou) if ((x) != cudaSuccess)  { CheckErr((ou)); }

__global__ void
LoopKQEforRows(const long j, double *uold, double *q, double *e, const double Hsmallr,  //
               const long Hnxt, const long Hnyt, const long Hnxyt,      //
               const int slices, const long Hnxystep, const long n) {
  double eken;
  long i, s;
  idx2d(i, s, Hnxyt);
  if (s >= slices)
    return;

  if (i >= n)
    return;

  q[IHVWS(i, s, ID)] = MAX(uold[IHU(i + ExtraLayer, j + s, ID)], Hsmallr);
  q[IHVWS(i, s, IU)] = uold[IHU(i + ExtraLayer, j + s, IU)] / q[IHVWS(i, s, ID)];
  q[IHVWS(i, s, IV)] = uold[IHU(i + ExtraLayer, j + s, IV)] / q[IHVWS(i, s, ID)];
  eken = half * (Square(q[IHVWS(i, s, IU)]) + Square(q[IHVWS(i, s, IV)]));
  q[IHVWS(i, s, IP)] = uold[IHU(i + ExtraLayer, j + s, IP)] / q[IHVWS(i, s, ID)] - eken;
  e[IHS(i, s)] = q[IHVWS(i, s, IP)];
}

void
cuComputeQEforRows(const long j, double *uold, double *q, double *e, const double Hsmallr, const long Hnx,      //
                   const long Hnxt, const long Hnyt, const long Hnxyt, const int slices,
                   const long Hnxystep) {
  dim3 grid, block;

  SetBlockDims(Hnx * slices, THREADSSZ, block, grid);
  LoopKQEforRows <<< grid, block >>> (j, uold, q, e, Hsmallr, Hnxt, Hnyt, Hnxyt, slices, Hnxystep,
                                      Hnx);
  CheckErr("courantOnXY");
  cudaThreadSynchronize();
  CheckErr("courantOnXY");
}

__global__ void
LoopKcourant(double *q, double *courant, const double Hsmallc, const double *c, //
             const long Hnxyt, const int slices, const long Hnxystep, const long n) {
  double cournox, cournoy, courantl;
  long i, s;
  idx2d(i, s, Hnxyt);
  if (s >= slices)
    return;

  if (i >= n)
    return;

  cournox = cournoy = 0.;

  cournox = c[IHS(i, s)] + DABS(q[IHVWS(i, s, IU)]);
  cournoy = c[IHS(i, s)] + DABS(q[IHVWS(i, s, IV)]);
  courantl = MAX(cournox, MAX(cournoy, Hsmallc));
  courant[IHS(i, s)] = MAX(courant[IHS(i, s)], courantl);
}

void
cuCourantOnXY(double *courant, const long Hnx, const long Hnxyt, const int slices,
              const long Hnxystep, double *c, double *q, double Hsmallc) {
  dim3 grid, block;

  SetBlockDims(Hnx * slices, THREADSSZ, block, grid);
  LoopKcourant <<< grid, block >>> (q, courant, Hsmallc, c, Hnxyt, slices, Hnxystep, Hnx);
  CheckErr("courantOnXY");
  cudaThreadSynchronize();
  CheckErr("courantOnXY");
}

extern "C" void
cuComputeDeltat(double *dt, const hydroparam_t H, hydrowork_t * Hw, hydrovar_t * Hv,
                hydrovarwork_t * Hvw) {
  long j;
  double *uoldDEV, *qDEV, *eDEV, *cDEV, *courantDEV;
  double *courant;
  double deltat;
  double maxCourant = 0;
  long Hnxyt = H.nxyt;
  cudaError_t status;
  long Hmin, Hmax;
  long slices, jend, Hstep, Hnxystep;

  WHERE("compute_deltat");

  //   compute time step on grid interior
  Hnxystep = H.nxystep;
  Hmin = H.jmin + ExtraLayer;
  Hmax = H.jmax - ExtraLayer;

  Hstep = H.nxystep;

#warning "hidden bug here"
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  // The next =1 is because a bug I haven't figured out yet prevents
  // the computation of *dt. Normally, it should be Hstep = H.nxystep;
  // only. If Hstep=1, results are always OK. I give up for the time
  // being.

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  Hstep = 1;
  if (Hstep < 1)
    Hstep = 1;

  // on recupere les buffers du device qui sont deja alloues
  cuGetUoldQECDevicePtr(&uoldDEV, &qDEV, &eDEV, &cDEV);
  status = cudaMalloc((void **) &courantDEV, Hnxystep * H.nxyt * sizeof(double));
  VERIF(status, "cudaMalloc cuComputeDeltat");
  status = cudaMemset(courantDEV, 0, Hnxystep * H.nxyt * sizeof(double));
  VERIF(status, "cudaMemset cuComputeDeltat");

  double *qIDDEV = &qDEV[IHVWS(0, 0, ID)];
  double *qIPDEV = &qDEV[IHVWS(0, 0, IP)];

  for (j = Hmin; j < Hmax; j += Hstep) {
    jend = j + Hstep;
    if (jend >= Hmax)
      jend = Hmax;
    slices = jend - j;

    cuComputeQEforRows(j, uoldDEV, qDEV, eDEV, H.smallr, H.nx, H.nxt, H.nyt, H.nxyt, slices,
                       Hnxystep);
    cuEquationOfState(0, H.nx, H.smallc, H.gamma, H.nxyt, slices, qIDDEV, eDEV, qIPDEV, cDEV);
    // on calcule courant pour chaque cellule de la ligne pour tous les j
    cuCourantOnXY(courantDEV, H.nx, H.nxyt, slices, Hnxystep, cDEV, qDEV, H.smallc);
  }

  //   courant = (double *) malloc(H.nxyt * Hnxystep * sizeof(double));
  //   cudaMemcpy(courant, courantDEV, H.nxyt * Hnxystep * sizeof(double), cudaMemcpyDeviceToHost);
  //   printarray(stdout, courant, H.nxyt, "Courant", H);
  //   free(courant);
  // on cherche le max global des max locaux
  maxCourant = reduceMax(courantDEV, H.nxyt * Hstep);

  deltat = H.courant_factor * H.dx / maxCourant;
  *dt = deltat;
  cudaFree(courantDEV);
  // fprintf(stdout, "compute_deltat: %lg %lg %lg %lg\n", maxCourant, H.courant_factor, H.dx, deltat);
}                               // compute_deltat


//EOF
