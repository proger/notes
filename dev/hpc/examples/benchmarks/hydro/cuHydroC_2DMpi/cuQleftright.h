#ifndef CUQLEFTRIGHT_H_INCLUDED
#define CUQLEFTRIGHT_H_INCLUDED

void
  cuQleftright(const long idim, const long Hnx, const long Hny, const long Hnxyt, const long Hnvar, const int slices, const int Hnxystep,// 
               double *RESTRICT qxmDEV,         // [Hnvar][Hnxystep][Hnxyt]
               double *RESTRICT qxpDEV,         // [Hnvar][Hnxystep][Hnxyt]
               double *RESTRICT qleftDEV,       // [Hnvar][Hnxystep][Hnxyt]
               double *RESTRICT qrightDEV       // [Hnvar][Hnxystep][Hnxyt]
  );

#endif // QLEFTRIGHT_H_INCLUDED
