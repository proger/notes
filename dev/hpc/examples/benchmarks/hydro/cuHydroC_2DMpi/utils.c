/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/time.h>

#include "utils.h"

double **
allocate(long imin, long imax, long nvar)
{
  long i;

#ifdef FAST
  double **r = (double **) malloc(nvar * sizeof(double *));

#else /*  */
  double **r = (double **) calloc(nvar, sizeof(double *));

#endif /*  */
  assert(r != NULL);
  for (i = 0; i < nvar; i++) {
    r[i] = DMalloc(imax - imin + 1 + MallocGuard);
  }
  return r;
}

double *
DMalloc(long n)
{

#ifdef FAST
  double *r = (double *) malloc((n + MallocGuard) * sizeof(double));

#else /*  */
  double *r = (double *) calloc((n + MallocGuard), sizeof(double));

#endif /*  */
  assert(r != NULL);
  return r;
}

long *
IMalloc(long n)
{

#ifdef FAST
  long *r = (long *) malloc((n + MallocGuard) * sizeof(long));

#else /*  */
  long *r = (long *) calloc((n + MallocGuard), sizeof(long));

#endif /*  */
  assert(r != NULL);
  return r;
}


#include "parametres.h"
#define VALPERLINE 11
void
printuold(FILE * fic, const hydroparam_t H, hydrovar_t * Hv)
{
  long i, j, nvar;
  for (nvar = 0; nvar < H.nvar; nvar++) {
    fprintf(fic, "=uold %ld >\n", nvar);
    for (j = 0; j < H.nyt; j++) {
      long nbr = 1;
      for (i = 0; i < H.nxt; i++) {
        fprintf(fic, "%13.6e ", Hv->uold[IHv(i, j, nvar)]);
        nbr++;
        if (nbr == VALPERLINE) {
          fprintf(fic, "\n");
          nbr = 1;
        }
      }
      if (nbr != 1)
        fprintf(fic, "\n");
      fprintf(fic, "%%\n");
    }
  }
}

void
printarray(FILE * fic, double *a, long n, const char *nom, const hydroparam_t H)
{
  int Hnxystep = H.nxystep;
  int Hnxyt = H.nxyt;
  long i,j, nbr = 1;
  fprintf(fic, "=%s >\n", nom);
  for (j = 0; j < H.nxystep; j++) {
    nbr = 1;
    for (i = 0; i < n; i++) {
      fprintf(fic, "%13.6e ", a[IHS(i,j)]);
      nbr++;
      if (nbr == VALPERLINE) {
	fprintf(fic, "\n");
	nbr = 1;
      }
    }
    if (nbr != 1)
      fprintf(fic, "\n");
    fprintf(fic, "-J-\n");
  }
  fprintf(fic, "---\n");
}

void
printarrayi(FILE * fic, long *a, long n, const char *nom)
{
  long i, nbr = 1;
  fprintf(fic, "=%s >\n", nom);
  for (i = 0; i < n; i++) {
    fprintf(fic, "%4ld ", a[i]);
    nbr++;
    if (nbr == VALPERLINE) {
      fprintf(fic, "\n");
      nbr = 1;
    }
  }
  if (nbr != 1)
    fprintf(fic, "\n");
}

void
printarrayv(FILE * fic, double *a, long n, const char *nom, const hydroparam_t H)
{
  long i, nbr = 1;
  long nvar;
  fprintf(fic, "=%s >\n", nom);
  for (nvar = 0; nvar < H.nvar; nvar++) {
    nbr = 1;
    for (i = 0; i < n; i++) {
      fprintf(fic, "%13.6e ", a[IHvw(i, nvar)]);
      nbr++;
      if (nbr == VALPERLINE) {
        fprintf(fic, "\n");
        nbr = 1;
      }
    }
    if (nbr != 1)
      fprintf(fic, "\n");
    fprintf(fic, "-J-\n");
  }
  fprintf(fic, "---\n");
}

void
printarrayv2(FILE * fic, double *a, long n, const char *nom, const hydroparam_t H)
{
  long i, j, nbr = 1;
  long nvar;
  int Hnxystep = H.nxystep;
  int Hnxyt = H.nxyt;
  fprintf(fic, "=%s >\n#", nom);
  for (nvar = 0; nvar < H.nvar; nvar++) {
    for (j = 0; j < Hnxystep; j++) {
      nbr = 1;
      for (i = 0; i < n; i++) {
	fprintf(fic, "%13.6le ", a[IHVWS(i, j, nvar)]);
	nbr++;
	if (nbr == VALPERLINE) {
	  fprintf(fic, "\n#");
	  nbr = 1;
	}
      }
      if (nbr != 1)
	fprintf(fic, "@\n#");
    }
    fprintf(fic, "-J-\n#");
  }
  fprintf(fic, "---\n");
}

void
timeToString(char *buf, const double timeInS)
{
  char ctenth[10];
  long hour = timeInS / 3600;
  long minute = (timeInS - hour * 3600) / 60;
  long second = timeInS - hour * 3600 - minute * 60;
  float tenth = timeInS - hour * 3600 - minute * 60 - second;
  sprintf(ctenth, "%.3f", tenth);
  sprintf(buf, "%02ld:%02ld:%02ld%s", hour, minute, second, &ctenth[1]);
} double
cclock(void)
{
  const double micro = 1.0e-06; /* Conversion constant */
  static long start = 0L, startu;
  struct timeval tp;            /* Structure used by gettimeofday */
  double wall_time;             /* To hold the result */
  if (gettimeofday(&tp, NULL) == -1)
    wall_time = -1.0e0;

  else if (!start) {
    start = tp.tv_sec;
    startu = tp.tv_usec;
    wall_time = 0.0e0;
  } else
    wall_time = (double) (tp.tv_sec - start) + micro * (tp.tv_usec - startu);
  return wall_time;
}


//EOF
