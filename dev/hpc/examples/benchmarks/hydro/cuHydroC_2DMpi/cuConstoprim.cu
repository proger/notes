/*
     A simple 2D hydro code
     (C) Romain Teyssier : CEA/IRFU           -- original F90 code
     (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
     (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
   */
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <cuda.h>
#include "parametres.h"
#include "utils.h"
#include "gridfuncs.h"
#include "cuConstoprim.h"


__global__ void
Loop1KcuConstoprim(const long n, const long Hnxyt, const double Hsmallr, const long Hnvar, const int slices, const int Hnxystep,     //
                   double *RESTRICT u,  // [Hnvar][Hnxystep][Hnxyt]
                   double *RESTRICT q,  // [Hnvar][Hnxystep][Hnxyt]
                   double *RESTRICT e   //        [Hnxystep][Hnxyt] 
  ) {
  double eken;
  long i, j, idx;
  idx = idx1d();
  j = idx / Hnxyt;
  i = idx % Hnxyt;

  if (j >= slices)
    return;
  if (i >= n) return;

  q[IHVWS(i,j,ID)] = MAX(u[IHVWS(i,j,ID)], Hsmallr);
  q[IHVWS(i,j,IU)] = u[IHVWS(i,j,IU)] / q[IHVWS(i,j,ID)];
  q[IHVWS(i,j,IV)] = u[IHVWS(i,j,IV)] / q[IHVWS(i,j,ID)];
  eken = half * (Square(q[IHVWS(i,j,IU)]) + Square(q[IHVWS(i,j,IV)]));
  q[IHVWS(i,j,IP)] = u[IHVWS(i,j,IP)] / q[IHVWS(i,j,ID)] - eken;
  e[IHS(i,j)] = q[IHVWS(i,j,IP)];
}

__global__ void
Loop2KcuConstoprim(const long n, const long Hnxyt, const long Hnvar, const int slices, const int Hnxystep,  //
                   double *RESTRICT u,  // [Hnvar][Hnxystep][Hnxyt]
                   double *RESTRICT q   // [Hnvar][Hnxystep][Hnxyt]
  ) {
  long IN;
  long i, j;
  idx2d(i, j, Hnxyt);
  if (j >= slices)
    return;
  if (i >= n) return;

  for (IN = IP + 1; IN < Hnvar; IN++) {
    q[IHVWS(i,j,IN)] = u[IHVWS(i,j,IN)] / q[IHVWS(i,j,IN)];
  }
}

void
cuConstoprim(const long n, const long Hnxyt, const long Hnvar, const double Hsmallr, const int slices, const int Hnxystep,  //
             double *RESTRICT uDEV,     // [Hnvar][Hnxystep][Hnxyt]
             double *RESTRICT qDEV,     // [Hnvar][Hnxystep][Hnxyt]
             double *RESTRICT eDEV      //        [Hnxystep][Hnxyt]
  ) {
  dim3 grid, block;
  WHERE("constoprim");
  SetBlockDims(Hnxyt * slices, THREADSSZ, block, grid);
  Loop1KcuConstoprim <<< grid, block >>> (n, Hnxyt, Hsmallr, Hnvar, slices, Hnxystep, uDEV, qDEV, eDEV);
  CheckErr("Loop1KcuConstoprim");
  if (Hnvar > IP + 1) {
    Loop2KcuConstoprim <<< grid, block >>> (n, Hnxyt, Hnvar, slices, Hnxystep,uDEV, qDEV);
    CheckErr("Loop2KcuConstoprim");
  }
  cudaThreadSynchronize();
  CheckErr("After synchronize cuConstoprim");
}                               // constoprim


#undef IHS
#undef IHVW
#undef IHVWS
//EOF
