#ifndef CUSLOPE_H_INCLUDED
#define CUSLOPE_H_INCLUDED

void cuSlope(const long narray, const long Hnvar, const long Hnxyt, const double slope_type, const int slices, const int Hnxystep,  // 
             double *RESTRICT qDEV,     // [Hnvar][Hnxystep][Hnxyt]
             double *RESTRICT dqDEV     // [Hnvar][Hnxystep][Hnxyt]
  );

#endif // SLOPE_H_INCLUDED
