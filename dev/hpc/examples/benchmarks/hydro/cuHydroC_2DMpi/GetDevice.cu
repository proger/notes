//
// (C) Guillaume.Colin-de-Verdiere at cea.fr
//

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <mpi.h>
#include <cuda.h>

#include "GetDevice.h"
#include "gridfuncs.h"
#ifdef WITHMPI

typedef struct _hosts {
  char hostname[256];
  int hostnum;
  int nbdevice;
} hosts_t;

static int
SortHost(const void *a, const void *b)
{
  hosts_t *ha = (hosts_t *) a, *hb = (hosts_t *) b;
  return strcmp(ha->hostname, hb->hostname);
}

// Fortran interface
void
getdevice_(int *nbdevice, int *thedev)
{
  int device = -2;
  device = GetDevice(*nbdevice);
  *thedev = device;
}

int DeviceSet(void)
{
  cudaError_t status;
  int ndev = GetDeviceCount();
  if (ndev == -1) MPI_Abort(MPI_COMM_WORLD, 1);
  int mydev = GetDevice(ndev);
  if (mydev == -1) {
    hosts_t h;
    gethostname(h.hostname, 256);
    fprintf(stderr, "Invalid MPI partition : no device left on %s\n", h.hostname);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  status = cudaSetDevice(mydev);
  if (status != cudaSuccess) {
    CheckErr("cudaSetDevice");
    return -1;
  }
  return 0;
}

int GetDeviceCount(void)
{
  cudaError_t status;
  int deviceCount;
  status = cudaGetDeviceCount(&deviceCount);
  if (status != cudaSuccess) {
    CheckErr("cudaGetDeviceCount");
    return -1;
  }
  if (deviceCount == 0) {
    printf("There is no device supporting CUDA\n");
    return -1;
  }
  return deviceCount;
}

int
GetDevice(int nbdevice)
{
  int i, seen;
  int mpi_rank;
  int mpi_size;
  hosts_t h, *hlist;
  MPI_Status st;
  int Tag = 54321;
  int thedev = -1;
  char message[1024];

  // get MPI geometry
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
  // set local parameters
  gethostname(h.hostname, 256);
  h.hostnum = mpi_rank;
  h.nbdevice = nbdevice;

  // Get the global list
  MPI_Barrier(MPI_COMM_WORLD);
  hlist = (hosts_t *) calloc(mpi_size, sizeof(hosts_t));

  if (mpi_rank == 0) {
    memcpy(&hlist[0], &h, sizeof(h));
    for (i = 1; i < mpi_size; i++) {
      MPI_Recv(&hlist[i], sizeof(h), MPI_BYTE, i, Tag, MPI_COMM_WORLD,
	       &st);
    }
  } else {
    MPI_Send(&h, sizeof(h), MPI_BYTE, 0, Tag, MPI_COMM_WORLD);
  }
  MPI_Barrier(MPI_COMM_WORLD);

  // sort and broadcast the list 
  if (mpi_rank == 0) {
    qsort(hlist, mpi_size, sizeof(hosts_t), &SortHost);
    for (i = 0; i < mpi_size; i++) {
      sprintf(message, "-- %s -- rank=%d -- nb_dev=%d\n", hlist[i].hostname,
	      hlist[i].hostnum, hlist[i].nbdevice);
      fputs(message, stderr);
    }
    for (i = 1; i < mpi_size; i++) {
      MPI_Send(hlist, mpi_size * sizeof(hlist[0]), MPI_BYTE, i, Tag,
	       MPI_COMM_WORLD);
    }
  } else {
    MPI_Recv(hlist, mpi_size * sizeof(hlist[0]), MPI_BYTE, 0, Tag,
	     MPI_COMM_WORLD, &st);
  }
  MPI_Barrier(MPI_COMM_WORLD);

  // look for our entry and see if we can have a device
  seen = 0;
  thedev = -1;
  for (i = 0; i < mpi_size; i++) {
    if (strcmp(h.hostname, hlist[i].hostname) == 0) {
      seen++;
      if ((hlist[i].hostnum == mpi_rank) && (seen <= nbdevice)) {
	thedev = seen - 1;
	sprintf(message, "Device selected: %d on %s\n", thedev, h.hostname);
	fputs(message, stderr);
	break;
      }
    }
  }
  free(hlist);
  return thedev;
}

#endif // WITHMPI
//EOF
