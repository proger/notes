#ifndef CUTRACE_H_INCLUDED
#define CUTRACE_H_INCLUDED

void cuTrace(double *RESTRICT qDEV, double *RESTRICT dqDEV,
             double *RESTRICT cDEV, double *RESTRICT qxmDEV,
             double *RESTRICT qxpDEV, const double dtdx, const long n, const long Hscheme, const long Hnvar, const long Hnxyt);

#endif // TRACE_H_INCLUDED
