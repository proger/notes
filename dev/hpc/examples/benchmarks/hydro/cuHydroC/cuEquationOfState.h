#ifndef CUEQUATION_OF_STATE_H_INCLUDED
#define CUEQUATION_OF_STATE_H_INCLUDED

#include "utils.h"
#include "parametres.h"

void cuEquationOfState(double *RESTRICT rhoDEV, double *RESTRICT eintDEV,
                       double *RESTRICT pDEV, double *RESTRICT cDEV, long imin,
                       long imax, const double Hsmallc, const double Hgamma);

#endif // EQUATION_OF_STATE_H_INCLUDED
