#ifndef CUCOMPUTE_DELTAT_H_INCLUDED
#define CUCOMPUTE_DELTAT_H_INCLUDED

#ifdef __cplusplus
extern "C"
#endif
void cuComputeDeltat(double *dt, const hydroparam_t H, hydrowork_t * Hw, hydrovar_t * Hv, hydrovarwork_t * Hvw);

#endif // COMPUTE_DELTAT_H_INCLUDED
