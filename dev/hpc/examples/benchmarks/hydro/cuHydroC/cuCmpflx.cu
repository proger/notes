/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <math.h>
#include <malloc.h>
// #include <unistd.h>
// #include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "cuCmpflx.h"
#include "gridfuncs.h"

#define IHVW(i,v) ((i) + (v) * Hnxyt)

__global__ void
Loop1KcuCmpflx(double *RESTRICT qgdnv, double *RESTRICT flux, const long narray, const long Hnxyt, const double Hgamma)
{
  double entho, ekin, etot;
  long i = idx1d();
  if (i >= narray)
    return;

  entho = one / (Hgamma - one);
  // Mass density
  flux[IHVW(i, ID)] = qgdnv[IHVW(i, ID)] * qgdnv[IHVW(i, IU)];
  // Normal momentum
  flux[IHVW(i, IU)] = flux[IHVW(i, ID)] * qgdnv[IHVW(i, IU)] + qgdnv[IHVW(i, IP)];
  // Transverse momentum 1
  flux[IHVW(i, IV)] = flux[IHVW(i, ID)] * qgdnv[IHVW(i, IV)];
  // Total energy
  ekin = half * qgdnv[IHVW(i, ID)] * (Square(qgdnv[IHVW(i, IU)]) + Square(qgdnv[IHVW(i, IV)]));
  etot = qgdnv[IHVW(i, IP)] * entho + ekin;
  flux[IHVW(i, IP)] = qgdnv[IHVW(i, IU)] * (etot + qgdnv[IHVW(i, IP)]);
}

__global__ void
Loop2KcuCmpflx(double *RESTRICT qgdnv, double *RESTRICT flux, const long narray, const long Hnxyt, const long Hnvar)
{
  long IN, i = idx1d();
  if (i >= narray)
    return;

  for (IN = IP + 1; IN < Hnvar; IN++) {
    flux[IHVW(i, IN)] = flux[IHVW(i, IN)] * qgdnv[IHVW(i, IN)];
  }
}

void
cuCmpflx(double *RESTRICT qgdnv, double *RESTRICT flux, const long narray,
         const long Hnxyt, const long Hnvar, const double Hgamma)
{
  dim3 grid, block;

  WHERE("cmpflx");

  SetBlockDims(narray, THREADSSZ, block, grid);

  // Compute fluxes
  Loop1KcuCmpflx <<< grid, block >>> (qgdnv, flux, narray, Hnxyt, Hgamma);
  CheckErr("Loop1KcuCmpflx");
  // Other advected quantities
  if (Hnvar > IP + 1) {
    Loop2KcuCmpflx <<< grid, block >>> (qgdnv, flux, narray, Hnxyt, Hnvar);
    CheckErr("Loop2KcuCmpflx");
  }
  cudaThreadSynchronize();
  CheckErr("After synchronize cuCmpflx");
}                               // cmpflx


#undef IHVW

//EOF
