/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "gridfuncs.h"
#include "cuQleftright.h"

#define IHVW(i,v) ((i) + (v) * Hnxyt)

__global__ void
Loop1KcuQleftright(const long bmax, const long Hnvar, const long Hnxyt,
                   double *RESTRICT qxm, double *RESTRICT qxp, double *RESTRICT qleft, double *RESTRICT qright)
{
  long nvar;
  long i = idx1d();
  if (i >= bmax)
    return;

  for (nvar = 0; nvar < Hnvar; nvar++) {
    qleft[IHVW(i, nvar)] = qxm[IHVW(i + 1, nvar)];
    qright[IHVW(i, nvar)] = qxp[IHVW(i + 2, nvar)];
  }
}

void
cuQleftright(const long idim, const long Hnx, const long Hny, const long Hnxyt,
             const long Hnvar, double *RESTRICT qxm, double *RESTRICT qxp, double *RESTRICT qleft, double *RESTRICT qright)
{
  long bmax;
  dim3 block, grid;
  WHERE("qleftright");
  if (idim == 1) {
    bmax = Hnx + 1;
  } else {
    bmax = Hny + 1;
  }
  SetBlockDims(bmax, THREADSSZ, block, grid);
  Loop1KcuQleftright <<< grid, block >>> (bmax, Hnvar, Hnxyt, qxm, qxp, qleft, qright);
  CheckErr("Loop1KcuQleftright");
  cudaThreadSynchronize();
}

#undef IHVW

// EOF
