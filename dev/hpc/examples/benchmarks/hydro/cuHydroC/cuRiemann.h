#ifndef CURIEMANN_H_INCLUDED
#define CURIEMANN_H_INCLUDED


void cuRiemann(double *RESTRICT qleftDEV, double *RESTRICT qrightDEV,
               double *RESTRICT qgdnvDEV, double *RESTRICT rlDEV,
               double *RESTRICT ulDEV, double *RESTRICT plDEV,
               double *RESTRICT clDEV, double *RESTRICT wlDEV,
               double *RESTRICT rrDEV, double *RESTRICT urDEV,
               double *RESTRICT prDEV, double *RESTRICT crDEV,
               double *RESTRICT wrDEV, double *RESTRICT roDEV,
               double *RESTRICT uoDEV, double *RESTRICT poDEV,
               double *RESTRICT coDEV, double *RESTRICT woDEV,
               double *RESTRICT rstarDEV, double *RESTRICT ustarDEV,
               double *RESTRICT pstarDEV, double *RESTRICT cstarDEV,
               long *RESTRICT sgnmDEV, double *RESTRICT spinDEV,
               double *RESTRICT spoutDEV, double *RESTRICT ushockDEV,
               double *RESTRICT fracDEV, double *RESTRICT scrDEV,
               double *RESTRICT delpDEV, double *RESTRICT poldDEV,
               long *RESTRICT indDEV, long *RESTRICT ind2DEV,
               const long narray,
               const double Hsmallr,
               const double Hsmallc, const double Hgamma, const long Hniter_riemann, const long Hnvar, const long Hnxyt);

#endif // CURIEMANN_H_INCLUDED
