/*
$Id: cuMakeBoundary.cu,v 1.1 2010/04/28 07:02:59 coling Exp coling $
$Log: cuMakeBoundary.cu,v $
Revision 1.1  2010/04/28 07:02:59  coling
Initial revision

*/

/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "cuMakeBoundary.h"
#include "gridfuncs.h"
#include "utils.h"

#ifdef IHv
#undef IHv
#endif

#define IHv(i,j,v) ((i) + (Hnxt * (Hnyt * (v) + (j))))

__global__ void
Loop1KcuMakeBoundary(const long i, const long i0, const double sign, const long Hjmin,
                     const long nx, const long Hnxt, const long Hnyt, const long Hnvar, double *uold)
{
  long j, ivar;
  double vsign = sign;
  idx2d(j, ivar, nx);
  if (ivar >= Hnvar)
    return;

  // recuperation de la conditon qui etait dans la boucle
  if (ivar == IU)
    vsign = -1.0;

  j += (Hjmin + ExtraLayer);
  uold[IHv(i, j, ivar)] = uold[IHv(i0, j, ivar)] * vsign;
}

__global__ void
Loop2KcuMakeBoundary(const long j, const long j0, const double sign, const long Himin,
                     const long nx, const long Hnxt, const long Hnyt, const long Hnvar, double *uold)
{
  long i, ivar;
  double vsign = sign;
  idx2d(i, ivar, nx);
  if (ivar >= Hnvar)
    return;

  // recuperation de la conditon qui etait dans la boucle
  if (ivar == IV)
    vsign = -1.0;

  i += (Himin + ExtraLayer);
  uold[IHv(i, j, ivar)] = uold[IHv(i, j0, ivar)] * vsign;
}


void
cuMakeBoundary(long idim, const hydroparam_t H, hydrovar_t * Hv, double *uoldDEV)
{

  // - - - - - - - - - - - - - - - - - - -
  // Cette portion de code est � v�rifier
  // d�tail. J'ai des doutes sur la conversion
  // des index depuis fortran.
  // - - - - - - - - - - - - - - - - - - -
  dim3 grid, block;
  long i, i0, j, j0;
  long n;
  double sign;
  WHERE("make_boundary");
  if (idim == 1) {

    // Left boundary
    n = ((H.jmax - ExtraLayer) - (H.jmin + ExtraLayer));
    SetBlockDims(n * H.nvar, THREADSSZ, block, grid);
    for (i = 0; i < ExtraLayer; i++) {
      sign = 1.0;
      if (H.boundary_left == 1) {
        i0 = ExtraLayerTot - i - 1;
        //                 if (ivar == IU) {
        //                     sign = -1.0;
        //                 }
      } else if (H.boundary_left == 2) {
        i0 = 2;
      } else {
        i0 = H.nx + i;
      }
      // on traite les deux boucles d'un coup
      Loop1KcuMakeBoundary <<< grid, block >>> (i, i0, sign, H.jmin, n, H.nxt, H.nyt, H.nvar, uoldDEV);
      CheckErr("Loop1KcuMakeBoundary");
      cudaThreadSynchronize();
      CheckErr("After synchronize Loop1KcuMakeBoundary");
    }

    // Right boundary
    for (i = H.nx + ExtraLayer; i < H.nx + ExtraLayerTot; i++) {
      sign = 1.0;
      if (H.boundary_right == 1) {
        i0 = 2 * H.nx + ExtraLayerTot - i - 1;
        //                 if (ivar == IU) {
        //                     sign = -1.0;
        //                 }
      } else if (H.boundary_right == 2) {
        i0 = H.nx + ExtraLayer;
      } else {
        i0 = i - H.nx;
      }
      Loop1KcuMakeBoundary <<< grid, block >>> (i, i0, sign, H.jmin, n, H.nxt, H.nyt, H.nvar, uoldDEV);
      CheckErr("Loop1KcuMakeBoundary 2");
      cudaThreadSynchronize();
      CheckErr("After synchronize Loop1KcuMakeBoundary 2");
    }
  } else {
    n = ((H.imax - ExtraLayer) - (H.imin + ExtraLayer));
    SetBlockDims(n * H.nvar, THREADSSZ, block, grid);
    // Lower boundary
    j0 = 0;
    for (j = 0; j < ExtraLayer; j++) {
      sign = 1.0;
      if (H.boundary_down == 1) {
        j0 = ExtraLayerTot - j - 1;
        //                 if (ivar == IV) {
        //                     sign = -1.0;
        //                 }
      } else if (H.boundary_down == 2) {
        j0 = ExtraLayerTot;
      } else {
        j0 = H.ny + j;
      }
      Loop2KcuMakeBoundary <<< grid, block >>> (j, j0, sign, H.imin, n, H.nxt, H.nyt, H.nvar, uoldDEV);
      CheckErr("Loop2KcuMakeBoundary ");
      cudaThreadSynchronize();
      CheckErr("After synchronize Loop2KcuMakeBoundary ");
    }

    // Upper boundary
    for (j = H.ny + ExtraLayer; j < H.ny + ExtraLayerTot; j++) {
      sign = 1.0;
      if (H.boundary_up == 1) {
        j0 = 2 * H.ny + ExtraLayerTot - j - 1;
        //                 if (ivar == IV) {
        //                     sign = -1.0;
        //                 }
      } else if (H.boundary_up == 2) {
        j0 = H.ny + 1;
      } else {
        j0 = j - H.ny;
      }
      Loop2KcuMakeBoundary <<< grid, block >>> (j, j0, sign, H.imin, n, H.nxt, H.nyt, H.nvar, uoldDEV);
      CheckErr("Loop2KcuMakeBoundary 2");
      cudaThreadSynchronize();
      CheckErr("After synchronize Loop2KcuMakeBoundary 2");
    }
  }
}                               // make_boundary


//EOF
