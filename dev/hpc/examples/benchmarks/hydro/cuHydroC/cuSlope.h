#ifndef CUSLOPE_H_INCLUDED
#define CUSLOPE_H_INCLUDED

void cuSlope(double *RESTRICT qDEV, double *RESTRICT dqDEV,
             const long narray, const long Hnvar, const long Hnxyt, const double slope_type);

#endif // SLOPE_H_INCLUDED
