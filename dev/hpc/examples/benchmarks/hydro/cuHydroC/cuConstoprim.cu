/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <cuda.h>

#include "parametres.h"
#include "utils.h"
#include "gridfuncs.h"
#include "cuConstoprim.h"

#define IHVW(i,v) ((i) + (v) * Hnxyt)

__global__ void
Loop1KcuConstoprim(const long n,
                   double *RESTRICT u, double *RESTRICT q, double *RESTRICT e, const long Hnxyt, const double Hsmallr)
{
  double eken;
  long i = idx1d();
  if (i >= n)
    return;

  q[IHVW(i, ID)] = MAX(u[IHVW(i, ID)], Hsmallr);
  q[IHVW(i, IU)] = u[IHVW(i, IU)] / q[IHVW(i, ID)];
  q[IHVW(i, IV)] = u[IHVW(i, IV)] / q[IHVW(i, ID)];
  eken = half * (Square(q[IHVW(i, IU)]) + Square(q[IHVW(i, IV)]));
  q[IHVW(i, IP)] = u[IHVW(i, IP)] / q[IHVW(i, ID)] - eken;
  e[i] = q[IHVW(i, IP)];
}

__global__ void
Loop2KcuConstoprim(const long n, double *RESTRICT u, double *RESTRICT q, const long Hnxyt, const long Hnvar)
{
  long IN;
  long i = idx1d();
  if (i >= n)
    return;

  for (IN = IP + 1; IN < Hnvar; IN++) {
    q[IHVW(i, IN)] = u[IHVW(i, IN)] / q[IHVW(i, IN)];
  }
}

void
cuConstoprim(double *RESTRICT u, double *RESTRICT q, double *RESTRICT e,
             const long n, const long Hnxyt, const long Hnvar, const double Hsmallr)
{
  dim3 grid, block;

  WHERE("constoprim");
  SetBlockDims(n, THREADSSZ, block, grid);
  Loop1KcuConstoprim <<< grid, block >>> (n, u, q, e, Hnxyt, Hsmallr);
  CheckErr("Loop1KcuConstoprim");
  if (Hnvar > IP + 1) {
    Loop2KcuConstoprim <<< grid, block >>> (n, u, q, Hnxyt, Hnvar);
    CheckErr("Loop2KcuConstoprim");
  }
  cudaThreadSynchronize();
  CheckErr("After synchronize cuConstoprim");
}                               // constoprim


#undef IHVW
//EOF
