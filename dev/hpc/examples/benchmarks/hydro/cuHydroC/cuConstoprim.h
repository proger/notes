#ifndef CUCONSTOPRIM_H_INCLUDED
#define CUCONSTOPRIM_H_INCLUDED

#include "utils.h"

void cuConstoprim(double *RESTRICT uDEV, double *RESTRICT qDEV, double *RESTRICT eDEV,
                  const long n, const long Hnxyt, const long Hnvar, const double Hsmallr);

#endif // CUCONSTOPRIM_H_INCLUDED
