BEGIN { $gpu=0.0; $cpu=0.0; $nbr=0; }
 while (<>) { 
  if (/\[/) {
	$_ =~ s/\] //g;
	@a=split(" ");  $gpu=$gpu + $a[3]; $cpu=$cpu + $a[5]; $nbr++;
	# print $a[3] . " " . $a[5] . "\n";
  }
 }
END { 
	$nom=substr $a[1], 0, 20;
	printf "%14.3f %14.3f %9d <%14.3f> <%14.3f> %s\n", $gpu, $cpu, $nbr, $gpu/$nbr, $cpu/$nbr, $nom; 
}
