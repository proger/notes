#ifndef CUCONSERVAR_H
#define CUCONSERVAR_H

void
  cuGatherConservativeVars(const long idim,
                           const long rowcol,
                           double *RESTRICT uoldDEV,
                           double *RESTRICT uDEV,
                           const long Himin,
                           const long Himax,
                           const long Hjmin,
                           const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt);

void
  cuUpdateConservativeVars(const long idim,
                           const long rowcol,
                           const double dtdx,
                           double *RESTRICT uoldDEV,
                           double *RESTRICT uDEV,
                           double *RESTRICT fluxDEV,
                           const long Himin,
                           const long Himax,
                           const long Hjmin,
                           const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt);
#endif
