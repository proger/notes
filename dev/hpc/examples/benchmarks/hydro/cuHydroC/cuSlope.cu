/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "gridfuncs.h"
#include "cuSlope.h"

#define DABS(x) (double) fabs((x))

#define IHVW(i, v) ((i) + (v) * Hnxyt)

__global__ void
LoopKcuSlope(double *RESTRICT q, double *RESTRICT dq,
             const long Hnvar, const long Hnxyt, const double slope_type, const long ijmin, const long ijmax)
{
  long n;
  double dlft, drgt, dcen, dsgn, slop, dlim;
  long ihvwin, ihvwimn, ihvwipn;

  long i;
  idx2d(i, n, (ijmax - ijmin));

  if (n >= Hnvar)
    return;

  i = i + ijmin;

  ihvwin = IHVW(i, n);
  ihvwimn = IHVW(i - 1, n);
  ihvwipn = IHVW(i + 1, n);
  dlft = slope_type * (q[ihvwin] - q[ihvwimn]);
  drgt = slope_type * (q[ihvwipn] - q[ihvwin]);
  dcen = half * (dlft + drgt) / slope_type;
  dsgn = (dcen > 0) ? (double) 1.0 : (double) -1.0;     // sign(one, dcen);
  slop = (double) MIN(DABS(dlft), DABS(drgt));
  dlim = ((dlft * drgt) <= zero) ? zero : slop;
  //         if ((dlft * drgt) <= zero) {
  //             dlim = zero;
  //         }
  dq[ihvwin] = dsgn * (double) MIN(dlim, DABS(dcen));
}

void
cuSlope(double *RESTRICT q, double *RESTRICT dq, const long narray, const long Hnvar, const long Hnxyt, const double slope_type)
{
  long ijmin, ijmax;
  dim3 grid, block;

  WHERE("slope");
  ijmin = 0;
  ijmax = narray;
  SetBlockDims(((ijmax - 1) - (ijmin + 1)) * Hnvar, THREADSSZ, block, grid);
  LoopKcuSlope <<< grid, block >>> (q, dq, Hnvar, Hnxyt, slope_type, ijmin, ijmax);
  CheckErr("LoopKcuSlope");
  cudaThreadSynchronize();
}                               // slope

#undef IHVW
//EOF
