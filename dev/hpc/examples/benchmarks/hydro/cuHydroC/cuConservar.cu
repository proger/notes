#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "parametres.h"
#include "utils.h"
#include "cuConservar.h"
#include "gridfuncs.h"

#define IHU(i, j, v)  ((i) + Hnxt * ((j) + Hnyt * (v)))
#define IHVW(i, v) ((i) + (v) * Hnxyt)

__global__ void
Loop1KcuGather(double *RESTRICT uold,
               double *RESTRICT u,
               const long rowcol, const long Hnxt, const long Himin, const long Himax, const long Hnyt, const long Hnxyt)
{
  long i = idx1d();
  if (i < Himin)
    return;
  if (i >= Himax)
    return;

  u[IHVW(i, ID)] = uold[IHU(i, rowcol, ID)];
  u[IHVW(i, IU)] = uold[IHU(i, rowcol, IU)];
  u[IHVW(i, IV)] = uold[IHU(i, rowcol, IV)];
  u[IHVW(i, IP)] = uold[IHU(i, rowcol, IP)];
}

__global__ void
Loop2KcuGather(double *RESTRICT uold,
               double *RESTRICT u,
               const long rowcol, const long Hnxt, const long Himin, const long Himax, const long Hnyt, const long Hnxyt)
{
  long i = idx1d();
  if (i < Himin)
    return;
  if (i >= Himax)
    return;

  u[IHVW(i, ID)] = uold[IHU(rowcol, i, ID)];
  u[IHVW(i, IV)] = uold[IHU(rowcol, i, IU)];
  u[IHVW(i, IU)] = uold[IHU(rowcol, i, IV)];
  u[IHVW(i, IP)] = uold[IHU(rowcol, i, IP)];
}

__global__ void
Loop3KcuGather(double *RESTRICT uold,
               double *RESTRICT u,
               const long rowcol,
               const long Hnxt, const long Himin, const long Himax, const long Hnyt, const long Hnxyt, const long Hnvar)
{
  long i = idx1d();
  long ivar;

  if (i < Himin)
    return;
  if (i >= Himax)
    return;

  for (ivar = IP + 1; ivar < Hnvar; ivar++) {
    u[IHVW(i, ivar)] = uold[IHU(i, rowcol, ivar)];
  }
}

__global__ void
Loop4KcuGather(double *RESTRICT uold,
               double *RESTRICT u,
               const long rowcol,
               const long Hnxt, const long Himin, const long Himax, const long Hnyt, const long Hnxyt, const long Hnvar)
{
  long i = idx1d();
  long ivar;

  if (i < Himin)
    return;
  if (i >= Himax)
    return;

  // reconsiderer le calcul d'indices en supprimant la boucle sur ivar et
  // en la ventilant par thread
  for (ivar = IP + 1; ivar < Hnvar; ivar++) {
    u[IHVW(i, ivar)] = uold[IHU(rowcol, i, ivar)];
  }
}
void
cuGatherConservativeVars(const long idim, const long rowcol,
                         double *RESTRICT uold,
                         double *RESTRICT u,
                         const long Himin,
                         const long Himax,
                         const long Hjmin, const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  dim3 grid, block;


  WHERE("gatherConservativeVars");
  if (idim == 1) {
    // Gather conservative variables
    SetBlockDims((Himax - Himin), THREADSSZ, block, grid);
    Loop1KcuGather <<< grid, block >>> (uold, u, rowcol, Hnxt, Himin, Himax, Hnyt, Hnxyt);

    if (Hnvar > IP + 1) {
      Loop3KcuGather <<< grid, block >>> (uold, u, rowcol, Hnxt, Himin, Himax, Hnyt, Hnxyt, Hnvar);
    }
  } else {
    // Gather conservative variables
    SetBlockDims((Hjmax - Hjmin), THREADSSZ, block, grid);
    Loop2KcuGather <<< grid, block >>> (uold, u, rowcol, Hnxt, Hjmin, Hjmax, Hnyt, Hnxyt);
    if (Hnvar > IP + 1) {
      Loop4KcuGather <<< grid, block >>> (uold, u, rowcol, Hnxt, Hjmin, Hjmax, Hnyt, Hnxyt, Hnvar);
    }
  }
}

__global__ void
Loop1KcuUpdate(const long rowcol, const double dtdx,
               double *RESTRICT uold,
               double *RESTRICT u,
               double *RESTRICT flux, const long Himin, const long Himax, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  long i = idx1d();


  if (i < (Himin + ExtraLayer))
    return;
  if (i >= (Himax - ExtraLayer))
    return;

  uold[IHU(i, rowcol, ID)] = u[IHVW(i, ID)] + (flux[IHVW(i - 2, ID)] - flux[IHVW(i - 1, ID)]) * dtdx;
  uold[IHU(i, rowcol, IU)] = u[IHVW(i, IU)] + (flux[IHVW(i - 2, IU)] - flux[IHVW(i - 1, IU)]) * dtdx;
  uold[IHU(i, rowcol, IV)] = u[IHVW(i, IV)] + (flux[IHVW(i - 2, IV)] - flux[IHVW(i - 1, IV)]) * dtdx;
  uold[IHU(i, rowcol, IP)] = u[IHVW(i, IP)] + (flux[IHVW(i - 2, IP)] - flux[IHVW(i - 1, IP)]) * dtdx;
}

__global__ void
Loop2KcuUpdate(const long rowcol, const double dtdx,
               double *RESTRICT uold,
               double *RESTRICT u,
               double *RESTRICT flux,
               const long Himin, const long Himax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  long i = idx1d();
  long ivar;

  if (i < (Himin + ExtraLayer))
    return;
  if (i >= (Himax - ExtraLayer))
    return;

  for (ivar = IP + 1; ivar < Hnvar; ivar++) {
    uold[IHU(i, rowcol, ivar)] = u[IHVW(i, ivar)] + (flux[IHVW(i - 2, ivar)] - flux[IHVW(i - 1, ivar)]) * dtdx;
  }
}

__global__ void
Loop3KcuUpdate(const long rowcol, const double dtdx,
               double *RESTRICT uold,
               double *RESTRICT u,
               double *RESTRICT flux, const long Hjmin, const long Hjmax, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  long j = idx1d();

  if (j < (Hjmin + ExtraLayer))
    return;
  if (j >= (Hjmax - ExtraLayer))
    return;

  uold[IHU(rowcol, j, ID)] = u[IHVW(j, ID)] + (flux[IHVW(j - 2, ID)] - flux[IHVW(j - 1, ID)]) * dtdx;
  uold[IHU(rowcol, j, IP)] = u[IHVW(j, IP)] + (flux[IHVW(j - 2, IP)] - flux[IHVW(j - 1, IP)]) * dtdx;
  uold[IHU(rowcol, j, IV)] = u[IHVW(j, IU)] + (flux[IHVW(j - 2, IU)] - flux[IHVW(j - 1, IU)]) * dtdx;
  uold[IHU(rowcol, j, IU)] = u[IHVW(j, IV)] + (flux[IHVW(j - 2, IV)] - flux[IHVW(j - 1, IV)]) * dtdx;
}

__global__ void
Loop4KcuUpdate(const long rowcol, const double dtdx,
               double *RESTRICT uold,
               double *RESTRICT u,
               double *RESTRICT flux,
               const long Hjmin, const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  long j = idx1d();
  long ivar;

  if (j < (Hjmin + ExtraLayer))
    return;
  if (j >= (Hjmax - ExtraLayer))
    return;

  for (ivar = IP + 1; ivar < Hnvar; ivar++) {
    uold[IHU(rowcol, j, ivar)] = u[IHVW(j, ivar)] + (flux[IHVW(j - 2, ivar)] - flux[IHVW(j - 1, ivar)]) * dtdx;
  }
}

void
cuUpdateConservativeVars(const long idim, const long rowcol, const double dtdx,
                         double *RESTRICT uold,
                         double *RESTRICT u,
                         double *RESTRICT flux,
                         const long Himin,
                         const long Himax,
                         const long Hjmin, const long Hjmax, const long Hnvar, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  dim3 grid, block;
  WHERE("updateConservativeVars");

  if (idim == 1) {
    SetBlockDims((Himax - ExtraLayer) - (Himin + ExtraLayer), THREADSSZ, block, grid);
    // Update conservative variables
    Loop1KcuUpdate <<< grid, block >>> (rowcol, dtdx, uold, u, flux, Himin, Himax, Hnxt, Hnyt, Hnxyt);
    CheckErr("Loop1KcuUpdate");
    if (Hnvar > IP + 1) {
      Loop2KcuUpdate <<< grid, block >>> (rowcol, dtdx, uold, u, flux, Himin, Himax, Hnvar, Hnxt, Hnyt, Hnxyt);
      CheckErr("Loop2KcuUpdate");
    }
  } else {
    // Update conservative variables
    SetBlockDims((Hjmax - ExtraLayer) - (Hjmin + ExtraLayer), THREADSSZ, block, grid);
    Loop3KcuUpdate <<< grid, block >>> (rowcol, dtdx, uold, u, flux, Hjmin, Hjmax, Hnxt, Hnyt, Hnxyt);
    CheckErr("Loop3KcuUpdate");
    if (Hnvar > IP + 1) {
      Loop4KcuUpdate <<< grid, block >>> (rowcol, dtdx, uold, u, flux, Hjmin, Hjmax, Hnvar, Hnxt, Hnyt, Hnxyt);
      CheckErr("Loop4KcuUpdate");
    }
  }
  cudaThreadSynchronize();
  CheckErr("cudaThreadSynchronize");
}

#undef IHVW
#undef IHU

//EOF
