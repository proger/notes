#ifndef GRIDFUNCS_H
#define GRIDFUNCS_H

inline __device__ long
idx1dl(void)
{
  return threadIdx.y * blockDim.x + threadIdx.x;
}

inline __device__ long
idx1d(void)
{
  return blockIdx.y * (gridDim.x * blockDim.x) + blockDim.x * blockIdx.x + threadIdx.x;
}

inline __device__ void
idx2d(long &x, long &y, const long nx)
{
  long i1d = idx1d();
  y = i1d / nx;
  x = i1d - y * nx;
}

inline __device__ long
blcknum1d(void)
{
  return blockIdx.y * gridDim.x + blockIdx.x;
}

inline __device__ long
nbblcks(void)
{
  return gridDim.y * gridDim.x;
}

#define THREADSSZ 128
#define THREADSSZs 64

void SetBlockDims(long nelmts, long NTHREADS, dim3 & block, dim3 & grid);
void CheckErr(const char *where);
void initDevice(long myCard);
void releaseDevice(long myCard);
long getDeviceCapability(int *nDevice, long *maxMemOnDevice, long *maxThreads);

double reduceMax(double *array, long nb);
#endif
