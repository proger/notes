/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdio.h>
// #include <stdlib.h>
#include <malloc.h>
// #include <unistd.h>
#include <math.h>

#include "parametres.h"
#include "cuComputeDeltat.h"
#include "cuHydroGodunov.h"
#include "gridfuncs.h"
#include "utils.h"
#include "cuda.h"
#include "cuEquationOfState.h"

#define DABS(x) (double) fabs((x))
#define IHV(i, j, v)  ((i) + Hnxt * ((j) + Hnyt * (v)))
#define IHVW(i, v) ((i) + (v) * Hnxyt)
#define VERIF(x, ou) if ((x) != cudaSuccess)  { CheckErr((ou)); }

__global__ void
LoopKQEforRow(const long j, double *uold, double *q, double *e, const double Hsmallr,
              const long Hnxt, const long Hnyt, const long Hnxyt, const long n)
{
  double eken;
  long i = idx1d();

  if (i >= n)
    return;

  long idxuID = IHV(i + ExtraLayer, j, ID);
  long idxuIU = IHV(i + ExtraLayer, j, IU);
  long idxuIV = IHV(i + ExtraLayer, j, IV);
  long idxuIP = IHV(i + ExtraLayer, j, IP);
  q[IHVW(i, ID)] = MAX(uold[idxuID], Hsmallr);
  q[IHVW(i, IU)] = uold[idxuIU] / q[IHVW(i, ID)];
  q[IHVW(i, IV)] = uold[idxuIV] / q[IHVW(i, ID)];
  eken = half * (Square(q[IHVW(i, IU)]) + Square(q[IHVW(i, IV)]));
  q[IHVW(i, IP)] = uold[idxuIP] / q[IHVW(i, ID)] - eken;
  e[i] = q[IHVW(i, IP)];
}

void
cuComputeQEforRow(const long j, double *uold, double *q, double *e,
                  const double Hsmallr, const long Hnx, const long Hnxt, const long Hnyt, const long Hnxyt)
{
  dim3 grid, block;

  SetBlockDims(Hnx, THREADSSZ, block, grid);
  LoopKQEforRow <<< grid, block >>> (j, uold, q, e, Hsmallr, Hnxt, Hnyt, Hnxyt, Hnx);
  CheckErr("courantOnXY");
  cudaThreadSynchronize();
  CheckErr("courantOnXY");
}

__global__ void
LoopKcourant(double *q, double *courant, const double Hsmallc, const double *c, const long Hnxyt, const long n)
{
  double cournox, cournoy, courantl;
  long i = idx1d();

  if (i >= n)
    return;

  cournox = cournoy = 0.;

  cournox = c[i] + DABS(q[IHVW(i, IU)]);
  cournoy = c[i] + DABS(q[IHVW(i, IV)]);
  courantl = MAX(cournox, MAX(cournoy, Hsmallc));
  courant[i] = MAX(courant[i], courantl);
}

void
cuCourantOnXY(double *courant, const long Hnx, const long Hnxyt, double *c, double *q, double Hsmallc)
{
  dim3 grid, block;

  SetBlockDims(Hnx, THREADSSZ, block, grid);
  LoopKcourant <<< grid, block >>> (q, courant, Hsmallc, c, Hnxyt, Hnx);
  CheckErr("courantOnXY");
  cudaThreadSynchronize();
  CheckErr("courantOnXY");
}

extern "C" void
cuComputeDeltat(double *dt, const hydroparam_t H, hydrowork_t * Hw, hydrovar_t * Hv, hydrovarwork_t * Hvw)
{
  long j;
  double *uoldDEV, *qDEV, *eDEV, *cDEV, *courantDEV;
  double maxCourant;
  long Hnxyt = H.nxyt;
  cudaError_t status;

  WHERE("compute_deltat");

  //   compute time step on grid interior

  // on recupere les buffers du device qui sont deja alloues
  cuGetUoldQECDevicePtr(&uoldDEV, &qDEV, &eDEV, &cDEV);
  status = cudaMalloc((void **) &courantDEV, H.nxyt * sizeof(double));
  VERIF(status, "cudaMalloc cuComputeDeltat");
  status = cudaMemset(courantDEV, 0, H.nxyt * sizeof(double));
  VERIF(status, "cudaMemset cuComputeDeltat");

  double *qIDDEV = &qDEV[IHVW(0, ID)];
  double *qIPDEV = &qDEV[IHVW(0, IP)];

  for (j = H.jmin + ExtraLayer; j < H.jmax - ExtraLayer; j++) {
    cuComputeQEforRow(j, uoldDEV, qDEV, eDEV, H.smallr, H.nx, H.nxt, H.nyt, H.nxyt);
    cuEquationOfState(qIDDEV, eDEV, qIPDEV, cDEV, 0, H.nx, H.smallc, H.gamma);
    // on calcule courant pour chaque cellule de la ligne pour tous les j
    cuCourantOnXY(courantDEV, H.nx, H.nxyt, cDEV, qDEV, H.smallc);
  }

  // on cherche le max global des max locaux
  maxCourant = reduceMax(courantDEV, H.nx);

  *dt = H.courant_factor * H.dx / maxCourant;
  cudaFree(courantDEV);
  // fprintf(stdout, "%g %g %g %g\n", cournox, cournoy, H.smallc, H.courant_factor);
}                               // compute_deltat


//EOF
