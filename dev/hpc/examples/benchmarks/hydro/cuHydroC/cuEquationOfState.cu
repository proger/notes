/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

// #include <stdlib.h>
// #include <unistd.h>
#include <math.h>
#include <stdio.h>

#include "gridfuncs.h"
#include "cuEquationOfState.h"
#include "utils.h"

__global__ void
LoopEOS(double *RESTRICT rho, double *RESTRICT eint,
        double *RESTRICT p, double *RESTRICT c, const long imin, const long imax, const double Hsmallc, const double Hgamma)
{
  double smallp;
  long k = idx1d();

  if (k < imin)
    return;
  if (k >= imax)
    return;

  smallp = Square(Hsmallc) / Hgamma;
  p[k] = (Hgamma - one) * rho[k] * eint[k];
  p[k] = MAX(p[k], (double) (rho[k] * smallp));
  c[k] = sqrt(Hgamma * p[k] / rho[k]);
}

void
cuEquationOfState(double *RESTRICT rho, double *RESTRICT eint,
                  double *RESTRICT p, double *RESTRICT c, long imin, long imax, const double Hsmallc, const double Hgamma)
{
  dim3 grid, block;
  WHERE("equation_of_state");
  SetBlockDims((imax - imin), THREADSSZ, block, grid);
  LoopEOS <<< grid, block >>> (rho, eint, p, c, imin, imax, Hsmallc, Hgamma);
  CheckErr("LoopEOS");
  cudaThreadSynchronize();
  CheckErr("LoopEOS");
}                               // equation_of_state


// EOF
