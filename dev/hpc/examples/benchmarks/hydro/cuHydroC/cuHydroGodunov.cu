/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <cuda.h>

extern "C" {
#include "parametres.h"
#include "hydro_funcs.h"
#include "utils.h"
#include "make_boundary.h"

#include "cmpflx.h"
#include "conservar.h"
#include "equation_of_state.h"
#include "qleftright.h"
#include "constoprim.h"
#include "riemann.h"
#include "trace.h"
#include "slope.h"
};

#include "cuHydroGodunov.h"
#include "cuConservar.h"
#include "cuConstoprim.h"
#include "cuSlope.h"
#include "cuTrace.h"
#include "cuEquationOfState.h"
#include "cuQleftright.h"
#include "cuRiemann.h"
#include "cuCmpflx.h"
#include "cuMakeBoundary.h"
#include "gridfuncs.h"

#define VERIF(x, ou) if ((x) != cudaSuccess)  { CheckErr((ou)); }

// Variables DEVice 
double *uoldDEV = 0, *uDEV = 0, *eDEV = 0, *qDEV = 0, *dqDEV = 0, *cDEV = 0, *qxpDEV = 0, *qxmDEV = 0;
double *qleftDEV = 0, *qrightDEV = 0, *qgdnvDEV = 0, *fluxDEV = 0;
double *rlDEV = 0, *ulDEV = 0, *plDEV = 0, *clDEV = 0, *wlDEV = 0, *rrDEV = 0, *urDEV = 0;
double *prDEV = 0, *crDEV = 0, *wrDEV = 0, *roDEV = 0, *uoDEV = 0, *poDEV = 0, *coDEV = 0, *woDEV = 0;
double *rstarDEV = 0, *ustarDEV = 0, *pstarDEV = 0, *cstarDEV = 0;
long *sgnmDEV = 0;
double *spinDEV = 0, *spoutDEV = 0, *ushockDEV = 0, *fracDEV = 0, *scrDEV = 0, *delpDEV = 0, *poldDEV = 0;
long *indDEV = 0, *ind2DEV = 0;

void
cuGetUoldQECDevicePtr(double **uoldDEV_p, double **qDEV_p, double **eDEV_p, double **cDEV_p)
{
  *uoldDEV_p = uoldDEV;
  *qDEV_p = qDEV;
  *eDEV_p = eDEV;
  *cDEV_p = cDEV;
}

extern "C" void
cuPutUoldOnDevice(const hydroparam_t H, hydrovar_t * Hv)
{
  cudaError_t status;
  status = cudaMemcpy(uoldDEV, Hv->uold, H.arUoldSz * sizeof(double), cudaMemcpyHostToDevice);
  VERIF(status, "cmcpy H2D uoldDEV");
}

extern "C" void
cuGetUoldFromDevice(const hydroparam_t H, hydrovar_t * Hv)
{
  cudaError_t status;
  status = cudaMemcpy(Hv->uold, uoldDEV, H.arUoldSz * sizeof(double), cudaMemcpyDeviceToHost);
  VERIF(status, "cmcpy D2H uoldDEV");
}

extern "C" void
cuAllocOnDevice(const hydroparam_t H)
{
  cudaError_t status;
  status = cudaMalloc((void **) &uoldDEV, H.arUoldSz * sizeof(double));
  VERIF(status, "malloc uoldDEV");
  status = cudaMalloc((void **) &uDEV, H.arVarSz * sizeof(double));
  VERIF(status, "malloc uDEV");
  status = cudaMalloc((void **) &qDEV, H.arVarSz * sizeof(double));
  VERIF(status, "malloc qDEV");
  status = cudaMalloc((void **) &dqDEV, H.arVarSz * sizeof(double));
  VERIF(status, "malloc dqDEV");
  status = cudaMalloc((void **) &qxpDEV, H.arVarSz * sizeof(double));
  VERIF(status, "malloc qxpDEV");
  status = cudaMalloc((void **) &qleftDEV, H.arVarSz * sizeof(double));
  VERIF(status, "malloc qleftDEV");
  status = cudaMalloc((void **) &qrightDEV, H.arVarSz * sizeof(double));
  VERIF(status, "malloc qrightDEV");
  status = cudaMalloc((void **) &qxmDEV, H.arVarSz * sizeof(double));
  VERIF(status, "malloc qxmDEV");
  status = cudaMalloc((void **) &qgdnvDEV, H.arVarSz * sizeof(double));
  VERIF(status, "malloc qgdnvDEV");
  status = cudaMalloc((void **) &fluxDEV, H.arVarSz * sizeof(double));
  VERIF(status, "malloc fluxDEV");
  status = cudaMalloc((void **) &eDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc eDEV");
  status = cudaMalloc((void **) &cDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc cDEV");

  status = cudaMalloc((void **) &rlDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc rlDEV");
  status = cudaMalloc((void **) &ulDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc ulDEV");
  status = cudaMalloc((void **) &plDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc plDEV");
  status = cudaMalloc((void **) &clDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc clDEV");
  status = cudaMalloc((void **) &wlDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc wlDEV");

  status = cudaMalloc((void **) &rrDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc rrDEV");
  status = cudaMalloc((void **) &urDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc urDEV");
  status = cudaMalloc((void **) &prDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc prDEV");
  status = cudaMalloc((void **) &crDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc crDEV");
  status = cudaMalloc((void **) &wrDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc wrDEV");

  status = cudaMalloc((void **) &roDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc roDEV");
  status = cudaMalloc((void **) &uoDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc uoDEV");
  status = cudaMalloc((void **) &poDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc poDEV");
  status = cudaMalloc((void **) &coDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc coDEV");
  status = cudaMalloc((void **) &woDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc woDEV");

  status = cudaMalloc((void **) &rstarDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc rstarDEV");
  status = cudaMalloc((void **) &ustarDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc ustarDEV");
  status = cudaMalloc((void **) &pstarDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc pstarDEV");
  status = cudaMalloc((void **) &cstarDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc cstarDEV");

  status = cudaMalloc((void **) &sgnmDEV, H.arSz * sizeof(long));
  VERIF(status, "malloc sgnmDEV");
  status = cudaMalloc((void **) &spinDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc spinDEV");
  status = cudaMalloc((void **) &spoutDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc spoutDEV");

  status = cudaMalloc((void **) &ushockDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc ushockDEV");
  status = cudaMalloc((void **) &fracDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc fracDEV");
  status = cudaMalloc((void **) &scrDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc scrDEV");
  status = cudaMalloc((void **) &delpDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc delpDEV");
  status = cudaMalloc((void **) &poldDEV, H.arSz * sizeof(double));
  VERIF(status, "malloc poldDEV");

  status = cudaMalloc((void **) &indDEV, H.arSz * sizeof(long));
  VERIF(status, "malloc indDEV");
  status = cudaMalloc((void **) &ind2DEV, H.arSz * sizeof(long));
  VERIF(status, "malloc ind2DEV");
}

extern "C" void
cuFreeOnDevice()
{
  cudaError_t status;
  // liberation de la memoire sur le device (en attendant de la remonter dans le main).
  status = cudaFree(uoldDEV);
  VERIF(status, "free uoldDEV");
  status = cudaFree(uDEV);
  VERIF(status, "free uDEV");
  status = cudaFree(qDEV);
  VERIF(status, "free qDEV");
  status = cudaFree(dqDEV);
  VERIF(status, "free dqDEV");
  status = cudaFree(qxpDEV);
  VERIF(status, "free qxpDEV");
  status = cudaFree(qxmDEV);
  VERIF(status, "free qxmDEV");
  status = cudaFree(eDEV);
  VERIF(status, "free eDEV");
  status = cudaFree(cDEV);
  VERIF(status, "free cDEV");
  status = cudaFree(qleftDEV);
  VERIF(status, "free qleftDEV");
  status = cudaFree(qrightDEV);
  VERIF(status, "free qrightDEV");
  status = cudaFree(qgdnvDEV);
  VERIF(status, "free qgndvDEV");
  status = cudaFree(fluxDEV);
  VERIF(status, "free fluxDEV");

  status = cudaFree(rlDEV);
  VERIF(status, "free rlDEV");
  status = cudaFree(ulDEV);
  VERIF(status, "free ulDEV");
  status = cudaFree(plDEV);
  VERIF(status, "free plDEV");
  status = cudaFree(clDEV);
  VERIF(status, "free clDEV");
  status = cudaFree(wlDEV);
  VERIF(status, "free wlDEV");

  status = cudaFree(rrDEV);
  VERIF(status, "free rrDEV");
  status = cudaFree(urDEV);
  VERIF(status, "free urDEV");
  status = cudaFree(prDEV);
  VERIF(status, "free prDEV");
  status = cudaFree(crDEV);
  VERIF(status, "free crDEV");
  status = cudaFree(wrDEV);
  VERIF(status, "free wrDEV");

  status = cudaFree(roDEV);
  VERIF(status, "free roDEV");
  status = cudaFree(uoDEV);
  VERIF(status, "free uoDEV");
  status = cudaFree(poDEV);
  VERIF(status, "free poDEV");
  status = cudaFree(coDEV);
  VERIF(status, "free coDEV");
  status = cudaFree(woDEV);
  VERIF(status, "free woDEV");

  status = cudaFree(rstarDEV);
  VERIF(status, "free rstarDEV");
  status = cudaFree(ustarDEV);
  VERIF(status, "free ustarDEV");
  status = cudaFree(pstarDEV);
  VERIF(status, "free pstarDEV");
  status = cudaFree(cstarDEV);
  VERIF(status, "free cstarDEV");

  status = cudaFree(sgnmDEV);
  VERIF(status, "free sgnmDEV");
  status = cudaFree(spinDEV);
  VERIF(status, "free spinDEV");
  status = cudaFree(spoutDEV);
  VERIF(status, "free spoutDEV");

  status = cudaFree(ushockDEV);
  VERIF(status, "free ushockDEV");
  status = cudaFree(fracDEV);
  VERIF(status, "free fracDEV");
  status = cudaFree(scrDEV);
  VERIF(status, "free scrDEV");
  status = cudaFree(delpDEV);
  VERIF(status, "free delpDEV");
  status = cudaFree(poldDEV);
  VERIF(status, "free poldDEV");

  status = cudaFree(indDEV);
  VERIF(status, "free indDEV");
  status = cudaFree(ind2DEV);
  VERIF(status, "free ind2DEV");
}

void
Dmemset(double *dq, double motif, size_t nbr)
{
  long i;
  for (i = 0; i < nbr; i++) {
    dq[i] = motif;
  }
}

void
cuHydroGodunov(long idim, double dt, const hydroparam_t H, hydrovar_t * Hv, hydrowork_t * Hw, hydrovarwork_t * Hvw)
{

  // Local variables
  long i, j;
  double dtdx;

  cudaError_t status;

  WHERE("hydro_godunov");

  // constant
  dtdx = dt / H.dx;

  // Update boundary conditions
  if (H.prt) {
    fprintf(stdout, "godunov %d\n", idim);
    PRINTUOLD(H, Hv);
  }
  cuMakeBoundary(idim, H, Hv, uoldDEV);
  if (idim == 1) {
    for (j = H.jmin + ExtraLayer; j < H.jmax - ExtraLayer; j++) {

      cudaMemset(uDEV, 0, H.arVarSz * sizeof(double));
      cuGatherConservativeVars(idim, j, uoldDEV, uDEV, H.imin, H.imax, H.jmin, H.jmax, H.nvar, H.nxt, H.nyt, H.nxyt);
      // Convert to primitive variables

      cuConstoprim(uDEV, qDEV, eDEV, H.nxt, H.nxyt, H.nvar, H.smallr);

      double *qIDDEV = &qDEV[IHvw(0, ID)];
      double *qIPDEV = &qDEV[IHvw(0, IP)];
      cuEquationOfState(qIDDEV, eDEV, qIPDEV, cDEV, 0, H.nxt, H.smallc, H.gamma);

      status = cudaMemset(dqDEV, 0, H.arVarSz);
      VERIF(status, "cmset dq");

      // Characteristic tracing
      if (H.iorder != 1) {
        cuSlope(qDEV, dqDEV, H.nxt, H.nvar, H.nxyt, H.slope_type);
      }
      cuTrace(qDEV, dqDEV, cDEV, qxmDEV, qxpDEV, dtdx, H.nxt, H.scheme, H.nvar, H.nxyt);
      cuQleftright(idim, H.nx, H.ny, H.nxyt, H.nvar, qxmDEV, qxpDEV, qleftDEV, qrightDEV);
      // Solve Riemann problem at interfaces
      cuRiemann(qleftDEV, qrightDEV, qgdnvDEV,
                rlDEV, ulDEV, plDEV, clDEV, wlDEV,
                rrDEV, urDEV, prDEV, crDEV, wrDEV,
                roDEV, uoDEV, poDEV, coDEV, woDEV,
                rstarDEV, ustarDEV, pstarDEV, cstarDEV,
                sgnmDEV, spinDEV, spoutDEV, ushockDEV, fracDEV,
                scrDEV, delpDEV, poldDEV, indDEV, ind2DEV,
                H.nx + 1, H.smallr, H.smallc, H.gamma, H.niter_riemann, H.nvar, H.nxyt);
      // Compute fluxes
      status = cudaMemset(fluxDEV, 0, H.arVarSz);
      cuCmpflx(qgdnvDEV, fluxDEV, H.nxt, H.nxyt, H.nvar, H.gamma);
      cuUpdateConservativeVars(idim, j, dtdx,
                               uoldDEV, uDEV, fluxDEV, H.imin, H.imax, H.jmin, H.jmax, H.nvar, H.nxt, H.nyt, H.nxyt);
    }                           // for j

    if (H.prt) {
      printf("After pass %d\n", idim);
      PRINTUOLD(H, Hv);
    }
  } else {
    for (i = H.imin + ExtraLayer; i < H.imax - ExtraLayer; i++) {
      cudaMemset(uDEV, 0, H.arVarSz * sizeof(double));
      cuGatherConservativeVars(idim, i, uoldDEV, uDEV, H.imin, H.imax, H.jmin, H.jmax, H.nvar, H.nxt, H.nyt, H.nxyt);
      PRINTARRAYV(Hvw->u, H.nyt, "uY", H);

      // Convert to primitive variables
      cuConstoprim(uDEV, qDEV, eDEV, H.nyt, H.nxyt, H.nvar, H.smallr);
      double *qIDDEV = &qDEV[IHvw(0, ID)];
      double *qIPDEV = &qDEV[IHvw(0, IP)];
      cuEquationOfState(qIDDEV, eDEV, qIPDEV, cDEV, 0, H.nyt, H.smallc, H.gamma);
      // Characteristic tracing
      // compute slopes
      status = cudaMemset(dqDEV, 0, H.arVarSz);
      VERIF(status, "cmset dq");

      if (H.iorder != 1) {
        cuSlope(qDEV, dqDEV, H.nyt, H.nvar, H.nxyt, H.slope_type);
      }
      PRINTARRAYV(Hvw->dq, H.nyt, "dqY", H);
      cuTrace(qDEV, dqDEV, cDEV, qxmDEV, qxpDEV, dtdx, H.nyt, H.scheme, H.nvar, H.nxyt);
      cuQleftright(idim, H.nx, H.ny, H.nxyt, H.nvar, qxmDEV, qxpDEV, qleftDEV, qrightDEV);
      PRINTARRAYV(Hvw->qleft, H.ny + 1, "qleftY", H);
      PRINTARRAYV(Hvw->qright, H.ny + 1, "qrightY", H);

      // Solve Riemann problem at interfaces
      cuRiemann(qleftDEV, qrightDEV, qgdnvDEV, rlDEV, ulDEV,
                plDEV, clDEV, wlDEV, rrDEV, urDEV, prDEV,
                crDEV, wrDEV, roDEV, uoDEV, poDEV, coDEV,
                woDEV, rstarDEV, ustarDEV, pstarDEV, cstarDEV,
                sgnmDEV, spinDEV, spoutDEV, ushockDEV, fracDEV,
                scrDEV, delpDEV, poldDEV, indDEV, ind2DEV,
                H.ny + 1, H.smallr, H.smallc, H.gamma, H.niter_riemann, H.nvar, H.nxyt);
      // Compute fluxes
      status = cudaMemset(fluxDEV, 0, H.arVarSz);
      cuCmpflx(qgdnvDEV, fluxDEV, H.nyt, H.nxyt, H.nvar, H.gamma);
      PRINTARRAYV(Hvw->flux, H.ny + 1, "fluxY", H);
      cuUpdateConservativeVars(idim, i, dtdx, uoldDEV, uDEV, fluxDEV, H.imin,
                               H.imax, H.jmin, H.jmax, H.nvar, H.nxt, H.nyt, H.nxyt);
    }                           // else
    if (H.prt) {
      printf("After pass %d\n", idim);
      PRINTUOLD(H, Hv);
    }
  }
}                               // hydro_godunov
