PGM=HydroC
HEADER=$(wildcard *.h)
SRC=cmpflx.c conservar.c equation_of_state.c hydro_godunov.c main.c parametres.c riemann.c trace.c vtkfile.c compute_deltat.c constoprim.c hydro_funcs.c hydro_utils.c make_boundary.c qleftright.c slope.c utils.c
SRCCU=$(wildcard *.cu)

OBJ = $(patsubst %.cu, %.o, ${SRCCU}) $(patsubst %.c,  %.o, ${SRC}) 

DEP = $(patsubst %.cu, %.d, ${SRCCU}) $(patsubst %.c,  %.d, ${SRC})

NVCC=gcc
MACHINE=$(shell uname -n | sed  's/[0-9]//')
ifeq ($(MACHINE), uchu)
#cg au lieu de ca pour que le L2 (sinon L1+L2)
NVCC=nvcc -arch=sm_13
endif
ifeq ($(MACHINE), capac)
NVCC=nvcc -arch=sm_20 -Xptxas -dlcm=cg
endif
ifeq ($(MACHINE), inti)
NVCC=nvcc -arch=sm_20 -Xptxas -dlcm=cg
endif
CC=gcc -Wall

OPT=-g
OPT=-O3 -DNDEBUG -DFAST

# OPT+= -DFLOPS

CFLAGS=$(OPT) -I/opt/cuda/3.1/include

$(PGM): $(OBJ)
	$(NVCC) -o $(PGM) $(OPT) $(OBJ) -lm $(HMPPENDFLAGS)

depend:
	-rm ${DEP}; make ${DEP}

LSRCH=$(shell echo $(HEADER) | tr ' ' '\n' | sort)
LSRCC=$(shell echo $(SRC) | tr ' ' '\n'| sort)
LSRCU=$(shell echo $(SRCCU) | tr ' ' '\n'| sort)

listing:
	a2ps --medium=A4dj -C -1 -R -l90 --toc -E --prologue=color --file-align=fill -o listing.ps Makefile $(LSRCH) $(LSRCC) $(LSRCU)
	ps2pdf listing.ps
	rm listing.ps

CUHYDROC:
	make clean
	cd $(HOME); tar czvf CUHYDROC.`date +%y%m%d_%H%M%S`.tgz cuHydroC/*.c cuHydroC/*.cu cuHydroC/*.h cuHydroC/Makefile cuHydroC/input

save:
	scp -r uchu:cuHydroC $(HOME)
	make clean
	-make listing
	cd $(HOME); tar czvf CUHYDROC.`date +%y%m%d_%H%M%S`.tgz cuHydroC

OKDIR=$(shell date +%y%m%d_%H%M%S)
ok:
	mkdir -p Archives/ok$(OKDIR)
	cp *.c *.cu *.h Makefile Archives/ok$(OKDIR)

clean   :
	-/bin/rm -f *.o *.so *~ *.vts  *.bic *.bak *.out ${PGM} *.csv *.log *.d

indent:
	indent $(SRC) $(SRCCU) $(HEADER)

dos2unix:
	dos2unix $(SRC) $(HEADER)
	perl -i.bak -p -e 's/\015//ig' $(SRC) $(SRCCU) $(HEADER)


.SUFFIXES:  .o .d .c .cu
include $(DEP)

.c.d:
	@gcc ${CPPFLAGS} ${CFLAGS} $(HFLAGS) -M $< -o $@

.cu.d:
	@$(NVCC) ${CPPFLAGS} ${CFLAGS} $(HFLAGS) -M $< -o $@
	@perl -i -p -e "s+$(HOME)/cuHydroC//++" $@

.c.o    :
	${CC} ${CFLAGS} -c $< $(HMPPENDFLAGS)

.cpp.o  :
	${CC} ${CFLAGS} -c $< -o $@
.cu.o:
	${NVCC} ${CFLAGS} -c $< -o $@

LNAME=$(shell basename $(PWD))
RUNDIR=/ptmp/ocre/coling/$(LNAME)
run: $(PGM)
	mkdir -p $(RUNDIR)
	cp $(PGM) input $(RUNDIR)
	cd $(RUNDIR); rm -f output*.vts
	# cd $(RUNDIR); time ./$(PGM) -i input
	cd $(RUNDIR); time ccc_mprun -p opengpu ./$(PGM) -i input

vrun: $(PGM)
	mkdir -p $(RUNDIR)
	cp $(PGM) input $(RUNDIR)
	cd $(RUNDIR); valgrind --tool=memcheck --leak-check=full ./$(PGM) -i input

prun: $(PGM)
	env CUDA_PROFILE=1 CUDA_PROFILE_LOG=./cudaProfile.log CUDA_PROFILE_CONFIG=./config.txt ccc_mprun ./$(PGM) -i input
	./anallog ./cudaProfile.log | sort -n
	# ./$(PGM) -i input

fprun: $(PGM)
	env CUDA_PROFILE=1 CUDA_PROFILE_LOG=./cudaProfile.log CUDA_PROFILE_CONFIG=./config.txt ./$(PGM) -i input
	./anallog ./cudaProfile.log
	# ./$(PGM) -i input

frun: $(PGM)
	./$(PGM) -i input
	# ./$(PGM) -i input

lrun: $(PGM)
	ccc_mprun ./$(PGM) -i input
	# ./$(PGM) -i input

gdb: $(PGM)
	mkdir -p $(RUNDIR)
	cp $(PGM) input $(RUNDIR)
	cd $(RUNDIR); gdb ./$(PGM)

ptmp: $(PGM)
	mkdir -p /ptmp/ocre/coling/cuHydroC
	cp $(PGM) input  /ptmp/ocre/coling/cuHydroC
	cd  /ptmp/ocre/coling/cuHydroC; ccc_mprun ./$(PGM) -i input

ddt: $(PGM)
	mkdir -p $(RUNDIR)
	cp $(PGM) input $(RUNDIR)
	cd $(RUNDIR); /opt/allinea/ddt-2.5.alpha/bin/ddt ./$(PGM) -i $(HOME)/HYDRO/Mono/Input/input_sedov_noio_20x40.nml

FORCE:

#EOF
