/*
  A simple 2D hydro code
  (C) Romain Teyssier : CEA/IRFU           -- original F90 code
  (C) Pierre-Francois Lavallee : IDRIS      -- original F90 code
  (C) Guillaume Colin de Verdiere : CEA/DAM -- for the C version
*/

#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <cuda.h>

#include "parametres.h"
#include "utils.h"
#include "cuRiemann.h"
#include "gridfuncs.h"

#define DABS(x) (double) fabs((x))
#define IHVW(i, v) ((i) + (v) * Hnxyt)

typedef struct _Args {
  double *qleft;
  double *qright;
  double *qgdnv;
  double *rl;
  double *ul;
  double *pl;
  double *cl;
  double *wl;
  double *rr;
  double *ur;
  double *pr;
  double *cr;
  double *wr;
  double *ro;
  double *uo;
  double *po;
  double *co;
  double *wo;
  double *rstar;
  double *ustar;
  double *pstar;
  double *cstar;
  long *sgnm;
  double *spin;
  double *spout;
  double *ushock;
  double *frac;
  double *scr;
  double *delp;
  double *pold;
  long *ind;
  long *ind2;
  long narray;
  double Hsmallr;
  double Hsmallc;
  double Hgamma;
  long Hniter_riemann;
  long Hnvar;
  long Hnxyt;
} Args_t;

// memoire de constante sur le device qui va contenir les arguments de riemann
// on les transmets en bloc en une fois et les differents kernels pourront y acceder.
__constant__ Args_t K;

__global__ void
Loop1KcuRiemann()
{
  double smallp, gamma6, ql, qr, usr, usl, wwl, wwr, smallpp;
  long iter;
  long tid = idx1dl();
  double ulS;
  double plS;
  double clS;
  double urS;
  double prS;
  double crS;
  double uoS;
  double delpS;
  double poldS;
  long i = idx1d();
  long Hnxyt = K.Hnxyt;
  if (i >= K.narray)
    return;

  smallp = Square(K.Hsmallc) / K.Hgamma;

  K.rl[i] = MAX(K.qleft[IHVW(i, ID)], K.Hsmallr);
  K.ul[i] = K.qleft[IHVW(i, IU)];
  K.pl[i] = MAX(K.qleft[IHVW(i, IP)], (double) (K.rl[i] * smallp));
  K.rr[i] = MAX(K.qright[IHVW(i, ID)], K.Hsmallr);
  K.ur[i] = K.qright[IHVW(i, IU)];
  K.pr[i] = MAX(K.qright[IHVW(i, IP)], (double) (K.rr[i] * smallp));
  // Lagrangian sound speed
  K.cl[i] = K.Hgamma * K.pl[i] * K.rl[i];
  K.cr[i] = K.Hgamma * K.pr[i] * K.rr[i];
  // First guess
  K.wl[i] = sqrt(K.cl[i]);
  K.wr[i] = sqrt(K.cr[i]);
  K.pstar[i] = ((K.wr[i] * K.pl[i] + K.wl[i] * K.pr[i]) + K.wl[i] * K.wr[i] * (K.ul[i] - K.ur[i])) / (K.wl[i] + K.wr[i]);
  K.pstar[i] = MAX(K.pstar[i], 0.0);
  K.pold[i] = K.pstar[i];
  // ind est un masque de traitement pour le newton
  K.ind[i] = 1;                 // toutes les cellules sont a traiter

  ulS = K.ul[i];
  plS = K.pl[i];
  clS = K.cl[i];
  urS = K.ur[i];
  prS = K.pr[i];
  crS = K.cr[i];
  uoS = K.uo[i];
  delpS = K.delp[i];
  poldS = K.pold[i];

  smallp = Square(K.Hsmallc) / K.Hgamma;
  smallpp = K.Hsmallr * smallp;
  gamma6 = (K.Hgamma + one) / (two * K.Hgamma);

  long indi = K.ind[i];

  for (iter = 0; iter < K.Hniter_riemann; iter++) {
    double precision = 1.e-6;
    wwl = sqrt(clS * (one + gamma6 * (poldS - plS) / plS));
    wwr = sqrt(crS * (one + gamma6 * (poldS - prS) / prS));
    ql = two * wwl * Square(wwl) / (Square(wwl) + clS);
    qr = two * wwr * Square(wwr) / (Square(wwr) + crS);
    usl = ulS - (poldS - plS) / wwl;
    usr = urS + (poldS - prS) / wwr;
    double t1 = qr * ql / (qr + ql) * (usl - usr);
    double t2 = -poldS;
    delpS = MAX(t1, t2);
    poldS = poldS + delpS;
    uoS = DABS(delpS / (poldS + smallpp));
    indi = uoS > precision;
    if (!indi)
      break;
  }
  __syncthreads();
  K.uo[i] = uoS;
  K.pold[i] = poldS;

  gamma6 = (K.Hgamma + one) / (two * K.Hgamma);

  K.pstar[i] = K.pold[i];
  K.wl[i] = sqrt(K.cl[i] * (one + gamma6 * (K.pstar[i] - K.pl[i]) / K.pl[i]));
  K.wr[i] = sqrt(K.cr[i] * (one + gamma6 * (K.pstar[i] - K.pr[i]) / K.pr[i]));

  K.ustar[i] = half * (K.ul[i] + (K.pl[i] - K.pstar[i]) / K.wl[i] + K.ur[i] - (K.pr[i] - K.pstar[i]) / K.wr[i]);
  K.sgnm[i] = (K.ustar[i] > 0) ? 1 : -1;
  if (K.sgnm[i] == 1) {
    K.ro[i] = K.rl[i];
    K.uo[i] = K.ul[i];
    K.po[i] = K.pl[i];
    K.wo[i] = K.wl[i];
  } else {
    K.ro[i] = K.rr[i];
    K.uo[i] = K.ur[i];
    K.po[i] = K.pr[i];
    K.wo[i] = K.wr[i];
  }
  K.co[i] = MAX(K.Hsmallc, sqrt(DABS(K.Hgamma * K.po[i] / K.ro[i])));
  K.rstar[i] = K.ro[i] / (one + K.ro[i] * (K.po[i] - K.pstar[i]) / Square(K.wo[i]));
  K.rstar[i] = MAX(K.rstar[i], K.Hsmallr);
  K.cstar[i] = MAX(K.Hsmallc, sqrt(DABS(K.Hgamma * K.pstar[i] / K.rstar[i])));
  K.spout[i] = K.co[i] - K.sgnm[i] * K.uo[i];
  K.spin[i] = K.cstar[i] - K.sgnm[i] * K.ustar[i];
  K.ushock[i] = K.wo[i] / K.ro[i] - K.sgnm[i] * K.uo[i];
  if (K.pstar[i] >= K.po[i]) {
    K.spin[i] = K.ushock[i];
    K.spout[i] = K.ushock[i];
  }
  K.scr[i] = MAX((double) (K.spout[i] - K.spin[i]), (double) (K.Hsmallc + DABS(K.spout[i] + K.spin[i])));
  K.frac[i] = (one + (K.spout[i] + K.spin[i]) / K.scr[i]) * half;
  K.frac[i] = MAX(zero, (double) (MIN(one, K.frac[i])));

  K.qgdnv[IHVW(i, ID)] = K.frac[i] * K.rstar[i] + (one - K.frac[i]) * K.ro[i];
  K.qgdnv[IHVW(i, IU)] = K.frac[i] * K.ustar[i] + (one - K.frac[i]) * K.uo[i];
  K.qgdnv[IHVW(i, IP)] = K.frac[i] * K.pstar[i] + (one - K.frac[i]) * K.po[i];

  if (K.spout[i] < zero) {
    K.qgdnv[IHVW(i, ID)] = K.ro[i];
    K.qgdnv[IHVW(i, IU)] = K.uo[i];
    K.qgdnv[IHVW(i, IP)] = K.po[i];
  }
  if (K.spin[i] > zero) {
    K.qgdnv[IHVW(i, ID)] = K.rstar[i];
    K.qgdnv[IHVW(i, IU)] = K.ustar[i];
    K.qgdnv[IHVW(i, IP)] = K.pstar[i];
  }

  if (K.sgnm[i] == 1) {
    K.qgdnv[IHVW(i, IV)] = K.qleft[IHVW(i, IV)];
  } else {
    K.qgdnv[IHVW(i, IV)] = K.qright[IHVW(i, IV)];
  }
}

__global__ void
Loop10KcuRiemann()
{
  long invar;
  long i = idx1d();
  long Hnxyt = K.Hnxyt;
  if (i >= K.narray)
    return;

  for (invar = IP + 1; invar < K.Hnvar; invar++) {
    if (K.sgnm[i] == 1) {
      K.qgdnv[IHVW(i, invar)] = K.qleft[IHVW(i, invar)];
    }
    if (K.sgnm[i] != 1) {
      K.qgdnv[IHVW(i, invar)] = K.qright[IHVW(i, invar)];
    }
  }
}

void
cuRiemann(double *RESTRICT qleft, double *RESTRICT qright,
          double *RESTRICT qgdnv, double *RESTRICT rl,
          double *RESTRICT ul, double *RESTRICT pl, double *RESTRICT cl,
          double *RESTRICT wl, double *RESTRICT rr, double *RESTRICT ur,
          double *RESTRICT pr, double *RESTRICT cr, double *RESTRICT wr,
          double *RESTRICT ro, double *RESTRICT uo, double *RESTRICT po,
          double *RESTRICT co, double *RESTRICT wo,
          double *RESTRICT rstar, double *RESTRICT ustar,
          double *RESTRICT pstar, double *RESTRICT cstar,
          long *RESTRICT sgnm, double *RESTRICT spin,
          double *RESTRICT spout, double *RESTRICT ushock,
          double *RESTRICT frac, double *RESTRICT scr,
          double *RESTRICT delp, double *RESTRICT pold,
          long *RESTRICT ind, long *RESTRICT ind2,
          const long narray,
          const double Hsmallr,
          const double Hsmallc, const double Hgamma, const long Hniter_riemann, const long Hnvar, const long Hnxyt)
{
  // Local variables
  dim3 block, grid;
  Args_t k;
#define IHVW(i, v) ((i) + (v) * Hnxyt)

  WHERE("riemann");
  k.qleft = qleft;
  k.qright = qright;
  k.qgdnv = qgdnv;
  k.rl = rl;
  k.ul = ul;
  k.pl = pl;
  k.cl = cl;
  k.wl = wl;
  k.rr = rr;
  k.ur = ur;
  k.pr = pr;
  k.cr = cr;
  k.wr = wr;
  k.ro = ro;
  k.uo = uo;
  k.po = po;
  k.co = co;
  k.wo = wo;
  k.rstar = rstar;
  k.ustar = ustar;
  k.pstar = pstar;
  k.cstar = cstar;
  k.sgnm = sgnm;
  k.spin = spin;
  k.spout = spout;
  k.ushock = ushock;
  k.frac = frac;
  k.scr = scr;
  k.delp = delp;
  k.pold = pold;
  k.ind = ind;
  k.ind2 = ind2;
  k.narray = narray;
  k.Hsmallr = Hsmallr;
  k.Hsmallc = Hsmallc;
  k.Hgamma = Hgamma;
  k.Hniter_riemann = Hniter_riemann;
  k.Hnvar = Hnvar;
  k.Hnxyt = Hnxyt;

  cudaMemcpyToSymbol(K, &k, sizeof(Args_t), 0, cudaMemcpyHostToDevice);
  CheckErr("cudaMemcpyToSymbol");

  // 64 threads donnent le meilleur rendement compte-tenu de la complexite du kernel
  SetBlockDims(narray, 192, block, grid);

#if CUDA_VERSION > 2000
  // cudaFuncSetCacheConfig(Loop1KcuRiemann, cudaFuncCachePreferShared);
  cudaFuncSetCacheConfig(Loop1KcuRiemann, cudaFuncCachePreferL1);
  // cudaFuncSetCacheConfig(Loop1KcuRiemann, cudaFuncCachePreferNone);
#endif

  // Pressure, density and velocity
  Loop1KcuRiemann <<< grid, block >>> ();
  CheckErr("Avant synchronize Loop1KcuRiemann");
  cudaThreadSynchronize();
  CheckErr("After synchronize Loop1KcuRiemann");

  // other passive variables
  if (Hnvar > IP + 1) {
    Loop10KcuRiemann <<< grid, block >>> ();
    cudaThreadSynchronize();
    CheckErr("After synchronize Loop10KcuRiemann");
  }
}                               // riemann


//EOF
