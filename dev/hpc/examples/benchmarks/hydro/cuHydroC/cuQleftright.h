#ifndef CUQLEFTRIGHT_H_INCLUDED
#define CUQLEFTRIGHT_H_INCLUDED

void
  cuQleftright(const long idim, const long Hnx, const long Hny, const long Hnxyt,
               const long Hnvar,
               double *RESTRICT qxmDEV, double *RESTRICT qxpDEV, double *RESTRICT qleftDEV, double *RESTRICT qrightDEV);

#endif // QLEFTRIGHT_H_INCLUDED
