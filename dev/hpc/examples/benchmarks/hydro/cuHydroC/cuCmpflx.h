#ifndef CUCMPFLX_H_INCLUDED
#define CUCMPFLX_H_INCLUDED

#include "utils.h"
void cuCmpflx(double *RESTRICT qgdnv, double *RESTRICT flux, const long narray,
              const long Hnxyt, const long Hnvar, const double Hgamma);

#endif // CMPFLX_H_INCLUDED
