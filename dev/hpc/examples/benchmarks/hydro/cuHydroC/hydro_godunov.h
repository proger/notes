#ifndef HYDRO_GODUNOV_H_INCLUDED
#define HYDRO_GODUNOV_H_INCLUDED

#if 0

void hydro_godunov(long idim, double dt, const hydroparam_t H, hydrovar_t * Hv, hydrowork_t * Hw, hydrovarwork_t * Hvw);
#endif
#endif // HYDRO_GODUNOV_H_INCLUDED
