#include "stream_defs.h"
// For SSE
#include <xmmintrin.h>

void tuned_STREAM_Triad(STREAM_TYPE scalar)
{
extern STREAM_TYPE      a[STREAM_ARRAY_SIZE+OFFSET],
                        b[STREAM_ARRAY_SIZE+OFFSET],
                        c[STREAM_ARRAY_SIZE+OFFSET];

ssize_t j;

// First : declare an array containing only the scalar value
__m128 mm_scalar = _mm_set_ps1(scalar);
// For scalar *c
__m128 mm_ctemp;

__m128 *mm_c, * mm_cmul, * mm_b, *mm_a;

mm_c= (__m128 *) c;
mm_b= (__m128 *) b;
mm_a= (__m128 *) a;


        printf("VERSION Unroll\n");
        for (j=0; j<STREAM_ARRAY_SIZE/4; j=j++)
	{
	// We step with a stride of 4
        // Multiply c by the scalar, putting the result in mm_ctemp
	mm_ctemp = _mm_mul_ps(*mm_c,mm_scalar); 

        // Add b and mm_ctemp, putting it in a
        *mm_a = _mm_add_ps(mm_ctemp,*mm_b);

	// Do some pointer arithmetic on the mm types
	mm_c ++;
	mm_b ++;
	mm_a ++;

	
	}
}

