#include "stream_defs.h"

void tuned_STREAM_Triad(STREAM_TYPE scalar)
{
extern STREAM_TYPE      a[STREAM_ARRAY_SIZE+OFFSET],
                        b[STREAM_ARRAY_SIZE+OFFSET],
                        c[STREAM_ARRAY_SIZE+OFFSET];

ssize_t j;

        printf("VERSION Unroll\n");
        for (j=0; j<STREAM_ARRAY_SIZE; j=j+8)
	{
          a[j] =   b[j]+scalar*c[j];
          a[j+1] = b[j+1]+scalar*c[j+1];
          a[j+2] = b[j+2]+scalar*c[j+2];
          a[j+3] = b[j+3]+scalar*c[j+3];
          a[j+4] = b[j+4]+scalar*c[j+4];
          a[j+5] = b[j+5]+scalar*c[j+5];
          a[j+6] = b[j+6]+scalar*c[j+6];
          a[j+7] = b[j+7]+scalar*c[j+7];
	}
}

