#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define TRUE 1
#define FALSE 0

#define NY 10000
#define NX 10000
#define NBGEN 5

typedef unsigned char Byte ;

void print_bit(Byte b,char whichone)
{
printf("%c",b&(1<<whichone) ? 'x' : '-');
}

void print_byte(Byte b)
{
char w;

for (w=7 ; w >= 0 ; --w)
	print_bit(b,w);
putchar(' ');
}

unsigned char set_bit(Byte b, char whichone, int value)
{
if (value)
	{
	return (b | (1 << whichone));
	}
else    {
	return (b & ~(1 << whichone));
	}
}

//void evolution(Byte b[NX][NY],int i,int j,int maxi, int maxj)
void evolution(Byte *g1,Byte *g2, int maxi, int maxj)
{
/* Les voisins : i-1,j-1    i-1,j    i-1,j+1
                  i, j-1      X      i+1,j+1
                 i+1,j-1    i+1,j    i+1,j+1  
 Le tout avec modulo : (i-1+maxi)%maxi, (i-1)+maxj(;...

*/

int xv,yv;
int nbvoisins=0;
int i,j;

for (i=0 ; i < maxi ; i++)
	for (j=0 ; j < maxj ; j++)

{
	nbvoisins=0;


/* On compte les voisins */

for (xv=i-1 ;  xv <= i+1 ; xv ++)
	for (yv=j-1 ; yv <= j+1 ; yv++)
	{
//		printf("%d,%d-(%d,%d) ",xv,yv,(xv+maxi)%maxi,(yv+maxj)%maxj);
//	if ((xv != i) && (yv !=j)) 
		{
		nbvoisins+=g1[((xv+maxi)%maxi)*maxj+(yv+maxj)%maxj];
		}
	}
nbvoisins -= g1[i*maxj+j];
// printf("%d %d dispose de %d voisins\n",i,j,nbvoisins);

switch (nbvoisins) {
	case 2:
		g2[i*maxj+j]=g1[i*maxj+j];
		break;
	case 3 : 
		g2[i*maxj+j]=1;
		break;
	default:
		g2[i*maxj+j]=0;
}

/*
if (nbvoisins < 2 || nbvoisins > 3)
	g2[i*maxj+j]=0;
else if (nbvoisins==2)
	g2[i*maxj+j]=g1[i*maxj+j];

if ((g1[i*maxj+j]==0) && (nbvoisins==3))
	{ 
	g2[i*maxj+j]=1; 
	}

if (g1[i*maxj+j]==1)
	if (nbvoisins==3 || nbvoisins==2)
		g2[i*maxj+j]=1;
	else
		g2[i*maxj+j]=0;

*/
	}
}

//void affiche (Byte b[NX][NY],int maxi, int maxj)
void affiche (Byte *b,int maxi, int maxj)
{
int i,j;


for (i=0 ; i < maxi ; i++)
	{
	for (j=0 ; j < maxj ; j++)
		putchar((b[i*maxj+j])==1 ? 'x' : '-');
	putchar('\n');
	}
}
	
void faffiche (Byte *b,int maxi, int maxj)
{
int i,j;
FILE *f=fopen("etape","wt");

for (i=0 ; i < maxi ; i++)
	{
	for (j=0 ; j < maxj ; j++)
		fprintf(f,"%c",(b[i*maxj+j])==1 ? 'x' : '-');
	fprintf(f,"\n");
	}

fclose(f);
}
	


int main (int argc, char ** argv)
{
// Byte tbl[NX][NY];
Byte * t1=calloc(NX*NY,sizeof(Byte));
Byte * t2=calloc(NX*NY,sizeof(Byte));
Byte * t3;

int i,j,k;

srandom((long)t3+(long)t1+(long)t2);

for (i=0 ; i < NX; i++)
for (j=0 ; j < NY; j++)
	{
	t1[i*NY+j]=random()%2;
	}



//affiche(t1,NX,NY);


for (k=0 ; k < NBGEN ; k++)
{
	printf("Generation %d\n",k);
// for (i=0 ; i < NX ; i++)
// 	{
//	for (j=0 ; j < NY ; j++)
//		{
		evolution(t1,t2,NX,NY);
//		}
// 	}

// Swap pointers
t3=t1;
t1=t2;
t2=t3;
memset(t2,0,NX*NY*sizeof(Byte));

//affiche(t1,NX,NY);

 if ((k%10))
	faffiche(t1,NX,NY);

}

} // End main
