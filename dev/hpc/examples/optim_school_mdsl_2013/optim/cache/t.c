#include <stdio.h>
#include <stdlib.h>
#define ADDR(i,j,dim) (i*dim+j)

#define TYPE float

TYPE * alloue (int nb_elem)
{
TYPE * t;
printf("Allocation %d elements\n",nb_elem);
	t=malloc(nb_elem*sizeof(TYPE));
if (t != (TYPE *)NULL) return t;
else printf("Erreur allocation %d éléments de taille %d\n",nb_elem,sizeof(TYPE)),exit(-1);
}

void parcours_ko(TYPE *t,int dim)
{
int i,j;
	for (j=0 ; j < dim ; j++)
		for (i=0 ; i < dim ; i++)
		{
		t[ADDR(i,j,dim)]=i+j;
		}
}

void parcours_ok(TYPE *t,int dim)
{
int i,j;
for (i=0 ; i < dim ; i++)
	for (j=0 ; j < dim ; j++)
		{
		t[ADDR(i,j,dim)]=i+j;
		}
}


int main (int argc, char **argv)
{
int nb_elem,dim;
TYPE *t;

if (argc != 2)
	{
	printf("Utilisation : %s dimx1024\n",argv[0]);
	exit(-1);
	}

dim=atoi(argv[1])*1024;
printf("%s (%d %d) \n",argv[1],dim,dim*dim);

t=alloue(dim*dim);

parcours_ok(t,dim);
parcours_ko(t,dim);
}
