
import pycuda.driver as drv
import pycuda.tools
import pycuda.autoinit
import numpy
import numpy.linalg as la
from pycuda.compiler import SourceModule

# heat parameters
LX = 1.0
LY = 1.0

NX = 128
NY = 128

CFL = 0.125
DX = LX/(NX-1)
DY = LY/(NY-1)
DT = CFL*DX*DX

NITER = 10
TMAX  = NITER*DT

ALPHA = 1.0
R  = ALPHA*DT/DX/DX
R2 = 1.0-2*2*R


# naive kernel
mod = SourceModule("""
__global__ void heat2d_ftcs_naive_order2_kernel(float *A, float *B, 
						unsigned int NX,
						unsigned int NY,
						float R,
						float R2)
{
  
  int i = blockIdx.x * blockDim.x + threadIdx.x ;
  int j = blockIdx.y * blockDim.y + threadIdx.y ;
  
  // column-major order
  int index,index1,index2,index3,index4;
  index = j*NX + i;
  index1= index + 1;
  index2= index - 1;
  index3= index + NX;
  index4= index - NX;

  if(i>0 && i<NX-1 && j>0 && j<NY-1) {
    B[index] = R2*A[index] + R*( A[index1] + A[index2] + A[index3] + A[index4] );
  }
  
} // heat2d_ftcs_naive_order2_kernel
""")

heat_naive = mod.get_function("heat2d_ftcs_naive_order2_kernel")


# CPU arrays
data1 = numpy.zeros( (NX,NY)).astype(numpy.float32)
data1[NX/2-10:NX/2+10, NY/2-10:NY/2+10] = 1.0

data2 = data1.copy()

# GPU arrays
d_data1 = drv.mem_alloc(data1.size * data1.dtype.itemsize)
d_data2 = drv.mem_alloc(data1.size * data2.dtype.itemsize)

drv.memcpy_htod(d_data1, data1)
drv.memcpy_htod(d_data2, data2)

# launch kernel
heat_naive(
    d_data1,
    d_data2,
    numpy.uint32(NX),
    numpy.uint32(NY),
    numpy.float32(R),
    numpy.float32(R2),
    block=(16,16,1),
    grid=( (NX+15)/16,(NY+15)/16) )

results = numpy.empty_like(data1)
drv.memcpy_dtoh(results, d_data2)

print results
print results.max()
numpy.savetxt('heat2d.txt',results)
