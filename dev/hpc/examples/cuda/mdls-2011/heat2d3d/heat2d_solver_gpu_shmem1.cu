/**
 * \file heat2d_solver_gpu_shmem1.cu
 * \brief Solve 2D heat equation (finite difference method). GPU version (shmem1).
 *
 * We solve the 2D Heat equation \f$\partial_t \phi = \alpha \left[
 * \partial^2_x \phi + \partial^2_y \phi \right] \f$, \f$ 0 \leq x
 * \leq L_x \f$, \f$ 0 \leq y \leq L_y \f$, \f$ 0 \leq t\f$.\\
 *
 * Method : Finite Difference, FTCS scheme
 *
 * boundary condition : Dirichlet
 *
 * GPU version : shared memory 1
 *
 * \author Pierre Kestener.
 * \date 17-dec-2009.
 */

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/time.h> // gettimeofday

// includes, project
#include <cutil_inline.h>

// parameters + real_t typedef
#include "param.h"

// for output results
#include "output.h"

// GPU solver
#include "heat2d_kernel_gpu_shmem1.cu"

// CPU solver
#include "heat_kernel_cpu.h"

// initial conditions
#include "misc.h"

////////////////////////////////////////////////////////////////////////////////
// declaration, forward
void runTest( int argc, char** argv);

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int
main(int argc, char** argv) 
{
  runTest(argc, argv);

  cutilExit(argc, argv);
}

////////////////////////////////////////////////////////////////////////////////
//! Run a simple test for CUDA
////////////////////////////////////////////////////////////////////////////////
void
runTest(int argc, char** argv) 
{
  // use command-line specified CUDA device, otherwise use device with highest Gflops/s
  if( cutCheckCmdLineFlag(argc, (const char**)argv, "device") )
    cutilDeviceInit(argc, argv);
  else
    cudaSetDevice( cutGetMaxGflopsDeviceId() );

  /*
   * read and print parameters
   */
  // default parameter file
  std::string paramFile("heatEqSolver.par");

  // if argv[1] exists use it as a parameter file
  if (argc>1) {
    printf("trying to read parameters from file %s ...\n",argv[1]);
    paramFile = std::string(argv[1]);
  }

  // read parameter file
  readParamFile(paramFile);

  // print parameters on screen
  printParameters("HEAT 2D - GPU (SHMEM1)");





  unsigned int timer = 0;
  cutilCheckError( cutCreateTimer( &timer));

  unsigned int mem_size = sizeof(real_t)*NX*NY;

  // allocate host memory
  real_t* data1 = (real_t*) malloc( mem_size);
  real_t* data2 = (real_t*) malloc( mem_size);
  
  ///////////////////////////////////////////////////
  // compute GPU solution to 2D heat equation
  ///////////////////////////////////////////////////
  
  // inital condition
  initCondition2D (data1);

  // allocate device memory
  real_t* d_data1;
  real_t* d_data2;

  // device memory allocation (using cudaMallocPitch)
  size_t pitchBytes1;
  cudaMallocPitch((void**) &d_data1, &pitchBytes1, NX*sizeof(real_t), NY);
  unsigned int _pitch1 = pitchBytes1 / sizeof(real_t);

  size_t pitchBytes2; // should be the same as pitchBytes1
  cudaMallocPitch((void**) &d_data2, &pitchBytes2, NX*sizeof(real_t), NY);
  //unsigned int _pitch2 = pitchBytes2 / sizeof(real_t);

  // copy host memory to device (using cudaMemcpy2D)
  cudaMemcpy2D(d_data1,pitchBytes1,
		data1  ,NX*sizeof(real_t), NX*sizeof(real_t), NY,
		cudaMemcpyHostToDevice);
  cudaMemcpy2D(d_data2,pitchBytes2,
		data1  ,NX*sizeof(real_t), NX*sizeof(real_t), NY,
		cudaMemcpyHostToDevice);
 
  // setup execution parameters for cuda kernel
  // grid dimension for sharedmem kernel
  dim3  threads;
  dim3  grid;
  if (useOrder2 or useOrder2b) {
    threads.x = BLOCK_DIMX;
    threads.y = BLOCK_DIMY;
    grid.x    = (NX+BLOCK_INNER_DIMX-1)/BLOCK_INNER_DIMX;
    grid.y    = (NY+BLOCK_INNER_DIMY-1)/BLOCK_INNER_DIMY;
  } else {
    threads.x = BLOCK_DIMX2;
    threads.y = BLOCK_DIMY2;    
    grid.x    = (NX+BLOCK_INNER_DIMX2-1)/BLOCK_INNER_DIMX2;
    grid.y    = (NY+BLOCK_INNER_DIMY2-1)/BLOCK_INNER_DIMY2;
  }
  
  printf("grid  size : %u %u\n",grid.x,grid.y);
  printf("block size : %u %u\n",threads.x,threads.y);
  
  // copy scheme parameters to device constant memory
  cudaMemcpyToSymbol(::o2Gpu ,&o2 ,sizeof(struct SecondOrderParam),0,cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol(::o4Gpu ,&o4 ,sizeof(struct FourthOrderParam),0,cudaMemcpyHostToDevice);


  // start timer
  cutilSafeCall( cudaThreadSynchronize() );
  cutilCheckError( cutResetTimer( timer));
  cutilCheckError( cutStartTimer( timer));
  // time loop executing sharedmem kernel
  int iTime   =  0;
  int iOutput = -1;
  for (real_t t=0.0f; t<TMAX; t+=(2*DT), iTime+=2) {
  
    if (useOrder2) {
      
      heat2d_ftcs_sharedmem_order2_kernel<<< grid, threads >>>( d_data1, d_data2, 
								_pitch1, NX, NY);
      
      // check if kernel execution generated and error
      cutilCheckMsg("Kernel execution failed");
      
      heat2d_ftcs_sharedmem_order2_kernel<<< grid, threads >>>( d_data2, d_data1, 
								_pitch1, NX, NY);
      // check if kernel execution generated and error
      cutilCheckMsg("Kernel execution failed");
    
    } else if (useOrder2b) {

      heat2d_ftcs_sharedmem_order2b_kernel<<< grid, threads >>>( d_data1, d_data2, 
								_pitch1, NX, NY);
      
      // check if kernel execution generated and error
      cutilCheckMsg("Kernel execution failed");
      
      heat2d_ftcs_sharedmem_order2b_kernel<<< grid, threads >>>( d_data2, d_data1, 
								_pitch1, NX, NY);
      // check if kernel execution generated and error
      cutilCheckMsg("Kernel execution failed");

    } else { // use the 4th order accurate scheme
      
      heat2d_ftcs_sharedmem_order4_kernel<<< grid, threads >>>( d_data1, d_data2, 
								_pitch1, NX, NY);
      
      // check if kernel execution generated and error
      cutilCheckMsg("Kernel execution failed");
      
      heat2d_ftcs_sharedmem_order4_kernel<<< grid, threads >>>( d_data2, d_data1, 
								_pitch1, NX, NY);
      // check if kernel execution generated and error
      cutilCheckMsg("Kernel execution failed");
    }
    
    /* save output (just for cross-checking, do not save when
       measuring computing time */
    if (ENABLE_GPU_SAVE) {
      
      if (iTime%T_OUTPUT == 0) {
	iOutput++;
	cutilSafeCall( cudaMemcpy2D( data1, NX*sizeof(real_t),
				     d_data1, pitchBytes1, NX*sizeof(real_t), NY,
				     cudaMemcpyDeviceToHost) );
      }
      
      // PGM save
      if (SAVE_PGM and iTime%T_OUTPUT == 0)
	save_pgm(data1, "heat2d_gpu_shmem1_",iOutput,NX,NY);
      
      // MathGL save (3D view)
      if (SAVE_MGL and iTime%T_OUTPUT == 0)
	save_mgl(data1, "heat2d_gpu_shmem1_",iOutput,NX,NY);

      // VTK output
      if (SAVE_VTK and iTime%T_OUTPUT == 0)
	save_vtk(data1, "heat2d_gpu_shmem1_",iOutput);

    }

  }
    
  // stop timer
  cutilSafeCall(   cudaThreadSynchronize());
  cutilCheckError( cutStopTimer( timer));
  real_t gpu_time = cutGetTimerValue( timer);
  printf( "GPU Processing time: %f (ms)\n", gpu_time);
  
  // copy result from device to host
  real_t *resGPU = (real_t*) malloc( mem_size);
  cudaMemcpy2D(resGPU,  NX*sizeof(real_t),
	       d_data1, pitchBytes1     , NX*sizeof(real_t), NY,
	       cudaMemcpyDeviceToHost);
    
    
  ////////////////////////////////////////////////////////
  // compute reference (CPU) solution to 2D heat equation
  // for performance comparison
  ////////////////////////////////////////////////////////
  initCondition2D (data1);
  initCondition2D (data2);
  
  cutilSafeCall( cudaThreadSynchronize() );
  cutilCheckError( cutResetTimer( timer));
  cutilCheckError( cutStartTimer( timer));
  
  //timeval_t start,stop;
  //start = get_time();
  // time loop
  iTime=0;
  for (real_t t=0.0f; t<TMAX; t+=(2*DT), iTime+=2) {
    
    // compute next 2 time steps
    if (useOrder2) {
      heat2d_ftcs_cpu_order2( data1, data2);
      heat2d_ftcs_cpu_order2( data2, data1);
    } else if (useOrder2b) {
      heat2d_ftcs_cpu_order2b( data1, data2);
      heat2d_ftcs_cpu_order2b( data2, data1);
    } else {
      heat2d_ftcs_cpu_order4( data1, data2);
      heat2d_ftcs_cpu_order4( data2, data1);
    }
  }
  
  cutilSafeCall( cudaThreadSynchronize() );
  cutilCheckError( cutStopTimer( timer));
  real_t cpu_time = cutGetTimerValue( timer);
  
  printf( "CPU Processing time: %g (ms)\n", cpu_time);
  printf( "Speedup GPU/CPU : %f\n",cpu_time/gpu_time);

  cutilCheckError( cutDeleteTimer( timer));
   
  printf("...comparing the results\n");
  double sum = 0, delta = 0;
  for(unsigned i = 0; i < NX*NY; i++){
    delta += (resGPU[i] - data1[i]) * (resGPU[i] - data1[i]);
    sum   += data1[i] * data1[i];
  }
  double L2norm = sqrt(delta / sum);
  printf("iteration %d relative L2 norm: %.10g\n", iTime, L2norm);

 
  // cleanup memory
  free(data1);
  free(data2);
  free(resGPU);
  
  cutilSafeCall(cudaFree(d_data1));
  cutilSafeCall(cudaFree(d_data2));
  
  cudaThreadExit();
}
