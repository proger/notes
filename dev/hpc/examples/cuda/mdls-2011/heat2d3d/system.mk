#
# TP CUDA
# GPU school, Maison de la Simulation
# Saclay, December 2011
#

CC=gcc
CXX=g++

DEBUG=no

# PATH to CUDA toolkit
CUDAROOT=/usr/local/cuda40

# path to CUDA SDK (needed for cutil library)
CUDASDKROOT=/home/mdls/install/nvidia/sdk/NVIDIA_GPU_Computing_SDK4.0.17
CUTIL_LDFLAGS=-L$(CUDASDKROOT) -lcutil_x86_64
NVCC=$(CUDAROOT)/bin/nvcc

#
# nvcc flags. 
# --device-emulation to generate device emulation mode.
# --keep             to keep all intermediate files. 
# --keep --clean     to remove all intermediate files.
# -ptxas-options=-v
# see also nvcc man page
#
ifeq ($(DEBUG),yes)
NVCFLAGS=-g -O0 --compiler-options -O0,-Wall,-fPIC,-g --device-emulation
CFLAGS=-g3 -O0
else
NVCFLAGS=-O2 --compiler-options -fno-strict-aliasing 
CFLAGS=-O2
endif

#NVCFLAGS+= -gencode=arch=compute_10,code=\"sm_10,compute_10\" \
#	--ptxas-options=-v --use_fast_math \
#	-I $(CUDASDKROOT)/C/common/inc \
#	-I $(CUDASDKROOT)/include
#NVCFLAGS+= -gencode=arch=compute_13,code=\"sm_13,compute_13\" 
NVCFLAGS+= -arch=sm_21 \
	--ptxas-options=-v --use_fast_math \
	-I $(CUDASDKROOT)/C/common/inc \
	-I $(CUDASDKROOT)/include

CFLAGS+=-Wall -Wextra -fPIC \
        -L $(CUDAROOT)/lib64 -Wl,-rpath=$(CUDAROOT)/lib64 \
        -L $(CUDASDKROOT)/C/lib

# C++ flags
CXXFLAGS=$(CFLAGS)

# visualization with MGL
# uncomment the 3 following lines to enable MGL (http://mathgl.sourceforge.net/)
# install package libmgl-dev under Ubuntu
# CFLAGS   += -DUSE_MGL
# CXXFLAGS += -DUSE_MGL
# MGL_LDFLAGS= -lmgl



%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

%.o: %.cpp
#	@echo "########"
	@echo "OBJECT : $@"
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o: %.cu
#	@echo "########"
	@echo "OBJECT : $@"
	$(NVCC) $(NVCFLAGS) -c -o $@ $<

