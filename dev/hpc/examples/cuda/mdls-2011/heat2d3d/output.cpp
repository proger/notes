#include "output.h"

/**
 * \brief Save using XSM file format.
 * A one line header used in xsmurf.
 */
void save_bin(real_t* data, const char* prefix, int time, int sizeX, int sizeY)
{
  FILE *fp;
  char number[16];
  char filename[256] = "";
  
  // make file name
  strcat(filename,prefix);
  sprintf(number,"%03d.xsm",time);
  strcat(filename,number);
  
  // open, write data, close
  fp = fopen(filename,"w");
  fprintf(fp,"Binary 1 %dx%d %d(4 byte reals)\n",sizeX,sizeY,sizeX*sizeY);
  fwrite(data,sizeof(real_t), sizeX*sizeY, fp);
  fclose(fp);
  
} // save_bin

/**
 * \brief Save using PGM file format.
 */
void save_pgm(real_t* data, const char* prefix, int time, int sizeX, int sizeY)
{
  FILE *fp;
  char number[16];
  char filename[256] = "";
  
  // make file name
  strcat(filename,prefix);
  sprintf(number,"%03d.pgm",time);
  strcat(filename,number);
  
  // open, write data, close
  fp = fopen(filename,"w");
  fprintf(fp,"P5\n%d %d\n255\n",sizeX,sizeY);
  for (int index=0; index<sizeX*sizeY; ++index) {
    unsigned char tmp = (unsigned char) (data[index]*255.0f);
    fwrite(&tmp,sizeof(unsigned char), 1, fp);
  }
  fclose(fp);

} // save_pgm

/**
 * \brief Save results using MGL library and png file format
 */
void save_mgl(real_t* data, const char* prefix, int time, int sizeX, int sizeY)
{

#ifdef USE_MGL

  // build filename
  char number[16];
  char filename[256] = "";

  strcat(filename,prefix);
  sprintf(number,"%03d.png",time);
  strcat(filename,number);
  
  // create mglData object
  mglGraph *gr = new mglGraphZB;
  mglData a(sizeX,sizeY);
  a.Set(data,sizeX,sizeY);
  
  // do the plot
  gr->Rotate(40,60);
  gr->Light(true);
  gr->Box();
  gr->Surf(a);
  
  // save to file
  gr->WritePNG(filename, 0, true); 
  delete gr;

#else

  (void) data;
  (void) prefix;
  (void) time;
  (void) sizeX;
  (void) sizeY;

  printf("Warning : library MathGL is not available or disabled !\n");

#endif // USE_MGL

} // save_mgl

/**
 * \brief Save results using VTK file format (more precisely VTI for
 * image data).
 * \see http://www.vtk.org/Wiki/VTK_XML_Formats
 */
#define VTK_ASCII
void save_vtk(real_t* data, const char* prefix, int time)
{
  FILE *fp;
  char number[16];
  char filename[256] = "";
  
  // make file name
  strcat(filename,prefix);
  sprintf(number,"%03d.vti",time);
  strcat(filename,number);

  // open file
  fp = fopen(filename,"w");

  // if writing raw binary data (file does not respect XML standard)
#ifdef VTK_ASCII
  fprintf(fp,"<?xml version=\"1.0\"?>\n");
#endif // VTK_ASCII

  // write xml data header
  if (isBigEndian())
    fprintf(fp, "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"BigEndian\">\n");
  else
    fprintf(fp, "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\">\n");

  fprintf(fp, "  <ImageData WholeExtent=\"%d %d %d %d %d %d\" Origin=\"0 0 0\" Spacing=\"1 1 1\">\n",0,NX-1,0,NY-1,0,NZ-1);
  fprintf(fp, "  <Piece Extent=\"%d %d %d %d %d %d\">\n",0,NX-1,0,NY-1,0,NZ-1);
  fprintf(fp, "    <PointData>\n");
 

#ifdef VTK_ASCII

  // write ascii data
#ifdef USE_DOUBLE
  fprintf(fp, "      <DataArray type=\"Float64\" Name=\"%s\" format=\"ascii\" >", "scalar_data");
#else
  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"%s\" format=\"ascii\" >", "scalar_data");
#endif // USE_DOUBLE

  for(int k = 0; k < NZ; k++) {
    for(int j = 0; j < NY; j++) {
      for(int i = 0; i < NX; i++) {
	int index = i+NX*(j+NY*k);
	fprintf(fp, "%g ", data[index]);
      }
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "      </DataArray>\n");

  // write trailer
  fprintf(fp, "    </PointData>\n");
  fprintf(fp, "    <CellData>\n");
  fprintf(fp, "    </CellData>\n");
  fprintf(fp, "  </Piece>\n");
  fprintf(fp, "  </ImageData>\n");
  fprintf(fp, "</VTKFile>\n");
	    
#else

  // write binary data using appended (but no base 64 encoding)
#ifdef USE_DOUBLE
  fprintf(fp, "      <DataArray type=\"Float64\" Name=\"%s\" format=\"appended\" offset=\"0\" />\n","scalar_data"); 
#else
  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"%s\" format=\"appended\" offset=\"0\" />\n","scalar_data"); 
#endif // USE_DOUBLE

  fprintf(fp, "    </PointData>\n");
  fprintf(fp, "    <CellData>\n");
  fprintf(fp, "    </CellData>\n");
  fprintf(fp, "  </Piece>\n");
  fprintf(fp, "  </ImageData>\n");
  
  fprintf(fp, "  <AppendedData encoding=\"raw\">\n");
  
  // write the leading undescore
  fprintf(fp, "_");
  
  // then write heavy data (column major format)
  unsigned int nbOfWords = NX*NY*NZ*sizeof(real_t);
  fwrite( (void *) &nbOfWords, sizeof(unsigned int), 1, fp);
  
  fwrite((void *)data, sizeof(real_t), NX*NY*NZ, fp);
  
  fprintf(fp, "  </AppendedData>\n");
  fprintf(fp, "</VTKFile>\n");
	
#endif // VTK_ASCII

  // close vtk file
  fclose(fp);

} // save_vtk

/**
 * \brief Save using XSM file format.
 * A one line header used in xsmurf.
 */
void save_bin_3d(real_t* data, const char* prefix, int time, 
		 int sizeX, int sizeY, int sizeZ)
{
  FILE *fp;
  char number[16];
  char filename[256] = "";
  
  // make file name
  strcat(filename,prefix);
  sprintf(number,"%03d.xsm",time);
  strcat(filename,number);
  
  // open, write data, close
  fp = fopen(filename,"w");
  fprintf(fp,"Binary 1 %dx%dx%d %d(4 byte reals)\n",sizeX,sizeY,sizeZ,sizeX*sizeY*sizeZ);
  fwrite(data,sizeof(real_t), sizeX*sizeY*sizeZ, fp);
  fclose(fp);
  
} // save_bin_3d

/**
 * \brief Save results using MGL library and png file format
 */
void save_mgl_3d(real_t* data, const char* prefix, int time, 
		 int sizeX, int sizeY, int sizeZ)
{

#ifdef USE_MGL

  // build filename
  char number[16];
  char filename[256] = "";

  strcat(filename,prefix);
  sprintf(number,"%03d.png",time);
  strcat(filename,number);
  
  // create mglData object
  mglGraph *gr = new mglGraphZB;
  mglData a(sizeX,sizeY,sizeZ);
  a.Set(data,sizeX,sizeY,sizeZ);
  
  // do the plot
  gr->Rotate(40,60);
  gr->Light(true);
  gr->Box();
  
  gr->Alpha(true);
  gr->Surf3(a);
  /*gr->DensX(a.Sum("x"),"",-1);
  gr->DensY(a.Sum("y"),"",1);
  gr->DensZ(a.Sum("z"),"",-1);*/
  
  // save to file
  gr->WritePNG(filename, 0, true); 
  delete gr;

#else

  (void) data;
  (void) prefix;
  (void) time;
  (void) sizeX;
  (void) sizeY;
  (void) sizeZ;

  printf("Warning : library MathGL is not available or disabled !\n");

#endif // USE_MGL

} // save_mgl_3d
