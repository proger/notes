!
! This version uses the Nvidia CUBLAS library with non thunking !
!
module precision_m

  integer, parameter :: sp_kind   = kind(1.0)  !< single precision
  integer, parameter :: dp_kind   = kind(1.d0) !< double precision
  integer, parameter :: int_kind  = kind(1)

  ! default: use single precison
  integer, parameter :: fp_kind = sp_kind

  contains

    function isBigEndian()
      integer, parameter :: short = selected_int_kind(4)
      integer( short ) :: source = 1_short
      logical :: isBigEndian
      isBigEndian = .false.
      if ( iachar( transfer( source, 'a' ) ) == 0 ) isBigEndian = .true.
      return
    end function isBigEndian

    function useDoublePrecision()
      real(fp_kind) :: tmp
      logical :: useDoublePrecision
      useDoublePrecision = .false.
      if (kind(tmp) == dp_kind ) useDoublePrecision=.true.
    end function useDoublePrecision

end module precision_m

module kernel_m

  use precision_m

contains
  
  !! SAXPY on CPU
  subroutine saxpy_cpu (a , x, y, N )
    real(fp_kind),    intent(in)    :: a
    real(fp_kind),    intent(inout) :: x (N)
    real(fp_kind),    intent(in)    :: y (N)
    integer, intent(in)    :: N
    ! local variables
    integer :: i

    do i=1,N
       x(i) = a*x(i)+y(i)
    end do

  end subroutine saxpy_cpu

  attributes ( global ) subroutine saxpy_gpu(a, x, y, N)
    real(fp_kind),    intent(in)   , value  :: a
    real(fp_kind),    intent(inout), device :: x (N)
    real(fp_kind),    intent(in)   , device :: y (N)
    integer,          intent(in)   , value  :: N

    ! local variables
    integer :: i
    i = ( blockIdx %x -1)* blockDim % x + threadIdx % x
    if (i <= N) x(i) = a*x(i) + y(i)

  end subroutine saxpy_gpu

end module kernel_m

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! TIMER CPU
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module timer_cpu_m
  
  use precision_m

  ! declare a Timer type
  type Timer
     real(dp_kind) :: elapsed=0.0
     real(dp_kind) :: start=0.0
     real(dp_kind) :: stop=0.0
  end type Timer

  type(Timer) :: cpu_timer

contains
  
  ! start timer
  subroutine timerStart(t)
    implicit none
    type(Timer), intent(inout) :: t
    
    call cpu_time(t%start)
    
  end subroutine timerStart
  
  ! stop timer and accumulate timings
  subroutine timerStop(t)
    implicit none
    type(Timer), intent(inout) :: t
    
    call cpu_time(t%stop)
    t%elapsed = t%elapsed + t%stop - t%start
    
  end subroutine timerStop
  
end module timer_cpu_m

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! TIMER GPU
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module timer_gpu_m
  
  use CudaFor
  use precision_m
  
  ! declare a timer for GPU execution
  type CudaTimer
     type(CudaEvent)    :: startEv, stopEv
     real(dp_kind)      :: elapsed=0.d0
  end type CudaTimer
  
  ! GPU timers
  type(CudaTimer) :: gpu_timer
   
contains
  
  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! Cuda timer routines
  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! create cuda timers
  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine init_cuda_timer
    
    implicit none
    
    integer :: istat
    
    ! create events
    istat = cudaEventCreate(gpu_timer%startEv)
    istat = cudaEventCreate(gpu_timer%stopEv)
    
  end subroutine init_cuda_timer
  
  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! delete cuda timers
  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine cleanup_cuda_timer
    
    implicit none
    
    integer :: istat

    ! destroy events
    istat = cudaEventDestroy(gpu_timer%startEv)
    istat = cudaEventDestroy(gpu_timer%stopEv)
    
  end subroutine cleanup_cuda_timer

  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! start a cuda timer
  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine cudaTimerStart(t)
    implicit none
    type(CudaTimer), intent(inout) :: t
    
    integer :: istat
    istat = cudaEventRecord(t%startEv, 0)

  end subroutine cudaTimerStart

  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! reset a cuda timer
  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine cudaTimerReset(t)
    implicit none
    type(CudaTimer), intent(inout) :: t
    
    t%elapsed=0.0

  end subroutine cudaTimerReset
 
  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! stop a cuda timer
  !! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine cudaTimerStop(t)
    implicit none
    type(CudaTimer), intent(inout) :: t
    
    integer :: istat
    real(kind=sp_kind) :: gpu_time_ms ! time in milli-second

    istat = cudaEventRecord(t%stopEv, 0)
    istat = cudaEventSynchronize(t%stopEv)

    ! get elapsed time since last start
    istat = cudaEventElapsedTime(gpu_time_ms, t%startEv, t%stopEv)

    ! accumulate gpu_time_ms into elapsed
    t%elapsed = t%elapsed + 1e-3*gpu_time_ms ! elapsed in second
    
  end subroutine cudaTimerStop
  
end module Timer_gpu_m

!! interface to the C/CUDA-library CUBLAS
!! using iso_c_binding
module cublas_interf_m

  interface cublasInit
     integer function cublasInit () bind (C, name ='cublasInit')
     end function cublasInit
  end interface cublasInit
  
  interface cublasSaxpy
     subroutine cublasSaxpy (n , alpha, x, incx, y, incy) bind (C, name ='cublasSaxpy')
       use iso_c_binding
       integer ( c_int ), value :: n, incx, incy
       real ( c_float ),  value :: alpha
       real ( c_float ), device :: x(n) , y(n)
     end subroutine cublasSaxpy
  end interface cublasSaxpy
  
end module cublas_interf_m


!!
!! MAIN PROGRAM
!!
program saxpy_cublas_fortran

  use cudafor
  use kernel_m
  use timer_cpu_m
  use timer_gpu_m
  use cublas_interf_m

  implicit none

  integer , parameter    :: n =4*1024*1024 , blockSize = 256
  integer , parameter    :: numTimingReps=100 ! number of repetitions for timings
  real                   :: alpha
  real(fp_kind)          :: h_x(n), h_y(n)
  real(fp_kind) , device :: d_x(n), d_y(n)
  integer                :: i,iter,istat
  real(dp_kind)          :: elapsed

  !! cuda parameters
  integer, parameter :: numThreadsPerBlock = 128
  integer, parameter :: numBlocks = (N+numThreadsPerBlock-1) / numThreadsPerBlock


  !! initialize cuda timer
  call init_cuda_timer()

  !! initialize data on CPU
  alpha = 2.0
  do i=1,n
     h_x(i)=1.0+i
     h_y(i)=N-i+1
  end do

  !! copy data to GPU
  d_x = h_x
  d_y = h_y

  !! compute on CPU
  call timerStart(cpu_timer)
  do iter=1,numTimingReps
     call saxpy_cpu(alpha,h_x,h_y,N)
  end do
  call timerStop(cpu_timer)
  elapsed = cpu_timer%elapsed
  write(*,*) 'OUR CPU CODE: ',N,'elements', (elapsed*1000)/numTimingReps,' ms per iteration, ',2*N*numTimingReps/(elapsed*1e9),' GFLOP/s', 3*N*4*numTimingReps / (elapsed*1e9),' GB/s'


  !! init cuBlas
  istat = cublasInit()

  !! copy data to GPU
  d_x = h_x
  d_y = h_y

  !! compute on GPU
  call cudaTimerStart(gpu_timer)
  do i=1,numTimingReps
     call cublasSaxpy (N, alpha, d_x, 1, d_y, 1 )
  end do
  call cudaTimerStop(gpu_timer)
  elapsed = gpu_timer%elapsed
  write(*,*) 'CUBLAS GPU CODE: ',N,'elements', (elapsed*1000)/numTimingReps,' ms per iteration, ',2*N*numTimingReps/(elapsed*1e9),' GFLOP/s', 3*N*4*numTimingReps / (elapsed*1e9),' GB/s'


  !! cleanup
  call cleanup_cuda_timer()

end program saxpy_cublas_fortran
