/* Macro & function for handling CUDA error codes */

void handle_error(cudaError_t err, const char *file, int line);

#define HANDLE_ERROR(err) (handle_error(err, __FILE__, __LINE__))
