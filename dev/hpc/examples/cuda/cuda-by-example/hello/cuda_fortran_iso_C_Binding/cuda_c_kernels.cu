#include <stdio.h>

__global__ void add( int a, int b, int *c ) {
  *c = a + b;
}

/*
 * The extern "C" tells the CUDA compiler NVCC to compile the function as if it is a C function and thus it can be linked in to FORTRAN. Remember nvcc is a c/c++ compiler.
 */
extern "C"
{

  void add_kernel_wrapper(int a, int b, int *c, dim3 *dimGrid, dim3 *dimBlock)
  {
    printf("Calling now cuda/c kernel...\n");
    add<<<*dimGrid, *dimBlock>>>(a,b,c);
  }
}
