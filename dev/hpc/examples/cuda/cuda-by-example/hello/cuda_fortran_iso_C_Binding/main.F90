!
! Simplest hello world in CUDA/Fortran using iso_c_binding
!


program helloCudaFortran

  use cuda
  use m_kernels_interface

  implicit none

  integer :: a,b
  integer, target :: c
  integer, pointer :: c_addr

  ! cuda variables
  type (dim3) :: nblk, nthrd
  type (c_ptr) :: err_ptr
  type (c_ptr) :: dev_c

  integer(c_int) :: NTHREADS_PER_BLOCK = 1
  integer(c_int) :: NUM_BLOCKS = 1

  ! other variables
  logical :: err_bool
  integer(kind(cudaSuccess)) :: err

  ! pas tres joli-joli (ugly macro)
#define HANDLE_ERROR( return_err )  call my_cudaErrorCheck( return_err, err_bool )

  a=2
  b=7
  c_addr => c

  ! device memory allocation
  HANDLE_ERROR( cudaMalloc( dev_c, 4 ) )
  
  ! perform computation on GPU
  nblk = dim3( NUM_BLOCKS ,1, 1 )
  nthrd = dim3( NTHREADS_PER_BLOCK, 1, 1 )

  call scalarAdd(a, b, dev_c, nblk, nthrd)

  ! copy back result into host memory
  err = cudaMemcpy(c_loc(c_addr), dev_c, 4, cudaMemcpyDeviceToHost)

  ! print result on standard output
  write (*,*) 'Result of GPU computations:'
  write (*,*) a,'+',b,'=',c
  
  if (c /= 9) then
     write (*,*) 'version 1 Program failed'
  else
     write(*,*) 'version 1 Program passed'
  end if

end program helloCudaFortran
