/*
 * Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property and 
 * proprietary rights in and to this software and related documentation. 
 * Any use, reproduction, disclosure, or distribution of this software 
 * and related documentation without an express license agreement from
 * NVIDIA Corporation is strictly prohibited.
 *
 * Please refer to the applicable NVIDIA end user license agreement (EULA) 
 * associated with this source code for terms and conditions that govern 
 * your use of this NVIDIA software.
 * 
 */

/*
 * CUDA by example, by Jason Sanders and Edward Kandrot,
 * Addison-Wesley, 2010
 * http://developer.nvidia.com/cuda-example-introduction-general-purpose-gpu-programming
 */


#include <stdio.h>

/*
 * handle CUDA-related error messages
 */
static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
  if (err != cudaSuccess) {
    printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
	    file, line );
    exit( EXIT_FAILURE );
  }
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))


/**
 * a simple CUDA kernel
 *
 * \param[in]  a input integer
 * \param[in]  b input integer
 * \param[out] c pointer-to-integer for result
 */
__global__ void add( int a, int b, int *c ) {
  *c = a + b;
}

/*
 * main
 */
int main( void ) {
  int c;
  int *dev_c;
  
  // GPU device memory allocation
  HANDLE_ERROR( cudaMalloc( (void**)&dev_c, sizeof(int) ) );
  
  // perform computation on GPU
  add<<<1,1>>>( 2, 7, dev_c );

  // get back computation result into host CPU memory
  HANDLE_ERROR( cudaMemcpy( &c, dev_c, sizeof(int),
			    cudaMemcpyDeviceToHost ) );

  // output result on screen
  printf( "2 + 7 = %d\n", c );

  // de-allocate GPU device memory
  HANDLE_ERROR( cudaFree( dev_c ) );

  return 0;
}
