/* 
 * nvcc --ptxas-options -v -o helloCudaC_array helloword_array.cu
 */

/*
 * adapted from :
 * CUDA by example, by Jason Sanders and Edward Kandrot,
 * Addison-Wesley, 2010
 * http://developer.nvidia.com/cuda-example-introduction-general-purpose-gpu-programming
 */

/* Modified version to handle arrays */


#include <stdio.h>

/*
 * handle CUDA-related error messages
 */
static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
  if (err != cudaSuccess) {
    printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
	    file, line );
    exit( EXIT_FAILURE );
  }
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))


/**
 * a simple CUDA kernel
 *
 * \param[in]  a input integer
 * \param[in]  b input integer
 * \param[out] c pointer-to-integer for result
 */
__global__ void add( int *a, int *b, int *c, int n ) {

  int i = threadIdx.x + blockIdx.x*blockDim.x;

  if (i<n)
    c[i] = a[i] + b[i];
}

/*
 * main
 */
int main( void ) {
  // array size
  int N = 16;

  // host variables
  int *a, *b, *c;

  // device variables
  int *dev_a, *dev_b, *dev_c;
  
  // CPU memory allocation / initialization
  a = (int *) malloc(N*sizeof(int));
  b = (int *) malloc(N*sizeof(int));
  c = (int *) malloc(N*sizeof(int));
  for (int i=0; i<N; i++) {
    a[i]=i;
    b[i]=N-i;
  }

  // GPU device memory allocation / initialization
  HANDLE_ERROR( cudaMalloc( (void**)&dev_a, N*sizeof(int) ) );
  HANDLE_ERROR( cudaMalloc( (void**)&dev_b, N*sizeof(int) ) );
  HANDLE_ERROR( cudaMalloc( (void**)&dev_c, N*sizeof(int) ) );
  HANDLE_ERROR( cudaMemcpy( dev_a, a, N*sizeof(int),
			    cudaMemcpyHostToDevice ) );
  HANDLE_ERROR( cudaMemcpy( dev_b, b, N*sizeof(int),
			    cudaMemcpyHostToDevice ) );


  // perform computation on GPU
  int nbThreads = 8;
  dim3 blockSize(nbThreads,1,1);
  dim3 gridSize(N/nbThreads+1,1,1);
  add<<<blockSize,gridSize>>>( dev_a, dev_b, dev_c, N );

  // get back computation result into host CPU memory
  HANDLE_ERROR( cudaMemcpy( c, dev_c, N*sizeof(int),
			    cudaMemcpyDeviceToHost ) );

  // output result on screen
  int passed=1;
  for (int i=0; i<N; i++) {
    if (c[i] != N) {
      passed = 0;
      printf("wrong value : %d %d\n",i,c[i]);
    }
  }
  if (passed) {
    printf("test succeeded !\n");
  } else {
    printf("test failed !\n");
  }

  // de-allocate CPU host memory
  free(c);
  free(b);
  free(a);

  // de-allocate GPU device memory
  HANDLE_ERROR( cudaFree( dev_c ) );
  HANDLE_ERROR( cudaFree( dev_b ) );
  HANDLE_ERROR( cudaFree( dev_a ) );

  return 0;
}
