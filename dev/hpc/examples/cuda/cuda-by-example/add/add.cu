/*
 * Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property and 
 * proprietary rights in and to this software and related documentation. 
 * Any use, reproduction, disclosure, or distribution of this software 
 * and related documentation without an express license agreement from
 * NVIDIA Corporation is strictly prohibited.
 *
 * Please refer to the applicable NVIDIA end user license agreement (EULA) 
 * associated with this source code for terms and conditions that govern 
 * your use of this NVIDIA software.
 * 
 */

/*
 * CUDA by example, by Jason Sanders and Edward Kandrot,
 * Addison-Wesley, 2010
 * http://developer.nvidia.com/cuda-example-introduction-general-purpose-gpu-programming
 */


#include <stdio.h>

/*
 * handle CUDA-related error messages
 */
static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
  if (err != cudaSuccess) {
    printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
	    file, line );
    exit( EXIT_FAILURE );
  }
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))


/**
 * a simple CUDA kernel
 *
 * \param[in]  a input integer
 * \param[in]  b input integer
 * \param[out] c pointer-to-integer for result
 */
__global__ void add( int *a, int *b, int *c ) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
  	c[idx] = a[idx] + b[idx];
}

#define N 16

/*
 * main
 */
int main( void ) {
	int a[N], b[N], c[N];
	int *dev_a, *dev_b, *dev_c;

	// init a & b
	for (int i = 0 ; i < N ; ++i)
		b[i] = (a[i] = i) + 10;

	// GPU device memory allocation
	HANDLE_ERROR( cudaMalloc( (void**)&dev_a, sizeof(int) * N) );
	HANDLE_ERROR( cudaMalloc( (void**)&dev_b, sizeof(int) * N) );
	HANDLE_ERROR( cudaMalloc( (void**)&dev_c, sizeof(int) * N) );

	// copy a & b
	HANDLE_ERROR(cudaMemcpy(dev_a, a, sizeof(int) * N, cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(dev_b, b, sizeof(int) * N, cudaMemcpyHostToDevice));

	// perform computation on GPU
	dim3 dimBlock(N / 2);
	dim3 dimGrid(2);
	add<<<dimGrid, dimBlock>>>(dev_a, dev_b, dev_c );

	// get back computation result into host CPU memory
	HANDLE_ERROR(cudaMemcpy( c, dev_c, sizeof(int) * N, cudaMemcpyDeviceToHost ) );

	// output result on screen
	for (int i = 0 ; i < N ; ++i)
		printf( "%d + %d = %d\n", a[i], b[i], c[i] );

	// de-allocate GPU device memory
	HANDLE_ERROR(cudaFree(dev_a));
	HANDLE_ERROR(cudaFree(dev_b));
	HANDLE_ERROR(cudaFree(dev_c));

	return 0;
}
