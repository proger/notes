#include <stdio.h>
#include <stdlib.h>

#define N 1000

/********
 * MAIN *
 ********/

int main(int argc, char* argv[]) {
	float *a, *b, *c;

	/* Create arrays */
	a = (float*)malloc(sizeof(float) * N);
	if ( ! a) {
		printf("malloc error.\n");
		exit(1);
	}
	b = (float*)malloc(sizeof(float) * N);
	if ( ! b) {
		printf("malloc error.\n");
		exit(1);
	}
	c = (float*)malloc(sizeof(float) * N);
	if ( ! c) {
		printf("malloc error.\n");
		exit(1);
	}

	/* Initialize arrays */
	float x = 1;
	float *p, *q;
	for (p = a, q = b ; p < a + N ; ++p, ++q) {
		*p = x++;
		*q = 1;
	}

	/* Compute */
	float *r;
	for (p = a, q = b, r = c ; p < a + N ; ++p, ++q, ++r)
		*r = *p + *q;

	// Check result
	x = 2;
	for (p = c ; p < c + N ; ++p) {
		if (*p != x++) {
			printf("Wrong result !\n");
			break;
		}
	}

	/* Print c array */
	for (p = c ; p < c + 10 && p < c + N ; ++p)
		printf("%f ", *p);
	printf("...");
	for (p = c + N - 10 ; p >= c && p < c + N ; ++p)
		printf(" %f", *p);
	printf("\n");

	/* Free memory */
	free(c);
	free(b);
	free(a);

	return 0;
}
