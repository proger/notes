#include <cuda_runtime.h>
#include <stdio.h>

#define N 1000

__global__ void add(float *a, float *b, float *c, int n) {

	int id = blockDim.x * blockIdx.x + threadIdx.x;

	if (id < n)
		c[id] = a[id] + b[id];
}

/********
 * MAIN *
 ********/

int main(int argc, char* argv[]) {

	int dev;
	cudaError_t err;
	float *a, *b, *c;
	float *d_a, *d_b, *d_c;

	/* Create arrays */
	a = (float*)malloc(sizeof(float) * N);
	if ( ! a) {
		printf("malloc error.\n");
		exit(1);
	}
	b = (float*)malloc(sizeof(float) * N);
	if ( ! b) {
		printf("malloc error.\n");
		exit(1);
	}
	c = (float*)malloc(sizeof(float) * N);
	if ( ! c) {
		printf("malloc error.\n");
		exit(1);
	}

	/* Initialize arrays */
	float x = 1;
	float *p, *q;
	for (p = a, q = b ; p < a + N ; ++p, ++q) {
		*p = x++;
		*q = 1;
	}

	/* Device count */
	err = cudaGetDevice(&dev);
	if (err != cudaSuccess) {
		printf("cudaGetDevice error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	printf("Running on device %d\n", dev);

	/* Set grid and block dimensions */
	dim3 gridDim(N / 1024 + 1);  // 5000 thread blocks
	dim3 blockDim(1024); // nb threads per block


	cudaEvent_t start, end;
	float eventTime;
 	err = cudaEventCreate(&start);
	if (err != cudaSuccess) {
		printf("cudaEventCreate error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	err = cudaEventCreate(&end);
	if (err != cudaSuccess) {
		printf("cudaEventCreate error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	err = cudaEventRecord(start, 0);
	if (err != cudaSuccess) {
		printf("cudaEventRecord error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Allocate memory on device
	err = cudaMalloc((void**)&d_a, sizeof(float) * N);
	if (err != cudaSuccess) {
		printf("cudaMalloc error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Allocate memory on device
	err = cudaMalloc((void**)&d_b, sizeof(float) * N);
	if (err != cudaSuccess) {
		printf("cudaMalloc error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Allocate memory on device
	err = cudaMalloc((void**)&d_c, sizeof(float) * N);
	if (err != cudaSuccess) {
		printf("cudaMalloc error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Copy buffer from host to device
	err = cudaMemcpy(d_a, a, N * sizeof(float), cudaMemcpyHostToDevice);
	if (err != cudaSuccess) {
		printf("cudaMemcpy error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Copy buffer from host to device
	err = cudaMemcpy(d_b, b, N * sizeof(float), cudaMemcpyHostToDevice);
	if (err != cudaSuccess) {
		printf("cudaMemcpy error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Run kernel
	add<<<gridDim, blockDim>>>(d_a, d_b, d_c, N);

	// Copy buffer from device to host
	err = cudaMemcpy(c, d_c, N * sizeof(float), cudaMemcpyDeviceToHost);
	if (err != cudaSuccess) {
		printf("cudaMemcpy error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Check result
	x = 2;
	for (p = c ; p < c + N ; ++p) {
		if (*p != x++) {
			printf("Wrong result !\n");
			break;
		}
	}

	// Free memory on device
	err = cudaFree((void*)d_c);
	if (err != cudaSuccess) {
		printf("cudaFree error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Free memory on device
	err = cudaFree((void*)d_b);
	if (err != cudaSuccess) {
		printf("cudaFree error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Free memory on device
	err = cudaFree((void*)d_a);
	if (err != cudaSuccess) {
		printf("cudaFree error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	err = cudaEventRecord(end, 0);
	if (err != cudaSuccess) {
		printf("cudaEventRecord error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	err = cudaEventSynchronize(end);
	if (err != cudaSuccess) {
		printf("cudaEventSynchronize error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	err = cudaEventElapsedTime(&eventTime, start, end);
	if (err != cudaSuccess) {
		printf("cudaEventElapsedTime error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	printf("Elapsed time = %f ms\n", eventTime);

	/* Print c array */
	for (p = c ; p < c + 10 && p < c + N ; ++p)
		printf("%f ", *p);
	printf("...");
	for (p = c + N - 10 ; p >= c && p < c + N ; ++p)
		printf(" %f", *p);
	printf("\n");


	/* Free memory */
	free(c);
	free(b);
	free(a);

	return 0;
}
