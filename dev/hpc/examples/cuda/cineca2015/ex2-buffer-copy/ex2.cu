#include <cuda_runtime.h>
#include <stdio.h>

#define N 1000

/********
 * MAIN *
 ********/

int main(int argc, char* argv[]) {

	int dev;
	cudaError_t err;
	float *a;
	float *d_a;

	a = (float*)malloc(sizeof(float) * N);
	if ( ! a) {
		printf("malloc error.\n");
		exit(1);
	}

	/* Device count */
	err = cudaGetDevice(&dev);
	if (err != cudaSuccess) {
		printf("cudaGetDevice error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	printf("Running on device %d\n", dev);

	cudaEvent_t start, end;
	float eventTime;
 	err = cudaEventCreate(&start);
	if (err != cudaSuccess) {
		printf("cudaEventCreate error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	err = cudaEventCreate(&end);
	if (err != cudaSuccess) {
		printf("cudaEventCreate error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	err = cudaEventRecord(start, 0);
	if (err != cudaSuccess) {
		printf("cudaEventRecord error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Allocate memory on device
	err = cudaMalloc((void**)&d_a, sizeof(float) * N);
	if (err != cudaSuccess) {
		printf("cudaMalloc error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Copy buffer from host to device
	err = cudaMemcpy(d_a, a, N * sizeof(float), cudaMemcpyHostToDevice);
	if (err != cudaSuccess) {
		printf("cudaMemcpy error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Copy buffer from device to host
	err = cudaMemcpy(a, d_a, N * sizeof(float), cudaMemcpyDeviceToHost);
	if (err != cudaSuccess) {
		printf("cudaMemcpy error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	// Free memory on device
	err = cudaFree((void*)d_a);
	if (err != cudaSuccess) {
		printf("cudaFree error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

	err = cudaEventRecord(end, 0);
	if (err != cudaSuccess) {
		printf("cudaEventRecord error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	err = cudaEventSynchronize(end);
	if (err != cudaSuccess) {
		printf("cudaEventSynchronize error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	err = cudaEventElapsedTime(&eventTime, start, end);
	if (err != cudaSuccess) {
		printf("cudaEventElapsedTime error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	printf("Elapsed time = %f ms\n", eventTime);

	/* Free memory */
	free(a);

	return 0;
}
