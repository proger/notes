#include <cuda_runtime.h>
#include <stdio.h>

/*cudaError_t err;
#define ERR_CHECK_1(fct, param1) { err = fct(param1); if (err != cudaSuccess) { printf(fct ## " error: %s\n", cudaGetErrorString(err); exit(1); } }
*/

/********
 * MAIN *
 ********/

int main(int argc, char* argv[]) {

	int nb_devices;
	cudaError_t err;

	/* Device count */
	err = cudaGetDeviceCount(&nb_devices);
	if (err != cudaSuccess) {
		printf("cudaGetDeviceCount error: %s\n", cudaGetErrorString(err));
		exit(1);
	}
	printf("Nb CUDA GPUs = %d\n", nb_devices);

	/* Loop on all devices */
	for (int i_dev = 0 ; i_dev < nb_devices ; ++i_dev) {

		cudaDeviceProp prop;

		/* Get device properties */
		err = cudaGetDeviceProperties(&prop, i_dev);
		if (err != cudaSuccess) {
			printf("cudaGetDeviceProperties error: %s\n", cudaGetErrorString(err));
			exit(1);
		}

		/* Print device information */
		/*printf("Device %d: compute mode = %s\n", i_dev, prop.computeMode);*/
		printf("Device %d: compute capability = %d.%d\n", i_dev, prop.major, prop.minor);
		printf("Device %d: global memory = %ld MB\n", i_dev, prop.totalGlobalMem / 1024 / 1024);
		printf("Device %d: shared memory = %ld\n", i_dev, prop.sharedMemPerBlock);
		printf("Device %d: constant memory = %ld\n", i_dev, prop.totalConstMem);
		printf("Device %d: num threads per block = %d\n", i_dev, prop.maxThreadsPerBlock);
		printf("Device %d: num registers per block = %d\n", i_dev, prop.regsPerBlock);
	}

	return 0;
}
