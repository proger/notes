/* Make a sum of a series of numbers, using a regular repartition between threads,
   but summing results of threads using a tree. This summing technic is efficient when running on n cores with n big enough,
   since instead of having each thread adding its result inside the global sum (n operations), each thread sends its sum to another using a tree repartition, and thus doing only log2(n) operations. 

   Uses bitwise operators to determine senders and receivers.
   Number of cores/threads is not necessarily a power of 2. */

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <unistd.h>
#include <string.h>

/* Gets element i of vector */
long get_element(long i) {
	usleep(100);
	return i;
}

/* Thead function */
void thread_sum_fct(long n, long(*get_fct)(long), int *thread_done, long *thread_sum, long *global_sum) {
	int p_i = omp_get_thread_num();
	int p = omp_get_num_threads();

	/* sum my part of the vector */
	long sum = 0;
	long first_i = p_i * n / p;
	long last_i = p_i == p - 1 ? n : (p_i + 1) * n / p;
	long i;
	for (i = first_i ; i < last_i ; ++i)
		sum += (*get_fct)(i);

	/* Add sum of other threads */
	int bitmask = 1;
	while ((p_i & bitmask) == 0 /* is receiver */
	       && (p_i ^ bitmask) < p /* sender exists */
	      ) {
		while ( ! thread_done[p_i ^ bitmask]) {} /* wait for other thread to finish */
		sum += thread_sum[p_i ^ bitmask];
		bitmask <<= 1;
	}

	/* master thread set the global sum */
	if (p_i == 0)
		*global_sum = sum;

	/* other threads set their sum in shared array and signal they're done. */
	else {
		thread_sum[p_i] = sum;
		thread_done[p_i] = 1;
	}
}

/* MAIN */
int main(int argc, char* argv[]) {

	/* set default values */
	long n = 10;
	int p = 4;

	/* read arguments */
	if (argc >= 1) /* first argument is size of array */
		n = atol(argv[1]);
	if (argc >= 2) /* second argument is number of threads */
		p = atoi(argv[2]); 

	/* initialize shared data */
	int *thread_done = (int*)malloc(sizeof(int) * p);
	memset(thread_done, 0, sizeof(int) * p);
	long *thread_sum = (long*)malloc(sizeof(long) * p);
	memset(thread_sum, 0, sizeof(long) * p);

	/* sum */
	long sum = 0;
#pragma omp parallel num_threads(p)
	thread_sum_fct(n, get_element, thread_done, thread_sum, &sum);

	/* display sum */
	printf("%ld\n", sum);

	return 0;
}

