/* Make a sum of a series of numbers, using a regular repartition between threads,
   but summing results of threads using a tree. This summing technic is efficient when running on n cores with n big enough,
   since instead of having each thread adding its result inside the global sum (n operations), each thread sends its sum to another using a tree repartition, and thus doing only log2(n) operations.

   Number of cores/threads is not necessarily a power of 2.

   Number of additions made by the core 0 is n / p + log2(p).
   Number of receives made by the core 0 is log2(p).
   Total of operations made by core 0 is n/p+2*log2(p).

   In a sum made on one processor only, the number of operations is n.
   In a sum made on n cores, with final sum made by core 0, number of operations made by core 0 is : n/p+p.

   Array of nunber of operations, number of cores (p) varies in lines, number of elements to sum (n) in columns:
   p \ n 1024   65536   4294967296
   1     1024	65536	4294967296
   2     514	32770	2147483650
   4     260	16388	1073741828
   8     134	8198	536870918
   16    72	4104	268435464
   32    42	2058	134217738
   64    28	1036	67108876
   128   22	526	33554446
   256   20	272	16777232
   512   20	146	8388626
   1024  21	84	4194324
   2048  22	54	2097174
   4096  24	40	1048600
   8192  26	34	524314
   16384 28	32	262172
   32768 30	32	131102
   65536 32	33	65568
*/

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <unistd.h>
#include <string.h>

/* Gets element i of vector */
long get_element(long i) {
	usleep(100);
	return i;
}

/* Thead function */
void thread_sum_fct(long n, long(*get_fct)(long), int *thread_done, long *thread_sum, long *global_sum) {
	int p_i = omp_get_thread_num();
	int p = omp_get_num_threads();

	/* sum my part of the vector */
	long sum = 0;
	long first_i = p_i * n / p;
	long last_i = p_i == p - 1 ? n : (p_i + 1) * n / p;
	long i;
	for (i = first_i ; i < last_i ; ++i)
		sum += (*get_fct)(i);

	/* Add sum of other threads */
	int divisor = 2;
	int core_difference = 1;
	while (p_i % divisor == 0 && p_i + core_difference < p) {
		while ( ! thread_done[p_i + core_difference]) {} /* wait for other thread to finish */
		sum += thread_sum[p_i + core_difference];
		core_difference *= 2;
		divisor *= 2;
	}

	/* master thread set the global sum */
	if (p_i == 0)
		*global_sum = sum;

	/* other threads set their sum in shared array and signal they're done. */
	else {
		thread_sum[p_i] = sum;
		thread_done[p_i] = 1;
	}
}

/* MAIN */
int main(int argc, char* argv[]) {

	/* set default values */
	long n = 10;
	int p = 4;

	/* read arguments */
	if (argc >= 1) /* first argument is size of array */
		n = atol(argv[1]);
	if (argc >= 2) /* second argument is number of threads */
		p = atoi(argv[2]); 

	/* initialize shared data */
	int *thread_done = (int*)malloc(sizeof(int) * p);
	memset(thread_done, 0, sizeof(int) * p);
	long *thread_sum = (long*)malloc(sizeof(long) * p);
	memset(thread_sum, 0, sizeof(long) * p);

	/* sum */
	long sum = 0;
#pragma omp parallel num_threads(p)
	thread_sum_fct(n, get_element, thread_done, thread_sum, &sum);

	/* display sum */
	printf("%ld\n", sum);

	return 0;
}
