/* Make a sum of series of numbers, using p threads.
   Getting element i takes (i+1) more times than getting element 0. 

   Summing the n elements take n(n+1) in time.

   If we have 2 cores/threads, then we must devide this time between the 2 threads. If thread 1 sums in [0;a[ and thread 2 in [a;n[, then we have the equation a(a+1) = n(n+1) - a(a+1), and by eliminating powers lower than 2 we get 2a^2 = n^2, so a^2=n^2/2. 

   With 3 threads (thread a1 in [0;a1[, thread 2 in [a1;a2[, thread 3 in [a2;n[), we get the following equations : 
      a2^2 = 2 n^2 / 3
      a1^2 = a2^2 / 2 = n^2 / 3

   With p threads, we have ai^2 = i * n^2 / p 
   */

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <unistd.h>
#include <math.h>

/* Gets element i of vector */
long get_element(long i) {
	usleep(2 * (i+1));
	return i;
}

/* Thead function */
void thread_sum(long n, long(*get_fct)(long), long *global_sum) {
	int p_i = omp_get_thread_num();
	int p = omp_get_num_threads();

	/* sum my part of the vector */
	long sum = 0;
	long first_i = p_i == 0 ? 0 : sqrt((p_i) * 1.0 / p) * n;
	long last_i = p_i == p - 1 ? n : sqrt((p_i + 1) * 1.0 / p) * n;
printf("p_i=%d first_i=%ld last_i=%ld\n", p_i, first_i, last_i);
	long i;
	for (i = first_i ; i < last_i ; ++i)
		sum += (*get_fct)(i);

	/* integrate my sum with the global sum */
#	pragma omp critical
	*global_sum += sum;
}

/* MAIN */
int main(int argc, char* argv[]) {

	/* set default values */
	long n = 10;
	int p = 4;

	/* read arguments */
	if (argc >= 1) /* first argument is size of array */
		n = atol(argv[1]);
	if (argc >= 2) /* second argument is number of threads */
		p = atoi(argv[2]); 

	/* sum */
	long sum = 0;
#	pragma omp parallel num_threads(p)
	thread_sum(n, get_element, &sum);

	/* display sum */
	printf("%ld\n", sum);

	return 0;
}
