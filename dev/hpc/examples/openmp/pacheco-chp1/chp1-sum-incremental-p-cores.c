/* Make a normal sum (i.e.: non-parallel) of a series of numbers */

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <unistd.h>

/* Gets element i of vector */
long get_element(long i) {
	usleep(2 * (i+1));
	return i;
}

/* Thead function */
void thread_sum(long n, long(*get_fct)(long), long *global_sum) {
	int p_i = omp_get_thread_num();
	int p = omp_get_num_threads();

	/* sum my part of the vector */
	long sum = 0;
	long first_i = p_i * n / p;
	long last_i = p_i == p - 1 ? n : (p_i + 1) * n / p;
	long i;
	for (i = first_i ; i < last_i ; ++i)
		sum += (*get_fct)(i);

	/* integrate my sum with the global sum */
#pragma omp critical
	*global_sum += sum;
}

/* MAIN */
int main(int argc, char* argv[]) {

	/* set default values */
	long n = 10;
	int p = 4;

	/* read arguments */
	if (argc >= 1) /* first argument is size of array */
		n = atol(argv[1]);
	if (argc >= 2) /* second argument is number of threads */
		p = atoi(argv[2]); 

	/* sum */
	long sum = 0;
#pragma omp parallel num_threads(p)
	thread_sum(n, get_element, &sum);

	/* display sum */
	printf("%ld\n", sum);

	return 0;
}
