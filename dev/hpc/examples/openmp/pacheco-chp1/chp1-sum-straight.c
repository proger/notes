/* Make a normal sum (i.e.: non-parallel) of a series of numbers */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

/* Gets element i of vector */
long get_element(long i) {
	usleep(100);
	return i;
}

/* MAIN */
int main(int argc, char* argv[]) {

	/* set default values */
	long n = 10;

	/* read arguments */
	if (argc >= 1) /* first argument is size of array */
		n = atol(argv[1]);

	/* sum */
	long sum = 0;
	long i = 0;
	for (i = 0 ; i < n ; ++i)
		sum += get_element(i);

	/* display sum */
	printf("%ld\n", sum);

	return 0;
}
