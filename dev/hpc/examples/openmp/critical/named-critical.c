#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int glob_i = -1;
int glob_j = -1;

void thread() {
	int my_rank = omp_get_thread_num();
	printf("Thread %d started\n", my_rank);

#pragma omp barrier

	printf("Thread %d at first critical section.\n", my_rank);
#pragma omp critical(first)
	{
		glob_i = my_rank;
		/* all threads except thread 0 will wait until glob_j is modified. */
		if (my_rank != 0)
			while (glob_j != 0) {}
	} 
	
	/* thread 0 waits until another thread is entered inside previous critical section. */
	if (my_rank == 0)
		while (glob_i == my_rank) {}

	printf("Thread %d at second critical section.\n", my_rank);
#pragma omp critical(second)
	glob_j = my_rank;

	printf("Thread %d terminated.\n", my_rank);
}

int main(int argc, char* argv[]) {
	int thread_count = 2; /* set default value */
	if (argc > 1)
		thread_count = strtol(argv[1], NULL, 10);
#	pragma omp parallel num_threads(thread_count)
	thread();
	return 0;
}
