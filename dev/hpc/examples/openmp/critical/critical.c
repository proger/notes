#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

/* This program illustrates the fact that a critical section is unnamed critical section, and that the same unspecified name is used across all critical sections.
   Any code section protected by a critical directive will block, when entered by a thread, all other threads on all other critical sections.

   Thus it's always important to use a named critical section instead of a unnamed critical section, for protecting different part of code that aren't in concurrency.
 */

int glob_i = -1;
int glob_j = -1;

void thread() {
	int my_rank = omp_get_thread_num();
	printf("Thread %d started\n", my_rank);

#pragma omp barrier

	printf("Thread %d at first critical section.\n", my_rank);
#pragma omp critical
	{
		glob_i = my_rank;
		/* all threads except thread 0 will wait until glob_j is modified. */
		if (my_rank != 0)
			while (glob_j != 0) {}
	} 
	
	/* thread 0 waits until another thread is entered inside previous critical section. */
	if (my_rank == 0)
		while (glob_i == my_rank) {}

	printf("Thread %d at second critical section.\n", my_rank);
#pragma omp critical
	glob_j = my_rank;

	printf("Thread %d terminated.\n", my_rank);
}

int main(int argc, char* argv[]) {
	int thread_count = 2; /* set default value */
	if (argc > 1)
		thread_count = strtol(argv[1], NULL, 10);
#	pragma omp parallel num_threads(thread_count)
	thread();
	return 0;
}
