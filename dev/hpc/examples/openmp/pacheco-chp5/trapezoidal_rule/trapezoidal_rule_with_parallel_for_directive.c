#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

/* Function */
double f(double x) {
	return sin(x) * cos(x);
}

int main(int argc, char* argv[]) {
	double global_result = 0.0;
	double a, b, h; 
	int    n, i;
	int    thread_count = 10;

	if (argc > 1) 
		thread_count = strtol(argv[1], NULL, 10);
	if (argc > 4) {
		a = strtod(argv[2], NULL);
		b = strtod(argv[3], NULL);
		n = strtol(argv[4], NULL, 10);
	}

	/* Check that n is divisible by thread_count */
	if (n % thread_count != 0) {
		fprintf(stderr, "n must be evenly divisible by thread_count !\n");
		exit(1);
	}

	h = (b - a) / n;
	global_result = (f(a) + f(b)) / 2.0;
#	pragma omp parallel for num_threads(thread_count) reduction(+: global_result)
	for (i = 1 ; i<= n-1 ; ++i)
		global_result += f(a + i * h);
	global_result *= h;

	printf("With n = %d trapezoids, our estimate of the integral from %f to %f = %.14e\n", n, a, b, global_result);

	return 0;
}
