#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <omp.h>

int main(int argc, char* argv[]) {
	int     n = 20000;
	int     thread_count = 10;
	int     *a, *p;
	int     phase, i, tmp;

	if (argc > 1) 
		thread_count = strtol(argv[1], NULL, 10);
	if (argc > 1) 
		n = strtol(argv[2], NULL, 10);

	/* Check that n is divisible by thread_count */
	if (n % thread_count != 0) {
		fprintf(stderr, "n must be evenly divisible by thread_count !\n");
		exit(1);
	}

	/* create list */
	a = (int*)malloc(sizeof(int) * n);
	srand(time(NULL));
	for (i = 0, p = a; i < n ; ++i, ++p) 
		*p = rand();

	/* sort */
	/* it's a lot better to create threads first, and then use them, when the same set of threads is reused several times. */
# pragma omp parallel num_threads(thread_count) default(none) shared(a, n) private(i, tmp, phase)
	for (phase = 0 ; phase < n ; ++phase)
		if (phase % 2 == 0) {
//#           pragma omp parallel for num_threads(thread_count) default(none) shared(a, n) private(i, tmp)
# pragma omp for
			for (i = 1 ; i < n ; i += 2)
				if (a[i-1] > a[i]) {
					tmp = a[i-1];
					a[i-1] = a[i];
					a[i] = tmp;
				}
		}
		else {
//#           pragma omp parallel for num_threads(thread_count) default(none) shared(a, n) private(i, tmp)
# pragma omp for
			for (i = 1 ; i < n - 1 ; i += 2)
				if (a[i] > a[i+1]) {
					tmp = a[i+1];
					a[i+1] = a[i];
					a[i] = tmp;
				}
		}

	/* destroy list */
	free(a);

	return 0;
}
