#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(int argc, char* argv[]) {
	int     n = 20000;
	int     thread_count = 10;
	int     *a, *p;
	int     phase, i, tmp;

	if (argc > 1) 
		thread_count = strtol(argv[1], NULL, 10);
	if (argc > 1) 
		n = strtol(argv[2], NULL, 10);

	/* Check that n is divisible by thread_count */
	if (n % thread_count != 0) {
		fprintf(stderr, "n must be evenly divisible by thread_count !\n");
		exit(1);
	}

	/* create list */
	a = (int*)malloc(sizeof(int) * n);
	srand(time(NULL));
	for (i = 0, p = a; i < n ; ++i, ++p) 
		*p = rand();

	/* sort */
	for (phase = 0 ; phase < n ; ++phase)
		if (phase % 2 == 0)
			for (i = 1 ; i < n ; i += 2)
				if (a[i-1] > a[i]) {
					tmp = a[i-1];
					a[i-1] = a[i];
					a[i] = tmp;
				}
		else
			for (i = 1 ; i < n - 1 ; i += 2)
				if (a[i] > a[i+1]) {
					tmp = a[i+1];
					a[i+1] = a[i];
					a[i] = tmp;
				}

	/* destroy list */
	free(a);

	printf("Sorted a list of %d int values, using %d threads.", n, thread_count);

	return 0;
}
