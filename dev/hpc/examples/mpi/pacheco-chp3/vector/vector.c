/* An introduction to Parallel Programming, Peter S. Pacheco
   Exercice 3.9
   Multiplication of a vector by a scalar, and dot product.
 */

#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

/* READ DATA
   Read data from standard input.
   Format is :
   size of vector
   a scalar
   v1[0]	v2[0]
   v1[1]	v2[1]
   v1[2]	v2[2]
   ...
   v1[n-1]	v2[n-1]
 */
void read_data(long *n, double *scalar, double **v1, double **v2) {
	int i;
	double *p, *q;

	*n = 64;
	*scalar = 10.0;
	*v1 = (double*)malloc(sizeof(double) * *n);
	*v2 = (double*)malloc(sizeof(double) * *n);

	for (p = *v1, q = *v2, i = 0 ; i < *n ; ++i, ++p, ++q)
		*p = *q = i;
}

/* MAIN */
int main(int argc, char *argv[]) {

	long n;
	int my_rank, comm_sz;
	double *v1, *v2, scalar;
	double *subv1, *subv2;
	double local_dot_product, dot_product;
	double *p, *q;
	long i;

	/* Initialize MPI */
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

	// build vectors
	n = 0;
	v1 = v2 = 0;
	scalar = 0.0;
	if (my_rank == 0)
		read_data(&n, &scalar, &v1, &v2);
	
	// Send data to all processors
	MPI_Bcast(&n, 1, MPI_LONG, 0, MPI_COMM_WORLD);
	MPI_Bcast(&scalar, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	subv1 = (double*)malloc(sizeof(double) * n / comm_sz);
	subv2 = (double*)malloc(sizeof(double) * n / comm_sz);
	MPI_Scatter(v1, n / comm_sz, MPI_DOUBLE, subv1, n / comm_sz, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Scatter(v2, n / comm_sz, MPI_DOUBLE, subv2, n / comm_sz, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	// compute dot product
	local_dot_product = 0;
	for (p = subv1, q = subv2, i = 0 ; i < n / comm_sz ; ++i, ++p, ++q)
		local_dot_product += *p * *q;

	// compute scalar product
	for (p = subv1, i = 0 ; i < n / comm_sz ; ++i, ++p)
		*p *= scalar;

	// get result
	MPI_Gather(subv1, n / comm_sz, MPI_DOUBLE, v1, n / comm_sz, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Reduce(&local_dot_product, &dot_product, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	if (my_rank == 0) {
		printf("Dot product = %.2f\n", dot_product);
		for (p = v1, i = 0 ; i < n ; ++i, ++p)
			printf("scalar_product[%ld] = %.2f\n", i, v1[i]);
	}

	/* free memory */
	free(subv1);
	free(subv2);
	if (my_rank == 0) {
		free(v1);
		free(v2);
	}

	/* Terminate MPI */
	MPI_Finalize();

	return 0;
}
