#include <mpi.h>
#include <stdio.h>
#include <string.h>

const int MAX_STRING = 100;

int main(void) {
	char  msg[MAX_STRING];
	int   nb_processes;
	int   my_rank;
	int   q;

	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &nb_processes);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	if (my_rank != 0) {
		sprintf(msg, "Hello World from process %d of %d !\n", my_rank, nb_processes);
		MPI_Send(msg, strlen(msg) + 1, MPI_CHAR, 0, 0/*tag*/, MPI_COMM_WORLD);
	}
	else {
		printf("Hello World from process %d of %d !\n", my_rank, nb_processes);
		for (q = 1 ; q < nb_processes ; ++q) {
			MPI_Recv(msg, MAX_STRING, MPI_CHAR, q, 0/*tag*/, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			printf(msg);
		}
	}

	MPI_Finalize();

	return 0;
}
