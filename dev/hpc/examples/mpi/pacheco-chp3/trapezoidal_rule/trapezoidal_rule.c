#include <mpi.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

/* GET ARGUMENTS */
void get_args(int argc, char *argv[], int *n) {
	int c;

	while ((c = getopt(argc, argv, "n:")) != -1) {
		switch (c) {
			case 'n':
				*n = atoi(optarg);
				break;
		}
	}
}

/* Function to integrate */
double f(double x) {
	return 1.0 / x;
}

/* Trapezoidal rule */
double trap(double a, double b, int n, double h) {
	double s;
	int i;

	s = (f(a) + f(b)) / 2.0;
	for (i = 1 ; i < n ; ++i)
		s += f(a + i * h);
	s *= h;

	return s;
}

/* MAIN */
int main(int argc, char *argv[]) {

	int n = 10;
	int my_rank, comm_sz;
	double a = 1.0, b = 10.0;
	int my_n;
	double h, my_a, my_b, my_s, total_s;
	int source;

	/* read arguments */
	get_args(argc, argv, &n);

	/* Initialize MPI */
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

	/* Compute my part of the surface */
	h = (b - a) / n;
	my_n = n / comm_sz;
	my_a = a + my_rank * my_n * h;
	/* In case n is not dividible by comm_sz, we put more intervals to compute on last processor.
TODO: This is not a good solution, since the last processor get a lot more work to do. It would be better to scatter the work among several processors. 
	 */
	if (comm_sz > 1 && my_rank == comm_sz - 1)
		my_s = trap(my_a, b, n - my_n * (comm_sz - 1), h);
	else {
		my_b = my_a + my_n * h;
		my_s = trap(my_a, my_b, my_n, h);
	}

	MPI_Reduce(&my_s, &total_s, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	if (my_rank == 0) {
		double real_s = log(b) - log(a);
		printf("With n = %d trapezoids, our estimate of the integral from %.2f to %.2f = %.15e\nReal value is %.15e and error is %.15e\n", n, a, b, total_s, real_s, real_s - total_s);
	}

	/* Terminate MPI */
	MPI_Finalize();

	return 0;
}
