/* An introduction to Parallel Programming, Peter S. Pacheco
   Program Assignments 3.2
   Compute the number PI using a Monte Carlo method.
 */

#include <mpi.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

/* GET ARGUMENTS */
void get_args(int argc, char *argv[], long long *n) {
	int c;

	while ((c = getopt(argc, argv, "n:")) != -1) {
		switch (c) {
			case 'n':
				*n = atoll(optarg);
				break;
		}
	}
}

/* MAIN */
int main(int argc, char *argv[]) {

	long long n = 10;
	int my_rank, comm_sz;
	double x, y;
	long long i, nb_local_hits, nb_hits;
	long double pi;

	/* read arguments */
	get_args(argc, argv, &n);

	/* Initialize MPI */
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

	/* Send work to all processors */
	MPI_Bcast(&n, 1, MPI_LONG_LONG, 0, MPI_COMM_WORLD);

	/* compute number of hits */
	srand(time(NULL));
	nb_local_hits = 0;
	for (i = 0 ; i < n / comm_sz ; ++i) {
		x = (rand() / (double)RAND_MAX) * 2.0 - 1.0;
		y = (rand() / (double)RAND_MAX) * 2.0 - 1.0;
		if (x*x+y*y <= 1.0)
			++nb_local_hits;
	}
	MPI_Reduce(&nb_local_hits, &nb_hits, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

	/* compute PI */
	if (my_rank == 0) {
		pi = 4 * nb_hits / (long double)n;
		printf("PI = %.20Lf\n", pi);
	}

	/* Terminate MPI */
	MPI_Finalize();

	return 0;
}
