! Copyright 2003-2012 Intel Corporation. All Rights Reserved.
!
! The source code contained or described herein and all documents related 
! to the source code ("Material") are owned by Intel Corporation or its 
! suppliers or licensors.  Title to the Material remains with Intel Corporation 
! or its suppliers and licensors.  The Material is protected by worldwide 
! copyright and trade secret laws and treaty provisions.  No part of the 
! Material may be used, copied, reproduced, modified, published, uploaded, 
! posted, transmitted, distributed, or disclosed in any way without Intel's 
! prior express written permission.
!
! No license under any patent, copyright, trade secret or other intellectual
! property right is granted to or conferred upon you by disclosure or delivery
! of the Materials, either expressly, by implication, inducement, estoppel 
! or otherwise.  Any license under such intellectual property rights must
! be express and approved by Intel in writing.


#define ISIZE 1024*1024*64
#define PHI_DEV 0
#define ALPHA 1.3
#define ITERS 10
#define NUM_THREADS 240

!#define ALLOC alloc_if(.true.)
!#define FREE free_if(.true.)
!#define REUSE alloc_if(.false.)
!#define RETAIN free_if(.false.)

#define VALIDATE .true.

module data_arrays
  real(kind=4), allocatable, dimension(:) :: ina0, inb0, out0, ina1, inb1, out1
  !dir$ attributes offload:mic ::  ina0, inb0, out0, ina1, inb1, out1
  real(kind=4), allocatable, dimension(:) :: ref0,ref1
end module data_arrays

subroutine do_async()
  use data_arrays
  use omp_lib

  integer a1, x0

  ! Send array 0 to the coprocessor
  !dir$ offload_transfer target(mic:PHI_DEV)&
    & in(ina0:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & in(inb0:length(ISIZE) alloc_if(.false.) free_if(.false.))

  ! Start the asynchronous transfer for array 1 and send signal "a1"
  ! when complete
  !dir$ offload_transfer target(mic:PHI_DEV)&
    & in(ina1:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & in(inb1:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & signal(a1)

  ! Start simultaneous compute while the transfer for array 1
  ! is occuring
  !dir$ offload begin target(mic:PHI_DEV)&
    & nocopy(ina0:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & nocopy(inb0:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & nocopy(out0:length(ISIZE) alloc_if(.false.) free_if(.false.))
    ! Perform SAXPY operation
    !$omp parallel do 
    do i=1,ISIZE
      out0(i)=ALPHA*ina0(i)+inb0(i)
    end do
  !dir$ end offload

  ! Start the asynchronous transfer for out0
  !dir$ offload_transfer target(mic:PHI_DEV)&
    & out(out0:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & signal(x0)

  ! Start simultaneous compute while the transfer for out0 is occuring
  ! once you receive the signal "a1" and transfer out1 back to host when done
  !dir$ offload begin target(mic:PHI_DEV)&
    & nocopy(ina1:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & nocopy(inb1:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & out(out1:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & wait(a1)
    ! Perform SAXPY operation
    !$omp parallel do 
      do i=1,ISIZE
        out1(i)=ALPHA*ina1(i)+inb1(i)
      end do
  !dir$ end offload
end subroutine do_async

subroutine do_offload()
  use data_arrays
  use omp_lib

  ! Offload 1
  !dir$ offload begin target(mic:PHI_DEV)&
    & in(ina0:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & in(inb0:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & out(out0:length(ISIZE) alloc_if(.false.) free_if(.false.))
    !$omp parallel do 
    do i=1,ISIZE
      out0(i)=ALPHA*ina0(i)+inb0(i)
    end do
  !dir$ end offload

  ! Offload 2
  !dir$ offload begin target(mic:PHI_DEV)&
    & in(ina1:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & in(inb1:length(ISIZE) alloc_if(.false.) free_if(.false.))&
    & out(out1:length(ISIZE) alloc_if(.false.) free_if(.false.))
    ! Perform SAXPY operation
    !$omp parallel do 
    do i=1,ISIZE
      out1(i)=ALPHA*ina1(i)+inb1(i)
    end do
  !dir$ end offload
end subroutine do_offload

program async_start
  use data_arrays
  use omp_lib
  use ifport

  real(kind=4) :: start
  real(kind=8) :: runtime

  ! Allocate arrays on host
  allocate(ina0(ISIZE), inb0(ISIZE), ina1(ISIZE), inb1(ISIZE), STAT=istat)
  allocate(out0(ISIZE),out1(ISIZE), STAT=istat)
  allocate(ref0(ISIZE),ref1(ISIZE), STAT=istat)

  ! Initialize host arrays
  call random_seed()
  call random_number(ina0)
  call random_number(inb0)
  out0 = 0.0
  call random_number(ina1)
  call random_number(inb1)
  out1 = 0.0
  ina0 = ina0*100
  inb0 = inb0*100
  ina1 = ina1*100
  inb1 = inb1*100
  ref0 = ALPHA*ina0 + inb0
  ref1 = ALPHA*ina1 + inb1

  ! Allocate the arrays on coporcessor
  !dir$ offload begin target(mic:PHI_DEV)&
   & nocopy(ina0:length(ISIZE) alloc_if(.true.) free_if(.false.))&
   & nocopy(inb0:length(ISIZE) alloc_if(.true.) free_if(.false.))&
   & nocopy(out0:length(ISIZE) alloc_if(.true.) free_if(.false.))&
   & nocopy(ina1:length(ISIZE) alloc_if(.true.) free_if(.false.))&
   & nocopy(inb1:length(ISIZE) alloc_if(.true.) free_if(.false.))&
   & nocopy(out1:length(ISIZE) alloc_if(.true.) free_if(.false.))
   ! Set the number of threads on the coprocessor
   call omp_set_num_threads(NUM_THREADS)
  !dir$ end offload


  start=SECNDS(0.0)
  do i=1,ITERS
    call do_offload()
  end do
  runtime = SECNDS(start)
  print *,"Offload Runtime: ",runtime/ITERS
  ! Validate the output
  if (VALIDATE) then
   do i=1,ISIZE
    if(abs(ref0(i)-out0(i))>1e-3) then
      print *,"ERROR:",i
      print *, ref0(i) , out0(i)
      exit
    endif
   end do
  end if

  start=SECNDS(0.0)
  do i=1,ITERS
    call do_async()
  end do
  runtime=SECNDS(start)
  print  *,"Async Runtime: ",runtime/ITERS

  ! Validate the output
  if (VALIDATE) then
   do i=1,ISIZE
    if(abs(ref0(i)-out0(i))>1e-3) then
      print *,"ERROR:",i
      print *, ref0(i) , out0(i)
      exit
    end if
   end do
  end if

  ! Free the arrays on the coprocessor
  !dir$ offload begin target(mic:PHI_DEV)&
    & nocopy(ina0:length(ISIZE) alloc_if(.false.) free_if(.true.))&
    & nocopy(inb0:length(ISIZE) alloc_if(.false.) free_if(.true.))&
    & nocopy(out0:length(ISIZE) alloc_if(.false.) free_if(.true.))&
    & nocopy(ina1:length(ISIZE) alloc_if(.false.) free_if(.true.))&
    & nocopy(inb1:length(ISIZE) alloc_if(.false.) free_if(.true.))&
    & nocopy(out1:length(ISIZE) alloc_if(.false.) free_if(.true.))
  !dir$ end offload 

end program async_start
