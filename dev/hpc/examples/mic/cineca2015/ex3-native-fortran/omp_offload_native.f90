!
! Copyright 2003-2012 Intel Corporation. All Rights Reserved.
!
! The source code contained or described herein and all documents related 
! to the source code ("Material") are owned by Intel Corporation or its 
! suppliers or licensors.  Title to the Material remains with Intel Corporation 
! or its suppliers and licensors.  The Material is protected by worldwide 
! copyright and trade secret laws and treaty provisions.  No part of the 
! Material may be used, copied, reproduced, modified, published, uploaded, 
! posted, transmitted, distributed, or disclosed in any way without Intel's 
! prior express written permission.
!
! No license under any patent, copyright, trade secret or other intellectual
! property right is granted to or conferred upon you by disclosure or delivery
! of the Materials, either expressly, by implication, inducement, estoppel 
! or otherwise.  Any license under such intellectual property rights must
! be express and approved by Intel in writing.
!
!******************************************************************************
! Content:
!      Matrix multiplication sample code - for native Intel(R) Xeon Phi(TM)
!      coprocessor execution.
!      
!******************************************************************************

#define sz_align 64

include 'timer.f90'

program mat_mul_native

  use timer

  implicit none
  integer :: n               ! Matrix dimensions
  real, allocatable, dimension(:) :: a, b, c

  integer :: length, istatus
  integer :: iteration = 1
  character(len=20) :: argument
  real :: secs = 0.0 
  real :: transfer_time = 0.0 
  integer :: i,j

  ! integer sz_align             ! alignment size

  ! Check command line arguments
  !if (command_argument_count() /= 2) then
  ! The allocate routine does not accept an alignment argument
  ! so we are using the compiler directive. To change alignment
  ! code must be changed and recompiled
  if (command_argument_count() /= 1) then
    call get_command_argument(0, argument)
  !  print *, "Usage: ", argument, " <N> <sz_align>"
    print *, "Usage: ", argument, " <N> "
    stop
  endif

  ! Parse command line arguments 
  call get_command_argument(1, argument, length, istatus)
  if(istatus/=0) then
    print *, "Unable to read array length"
    stop
  endif
  read (argument, '(I)'), n
  if (n <= 0) then
    print *, "Invalid matrix size"
    stop
  end if

  !call get_command_argument(2, argument, length, istatus)
  !if(istatus/=0) then
  !  print *, "Unable to read alignment size"
  !  stop
  !endif
  !read (argument, '(I)'), sz_align
  !if (n <= 0) then
  !  print *, "Invalid matrix size"
  !  stop
  !end if

  ! Change this value to change alignment
  !dir$ attributes align : sz_align :: a,b,c
  ! Allocate the matrices
  allocate( a(n*n), stat=istatus )
  if(istatus /= 0) then
    print *, "Could not allocate matrix A"
    stop
  end if
  allocate( b(n*n), stat=istatus )
  if(istatus /= 0) then
    print *, "Could not allocate matrix B"
    deallocate(a)
    stop
  end if
  allocate( c(n*n), stat=istatus )
  if(istatus /= 0) then
    print *, "Could not allocate matrix C"
    deallocate(a,b)
    stop
  end if

  ! Initialize the matrices
  a = 1.0
  b = 2.0
  c = 0.0

  do i = 1,iteration
    ! re-initialize the output matrix
    c = 0.0

    call start_timing()

    ! call the compute matrix mul function
    call mmul(a,n,b,n,c,n,n)

    secs = secs + end_timing()
  end do

  print *, "avg time : ", secs/iteration, " sec"
  ! Display the result
  !do i = 1, n
  !   print *, (C(j + (i-1) * n),j=1,n)
  !end do 
  print *, "C(1)=", c(1)

  ! Free the matrices 
  deallocate(a,b,c)

end program mat_mul_native

subroutine mmul(a, lda, b, ldb, c, ldc, n)
  use omp_lib
  integer :: lda, ldb, ldc, n
  real, dimension(n*n) :: a,b,c
  integer :: i, j, k
  integer iMaxThreads

  iMaxThreads = omp_get_max_threads()

  print *, "matrix multiplication running with ", iMaxThreads," threads"

  !$omp parallel do 
  do i = 1, n
    do j = 1, n
      do k = 1, n
        c((j-1)*ldc+i) = c((j-1)*ldc+i) + a((k-1)*lda+i) * b((j-1)*ldb+k)
      end do
    end do
  end do
  !$omp end parallel do
end subroutine mmul

