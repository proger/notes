// Copyright 2003-2012 Intel Corporation. All Rights Reserved.
// 
// The source code contained or described herein and all documents related 
// to the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors.  Title to the Material remains with Intel Corporation
// or its suppliers and licensors.  The Material is protected by worldwide
// copyright and trade secret laws and treaty provisions.  No part of the
// Material may be used, copied, reproduced, modified, published, uploaded,
// posted, transmitted, distributed, or disclosed in any way without Intel's
// prior express written permission.
// 
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or delivery
// of the Materials, either expressly, by implication, inducement, estoppel
// or otherwise.  Any license under such intellectual property rights must
// be express and approved by Intel in writing.



#pragma offload_attribute(push,target(mic))
#include <stdio.h>
#include <math.h>
#include <mkl.h>
#include <mkl_vsl.h>
#include <omp.h>
#include "timer.h"
#pragma offload_attribute(pop)

#define max(a, b) (((a)>(b))?(a):(b))
#define TIMESTEPS 1000
#define PATHS 1000000
#define SEED 1

__attribute__((target(mic))) void offload_check(void)
{
#ifdef __MIC__
  printf("Code running on MIC\n");
#else
  printf("Code running on host\n");
#endif
}

/* program to solve Monte Carlo problem */
main(int argc, char **argv)
{
  float secs = 0.0f;
  double             T, r, sigma, rho, alpha, dt, con1, con2, xmu, var;
  double             payoff, sum0, sum1, ave, sdv;
  int                eTime, sTime;
  int                k, paths, timeSteps, mm;
  int                seed = SEED;
  int                iMaxThreads, iNumThreads;
  int                doOffload = 1;

  Timer myTimer; /*timer*/
  myTimer.start();

  paths = PATHS;
  timeSteps = TIMESTEPS; 

  if (argc > 1)
    doOffload = atoi(argv[1]);

  printf("Offload flag = %d\n", doOffload);
    

#pragma offload target(mic) if (1==doOffload)
{
  VSLStreamStatePtr  stream0, stream1;
  VSLStreamStatePtr  *stream0Thread, *stream1Thread;

  offload_check();

  iMaxThreads = omp_get_max_threads();
  iNumThreads = iMaxThreads;

  printf("Monte Carlo simulation of %d paths with %d timesteps\n", paths, timeSteps);
  printf("   using %d threads\n", iMaxThreads);

  /* set various constants */
  T     = 1.0;
  r     = 0.05;
  sigma = 0.1;
  rho   = 0.5;
  alpha = sqrt(1.0-rho*rho);

  dt    = 1.0/timeSteps;

  con1  = 1.0 + r*dt;
  con2  = sqrt(dt)*sigma;

  xmu = 0.0;
  var = 1.0;
  payoff = 0.951229424500714; // exp(-r*T);

  /* initialise VSL random number generator */
  vslNewStream(&stream0, VSL_BRNG_MCG59, seed);
  vslCopyStream(&stream1, stream0);  // Create a second instance of the same randome stream
  vslLeapfrogStream( stream0, 0, 2); // The master stream will have 2 partitions.  0 takes
  vslLeapfrogStream( stream1, 1, 2); // the first, 1 the second

  // Now give each thread its view of this stream and offset them properly
  stream0Thread = (VSLStreamStatePtr *)malloc(iNumThreads * sizeof(VSLStreamStatePtr));
  stream1Thread = (VSLStreamStatePtr *)malloc(iNumThreads * sizeof(VSLStreamStatePtr));

  for ( k = 0; k < iNumThreads; k++)
  {
     vslCopyStream( &(stream0Thread[k]), stream0);
     vslCopyStream( &(stream1Thread[k]), stream1);
     vslSkipAheadStream(stream0, timeSteps);
     vslSkipAheadStream(stream1, timeSteps);
  }

  /* loop over paths */
  sum0 = 0.0;
  sum1 = 0.0;

#pragma omp parallel for reduction(+: sum0) reduction(+: sum1)
  for (mm=0; mm<paths; mm++) 
  {
    int nn;
    double S1, S2;
    double Y1, Y2;
    double rvec0[TIMESTEPS], rvec1[TIMESTEPS];
    int iThreadNum = omp_get_thread_num();

    /* calculate random vector */
    vdRngGaussian(VSL_METHOD_DGAUSSIAN_BOXMULLER2,
                  stream0Thread[iThreadNum], timeSteps, rvec0, xmu, var);
    vdRngGaussian(VSL_METHOD_DGAUSSIAN_BOXMULLER2,
                  stream1Thread[iThreadNum], timeSteps, rvec1, xmu, var);

    /* calculate path  */
    S1 = 1.0;
    S2 = 1.0;

    for (nn=0; nn<timeSteps; nn++) 
    {
      Y1 = rvec0[nn];
      Y2 = rho*Y1 + alpha*rvec1[nn];

      S1 = S1*(con1 + con2*Y1);
      S2 = S2*(con1 + con2*Y2);
    }

    if ( fabs(S1-1.0)<0.1 && fabs(S2-1.0)<0.1 ) 
    {
      sum0 = sum0 + payoff;
      sum1 = sum1 + payoff*payoff;
    }

    vslSkipAheadStream(stream0Thread[iThreadNum], (iNumThreads-1)*timeSteps);
    vslSkipAheadStream(stream1Thread[iThreadNum], (iNumThreads-1)*timeSteps);

  }


  /* calculate average and standard deviation */
  ave = sum0/paths;
  sdv = sqrt((sum1/paths - ave*ave)/paths);
  vslDeleteStream(&stream0);
  vslDeleteStream(&stream1);
}  // offload
  secs = myTimer.end();
  printf("Time Elapsed: %f secs \n", secs);
  printf("Average %f, Standard deviation of error %f\n", ave, sdv);
}
