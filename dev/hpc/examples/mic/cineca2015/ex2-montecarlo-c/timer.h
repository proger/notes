// Copyright 2003-2012 Intel Corporation. All Rights Reserved.
// 
// The source code contained or described herein and all documents related 
// to the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors.  Title to the Material remains with Intel Corporation
// or its suppliers and licensors.  The Material is protected by worldwide
// copyright and trade secret laws and treaty provisions.  No part of the
// Material may be used, copied, reproduced, modified, published, uploaded,
// posted, transmitted, distributed, or disclosed in any way without Intel's
// prior express written permission.
// 
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or delivery
// of the Materials, either expressly, by implication, inducement, estoppel
// or otherwise.  Any license under such intellectual property rights must
// be express and approved by Intel in writing.



#ifndef _TIMER_H
#define _TIMER_H

#include  <sys/time.h>

struct Timer
{
	float compute_time;
	float transfer_time;

	void start()
	{
		gettimeofday(&startTv, NULL);
	}

	float end()
	{
		gettimeofday(&endTv, NULL);
		return secs();
	}

	float secs()
	{
		return (float) (endTv.tv_sec - startTv.tv_sec) 
			+ (float) (endTv.tv_usec - startTv.tv_usec) * 1e-6;
	}

	struct timeval startTv,endTv;
};

#endif

