!
! Copyright 2003-2012 Intel Corporation. All Rights Reserved.
!
! The source code contained or described herein and all documents related 
! to the source code ("Material") are owned by Intel Corporation or its 
! suppliers or licensors.  Title to the Material remains with Intel Corporation 
! or its suppliers and licensors.  The Material is protected by worldwide 
! copyright and trade secret laws and treaty provisions.  No part of the 
! Material may be used, copied, reproduced, modified, published, uploaded, 
! posted, transmitted, distributed, or disclosed in any way without Intel's 
! prior express written permission.
!
! No license under any patent, copyright, trade secret or other intellectual
! property right is granted to or conferred upon you by disclosure or delivery
! of the Materials, either expressly, by implication, inducement, estoppel 
! or otherwise.  Any license under such intellectual property rights must
! be express and approved by Intel in writing.
!
!******************************************************************************
! Content:
!      Timer routines to be used with sample codes.
!      
!*****************************************************************************/

module timer

  integer (kind=8) :: start_time,end_time
  real (kind=8) :: clocks_per_second
  integer (kind=8) :: max_clocks_start,max_clocks_end

contains

  subroutine start_timing()
    call system_clock(start_time, clocks_per_second, max_clocks_start)
  end subroutine start_timing

  function end_timing()
    real (kind=8) end_timing
    call system_clock(end_time, clocks_per_second, max_clocks_end)
    end_timing = dble(end_time - start_time) / clocks_per_second 
  end function end_timing

end module timer
