!
! Copyright 2003-2012 Intel Corporation. All Rights Reserved.
!
! The source code contained or described herein and all documents related 
! to the source code ("Material") are owned by Intel Corporation or its 
! suppliers or licensors.  Title to the Material remains with Intel Corporation 
! or its suppliers and licensors.  The Material is protected by worldwide 
! copyright and trade secret laws and treaty provisions.  No part of the 
! Material may be used, copied, reproduced, modified, published, uploaded, 
! posted, transmitted, distributed, or disclosed in any way without Intel's 
! prior express written permission.
!
! No license under any patent, copyright, trade secret or other intellectual
! property right is granted to or conferred upon you by disclosure or delivery
! of the Materials, either expressly, by implication, inducement, estoppel 
! or otherwise.  Any license under such intellectual property rights must
! be express and approved by Intel in writing.
!
!******************************************************************************
! Content:
!      Sample code to estimate PI using Monte Carlo method - offload solution.
!      
!******************************************************************************

#include "timer.f90"

! program to solve Monte Carlo problem 
program mCarlo
  use omp_lib
  use timer

  implicit none
  real (kind=8) :: secs = 0.0
  logical :: doOffload = .true.
  character (len=20) :: argument
  integer :: length, istatus
  real (kind=8) :: ave, sdv

  call start_timing()

  ! Check command line arguments 
  if (command_argument_count() > 0) then
    call get_command_argument(1, argument, length, istatus)
    if(istatus/=0) then
      print *, "Unable to read offload flag"
      stop
    end if
    read (argument, "(L)",iostat=istatus), doOffload
    if (istatus /= 0) then
      print *, "Invalid offload flag"
      stop
    end if
  end if

  print *,"Offload flag = ", doOffload
    
! offload block starts here; in the C++ version this is done with a block
! statement; in the Fortran, we are using a subroutine to make it easier
! to set up all vector streams local to the coprocessor
  !dir$ attributes offload:mic :: do_calculation
  !dir$ offload target(mic) if (doOffload)
  call do_calculation(ave, sdv)
  ! because we are offloading only a single subroutine call, we do not use
  ! "offload begin" and "end offload"
! offload block ends here
  secs = end_timing()
  print *,"Time Elapsed: ",secs," secs"
  print *,"Average ",ave," Standard deviation of error ",sdv
end program mCarlo

!dir$ attributes offload : mic :: do_calculation
subroutine do_calculation(ave, sdv)

  use omp_lib
!dir$ options/offload_attribute_target=mic
  include 'mkl.fi'
!dir$ end options

  integer, parameter ::SEED = 1
  integer (kind=8), parameter :: TIMESTEPS = 1000
  integer, parameter :: PATHS = 1000000

  real (kind=8) :: T, r, sigma, rho, alpha, dt, con1, con2, xmu, var
  real (kind=8) :: payoff, sum0, sum1, ave, sdv
  integer :: k, mm, nn
  integer :: iMaxThreads, iNumThreads, iThreadNum
  real (kind=8) :: S1, S2
  real (kind=8) :: Y1, Y2
  real (kind=8), allocatable :: rvec0(:), rvec1(:)

  type(VSL_STREAM_STATE)  :: stream0, stream1
  type(VSL_STREAM_STATE), allocatable ::  stream0Thread(:), stream1Thread(:)

!dir$ attributes offload :mic :: offload_check
  call offload_check()

  iMaxThreads = omp_get_max_threads()
  iNumThreads = iMaxThreads

  print '("Monte Carlo simulation of ", I10," paths with ",I10, " timesteps")',&
    PATHS,TIMESTEPS 
  print *,"   using ",iMaxThreads," threads"

  ! set various constants 
  T     = 1.0D0
  r     = 0.05D0
  sigma = 0.1D0
  rho   = 0.5D0
  alpha = sqrt(1.0D0-rho*rho)

  dt    = 1.0/timeSteps

  con1  = 1.0 + r*dt
  con2  = sqrt(dt)*sigma

  xmu = 0.0
  var = 1.0
  payoff = 0.951229424500714 ! exp(-r*T)

  ! initialise VSL random number generator 
  istatus = vslnewstream(stream0, VSL_BRNG_MCG59, SEED)
  ! Create a second instance of the same random stream
  istatus = vslcopystream(stream1, stream0)  
  ! The master stream will have 2 partitions. 0 takes the first, 1 the second
  istatus = vslleapfrogstream( stream0, 0, 2) 
  istatus = vslleapfrogstream( stream1, 1, 2) 

  ! Now give each thread its view of this stream and offset them properly
  allocate(stream0Thread(iNumThreads))
  allocate(stream1Thread(iNumThreads))

  do k = 1, iNumThreads
     istatus=vslCopyStream( stream0Thread(k), stream0)
     istatus=vslCopyStream( stream1Thread(k), stream1)
     istatus=vslSkipAheadStream(stream0, timeSteps)
     istatus=vslSkipAheadStream(stream1, timeSteps)
  end do

  ! loop over paths 
  sum0 = 0.0
  sum1 = 0.0

  !$omp parallel do reduction(+: sum0) reduction(+: sum1) &
     private( nn, S1, S2, Y1, Y2, rvec0, rvec1, iThreadNum)
  do mm=1, PATHS 
    allocate( rvec0(TIMESTEPS), rvec1(TIMESTEPS))
    ! since threads are numbered from 0, add 1 (it's cheap)
    iThreadNum = omp_get_thread_num() +1

    ! calculate random vector 
    istatus = vdrnggaussian(VSL_METHOD_DGAUSSIAN_BOXMULLER2, &
                  stream0Thread(iThreadNum), timeSteps, rvec0, xmu, var)
    istatus = vdrnggaussian(VSL_METHOD_DGAUSSIAN_BOXMULLER2, &
                  stream1Thread(iThreadNum), timeSteps, rvec1, xmu, var)

    ! calculate path 
    S1 = 1.0
    S2 = 1.0

    do nn=1, TIMESTEPS 
      Y1 = rvec0(nn)
      Y2 = rho*Y1 + alpha*rvec1(nn)

      S1 = S1*(con1 + con2*Y1)
      S2 = S2*(con1 + con2*Y2)
    end do

    if ( abs(S1-1.0D0)<0.1D0 .and. abs(S2-1.0D0)<0.1D0 ) then
      sum0 = sum0 + payoff
      sum1 = sum1 + payoff*payoff
    end if

    istatus = vslskipaheadstream(stream0Thread(iThreadNum), (iNumThreads-1)*TIMESTEPS)
    istatus = vslskipaheadstream(stream1Thread(iThreadNum), (iNumThreads-1)*TIMESTEPS)

    deallocate( rvec0, rvec1)
  end do

  ! calculate average and standard deviation
  ave = sum0/PATHS
  sdv = sqrt((sum1/PATHS - ave*ave)/PATHS)
  deallocate(stream0Thread)
  deallocate(stream1Thread)
  istatus = vsldeletestream(stream0)
  istatus = vsldeletestream(stream1)
end subroutine do_calculation

!dir$ attributes offload :mic :: offload_check
subroutine offload_check()
#ifdef __MIC__
  print *,"Code running on MIC"
#else
  print *,"Code running on host"
#endif
end subroutine offload_check

