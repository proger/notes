// Copyright 2003-2012 Intel Corporation. All Rights Reserved.
// 
// The source code contained or described herein and all documents related 
// to the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors.  Title to the Material remains with Intel Corporation
// or its suppliers and licensors.  The Material is protected by worldwide
// copyright and trade secret laws and treaty provisions.  No part of the
// Material may be used, copied, reproduced, modified, published, uploaded,
// posted, transmitted, distributed, or disclosed in any way without Intel's
// prior express written permission.
// 
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or delivery
// of the Materials, either expressly, by implication, inducement, estoppel
// or otherwise.  Any license under such intellectual property rights must
// be express and approved by Intel in writing.



/* System headers */
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdint.h>
#include <vector>
#include "malloc.h"
#include "timer.h"
#include "omp.h"

#define iteration 1

using namespace std;

void mmul(float *,int ,float *,int ,float *,int ,int);

float *A = NULL;
float *B = NULL;
float *C = NULL;


void mmul(float *a, int lda, float *b, int ldb, float *c, int ldc, int n)
{

        int iMaxThreads;
	iMaxThreads = omp_get_max_threads();

	printf("matrix multiplication running with %d threads\n", iMaxThreads);

	#pragma omp parallel for 
	for (int j = 0; j < n; ++j){
		for (int k = 0; k < n; ++k){
			for (int i = 0; i < n; ++i){
				c[j * ldc + i] += a[k * lda + i] * b[j * ldb + k];
			}
		}
	}

}

int main(int argc, char **argv)
{

	int N; /* Matrix dimensions */
	int sz_align; /*alignment size*/
	int matrix_bytes; /* Matrix size in bytes */
	int matrix_elements; /* Matrix size in elements */
	Timer myTimer;

	/* Check command line arguments */
	if (argc != 3) {
		printf("\nUsage: %s <N> <sz_align>\n\n", argv[0]);
		return -1;
	}

	/* Parse command line arguments */
	N = atoi(argv[1]);
	if (N <= 0) {
		printf("Invalid matrix size\n");
		return -1;
	}

	sz_align = atoi(argv[2]);
	
	matrix_elements = N * N;
	matrix_bytes = sizeof(float) * matrix_elements;

	/* Allocate the matrices */
	A = (float *)_mm_malloc(matrix_bytes,sz_align);
	if (A == NULL) {
		printf("Could not allocate matrix A\n");
		return -1;
	}

	B = (float *)_mm_malloc(matrix_bytes,sz_align);
	if (B == NULL) {
		printf("Could not allocate matrix B\n");
		return -1;
	}

	C = (float *)_mm_malloc(matrix_bytes,sz_align);
	if (C == NULL) {
		printf("Could not allocate matrix C\n");
		return -1;
	}

	/* Initialize the matrices */
	#pragma novector
	for (int i = 0; i < matrix_elements; i++) {
		A[i] = 1.0; B[i] = 2.0; C[i] = 0.0;
	}

	float secs = 0.0f;
	float transfer_time = 0.0f;


	for(int i = 0; i<iteration; i++){
		/*re-initialize the output marrix	*/
		#pragma novector
		for (int i = 0; i < matrix_elements; i++) {
                	C[i] = 0.0;
        	}
		
		myTimer.start();	
			
		/*call the compute matrix mul function*/
		mmul(A,N,B,N,C,N,N);
	
		secs += myTimer.end();
	}
	
	fprintf(stderr, "\tavg time : %f s \n", secs/iteration);
	/* Display the result */
	/*for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++)
			printf("%7.3f ", C[j + i * N]);
		printf("\n");
	}*/
	/* Free the matrices */
	_mm_free(A); _mm_free(B); _mm_free(C);

    return 0;
}

