!
! Copyright 2003-2012 Intel Corporation. All Rights Reserved.
!
! The source code contained or described herein and all documents related 
! to the source code ("Material") are owned by Intel Corporation or its 
! suppliers or licensors.  Title to the Material remains with Intel Corporation 
! or its suppliers and licensors.  The Material is protected by worldwide 
! copyright and trade secret laws and treaty provisions.  No part of the 
! Material may be used, copied, reproduced, modified, published, uploaded, 
! posted, transmitted, distributed, or disclosed in any way without Intel's 
! prior express written permission.
!
! No license under any patent, copyright, trade secret or other intellectual
! property right is granted to or conferred upon you by disclosure or delivery
! of the Materials, either expressly, by implication, inducement, estoppel 
! or otherwise.  Any license under such intellectual property rights must
! be express and approved by Intel in writing.
!
!******************************************************************************
! Content:
!      Matrix multiplication sample code - data persistence.
!      
!******************************************************************************


#define RETAIN free_if(.false.)
#define REUSE alloc_if(.false.)
#define ALLOC alloc_if(.true.)
#define FREE free_if(.true.)

include 'timer.f90'

module array_holder
 !dir$ attributes offload:mic:: a,b,c
 real, allocatable, dimension(:) :: a, b, c
end module array_holder

program mat_mul
  use timer
  use array_holder

  implicit none
  integer :: n ! Matrix dimensions
  integer :: length, istatus
  integer :: iteration = 1
  integer :: i,j
  character (len=20) :: argument
  real :: secs, transfer_time
  integer :: nthreads

  ! Check command line arguments
  if (command_argument_count() /= 1) then
    call get_command_argument(0, argument)
    print *, "Usage: ", argument, " <N> "
    stop
  end if
  ! Parse command line arguments 
  call get_command_argument(1, argument, length, istatus)
  if(istatus/=0) then
    print *, "Unable to read array length"
    stop
  end if
  read (argument, "(I)"), n
  if (n <= 0) then
    print *, "Invalid matrix size"
    stop
  end if

  ! Allocate the matrices
  allocate( a(n*n), stat=istatus )
  if(istatus /= 0) then
    print *, "Could not allocate matrix A"
    stop
  end if
  allocate( b(n*n), stat=istatus )
  if(istatus /= 0) then
    print *, "Could not allocate matrix B"
    deallocate(a)
    stop
  end if
  allocate( c(n*n), stat=istatus )
  if(istatus /= 0) then
    print *, "Could not allocate matrix C"
    deallocate(a,b)
    stop
  end if
  ! Initialize the matrices
  a = 1.0
  b = 2.0
  c = 0.0

  secs = 0.0
  transfer_time = 0.0


  do i = 1, iteration
    c = 0.0

    ! call the compute matrix mul function
    call mmul(a,n,b,n,c,n,n)

  end do

  ! print *, "avg time : ", secs/iteration, " sec"
  ! Display the result
  !do i = 1, n
  !  print *, (C(j + (i-1) * n),j=1,n)
  !end do 
  print *, "C(1)=",c(1)

  ! Free the matrices 
  deallocate(A,stat=istatus)
  deallocate(B,stat=istatus)
  deallocate(C,stat=istatus)
end program mat_mul

!dir$ attributes offload : mic :: offload_check
subroutine offload_check()
#ifdef __MIC__
  print *,"Code running on MIC"
#else
  print *,"Code running on host"
#endif
end subroutine offload_check

subroutine mmul(a, lda, b, ldb, c, ldc, n)
  use omp_lib

  integer :: lda, ldb, ldc, n
  real, dimension(n*n) :: a,b,c
  integer :: i, j, k
  integer iMaxThreads

  ! transfer data to the card and let it persist using free_if(0) specifier
  !dir$ offload begin target(mic:0) in(a:length(n*n) RETAIN) &
    & in(b:length(n*n) RETAIN) in(c:length(n*n) RETAIN)
  !dir$ end offload

  ! reuse the data on the card using alloc_if(0) free_if(0) specifier
  !dir$ offload begin target(mic:0) nocopy(a:length(n*n) REUSE RETAIN) &
    & nocopy(b:length(n*n) REUSE RETAIN) nocopy(c:length(n*n) REUSE RETAIN)
  !dir$ attributes offload : mic :: offload_check
  call offload_check()

  iMaxThreads = omp_get_max_threads()

  print *,"matrix multiplication running with ",iMaxThreads," threads"
  !$omp parallel do 
  do i = 1, n
    do j = 1, n
      do k = 1, n
        c((j-1)*ldc+i) = c((j-1)*ldc+i) + a((k-1)*lda+i) * b((j-1)*ldb+k)
      end do
    end do
  end do
  !$omp end parallel do
  !dir$ end offload

  ! transfer C matrix to the host
  !dir$ offload begin target(mic:0) out(c:length(n*n))
  !dir$ end offload

  ! transfer the output matrix to host and free all the data on the card 
  ! using free_if(1)
  !dir$ offload begin target(mic:0) nocopy(a:length(n*n) FREE) &
    & nocopy(b:length(n*n) FREE) out(c:length(n*n) REUSE FREE)
  !dir$ end offload
end subroutine mmul
