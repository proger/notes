!
! Copyright 2003-2012 Intel Corporation. All Rights Reserved.
!
! The source code contained or described herein and all documents related 
! to the source code ("Material") are owned by Intel Corporation or its 
! suppliers or licensors.  Title to the Material remains with Intel Corporation 
! or its suppliers and licensors.  The Material is protected by worldwide 
! copyright and trade secret laws and treaty provisions.  No part of the 
! Material may be used, copied, reproduced, modified, published, uploaded, 
! posted, transmitted, distributed, or disclosed in any way without Intel's 
! prior express written permission.
!
! No license under any patent, copyright, trade secret or other intellectual
! property right is granted to or conferred upon you by disclosure or delivery
! of the Materials, either expressly, by implication, inducement, estoppel 
! or otherwise.  Any license under such intellectual property rights must
! be express and approved by Intel in writing.
!
!******************************************************************************
! Content:
!      Matrix multiplication sample code - omp offload solution.
!      
!******************************************************************************

!dir$ attributes offload : mic :: offload_check
subroutine offload_check()
#ifdef __MIC__
  print *,"Code running on MIC"
#else
  print *,"Code running on host"
#endif
end subroutine offload_check

subroutine mmul(a, lda, b, ldb, c, ldc, n)
  use omp_lib
  integer :: lda, ldb, ldc, n
  real, dimension(n*n) :: a,b,c
  integer :: i, j, k
  integer iMaxThreads

  ! send over the data to the card using the in clause, 
  ! execute the code and return data to the host using inout clause

  !dir$ offload begin target(mic) in(a:length(n*n)) in(b:length(n*n)) &
    & inout(c:length(n*n))

  ! code to test whether or not running in MIC
  !dir$ attributes offload : mic :: offload_check
  call offload_check()

  iMaxThreads = omp_get_max_threads()

  print *,"matrix multiplication running with ", iMaxThreads," threads"
  !$omp parallel do 
  do i = 1, n
    do j = 1, n
      do k = 1, n
        c((j-1)*ldc+i) = c((j-1)*ldc+i) + a((k-1)*lda+i) * b((j-1)*ldb+k)
      end do
    end do
  end do
  !$omp end parallel do
  !dir$ end offload
end subroutine mmul
