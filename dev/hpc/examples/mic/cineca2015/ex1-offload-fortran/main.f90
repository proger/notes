! Copyright 2003-2012 Intel Corporation. All Rights Reserved.
!
! The source code contained or described herein and all documents related 
! to the source code ("Material") are owned by Intel Corporation or its 
! suppliers or licensors.  Title to the Material remains with Intel Corporation 
! or its suppliers and licensors.  The Material is protected by worldwide 
! copyright and trade secret laws and treaty provisions.  No part of the 
! Material may be used, copied, reproduced, modified, published, uploaded, 
! posted, transmitted, distributed, or disclosed in any way without Intel's 
! prior express written permission.
!
! No license under any patent, copyright, trade secret or other intellectual
! property right is granted to or conferred upon you by disclosure or delivery
! of the Materials, either expressly, by implication, inducement, estoppel 
! or otherwise.  Any license under such intellectual property rights must
! be express and approved by Intel in writing.
!
!#******************************************************************************
!# Content:
!#      Driver for matrix multiplication examples.
!#      
!#*****************************************************************************/

include 'timer.f90'

module array_holder
 !dir$ attributes offload:mic:: a,b,c
 real, allocatable, dimension(:) :: a, b, c
end module array_holder

program mat_mul

  use timer
  use array_holder

  implicit none
  integer :: n               ! Matrix dimensions

  integer :: length, istatus
  integer :: iteration = 1
  character (len=20) :: argument
  real :: secs = 0.0
  integer :: i,j

  ! Check command line arguments 
  if (command_argument_count() /= 1) then
    call get_command_argument(0, argument)
    print *, "Usage: ", argument, " <N> "
    stop
  end if
  ! Parse command line arguments 
  call get_command_argument(1, argument, length, istatus)
  if(istatus/=0) then
    print *, "Unable to read array length"
    stop
  end if
  read (argument, "(I)"), n
  if (n <= 0) then
    print *, "Invalid matrix size"
    stop
  end if

  allocate( a(n*n), stat=istatus )
  if(istatus /= 0) then
    print *, "Could not allocate matrix A"
    stop
  end if
  allocate( b(n*n), stat=istatus )
  if(istatus /= 0) then
    print *, "Could not allocate matrix B"
    deallocate(a)
    stop
  end if
  allocate( c(n*n), stat=istatus )
  if(istatus /= 0) then
    print *, "Could not allocate matrix C"
    deallocate(a,b)
    stop
  end if

  ! Initialize the matrices. 
  a = 1.0
  b = 2.0
  c = 0.0

  do i = 1, iteration
    c = 0.0

    call start_timing()

    ! call the compute matrix mul function
    call mmul(a,n,b,n,c,n,n)

    secs = secs + end_timing()
  enddo

  print *, "avg time : ", secs/iteration, " sec"
  ! Display the result
  !do i = 1, n
  !  print *, (C(j + (i-1) * n),j=1,n)
  !end do 
  print *, "C(1)=", C(1)

  ! Free the matrices 
  deallocate(A,stat=istatus)
  deallocate(B,stat=istatus)
  deallocate(C,stat=istatus)
end program mat_mul
