# OpenMOLE

 * [OpenMOLE](http://www.openmole.org/current/). Execute a program on distributed computing environments. It executes several instance of the same program with different parameters or datasets. Typical usage is model calibration, model exploration, machine learning, optimization and data processing.

Romain Reuillon, Mathieu Leclaire
ERC Geodiversity
Institut des Systèmes Complexes

Parallélisation de calculs classiques ; modèles déjà facilement parallélisable.

Les infrastructures comme le Grid (mise en commun de clusters à travers le monde, de manière virtuelle) sont difficiles d'accès car complexes.

OpenMOLE se propose de faciliter l'accès à des ressources comme le Grid, de manière transparente (i.e.: en cachant la complexité).

Le code est OpenMOLE est développé en local sur sa machine, et testé, puis exécuter sur une grille de calcul.

Définition de boîtes noires sous OpenMOLE (entrées, sorties, code (NetLogo (plateforme de simulation multi-agents), code compilé, ...), ...). Les boîtes noires (taches) peuvent être reliées entre elles pour composer une expérience.

cdepack: outil développé par Stanford pour packager un exécutable linux avec toutes ses dépendances (en exécutant une fois le programme en local), et pouvoir ainsi l'exécuter sur n'importe quelle autre machine linux.

Cours de SCALA en ligne:
https://www.coursera.org/course/progfun
