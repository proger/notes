# nvidia-smi

Get information about NVIDIA GPUs.

Get information about GPU cards:
```sh
nvidia-smi
```

Get topology info:
```sh
nvidia-smi -m
```

Monitoring:
```sh
nvidia-smi dmon -i 1 -d 10
```
