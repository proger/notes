# SLURM
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

 * [Documentation](https://slurm.schedmd.com/documentation.html).
 * [Sample SLURM Scripts](https://help.rc.ufl.edu/doc/Sample_SLURM_Scripts).

`srun` executes in interactive and blocking mode.
`sbatch` executes in batch processing and non blocking mode.

Get information about nodes and partitions:
```sh
sinfo
```

Running a script:
```sh
sbatch myscript.slurm
```

Get list of jobs:
```sh
squeue # All
squeue --me # Just of the current user
```

Example of script:
```sh
#!/usr/bin/bash
#SBATCH --job-name=memoveralloc
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=john@lab.zz
#SBATCH -N 1
#SBATCH --mem=1gb
#SBATCH --time=00:05:00
#SBATCH --output=memoveralloc_%j.log
pwd; hostname; date

my/program arg1 arg2
echo "status=$?"

date
```

Look at running job:
```sh
sstat <jobid>
```

Look at list of terminated jobs:
```sh
sacct
```

Look at resource usage (CPU, memory) of terminated job:
```sh
seff <jobid>
```

Kill/cancel a job:
```sh
scancel <jobid>
```

## Installing

 * [Slurm](https://wiki.archlinux.org/title/Slurm).
 * [How to quickly set up Slurm on Ubuntu 20.04 for single node workload scheduling](https://drtailor.medium.com/how-to-setup-slurm-on-ubuntu-20-04-for-single-node-work-scheduling-6cc909574365).

On:
```sh
sudo apt install -y slurmd slurmctld # Debian
sudo pacman -S slurm-llnl
```
To configure a single-node slurm instance, edit `/etc/slurm/slurm.conf`, set
`SlurmctldHost` and `NodeName` to `localhost` and adjust `CPUs` and
`RealMemory` with computer settings:
```config
SlurmctldHost=localhost
NodeName=localhost CPUs=1 RealMemory=500 State=UNKNOWN
```
Start:
```sh
sudo mkdir /var/log/slurm-llnl
sudo chown slurm:slurm /var/log/slurm-llnl
sudo mkdir /var/lib/slurm-llnl
sudo chown slurm:slurm /var/lib/slurm-llnl
sudo systemctl start slurmd
sudo systemctl status slurmd
sudo systemctl start slurmctld
sudo systemctl status slurmctld
sudo scontrol update nodename=localhost state=idle
sinfo
```
