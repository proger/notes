# GPGPU

General-Purpose computing on Graphics Processing Units (GPGPU).
Use of GPU to perform tasks traditionally handled by CPU.

You might have a look at the book GPUGems2, chapter 31: Mapping Computational concepts to GPUs by Mark Harris. See <http://http.developer.nvidia.com/GPUGems2/gpugems2_chapter31.html>.

## CPU AND GPU

CPU
	* multi-core
	* sophisticated control logic unit
	* large cache memories
	* best for complex algorithms (with many conditionals)

GPU
	* many-cores
	* minimized control logic unit
	* prefer computing to storing temporary objects (global memory access is expensive).
	* not good with conditionals.
	* best with simple algorithm (straightforward) on huge amount of data
	* long-latency memory accesses.
	* take advantage of large number of threads to over-come long-latency memory accesses.
	* issue of portability between different technology: ATI GPUs are vectorial, NVIDIA GPUs are scalar. So a kernel code which takes advantage of vectorial programming on ATI cards, won't work well on NVIDIA cards.

CPU/GPU DATA TRANSFER
	* data transfer between CPU and GPU has a high rate but a big latency. So the bus is best at transfering huge amount of data, it costs a lot to transfer small amount of data.

One possible architecture when there are two CPUs on a computer is to plug one GPU on each CPU.

     +-------+  PQI  +-------+
     |       |<----->|       |
     | CPU_1 |  PQI  | CPU_2 |
     |       |<----->|       |
     +-------+       +-------+
         ^               ^
         |               |
         | PCIe3         | PCIe3   --> Note that there are 5 PCI slots by CPU,
         |               |             but a GPU requires 2 slots.
         V               V
     +-------+       +-------+
     |       |       |       |
     | GPU_1 |       | GPU_2 |
     |       |       |       |
     +-------+       +-------+

If GPU_1 communicates with CPU_2, the communication rate decreases. This is because we must go through the PQI bus which is slower.

## CUDA

Use `__CUDACC__` macro to avoid compiling CUDA code when compiling with another compiler than nvcc (g++, gcc, ...):
```c
#ifdef __CUDACC__
	/* CUDA code */
#endif
```

### Installing

See [NVIDIA developer web site](https://developer.nvidia.com/cuda-downloads) for downloading the CUDA toolkit. It includes the development toolkit, the drivers and the samples.

See the [toolkit archive](https://developer.nvidia.com/cuda-toolkit-archive) for downloading an older version.

See [MacOS-X Getting started guide](http://docs.nvidia.com/cuda/cuda-getting-started-guide-for-mac-os-x/#abstract) for explanations on how to install on MacOS-X:

 * Install the toolkit.
 * Run `kextstat | grep -i cuda` to check that the driver is installed correctly.
 * Go to `/Developer/NVIDIA/CUDA-7.0/samples` and compile the following examples: 
		make -C 0_Simple/vectorAdd
		make -C 0_Simple/vectorAddDrv
		make -C 1_Utilities/deviceQuery
		make -C 1_Utilities/bandwidthTest
 * Run the `deviceQuery` example: `bin/x86_64/darwin/release/deviceQuery`.
 
#### Installing on MacBook Air 2.13GHz Intel Core 2 Duo NVIDIA GeForce 320M with MacOS-X 10.9.5

With CUDA toolkit 7, `deviceQuery` fails:
	cudaGetDeviceCount returned 35
	-> CUDA driver version is insufficient for CUDA runtime version
	Result = FAIL

Works fine with CUDA toolkit 6.5.

### Compiling

Locate CUDA home directory `/usr/local/cuda*`.
Set `PATH`, `LIBRARY_PATH`, `CPATH` and `(DY)LD_LIBRARY_PATH` correctly.

Get manual
```bash
nvcc --help
```

For targetting an architecture:
```bash
nvcc --generate-code arch=compute_20,code=sm_20
nvcc -arch=compute_11 -code=sm_11
nvcc -arch=sm_13 	# <=> -arch=compute_13 -code=sm_13
```
`-arch` specifies the class of nVidia GPU, it influences the PTX intermediate virtual machine code generated.
`compute_*` are virtual architectures.
`sm_*` are real architectures.
`-code` specifies the targeted GPU. It can contains real or virtual architectures. At runtime the CUDA runtime system will try to load the best binary, or dynamically translate the PTX code embedded (virtual architectures `compute_*`).

### Running errors

`CUDA driver version is insufficient for CUDA runtime version`

### Profiling

#### TAU

Automatic source code instrumentation.

 * See slides from A. Malony, University of Oregon, talk given at SC11, tutorial M12.
 * See slides from S. Biersdorff, at <http://nic.uoregon.edu/~scottb/tau-overview.pdf>.

#### NSight

NSight VSE

#### NVVP

NVidia Visual Profiler.

#### nvprof

Command line profiling tool, equivalent to NVVP.

```bash
nvprof <program>
nvprof --kernels"::spmv_kernel_" --metrics "dram_utilization" <program>
nvprof --help
```

To get the list of all metrics:
```bash
nvprof --query-metrics
```

### OpenACC
 TODO Apparently generate CUDA code, and maybe other things.

### CUDA Architecture

     +---------------+ +---------------+
     |       SM      | |       SM      |    SM = streaming multiprocessor
     | +-----------+ | | +-----------+ |
     | | Registers | | | | Registers | |
     | +-----------+ | | +-----------+ |
     | +-----------+ | | +-----------+ |
     | |     L1    | | | |     L1    | |
     | +-----------+ | | +-----------+ |
     +---------------+ +---------------+
             ^                 ^
             |                 |
             v                 v
     +---------------------------------+
     |               L2                |
     +---------------------------------+
                     ^
                     |
                     v
     +---------------------------------+
     |              DRAM               |
     +---------------------------------+

 * A GPU executes one or more kernel grids.
 * An SM executes one or more thread blocks.
 * An SP (Streaming Processor), in an SM, executes threads.

The registers are shared on each SM. Thus the threads share the registers. One thread keep the same registers during all its life time (the number of registers is determined at compile time). If each thread allocates a huge number of registers, then the number threads will be limited.

The idem idea with the blocks and the L1 cache. If we have a shared memory (cache L1) of 48kiB and that each block uses 40kiB, then only one block will get executed on the SM.

Inside each execution block, the threads are grouped into warps of (today) 32 threads. The 32 threads of a warp execute the same instruction at the same time.

Control flow efficiency = percentage of threads that _do_ work in a warp (in % of time of threads).

Existing architectures: Fermi, Kepler, Maxwell.

Uses IEEE 754-2008 floating point standard.

FMA (Fused multiply-add) )instruction for float and double.
New designed integer ALU optimized for 64bit.

#### Caches

Each line of the L1 cache makes 128 bytes.
Each line of the L2 cache makes ..? bytes.

#### Cache transactions

Each loading of a line of consecutive values from the main memory to the cache L1 requires 1 transaction.

Example 1:
Each of the 32 threads of a warp accesses a 32 bit float. In this case, all this well as long as the set of 32 floats are aligned in memory, so they can be put together on the single L1 cache line (32 x 4 bytes = 128 bytes).

Example 2;
Like example 1 but instead of floats, the threads access doubles, then it makes 32 x 8 bytes = 256 bytes to load. It will be split into 2 lines of L1 cache, and thus will require 2 transactions to be loaded.

Example 3:
Like example 3 but the doubles are arranged in columns instead of lines in the main memory, and each line in the main memory makes 128 bytes, then we will load all the 32 lines of doubles from main memory to cache L1. So it will make 32 transactions.

#### Branch/Warp divergence

```cuda
if (threadIdx.x % 32 < 24) {
}
else {
}
```

In a warp of threads, all 32 threads execute the same instruction at the same time. If 24 threads execute the `if` statement and the other 8 the `else` statement, then the GPU is forced to execute the `if` in a first step, and the `else` in a second step. As a consequence total_time = if_time + else_time.

Technique of divergence reduction; it's preferable to make the condition on the index of the block instead of the index of the thread. Thus a block will make the `if` and another the `else`. Since warps are created from blocks, that means that different warps will run the `if` and the `else`.

#### Bad alignment inside memory

Compilers align data in memory (basic types, structure, ...). This means that it makes sure the first byte of a data start at a line inside the main memory. The line correspond to a line of memory that would be loaded inside the L1 cache.

Example 4: bad alignment inside memory
Suppose you have a structure like this:
```cuda
struct {
	unsigned char flag;
	double a[64];
}
```
and that you packe the structure so that there's no byt space between the fields.
Then it will look like this in memory:
      flag
      |  start of array a
      |  |
      |  |
      v  v
     +--+--+--+--+--+              +--+--+--+--+
     |  |  |  |  |  |..............|  |  |  |  |  <-- first line of cache
     +--+--+--+--+--+              +--+--+--+--+
     +--+--+--+--+--+              +--+--+--+--+
     |  |  |  |  |  |..............|  |  |  |  |  <-- second line of cache
     +--+--+--+--+--+              +--+--+--+--+
       ^
       |
       |
       end of array a

So the array a will require two cache lines to be loaded inside L1 cache.

#### Cache blocking

On a GPU the L1 and texture caches are very small. If we use them to do cache blocking (as it's done on CPUs), they will be saturated.

Example of cache blocking: I load a whole line of matrix, because mine thread will iterate on the whole line. On CPU this can work because the cache is big and the number of threads small, so each thread can get its line of matrix inside the cache.
On GPU, the number of threads is big, and so the number of matrix lines to load is too big.

### Texture cache

`__ldg(&...)` loads the data inside the texture cache.

### Kernel

A kernel is a device function.

Define a kernel:
```cuda
// set dimensions (grid & block)
dim3 dimBlock(N / 2);
dim3 dimBlock(N / 2, N);
dim3 dimGrid(2);
dim3 dimGrid(2, 4, 1);

// maximum in grid & block dimensions
// TODO

// declaration of a kernel function
__global__ void add( int *a, int *b, int *c ) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	/* blockDim  --> dimension of the block
	   threadIdx --> index of thread inside the block
	   blockIdx  --> index of block inside the grid */
	c[idx] = a[idx] + b[idx];
}
```

Call a kernel:
```cuda
add<<<dimGrid, dimBlock>>>(dev_a, dev_b, dev_c );
```

### Functions

Qualifier      | Executed on | Callable from
-------------- | ----------- | --------------
`__device__`   | device      | device
`__global__`   | device      | host
`__host__`     | host        | host

### Variables

Qualifier      | Memory   | Lifetime    | Scope
-------------- | -------- | ----------- | -------
`<none>`       | register | kernel      | thread
`__shared__`   | shared   | kernel      | block
`__device__`   | global   | application | grid
`__constant__` | constant | application | grid

### Memory allocation

```cuda
// allocate memory on GPU
HANDLE_ERROR( cudaMalloc( (void**)&dev_a, sizeof(int) * N) );

// free memory on GPU
HANDLE_ERROR(cudaFree(dev_a));

// memcpy
HANDLE_ERROR(cudaMemcpy(dev_a, a, sizeof(int) * N, cudaMemcpyHostToDevice));
/* parameter for direction of copy:
   cudaMemcpyHostToDevice
   cudaMemcpyDeviceToDevice
   cudaMemcpyDeviceToHost */

/* allocate memory on GPU taking into account coalescence:
   it will pad lines in order to meet the alignment requirements,
   and thus improve performances. So in the array, the lines will be of
   length pitch, but only NX columns will be used.
 */ 
size_t pitchBytes1;
cudaMallocPitch((void**) &d_data1, &pitchBytes1, NX*sizeof(real_t), NY);
```

### Shared memory allocation

Shared memory is the memory shared by threads inside a same block.

Static allocation inside a kernel:
```cuda
__global__ void Kernel(int count_a, int count_b) {
	__shared__ int a[100];
	__shared__ int b[4];
}
```

Dynamic allocation:
```cuda
__global__ void Kernel(int count_a, int count_b) {
	extern __shared__ int *shared;
	int *a = &shared[0]; //a is manually set at the beginning of shared
	int *b = &shared[count_a]; //b is manually set at the end of a
}

sharedMemory = count_a*size(int) + size_b*size(int);
Kernel <<<numBlocks, threadsPerBlock, sharedMemory>>> (count_a, count_b);
```

### CUDA asynchronous calls

A kernel call is asynchronous by design.
A memory copy can also be asynchronous.
To synchronize, call `cudaDeviceSynchronize()`.

### CUDA errors

Synchronous calls return a `cudaError_t` value.
Asynchronous calls don't return errors unless they are synchronize with `cudaDeviceSynchronize()`.
To get last error, call `cudaGetLastError()`.

To get an error message, call `cudaGetErrorString()`.

### CUDA threads organization (grid)

kenerl executed as a *grid* of threads.

Grid = 1D, 2D or 3D array of thread blocks.
Block = 1D, 2D or 3D array of threads.
All blocks have the same number of threads organized in the same manner.
                  
Example of a 2D grid 3x2:
   -------------------------------------
   |               Grid                |
   |                                   |
   |  ---------  ---------  ---------  |
   |  | Block |  | Block |  | Block |  |
   |  | (0,0) |  | (1,0) |  | (2,0) |  |
   |  ---------  ---------  ---------  |
   |                                   |
   |  ---------  ---------  ---------  |
   |  | Block |  | Block |  | Block |  |
   |  | (0,1) |  | (1,1) |  | (2,1) |  |
   |  ---------  ---------  ---------  |
   |                                   |
   -------------------------------------

Each block in this grid is 5x4:
                    
   ------------------------------------------------------------
   |                          Block                           |
   |                                                          |
   |  ---------- ---------- ---------- ---------- ----------  |
   |  | Thread | | Thread | | Thread | | Thread | | Thread |  |
   |  | (0,0)  | | (1,0)  | | (2,0)  | | (3,0)  | | (4,0)  |  |
   |  ---------- ---------- ---------- ---------- ----------  |
   |                                                          |
   |  ---------- ---------- ---------- ---------- ----------  |
   |  | Thread | | Thread | | Thread | | Thread | | Thread |  |
   |  | (0,1)  | | (1,1)  | | (2,1)  | | (3,1)  | | (4,1)  |  |
   |  ---------- ---------- ---------- ---------- ----------  |
   |                                                          |
   |  ---------- ---------- ---------- ---------- ----------  |
   |  | Thread | | Thread | | Thread | | Thread | | Thread |  |
   |  | (0,2)  | | (1,2)  | | (2,2)  | | (3,2)  | | (4,2)  |  |
   |  ---------- ---------- ---------- ---------- ----------  |
   |                                                          |
   |  ---------- ---------- ---------- ---------- ----------  |
   |  | Thread | | Thread | | Thread | | Thread | | Thread |  |
   |  | (0,3)  | | (1,3)  | | (2,3)  | | (3,3)  | | (4,3)  |  |
   |  ---------- ---------- ---------- ---------- ----------  |
   |                                                          |
   ------------------------------------------------------------

Inside a block of threads, threads can cooperate using:

 * barrier synchronization with `__syncthreads()` function.
 * shared memory.

## HMPP

Jean-Charles VASNIER
Application Engineer - IT Trainer 
CAPS entreprise - Global Solution for Many-core Programming 

Discover How to Survive the Many-Core Revolution 
CAPS Methodology for Code Migration to Many-Core 

T. +33 222 511 610 - F. +33 223 201 643 
M. +33 684 921 868
Immeuble Cap Nord A - 4, allée Marie Berhaut 
35000 Rennes - France 

http://www.caps-entreprise.com 
http://twitter.com/CAPSentreprise & http://weibo.com/capsentreprise 
http://www.openhmpp.org
