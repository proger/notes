# INTEL XEON PHI

Xeon Phi is the commercial of the Intel Many Integrated Core Architecture or Intel MIC.

Intel provides a mathematics library called *MKL*, Intel Math Kernel Library, that can be run either on the CPU or on the MIC.

The organization of the threads can be controlled with the KMP_AFFINITY value, which can be set to `compact`, `balanced` or `scatter`. See the example below for 4 cores and 8 threads:
  
             ------     ------     ------     ------
   cores --> |  1 |     |  2 |     |  3 |     |  4 |
             |    |     |    |     |    |     |    |
             ------     ------     ------     ------
  
   compact   1 2 3 4    5 6 7 8   

   balanced  1 2        3 4        5 6        7 8

   scatter   1 5        2 6        3 7        4 8
