# Kubernetes
<!-- vimvars: b:markdown_embedded_syntax={'sh':''} -->

Kubernetes is a container orchestrator.

 * [Tutorials](https://kubernetes.io/docs/tutorials/).
 * [Run a Stateless Application Using a Deployment](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/).
 * [Run a Single-Instance Stateful Application](https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/). Uses a persistent volume.

## Minikube

### Install

 * [Running Kubernetes Locally via Minikube](http://kubernetes.io/docs/getting-started-guides/minikube/).
 * [Install and Set Up kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/).
 * [minikube](https://github.com/kubernetes/minikube/releases).
 * [Helm release](https://github.com/kubernetes/helm/releases).

First check VT-x/AMD-v virtualization is enabled in BIOS:
```sh
sysctl -a | grep machdep.cpu.features | grep VMX
```

Install `kubectl` on macOS:
```sh
brew install kubernetes-cli
```

Install `kubectl` on Debian:
```sh
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
```
See [Install and Set Up kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

Download and install minikube:
```sh
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.9.0/minikube-darwin-amd64 # ! Check for newer version of minikube
chmod +x minikube
mv minikube /usr/local/bin/
```

### Use

Start a minikube cluster:
```sh
minikube start
```
It creates a “minikube” context, and set it to default in kubectl.

To stop the cluster:
```sh
minikube stop
```

To destroy a stopped cluster:
```sh
minikube delete
```

Open the Kubernetes dashboard inside a browser (need first to switch to context):
```sh
minikube dashboard
```

Minikube contains a built-in Docker daemon for running containers. If you use another Docker daemon for building your containers, you will have to publish them to a registry before minikube can pull them. You can use minikube’s built in Docker daemon to avoid this extra step of pushing your images. Use the built-in Docker daemon with:
```sh
eval $(minikube docker-env)
```

## kubectl

Switch to context and look at pods (you should see the "manager" and the "dashboard"):
```sh
kubectl config use-context minikube
kubectl get pods --all-namespaces
```

Get logs of pods:
```sh
kubectl logs -f mypod
```

Get the nodes in the cluser:
```sh
kubectl get nodes
```

To log into a running pod:
```sh
kubectl exec -i -t my-pod bash
```

Create a deployment:
```sh
kubectl apply -f mydeployment.yaml
```

List deployments:
```sh
kubectl get deployments
```

Get information on a deployment:
```sh
kubectl describe deployment mydeployment
```

List pods:
```sh
kubectl get pods
```

Delete a deployment:
```sh
kubectl delete deployment mydeployment
```

## Helm

To install Tiller on minikube:
```sh
helm init
```

To upgrade Tiller (the server side of Helm):
```sh
helm init --upgrade
```

To get versions of Helm and Tiller (Server):
```sh
helm version
```


