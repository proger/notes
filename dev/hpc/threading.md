INTEL TBB
=========

Intel® Threading Building Blocks (Intel® TBB) lets you easily write parallel C++ programs that take full advantage of multicore performance, that are portable and composable, and that have future-proof scalability.

[TBB](https://www.threadingbuildingblocks.org) (Threads Building Blocks) est une bibliothèque C++ pour le parallélisme en mémoire partagée, basée sur un principe de vol de tâches.

MONITOR
=======

Provides mutual exclusion at a higher-level.
A monitor is an object whose methods can only be executed by one thread at a time.

MUTEX
=====

MUTUAL EXCLUSION LOCK = MUTEX = LOCK 

Used to control access a critical section (e.g.: write into shared memory like a global variable).

A mutex is a binary semaphore that usually incorporates extra features, such as ownership, priority inversion protection or recursivity. The differences between mutexes and semaphores are operating system dependent, though mutexes are implemented by specialized and faster routines. Mutexes are meant to be used for mutual exclusion (post/release operation is restricted to thread that called pend/acquire) only and binary semaphores are meant to be used for event notification (post-ability from any thread) and mutual exclusion.
Events are also sometimes called event semaphores and are used for event notification.

A mutex has support in the underlying hardware.

Steps:
 1. Obtain the mutex (lock). This is usually a blocking call, i.e.: if the mutex is already locked, the call to the function will block execution of the thread.
 2. Execute the critical section. This section must be as short and as fast as possible, since this is a serial code and decrease parallel code performance.
 3. Relinquish the mutex (unlock).

BUSY-WAITING
============

Insure access to critical section by use of programming mean only (no hardware object like mutex).

Example:
```c
if (my_rank == 1)
	while ( ! ok_for_1);
/* critical section */
if (my_rank == 0)
	ok_for_1 = true;
```

This is called busy-waiting since the thread can be very busy waiting for the condition, the while loop is highly time-consuming.

Another form of busy-waiting:
```c
flag = 0; /* somewhere in the code of main thread */
/* ... */
y = Compute(my_rank);
while (flag != my_rank);
x += y;
++flag;
```

IMPORTANT: the compiler isn't aware that the program is multithreaded when using busy-waiting, so if optimizations are one it can rearrange busy-waiting code and exchange order of lines.

SEMAPHORE
=========

Aim: control producer-consumer synchronization (i.e.: a thread must wait for another thread to finish some job).

In computer science, a semaphore is a protected variable or abstract data type that constitutes a classic method of controlling access by several processes to a common resource in a parallel programming environment. A semaphore generally takes one of two forms: binary and counting. A binary semaphore is a simple "true/false" (locked/unlocked) flag that controls access to a single resource. A counting semaphore is a counter for a set of available resources. Either semaphore type may be employed to prevent a race condition. On the other hand, a semaphore is of no value in preventing resource deadlock, such as illustrated by the dining philosophers problem.

BARRIER
=======

A barrier is used to synchronize multiple thread between them at some point in the code. So they start together from this point.

It can be used to time a section of code, by measuring time of the slowest thread.

It can also be used to debug code by printing message when all threads reach a certain section of code:

```c
/* code for the barrier, here */
if (my_rank == 0) {
	printf("All threads reached this point\n");
	fflush(stdout);
}

/* Condition variables are the best solution for implementing a barrier : */
lock mutex;
if (condition has occured)
	signal threads;
else {
	unlock the mutex and block;
	/* when thread is unblocked, mutex is relocked */
}
unlock mutex;
```

MEMORY
======

Shared-Memory model
-------------------

Variables can be shared (among all threads, in read/write) or private (accessible only by one thread).

Communication is implicit, through shared variables.

Transactional memory
--------------------

The critical section is treated as a transaction.
Either a thread successfully completes the critical section or any partial results are rolled back.

THREAD SAFETY
=============

An Introduction to Parallel Programming, Pacheco, chp 4.11, p 195
An Introduction to Parallel Programming, Pacheco, chp 5.10, p 257/

 A block of code is thread-safe if it can be simultaneously executed by multiple threads without causing problems.

Functions like the C standard function `strtok` (which splits a string and return the next substr at each subsequent call) that uses static variables are not thread safe. This is because a static variable lives on the heap, and is shared by all threads.

A thread-safe version of `strtok` exists, it's called `strtok_r`.

Other non-thread-safe C standard functions: `random`, `localtime`.
======================== cache coherence.txt
/* vi: se ft=C : */

/* Processors sharing the same memory, and having internal caches, present issues about coherence of these internal caches.

Time    Core 0          Core 1
0       y0 = x;         y1 = 3 * x;
1       x = 7;          ... 
2       ...             z1 = 4 * x; // value of z1 ?

To be sure that on step 2, the value of x used for computing z1 is 7, a system of cache coherence is used.
On step 0, the value of x is copied to cache of core 0 and to cache of core 1.
On step 1, the value of x is copied from cache of core 0 to main memory.
On step 2, core 1 is warned that it must update its cache line containing x, by copying new values from main memory.

SOLUTION 1: SNOOPING CACHE COHERENCE
====================================
An Introduction to Parallel Programming, Pacheco, chp 2.3.4, p44

Caches are synchronized by sending messages on bus when some cache lines need updating. Each processor snoops (listen to) the bus for messages that are relevant to them.

SOLUTION 2: DIRECTOY-BASED CACHE COHERENCE
==========================================
An Introduction to Parallel Programming, Pacheco, chp 2.3.4, p44

Snooping cache coherence is too expensive (in network broadcasts) for large number of processors.
Each processor store cache information in a private directory, thus reducing the network trafic.

*/

/***********************************************************/
/***********************************************************/
/* PERFORMANCE CONSEQUENCIES OF THE USE OF CACHE COHERENCE */
/***********************************************************/
/***********************************************************/

/*---------------*/
/* FALSE SHARING */
/*---------------*/
/* An Introduction to Parallel Programming, Pacheco, chp 2.3, p 45 */

/* Suppose we execute the following line of codes on 2 cores : */
int i, j, iter_count;
int m, n, core_count;
double y[m];

iter_count = m / core_count;

/* core p does this */
for ( i = p * iter_count ; i < (p+1) * iter_count ; ++i)
	for ( j = 0 ; j < n ; ++j)
		y[i] += f(i,j);

/* Suppose that :
	_ m = 8, 
	_ double are 8 bytes, 
	_ cache lines are 64 bytes, 
	_ y[0] is stored at the beginning of a cache line
*/

/* Array y take one full cache line (m = 8).
   So each time a core updates y array (y[i]=...), the whole cache line is invalidated, and when the other core tries to execute the same statement (y[i]=...) it must first update the cache line from main memory. So y assignement will always access main memory, in spite of the fact that the two cores doesn't share any elements of y (core 0 accesses y[0]..y[3], and core 1 accesses y[4]..y[7]).
   This is called false sharing because the system is behaving as if the elements of y were being shared by the cores.
 */

/* More generaly, false sharing is when the cache coherence system forces update of cache memory for other cores, when one core has modified the cache line. Even tought the other cores doesn't use the variable (or the part of the array) modified. So this can happen for an array stored in a cache line, like in this example, but also with several variables stored together in the same cache line (if "a" and "b" are stored together inside the same cache line, and core 0 keeps modifying "a" all the time, then core 1 will have to update its cache line even if it doesn't use "a" but only "b"). */

/*-------------------------*/
/* MATRIX x VECTOR PRODUCT */
/*-------------------------*/
/* An Introduction to Parallel Programming, Pacheco, chp 4, p 190 */
/* An Introduction to Parallel Programming, Pacheco, chp 5, p 252 */

/* Product of a matrix by a vector : work is divided among several threads. */

/* Pthreads version */
void* thread_mat_vect_mult(void *rank) {
	long my_rank = (long)rank;
	int i,j ;
	int local_m = m / thread_count;
	int my_first_row = my_rank * local_m;
	int my_last_row = (my_rank + 1) * local_m - 1;

	for (i = my_first_row ; i <= my_last_row ; ++i) {
		y[i] = 0.0; /* <-- write-miss */
		for (j = 0 ; j < n ; ++j)
			y[i] = A[i][j] * x[j]; /* <-- read-miss of x, and false sharing of y */
	}

	return NULL;
}

/* OpenMP version */
#pragma omp parallel for num_threads(thread_count) default(none) private(i, j) shared(A, x, y, m, n)
for (i = 0 ; i < m ; ++i) {
	y[i] = 0.0; /* <-- write-miss */
	for (j = 0 ; i < n ; ++j)
		y[i] += A[i][j] * x[j]; /* <-- read-miss of x, and false sharing of y */
}

/* We test this code with different sizes of A, and different numbers of threads, and measure computing time and efficiency.

   We recall that :
		Speedup = S = Tserial / Tparallel
		Efficiency = S / p = Tserial / ( p * Tparallel)
		p = number of threads

	Here are the results for OpenMP version :

	----------------------------------------------------------------------
	|         |               Matrix Dimension                           |
	|         |----------------------------------------------------------|
	| Threads | 8,000,000 x 8 (A) | 8000 x 8000  (B) | 8 x 8,000,000 (C) |
	|         |-------------------|------------------|-------------------|
	|         | Time  | Eff.      | Time  | Eff.     | Time  | Eff.      |
	|---------|-------|-----------|-------|----------|-------|-----------|
	|    1    | 0.322 | 1.000     | 0.264 | 1.000    | 0.333 | 1.000     |
	|    2    | 0.219 | 0.735     | 0.189 | 0.698    | 0.300 | 0.555     |
	|    4    | 0.141 | 0.571     | 0.119 | 0.555    | 0.303 | 0.275     |
	----------------------------------------------------------------------
	Processor has 2 cores.
	Cache line is 64 bytes long.

	Cases A and C are slower than B, this is partially due to cache performance issue.

	In case A, y is big and causes write-misses more often than in the other cases.
	In case C, x is big and causes read-misses more often than in the other cases.

	With 2 or 4 threads, case C is a lot less efficient than cases A and B.
	Explanation:
		Take the case of 4 threads.
		Case A --> y has 8,000,000 elements, so each thread is assigned 2,000,000 elements.
		Case B --> each thread get 2,000 elements.
		Case C --> each thread get 2 elements.

		double is 8 bytes, and a cache line is 64 byte. So a line can contain up to 8 doubles.
		Each time an element of y is written to the cache, the full line is invalidated if it's also stored in the cache of another processor (each processor has its own cache).
		In case C, if suppose threads 0 and 1 are on core 0 and threads 2 and 3 on core 1, and y is stored in a single cache line, then every write to some element of y will invalidate the line in the other processor's cache (since the two processor have the same cache line for y).
		So each thread will update the cache line 8,000,000 times, for each element of y, even though each thread access a different set of elements of y.
		In total, since each thread processes 2 elements of y, each thread will update the cache line 16,000,000 times.
		Almost all of these updates force access to main memory.
		This is a "FALSE SHARING" case.

	For case B, there's no false sharing issue, since for the case of 4 threads:
	thread 0 processes y[0], y[1], ..., y[1999]
	thread 1 processes y[2000], y[2001], ..., y[3999]
	thread 2 processes y[4000], y[4001], ..., y[5999]
	thread 3 processes y[6000], y[6001], ..., y[7999]
	Most of the time the cache line containg y values for each thread, will not be shared by other threads.
	Even at borders, for instance y[1999], there will be no issue since the other thread will be long done with y[2000] value.
   */

/* Write misses.
   
   
   */

/* Depending on the size of the matrix, the number of write-misses or read-misses can be huge.

   For instance for a 8 x 8,000,000 matrix, the y vector has 8,000,000 elements and so can't reside entirely in the cache the time of the computation. When assigning a value to one of its element, we often meet a write-miss, and force a cache update.
   On the other hand, for a 8,000,000 x 8 matrix, the x vector is huge and so we get a huge number of read-misses.
   
   Valgrind cache profiler can be used to detect such issues.

   False sharing appears when for instance for a 8 x 8,000,000 matrix, y vector is stored in single cache line. Each time y is modified, the cache line will be invalidated. So each thread will force cache update 8,000,000 times, despite the fact they use different parts of y vector.

   Two solutions for false sharing :
	_ pad y vector with dummy elements, so that on each line of cache there are only y elements for one thread. So no line of cache containing y elements will be shared among threads.
	_ each thread uses a local y vector, and at the end of the multiplication the thread copy its result to the main y vector.
*/
======================== false sharing.txt
See "cache coherence".
======================== hardware_multithreading_architecture.txt
There are two main hardware architectures on CPUs.

***************************
* TEMPORAL MULTITHREADING *
***************************

Only one thread at a time is executed inside a pipeline stage.

*************************************
* SIMULTANEOUS MULTITHREADING (SMT) *
*************************************

More than one thread can be executed inside a pipeline stage.
In practical, this is limited to two threads.
======================== non-determinism.txt
Since the order of execution of threads is decided by the OS and hardware, it's impossible to predict and will be different at each execution. As a consequence the parallel computation is nondeterministic.

So particular attention must be put on shared memory, since write access of several threads to the same shared object can result in incorrect results.

Race condition = when threads or processes attemp to simultaneously access a resource (computation depends on which thread wins the race).

Critical section = a block of code that can only be executed by one thread at a time. Mutually exclusive access to the critical section must be insured by programming means.

Mutual exclusion lock = mutex = lock = mechanism for insuring mutual exclusion.
======================== performance.txt
_ avoid using too much locking (locking of too much data) or using mutex where read-write locks are preferable.
_ take into account cache coherence issues that cost much in performance.
======================== thread launching schemes.txt
Dynamic threads
===============
A master thread waits for work requests. When a new request arrives, the master thread launches a worker thread.
The worker thread carries out the request, terminates and then joins the master thread.
Forking and joining threads can be time-consuming, so the static threads paradigm could be preferred since during the lifetime of the master thread all worker threads are kept alive even if there are no requests.

Static threads
==============
A master thread launches all needed threads at start-up.
After all the threads terminate, the master terminates too.
It's greedy in terms of resources, since some threads could be idle. 

This paradigm is close to the most widely used paradigm for distributed-memory programming.

Mixed approach
==============
A master thread launches all needed threads at start-up.
Threads that are not useful (not enough work to do) are kept idle.
Master finishes all threads when work is done.
======================== thread_and_core_association.txt
Pin threads to cores (i.e. associate each thread with a particular core).
Don't let OS moves threads from one core to another.
======================== OpenMP/OpenMP.txt
http://openmp.org/wp/

OpenMP (Open Multi-Processing) is an API that supports multi-platform shared memory multiprocessing programming in C, C++, and Fortran.
It was explicitly designed to allow programmers to incrementally parallelize existing serial programs.

Since it uses pragmas, OpenMP code, if written carefully, can also be compiled on a platform that doesn't support OpenMP. Use the macro _OPENMP to avoid compiling of OpenMP code on a platform that doesn't support it. 
======================== OpenMP/atomic.txt
/* vi: se ft=C : */

#include <omp.h>

/* Protection of shared variable by an atomic directive. */

/* For atomic operations, use atomic pragma directive.
   It is much more efficient because it uses the property of the atomic instructions (single load-modify-store instruction), avoiding the use of classic heavy protection for critical sections.
 */
#pragma omp atomic
++x;
/* The following operators are considered atomic:
   ++(pre) (post)++ --(pre) (post)-- += -= *= /= &= ^= |= <<= >>=
   
   Note that the expression at the right of the assignment operator must not make reference to the shared variable being modified:
   x += x;      --> Non-sense !

   Note also that the expression at the right of the assignment operator is not protected:
   x += y++;    --> y isn't in a critical section !
 */

/* It isn't specified if atomic sections must or not enforce the same mutual exclusion across all directives. Some implementations of OpenMP will block execution of (2) when (1) is executed, even if the accessed variables are different: */
#pragma omp atomic
x++; /* (1) */
#pragma omp atomic
y++; /* (2) */
/* Some other implementations will allow (2) to be executed when (1) is executed. */
======================== OpenMP/compiling.txt
# vi: ft=sh

# Unlike MPI or Pthreads, it requires compiler support for some special operations. So not all C compilers can compile OpenMP code.

# gcc
gcc -fopenmp ...
======================== OpenMP/critical.txt
/* vi: se ft=C : */

#include <omp.h>

/* To protect an access to a shared variable,
   use critical pragma directive.
   All threads will wait when meeting a critical section, if another thread is already executing such a critical section.
   This is a particular case of the named critical section, where the name isn't provided. The same unspecified name is then used accross all unnamed critical sections.
 */
#pragma omp critical
*global_var += i;

/* Named critical section.
   All threads will wait when meeting a named critical section, if another thread is already executing a critical section of the same name.
 */
#pragma omp critical(foo)
my_shared_array[i] += 3;

/* Don't mix atomic and critical for a single critical section: */
#pragma omp atomic
x += f(y);
#pragma omp critical    /* here the writer uses critical because of operator = */
x = g(x);
/* The critical directive won't exclude the execution of the code marked by the atomic directive.
   The solutions are either to change function g(x) so that operator += or another can be used and thus atomic directive can be used, or to use only critical directive in both cases.
 */
======================== OpenMP/includes.txt
/* vi: se ft=C : */

#ifdef _OPENMP
#include <omp.h>
#endif
======================== OpenMP/locks.txt
/* vi: se ft=C : */

#include <omp.h>

/* Locks are dynamic structures.
   So here we meet the limits of OpenMP pragma model, since not all concurrency problems can be solved with compile time decisions.

   Locks are necessary in order, for instance, to get a fine grain control on critical access of dynamic data structures. If we suppose we have a complex data structure shared by threads, certainly not all threads access at the same time to all the data structure. So the data structure can be divided in parts. Access of each part is then controlled by a lock.
 */

/* Simple locks (=mutex). */
omp_lock_t mylock;
omp_init_lock(&mylock); /* initialize lock structure. */
omp_destroy_lock(&mylock); /* uninitialize lock structure. */
omp_set_lock(&mylock); /* set lock */
omp_unset_lock(&mylock); /* unset lock */

/* Nested locks (=semaphore). */
======================== OpenMP/parallel for directive.txt
/* vi: se ft=C : */

#pragma omp parallel for num_threads(N) reduction(+: sum)
for (i = 1 ; i <= n-1 ; ++i)      /* each thread receives a copy of i */
	sum += f(a + i * h);            /* sum must be a reduction variable, so this line becomes a protected critical section */

/* Only for loops for which the number of iterations can be determined before.
   ==> No ifinite loops, no break/return from inside a loop.
 */

/* Legal forms for the for statement : */
for (index = start ; index < end ; ++index)
                           <=      --index
                           >=      index += incr
                           >       index -= incr

/* Data dependence (loop-carried dependence) */
fibo[0] = fibo[1] = 1;
#pragma omp parallel for num_threads(N)
for (i = 2 ; i < n ; ++i)
	fibo[i] = fibo[i-1] + fibo[i-2];
/* Can't work because the work is divided among the threads, so it can happen that fibo[i-1] and fibo[i-2] aren't computed at the time fibo[i] is computed. */

/* Private variables. */
double pi = 0.0;
double factor;
int i;
#pragma omp parallel for num_threads(N) reduction(+:pi) private(factor)
for (i = 0 ; i < n ; ++i) {
	factor = i % 2 == 0 ? 1.0 : -1.0; /* here factor must be private, or it will be shared among threads and take undefinied value before evaluation of the following line. */
	pi += factor / (2*i+1); /* computation of pi = 4 * sum((-1)^k / (2*k+1))  */
}
pi *= 4;

/* Forcing declaration of the scope of all variables. */
#pragma omp parallel for num_threads(N) default(none) reduction(+:pi) private(factor, i) shared(n)
for (i = 0 ; i < n ; ++i) {
	factor = i % 2 == 0 ? 1.0 : -1.0;
	pi += factor / (2*i+1);
}

/* Declaring threads before using them in a for loop.
   This avoid the cost of creating threads each time.
   Of course this in the case the same number of threads is created each time.
 */
#pragma omp parallel num_threads(N) default(none) shared(a, n) private(i, phase ,tmp)
for (/*some outer loop*/) {
	if (condition)
#pragma omp for
		for (i = 1 ; ... )
			do_work();              /* There's still an implicit barrier at the end of the for loop. */
	else
#pragma omp for
		for (i = 1 ; ... )
			do_work();
}
======================== OpenMP/running threads.txt
/* vi: se ft=C : */

#include <omp.h>

/* The structured block of code following this pragma directive is executed in threads : */
#pragma omp parallel num_threads(4)
my_thread();
/* Note that nothing guarantees that the system will effectively run the specified number of threads. */
/* OpenMP language: a team of threads is started, with one master thread (the original thread) and slave threads. */

/* You can specifiy a variable for the number of threads: */
#pragma omp parallel num_threads(my_number_of_threads)
my_thread();
======================== OpenMP/scheduling.txt
/* vi: se ft=C: */

/* Scheduling, in OpenMP, is the operation of dispatching work among threads. */

/* In a parralel for directive, the default scheduling is a block dispatching. */
sum = 0.0;
#pragma omp parallel for num_threads(thread_count) reduction(+:sum)
for (i = 0 ; i <= n ; ++i)
	sum += f(i);
/* to thread t (0..thread_cound-1) is assigned the values of i from (n * t / thread_count) to (n * (t+1) / thread_count - 1). */

/* Static scheduling (cyclic dispatching): */
sum = 0.0;
#pragma omp parallel for num_threads(thread_count) reduction(+:sum) schedule(static, 1)
for (i = 0 ; i <= n ; ++i)
	sum += f(i);
/* For n = 12 (0..11) and thread_count = 3, we have the following dispatching:
   thread       values of i
   0            0,3,6,9
   1            1,4,7,10
   2            2,5,8,11 */
/* The number in the schedule command, called chunksize, is the step used when iterating over values of i. So if we set schedule(static, 2), we get :
   thread       values of i
   0            0,1,6,7
   1            2,3,8,9
   2            4,5,10,11 */
/* If chunksize = n / thread_cound, then it's a block dispatching.
   If chunksize is ommited, it's set to n / thread_count. */

/* Dynamic scheduling */
sum = 0.0;
#pragma omp parallel for num_threads(thread_count) reduction(+:sum) schedule(dynamic, 4)
for (i = 0 ; i <= n ; ++i)
	sum += f(i);
/* Chunksize is 1 by default. */
/* Each time a threads finishes, it requests another chunk in the iteration. */

/* Guided scheduling */
sum = 0.0;
#pragma omp parallel for num_threads(thread_count) reduction(+:sum) schedule(guided, 4)
for (i = 0 ; i <= n ; ++i)
	sum += f(i);
/* Chunksize is 1 by default. */
/* Each time a threads finishes, it requests another chunk in the iteration. But the size of the new chunk decreases. The first chunk is s = n / thread_count, the second s/2, the third s/4, ... and the last is 1. */
/* If chunksize is set, size of chunk decreases down to chunksize (so the decrease rate is slower). The very last chunk can however be smaller than chunksize. */

/* Overhead.
   There's is some overhead when scheduling. Here's the list of scheduling modes sorted in increasing order of overhead size:
   static
   dynamic
   guided
 */

/* Runtime scheduling */
sum = 0.0;
#pragma omp parallel for num_threads(thread_count) reduction(+:sum) schedule(runtime)
for (i = 0 ; i <= n ; ++i)
	sum += f(i);
/* Scheduled is controled by setting OMP_SCHEDULE env var to one of the following values:
   static,n
   dynamic,n
   guided,n
   where n is the chunksize
 */
======================== OpenMP/scope.txt
/* vi: se ft=C : */

/* Variable scope can be declared inside parallel directive. */
#pragma omp parallel ... reduction(+:sum)
#pragma omp parallel ... private(i)
#pragma omp parallel ... shared(n)

/* Variable scope declaration can be omited. By default is variable is shared, except for the incremented variable of the for loop in a parallel for directive, which is private. */

/* Variable scope can be forced to be declared with the default(none) option. */
#pragma omp parallel ... default(none)
======================== OpenMP/threads number.txt
/* vi: se ft=C : */

#include <omp.h>

/* To declare the number of threads and run them (from the same one function) */
#pragma omp parallel num_threads(n)

/* To get number of threads and thread index from inside thread */
int p_i = omp_get_thread_num();
int p = omp_get_num_threads();
======================== Pthreads/Pthreads.txt
POSIX threads are only available on UNIX-like OSes: Linux, MacOS-X, Solaris, HPUX, ...
======================== Pthreads/barrier.txt
/* vi: se ft=C: */

/* CONDITION VARIABLE */
int counter = 0;
pthread_mutex_t mutex;
pthread_cond_t  cond_var;
/* ... */
void* mythread() {
	/*...*/

	/* Barrier */
	pthread_mutex_lock(&mutex);
	++counter;
	if (counter == thread_count) {
		counter = 0;
		pthread_cond_broadcast(&cond_var);
	}
	else
		while (pthread_cond_wait(&cond_var, &mutex) != 0); /* it is possible that pthread_cond_wait returns, due to other events that a call to pthread_cond_broadcast or pthread_cond_signal ; in which case the returned value will be nonzero. */
	pthread_mutex_unlock(&mutex);

	/*...*/
}

/* SEMAPHORES
   Main issue :
     _ counter variable can't be reused in second barrier.
          explanation: 2 threads 0 and 1. Thread 0 reaches sem_wait(&barrier_sem) in else clause,
	               but is idle by OS. Meanwhile thread 1 passes if clause and reaches the
		       second barrier. Since counter == 0, thread 1 goes to else clause of the
		       second barrier and passes sem_wait(&barrier_sem) function which decrements
		       semaphore. When thread 0 awaken, it blocks in sem_wait(&barrier_sem) since
		       semaphore is 0.
 */
int counter = 0; /* counter of the number threads that have reached the barrier */
sem_t count_sem = 1;
sem_t barrier_sem = 0;
/* ... */
void* mythread() {
	/*...*/

	/* Barrier */
	sem_wait(&count_sem);
	if (counter == thread_count - 1) { /* ast thread reaches this section of code */
		counter = 0;
		sem_post(&count_sem);
		for (int j = 0 ; j < thread_count - 1 ; ++j)
			sem_post(&barrier_sem);
	}
	else { /* all threads but last one reach this section of code */
		++counter;
		sem_post(&count_sem);
		sem_wait(&barrier_sem); /* all threads but last one block here */
	}

	/*...*/
}

/* BUSY-WAITING AND A MUTEX
   The main issues are :
     _ we need one counter variable for each barrier ;
     _ CPU usage due to busy-waiting loop.
 */
int counter = 0; /* counter of the number threads that have reached the barrier */
int thread_count; /* total number of threads */
pthread_mutex_t barrier_mutex;
/* ... */
void* mythread() {
	/*...*/

	/* Barrier */
	pthread_mutex_lock(&barrier_mutex);
	++counter;
	pthread_mutex_unlock(&barrier_mutex);
	while (counter < thread_count);

	/*...*/
}
======================== Pthreads/compiling.txt
gcc -o myprog myprog.c -lpthread
======================== Pthreads/condition variable.txt
/* vi: se ft=C: */

/* create */
pthread_cond_t condition_var;
if (pthread_cond_init(&condition_var, NULL/*attributes*/))
	/* an error occured */;

/* destroy */
if (pthread_cond_destroy(&condition_var))
	/* an error occured */;

/* unblock one of the blocked thread */
if (pthread_cond_signal(&condition_var))
	/* an error occured */;

/* unblock all the blocked threads */
if (pthread_cond_broadcast(&condition_var))
	/* an error occured */;

/* block on condition variable */
if (pthread_cond_wait(&condition_var, &mutex)) /* will unlock mutex, block on condition variable and then relock mutex */
	/* an error occured */;
/* pthread_cond_wait() implements the following sequence of calls :
   pthread_mutex_unlock(&mutex);
   wait_on_signal(&cond_var);
   pthread_mutex_lock(&mutex);
 */
======================== Pthreads/creation.txt
/* vi: se ft=C: */

#include <pthread.h>

/* create thread */
Data      *data;
pthread_t thread;
if (pthread_create(&thread, NULL, mythread, (void*)data))
	/* an error occured */;

/* close thread */
if (pthread_join(thread, NULL))
	/* an error occured */;

/* the thread */
void* mythread(void *data) {
	Data *mydata = (Data*)data;
	/* ... */
	return NULL;
}

/* get return value of thread */
void *returned_value;
pthread_join(thread, &returned_value);
======================== Pthreads/mutex.txt
/* vi: se ft=C: */

/* create a mutex */
pthread_mutex_t mutex;
if (pthread_mutex_init(&mutex, NULL))
	/* an error occured */;

/* destroy a mutex */
if (pthread_mutex_destroy(&mutex))
	/* an error occured */;

/* lock a mutex */
if (pthread_mutex_lock(&mutex))
	/* an error occured */;

/* unlock a mutex */
if (pthread_mutex_unlock(&mutex))
	/* an error occured */;
======================== Pthreads/read-write lock.txt
/* vi: se ft=C: */

/* A read/write lock is like a mutex, but that can be locked either for read access
   or for write access.
   It used when accessing complex objects that aren't thread safe.
 */

/* create */
pthread_rwlock_t *rwlock;
if (pthread_rwlock_init(&rwlock, NULL))
	/* an error occured */;

/* destroy */
if (pthread_rwlock_destroy(&rwlock))
	/* an error occured */;

/* lock for reading */
if (pthread_rwlock_rdlock(&rwlock))
	/* an error occured */;

/* lock for writing */
if (pthread_rwlock_wrlock(&rwlock))
	/* an error occured */;

/* unlock */
if (pthread_rwlock_unlock(&rwlock))
	/* an error occured */;
======================== Pthreads/semaphore.txt
/* vi: se ft=C: */

#include <semaphore.h>

/* create */
sem_t s;
if (sem_init(&s, 0, 1)) /* initialize semaphore s to 1 */
	/* an error occured */;

/* destroy */
if (sem_destroy(&s))
	/* an error occured */;

/* increment */
if (sem_post(&s))
	/* an error occured */;

/* Wait for semaphore to be non-null.
   sem_wait always return 0.
 */
sem_wait(&s); /* after the call, s is decremented */

/* Non-blocking wait */
if (sem_trywait(&s) == EAGAIN)
	/* wait failed, semaphore was null. */

/* Semaphore partagés entre processus
   Certaines implémentations permettent de partager un sémaphore
   entre plusieurs processus. Il faut pour cela passer une valeur non
   nulle pour le 2ème paramètre de la fonction sem_init.
 */
sem_init(&s, 1, 10);
