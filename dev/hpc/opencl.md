# OpenCL
<!-- vimvars: b:markdown_embedded_syntax={'sh':'','c':''} -->

Framework for heterogeneous platforms (CPU, GPU, DSP, FPGA, ...).

Maintained by the Khronos Group.
<http://www.khronos.org/registry/cl/>

Portability is assured, but not performance (as of today).

An OpenCL/C++ library is available (not finalized).

## OpenCL model

One host, several devices.
Each device contains several computing units (CU).
The CPUs are seen as devices, as GPUs.
In one CPU: CU = core, 1 PE/CU or n PEs/CU where n is the SIMD width.
PE = Processing Element

### NDRange

NDRange <=> grid & blocks de CUDA.
grid   --> Global Size
block  --> Work group
thread --> Work item
Global size in 2D: `Gx` x `Gy`.
Work group size in 2D: `Sx` x `Sy`.
Attention ! `Gx` and `Gy` are multiples of `Sx` and `Sy`.
So `Gx % Sx == 0 && Gy % Sy == 0`.
==> number of work groups: `Wx = Gx / Sx` and `Wy = Gy / Sy`.

Work items inside a work group can share *local memory* and *synchronize*.

### Command queue

Context = one or more devices, device memory, one or more command queues.
All commands to a device are submitted to a command queue.

## Compiling

### Linux

```sh
OCL=/usr/local/intel_ocl/usr
OCLINC=$(OCL)/include
OCLLIB=$(OCL)/lib64
gcc ... -L$(OCLLIB) -lOpenCL
gcc ... -I$(OCLINC)
```

Add the following include:
```c
#include <CL/cl.h>
```

### MACOS-X

Compile normally.
```c
#include <OpenCL/opencl.h>
```

Link with:
```sh
gcc ... -framework OpenCL
```

## OpenCL Error codes

Error codes are defined inside `cl.h`.

Under MacOS-X, OpenCL is installed with Xcode.
Look for `cl.h` under `/Applications/Xcode.app`, it should be a path like the following one:
`/Applications/Xcode.app/Contents//Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.7.sdk/System/Library/Frameworks/OpenCL.framework/Versions/A/Headers/cl.h`

## OpenCL vector data types

OpenCL defines vector types:
```c
cl_char4
cl_int4
cl_float4
cl_double4	/* Optional. */
cl_half8 /* An half float-point vector of 8 elements. Optional. */
/* ... */
```
Available sizes are 2, 3, 4, 8, and 16.

<n> is platform dependent.
The use of vectorial types is essential in order to use the material in an optimal manner.
The C arrays on the host stay unchanged.
The arrays are created on the device, with the vectorial type. Thus there is a change in the array size, since each element is a mini-vector.

On the MIC (Xeon Phi), OpenCl compiler optimizes automatically for the vector unit. So vector data types must not be used, since it will go against the compiler work. However, on a CPU it may be useful (but why does the compiler not do it automatically in this case ?).

## OpenCL memory qualifiers

Qualifier    | Description
------------ | ---------------------------------------
`__private`  | Device registers for a work item. When there are no more available, then global memory is used, and memory latency access can arise.
`__local`    | Device group memory. Shared within a group.
`__global`   | Device global memory.
`__constant` | Device constant memory (in fact a part of the global memory).

## OpenCL synchronization

OpenCL memory fence model is more refined that CUDA one. It allows synchronization on read or write accesses. However few hardware implements this feature (Atomics do ... What is Atomics? A hardware card, a manufacturer, a compiler, ...?).
