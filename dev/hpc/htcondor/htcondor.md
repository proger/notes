# HTCondor

 * [HTCondor](https://research.cs.wisc.edu/htcondor/).
 * [Installation on Linux](https://htcondor.readthedocs.io/en/latest/getting-htcondor/install-linux-as-root.html).

HTCondor can be installed on Linux, Windows and MacOS.

Get status:
```sh
condor_status
```

Print queue:
```sh
condor_q
```
