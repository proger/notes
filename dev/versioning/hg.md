# hg (Mercurial)

To get log history display as a tree,
first add the following lines inside `$HOME/.hgrc`:
```
[extensions]
graphlog =
```
Then call the command glog:
```bash
hg glog
```
