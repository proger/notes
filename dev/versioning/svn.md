# svn (Subversion)

Get help:
```bash
svn help some.command
```

Get source:
```bash
svn ... file:///var/svn/newrepos/some/project ...
svn ... svn+ssh://pierrick@teddy/svn/pierrick/newproject ...
```

Username:
```bash
svn ... --username <login> https://...
```

Create new repository:
```bash
svnadmin create /var/svn/newrepos
```

Import whole tree:
```bash
svn import mytree file:///var/svn/newrepos/some/project -m "Initial import"
svn import mytree svn+ssh://pierrick@teddy/svn/pierrick/newproject -m "Initial import"
```

List:
```bash
svn list file:///var/svn/newrepos/some/project
svn list svn+ssh://pierrick@teddy/svn/pierrick
```

Check out:
```bash
svn checkout http://svn.collab.net/repos/svn/trunk
svn checkout http://svn.collab.net/repos/svn/trunk subv
```

Update:
```bash
svn udpate
```

Add:
```bash
svn add <file>
```

Remove:
```bash
svn delete <file>
```

Copy:
```bash
svn copy <file1> <file2>
```

Move:
```bash
svn move <file1> <file2>
```

Mkdir (short for mkdir + svn add):
```bash
svn mkdir <dir>
```

See changes:
```bash
svn status
```

See diff:
```bash
svn diff
```

Ignoring files & directories:
```bash
svn propset svn:ignore <dirname> .
```

Editing ignore file:
```bash
svn propedit svn:ignore .
```

Undo changes:
```bash
svn revert <file>
```
Works also to cancel file addition or deletion.

Commit:
```bash
svn commit -m "blabla"
```

Resolve conflict:
```bash
svn resolve --accept theirs-full <myfile>
```
Accept action: postpone, base, mine-full, theirs-full, edit, and launch.
```bash
svn resolve --accept working <myfile> # when solving Tree Conflicts (two persons added the same file, or a file was update by one person and deleted by another).
```

External
Allows to make reference from a directory to another svn repository
```bash
svn propget svn:externals calc
```

Exporting a clean tree (without SVN binding):
```bash
svn export ...
```

## SVN Server

MacOS-X:
```bash
launchctl
load org.tigris.subversion.svnserve
start org.tigris.subversion.svnserve
```
<ctrl-D> to get out of launchctl
