CREATE
======

Create a repository:
```bash
cvs -d /usr/local/cvsroot init
```

USING SSH
=========

```bash
export CVS_RSH=ssh
cvs -d :ext:user@host:/path/to/cvs/dir ...
```

MODULES
=======

Create a module:
```bash
mkdir new-dir
cd new-dir
cvs import -m "Imported sources" new-dir TeddySoft start
cd ..
cvs co new-dir
```

command line explained :
`cvs import -m "Imported sources" <dir name in CVS> <vendor tag> <release tag>`.

List modules:
```bash
cvs co -c
```

example of modules file:
```
winscripts -d scripts scripts/windows
winprog &winscripts
entreprise entreprise &perl-lib &build-system
tools tools &perl-lib &build-system
```

ADMINISTRATIVE FILES
====================

```bash
cvs co CVSROOT
```

DEVELOPMENT GROUP
=================

Use a development group for all cvs users:
```bash
# create a group named "devel".
# add all users to this group
# change group of all files in cvs root directory, including cvs root directory itself. If cvs root dir is "/cvs":
chgrp devel /cvs
chgrp -R devel /cvs
chmod g+s /cvs # set sid on cvs root directory.
find /cvs -type d -exec chmod g+s {} \; # set sid on all directories of cvs root directory.
```

CHECKOUT
========

```bash
cvs co -P module-name
```
`-P` : prune empty directories

For a branch:
```bash
cvs co -P -r branch-name module-name
```

Specifying directory:
```bash
cvs co -P -d dir module-name
```

EXPORT
======

Get a copy of sources, without CVS admin dirs:
```bash
cvs export my_module
```

DIFF
====

```bash
cvs diff -D now my_file
```

UPDATE
======

```bash
cvs -q up -dP
```

Option | Description
------ | --------------------------------------------------------
`-q`   | Quiet
`-d`   | Directories (create new directories)
`-P`   | Prune (empty directories)
`-C`   | Overwrite local files with clean copies from repository

Date format:

 * `-D "1972-09-24"`
 * `-D "1972-09-24 20:05"`
 * `-D "24 Sep 1972 20:05"`
 * `-D "24 Sep"`
 * `-D "1 hour ago"`
 * `-D now`

ADD TREE STRUCTURE
==================

```bash
find . -name CVS -prune -o -type d -print | xargs cvs add 
```
then type, 
```bash
find . -name CVS -prune -o -type f -print | xargs cvs add 
```

CVSIGNORE
=========

To always ignore certain files, create a `CVSROOT/cvsignore` file.
`.cvsignore` should only be used for ignoring some special local files.
filenames containing spaces don't work. Workaround: use wildcard (? or `*`) to indicate spaces.

REMOVE
======

```bash
cvs remove my_file
```

HOW TO UNDO A PREVIOUS CHECKIN
==============================

Undo all changes made between revision 1.3 and 1.5 for "file", if file has been removed it will re-created as it was in version 1.3:
```bash
cvs update -j 1.5 -j 1.3 myfile
```
or
```bash
cvs update -j HEAD -j 1.2 myfile
```

TAG
===

```bash
cvs tag -c new-tag-name
```
`-c` check if files are locally modified, and abort tagging if this is the case.

Tagging the repository by date or revision/tag:
```bash
cvs rtag -r 1.4 new-tag-name
cvs rtag -D "2007-11-10" new-tag-name
```
`-f` : use most recent file if no corresponding date or revision is found for a file

Removing a tag:
```bash
cvs rtag -d tag-name
```

Move a tag (from an old revision/tag to a new one):
```bash
cvs tag -r 1.6 -F new-tag-name
```

Rename a tag:
```bash
cvs rtag -r old-name new-tag-name
cvs rtag -d old-name
```

BRANCH
======

Create a branch from a local working copy:
```bash
cvs tag -b branch-name
```

Create a branch directly from the repository:
```bash
cvs rtag -b -r existing-name branch-name module-name
```
or
```bash
cvs rtag -b -D "24 Sep" branch-name module-name
```

Accessing a branch by updating the local working copy
```bash
cvs update -r branch-name
```

Accessing a branch by creating a new copy:
```bash
cvs co -r branch-name module-name
```

Checking on what branch we are:
```bash
cvs status -v myfiles
```
Then look for the field "Sticky Tag"

Merging branches:
```bash
cvs update -j branch-name
```

Merging a second time from the same branch:
```bash
cvs update -j 1.2.2.2 -j branch-name                            # merge differences from revision 1.2.2.2 on branch branch-name
cvs update -j branch-name:yesterday -j branch-name              # merge differences since yesterday
cvs update -j merged_from_branch-name_to_trunk -j branch-name   # if you set a tag on branch branch-name when merging the first time
```

ERRORS
======

cannot commit with sticky date for file 
---------------------------------------

When you check out with a specific date the files are marked as belonging to  that date. It's a sticky tag.
Before committing you must first remove those sticky tags.
```bash
cvs up -A 
```

binary file:
```bash
cvs add -kb file # add a binary file, -kb removes keyword substitution
cvs log file # # to see if a file is in binary mode (look at "keyword substitution:" line. If value is 'kv', then it's not in binary mode, if it is 'b' then the mode is binary)
```

To make automatic mode for files, edit `cvswrappers` file.

In case of an error, to change a file to binary mode:
```bash
cvs admin -kb file
cvs update -A file
```
