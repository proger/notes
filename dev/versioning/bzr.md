# bzr (Bazaar)

To clone a git repository
```bash
bzr branch git://git.gnome.org/gnome-specimen
```

Getting stored identity:
```bash
bzr whoami
```

Setting identity:
```bash
bzr whoami "Pierrick Roger Mele <pierrick.rogermele@cea.fr>"
```

Init a shared repository:
```bash
bzr init-repo myrepos
```

Init a repository tree:
```bash
bzr init myrepos/trunk
```

Bazaar doesn't support submodules. However Bazaar has been working on "nested trees" concept. It's still under development.
