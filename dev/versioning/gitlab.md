# GitLab

 * [GitLab CI/CD](https://docs.gitlab.com/ee/ci/).

## Install

 * [Install self-managed GitLab](https://about.gitlab.com/install/).
 * [Install GitLab on Ubuntu 22.04](https://linuxhint.com/install-gitlab-ubuntu-22-04/).

## CLI

 * [GitLab CLI - glab](https://docs.gitlab.com/ee/integration/glab/).
 * [glab](https://gitlab.com/gitlab-org/cli/).

## API

Download project archive: <https://gitlab.com/api/v4/projects/dbapis%2Fr-sched/repository/archive.tar.bz2>

## Badges

 * [Badges](https://docs.gitlab.com/ee/user/project/badges.html).
 * [Adding Custom badges to Gitlab...](https://medium.com/@iffi33/adding-custom-badges-to-gitlab-a9af8e3f3569).

## CI

Using a private project as submodule:
 * [Using Git submodules with GitLab CI/CD](https://docs.gitlab.com/ee/ci/git_submodules.html#configure-the-gitmodules-file).
 * [How do I pass credentials to pull a submodule in a Gitlab CI script?](https://stackoverflow.com/questions/58019082/how-do-i-pass-credentials-to-pull-a-submodule-in-a-gitlab-ci-script).
 * [Using the Dependency Proxy to improve your pipelines](https://about.gitlab.com/blog/2020/12/15/dependency-proxy-updates/).

 * [Predefined variables reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

 * [Pipeline architecture](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html).

 * [Caching in GitLab CI/CD](https://docs.gitlab.com/ee/ci/caching/). Cache downloaded files between jobs.
 * [GitLab CI: Cache and Artifacts explained by example](https://dev.to/drakulavich/gitlab-ci-cache-and-artifacts-explained-by-example-2opi).
 * [artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifacts).
   + Allow to publish archives to download in the GitLab UI.
   + Allow to transmit files to following jobs.

A GitLab CI to run slurm jobs on a cluster:
 * [ECP CI](https://ecp-ci.gitlab.io/index.html).
   + [Administration](https://ecp-ci.gitlab.io/docs/admin.html).
   + [MPI Quick Start Tutorial](https://ecp-ci.gitlab.io/docs/tutorial/quickstart.html).
   + [HPC Batch Executor](https://ecp-ci.gitlab.io/docs/ci-users/ci-batch.html).
   + [Exascale Computing Project (ECP)](https://www.exascaleproject.org/about/).

Default stages are `.pre`, `build`, `test`, `deploy`, `.post`.
To add a new stage, use the `stages` clause:
```yaml
stages:
    - check
    - test
```

Run code in `after_script` section depending on job success or failure:
```yaml
myjob:
  stage: test
  script:
    - make test
  after_script:
    - [[ $CI_JOB_STATUS == failed ]] && ...
    - [[ $CI_JOB_STATUS == success ]] && ...
    - [[ $CI_JOB_STATUS == canceled ]] && ...
```

Enable use of Git submodules:
```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
```

Extraction of coverage percentage from the build output, inside a job:
```yaml
  coverage: '/Code coverage: \d+(?:\.\d+)?/'
```
Add corresponding badge inside the README:
```md
![coverage](https://gitlab.com/dbapis/r-sched/badges/release/coverage.svg?ignore_skipped=true).
```

Add CI pipeline badge into README:
```md
![pipeline](https://gitlab.com/dbapis/r-sched/badges/release/pipeline.svg?ignore_skipped=true)
```

### Container registry

Go to `Deploy` -> `Container Registry`.

On local computer:
```sh
docker login registry.gitlab.com
```
Needs a PAT (Private Access Token) when Two-factors authentication is enabled.
Use the token name as user name, and the token as password.

## Private Access Tokens (PAT)

Go to [Personal Access Tokens ](https://gitlab.com/-/user_settings/personal_access_tokens).
All tokens are listed here, but their key is not listed. The key is given at
the creation time, and we must kept it in a safe place.
The key is used to authenticate, alongside the login, in replacement of the
password.

## Quick actions

 * [Quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html).

Mark an issue as a duplicate of another:
```
/duplicate #123
```

## Pages

 * [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
