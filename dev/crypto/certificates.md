# Certificates

 * [SSH vs. X.509 Certificates](https://smallstep.com/blog/ssh-vs-x509-certificates/).
 * [Everything you should know about certificates and PKI but are too afraid to ask](https://smallstep.com/blog/everything-pki/).
