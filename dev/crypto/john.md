# John the Ripper

 * [john](https://github.com/openwall/john).

Password cracker.

For cracking a pdf:
```sh
pdf2john.pl myfile.pdf >myfile.hash
john myfile.hash
john --show myfile.hash
```
