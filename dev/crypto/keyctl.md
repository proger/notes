# keyctl

Install on Debian:
```sh
apt-get install keyutils
```

List keys of keyrings:
```sh
keyctl show
```
