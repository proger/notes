# gpg2

`gpg` is 1.0 version with old encryption algorith.
`gpg2` is the latest version.

Install:
```sh
sudo apt install gnupg2
```

Import a public key:
```sh
gpg2 --import mykey.asc
```

List keys:
```sh
gpg2 --list-keys
```

Set trust level for a key:
```sh
gpg2 --edit-key mykey
```
mykey: email address in general
Then type `trust` and choose level.

Print information about public key file:
```sh
gpg2 --show-keys mykey.file
```

Encrypt a single file with a password:
```sh
gpg2 -c myfile
```
It produces an encrypted file named `myfile.gpg`.
To decrypt the file, type:
```sh
gpg2 myfile.gpg
```
