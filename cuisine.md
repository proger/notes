# Cuisine

## Boissons

 * [Bubble tea](http://www.thekitchn.com/how-to-make-boba-and-bubble-tea-98067).

 * [Les Thés Terre de Ciel](https://www.the-puerh.com/fr/index.html). Thés Pu Erh.
 * [Idées reçues et légendes a propos du thé puerh](http://www.puerh.fr/article/idees_recues_legendes_the_puerh.htm).

### Vin

 * [Guide des régions & appellations](https://avis-vin.lefigaro.fr/connaitre-deguster/tout-savoir-sur-le-vin/guide-des-regions-et-des-appellations).
 * [Vin chaud d’Alsace](http://www.marmiton.org/recettes/recette_vin-chaud-aux-epices-alsace_12995.aspx).

 * [Découvrez toutes mes recettes à base de Pineau des Charentes](https://www.papillesetpupilles.fr/recettes/pineau-des-charentes/).

Alsace :
 * Gewurztraminer Grand Cru, Kitterlé 2016, Domaines Schlumberger.
 * Gewurztraminer, Vendange tardive 2017, Domaines Schlumberger. ++
 
Bordeaux :
 * Médoc :
  + Saint-Estèphe. 2013, rouge.

Bourgogne :
 * Chassagne-Montrachet, rouge, 2017, Maison Louis Latour. +++
 * Chassagne-Montrachet, blanc, 2017, Maison Louis Latour.
 * Dégustation chez Moillard-Grivot le dimanche 29 août 2021 :
  + Meursault 2017
  + Chassagne-Montrachet Premier Cru 2016 - Les Embazées
  + Gevrey-Chambertin 2018
  + Beaune Premier Cru 2017 - Les Épenottes
  + Nuits-Saint-Georges Premier Cru 2016 - Les Damodes
  + Vosne Romanée Premier Cru 2016 - Les Suchots
  + Corton Grand Cru 2015 - Les Grandes Lolières

Champagne :
 * [Champagne Billecart-Salmon](https://www.champagne-billecart.fr/). Vintage 2009 +++, acheté en 2021 à Langres. 

Irpinia :
 * "Quintodecimo, vignaioli in Mirabella Eclano", Vin Terra d’Eclano 2013, Aglianico (vitigno).
 * Joaquin Aziende Agricole Srl, Fiano di Avellino DOCG “Vino della Stella” 2014 - Joaquin
 * Coda di Volpe (o Caprettone), sépage blanc. Vesuvio e Irpinia.

Loire :
 * [Vins du Val de Loire](https://www.vinsvaldeloire.fr/fr).

Lombardia :
 * Franciacorta, Contadi Castaldi (en déc. 2018), Rosé, DOCG, Italie, Brescia, pétillant.

Toscana :
 * Brunello di Montalcino DOCG, rosso, Siena, Toscana.

### Thé

正山小种
Lapsang souchong, thé fumé, Fujian.

#### Recette du thé nordique

Thé noir de Chinet  45%
Gingembre           22%
Cannelle            12%
Graine de cardamome 10%
Chili                5%
Clou de girofle      4%
Argousier            2%


## Boulettes et quenelles

 * [Polpette di pane (boulettes de pain)](https://www.cuisine-italienne.eu/2016/01/polpette-di-pane-boulettes-de-pain/).

 * [Canederli alla Tirolese (Knödel)](http://ricette.giallozafferano.it/Canederli-alla-Tirolese-Knodel.html).
   + <https://www.youtube.com/watch?v=x-k788Hybbo>.

## Céréales & co

### Quinoa

 * [SALADE DE QUINOA, FÈVES, BROCOLIS, AMANDES ET NOIX](https://www.fashioncooking.fr/2015/07/salade-quinoa-feves-brocolis/).

 * [Quinoa aux petits légumes](https://www.signebarbara.com/2012/09/quinoa-aux-petits-legumes.html).

### Millet

 * [Millet au gruyère](https://www.marmiton.org/recettes/recette_millet-au-gruyere_14312.aspx).

### Polenta

 * [POLENTA AL FORNO](https://blog.giallozafferano.it/allacciateilgrembiule/polenta-al-forno/).
 * [Crostini di polenta ripassati in padella](https://blog.giallozafferano.it/tmm/crostini-di-polenta-ripassati-in-padella/).

## Crêpes, gaufres, galettes, beignets

 * [Tempura - How to Make The Best Tempura 天ぷら](https://www.justonecookbook.com/tempura-recipe/).

 * Harcha :
   + [Harcha (galette de semoule fine)](https://www.marmiton.org/recettes/recette_harcha-galette-de-semoule-fine_24290.aspx).
   + [Crêpes marocaines](https://www.marmiton.org/recettes/recette_crepes-marocaines_11695.aspx).
   + [Harcha nouvelle façon](https://www.marmiton.org/recettes/recette_harcha-nouvelle-facon_63873.aspx).

 * [batbout](https://www.marmiton.org/recettes/recette_batbout_335366.aspx).

 * [Galettes de pommes de terre de Pontivy](https://www.marmiton.org/recettes/recette_galettes-de-pommes-de-terre-de-pontivy_71644.aspx).
 
 * [Crumpets](https://www.bbcgoodfood.com/recipes/crumpets).

 * [Gaufres à la courgette, emmental et lardons](http://www.marmiton.org/recettes/recette_gaufres-a-la-courgette-emmental-et-lardons_229534.aspx).
 * [Gaufres de courgettes au saumon fumé](http://www.marmiton.org/recettes/recette_gaufres-de-courgettes-au-saumon-fume_73980.aspx).

 * [Pane injera](https://ricette.giallozafferano.it/Pane-injera.html). Etiopia.

 * [Cheese Naans](https://www.youtube.com/watch?v=RLQJKNnyb7o).

 * [Pitas (pains grecs à garnir)](https://www.marmiton.org/recettes/recette_pitas-pains-grecs-a-garnir_38611.aspx).

 * [Tortillas de maïs](https://cuisine.journaldesfemmes.fr/recette/347916-tortillas-de-mais).

 * [Pâte à gaufre](https://www.marmiton.org/recettes/recette_pate-a-gaufre_82416.aspx).

 * [Crêpes à la farine de maïs](https://www.monfournil.fr/recettes/crepes-a-farine-de/).

 * [Pâte à crêpes mi sarrasin](http://www.marmiton.org/recettes/recette_galettes-au-ble-noir-de-sophie_51829.aspx).

 * [Piadine](http://ricette.giallozafferano.it/Piadina-Romagnola.html).

 * [DORAYAKI (JAPANESE RED BEAN PANCAKE)](https://www.justonecookbook.com/dorayaki-japanese-red-bean-pancake/).

### Crêpes de sarrasin

 * [Pâte à crêpes de sarrasin](http://www.recettes-bretonnes.fr/galette-bretonne/recette-galette-sarrasin.html).
 * [Pâte à galettes de blé noir (crêpes au sarrasin)](https://cuisine.journaldesfemmes.fr/recette/355724-pate-a-galettes-de-ble-noir-crepes-au-sarrasin).

### Crêpes de froment

 * [Pâte à crêpes simple](https://www.marmiton.org/recettes/recette_pate-a-crepes-simple_27121.aspx).

Ingrédients :
 - 300g d'œufs (1 œuf / 100g de farine)
 - 500g de farine.
 - 1 litre de lait.
 - 3 cuillères à soupe d'huile d'olive.
 - 1 verre de bière (optionnel).

Laisser reposer une heure au frigo.
## Épices

 * [Le safran en cuisine](https://espritdepays.com/gastronomie-terroirs-viticulture/produits-terroirs-perigord/les-produits-des-terroirs-le-safran-l-or-rouge-du-perigord/le-safran-en-cuisine).

## Fromages

Irpinia:
 * [Carmasciando](http://www.carmasciando.it/sitodef/).

## Fruits et préparations sucrés

 * [Châtaignes au four](https://www.marmiton.org/recettes/recette_chataignes-au-four_27423.aspx).

 * [Pâte d’azukis](https://www.justonecookbook.com/how-to-make-anko-red-bean-paste/).

 * [Poires au vin](http://www.marmiton.org/recettes/recette_poires-pochees-au-vin_30583.aspx).

 * [Pommes au four](https://www.marmiton.org/recettes/recette_pommes-au-four_20545.aspx).

 * [Tranches d'oranges confites](https://www.meilleurduchef.com/fr/recette/orange-confite.html).
 * [Orangettes facile](https://www.cuisineactuelle.fr/recettes/orangettes-191481).
 * [Comment faire des écorces confites maison ?](https://www.jujube-en-cuisine.fr/ecorces-doranges-confites/).

## Gâteaux, tartes, galettes, biscuits

 * [Poires au gingembre enivrées de vin rouge](https://www.lecreuset.fr/fr_FR/poires-au-gingembre-enivrees-de-vin-rouge/r0000000001117.html).

 * [Yaourts maison SANS yaourtière (à la casserole et au four ou à la cocotte)](https://www.marmiton.org/recettes/recette_yaourts-maison-sans-yaourtiere-a-la-casserole-et-au-four-ou-a-la-cocotte_24394.aspx).
 * [Yaourts au micro-ondes](https://www.mayaourtiere.com/yaourts-au-micro-ondes/).

 * [Pane di sapa](https://www.ricettedisardegna.it/pani-saba/).

 * [Vrai baba au rhum](https://www.marmiton.org/recettes/recette_vrai-baba-au-rhum_17689.aspx).

 * [Kougelhopf, kouglof](https://www.recettes-alsace.fr/kougelhopf-kouglof/).

 * [Pudding](https://www.marmiton.org/recettes/recette_le-pudding-de-quand-j-etais-p-tite_31801.aspx).
 * [Recette Pudding au pain](https://lacuisinedannie.20minutes.fr/recette-pudding-au-pain-283.html).
 * [Pain perdu](https://www.marmiton.org/recettes/recette_pain-perdu_10975.aspx).
 * [Pain perdu traditionnel](https://www.atelierdeschefs.fr/recettes/25933/pain-perdu-traditionnel/).

 * [Rhubarb crumble](https://www.bbcgoodfood.com/recipes/rhubarb-crumble).

 * [Galette des rois à la frangipane](http://www.marmiton.org/recettes/recette_galette-des-rois-a-la-frangipane_20147.aspx).
 * [Couronne des rois briochée](https://www.750g.com/couronne-des-rois-briochee-r32422.htm).

 * [Tourteau fromager](https://www.accueil-vendee.com/recettes/le-tourteau-fromager/).

 * [Gâteau aux noix classique](https://cuisine.journaldesfemmes.fr/recette/1009480-gateau-aux-noix).

### Recettes sans gluten

 * [Gâteau à la farine de châtaigne : la recette facile](https://cuisine.journaldesfemmes.fr/recette/316077-gateau-a-la-farine-de-chataigne).
 * [Moelleux à la farine de châtaigne, un gâteau ardéchois](https://www.myparisiankitchen.com/moelleux-a-la-farine-de-chataigne-un-gateau-ardechois/).

 * [Gâteau farine de riz](https://www.mapatisserie.net/gateau-farine-de-riz/). TO TRY
 * [Gâteau au yaourt et à la farine de riz très moelleux](https://www.mesrecettes.info/gateau-au-yaourt-et-a-la-farine-de-riz-tres-moelleux/). TO TRY

### Recettes sans gluten et sans lactose

 * [Cake sans gluten sans lactose](https://maviedecoeliaque.fr/cake-sans-gluten-sans-lactose/). TO TRY
 * [Gâteau aux amandes sans gluten sans lactose](https://www.cuisineaz.com/recettes/gateau-aux-amandes-sans-gluten-sans-lactose-99872.aspx). TO TRY
 
### À l'orange ou citron

 * [Torta con crema al limone](https://ricetta.it/torta-con-crema-al-limone).

 * [TORTA CAPRESE ALL ARANCIA](https://blog.giallozafferano.it/allacciateilgrembiule/torta-caprese-all-arancia/).
 * [Gâteau à l'orange rapide](http://www.marmiton.org/recettes/recette_gateau-a-l-orange-rapide_13489.aspx#d57983-p3).
 * [Tartelette à l'orange](https://www.atelierdeschefs.fr/fr/recette/18016-tartelette-a-l-orange.php).
 * [Pastiera napoletana](https://ricette.giallozafferano.it/Pastiera-napoletana.html).

### Biscuits

Frollini, Mulino Bianco:
 * 300g di farina di frumento integrale
 * 80g di zucchero
 * 80g di zucchero di canna
 * 130g di burro
 * 2 tuorli d'uovo
 * un pizzico di sale
 * una scorza di limone grattugiata 
 * Lascia riposare in frigorifero per 30 minuti.
 * In forno 180°C per 15 minuti.

 * [Comment cuire les biscuits : cinq astuces (températures, matériel…)](https://www.undejeunerdesoleil.com/2016/04/comment-cuire-biscuits-astuces-temperatures.html).
 * [Galettes bretonnes (nouvelle recette)](https://www.undejeunerdesoleil.com/2016/03/galettes-fines-bretonnes.html).
 * [Sablé breton : recette et astuces](https://www.undejeunerdesoleil.com/2013/06/sable-breton-recette-et-astuces.html).
 * [Galettes bretonnes, des biscuits addictifs](https://www.undejeunerdesoleil.com/2014/08/galettes-bretonnes-biscuits.html).
 * [Galettes sablées bretonnes véritables](https://www.marmiton.org/recettes/recette_veritables-galettes-sablees-bretonnes_28243.aspx).

 * [Sablés aux fruits confits](https://cuisine.journaldesfemmes.fr/recette/315684-sables-aux-fruits-confits).
 * [Biscuits sablés aux noisettes](https://www.marmiton.org/recettes/recette_biscuits-sables-aux-noisettes_26136.aspx).
  + huile: + de 80g, 100g ?
 * [Sablés au citron pour le thé](https://www.marmiton.org/recettes/recette_sables-au-citron-pour-le-the_56238.aspx). 120g d'huile.

 * [Biscuits allemands de Noël](https://www.cuisineaz.com/recettes/biscuits-allemands-de-noel-40065.aspx).
 * [Weihnachtsplätzchen (biscuits de Noël allemands)](https://www.marmiton.org/recettes/recette_weihnachtsplatzchen_315779.aspx).
 * [Étoiles de Noël](https://noel.tourisme-alsace.com/fr/idees-de-recettes-de-noel/pain-epices.html).
 * [Les Leckerli de Bâle](http://misstamkitchenette.com/les-leckerli-de-bale/#.Wj92CyOZOHo).
 * [Sablés de Noël (Ausstecherle)](https://www.marmiton.org/recettes/recette_sables-de-noel-ausstecherle_33325.aspx).
 * [Petits biscuits de Noël](https://www.marmiton.org/recettes/recette_petits-biscuits-de-noel_19726.aspx).

 * [Mom’s Soft Raisin Cookies](https://www.tasteofhome.com/recipes/mom-s-soft-raisin-cookies/).
   + [Cookies maison au chocolat](https://www.marmiton.org/recettes/recette_cookies-maison_86989.aspx).

 * [Biscuits amande citron](https://www.marmiton.org/recettes/recette_biscuit-amande-citron_55995.aspx).
 * [Macarons aux amandes (petits fours)](https://www.marmiton.org/recettes/recette_macarons-aux-amandes-petits-fours_19531.aspx).

 * [Anzac biscuits](https://www.bbcgoodfood.com/recipes/anzac-biscuits)
 * [Scones](https://www.bbc.co.uk/food/recipes/scones_1285).

 * [Financiers](https://www.marmiton.org/recettes/recette_financiers_13690.aspx).
 * [Madeleines à belle bosse](https://cuisine.journaldesfemmes.fr/recette/343692-madeleines).
 * [Madeleines parfumées avec une jolie bosse](https://www.marmiton.org/recettes/recette_madeleines-parfumees-avec-une-jolie-bosse_372523.aspx).
 * [Madeleines faciles](https://www.marmiton.org/recettes/recette_madeleines-faciles_17700.aspx).
  + 50g d'huile au lieu de 100g de beurre. 
  + 40g lait d'amande au lieu de lait
  + 5 min à 240°C
  + 10 min à 200°C pour madeleines normales
  + rajouter  encore 10 min à 200°C pour moules plus gros (petit plat ou moules mini-babas).

### Chocolat

 * [Brownies](https://www.marmiton.org/recettes/recette_brownies_16951.aspx).
 * [Fondant au chocolat](https://www.marmiton.org/recettes/recette_fondant-au-chocolat_15025.aspx).
 * [Moelleux au chocolat](https://www.marmiton.org/recettes/recette_moelleux-au-chocolat_17982.aspx).
 * [Gâteau simple au chocolat](https://www.cuisineaz.com/recettes/gateau-simple-au-chocolat-34206.aspx).
 * [Gâteau au chocolat fondant rapide](https://www.marmiton.org/recettes/recette_gateau-au-chocolat-fondant-rapide_166352.aspx).
 * [Galette poire-chocolat](https://cuisine.journaldesfemmes.fr/recette/320812-galette-poire-chocolat).

### Riz au lait, gâteaux de riz, semoule, flan, crèmes, etc

 * 1l de lait
 * 100g de riz
 * 80g de sucre (ou 100g ?)
 * parfum : zeste citron, zeste d'orange, café, extrait de framboise, gousse de vanille, extrait de vanille, ...

 * [Riz au lait de ma maman](https://www.marmiton.org/recettes/recette_riz-au-lait-de-ma-maman_20380.aspx).
 * [Riz au lait](https://lacuisinedannie.20minutes.fr/recette-riz-au-lait-165.html).

 * [Gâteau de semoule (à la casserole)](http://www.marmiton.org/recettes/recette_gateau-de-semoule-a-la-casserole_43864.aspx). Sans œufs.
 * [Gâteau de semoule](http://www.marmiton.org/recettes/recette_gateau-de-semoule_24969.aspx).
 * [Migliaccio](http://ricette.giallozafferano.it/Migliaccio.html).
 * [Migliaccio senza ricotta, ricetta facile](https://www.youtube.com/watch?v=BZJw_wZ6n2w).
 * [Mamounié (gâteau de semoule libanais)](https://www.marmiton.org/recettes/recette_mamounie-gateau-de-semoule-libanais_59693.aspx).

 * [Crème au caramel](https://www.marmiton.org/recettes/recette_creme-au-caramel_27061.aspx).

 * [Petites crèmes ultra légères à l'agar agar](https://cuisine.journaldesfemmes.fr/recette/347851-petites-cremes-ultra-legeres-a-l-agar-agar).
 * [Crèmes dessert faciles vanille et chocolat](https://www.marmiton.org/recettes/recette_cremes-desserts-faciles-vanille-et-chocolat_253835.aspx).
 * 2g agar agar pour 500ml de lait ==> TROP. Essayer 1g. Essayer aussi avec de l'amidon.

 * [Flan pâtissier traditionnel](https://www.marmiton.org/recettes/recette_flan-patissier-traditionnel_15330.aspx).
 * Flan :
  + pâte brisée ou feuilletée
  + 1l de lait
  + 100g fécule
  + 150g sucre
  + 2 œufs
  + parfum
  + Faire chauffer le lait avec le sucre
  + Mélanger la fécule avec les œufs
  + Lorsque le lait est bien chaud, éteindre et ajouter le parfum
  + Ajouter le lait chaud aux œufs
  + Verser sur la pâte dans le moule
  + 180°C 40 min ou 200°C 30-40 min ?

### Far, quatre-quarts et gâteaux bretons

 * [Far breton aux pruneaux](http://www.marmiton.org/recettes/recette_far-breton-aux-pruneaux_19101.aspx).
  + œufs    n (p grammes)   4    5    6
  + lait    p * 4         800 1000 1200
  + farine  p             240  300  360
  + sucre   p / 2         120  150  180
 * [Le far breton au blé noir / sarrasin](http://www.recettes-bretonnes.fr/gateaux-bretons/far-breton-ble-noir.html).
  + œufs    n (p grammes)   4    6
  + lait    p * 4        1000 1400
  + farine  p             240  360
  + sucre   p * 4 / 10    100  140
 * [Le far breton à la poêle ou farz pitilig](http://www.recettes-bretonnes.fr/gateaux-bretons/far-poele-farz-pitilig.html).

 * [Gâteau breton](https://www.marmiton.org/recettes/recette_gateau-breton_21004.aspx).

 * [Quatre-quarts](http://www.recettes-bretonnes.fr/gateaux-bretons/quatre-quarts-breton.html).
  + Variante avec du lait:
   - 3 œufs
   - même poids de sucre
   - même poids de farine
   - même poids de beurre + lait (1/3 beurre, 2/3 lait)
   - levure
   - sel ou beurre salé
   - parfum (vanille, citron, cacao, ...)

### Cheesecake et gâteaux au yaourt ou fromage blanc

 * [Gâteau au yaourt](http://www.marmiton.org/recettes/recette_gateau-au-yaourt_12719.aspx).
 * [Gâteau de ma grand-mère](https://www.marmiton.org/recettes/recette_gateau-de-ma-grand-mere_17731.aspx).
 * [Le Gâteau marbré au chocolat](https://www.mon-marche.fr/recette/le-gateau-marbre-au-chocolat).
 * [Gâteau grand-mère](https://cuisine.journaldesfemmes.fr/recette/344653-gateau-grand-mere).
  + 2 bols de farine
  + 1 bol de lait
  + 1 bol de sucre
  + 1 bol d'huile (un peu moins ? 3/4?)
  + 2, 3 ou 4 oeufs ?
  + levure
  + parfum
  + 180 C au four pendant 45 min.

 * [Gâteau au fromage blanc](https://www.ptitchef.com/recettes/dessert/gateau-au-fromage-blanc-super-facile-fid-1507834).

 * [Cheesecake aux framboises sans cuisson](https://www.cuisineaz.com/recettes/cheesecake-aux-framboises-sans-cuisson-80993.aspx).

 * [Japanese Cotton Sponge Cake](https://www.youtube.com/watch?v=PFUC4UZ8cNw).

 * [Käsekuchen (tarte au fromage blanc) à la rhubarbe](https://www.visit.alsace/recettes/tarte-au-fromage-blanc-a-la-rhubarbe/).

 * [Cake aux amandes et au yaourt](https://cuisine.journaldesfemmes.fr/recette/355897-cake-aux-amandes-et-au-yaourt).
  + Variante avec du lait :
   - 120g œufs
   - 120g poudre d’amandes
   - 120g farine
   - 80g sucre
   - Vanille
   - Levure
   - 60g huile
   - Lait 120g

### Potiron

 * [Pumpkin pie](https://www.simplyrecipes.com/recipes/suzannes_old_fashioned_pumpkin_pie/).
 * [Torta morbida alla zucca](https://ricette.giallozafferano.it/Torta-morbida-alla-zucca.html).

### Banane

 * [Gâteau Banane de Stéph](https://www.marmiton.org/recettes/recette_gateau-banane-de-steph_31190.aspx).
 * [Compote de bananes à la cannelle](https://cuisine.journaldesfemmes.fr/recette/318725-compote-de-bananes-a-la-cannelle).


### Pommes

 * [Apfel Strudel](http://ricette.giallozafferano.it/Strudel-di-mele.html).
 * [Gâteau aux pommes facile](https://www.marmiton.org/recettes/recette_gateau-aux-pommes-facile_13493.aspx).
 * [Clafoutis light aux pommes](https://www.marmiton.org/recettes/recette_clafoutis-light-aux-pommes_14926.aspx).

 * [Pommes au four](https://www.marmiton.org/recettes/recette_pommes-au-four_20545.aspx).

### Galettes

 * [Broyé du Poitou](http://www.marmiton.org/recettes/recette_broyer-du-poitou_53917.aspx).
 * [Galette retaise à l’angélique du Poitou-Charentes](https://odelices.ouest-france.fr/recette/galette-retaise-a-langelique-du-poitou-charentes-r3634/).

Galette à l'huile :
 * 250g farine
 * 125g sucre --> moins ?       utilisé: 110g
 * 125g d'huile --> 110g utilisé
 * 1 œuf pour la pâte
 * 1 sachet de sucre vanillé
 * 3g de sel
 * du lait s'il manque de liquide pour former une boule ?
 * 1 jaune pour la dorure --> remplacer par du lait ?
 * levure chimique ?  --> non

## Légumes, tubercules, légumineuses, algues
 
 * [List of Japanese soups and stews](https://en.wikipedia.org/wiki/List_of_Japanese_soups_and_stews).

 * [Curry de légumes, pistaches et poivre noir](https://www.lecreuset.fr/fr_FR/curry-de-legumes--pistaches-et-poivre-noir/r0000000000016.html).
 * [Recette indienne Légumes Korma en vidéo](http://www.pankaj-blog.com/article-recette-indienne-korma-video-72101596.html).

 * [Ratatouille](https://www.marmiton.org/recettes/recette_ratatouille_23223.aspx).

### Algues

 * [Kombu no Tsukudani](https://www.thespruceeats.com/kombu-no-tsukudani-2031442).

### Aubergines

 * [Authentic Chinese Eggplant Recipe](https://www.alphafoodie.com/chinese-eggplant-with-garlic-sauce/).

### Asperges

 * [Gratin d'asperges blanches au cantal](https://www.marmiton.org/recettes/recette_gratin-d-asperges-blanches-au-cantal_73404.aspx).

### Carottes

 * [Carottes Vichy](http://www.marmiton.org/recettes/recette_carottes-vichy_17717.aspx).
 * [Duo de carottes et pommes de terre rôties aux épices](http://www.epicetoutlacuisinededany.fr/2016/10/duo-de-carottes-et-pommes-de-terre-roti-aux-epices.html).
 * [Purée de carottes](https://www.marmiton.org/recettes/recette_puree-de-carottes_64095.aspx).

### Céleri branche

 * [Gratin de céleri branche à la sauce tomate](https://croquantfondantgourmand.com/gratin-de-celeri-branche-a-la-tomate/).

### Céleri rave

 * [Céleri rave rôti à l'ail](https://www.marmiton.org/recettes/recette_celeri-rave-roti-a-l-ail_39682.aspx).

### Champignons

 * [Champignons farcis](https://www.marmiton.org/recettes/recette_champignons-farcis_25004.aspx).

### Chou

 * [Chou campagnard](https://www.marmiton.org/recettes/recette_chou-campagnard_12832.aspx). Chou vert aux lardons.

### Chou-fleur

 * [Gratin de chou-fleur](https://www.marmiton.org/recettes/recette_gratin-de-chou-fleur_23112.aspx).
 * [Salade de chou-fleur rôti, grenade et pignons de pin](https://www.papillesetpupilles.fr/2020/05/salade-de-chou-fleur-roti-grenade-et-pignons-de-pin.html/).
 * [Velouté de chou-fleur](https://www.marmiton.org/recettes/recette_veloute-de-chou-fleur_28185.aspx).

### Courges

 * [Gratin de courge butternut](https://www.marmiton.org/recettes/recette_gratin-de-courge-butternut_34853.aspx).
 * [Velouté de potiron](http://www.marmiton.org/recettes/recette_veloute-de-potiron_18663.aspx).

### Courgettes

 * [Flans de courgettes](https://chefsimon.com/gourmets/chef-simon/recettes/flans-de-courgettes--3).

### Cresson

 * [Soupe de cresson](https://www.marmiton.org/recettes/recette_soupe-de-cresson_19991.aspx).

### Fenouils

 * [Fenouils braisés](https://www.marmiton.org/recettes/recette_fenouils-braises_35390.aspx).

### Haricots secs

 * [Soupe de haricots blancs et croutes de Parmesan](https://www.gastronomico.fr/soupe-haricots-blancs-parmesan/).
 * [Fagioli all'uccelletto](https://ricette.giallozafferano.it/Fagioli-all-uccelletto.html).
 * [Haricots blancs à la tomate](https://www.meilleurduchef.com/fr/recette/haricot-blanc-tomate.html).
 * [Coco de Paimpol](https://www.marmiton.org/recettes/recette_coco-de-paimpol_31642.aspx).

 * [Haricots Mungo](https://fr.wikihow.com/pr%C3%A9parer-les-haricots-mungo).

### Lentilles

 * lentilles: Mettre les lentilles avec 3 fois leur volume d’eau froide dans une casserole. Portez à ébullition et laissez cuire à petit feu et à couvert, pendant 10 à 15 minutes.

 * [ Lentilles Vertes du Puy](https://www.marmiton.org/recettes/recette_lentilles-vertes-du-puy_34542.aspx).
 * [Ragù di lenticchie](https://ricette.giallozafferano.it/Ragu-di-lenticchie.html).
 * [Dal tempering](https://thoughtsfromajoy.wordpress.com/2013/01/09/perfect-dal-is-all-about-tadka-baghar-vagharne-chonk-phodni-or-just-call-it-tempering/).
 * [Pasta e lenticchie](https://ricette.giallozafferano.it/Pasta-e-lenticchie.html).

### Navets

 * [Navets à l'orientale](https://cuisine.journaldesfemmes.fr/recette/347065-navets-a-l-orientale).
 * [Navets caramelisés](https://www.marmiton.org/recettes/recette_navets-caramelises_20708.aspx).

### Oignons

 * [Tartelette aux petits oignons](https://edithetsacuisine.fr/tartelette-petits-oignons-blancs-frais/).

### Pois cassés

 * [Pois cassés et Morteau](https://www.cuisinemaison.net/legumes/pois-casse-et-morteau-18950).
 * [Pois cassés au curry](https://www.marmiton.org/recettes/recette_pois-casses-au-curry_68500.aspx).

### Poivrons

 * [Poivrons farcis (végétarien)](https://www.marmiton.org/recettes/recette_poivrons-farcis-vegetarien_27175.aspx).
 * [Poivrons farcis végétariens](https://www.cuisineaz.com/recettes/poivrons-farcis-vegetariens-101329.aspx).

### Pommes de terre

 * pommes de terre à l’eau: Dans l’eau froide, entières, avec la peau, porter à ébullition. 45 minutes en tout pour des grosses pommes de terre.
 * [Pommes de terre sautées aux oignons](https://www.marmiton.org/recettes/recette_pommes-de-terre-sautees-aux-oignons_28418.aspx).
 * [Galettes de pommes de terre aux lardons](https://www.cuisineactuelle.fr/recettes/galettes-de-pommes-de-terre-aux-lardons-5980).
 * [Gratin Dauphinois](https://www.legratindauphinois.fr).
 * [Gateau di patate: la Ricetta originale del Gattò di patate](https://www.tavolartegusto.it/ricetta/gateau-di-patate/).
 * [Gateau di patate](https://ricette.giallozafferano.it/Gateau-di-patate.html).
 * [Pasta e patate](https://ricette.giallozafferano.it/Pasta-e-patate.html).
 * [Pasta e patate alla napoletana](https://ricette.giallozafferano.it/Pasta-e-patate-alla-napoletana.html).

### Topinambours

 * [Topinambours rôtis à l’ail et au thym](https://www.lolibox.fr/topinambours-rotis-ail-thym/).

 * Gratin de topinambour : idem gratin de chou-fleur. Faire cuire 10min les topinambours dans de l'eau bouillante. Les épulecher et les couper en morceaux.
## Pain, pâtes et viennoiseries

 * [Pâte feuilletée](http://www.marmiton.org/recettes/recette_pate-feuilletee_20642.aspx).

 * [Le Petit Boulanger](http://lepetitboulanger.com/index.htm).
 * [Fabrication du pain](https://fr.wikipedia.org/wiki/Fabrication_du_pain).
 * Techniques qui font le pain:
  + [Modes de fermentation](https://www.youtube.com/watch?v=Whc2JJnAFGU).
  + [Le Pointage pesage](https://www.youtube.com/watch?v=NX6dQzJXVoY).
  + [Le Façonnage](https://www.youtube.com/watch?v=PWjSa6xBwtw).
  + [Façonnages divers](https://www.youtube.com/watch?v=Et-0CkiOfR4).
  + [L'apprêt](https://www.youtube.com/watch?v=Ru562dfRFSg).
  + [La mise au four](https://www.youtube.com/watch?v=4NLKLZVLzQA).
  + [La Cuisson](https://www.youtube.com/watch?v=2q24hiVzPWE).
  + [Le Ressuage](https://www.youtube.com/watch?v=utkHgk5Z0YU).
  + [Le Divisage](https://www.youtube.com/watch?v=Ru2XLjXQfuE).
  + [Le Boulage Préformage](https://www.youtube.com/watch?v=zqsnz6XXqVs).
 * [Boulangerie Pas à pas](https://www.youtube.com/channel/UCyukUh-uOQJE6YqedFTkQlw):
  + [Pain facile](https://www.youtube.com/watch?v=GmgccO5yDOk).
  + [La Tourte Auvergnate](https://www.youtube.com/watch?v=1fHzga2ZL9o).
  + [Façonnage, tourne et mise au four.](https://www.youtube.com/watch?v=WdROz5L2n5Y).
  * [Pain de seigle au citron](https://www.youtube.com/watch?v=QhCXDNDY6w4).

 * [Comment réaliser son pain maison sans machine](https://madame.lefigaro.fr/cuisine/conseils-et-etapes-pour-realiser-son-pain-maison-sans-machine-101016-117222).
  + bien mélanger les farines s'il y en a plusieurs
  + poids eau = 60% poids farine.
  + sel
  + mélanger
  + pétrir
  + laisser reposer 30 minutes.
  + Levure. Compter 2,5 à 3 fois plus de levure fraîche que de levure sèche.
   - --> quantité de levure ?
   - 3-5 grammes levure sèche pour 300-450 grammes de farine.
  + Pour la levure sèche:
   - Mettre de l'eau à chauffer à 40°C dans le four.
   - Mélanger la levure sèche à un peu de cette eau pour former une pâte.
  + pétrissage (replier la pâte sur elle-même). --> temps ?
  + laisser lever 2:30-5:00 à 30-35°C. Pâte en boule, couvrir d'un torchon. 
  + dégazer la pâte
  + façonner
  + laisser reposer les pains 20 minutes le temps de préchauffer le four (~240°C).

 * [Focaccia](https://ricette.giallozafferano.it/Focaccia.html).
 * [Focaccia integrale](https://ricette.giallozafferano.it/Focaccia-integrale.html).

 * [Pane cafone](http://ricette.giallozafferano.it/Pane-cafone.html).
 * [Pane di semola rimacinata](http://www.misya.info/ricetta/pane-di-semola-rimacinata.htm).
 * [Pane mafalde siciliane](http://ricette.giallozafferano.it/Mafalde-siciliane.html).
 * [Pane di Altamura, Pane Pugliese, fatto in casa](https://www.chefstefanobarbato.com/ita/ricette/pane-di-altamura-pane-pugliese-fatto-in-casa/).
 * [Pain campagnard sans gluten](https://www.marmiton.org/recettes/recette_pain-campagnard-sans-gluten_95840.aspx). Pain à la farine de sarrasin.

 * [Arepas : petits pains à la farine de maïs](https://www.marciatack.fr/arepas-farine-mais/).

 * [Brioche](https://www.marmiton.org/recettes/recette_brioche-de-micheline-26eme-rencontre_40232.aspx).
 * [Baguette viennoise](http://www.marmiton.org/recettes/recette_baguette-viennoise_30706.aspx).

 * [Chaussons aux pommes](http://www.marmiton.org/recettes/recette_chausson-aux-pommes-maison_16812.aspx).
 * [Croissants](https://www.meilleurduchef.com/fr/recette/croissant-facile.html).
 * [Brioches (croissants italiens)](https://ricette.giallozafferano.it/Brioches.html).
 * [Croissants sfogliati: la ricetta per farli in casa](https://video.cookist.it/video/an/Xnna_-SwH3PFLOiT).
 * [Pains au lait](http://www.marmiton.org/recettes/recette_petits-pain-au-lait-maison_220199.aspx).

### À propos de la levure

100g farine -> 1g levure sèche (et 1g de sel ?)

8g levure sèche = 25g levure fraîche = 38ml levure liquide

3-5 grammes levure sèche pour 300-450 grammes de farine.

Levure sèche :
 * La diluer dans l'eau tiède. Attention pas plus de 45 degrés.
 * Mettre la levure dans un verre d'eau tiède et remuer, puis attendre 10 minutes.

### Pâte brisée

 * [Pâte brisée à l'huile d'olive](https://cuisine.journaldesfemmes.fr/recette/1013751-pate-brisee-a-l-huile-d-olive).

 * 200g farine
 * 50ml huile
 * 50ml eau
 * 3,5g sel

### Pâte à choux

 * [Pâte à choux sucré ou salé](https://www.marmiton.org/recettes/recette_pate-a-choux-sucre-ou-sale_21951.aspx).

 * 125g de farine
 * 1 pincée de sel
 * 20cl d'eau
 * 60g de beurre ou d'huile
 * 3 œufs

 1. Mettre dans une casserole, l'eau, la pincée de sel, le beurre ou l'huile.
 2. Faire bouillir le tout et y verser d'un seul coup toute la farine tamisée.
 3. Bien remuer à la spatule et la travailler environ 5 min à feu doux jusqu'à ce que la pâte se décolle des parois et forme une boule.
 4. Retirer du feu et laisser refroidir quelques minutes.
 5. Ajouter un à un les oeufs en travaillant énergiquement la pâte à chaque fois.
 6. Former les choux ou les éclairs à la cuillère ou à la poche à douille.
 7. Faire cuire au four à 180°C (thermostat 6), environ 15 min.

### Oatcakes

 * [Oatcakes](https://www.allrecipes.com/recipe/257958/oatcakes/).

Pour quatre galettes:

 * 100g son d'avoine
 * 30g T55
 * [2,40g sucre] --> pas la peine
 * 2,30g sel
 * [3g levure chimique] --> pas la peine
 * 20g d'huile
 * eau bouillante

Former une boule, et mettre au réfrigérateur pour 30 minutes.
Mettre le four à 190°C.
Étaler la pâte et découper des cercles.
Disposer sur une plaque huilée.
Enfourner pour 20-30 minutes. --> plutôt 20min. Même 15 minutes.

### Pain de mie

 * [Pain de mie facile](https://www.youtube.com/watch?v=53ZWarr0Yyg).
 * [Pain de mie](https://cuisine.journaldesfemmes.fr/recette/309493-pain-de-mie).

Pain de mie sur poolish:

 * La veille, mélanger les ingrédients de J1 :
  + Chauffer l'eau à 40°C.
  + Y mélanger la levure sèche petit à petit en remuant bien. Il ne doit pas y avoir de boules ou de grumeaux à la fin.
  + Mélanger avec la farine de seigle.
  + Le résultat doit être une pâte un peu liquide, cela doit couler de la cuillère comme un sirop épais (miel).
 * Laisser 10h dans le four éteint préchauffé à 40°C.
 * Le lendemain il doit y avoir des bulles sur le mélange, qui doit être léger, comme mousseux.
 * Préparer les ingrédients de J2 :
  + Mettre la farine restante (froment) dans une jatte.
  + Y mettre le sel.
  + Faire chauffer le lait à 40°C.
  + Y mélanger la levure sèche petit à petit en remuant bien. Il ne doit pas y avoir de boules ou de grumeaux à la fin.
  + Y mélanger le sucre.
  + Y mélanger le curcuma.
  + Verser l'huile dans la poolish et bien mélanger.
 * Mélanger la poolish à la farine.
  + Y ajouter le lait en plusieurs fois en tournant bien à chaque fois.
  + Mélanger avec les mains pour bien homogénéiser.
 * Laisser reposer 20 minutes.
 * Pétrir pendant 15-20 minutes.
 * Laisser lever pendant **1:15** (ou un peu plus si besoin) dans le four éteint préchauffé à **40°C**.
 * Huiler un ou deux moules à cake (suivant la quantité de pâte et la taille des moules).
 * Sortir la pâte (qui doit avoir doublé de volume) et la faire tomber sur une plan de travail fariné.
 * Fariner la pâte.
 * La plier 2-3 fois en appuyant dessus pour faire sortir l'air, en utilisant le coupe-pâte pour la soulever.
 * La couper si besoin en deux avec le coupe-pâte.
 * Former un ou des boudins et le(s) mettre dans le(s) moule(s).
 * Mettre les moules dans le four éteint préchauffé à **40°C** avec un **bol d'eau** pendant **45 minutes**.
 * Retirer les moules (la pâte doit avoir gonflé jusqu'en haut du moule) et préchauffer le four à 210°C.
 * Enfourner **20 minutes** à **210°C** avec 3 émissions de **vapeur d'eau** (à 0:00 (début), 6:00 et 12:00).

Pour un pain de mie de 400g avec 1/4 de seigle :

| Produit               | Base  | J1    | J2    |
| --------------------- | ----- | ----- | ----- |
| Farine de blé T55     | 300g  | 0     | 300g  |
| Farine de seigle T130 | 100g  | 100g  | 0     |
| sel                   | 7g    | 0     | 7g    |
| sucre                 | 12g   | 0     | 12g   |
| levure sèche          | 4g    | 1,5g  | 2,5g  |
| eau                   | 185ml | 185ml | 0     |
| lait                  | 125ml | 0     | 125ml |
| huile d'olive         | 40g   | 0     | 40g   |
| curcuma (optionnel)   | 5g    |       | 5g    |

Pour un pain de mie de 800g (ou deux pains de mie) avec 1/4 de seigle :

| Produit               | Base  | J1    | J2    |
| --------------------- | ----- | ----- | ----- |
| Farine de blé T55     | 600g  | 0     | 600g  |
| Farine de seigle T130 | 200g  | 200g  | 0     |
| sel                   | 14g   | 0     | 14g   |
| sucre                 | 24g   | 0     | 24g   |
| levure sèche          | 8g    | 3g    | 5g    |
| eau                   | 370ml | 370ml | 0     |
| lait                  | 250ml | 0     | 250ml |
| huile d'olive         | 80g   | 0     | 80g   |
| curcuma (optionnel)   | 10g   |       | 10g   |

### Pain de froment sur poolish

 * [Pain sur Poolish à la maison](https://www.youtube.com/watch?v=Me7tGobX-Gw).

Recette de la vidéo :

Produit   | Base  | J1    | J2
--------- | ----- | ----- | ----
T65       | 400g  | 70g   | 330g
T80       | 100g  | 100g  | 0g
sel       | 8g    | 0     | 8g
levure    | 4g    | 1g    | 3g
eau       | 340ml | 170ml | 170ml

 * Poolish : mélange J1 (un peu liquide, visqueux) à température ambiante pour 10h, filmé, protégé des courants d'air.
 * frasage : on ajoute le reste des ingrédients J2 et on mélange. On doit obtenir une pâte visqueuse.
 * on laisse reposer 20 min.
 * pétrissage / étirage pour former une boule.
 * pointage 1h15 à température ambiante, bien couvert. Le volume doit doubler, la pâte doit être douce et un peu collante.
 * division / mise en forme.
 * détente 10 min.
 * façonnage
 * apprêt 45 min, 25°C, dans le four avec bol d'eau. On pose le pain sur un linge de lin tamisé de farine, soudure en bas.
 * on renverse le pain sur la plaque, soudure vers le haut.
 * cuisson 25min à 250°C

Recette essayée le 15/05/2020:

Produit                     | Base  | J1    | J2
--------------------------- | ----- | ----- | ----
Farine de blé semi-complète | 800g  | 200g  | 600g
du moulin de Mirebeau       |       |       |     
sel                         | 13g   | 0     | 13g
levure                      | 8g    | 3g    | 5g
eau                         | 500ml | 250ml | 250ml + 15 ml

### Seigle sur Poolish

  * Cuisson : 20 min. 250°C et 10 min. 220°C --> utile ?

Produit              | Base           | J1             | J2
-------------------- | -------------- | -------------- | ----
Farine seigle        | 400g + ~250 g  | 200g           |    g
Farine blé 1/2 comp. | 100g           |   0g           |    g
sel                  |   8g           |   0            |    g
levure               |   4g + 4g      |   2g           |    g
eau                  | 400ml          | 200ml + ??? ml |    ml


## Pâtes, raviolis, gnocchi chaussons salés
 
 * [Cuocere la pasta a fuoco spento](https://blog.gvllozafferano.it/ortaggichepassionebysara/cuocere-la-pasta-a-fuoco-spento/).

 * [Pasta con besciamella al forno](https://blog.giallozafferano.it/lacucinadiloredana/pasta-con-besciamella-al-forno/).

 * [Nouilles à la farine de riz](https://www.qooq.com/recipes/nouilles-la-farine-de-riz).

 * [Make Gyoza At Home (Pasta Roller Hack)](https://cookingtoentertain.com/gyoza/).

 * [Gnocchi di patate](https://ricette.giallozafferano.it/Gnocchi-di-patate.html).
 * [Gnocchi alla romana](http://ricette.giallozafferano.it/Gnocchi-alla-romana.html).

 * [Pasta e patate alla napoletana](https://ricette.giallozafferano.it/Pasta-e-patate-alla-napoletana.html).

 * [Spaghetti alle vongole](https://ricette.giallozafferano.it/Spaghetti-alle-vongole.html).
 * [Pasta con le sarde](https://ricette.giallozafferano.it/Pasta-con-le-sarde.html).
 * [Spaghetti al tonno](https://ricette.giallozafferano.it/Spaghetti-al-tonno.html).

 * [Pasta e fagioli](https://ricette.giallozafferano.it/Pasta-e-fagioli.html).
 * [Pasta e fagioli alla napoletana](https://ricette.giallozafferano.it/Pasta-e-fagioli-alla-napoletana.html).
 * [Pasta e ceci](https://ricette.giallozafferano.it/Pasta-e-ceci.html).
 * [Pasta e lenticchie](https://ricette.giallozafferano.it/Pasta-e-lenticchie.html).

 * [Spaghetti alla Carbonara](https://ricette.giallozafferano.it/Spaghetti-alla-Carbonara.html).
 * [Lasagne alla Bolognese](https://ricette.giallozafferano.it/Lasagne-alla-Bolognese.html).

 * [Lasagne con la zucca](http://ricette.giallozafferano.it/Lasagne-con-la-zucca.html).
 * [Lasagne di carciofi ai formaggi](http://ricette.giallozafferano.it/Lasagne-ai-carciofi.html).

 * [Pasta con barbabietola e pecorino](https://ricette.giallozafferano.it/Pasta-con-barbabietola-e-pecorino.html)

 * [Spaghetti con sugo alle carote](https://ricette-utenti.cookaround.com/spaghetti-con-sugo-alle-carote.html).
 * [Pasta con crema di carote](https://blog.giallozafferano.it/graficareincucina/pasta-crema-carote/).
 * [Pasta con pesto di carote](https://www.misya.info/ricetta/pasta-con-pesto-di-carote.htm).
 * [Pasta di semola di grano duro a mano](http://www.cookaround.com/ricetta/Pasta-di-semola-di-grano-duro-a-mano.html).

 * [Genovese](https://ricette.giallozafferano.it/Genovese.html).

 * [Orecchiette](http://ricette.giallozafferano.it/Orecchiette.html).
 * [Orecchiette broccoli e salsiccia](https://ricette.giallozafferano.it/Orecchiette-broccoli-e-salsiccia.html).

 * [Spaetzle, pâtes alsaciennes](https://www.recettes-alsace.fr/spaetzle-pates-alsaciennes/).

 * [Soba noodles in duck broth](http://www.japanesefoodreport.com/2008/01/soba-noodles-in-duck-broth.html).
## Poisson

 * [Rôti de lotte enlardé et ratatouille aux légumes provençaux](https://www.lecreuset.fr/fr_FR/roti-de-lotte-enlarde-et-ratatouille-aux-legumes-provencaux/r0000000001185.html).

 * [Fish & ships (Golden beer-battered fish with chips)](https://www.bbcgoodfood.com/recipes/7785/golden-beerbattered-fish-with-chips).

 * [Filet de hoki à la bordelaise](https://cuisine.notrefamille.com/recettes-cuisine/filet-de-hoki-a-la-bordelaise-_29953-r.html).

 * [Pesce spada alla siciliana](https://ricette.giallozafferano.it/Pesce-spada-alla-siciliana.html).

 * [Harengs fumés marinés façon brasserie](https://www.marmiton.org/recettes/recette_harengs-fumes-marines-facon-brasserie_51355.aspx).

Langoustines : dans l'eau bouillante, attendre que l'ébullition redémarre et compter 2-3 minutes.

Crevettes roses : 30s dsans l'eau bouillante ?

### Morue

 * [Brandade de morue](http://www.marmiton.org/recettes/recette_brandade-de-morue_12736.aspx).
 * [Morue du capitaine](https://www.marmiton.org/recettes/recette_morue-du-capitaine_55446.aspx).
 * [Morue à la catalane](https://www.marmiton.org/recettes/recette_morue-a-la-catalane_23039.aspx).
## Riz

 * [Sartù di riso vegetariano, leggero e goloso](https://www.lacuocaecletticaveg.com/sartu-di-riso-vegetariano-leggero-e-goloso/). Al sugo, con uova e scalogni.
 * [Sartù di riso senza carne: la specialità napoletana in chiave vegetariana](https://primochef.it/sartu-di-riso-vegetariano/cucina-naturale/).
 * [RISO AL FORNO con sugo e piselli](https://blog.giallozafferano.it/studentiaifornelli/riso-al-forno-sugo-e-piselli/). Con aglio.
 * [Riso al forno con piselli](https://www.petitchef.it/ricette/portata-principale/riso-al-forno-con-piselli-fid-992207). Con cipolla.

 * [Riz cantonais](https://spicetrekkers.com/recipes/cantonese-fried-rice  https://www.kikkoman.co.uk/recipes/all/classic-cantonese-fried-rice/).
 * [Riz pilaf](https://www.marmiton.org/recettes/recette_riz-pilaf_27905.aspx).

 * [Risotto allo Zafferano](https://ricette.giallozafferano.it/Risotto-allo-Zafferano.html). Le pistils de safran doit infuser dans l'eau froide toute la nuit, depuis la veille.
 * [Risotto al cotechino, lenticchie e spumante](https://ricette.giallozafferano.it/Risotto-al-cotechino-lenticchie-e-spumante.html).
 * [Risotto all'Amarone](https://ricette.giallozafferano.it/Risotto-all-Amarone.html).
 * [Risotto coi broccoli](https://ricette.giallozafferano.it/Risotto-coi-broccoli.html).

 * [Miso Fried Rice with Egg and Greens](https://nourisheveryday.com/miso-fried-rice-with-egg-and-greens/).

 * [Poivrons farcis au thon](https://www.cuisineaz.com/recettes/poivrons-farcis-au-thon-12661.aspx).

 * [Perfect Sushi Rice](https://www.allrecipes.com/recipe/99211/perfect-sushi-rice/).

### Riz indien épicé aux noix de cajoux et raisins

 * [Spicy Indian rice](https://www.bbcgoodfood.com/recipes/3238/spicy-indian-rice).

Ingredients:

 * a mugful of American long grain rice
 * 2 onions, sliced
 * 2 tbsp sunflower oil
 * ½ tsp turmeric
 * 1 cinnamon stick
 * 6 cardamom pods
 * 1 tsp cumin seeds
 * large handful of sultanas and roasted cashew nuts

Steps:

 1. Fry the onions in the sunflower oil in a large frying pan for 10-12 minutes until golden.
 2. Fill a roomy saucepan with water, bring to the boil and tip in a heaped teaspoon of salt - the water will bubble furiously. Add turmeric and the cinnamon stick to the water. Pour in the rice, stir once and return to the boil, then turn the heat down a little so that the water is boiling steadily, but not vigorously. Boil uncovered, without stirring (this makes for sticky rice) for 10 minutes. Lift some out with a slotted spoon and nibble a grain or two. If they're too crunchy, cook for another minute and taste again. They should be tender but with a little bite. Drain the rice into a large sieve and rinse by pouring over a kettle of very hot water
 3. Stir the crushed seeds of the cardamom pods into the onions with the cumin seeds and fry briefly.
 4. Toss in a large handful of sultanas and roasted cashew nuts, then the hot drained rice. This is great with curry and will also give an exotic edge to grilled meat or fish.

### Riz bastami indien

 * [Vegan Indian Basmati Rice Recipe](https://www.thespruceeats.com/easy-vegan-indian-basmati-rice-recipe-3378479).

Ingredients:

 * 1 cup basmati rice (uncooked)
 * Water for soaking
 * 2 Tbsp olive oil (or peanut oil)
 * 1-inch cinnamon stick (broken in half)
 * 1 bay leaf
 * 1/2 tsp cumin seeds
 * 1 whole clove (you can use 2 to 3 if you really like cloves)
 * 1/2 tsp whole black peppercorns
 * 1 large onion (chopped thin)
 * 1 cup basmati rice
 * 2 cups water
 * 1/2 tsp salt (or to taste)

Steps:

 1. Cover the Basmati rice with water in a small bowl and set aside. Allow the rice to soak for 20 minutes.
 2. Once the rice is done soaking (after 20 minutes), drain any excess water and move on to the next step.
 3. Over medium heat, warm the olive oil or peanut oil in a sauté pan or large skillet and add the cinnamon stick (break it in half), bay leaf, cumin seeds, whole clove and peppercorns. Warm these spices for just 10 to 15 seconds, then add the chopped onion and heat until the onion is well cooked, about 6 to 8 minutes.
 4. Add the soaked but uncooked Basmati rice, and toast, stirring, for 20 to 30 seconds, just until lightly browned, then add the water and bring to a boil.
 5. Once the water is boiling, reduce the heat to a low simmer, cover, and allow the rice to cook for 15 to 20 minutes, until rice is cooked through and liquid is absorbed. You can stir occasionally, as needed.
## Sauces

 * [Sauce béchamel](http://www.marmiton.org/recettes/recette_bechamel_23880.aspx).
  + 50g farine
  + 50g beurre ou huile
  + 60cl lait
  + sel
  + poivre
  + muscade

 * [Crème pâtissière facile](https://www.marmiton.org/recettes/recette_creme-patissiere-facile_28714.aspx).
 * [Crème pâtissière](https://www.marmiton.org/recettes/recette_creme-patissiere_35195.aspx).
## Tartes salées, cakes, pizzas & chaussons

 * [Cake à la Bretonne au Sarrasin](https://www.marmiton.org/recettes/recette_cake-a-la-bretonne-au-sarrasin_45582.aspx).

 * [Gino Sorbillo's Neapolitan Pizza](https://www.youtube.com/watch?v=E4r0tGfTh68).
  - Farina  1,55kg
  - Sale    45gr
  - Acqua   1L
  - Lievito 1,5g  (levure fraîche)

 * [Panzerotti (calzoni) fritti](https://ricette.giallozafferano.it/Panzerotti-calzoni-fritti.html).

CHAUSSONS LIBANAIS À LA VIANDE		Liban		http://aime-mange.com/chaussons-libanais-a-la-viande/	

Flammekueche au Munster	fromage	France	Alsace	http://www.marmiton.org/recettes/recette_flammekueche-au-munster_25606.aspx	
Quiche au poulet		France		http://www.marmiton.org/recettes/recette_quiche-au-poulet_32350.aspx	
Tourte au munster	munster	France	Alsace	http://www.marmiton.org/recettes/recette_tourte-au-munster_15080.aspx	
Tourte au Poulet		France		http://www.marmiton.org/recettes/recette_tourte-au-poulet_48684.aspx	

 * [Clafoutis poires, roquefort et noix, sauce au miel](http://cuisine.notrefamille.com/recettes-cuisine/clafoutis-poires-roquefort-et-noix-sauce-au-miel-_25811-r.html).
 * [Cake aux olives et lardons](https://www.marmiton.org/recettes/recette_cake-aux-olives-et-lardons_11292.aspx).

### Quiche lorraine

 * [Quiche lorraine](https://www.cuisineactuelle.fr/guide-du-bien-manger/conversions-et-mesures/recette-de-la-vraie-quiche-lorraine-319146).
 * [](https://www.regal.fr/recettes/plats/quiche-lorraine-7657)
 * [](https://www.femmeactuelle.fr/cuisine/recettes-de-cuisine/vraie-recette-quiche-lorraine-secrets-reussite-27192)
 * [Quiche Lorraine](https://www.marmiton.org/recettes/recette_quiche-lorraine_30283.aspx).

Avec du lait:
 * Lait ...mL
 * 2 œufs
 * muscade
 * lardons ...g
 * 200 degré. 30min.
## Tofu & seitan

 * [Blanquette de seitan](http://lesdelicesdelauriane.blogspot.com/2015/01/blanquette-de-seitan-vegan.html).

 * [Mapo Tofu](https://www.chinasichuanfood.com/mapo-tofu-recipe/).

 * [Tofu bourguignon végétalien](http://www.recettesjecuisine.com/fr/recettes/plats-principaux/vege/tofu-bourguignon-vegetalien/).

 * [Ginger sweet tofu with pak choi](https://www.bbcgoodfood.com/recipes/ginger-sweet-tofu-pak-choi).
 * [Sesame-crusted tofu with gingery noodles](https://www.bbcgoodfood.com/recipes/sesame-crusted-tofu-gingery-noodles).
 * [Miso-glazed tofu steaks with beansprout salad & egg strands](https://www.bbcgoodfood.com/recipes/miso-glazed-tofu-steaks-beansprout-salad-egg-strands).
 * [Pan-Fried Sesame Garlic Tofu](https://www.tablefortwoblog.com/pan-fried-sesame-garlic-tofu/).

 * [Okonomiyaki Authentic Recipe](https://www.chopstickchronicles.com/osaka-okonomiyaki/).

 * [Oden](https://www.justonecookbook.com/oden/).
## Viande, œufs

Temps de cuisson des saucisses de Francfort : 5 minutes dans l'eau frémissante.

### Œufs

 * [Japanese Omelette (Tamagoyaki)](https://tasty.co/recipe/japanese-omelette-tamagoyaki).

### Poulet

 * [Blanquette de poulet](http://www.marmiton.org/recettes/recette_blanquette-de-poulet_66981.aspx).
 * [Poule au pot façon grand-mère](http://madame.lefigaro.fr/recettes/poule-pot-facon-grand-mere-170609-201072).

 * [Tajine de poulet aux pruneaux](http://www.marmiton.org/recettes/recette_tajine-de-poulet-aux-pruneaux_20082.aspx).

 * [Poulet à l’ananas](https://blog.giallozafferano.it/ilchiccodimais/pollo-allananas-ricetta-cinese/).
 * [Japanese Chicken, Radish and Tofu Stew](https://www.foodiebaker.com/japanese-chicken-radish-and-tofu-stew/).
 * [Butter chicken](https://www.allrecipes.com/recipe/45957/chicken-makhani-indian-butter-chicken/).

### Porc

 * [Nems (recette facile)](https://www.adeline-cuisine.fr/recettes/nems-vietnamiens-recette-facile/).
   + Au four 180°C badigeonnés d'huile pendant 15 minutes ?

 * [Filet mignon de porc en croûte tout simple](https://www.marmiton.org/recettes/recette_filet-mignon-de-porc-en-croute-tout-simple_51838.aspx).

 * [Choux farcis au lard fermier, châtaignes et grue de cacao](https://www.lecreuset.fr/fr_FR/choux-farcis-au-lard-fermier--chataignes-et-grue-de-cacao/r0000000001133.html).

 * [Cassoeula](https://www.cookaround.com/ricetta/Cassoeula.html). Ragoût de Lombardie.
 * [Tracchiulella cu ‘o ‘rraù](https://www.vesuviolive.it/cucina/141551-ricetta-della-tracchiulella-cu-rrau-perche-si-chiama-cosi/). Ragoût napolitain.

### Veau

 * [Blanquette de veau facile](https://www.marmiton.org/recettes/recette_blanquette-de-veau-facile_19219.aspx).
 * [Rôti de veau au four](https://www.marmiton.org/recettes/recette_roti-de-veau-au-four_61313.aspx).
 * [Tajine de veau aux pruneaux](http://www.recetteshanane.com/2016/06/tajine-de-veau-aux-pruneaux.html).

 * [Osso buco](https://www.ricardocuisine.com/recettes/2149-osso-buco).

Rôti de veau en cocotte: 10 minute pour 450gr
 * Pour 800gr cuire : 17 minutes
 * Pour 1kg cuire : 22 minutes
 * Pour 1,2kg cuire : 26 minutes

Rôti de veau au four (190°C):
 * Pour 750 gr de viande compté 45 minutes
 * Pour 1 Kg de viande compté 1 heure

### Bœuf

 * [Daube provençale](https://www.marmiton.org/recettes/recette_daube-provencale_11978.aspx).
 * [Rôti de boeuf au four tout simple](https://www.marmiton.org/recettes/recette_roti-de-boeuf-au-four-tout-simple_342546.aspx).
 * [Hachis parmentier](https://www.cuisineaz.com/recettes/hachis-parmentier-8949.aspx).

### Agneau

 * [Souris d'agneau au miel et thym](https://www.marmiton.org/recettes/recette_souris-d-agneau-au-miel-et-thym_93493.aspx).
