# Work

## Dev

 * [GitLab](https://gitlab.com/).
 * [GitHub](https://github.com/).

## Training

 * [pktraining](https://gitlab.com/pktraining/).

 * [Git class](https://pktraining.gitlab.io/git-class/).
 * [GitLab class](https://pktraining.gitlab.io/gitlab-class/).
 * [Python class](https://pktraining.gitlab.io/python-class/).

### Cours Python Mines

 * [python-advanced/notebooks at main · ue12-p21/python-advanced](https://github.com/ue12-p21/python-advanced/tree/main/notebooks).
 * [Using at the Command Line — Jupytext documentation](https://jupytext.readthedocs.io/en/latest/using-cli.html).
 * [boisgera/python-advanced-companion: Compléments à Python Avancé](https://github.com/boisgera/python-advanced-companion).
 * [boisgera/python-fr: Python 🇫🇷](https://github.com/boisgera/python-fr).
 * [Python : des fondamentaux aux concepts avancés du langage - Course - FUN MOOC](https://www.fun-mooc.fr/en/cours/python-3-des-fondamentaux-aux-concepts-avances-du-langage/).
 * [Support Étudiants - Python avancé](https://nbhosting.inria.fr/builds/ue12-p22-python/handouts/latest/0-00-intro.html).
 * [TP Labyrinthes](https://boisgera.github.io/python-fr/tps/maze/index.html).
 * [3.10.8 Documentation](https://docs.python.org/3/index.html).
 * [INRIA nbhosting - Python avancé](https://nbhosting.inria.fr/builds/ue12-p21-python-advanced/handouts/latest/0-00-intro.html).
 * [Les Fonctions (PDF)](https://github.com/ue12-p21/python-advanced/blob/main/notebooks/media/les-fonctions.pdf).
 * [mines-p22 courses](https://nbhosting.inria.fr/public/mines-p22).
 * [ue12-p22 · GitHub](https://github.com/ue12-p22).
 * [TP : Student et Class — Python Exercices](https://nbhosting.inria.fr/builds/python-exos/python/latest/python-tps/students-grades/README-students.html).

## CEA

 * [COVID-19 Informations aux salariés](https://infos-salaries.cea.fr/).

 * [TalkSpirit](https://cea.talkspirit.com/).

 * [Institut de biologie François Jacob](https://jacob.cea.fr/).

 * [Malakoff Humanis](https://connexion.malakoffhumanis.com/particuliers/login/).

### CE

 * [ACAS - ALAS](https://oaasis.cea.fr/).
 * [ACTG](http://actg.fr/).

### CNRGH

 * [local GitLab at CNRGH](https://gitlab.ibfj-evry.fr/).
 * [CNRGH · GitLab.com](https://gitlab.com/cnrgh).
 * [CNRGH GitHub](https://github.com/CNRGH).

 * [Mattermost](https://mattermost.ibfj-evry.fr/lbi/channels/town-square).
 * [Point LBI | Jitsi Meet](https://meet.ibfj-evry.fr/PointLBI).
 * [CNRGH webmail](https://webmail.cnrgh.fr/).

 * [Wiki CNRGH](http://wiki.cnrgh.fr/).

 * [GLPI - Support system](https://glpi.ibfj-evry.fr/)

 * [Rundeck](https://rundeck.cnrgh.fr/).

Doc:
 * [Site doc HPC Nicolas Wiart](http://dev.cng.fr/hpc/html/).
 * [A.3. Agents SSH au CNRGH et Genoscope - - Calcul Haute Performance en Bioinformatique](http://dev.cng.fr/hpc/html/annexe-ssh-agent.html#annexe-ssh-agent-gitlab).

#### FromAG

 * [FromAG](https://gitlab.cnrgh.fr/lbi/FromAG).
 * [FromAG Browser - lbi4](https://lbi4.dev.os.cnrgh.fr/).
 * [FromAG Browser - gnomad-web.tst](https://gnomad-web.tst.os.cnrgh.fr/).
 * [FromAG Browser - release](https://gnomad-web.cnrgh.fr/).

### Telework

 * [VPN SSL](https://vpnssl.cea.fr/).
 * [Web mail](https://webmail.cea.fr/).

### Transport

 * [Follow Bus](https://cmsaclay.followbus.fr/accueil).
 * [Plan Transport 2019](http://plantransport3.blogspot.com/).

