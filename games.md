# Games

 * [tome2-ldasg | Tales of Maj'Eyal and T-Engine4](https://te4.org/tome2-ldasg).
 * [LEGO® Pick a Brick | Boutique LEGO® officielle FR](https://www.lego.com/fr-fr/pick-and-build/pick-a-brick).

## Jeux en ligne

 * [Board Game Arena](https://en.boardgamearena.com/).

 * [Ouverture pas facile](https://ouverture-pas-facile.fr/). <https://ouverture-pas-facile.fr/enigmes/jeu.html>
 * [Le Prisionnier Quantique](https://prisonnier-quantique.fr/index.html).
 * [Jeux vidéos d'ARTE](https://www.arte.tv/digitalproductions/fr/categories/jeux-video/).

## Escape games:

  * [The Game](https://www.thegame-france.com/).
  * [L'Antichambre](https://www.lantichambre.paris/). À partir de 12 ans.
  * [Top 3 des meilleurs Escape Games long format en France](https://www.escapegame.fr/top/top-escape-games-long-format/).

ESCAPE GAMES / MAISON HANTÉE :
 * https://thedarkdreams-paris.com
 * https://www.lemanoirmaudit.com/
 * https://www.panikroom.fr/
 * https://www.victoryescapegame.fr/

## Jeux de société

 * The Mandalorian: Adventures 

 * [Alex Crispin - Escape the Dark Castle (Original Soundtrack) (2018) (Old-School Dungeon Synth)](https://www.youtube.com/watch?v=Pa1IqU8fHfo).

 * [BOHNANZA](http://jeuxstrategie.free.fr/Bohnanza_complet.php)
 * [La vidéorègle du jeu "Bohnanza"](https://www.youtube.com/watch?v=uvgRU5mdfUU). 
 * [Le Lièvre & la Tortue : comment ça marche ?](https://www.youtube.com/watch?v=7m_DCs5bM2o).

 * [Steve Jackson Games](http://www.sjgames.com/). GURPS, Munchkin, ...

 * [Demian's Gamebook Web Page](https://gamebooks.org/).
 * [Fighting Fantasy Project](http://www.ffproject.com). Online playable gamebooks.
 * [10 Best Gamebook Apps](http://www.tomsguide.com/us/best-gamebook-apps,review-2419.html).
 * [Project Aon (Lone Wolf)](https://www.projectaon.org/en/Main/Home).
 * <http://gamebookadventures.com/>.

Magic the Gathering:

 * [Règles de base](https://www.smfcorp.net/mtg-reglesdebase-index.html).
 * [Lexique](https://www.smfcorp.net/mtg-lexique-index.html).
 * [Règles officielles](https://www.magic-ville.com/fr/regles/reglesoff.php).

Strategy:

 * Fabled lands 1 : Le Royaume déchiré
 * Terraforming Mars
 * Through the Ages
 * K2
 * [Schotten Totten](https://www.philibertnet.com/fr/iello/43124-schotten-totten-vf-iello-3760175513022.html).

Solo/coop Cthulhu & Sherlock:

 * 40€ Bureau of Investigation : Enquêtes à Arkham & Autres Contrées ---> 4 enquêtes
 * 50€ Sherlock Holmes - Détective Conseil : Les Meurtres de la Tamise et Autres Enquêtes ---> 10 enquêtes
 * 55€ Horreur à Arkham : Le Jeu de Cartes - Édition Révisée
 * 100€ Cthulhu - Death May Die: 1 à 5 joueurs. On doit interrompre le rituel qui invoque le Grand Ancien.
   + La Chèvre Noire: extension de  Cthulhu - Death May Die

Coop:

 * 56€ Betrayal at house on the hill: 3 joueurs ou +

Solo:

 * 30€ Warp's Edge. Jeu de carte solo
 * 14€ M.A.R.I. et l'usine foudroyée
 * 15€ M.A.R.I. on Mars
 * 36€ Final Girl: basé sur les films d'horreurs classiques. <https://www.youtube.com/watch?v=9is7J4oHcAw>.
 * 32€ Under Falling Skies

Jeux coopératifs style JDR:

  * Gloomhaven (~160 EUR) & Frosthaven (~230 EUR) : système d'aventure 1-4 joueurs, beaucoup d'heures/jours de jeu, système tactique de combat par cartes, avec progression des personnages et de l'aventure. Surtout basé autour du combat.
  * Zombicide
  * Aliens
  * Massive Darkness
  * Lands of Clazyr
  * Heredity
  * Les demeures de l'épouvante
  * Aeons End: deck building
  * Dark Rituals: 2-5 joueurs
  * Dungeon saga
  * Hero Quest: Fonctionne avec une appli. Refonte d'un vieux jeu, années 70-80, Gamesworkshop. Un peu cher pour ce que c'est.
  * Descent V3 : Fonctionne avec une appli. pas de système de leveling. 1-4 joueurs. 3-4h par partie.
  * Erune: Fonctionne avec une appli. a un système de leveling. 1-5 joueurs. 30' par partie.
  * Sword & Sorcery: 1 to 5 players. Dungeon crawler, with a campaign, lots of cards to make the monsters all different. Exploration de donjon, avec des cartes des pièces, des figurines, ...

### JDR

 * D&D:
   + [Scénarios](https://www.aidedd.org/adj/scenarios/).


## Jeux en plein air

 * [Mölkky](https://fr.wikipedia.org/wiki/Mölkky).

## Video games

 * [Puzzle games and explanations how to solve them](http://pzl.org.uk/): : Sudoku, Kakuro, Sokoban, Minesweeper, Othello, ...

 * [Raspberry PI](https://www.raspberrypi.org/).
 * [Retro Gaming Arcade Console with Raspberry Pi (RetroPie)](http://wiki.imal.org/project/retro-gaming-arcade-console-raspberry-pi-retropie).
 * [Planet Emulation](https://www.planetemu.net/).
 * [Edge Emulaton ROMs](https://edgeemu.net/).
 * [Emulator Games](https://www.emulatorgames.net/).

 * [Virtual Time](https://virtualtime.com/). Réalité virtuelle.

 * [X-Plane](https://www.x-plane.com/). Flight simulator.
   + [Older versions](https://www.x-plane.com/desktop/try-it/older/).

### Game & Watch

 * [Game & Watch](https://en.wikipedia.org/wiki/Game_%26_Watch).

### CPC & old games

 * [CPC Game Reviews](http://www.cpcgamereviews.com).
 * [CPC-SOFTS](https://www.cpc-power.com/).
 * [CPC Live](http://cpc-live.com/data/index.php?dir=A).
 * [Gamespot](www.gamespot.com).
 * [RetroPie](https://retropie.org.uk/).
 * [Games Database](https://www.gamesdatabase.org/). Propose aussi les musiques.
 * [World of Spectrum](http://www.worldofspectrum.org/).
 * [The Software Library: ZX Spectrum](https://archive.org/details/softwarelibrary_zx_spectrum).
 * [WoWroMs](https://wowroms.com/).
 * [Old Games](https://www.old-games.com). PAYANT. Vieux jeux PC : Discworld, Alone in the Dark, Little Big Adventure, ...
 * [Best Old Games](https://www.bestoldgames.net/). Found Lands of Lore here.

### PC / DOS / APPLE

 * [DOSGames.com](https://dosgames.com/).
 * [Abandonware DOS](https://www.abandonwaredos.com/).
 * [DOS Games Archive](https://www.dosgamesarchive.com/). PAYANT. Font payer les versions complètes des jeux.
 * [Abandonware France](https://www.abandonware-france.org/).

### Atari

 * [Atari mania](http://www.atarimania.com/).
